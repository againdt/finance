package com.getinsured.hix.finance.cybersource;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.cybersource.serviceimpl.OneTimeCreditCard;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;

public class OneTimeCreditCardTest extends BaseTest {

	@Autowired
	@Qualifier("OneTimeCreditCard")
	OneTimeCreditCard cccyb;
	
	@Test
	public void testAuthorizeCard() {
		HashMap request =new HashMap(); 
		
		request.put("firstName", "John");
        request.put("lastName", "Doe");
        request.put("street1", "1295 Charleston Road");
        request.put("city", "Mountain View");
        request.put("state", "CA");
        request.put("zip", "94043");
        request.put("country", "US");
        request.put("email", "nobody@cybersource.com");
        request.put("cardNumber", "4111111111111111");
        request.put("cardType", "001");

        //Split the expirationDate
        request.put("expirationMonth", "12");
        request.put("expirationYear", "2020");
        request.put("amount", "11.11");
				
        Map reply = cccyb.authorizeCard(request,"11");
	}

	@Test
	public void testCaptureCard() {
		HashMap request =new HashMap(); 
		
		request.put("firstName", "John");
        request.put("lastName", "Doe");
        request.put("street1", "1295 Charleston Road");
        request.put("city", "Mountain View");
        request.put("state", "CA");
        request.put("zip", "94043");
        request.put("country", "US");
        request.put("email", "nobody@cybersource.com");
        request.put("cardNumber", "4111111111111111");
        request.put("cardType", "001");

        //Split the expirationDate
        request.put("expirationMonth", "12");
        request.put("expirationYear", "2020");
				
        Map reply = cccyb.authorizeCard(request, "11");
        if(reply.get("decision").equals("ACCEPT")){
        	
        	Map capReply = cccyb.captureCard(reply.get("requestID").toString(), "11", CyberSourceKeyConstants.MERCHANT_CODE);
        }
	}

	@Test
	public void testCreditToCard() {
		HashMap request =new HashMap(); 
		request.put("firstName", "John");
        request.put("lastName", "Doe");
        request.put("street1", "1295 Charleston Road");
        request.put("city", "Mountain View");
        request.put("state", "CA");
        request.put("zip", "94043");
        request.put("country", "US");
        request.put("email", "nobody@cybersource.com");
        request.put("cardNumber", "4111111111111111");
        request.put("cardType", "001");

        //Split the expirationDate
        request.put("expirationMonth", "12");
        request.put("expirationYear", "2020");
				
        Map reply = cccyb.authorizeCard(request, "11");
        if(reply.get("decision").equals("ACCEPT")){

        	Map capReply = cccyb.creditToCard(request, "11");
        }
	}

}
