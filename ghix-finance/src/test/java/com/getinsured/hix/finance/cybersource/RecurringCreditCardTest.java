package com.getinsured.hix.finance.cybersource;

import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingException;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.cybersource.serviceimpl.CreditCardProcessor;
import com.getinsured.hix.finance.cybersource.serviceimpl.CustomerProfile;

public class RecurringCreditCardTest extends BaseTest
{
	@Autowired private CustomerProfile customerProfile;
	private static final Logger LOGGER = LoggerFactory.getLogger(RecurringCreditCardTest.class);
	private static String subscription_Id = "9997000175875604";
	@Autowired private CreditCardProcessor creditCardProcessor; 
	
	@BeforeClass
    public static void setUpDataSource() throws Exception {
        try {
            SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();

            OracleConnectionPoolDataSource ds = new OracleConnectionPoolDataSource();
            ds.setURL("jdbc:oracle:thin:@166.78.42.8:1521:ghixdb");
            ds.setUser("nmdev_123");
            ds.setPassword("nmdev_123#");
            builder.bind("jdbc/ghixDS", ds);
            builder.activate();
        } catch (NamingException ex) {
            ex.printStackTrace();
        }
}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	//@Ignore
	@Test
	public void createCreditCardSubscription()
	{
		HashMap createRequest = new HashMap();
		createRequest.put("billTo_firstName", "Alex");
		createRequest.put("billTo_lastName", "Anelka");
		createRequest.put("billTo_street1", "1295 Charleston Road");
		createRequest.put("billTo_city", "Mountain View");
		createRequest.put("billTo_state", "CA");
		createRequest.put("billTo_postalCode", "94043");
		createRequest.put("billTo_country", "US");
		createRequest.put("billTo_email", "alex.anelka@orient.com");
		createRequest.put("billTo_phoneNumber", "650-965-6000");
		createRequest.put("purchaseTotals_currency", "USD");
		createRequest.put("subscription_title", "Pymt Date Test");
		createRequest.put("subscription_paymentMethod", "credit card");
		createRequest.put("IS_RECURRING","TRUE");
		createRequest.put("recurringSubscriptionInfo_frequency", "weekly");
		createRequest.put("recurringSubscriptionInfo_numberOfPayments","12");
		createRequest.put("recurringSubscriptionInfo_startDate", "20141021");
		createRequest.put("recurringSubscriptionInfo_amount","99.99");
		
		createRequest.put("card_cardType", "001");
		createRequest.put("card_accountNumber", "4111111111111111");
		createRequest.put("card_expirationMonth", "12");
		createRequest.put("card_expirationYear", "2015");
		
		try
		{
			LOGGER.info("Request: "+createRequest);
			Map reply = customerProfile.createCustomerProfile(createRequest);
			LOGGER.info("Received Reply : "+reply);
			subscription_Id = (String)reply.get("paySubscriptionCreateReply_subscriptionID");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("rawtypes")
	@Ignore
	public void testSubscriptionRetreive()
	{
		try
		{
			Map reply = customerProfile.retrieveSubScriptionInfo(subscription_Id);
			LOGGER.info("testSubscriptionRetreive() Received Reply : "+reply);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}
	
	//@Test
	@Ignore
	public void testCreditCardDebit()
	{
		Map<String, String> request = new HashMap<String, String>();
		request.put("merchantReferenceCode", "TEST-CREDIT-CARD_PAYMENT");
		request.put("purchaseTotals_grandTotalAmount", "450.55");
		request.put("recurringSubscriptionInfo_subscriptionID","9997000175875604");
		try
		{
			Map reply = creditCardProcessor.pciCredit(request, "450.55");
			System.out.println("testCreditCardDebit() Received Reply : "+reply);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	@Test
	@Ignore
	public void testCancelSubscription()
	{
		try
		{
			Map reply = customerProfile.pciCancelSubscription(subscription_Id);
			LOGGER.info("testUpdateSubscription() Received Reply : "+reply);
			testSubscriptionRetreive();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Ignore
	public void testUpdateSubscription()
	{
		try
		{
			HashMap updateRequest = new HashMap();
			updateRequest.put("recurringSubscriptionInfo_subscriptionID", subscription_Id);
			updateRequest.put("billTo_phoneNumber", "605-611-2310");
			updateRequest.put("card_expirationMonth", "12");
			updateRequest.put("card_expirationYear", "2014");
			updateRequest.put("card_accountNumber", "4111111111111234");
			
			Map reply = customerProfile.updateCustomerProfile(updateRequest);
			LOGGER.info("testUpdateSubscription() Received Reply : "+reply);
			testSubscriptionRetreive();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
}
