package com.getinsured.hix.finance.cybersource.report;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.payment.service.jpa.URLAuthenticator;
import com.getinsured.hix.platform.util.HIXHTTPClient;

/**
 * 
 * @author Sharma_k
 *
 */
public class IndividualPymtReportDownloadTest extends BaseTest
{
	@Autowired HIXHTTPClient hIXHTTPClient;
	
	@Test
	public void downloadReport()
	{
		try
		{
			URL url = new URL("https://ebc.cybersource.com/ebc/Query");
			
			String charset = "UTF-8";
			String merchantID = "cybersource_nmhix";
			String merchantReferenceNumber = "ISS_INV-DEC2013-2_20131219_210932";
			String type = "transaction";
			String subtype = "transactionDetail";
			String requestID = "3875089735830176056467";
			String versionNumber = "1.5";
			
			String query = String.format("merchantID=%s&merchantReferenceNumber=%s&type=%s&subtype=%s&requestID=%s&versionNumber=%s", 
			     URLEncoder.encode(merchantID, charset), 
			     URLEncoder.encode(merchantReferenceNumber, charset),
			     URLEncoder.encode(type, charset),
			     URLEncoder.encode(subtype, charset),
			     URLEncoder.encode(requestID, charset),
			     URLEncoder.encode(versionNumber, charset));
			
			
			System.out.println("========= DownloadReport() method started for URL: "+ url + "==========");
			
			if (url != null)
			{
				Authenticator.setDefault(new URLAuthenticator("cybersource_nmhix","newmexico1234$"));
				
				HttpURLConnection connection = (HttpURLConnection) url.openConnection(); 
				connection.setDoOutput(true); // Triggers POST.
				connection.setRequestProperty("Accept-Charset", charset);
				connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
				OutputStream output = connection.getOutputStream();
				try
				{
				     output.write(query.getBytes(charset));
				}
				finally 
				{
					try
					{
						output.close();
					} 
					catch (IOException logOrIgnore)
					{
						
					}
				}
				InputStream response = connection.getInputStream();
				if(response != null)
				{
					System.out.println("Received Response: "+convertStreamToString(response));
				}
			}
			
			System.out.println("========= DownloadReport() method end ==========");		
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	
	public String convertStreamToString(InputStream in)
	{
		InputStreamReader is = new InputStreamReader(in);
		StringBuilder sb=new StringBuilder();
		BufferedReader br = new BufferedReader(is);
		try
		{
			String read = br.readLine();
			while(read != null)
			{
				sb.append(read);
				read =br.readLine();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return sb.toString();
	}
	
	
}
