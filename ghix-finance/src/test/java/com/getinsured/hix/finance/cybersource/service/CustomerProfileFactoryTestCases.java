package com.getinsured.hix.finance.cybersource.service;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Factory;

import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;



@SuppressWarnings({ "rawtypes", "unchecked" })
public class CustomerProfileFactoryTestCases
{
	private Map commonCustomerProfileData = new HashMap();
	
	 @Factory
	 public Object[] factoryMethod() 
	 {
		 commonTestSampleData();
		 return new Object[] {
				 new CustomerProfileInterfaceTestNG(testDataForVisaCard()),
				 new CustomerProfileInterfaceTestNG(testDataForMasterCard()),
				 new CustomerProfileInterfaceTestNG(testDataForAmexCard()),
				 new CustomerProfileInterfaceTestNG(testDataForDiscoverCard())
		 };
	 }
	 
	 	@BeforeClass
		public Map commonTestSampleData()
		{
			commonCustomerProfileData.put(CyberSourceKeyConstants.BILLTO_FIRSTNAME_KEY, "Didier");
			commonCustomerProfileData.put(CyberSourceKeyConstants.BILLTO_LASTNAME_KEY, "Drogba");
			
			commonCustomerProfileData.put(CyberSourceKeyConstants.BILLTO_STREET1_KEY, "A-256 High street park");
			commonCustomerProfileData.put(CyberSourceKeyConstants.BILLTO_CITY_KEY, "Santa Fe");
			commonCustomerProfileData.put(CyberSourceKeyConstants.BILLTO_STATE_KEY, "CA");
			commonCustomerProfileData.put(CyberSourceKeyConstants.BILLTO_POSTALCODE_KEY, "90012");
			commonCustomerProfileData.put(CyberSourceKeyConstants.BILLTO_COUNTRY_KEY, "US");
			
			commonCustomerProfileData.put(CyberSourceKeyConstants.BILLTO_EMAIL_KEY, "didier.drogba@chelsea.com");
			commonCustomerProfileData.put(CyberSourceKeyConstants.BILLTO_PHONENUMBER_KEY, "6056042354");
			
			commonCustomerProfileData.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, "USD");
			commonCustomerProfileData.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_FREQUENCY_KEY, "on-demand");
			
			return commonCustomerProfileData;
		}
		
		public Map testDataForVisaCard()
		{
			Map requestMap = new HashMap();
			requestMap.putAll(commonCustomerProfileData);
			requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_TITLE_KEY, "VISA Credit Card");
			requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY, "credit card");
			
			requestMap.put(CyberSourceKeyConstants.CARD_CARDTYPE_KEY, "001");
			requestMap.put(CyberSourceKeyConstants.CARD_ACCOUNTNUMBER_KEY, "4111111111111111");
			requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONMONTH_KEY, "12");
			requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONYEAR_KEY, "2015");
			
			return requestMap;
		}
		
		public Map testDataForMasterCard()
		{
			Map requestMap = new HashMap();
			requestMap.putAll(commonCustomerProfileData);
			requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_TITLE_KEY, "Master Credit Card");
			requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY, "credit card");
			
			requestMap.put(CyberSourceKeyConstants.CARD_CARDTYPE_KEY, "002");
			requestMap.put(CyberSourceKeyConstants.CARD_ACCOUNTNUMBER_KEY, "5555555555554444");
			requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONMONTH_KEY, "12");
			requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONYEAR_KEY, "2015");
			
			return requestMap;
		}
		
		public Map testDataForAmexCard()
		{
			Map requestMap = new HashMap();
			requestMap.putAll(commonCustomerProfileData);
			requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_TITLE_KEY, "American Express Credit Card");
			requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY, "credit card");
			
			requestMap.put(CyberSourceKeyConstants.CARD_CARDTYPE_KEY, "003");
			requestMap.put(CyberSourceKeyConstants.CARD_ACCOUNTNUMBER_KEY, "378282246310005");
			requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONMONTH_KEY, "12");
			requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONYEAR_KEY, "2015");
			
			return requestMap;
		}
		
		
		public Map testDataForDiscoverCard()
		{
			Map requestMap = new HashMap();
			requestMap.putAll(commonCustomerProfileData);
			requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_TITLE_KEY, "Discover Credit Card");
			requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY, "credit card");
			
			requestMap.put(CyberSourceKeyConstants.CARD_CARDTYPE_KEY, "004");
			requestMap.put(CyberSourceKeyConstants.CARD_ACCOUNTNUMBER_KEY, "6011111111111117");
			requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONMONTH_KEY, "12");
			requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONYEAR_KEY, "2015");
			
			return requestMap;
		}
}