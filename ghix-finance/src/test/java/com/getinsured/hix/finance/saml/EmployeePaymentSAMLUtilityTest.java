package com.getinsured.hix.finance.saml;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/*import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;*/
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.getinsured.hix.dto.enrollment.EnrollmentPaymentDTO;
import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.saml.utils.EmployeePaymentSAMLUtility;
import com.getinsured.hix.finance.utils.FinanceConstants.SAMLConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;

/**
 * The class <code>EmployeePaymentSAMLUtilityTest</code> contains tests for the
 * class {@link <code>EmployeePaymentSAMLUtility</code>}
 *
 * @pattern JUnit Test Case
 *
 * @generatedBy CodePro at 8/25/14 11:00 PM
 *
 * @author sahoo_s
 *
 * @version $Revision$
 */
public class EmployeePaymentSAMLUtilityTest extends BaseTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeePaymentSAMLUtilityTest.class);
	private static final String GHIX_ENVIRONMENT = "ghixEnvironment";
	
	@Value("#{configProp}")
	private Properties configProp;
	
	@Value("#{samlConfigProp}")
	private Properties samlConfigProp;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	/*@Autowired
	private PooledPBEStringEncryptor pooledPBEStringEncryptor;
*/	
	
	/**
	 * Run the String prepareSAMLOutput(Map<String,String>,
	 * EnrollmentPaymentDTO) method test
	 */
	@Test
	public void testPrepareSAMLOutput()
	{
		// add test code here
		EmployeePaymentSAMLUtility samlUtility = new EmployeePaymentSAMLUtility();
		EnrollmentPaymentDTO inputData = this.createEnrollmetPaymentDTO();
		Map<String,String> configParams = this.prepareConfigParams(inputData);
		String result = samlUtility.prepareSAMLOutput(configParams, inputData);
		System.out.println("Resulted Encoded String :: "+result);
	}
	
	private Map<String, String> prepareConfigParams(EnrollmentPaymentDTO paymentDTO)
	{
		Map<String, String> inputParams = new HashMap<String, String>();
		String samlProperties = "_"+DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE)+"_"+paymentDTO.getHiosIssuerId();
		String securityProperties = "_"+DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE)+"_"+paymentDTO.getHiosIssuerId()+"_"+configProp.getProperty(GHIX_ENVIRONMENT);
		 
		inputParams.put(SAMLConstants.PAYREDIRECT_KEYSTORE_LOC, samlConfigProp.getProperty(SAMLConstants.PAYREDIRECT_KEYSTORE_LOC+securityProperties));
		inputParams.put(SAMLConstants.PAYREDIRECT_PRIVATE_KEY, samlConfigProp.getProperty(SAMLConstants.PAYREDIRECT_PRIVATE_KEY+securityProperties));
		inputParams.put(SAMLConstants.PAYREDIRECT_PASSWORD, ghixJasyptEncrytorUtil.decryptStringByJasypt(samlConfigProp.getProperty(SAMLConstants.PAYREDIRECT_PASSWORD+securityProperties)));
		inputParams.put(SAMLConstants.PAYREDIRECT_SECUREKEY_PASSWORD, ghixJasyptEncrytorUtil.decryptStringByJasypt(samlConfigProp.getProperty(SAMLConstants.PAYREDIRECT_SECUREKEY_PASSWORD+securityProperties)));
		inputParams.put(SAMLConstants.ISSUER_NAME, samlConfigProp.getProperty(SAMLConstants.ISSUER_NAME+samlProperties));
		inputParams.put(SAMLConstants.SECURITY_DNS_NAME, samlConfigProp.getProperty(SAMLConstants.SECURITY_DNS_NAME+samlProperties));
		inputParams.put(SAMLConstants.SECURITY_ADDRESS, samlConfigProp.getProperty(SAMLConstants.SECURITY_ADDRESS+samlProperties));
		inputParams.put(SAMLConstants.SECURITY_KEY_INFO, samlConfigProp.getProperty(SAMLConstants.SECURITY_KEY_INFO+samlProperties));
		inputParams.put(SAMLConstants.ISSUER_AUTH_URL, samlConfigProp.getProperty(SAMLConstants.ISSUER_AUTH_URL+samlProperties));
		LOGGER.info("Inside EmployeeSAMLController :: PAYREDIRECT_KEYSTORE_LOC :: "+inputParams.get(SAMLConstants.PAYREDIRECT_KEYSTORE_LOC)+" :: PAYREDIRECT_PRIVATE_KEY :: "
				+inputParams.get(SAMLConstants.PAYREDIRECT_PRIVATE_KEY)+" :: PAYREDIRECT_PASSWORD :: "+inputParams.get(SAMLConstants.PAYREDIRECT_PASSWORD)
				+" :: PAYREDIRECT_SECUREKEY_PASSWORD :: "+inputParams.get(SAMLConstants.PAYREDIRECT_SECUREKEY_PASSWORD));
		return inputParams;
	}
	
	private EnrollmentPaymentDTO createEnrollmetPaymentDTO() {
		EnrollmentPaymentDTO paymentDTO = new EnrollmentPaymentDTO();
		paymentDTO.setEnrollmentId(17166);
		paymentDTO.setCmsPlanId("61589ID164000101");
		paymentDTO.setPaymentTxnId("ID17166000000");
		paymentDTO.setNetPremiumAmt(Float.valueOf("132.34"));
		paymentDTO.setGrossPremiumAmt(Float.valueOf("200.34"));
		paymentDTO.setAptcAmt(Float.valueOf("100"));
		paymentDTO.setBenefitEffectiveDate(new TSDate());
		paymentDTO.setExchgSubscriberIdentifier("1000068063");
		paymentDTO.setFirstName("Test");
		paymentDTO.setMiddleName("Test");
		paymentDTO.setLastName("Test");
		paymentDTO.setAddressLine1("Boise");
		paymentDTO.setAddressLine2("");
		paymentDTO.setState("ID");
		paymentDTO.setZip("83701");
		paymentDTO.setContactEmail("harry.gilli@mailcatch.com");
		paymentDTO.setEnrolleeNames("Harry,Gilbert;Joe,Stefhen");
		paymentDTO.setPaymentUrl("http://selecthealth.org/ffmpaymenttest/Medical.aspx");
		paymentDTO.setHiosIssuerId("26002");
		return paymentDTO;
	}
}

