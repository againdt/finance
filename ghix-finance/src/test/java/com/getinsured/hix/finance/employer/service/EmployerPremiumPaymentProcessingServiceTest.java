package com.getinsured.hix.finance.employer.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.getinsured.hix.dto.enrollment.EnrollmentCurrentMonthDTO;
import com.getinsured.hix.dto.finance.FinanceResponse;
import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.cybersource.utils.CyberSourceKeyException;
import com.getinsured.hix.finance.employer.service.jpa.EmployerPaymentProcessingServiceImpl;
import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.finance.paymentmethod.service.PaymentMethodService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;


@TransactionConfiguration(defaultRollback = false)
public class EmployerPremiumPaymentProcessingServiceTest extends BaseTest{

	@Autowired	private EmployerPaymentProcessingService employerPremiumPaymentProcessingService;
	@Autowired private PaymentMethodService paymentMethodService;
	@Autowired private EmployerPaymentProcessingServiceImpl employerPremiumPaymentProcessingServiceImpl;
	@Autowired private EmployerInvoiceCreationService employerInvoiceCreationService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployerPremiumPaymentProcessingServiceTest.class);
	@Test
	@Ignore
	public void testauthorizeEmployerCreditCard() {
		LOGGER.info("================== testauthorizeEmployerCreditCard =========================");
        HashMap<String, Object> request =new HashMap<String, Object>(); 
		
		request.put("firstName", "John");
        request.put("lastName", "Doe");
        request.put("street1", "1295 Charleston Road");
        request.put("city", "Mountain View");
        request.put("state", "CA");
        request.put("zip", "94043");
        request.put("country", "US");
        request.put("email", "nobody@cybersource.com");
        request.put("cardNumber", "4111111111111111");
        request.put("cardType", "001");

        //Split the expirationDate
        request.put("expirationMonth", "12");
        request.put("expirationYear", "2020");
        try{		
		employerPremiumPaymentProcessingService.authorizeEmployerCreditCard(request,"11","false");
        }catch(Exception ex)
        {
        	
        }
	}
	
	@Test
	@Ignore
	public void testcaptureEmployerPremium() {
		
		LOGGER.info("================== testcaptureEmployerPremium =========================");
        HashMap<String, Object> request =new HashMap<String, Object>(); 
		
		request.put("firstName", "John");
        request.put("lastName", "Doe");
        request.put("street1", "1295 Charleston Road");
        request.put("city", "Mountain View");
        request.put("state", "CA");
        request.put("zip", "94043");
        request.put("country", "US");
        request.put("email", "nobody@cybersource.com");
        request.put("cardNumber", "4111111111111111");
        request.put("cardType", "001");

        //Split the expirationDate
        request.put("expirationMonth", "12");
        request.put("expirationYear", "2020");
        try{
        employerPremiumPaymentProcessingService.captureEmployerPremiumUsingCreditCard(request,"11","false");
        }catch(Exception ex)
        {
        	
        }
	}
	
	@Test
	@Ignore
	@Rollback(false)
	public void testcreateEmployerInvoice() {
		LOGGER.info("================== testcreateEmployerInvoice =========================");
		int invoiceid = 0;
		try{
			invoiceid = employerInvoiceCreationService.createEmployerInvoice(2);	
		}catch(FinanceException e){
			LOGGER.error("Error", e);
		}
		LOGGER.info("Generated invoice id is: "+invoiceid);
	}
	
	@Test
	@Ignore
	@Rollback(false)
	public void testcreateEmployerBinderInvoice() {
		LOGGER.info("================== testcreateEmployerBinderInvoice =========================");
		int invoiceid = 0;
		/*try{
			//TODO need to change the value for the null parameter
			invoiceid = employerInvoiceCreationService.createEmployerBinderInvoice(151, null, null);	
		}catch(FinanceException e){
			LOGGER.error("Error", e);
		}*/
		LOGGER.info("Generated invoice id is: "+invoiceid);
	}
	
	@Test
	@Rollback(false)
	@Ignore
	public void testPaymentProcessing(){
		LOGGER.info("================== testPaymentProcessing =========================");
		PaymentMethods employerPaymentTypes = null;//paymentMethodService.getDefaultPaymentMethodsByModuleIdAndModuleName(5, PaymentMethods.ModuleName.EMPLOYER);
		Map<String, Object> financeResponseMap = null;
		PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();					
		paymentMethodRequestDTO.setModuleID(FinanceConstants.NUMERICAL_5);
		paymentMethodRequestDTO.setModuleName(PaymentMethods.ModuleName.EMPLOYER);					
		paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentMethods.PaymentIsDefault.Y);
		
		try{
			financeResponseMap = paymentMethodService.searchEmployerPaymentMethod(paymentMethodRequestDTO);
		}catch(GIException giex)
		{
			LOGGER.error("=============== Exception :: Inside makePayment ===============", giex);
		}
		if(financeResponseMap != null && !financeResponseMap.isEmpty() && financeResponseMap.containsKey(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY))
		{
			List<PaymentMethods> paymentMethodList = (List<PaymentMethods>) financeResponseMap.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY);
			if(paymentMethodList != null && !paymentMethodList.isEmpty() && paymentMethodList.size() == FinanceConstants.NUMERICAL_1)
				
			{
				employerPaymentTypes = paymentMethodList.get(FinanceConstants.NUMERICAL_0);
			}
		}
		FinanceResponse res = null;
		try{
			res = employerPremiumPaymentProcessingService.processPayment(6,420,employerPaymentTypes);
			int paymentId = res.getPaymentId();
			LOGGER.info("Payment invoice id is: "+paymentId);
		}catch(CyberSourceKeyException exception){
			LOGGER.error("error occurred while making a payment");
		} catch (FinanceException e) {
			LOGGER.error("error occurred while making a payment");
		}
	}
	
	@Test
	@Ignore
	public void testcaptureEmployerEcheck() {
		LOGGER.info("================== testcaptureEmployerEcheck =========================");
		
        HashMap<String, Object> request =new HashMap<String, Object>(); 
		
        request.put("firstName", "John");
        request.put("lastName", "Doe");
        request.put("street1", "1295 Charleston Road");
        request.put("city", "Mountain View");
        request.put("state", "CA");
        request.put("zip", "94043");
        request.put("country", "US");
        request.put("email", "nobody@cybersource.com");
        request.put("contactnumber", "234234234");
        
        request.put("accountNumber", "4100");
        request.put("accountType", "c");
        request.put("routingNumber", "071923284");
       try{
        employerPremiumPaymentProcessingService.captureEmployerEcheck(request,"11", "false");
       }
       catch(Exception ex)
       {
    	   
       }
	}
	
	/*@Test
	@Ignore
	public void testgetBinderEmployerInvoiceData() {
		LOGGER.info("================== testauthorizeEmployerCreditCard =========================");
		 List<EnrollmentCurrentMonthDTO> enrollmentCurrentMonthDTOList = this.getRequestObj();
		 *//**
		  * add periodCovered value as per requirement
		  *//*
		 String periodCovered = null;
        try{		
        	employerInvoiceCreationService.getBinderEmployerInvoiceData(enrollmentCurrentMonthDTOList, "N",null, periodCovered);
        }catch(Exception ex)
        {
        	ex.printStackTrace();
        }
	}*/
	
	private List<EnrollmentCurrentMonthDTO> getRequestObj()
	{
		EnrollmentCurrentMonthDTO obj = null;
		List<EnrollmentCurrentMonthDTO> listEnrollmentCurrentMonthDTO = new ArrayList<EnrollmentCurrentMonthDTO>();
		Employer e = new Employer();
		
		obj= new EnrollmentCurrentMonthDTO();		
		obj.setIssuer(new Issuer());
		obj.setEmployeeId(202);
		
		obj.setEmployer(e);
		obj.setEmployeeContribution(0F);
		obj.setNoOfPersons(1);
		obj.setInsuranceType("DENTAL");
		obj.setGroupPolicyNumber(null);
		obj.setGrossPremiumAmt(0f);
		obj.setEnrollmentId(105);		
		listEnrollmentCurrentMonthDTO.add(obj);
		
		obj= new EnrollmentCurrentMonthDTO();		
		obj.setIssuer(new Issuer());
		obj.setEmployeeId(202);		
		obj.setEmployer(e);
		obj.setEmployeeContribution(99.73F);
		obj.setNoOfPersons(1);
		obj.setInsuranceType("DENTAL");
		obj.setGroupPolicyNumber(null);
		obj.setGrossPremiumAmt(378.65f);
		obj.setEnrollmentId(141);		
		listEnrollmentCurrentMonthDTO.add(obj);
		
		obj= new EnrollmentCurrentMonthDTO();		
		obj.setIssuer(new Issuer());
		obj.setEmployeeId(202);		
		obj.setEmployer(e);
		obj.setEmployeeContribution(0F);
		obj.setNoOfPersons(1);
		obj.setInsuranceType("DENTAL");
		obj.setGroupPolicyNumber(null);
		obj.setGrossPremiumAmt(0f);
		obj.setEnrollmentId(142);		
		listEnrollmentCurrentMonthDTO.add(obj);
		
		return listEnrollmentCurrentMonthDTO;
	}
	
}
