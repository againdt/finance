package com.getinsured.hix.finance.paymentmethod.service;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.cybersource.utils.PaymentProcessorStrategy;
import com.getinsured.hix.finance.employer.service.EmployerInvoicesService;
import com.getinsured.hix.finance.paymentmethod.repository.IPaymentMethodRepository;
import com.getinsured.hix.finance.paymentmethod.service.PaymentMethodService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.model.BankInfo;
import com.getinsured.hix.model.CreditCardInfo;
import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.PaymentIsDefault;
import com.getinsured.hix.model.PaymentMethods.PaymentStatus;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.payment.util.PaymentUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;

@Test
@TransactionConfiguration
@SuppressWarnings("unchecked")
public class PaymentMethodsServiceTestNG extends BaseTest
{
	private static final Logger LOGGER = LoggerFactory
			.getLogger(PaymentMethodsServiceTestNG.class);
	
	@Autowired PaymentMethodService paymentMethodService;
	@Autowired	private EmployerInvoicesService employerInvoicesService;
	@Autowired IPaymentMethodRepository financePaymentMethodRepository;
	
	@Test(groups="GET_PAYMENT_METHOD")	
	public void testGetPaymentMethodById()
	{
		try{
		List<PaymentMethods> paymentMethodsList = financePaymentMethodRepository.findAll();
		Assert.assertTrue(paymentMethodsList.size()>0,"No record found");
		int paymentid= paymentMethodsList.get(0).getId();
		
		PaymentMethods paymentMethods  = paymentMethodService.getPaymentMethodsDetail(paymentid);
		Assert.assertNotNull(paymentMethods, "paymentMethods is null");
		
		}catch(Exception exception)
		{
			LOGGER.error("TestNG fail for testGetPaymentMethodById");
			Assert.fail("TestNG fail for testGetPaymentMethodById", exception);
		}
		
		LOGGER.info("testGetPaymentMethodById test completed successfully");
	}
	
	@Test(groups="ADD_PAYMENT_METHOD_BANK")
	//@Rollback(false)
	public void testAddPaymentMethodBank()
	{
		
		LOGGER.info("Started Test Method testAddPaymentMethodBank");
		try
		{
			PaymentMethods paymentMethodsObj = testSearchByPaymentMethodName(null, PaymentMethods.PaymentStatus.Active);
			Assert.assertNotNull(paymentMethodsObj, "paymentMethods is null");
			Assert.assertTrue( paymentMethodsObj.getModuleId() != 0);
			
			PaymentMethods paymentMethods = paymentMethodService.savePaymentMethods(populatePaymentMethods(PaymentMethods.PaymentType.BANK, paymentMethodsObj.getModuleId()));
			Assert.assertTrue(paymentMethods != null && paymentMethods.getId() != 0);
			
			PaymentMethods paymentMethodsNew  = paymentMethodService.getPaymentMethodsDetail(paymentMethods.getId());
			Assert.assertNotNull(paymentMethodsNew, "paymentMethods is null");
			Assert.assertEquals(paymentMethodsNew.getId(), paymentMethods.getId());
		
		}catch(GIException gi)
		{
			LOGGER.error("Error in testAddPaymentMethodBank", gi);
			Assert.fail("TestNG fail for testAddPaymentMethodBank", gi);
		}
		
		LOGGER.info("testAddPaymentMethodBank test completed successfully");
	}
	
	@Test(groups="EDIT_PAYMENT_METHOD_BANK")
	//@Rollback(false)
	public void testUpdatePaymentMethodBank()
	{
		LOGGER.info("Started Test Method testUpdatePaymentMethodBank");
		try
		{
			PaymentMethods paymentMethodsNew = testSearchByPaymentMethodName(PaymentMethods.PaymentType.BANK, PaymentMethods.PaymentStatus.Active);
			Assert.assertNotNull(paymentMethodsNew, "paymentMethods is null");			
			
			paymentMethodsNew.setPaymentMethodName(paymentMethodsNew.getPaymentMethodName()+" EDIT");
			paymentMethodsNew.getFinancialInfo().getBankInfo().setRoutingNumber("011001881");
			
			PaymentMethods paymentMethodEdit = paymentMethodService.savePaymentMethods(paymentMethodsNew);
			Assert.assertTrue(paymentMethodEdit != null && paymentMethodEdit.getId() != 0);		
			Assert.assertTrue(paymentMethodEdit.getPaymentMethodName().equals(paymentMethodsNew.getPaymentMethodName()) );
		
		}catch(GIException gi)
		{
			LOGGER.error("Error in testUpdatePaymentMethodBank", gi);
			Assert.fail("TestNG fail for testUpdatePaymentMethodBank", gi);
		}
		
		LOGGER.info("testUpdatePaymentMethodBank test completed successfully");
	}
	
	@Test(groups="ADD_PAYMENT_METHOD_CC")
	//@Rollback(false)
	public void testAddPaymentMethodCC()
	{
		LOGGER.info("Started Test Method testAddPaymentMethodCC");
		try
		{
			PaymentMethods paymentMethodsObj = testSearchByPaymentMethodName(null, PaymentMethods.PaymentStatus.Active);
			Assert.assertNotNull(paymentMethodsObj, "paymentMethods is null");
			Assert.assertTrue( paymentMethodsObj.getModuleId() != 0);
			
			PaymentMethods paymentMethods = paymentMethodService.savePaymentMethods(populatePaymentMethods(PaymentMethods.PaymentType.CREDITCARD, paymentMethodsObj.getModuleId()));
			Assert.assertTrue(paymentMethods != null && paymentMethods.getId() != 0);
		
			PaymentMethods paymentMethodsNew  = paymentMethodService.getPaymentMethodsDetail(paymentMethods.getId());
			Assert.assertNotNull(paymentMethodsNew, "paymentMethods is null");
			Assert.assertEquals(paymentMethodsNew.getId(), paymentMethods.getId());		
		}catch(GIException gi)
		{
			LOGGER.error("Error in testAddPaymentMethodCC", gi);
			Assert.fail("TestNG fail for testAddPaymentMethodCC", gi);
		}
		
		LOGGER.info("testAddPaymentMethodCC test completed successfully");
	}
	
	@Test(groups="EDIT_PAYMENT_METHOD_CC")
	//@Rollback(false)
	public void testUpdatePaymentMethodCC()
	{
		LOGGER.info("Started Test Method testUpdatePaymentMethodCC");
		try
		{
			PaymentMethods paymentMethodsNew = testSearchByPaymentMethodName(PaymentMethods.PaymentType.CREDITCARD, PaymentMethods.PaymentStatus.Active);
			Assert.assertNotNull(paymentMethodsNew, "paymentMethods is null");
			
			
			paymentMethodsNew.setPaymentMethodName(paymentMethodsNew.getPaymentMethodName()+" EDIT");
			paymentMethodsNew.getFinancialInfo().getCreditCardInfo().setNameOnCard("TEST NG CREDIT CARD EDIT");
			
			PaymentMethods paymentMethodEdit = paymentMethodService.savePaymentMethods(paymentMethodsNew);
			Assert.assertTrue(paymentMethodEdit != null && paymentMethodEdit.getId() != 0);		
			Assert.assertTrue(paymentMethodEdit.getPaymentMethodName().equals(paymentMethodsNew.getPaymentMethodName()));
			Assert.assertTrue(paymentMethodEdit.getFinancialInfo().getCreditCardInfo().getNameOnCard().equals("TEST NG CREDIT CARD EDIT"));
			
		}catch(GIException gi)
		{
			LOGGER.error("Error in testUpdatePaymentMethodCC", gi);
			Assert.fail("TestNG fail for testUpdatePaymentMethodCC", gi);
		}
		
		LOGGER.info("testUpdatePaymentMethodCC test completed successfully");
	}
	
	@Test(groups="ADD_PAYMENT_METHOD_MANUAL")
	//@Rollback(false)
	public void testAddPaymentMethodManual()
	{
		LOGGER.info("Started Test Method testAddPaymentMethodManual");
		try
		{
			PaymentMethods paymentMethodsObj = testSearchByPaymentMethodName(null, PaymentMethods.PaymentStatus.Active);
			Assert.assertNotNull(paymentMethodsObj, "paymentMethods is null");
			Assert.assertTrue( paymentMethodsObj.getModuleId() != 0);
			
			PaymentMethods paymentMethods = paymentMethodService.savePaymentMethods(populatePaymentMethods(PaymentMethods.PaymentType.MANUAL, paymentMethodsObj.getModuleId()));
			Assert.assertTrue(paymentMethods != null && paymentMethods.getId() != 0);
			
			PaymentMethods paymentMethodsNew  = paymentMethodService.getPaymentMethodsDetail(paymentMethods.getId());
			Assert.assertNotNull(paymentMethodsNew, "paymentMethods is null");
			Assert.assertEquals(paymentMethodsNew.getId(), paymentMethods.getId());
		
		}catch(GIException gi)
		{
			LOGGER.error("Error in testAddPaymentMethodManual", gi);
			Assert.fail("TestNG fail for testAddPaymentMethodManual", gi);
		}
		
		LOGGER.info("testAddPaymentMethodManual test completed successfully");
	}
	
	@Test(groups="EDIT_PAYMENT_METHOD_MANUAL")
	//@Rollback(false)
	public void testUpdatePaymentMethodManual()
	{
		LOGGER.info("Started Test Method testUpdatePaymentMethodManual");
		try
		{
			PaymentMethods paymentMethodsNew = testSearchByPaymentMethodName(PaymentMethods.PaymentType.MANUAL, PaymentMethods.PaymentStatus.Active);
			Assert.assertNotNull(paymentMethodsNew, "paymentMethods is null");
			
			paymentMethodsNew.setPaymentMethodName(paymentMethodsNew.getPaymentMethodName()+" EDIT");			
			
			PaymentMethods paymentMethodEdit = paymentMethodService.savePaymentMethods(paymentMethodsNew);
			Assert.assertTrue(paymentMethodEdit != null && paymentMethodEdit.getId() != 0);		
			Assert.assertTrue(paymentMethodEdit.getPaymentMethodName().equals(paymentMethodsNew.getPaymentMethodName()));
			
		}catch(GIException gi)
		{
			LOGGER.error("Error in testUpdatePaymentMethodManual", gi);
			Assert.fail("TestNG fail for testUpdatePaymentMethodManual", gi);
		}
		
		LOGGER.info("testUpdatePaymentMethodManual test completed successfully");
	}
	
	@Test(groups="CHANGE_DEFAULT")
	//@Rollback(false)
	public void testChangeDefaultSetting()
	{
		LOGGER.info("Started Test Method testChangeDefaultSetting");
		
		List<PaymentMethods> paymentMethodsList = financePaymentMethodRepository.findAll();
		Assert.assertTrue(!paymentMethodsList.isEmpty(),"No record found");
		int paymentid= paymentMethodsList.get(0).getId();
		int lastUpdatedBy = paymentMethodsList.get(0).getLastUpdatedBy();
		
		PaymentIsDefault isDefault = paymentMethodsList.get(0).getIsDefault();
		
		if(isDefault.equals(PaymentIsDefault.N))
		{
			isDefault = PaymentIsDefault.Y;
		}
		else
		{
			isDefault = PaymentIsDefault.N;
		}
		PaymentMethods paymentMethodNew =  null;
		paymentMethodNew = paymentMethodService.changeDefaultSetting(paymentid, isDefault, lastUpdatedBy);
		
		Assert.assertTrue(paymentMethodNew != null && paymentMethodNew.getIsDefault() == isDefault);
		LOGGER.info("testChangeDefaultSetting test completed successfully");
	}
	
	@Test(groups="ACTIVATE_DEACTIVATE_PAYMENY_METHOD")
	//@Rollback(false)
	public void testUpdateStatus()
	{
		LOGGER.info("Started Test Method testUpdateStatus");		
		
		PaymentMethods paymentMethodsObj = testSearchByPaymentMethodName(null, PaymentMethods.PaymentStatus.Active);
		Assert.assertNotNull(paymentMethodsObj, "paymentMethods is null");
		Assert.assertTrue( paymentMethodsObj.getId() != 0);
		
		
		int paymentid= paymentMethodsObj.getId();
		int lastUpdatedBy = paymentMethodsObj.getLastUpdatedBy();
		
		PaymentStatus status = paymentMethodsObj.getStatus();
		
		if(status.equals(PaymentStatus.Active))
		{
			status= PaymentStatus.InActive;
		}
		else
		{
			status= PaymentStatus.Active;
		}
		
		PaymentMethods paymentMethodsNew =  null;
		paymentMethodsNew = paymentMethodService.updateStatus(paymentid, status, lastUpdatedBy);
		
		Assert.assertTrue(paymentMethodsNew != null && paymentMethodsNew.getStatus() == status);
		
		LOGGER.info("testUpdateStatus test completed successfully");
		
	}
	
	public PaymentMethods testSearchByPaymentMethodName(PaymentMethods.PaymentType paymentType, PaymentMethods.PaymentStatus paymentStatus)
	{
		LOGGER.info("Started Test Method testSearchByPaymentMethodName");		
		
		List<PaymentMethods> paymentMethodsList = null;//paymentMethodService.searchByPaymentMethodName(paymentMethodName);
		PaymentMethods paymentMethods = null;
		Map<String, Object> financeResponseMap = null;
		PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();					
		
		if(paymentType != null)
		{
			paymentMethodRequestDTO.setPaymentType(paymentType);
		}
		
		else if(paymentStatus != null)
		{
			paymentMethodRequestDTO.setPaymentMethodStatus(paymentStatus);
		}
		paymentMethodRequestDTO.setPaginationRequired(true);
		paymentMethodRequestDTO.setStartRecord(0);
		paymentMethodRequestDTO.setPageSize(1);
		try{
			financeResponseMap = paymentMethodService.searchEmployerPaymentMethod(paymentMethodRequestDTO);
		}catch(GIException giex)
		{
			LOGGER.error("=============== Exception :: Inside testSearchByPaymentMethodName ===============", giex);
		}
		if(financeResponseMap != null && !financeResponseMap.isEmpty() && financeResponseMap.containsKey(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY))
		{
			paymentMethodsList = (List<PaymentMethods>) financeResponseMap.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY);
			
		}
		Assert.assertTrue(paymentMethodsList != null && !paymentMethodsList.isEmpty());
		
		paymentMethods = paymentMethodsList.get(0);
		if(paymentType != null)
		{
			Assert.assertTrue(paymentMethods.getPaymentType() == paymentType);
		}
		
		else if(paymentStatus != null)
		{
			Assert.assertTrue(paymentMethods.getStatus() == paymentStatus);
		}					
		
		LOGGER.info("testSearchByPaymentMethodName test completed successfully");
		return paymentMethods;
	}
	
	private PaymentMethods populatePaymentMethods(PaymentMethods.PaymentType paymentType, final int moduleId)
	{
		final String isPaymentGateWay =  DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_PAYMENT_GATEWAY);
		
		PaymentMethods paymentMethods = new PaymentMethods();
		FinancialInfo financialInfo = new FinancialInfo();
		
		paymentMethods.setStatus(PaymentMethods.PaymentStatus.Active);
		paymentMethods.setIsDefault(PaymentMethods.PaymentIsDefault.N);
		paymentMethods.setModuleId(moduleId);
		paymentMethods.setModuleName(PaymentMethods.ModuleName.EMPLOYER);
		
		financialInfo.setFirstName("Employer");
		financialInfo.setLastName("Employer");
		
		financialInfo.setAddress1("Test");
		financialInfo.setCity("Belen");
		financialInfo.setState("NM");
		financialInfo.setZipcode("87002");
		
		financialInfo.setEmail("employer@ghix.com");
		financialInfo.setContactNumber("408-571-9409");
		
		if(paymentType == PaymentMethods.PaymentType.BANK)
		{			
			paymentMethods.setPaymentType(paymentType);			
			paymentMethods.setPaymentMethodName("TEST NG ADD BANK");			
			paymentMethods.setSubscriptionId(null);			
			
			BankInfo bankInfo = new BankInfo();
			bankInfo.setAccountNumber("4100");
			bankInfo.setRoutingNumber("011000015");
			bankInfo.setAccountType("C");	
			
			financialInfo.setBankInfo(bankInfo);
			financialInfo.setCreditCardInfo(null);
			financialInfo.setPaymentType(FinancialInfo.PaymentType.BANK);
			
		}
		else if(paymentType == PaymentMethods.PaymentType.CREDITCARD)
		{
			paymentMethods.setPaymentType(paymentType);			
			paymentMethods.setPaymentMethodName("TEST NG ADD CREDIT CARD");			
			paymentMethods.setSubscriptionId(null);
			
			
			CreditCardInfo creditCardInfo = new CreditCardInfo();
			creditCardInfo.setCardNumber("4111111111111111");
			
			if( (isPaymentGateWay != null && isPaymentGateWay.equalsIgnoreCase(Boolean.FALSE.toString())) 
					|| FinanceConstants.YES.equalsIgnoreCase(PaymentProcessorStrategy.USE_MOCKFOR_PAYMENT_AND_REPORT))
			{
				creditCardInfo.setCardType(PaymentUtil.getCardTypeCode("visa"));
			}
			else{
				creditCardInfo.setCardType("visa");
			}
			creditCardInfo.setExpirationMonth("06");
			creditCardInfo.setExpirationYear("2016");
			creditCardInfo.setNameOnCard("TEST NG CREDITCARD");
			
			financialInfo.setCreditCardInfo(creditCardInfo);
			financialInfo.setPaymentType(FinancialInfo.PaymentType.CREDITCARD);
			
		}
		else if(paymentType == PaymentMethods.PaymentType.MANUAL)
		{
			paymentMethods.setPaymentType(paymentType);			
			paymentMethods.setPaymentMethodName("TEST NG ADD MANUAL");			
			paymentMethods.setSubscriptionId(null);
			
			
			Location location = new Location();
			location.setAddress1("Test");
			location.setCity("Belen");
			location.setCounty("Valencia");
			location.setCountycode("35061");
			location.setZip("87002");
			location.setState("NM");
			location.setLat(0.0);
			location.setLon(0.0);
			financialInfo.setLocation(location);
			financialInfo.setPaymentType(FinancialInfo.PaymentType.MANUAL);
		}
		
		paymentMethods.setFinancialInfo(financialInfo);
		return paymentMethods;
	}
}
