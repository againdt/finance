package com.getinsured.hix.finance.cybersource;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.cybersource.serviceimpl.CustomerProfile;
import com.getinsured.hix.finance.cybersource.serviceimpl.OneTimeCreditCard;
import com.getinsured.hix.finance.cybersource.serviceimpl.OneTimeECheck;
import com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;

public class CreateCustomerProfileRequestTest extends BaseTest
{
	private String subscriptionId = null;
	
	@Autowired private OneTimeCreditCard oneTimeCC;
	
	public String getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(String subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	@Autowired private CustomerProfile customerProfile;
	@Autowired private EmployerPaymentProcessingService employerPremiumPaymentProcessingService;
	
	@Autowired
	OneTimeECheck eccyb;
	
	/*@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	@Ignore
	public void testSubScribeCustomer() 
	{
		try
		{
			HashMap request = new HashMap();
			request.put("firstName", "John");
			request.put("lastName", "Doe");
			request.put("street1", "1295 Charleston Road");
			request.put("city", "Mountain View");
			request.put("state", "CA");
			request.put("zip", "94043");
			request.put("country", "US");
			request.put("email", "nobody@cybersource.com");
			request.put("accountNumber", "4100");
			request.put("accountType", "c");
			request.put("routingNumber", "071923284");

			HashMap reply = customerProfile.SubScribeCustomer(request, "11", "monthly");
			if(reply != null && !reply.isEmpty())
			{
				setSubscriptionId(reply.get("paySubscriptionCreateReply_subscriptionID").toString());
			}
			System.out.println("Map values: " + getMapValue(reply));
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}*/
	
	@SuppressWarnings("rawtypes")
	private String getMapValue(Map map) 
	{

		StringBuffer dest = new StringBuffer();

		if (map != null && !map.isEmpty())
		{
			Iterator iter = map.keySet().iterator();
			String key, val;
			while (iter.hasNext())
			{
				key = (String) iter.next();
				val = (String) map.get(key);
				dest.append(key + "=" + val + ",");
			}
		}
		return (dest.toString());
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	
	public void testDebitAccount()
	{
		try
		{
			Map request = new HashMap();
			request.put("merchantID", "vimo");
			request.put("firstName", "John");
			request.put("lastName", "Doe");
			request.put("street1", "1295 Charleston Road");
			request.put("city", "Mountain View");
			request.put("state", "CA");
			request.put("zip", "94043");
			request.put("country", "US");
			request.put("email", "nobody@cybersource.com");
			request.put("accountNumber", "4100");
			request.put("accountType", "c");
			request.put("routingNumber", "071923284");
			request.put("amount", "100");
			request.put("contactnumber", "9990994948");
			
			Map reply = new HashMap();
			reply = eccyb.debitAccount(request, "11");
			System.out.println("Map values: " + getMapValue(reply));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	@Ignore
	public void updateAccount()
	{
		try
		{
			HashMap updateRequest = new HashMap();
			updateRequest.put("recurringSubscriptionInfo_subscriptionID", "3794727403620176056470");
			updateRequest.put("billTo_phoneNumber", "605-611-2310");
			/*updateRequest.put("check_accountNumber","0011011547512");*/
			/*updateRequest.put("check_secCode","WEB");
			updateRequest.put("check_accountType","S");
			updateRequest.put("check_bankTransitNumber","011000536");*/
			/*updateRequest.put("","");
			updateRequest.put("","");*/
			updateRequest.put("subscription_paymentMethod", "check");
			Map reply = new HashMap();
			reply = customerProfile.updateCustomerProfile(updateRequest);
			
			//reply = recEC.createCustomerProfile(updateRequest);
			System.out.println("Map values: " + getMapValue(reply));
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	@Ignore
	public void testNewDebitApi()
	{
		try
		{
			HashMap payRequest = new HashMap();
			payRequest.put("subscriptionID", "3781161211740176056470");
			Map reply = new HashMap();
			reply = customerProfile.eCheckDebitAccount(payRequest, "10");
			System.out.println("Map values: " + getMapValue(reply));
		}
		catch (Exception ex) 
		{
			ex.printStackTrace();
		}
		
	}
	
	@SuppressWarnings("rawtypes")
	@Test
	@Ignore
	public void testRetrieveInfo()
	{
		Map reply = new HashMap();
		try
		{
			reply = customerProfile.retrieveSubScriptionInfo("3793303828270176056442");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		System.out.println("Map values: " + getMapValue(reply));
	}
	
	//--------- Final testing begins
	
	/**
	 * For Credit Card Customer Profile
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	@Ignore
	public void createCreditCardCustomerProfile()
	{
		HashMap createRequest = new HashMap();
		createRequest.put("FIRSTNAME", "Didier");
		createRequest.put("LASTNAME", "Drogba");
		createRequest.put("ADDR_STREET1", "A-256 High street park");
		createRequest.put("CITY", "Santa Fe");
		createRequest.put("STATE", "CA");
		createRequest.put("POSTALCODE", "90012");
		createRequest.put("COUNTRY", "US");
		createRequest.put("EMAIL", "didier.drogba@chelsea.com");
		createRequest.put("PHONENUMBER", "6056042354");
		createRequest.put("CURRENCY", "USD");
		createRequest.put("TITLE", "Credit Card Payment");
		createRequest.put("PAYMENTMETHOD", "credit card");
		createRequest.put("SUBSCRIPTIONINFO_FREQUENCY", "on-demand");
		
		createRequest.put("CARD_CARDTYPE", "001");
		createRequest.put("CARD_ACCOUNTNUMBER", "4111111111111111");
		createRequest.put("CARD_EXPIRATIONMONTH", "12");
		createRequest.put("CARD_EXPIRATIONYEAR", "2015");
		
		try
		{
			Map reply = customerProfile.createCustomerProfile(createRequest);
			System.out.println("Received Reply : "+getMapValue(reply));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/**
	 * for Echeck Customer profile
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	//@Ignore
	public void createEcheckCustomerProfile()
	{
		HashMap createRequest = new HashMap();
		createRequest.put("FIRSTNAME", "Frank");
		createRequest.put("LASTNAME", "Lampard");
		createRequest.put("ADDR_STREET1", "B-222 times street");
		createRequest.put("CITY", "Edison");
		createRequest.put("STATE", "CA");
		createRequest.put("POSTALCODE", "90037");
		createRequest.put("COUNTRY", "US");
		createRequest.put("EMAIL", "frank.lampard@chelsea.com");
		createRequest.put("PHONENUMBER", "6056052552");
		createRequest.put("CURRENCY", "USD");
		createRequest.put("TITLE", "Bank Payment");
		createRequest.put("PAYMENTMETHOD", "check");
		createRequest.put("SUBSCRIPTIONINFO_FREQUENCY", "on-demand");
		
		createRequest.put("CHECK_ACCOUNTNUMBER","00110157451250");
		createRequest.put("CHECK_SECCODE","WEB");
		createRequest.put("CHECK_ACCOUNTTYPE","S");
		createRequest.put("CHECK_BANKTRANSITNUMBER","011001234");
		try
		{
			Map reply = customerProfile.createCustomerProfile(createRequest);
			System.out.println("Received Reply : "+getMapValue(reply));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	@Ignore
	public void testAuthorizePCIComplainceCard()
	{
		try
		{
			HashMap request = new HashMap();
			request.put("SUBSCRIPTIONID", "3781935150430176056470");
			try{
			Map reply =  oneTimeCC.pciAuthorizeCard(request, "10");
			System.out.println("Received Reply : "+getMapValue(reply));
			}
			catch(Exception ex)
			{
				
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	@Ignore
	public void testPCICreditAccount()
	{
		try
		{
			HashMap request = new HashMap();
			request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, "3781950878290176056442");
			request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, "ISS_INV-2013-1");
			Map reply =  eccyb.pciCreditAccount(request, "15.50");
			System.out.println("Received Reply : "+getMapValue(reply));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	//----------------------------------------------------------------------------------------------------------------------
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	@Ignore
	public void testAuthorizeEmployerCreditCard()
	{
		HashMap request = new HashMap();
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, "3798644094790176056428");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, "EMP_INV-2013-1");
		try
		{
			Map reply =	employerPremiumPaymentProcessingService.authorizeEmployerCreditCard(request, "150", "true");
			System.out.println("Received Reply : "+getMapValue(reply));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	@Ignore
	public void testCaptureEmployerPremiumUsingCreditCard()
	{
		HashMap request = new HashMap();
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, "3798481094370176056428");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, "EMP_INV-2013-1");
		try
		{
			Map reply =	employerPremiumPaymentProcessingService.captureEmployerPremiumUsingCreditCard(request,"150","true");
			System.out.println("Received Reply : "+getMapValue(reply));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	@Ignore
	public void testcaptureEmployerEcheck()
	{
		HashMap request = new HashMap();
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, "3798478193460176056470");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, "EMP_INV-2013-1235");
		try
		{
			Map reply =	employerPremiumPaymentProcessingService.captureEmployerEcheck(request,"90","true");
			System.out.println("Received Reply : "+getMapValue(reply));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	@Ignore
	public void testRefundUsingCreditCard()
	{
		HashMap request = new HashMap();
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, "3798644094790176056428");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, "EMP_INV-2013-1");
		try
		{
			Map reply =	employerPremiumPaymentProcessingService.refundUsingCreditCard(request, "100", "true");
			System.out.println("Received Reply : "+getMapValue(reply));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	@Ignore
	public void testRefundUsingEcheck()
	{
		HashMap request = new HashMap();
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, "3798478193460176056470");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, "EMP_INV-2013-1");
		try
		{
			Map reply =	employerPremiumPaymentProcessingService.refundUsingEcheck(request, "100","true");
			System.out.println("Received Reply : "+getMapValue(reply));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
}
