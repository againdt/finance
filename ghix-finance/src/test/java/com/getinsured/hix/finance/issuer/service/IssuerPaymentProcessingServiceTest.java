package com.getinsured.hix.finance.issuer.service;


import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;

import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.finance.utils.FinancialMgmtGenericUtil;
import com.getinsured.hix.finance.utils.FinancialMgmtUtils;
import com.getinsured.hix.model.IssuerPaymentInvoice;
import com.getinsured.hix.model.IssuerPayments;

public class IssuerPaymentProcessingServiceTest extends BaseTest
{
	@Autowired IssuerPaymentProcessingService issuerPaymentProcessingService;
	@Autowired private IssuerPaymentInvoiceService issuerPaymentInvoiceService;
	@Autowired private IssuerPaymentService issuerPaymentService;
	@Autowired private FinancialMgmtUtils financialMgmtUtils;
	
	/*@Test*/
	//@Ignore
	/*public void testCreateIssuerInvoice()
	{
		try{
			issuerPaymentProcessingService.createIssuerInvoice();
		}catch(FinanceException e){
			fail(e.getMessage());
		}
	}*/
	
	@Test
	@Ignore
	public void testDate()
	{
		DateTime dt = TSDateTime.getInstance();
		DateTime dt1 = TSDateTime.getInstance();
		DateTimeFormatter fmt = DateTimeFormat.forPattern("MM-dd-yyyy");
		dt = dt.plusMonths(1).withDayOfMonth(1);
		dt1 = dt.plusMonths(1).withDayOfMonth(1).minusDays(1);
		String periodCovered = fmt.print(dt) + " to " + fmt.print(dt1);
		System.out.println(periodCovered);
		Date[] date = getRequiredDateRange();
		System.out.println(date[0] + " " + date[1]);
	}
	
	public Date[] getRequiredDateRange()
	{
		//Two months date range is retuned ex 12-01-2012 to 02-28-2013
		DateTime begining = TSDateTime.getInstance();
		DateTime end = TSDateTime.getInstance();
		begining = begining.minusMonths(1).withDayOfMonth(1);
		end = begining.plusMonths(2).withDayOfMonth(1).minusDays(1);
		
		Date[] dates = new Date[2];
		dates[0] = begining.toDate();
		dates[1] = end.toDate();
		return dates;
	}
	
	@Test
	@Ignore
	public void testProcessPayment()
	{
		try{
			issuerPaymentProcessingService.processPayment();
		}catch(FinanceException e){
			fail(e.getMessage());
		}
	}
	
	@Test
	@Ignore
	public void testUpdateIssuerPaymentInvoice()
	{
		IssuerPaymentInvoice isuerPaymentInvoice = issuerPaymentInvoiceService.findIssuerPaymentInvoiceByInvoiceID(5);
		IssuerPayments issuerPayments = issuerPaymentService.findIssuerInvoiceNumber("INV-JAN2013-7");
		issuerPayments.setIsPartialPayment('N');
		issuerPayments.setStatus("PAID");
	//	isuerPaymentInvoice.setAmount(550);
		isuerPaymentInvoice.setAmount(FinancialMgmtGenericUtil.getBigDecimalObj(550f));
		isuerPaymentInvoice.setIssuerPayments(issuerPayments);
		IssuerPaymentInvoice newIssuerPaymentInvoice = issuerPaymentInvoiceService.updateIssuerPaymentInvoice(isuerPaymentInvoice);
		assertTrue(newIssuerPaymentInvoice != null && newIssuerPaymentInvoice.getIssuerPayments().getStatus() != "PAID");
	}
	
	@Test
	@Ignore
	public void testGenerateRemittanceInfo()
	{
		try
		{
			//financialMgmtUtils.getRemittanceDataByEnrollmentId(168);
			issuerPaymentProcessingService.generateRemittanceInfo();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
}
