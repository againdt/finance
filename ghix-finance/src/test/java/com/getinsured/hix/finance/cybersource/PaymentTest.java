package com.getinsured.hix.finance.cybersource;

import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingException;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.cybersource.serviceimpl.OneTimeCreditCard;
import com.getinsured.hix.finance.cybersource.serviceimpl.OneTimeECheck;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;

public class PaymentTest extends BaseTest
{
	@Autowired private OneTimeECheck oneTimeEC;
	@Autowired private OneTimeCreditCard oneTimeCC;
	
	@BeforeClass
	public static void setUpDataSource() throws Exception {
	    try {
	        SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();

	        OracleConnectionPoolDataSource ds = new OracleConnectionPoolDataSource();
	       /* ds.setURL("jdbc:oracle:thin:@166.78.42.8:1521:GHIXDB");
	        ds.setUser("nmdev_123");
	        ds.setPassword("nmdev_123#");*/
	        ds.setURL("jdbc:oracle:thin:@localhost:1521:XE");
	        ds.setUser("GI_STATE_MAIN");
	        ds.setPassword("GI_STATE_MAIN");
	        builder.bind("jdbc/ghixDS", ds);
	        builder.activate();
	    } catch (NamingException ex) {
	        ex.printStackTrace();
	    }
	}
	
	
	@Ignore
	public void testCreditCardPCICredit()
	{
		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;

		request.put(CyberSourceKeyConstants.CCCREDITSERVICE_RUN_KEY, "true");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, "TEST-PAYMENT");
		request.put(CyberSourceKeyConstants.PURCHASETOTALS_GRANDTOTALAMOUNT_KEY, "201.05");
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY,
				"9997000199328648");
		request.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, "CCD");
		try
		{
			Map response  = oneTimeCC.pciCreditToCard(request, "201.05");
			System.out.println(response);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}
	
	@Test
	public void testCreditToCard()
	{
		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;

		request.put("ccCreditService_run", "true");
		request.put("merchantReferenceCode", "MRC-14344");
		request.put("firstName", "Phill");
		request.put("lastName", "Huges");
		request.put("street1", "Winchester");
		request.put("city", "Sunnyvale");
		request.put("state", "California");
		request.put("zip", "94087");
		request.put("country", "US");
		request.put("email", "phil.huges@yopmail.com");
		request.put("purchaseTotals_grandTotalAmount", "150.05");

		request.put("cardNumber", "4111111111111111");
		request.put("cardType", "001");
		request.put("expirationMonth", "12");
		request.put("expirationYear", "2014");
		request.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, "CCD");
		
		try
		{
			Map response  = oneTimeCC.creditToCard(request, "150.05");
			System.out.println(response);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	
	
	@Ignore
	public void eCheckPciCreditAccount()
	{
		Map<String, String> request = new HashMap<String, String>();
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, "cybersource_nmhix");
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, "9997000128141591");
		request.put(CyberSourceKeyConstants.PURCHASETOTALS_GRANDTOTALAMOUNT_KEY, "200");
		request.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, CyberSourceKeyConstants.CHECK_SECCODE_CCD);
		try
		{
			Map response  = oneTimeEC.pciCreditAccount(request, "200");
			System.out.println(response);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	@Ignore
	public void echeckCreditAccount()
	{
		Map<String, String> request = new HashMap<String, String>();

		request.put("ecCreditService_run", "true");
		request.put("merchantReferenceCode", "MRC-14344");
		request.put("city", "Sunnyvale");
		request.put("country", "US");
		request.put("email", "somdev.konddev@yopmail.com");
		request.put("firstName", "Somdev");
		request.put("lastName", "Konddev");
		request.put("zip", "94087");
		request.put("state", "California");
		request.put("street1", "Wincheter high street");
		request.put("purchaseTotals_currency", "USD");
		request.put("purchaseTotals_grandTotalAmount", "105.05");
		request.put("contactnumber", "4085129988");
		request.put("accountNumber", "4100");
		request.put("accountType", "C");
		request.put("routingNumber", "011000536");
		request.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, "CCD");
		
		try
		{
			Map response  = oneTimeEC.creditAccount(request, "105.05");
			System.out.println(response);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

}
