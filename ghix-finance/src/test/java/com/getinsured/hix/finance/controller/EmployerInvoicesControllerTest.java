package com.getinsured.hix.finance.controller;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.finance.FinanceResponse;
import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.platform.util.GhixEndPoints.FinanceServiceEndPoints;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.thoughtworks.xstream.XStream;

/**
 * 
 * @author Sharma_k
 *
 */
public class EmployerInvoicesControllerTest extends BaseTest
{
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployerInvoicesControllerTest.class);
	@Autowired private RestTemplate restTemplate;
	
	@Test
	public void testCancelEmployerInvoice()
	{
		LOGGER.info("=========================== Starting TestCase for CancelEmployerInvoice ==================================");
		FinanceResponse financeResponse = new FinanceResponse();
		Map<String,Object> requestObj = new HashMap<String,Object>();
		requestObj.put(GhixConstants.FIN_CANCEL_INV_EMPLOYER_ID_KEY, 1);
		requestObj.put(GhixConstants.FIN_CANCEL_INV_INVOICE_TYPE_KEY, "BINDER_INVOICE");
		String get_resp = restTemplate.postForObject(FinanceServiceEndPoints.CANCEL_EMPLOYER_INVOICE, requestObj, String.class);
		LOGGER.info("Received Response: "+get_resp);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		financeResponse = (FinanceResponse)xstream.fromXML(get_resp);
		LOGGER.info(financeResponse.getStatus());
		
		LOGGER.info("======================== End TestCase for CancelEmployerInvoice ==========================================");
	}

}
