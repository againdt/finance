package com.getinsured.hix.finance.employer.service;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.EmployerInvoices;

public class EmployerInvoiceLineItemsServiceTest extends BaseTest{

	@Autowired	private EmployerInvoiceLineItemsService employerInvoiceLineItemsService;
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployerInvoiceLineItemsServiceTest.class);
	@Test
	public void testfindEmployerInvoicesById() {
		EmployerInvoices employerInvoices = new EmployerInvoices();
		employerInvoices.setId(2);
		List<EmployerInvoiceLineItems> listLineItems = employerInvoiceLineItemsService.getInvoiceLineItem(employerInvoices);
		LOGGER.info("test "+listLineItems.size());
	}
	
}
