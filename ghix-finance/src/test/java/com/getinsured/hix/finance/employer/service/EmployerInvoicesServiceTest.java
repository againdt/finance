package com.getinsured.hix.finance.employer.service;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.model.EmployerInvoices;

public class EmployerInvoicesServiceTest extends BaseTest{

	@Autowired	private EmployerInvoicesService employerInvoicesService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployerInvoicesServiceTest.class);
	@Test
	public void testGetBinderInvoicePaidStatusOfEmployer() {
		LOGGER.info("================== testGetBinderInvoicePaidStatusOfEmployer =========================");
		EmployerInvoices.PaidStatus status= employerInvoicesService.getBinderInvoicePaidStatusOfEmployer(14);
        System.out.println("status is "+status);
    }
}
