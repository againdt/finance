package com.getinsured.hix.finance.cybersource;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.naming.NamingException;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.cybersource.serviceimpl.CustomerProfile;
import com.getinsured.hix.finance.cybersource.serviceimpl.OneTimeECheck;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;

public class RecurringECheckTest extends BaseTest{

	@Autowired private OneTimeECheck oneTimeEcheck;
	@Autowired private CustomerProfile customerProfile;
	private static String subscription_Id = "9997000175697230";
	
	
	@BeforeClass
	public static void setUpDataSource() throws Exception {
	    try {
	        SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();

	        OracleConnectionPoolDataSource ds = new OracleConnectionPoolDataSource();
	        ds.setURL("jdbc:oracle:thin:@166.78.42.8:1521:GHIXDB");
	        ds.setUser("NMUAT_JUNE2");
	        ds.setPassword("NMUAT_JUNE2#");
	        builder.bind("jdbc/ghixDS", ds);
	        builder.activate();
	    } catch (NamingException ex) {
	        ex.printStackTrace();
	    }
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	//@Ignore
	public void createEcheckCustomerProfile()
	{
		HashMap createRequest = new HashMap();		
		createRequest.put(CyberSourceKeyConstants.PAYSUBSCRIPTIONCREATESERVICE_RUN_KEY,Boolean.TRUE.toString());
		createRequest.put(CyberSourceKeyConstants.MERCHANTID_KEY, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MERCHANT_ID));
		createRequest.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, CyberSourceKeyConstants.MERCHANT_CODE);
		
		createRequest.put(CyberSourceKeyConstants.BILLTO_FIRSTNAME_KEY, "Recurring Weekly");
		createRequest.put(CyberSourceKeyConstants.BILLTO_LASTNAME_KEY, "Start 03NOV14");
		createRequest.put(CyberSourceKeyConstants.BILLTO_STREET1_KEY, "103 Catron St");
		createRequest.put(CyberSourceKeyConstants.BILLTO_CITY_KEY,  "Santa Fe");
		createRequest.put(CyberSourceKeyConstants.BILLTO_STATE_KEY, "NM");
		createRequest.put(CyberSourceKeyConstants.BILLTO_POSTALCODE_KEY, "87501");
		createRequest.put(CyberSourceKeyConstants.BILLTO_COUNTRY_KEY, "USA");
		createRequest.put(CyberSourceKeyConstants.BILLTO_EMAIL_KEY, "shoes.employer@yopmail.com");
		createRequest.put(CyberSourceKeyConstants.BILLTO_PHONENUMBER_KEY, "408-660-9632");
		createRequest.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, "USD");
		createRequest.put(CyberSourceKeyConstants.SUBSCRIPTION_TITLE_KEY, "RECURRING WEEKLY 03NOV14");
		createRequest.put(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY, "check");
		createRequest.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_FREQUENCY_KEY, "weekly");
		
		createRequest.put("recurringSubscriptionInfo_amount", "100");
		createRequest.put("recurringSubscriptionInfo_startDate", "20141103");
		createRequest.put("recurringSubscriptionInfo_numberOfPayments", "2");
		
		createRequest.put(CyberSourceKeyConstants.CHECK_ACCOUNTNUMBER_KEY,"4100");
		createRequest.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, "WEB");
		createRequest.put(CyberSourceKeyConstants.CHECK_ACCOUNTTYPE_KEY, "C");
		createRequest.put(CyberSourceKeyConstants.CHECK_BANKTRANSITNUMBER_KEY, "011000536");
		try
		{
			Map reply = customerProfile.createCustomerProfile(createRequest);
			System.out.println("Received Reply : "+getMapValue(reply));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	@Ignore
	public void cancelRecurringEcheckCustomerProfile()
	{
		Map cancelRequest = new HashMap();		
		cancelRequest.put(CyberSourceKeyConstants.PAYSUBSCRIPTIONUPDATESERVICE_RUN_KEY,Boolean.TRUE.toString());
		cancelRequest.put(CyberSourceKeyConstants.MERCHANTID_KEY, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MERCHANT_ID));
		cancelRequest.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, CyberSourceKeyConstants.MERCHANT_CODE);		
		//cancelRequest.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, "9997000175692843");
		cancelRequest.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, "9997000175697230");
		try
		{
			//Map map = customerProfile.retrieveEcheckSubscription(cancelRequest, "100");
			Map reply = customerProfile.pciCancelSubscription(subscription_Id);
			System.out.println("Received Reply : "+getMapValue(reply));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	@Ignore
	public void refundRecurringEcheckCustomerProfile()
	{
		HashMap refundRequest = new HashMap();		
		refundRequest.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, subscription_Id);		
		refundRequest.put(CyberSourceKeyConstants.ECCREDITSERVICE_RUN_KEY, "true");
		refundRequest.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, CyberSourceKeyConstants.MERCHANT_CODE);
		//refundRequest.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, "9997000175697230");
		//refundRequest.put(CyberSourceKeyConstants.PURCHASETOTALS_GRANDTOTALAMOUNT_KEY, 100);
		refundRequest.put(CyberSourceKeyConstants.CYBERSOURCE_ECCREDIT_SERVICE_DEBIT_REQUESTID,"4135423027830176195999");
		try
		{
			//Map map = customerProfile.retrieveEcheckSubscription(cancelRequest, "100");
			Map reply = oneTimeEcheck.pciCreditAccount(refundRequest, "100");
			System.out.println("Received Reply : "+getMapValue(reply));
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	@Ignore
	public void retrieveRecurringEcheckCustomerProfile()
	{
		HashMap retrieveRequest = new HashMap();		
		retrieveRequest.put(CyberSourceKeyConstants.PAYSUBSCRIPTIONCREATESERVICE_RUN_KEY,Boolean.TRUE.toString());
		retrieveRequest.put(CyberSourceKeyConstants.MERCHANTID_KEY, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MERCHANT_ID));
		retrieveRequest.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, CyberSourceKeyConstants.MERCHANT_CODE);		
		//retrieveRequest.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, "9997000175692843");
		retrieveRequest.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, "9997000175697230");
		
		try
		{
			/*Map map = customerProfile.retrieveRecurringSubscription(retrieveRequest, "100");
			System.out.println("Received Reply : "+getMapValue(map));*/
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("rawtypes")
	private String getMapValue(Map map) 
	{

		StringBuffer dest = new StringBuffer();

		if (map != null && !map.isEmpty())
		{
			Iterator iter = map.keySet().iterator();
			String key, val;
			while (iter.hasNext())
			{
				key = (String) iter.next();
				val = (String) map.get(key);
				dest.append(key + "=" + val + ",");
			}
		}
		return (dest.toString());
	}
}
