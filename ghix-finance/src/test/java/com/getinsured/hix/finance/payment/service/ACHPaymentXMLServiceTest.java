package com.getinsured.hix.finance.payment.service;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;


import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.payment.dto.PaymentInfo;
import com.getinsured.hix.finance.payment.dto.PaymentInfo.IndividualInformation;
import com.getinsured.hix.finance.payment.dto.StandardClassCodeType;
import com.getinsured.hix.finance.utils.FinanceConstants;
/**
 * 
 * @author Sharma_k
 *
 */
public class ACHPaymentXMLServiceTest extends BaseTest
{
	@Autowired private ACHPaymentXMLService achPaymentXMLService;
	private static final Logger LOGGER = LoggerFactory.getLogger(ACHPaymentXMLServiceTest.class);
	
	@Test
	public void saveACHPaymentXMLFileTest()
	{
		PaymentInfo paymentInfo = new PaymentInfo();
		paymentInfo.setCompanyId("ACH-SVB-TEST");
		paymentInfo.setCompanyName("GETINSURED");
		paymentInfo.setEffectiveEntryDate("140508");
		paymentInfo.setEntryDescription("TEST PAYMENT");
		paymentInfo.setServiceClassCode("225");
		paymentInfo.setStandardClassCode(StandardClassCodeType.CCD);
		
		
		List<PaymentInfo.IndividualInformation> individualInformationList = new ArrayList<PaymentInfo.IndividualInformation>();
		
		PaymentInfo.IndividualInformation individualInformation = new PaymentInfo.IndividualInformation();
		individualInformation.setAccountNumber("001101574512");
		individualInformation.setAmount(new BigDecimal("2220.05"));
		individualInformation.setMerchantReferenceNumber("MRC-144044");
		individualInformation.setName("ROBERT MALTAS");
		individualInformation.setReceivingBankABA("001102356");
		individualInformation.setTransactionCode(new BigInteger("32"));
		individualInformationList.add(individualInformation);
		
		paymentInfo.setIndividualInformation(individualInformationList);
		String fileLocation = null;
		try
		{
		 fileLocation = achPaymentXMLService.saveACHPaymentXMLFile(paymentInfo);
		}
		catch(Exception ex)
		{
			LOGGER.error("Exception caught.."+ex);
		}
		File file = new File(fileLocation);
		assertTrue("PaymentInfo File generated successfully...", file.exists());
	}
	
	@Test
	public void generatePaymentInfoObject()
	{
		/*Map<String, Object> objectData = new HashMap<String, Object>();
		objectData*/
		DateFormat dateFormatter = new SimpleDateFormat("yyMMdd");
		Map<String, Object> mapData = new HashMap<String, Object>();
		mapData.put(FinanceConstants.ACHPaymentConstants.COMPANY_ID, "TEST-ACH-ACCOUNT");
		mapData.put(FinanceConstants.ACHPaymentConstants.COMPANY_NAME, "GetInsured");
		mapData.put(FinanceConstants.ACHPaymentConstants.EFFECTIVE_ENTRY_DATE, dateFormatter.format(new java.util.Date()));
		mapData.put(FinanceConstants.ACHPaymentConstants.ENTRY_DESCRIPTION, "Carrier Credit Test Payment");
		mapData.put(FinanceConstants.ACHPaymentConstants.SERVICE_CLASS_CODE, "220");
		mapData.put(FinanceConstants.ACHPaymentConstants.STANDARD_CLASS_CODE, StandardClassCodeType.CCD);
		
		IndividualInformation indivInformation = new IndividualInformation();
		indivInformation.setAccountNumber("859545556");
		indivInformation.setAmount(new BigDecimal(2360.53).setScale(2, BigDecimal.ROUND_HALF_UP));
		indivInformation.setMerchantReferenceNumber("ISS-INV-APR-2014_25_20142904");
		indivInformation.setName("Robert Maltas");
		indivInformation.setReceivingBankABA("110011102");
		indivInformation.setTransactionCode(new BigInteger("22"));
		
		IndividualInformation indivInformation1 = new IndividualInformation();
		indivInformation1.setAccountNumber("4525200122");
		indivInformation1.setAmount(new BigDecimal(1000.05).setScale(2, BigDecimal.ROUND_HALF_UP));
		indivInformation1.setMerchantReferenceNumber("ISS-INV-APR-2014_26_20142904");
		indivInformation1.setName("John Corner");
		indivInformation1.setReceivingBankABA("000011110");
		indivInformation1.setTransactionCode(new BigInteger("22"));
		List <IndividualInformation> individualInformationList = new ArrayList<IndividualInformation>();
		individualInformationList.add(indivInformation);
		individualInformationList.add(indivInformation1);
		mapData.put(FinanceConstants.ACHPaymentConstants.INDIVIDUAL_INFO, individualInformationList);
		
		PaymentInfo paymentInfoObject = achPaymentXMLService.generatePaymentInfoObject(mapData);
		
		assertNotNull("PaymentInfo Object is null or empty", paymentInfoObject);
	}
}
