package com.getinsured.hix.finance.issuer.service;


import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.GregorianCalendar;
import java.util.List;

import com.getinsured.hix.finance.BaseTest;

import org.junit.Test;
import org.junit.Ignore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.getinsured.hix.finance.issuer.repository.IIssuerRemittanceRepository;
import com.getinsured.hix.model.EmployerInvoices.PaidStatus;
import com.getinsured.hix.model.IssuerRemittance;
import com.getinsured.hix.model.IssuerRemittance.IsActive;

import org.springframework.beans.factory.annotation.Autowired;

public class IssuerInvoiceServiceTest extends BaseTest
{
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerInvoiceServiceTest.class);
	@Autowired IIssuerRemittanceRepository iIssuerRemittanceRepository;
	@Autowired IssuerRemittanceService issuerRemittanceService;
	
	@Test
	@Ignore
	public void testFindIssuerInvoicesById()
	{
		LOGGER.info("testFindIssuerInvoicesById started");
		
		List<IssuerRemittance> issuerRemittancelist = iIssuerRemittanceRepository.findAll();
		assertTrue("No record found",issuerRemittancelist.size()>0);
		IssuerRemittance issuerRemittance = null;
		int invoiceId = issuerRemittancelist.get(0).getId();
		issuerRemittance = issuerRemittanceService.findIssuerRemittanceById(invoiceId);
		assertTrue(issuerRemittance != null && issuerRemittance.getId() == invoiceId);
		LOGGER.info("testFindIssuerInvoicesById ends");
	}
	
	@Test
	@Ignore
	public void testUnpaidIssuerInvoices()
	{
		LOGGER.info("testUnpaidIssuerInvoices started");
		Date[] dates = getDateRange();
		//List<IssuerInvoices> issuerInvoicesList = issuerInvoicesService.getUnpaidIssuerInvoices(new java.sql.Date(dates[0].getTime()), new java.sql.Date(dates[1].getTime()));
		List<IssuerRemittance> issuerInvoicesList = issuerRemittanceService.getUnpaidIssuerRemittance();
		assertTrue(issuerInvoicesList != null && (issuerInvoicesList.get(0).getPaidStatus().equals(PaidStatus.DUE) || issuerInvoicesList.get(0).getPaidStatus().equals(PaidStatus.PARTIALLY_PAID)));
		LOGGER.info("testUnpaidIssuerInvoices ends");
	}
	
	
	public Date[] getDateRange() {

		Date begining, end;

		{
			Calendar calendar = getCalendarForNow();
			calendar.set(Calendar.DAY_OF_MONTH,
					calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
			setTimeToBeginningOfDay(calendar);
			begining = calendar.getTime();
		}

		{
			Calendar calendar = getCalendarForNow();
			calendar.set(Calendar.DAY_OF_MONTH,
					calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			setTimeToEndofDay(calendar);
			end = calendar.getTime();
		}

		Date[] dates = new Date[2];
		dates[0] = begining;
		dates[1] = end;

		return dates;
	}
	
	private static Calendar getCalendarForNow() {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(new TSDate());
		return calendar;
	}

	private static void setTimeToBeginningOfDay(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	}

	private static void setTimeToEndofDay(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
	}
	
	@Test
	public void findIssuerInvoicesByIssuerIdTest()
	{
		LOGGER.info("findIssuerInvoicesByIssuerIdTest started");
		List<IssuerRemittance> issuerRemittancelist = iIssuerRemittanceRepository.findAll();
		assertTrue("No record found",issuerRemittancelist.size()>0);
		IssuerRemittance issuerRemittance = null;
		issuerRemittance = issuerRemittancelist.get(0);
		IssuerRemittance newIssuerInvoices = issuerRemittanceService.findIssuerRemittanceByIssuerId(issuerRemittance.getIssuer().getId());
		assertTrue(issuerRemittance != null && issuerRemittance.getIssuer().getId() == newIssuerInvoices.getIssuer().getId());
		
		LOGGER.info("findIssuerInvoicesByIssuerIdTest ends");
	}
	
	@Test
	public void updateIssuerInvoicesActiveStatusTest()
	{
		LOGGER.info("updateIssuerInvoicesActiveStatusTest started");
		List<IssuerRemittance> issuerRemittancelist = iIssuerRemittanceRepository.findAll();
		assertTrue("No record found",issuerRemittancelist.size()>0);
		
		for(IssuerRemittance issuerRemittance : issuerRemittancelist)
		{
			if(issuerRemittance.getIsActive().equals(IsActive.Y) )
			{
				IssuerRemittance newIssuerInvoices = issuerRemittanceService.updateIssuerRemittanceActiveStatus(issuerRemittance.getId(), IsActive.N);
				assertTrue(newIssuerInvoices != null && !newIssuerInvoices.getIsActive().equals(IsActive.Y));
			}
		}
		LOGGER.info("updateIssuerInvoicesActiveStatusTest ends");
	}
}
