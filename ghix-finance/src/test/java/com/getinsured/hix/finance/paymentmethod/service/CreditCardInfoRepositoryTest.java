package com.getinsured.hix.finance.paymentmethod.service;

import static org.junit.Assert.assertNotNull;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.paymentmethod.repository.ICreditCardInfoRepository;
import com.getinsured.hix.model.CreditCardInfo;

public class CreditCardInfoRepositoryTest extends BaseTest {

	@Autowired
	ICreditCardInfoRepository iCreditCardInfoRepository; 
	
	@Test
	public void testFindById() {
		CreditCardInfo cci1 = iCreditCardInfoRepository.findOne(3);
	//	System.out.println(cci1.toString());
		
		assertNotNull(cci1);
	}

	@Test
	public void testFindAll() {
		List <CreditCardInfo> cciList = iCreditCardInfoRepository.findAll();
	//	System.out.println(cciList);
		assertNotNull(cciList);
	}

	@Test
	public void testSave() {
		CreditCardInfo cci = new CreditCardInfo();
		cci.setCardNumber("4111111111111111");
		cci.setCardType("visa");
		cci.setCreated(new TSDate());
		cci.setExpirationMonth("12");
		cci.setExpirationYear("2020");
		cci.setUpdated(new TSDate());
		iCreditCardInfoRepository.save(cci);
	}
}
