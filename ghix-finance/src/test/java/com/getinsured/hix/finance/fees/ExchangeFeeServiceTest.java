package com.getinsured.hix.finance.fees;

import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.fees.repository.IExchangeFeeRepository;
import com.getinsured.hix.finance.fees.service.ExchangeFeeService;
import com.getinsured.hix.finance.issuer.repository.IIssuerRemittanceRepository;
import com.getinsured.hix.finance.issuer.service.IssuerRemittanceService;
import com.getinsured.hix.finance.utils.FinancialMgmtGenericUtil;
import com.getinsured.hix.model.ExchangeFee;
import com.getinsured.hix.model.IssuerRemittance;

public class ExchangeFeeServiceTest extends BaseTest{
	
	@Autowired IExchangeFeeRepository exchangeFeeRepository;
	@Autowired ExchangeFeeService exchangeFeeService;
	@Autowired IIssuerRemittanceRepository issuerInvoicesRepository;
	@Autowired IssuerRemittanceService issuerInvoicesService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExchangeFeeServiceTest.class);

	
	@Test
	@Ignore
	public void testCreateExchangeFee()
	{
		ExchangeFee exchangeFee = exchangeFeeService.saveExchangeFee(createExchangeFee());
		LOGGER.info(""+exchangeFee);
		assertTrue("No record found", exchangeFee !=null);
	}
	
	@Test
	public void testFindExchangeFeeByIssuerInvoiceID()
	{
		ExchangeFee exchangeFee = exchangeFeeService.findExchangeFeeByIssuerRemittance(2);
		LOGGER.info(""+exchangeFee);
		//assertTrue("No record found", exchangeFee !=null);
	}
	
	@Test
	@Ignore
	public void testUpdateExchangeFee()
	{
//		ExchangeFee exchangeFee = exchangeFeeService.updateExchangeFee(2, 100);
		ExchangeFee exchangeFee = exchangeFeeService.updateExchangeFee(2);
		LOGGER.info(""+exchangeFee);
		assertTrue("No record found", exchangeFee !=null);
	}
	
	private ExchangeFee createExchangeFee(){
		
		List<IssuerRemittance> issuerInvoicelist = issuerInvoicesRepository.findAll();
		IssuerRemittance issuerRemittance = null;
		int invoiceId = issuerInvoicelist.get(0).getId();
		issuerRemittance = issuerInvoicesService.findIssuerRemittanceById(invoiceId);
		
		ExchangeFee exchangeFee = new ExchangeFee();
		exchangeFee.setIssuer(issuerRemittance.getIssuer());
		exchangeFee.setIssuerRemittance(issuerRemittance);
	/*	exchangeFee.setPaymentReceived(0);
		exchangeFee.setTotalFeeBalanceDue(500);
		exchangeFee.setTotalPayment(0);
		exchangeFee.setTotalRefund(0);*/
		exchangeFee.setPaymentReceived(FinancialMgmtGenericUtil.getDefaultBigDecimal());
		exchangeFee.setTotalFeeBalanceDue(FinancialMgmtGenericUtil.getBigDecimalObj(500f));
		exchangeFee.setTotalPayment(FinancialMgmtGenericUtil.getDefaultBigDecimal());
		exchangeFee.setTotalRefund(FinancialMgmtGenericUtil.getDefaultBigDecimal());
		exchangeFee.setStatus(ExchangeFee.PaidStatus.DUE);
		exchangeFee.setCreatedDate(new TSDate());
		exchangeFee.setAutomaticProcess("");
		
		Date[] dates = getDateRange();
		
		exchangeFee.setStartingDate(dates[0]);
		exchangeFee.setEndingDate(dates[1]);
		
		return exchangeFee;
	}
	
	public Date[] getDateRange() {
		
	    Date begining, end;

	    {
	        Calendar calendar = getCalendarForNow();
	        calendar.set(Calendar.DAY_OF_MONTH,
	                calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
	        setTimeToBeginningOfDay(calendar);
	        begining = calendar.getTime();
	    }

	    {
	        Calendar calendar = getCalendarForNow();
	        calendar.set(Calendar.DAY_OF_MONTH,
	                calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
	        setTimeToEndofDay(calendar);
	        end = calendar.getTime();
	    }
	    
	    Date[] dates = new Date[2];
	    dates[0] = begining;
	    dates[1] = end;

	    return dates;
	}

	private static Calendar getCalendarForNow() {
	    Calendar calendar = GregorianCalendar.getInstance();
	    calendar.setTime(new TSDate());
	    return calendar;
	}

	private static void setTimeToBeginningOfDay(Calendar calendar) {
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	}

	private static void setTimeToEndofDay(Calendar calendar) {
	    calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
	    calendar.set(Calendar.MILLISECOND, 999);
	}
}
