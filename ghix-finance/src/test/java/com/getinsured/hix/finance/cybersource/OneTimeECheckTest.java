package com.getinsured.hix.finance.cybersource;

import java.util.HashMap;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.cybersource.serviceimpl.OneTimeECheck;

public class OneTimeECheckTest extends BaseTest {

	@Autowired
	@Qualifier("OneTimeECheck")
	OneTimeECheck eccyb;
	
	
	@Test
	public void testDebitAccount() {
		HashMap request =new HashMap(); 
		request.put("merchantID", "vimo");
		request.put("firstName", "John");
        request.put("lastName", "Doe");
        request.put("street1", "1295 Charleston Road");
        request.put("city", "Mountain View");
        request.put("state", "CA");
        request.put("zip", "94043");
        request.put("country", "US");
        request.put("email", "nobody@cybersource.com");
        request.put("accountNumber","4100");
        request.put("accountType","c");
        request.put("routingNumber","071923284");
        request.put("amount", "100");
        
        eccyb.debitAccount(request,"11");
	}

	@Test
	public void testCreditAccount() {
		HashMap request =new HashMap(); 
		request.put("firstName", "John");
        request.put("lastName", "Doe");
        request.put("street1", "1295 Charleston Road");
        request.put("city", "Mountain View");
        request.put("state", "CA");
        request.put("zip", "94043");
        request.put("country", "US");
        request.put("email", "nobody@cybersource.com");
        request.put("accountNumber","4100");
        request.put("accountType","c");
        request.put("routingNumber","071923284");
        request.put("amount", "11.11");
        
        eccyb.creditAccount(request,"11");
	}

}
