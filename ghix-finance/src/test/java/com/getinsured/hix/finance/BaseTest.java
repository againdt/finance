package com.getinsured.hix.finance;

import org.junit.BeforeClass;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.util.Log4jConfigurer;

import com.getinsured.hix.platform.util.exception.GIRuntimeException;


/**
 * 
 * @author Sharma_k
 * @since 20th June 2013
 * BaseTest added for finance module
 * 
 * @version1.1 effective from 24th March 2015
 * Adding TestNG Framework to BaseTest
 */

/*@RunWith(SpringJUnit4ClassRunner.class)*/
/*@ContextConfiguration(locations = "classpath:/applicationContext.xml")*/
@ContextConfiguration(
        locations = {"classpath:applicationContext-externalDB-tests.xml"})
public abstract class BaseTest extends AbstractTransactionalTestNGSpringContextTests
{
	/*public static ApplicationContext context = null;*/

	@BeforeClass
	public static void setUp()
	{		
		try
		{
			/*context = new ClassPathXmlApplicationContext("/applicationContext.xml");*/
			String rootPath = System.getenv("GHIX_HOME");
			Log4jConfigurer.initLogging(rootPath + "/ghix-setup/conf/ghix-log4j.xml");
		}
		catch(Exception ex)
		{	
			throw new GIRuntimeException(ex);
		}
	}
}