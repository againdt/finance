package com.getinsured.hix.finance.issuer.service;

import static org.junit.Assert.assertTrue;
import java.util.List;
import com.getinsured.hix.finance.BaseTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.getinsured.hix.model.IssuerPaymentInvoice;

import com.getinsured.hix.finance.issuer.repository.IIssuerPaymentInvoiceRepository;

public class IssuerPaymentInvoiceServiceTest extends BaseTest
{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerPaymentInvoiceServiceTest.class);
	@Autowired IIssuerPaymentInvoiceRepository issuerPaymentInvoiceRepository;
	@Autowired IssuerPaymentInvoiceService issuerPaymentInvoiceService;
	
	
	@Test
	public void testfindIssuerPaymentInvoiceByInvoiceID()
	{
		LOGGER.info("testFindIssuerPaymentInvoiceByInvoiceID started");
		List<IssuerPaymentInvoice> issuerPaymentInvoiceList = issuerPaymentInvoiceRepository.findAll();
		assertTrue("No record found",issuerPaymentInvoiceList.size()>0);
		IssuerPaymentInvoice issuerPaymentInvoice = null;
		int invoiceId = issuerPaymentInvoiceList.get(0).getIssuerRemittance().getId();
		issuerPaymentInvoice = issuerPaymentInvoiceService.findIssuerPaymentInvoiceByInvoiceID(invoiceId);
		assertTrue(issuerPaymentInvoice != null && issuerPaymentInvoice.getIssuerRemittance().getId() == invoiceId);
		LOGGER.info("testFindIssuerPaymentInvoiceByInvoiceID ends");
	}
}
