package com.getinsured.hix.finance.paymentmethod.service;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.paymentmethod.repository.IBankInfoRepository;
import com.getinsured.hix.model.BankInfo;

public class BankInfoRepositoryTest extends BaseTest {

	@Autowired
	IBankInfoRepository ibankInfoRepository;
	
	@Test
	@Ignore
	public void testFindById() {
		BankInfo bi1 = ibankInfoRepository.findOne(4);
		System.out.println("=============================================================================================");
		System.out.println(bi1.toString());
	}

	@Test
	public void testSave() {
		BankInfo bi =new BankInfo();
		bi.setAccountNumber("111111111111");
		bi.setAccountType("c");
		bi.setBankName("Deutsch");
		bi.setCreated(new TSDate());
		bi.setRoutingNumber("4545454545");
		bi.setUpdated(new TSDate());
		ibankInfoRepository.save(bi);
	}
	
	@Test
	public void testFindAll() {
		List <BankInfo> bi2 = ibankInfoRepository.findAll();
		System.out.println(bi2);
	}
}
