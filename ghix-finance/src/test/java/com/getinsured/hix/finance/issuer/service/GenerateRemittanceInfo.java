package com.getinsured.hix.finance.issuer.service;

/*import java.sql.Date;*/
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.dto.finance.Finance;
import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.model.IssuerRemittanceLineItem;
import com.getinsured.hix.model.IssuerPaymentDetail;
//import com.getinsured.hix.util.GhixConfiguration;

public class GenerateRemittanceInfo extends BaseTest
{
	Finance finance;
	@Autowired private IssuerPaymentDetailService issuerPaymentDetailService;
	@Autowired private IssuerRemittanceLineItemService issuerRemittanceLineItemService;
	@Autowired private IssuerPaymentProcessingService issuerPaymentProcessingService;
	
	@Test
	public void testRemittanceService()
	{
		try
		{
			issuerPaymentProcessingService.generateRemittanceInfo();
		}
		catch(Exception ex)
		{
			//ex.printStackTrace();
		}
	}
	
	@Ignore
	public void generateXml()
	{
		try{
		finance = new Finance();
		Finance.Payment payment = new Finance.Payment();
		
		
		Date[] dates = getDateRange();
		System.out.println("Date Range is "+dates[0]+"    "+dates[1]);
		
		List<IssuerPaymentDetail>issuerPaymentList =  issuerPaymentDetailService.getCurrentMonthPayments(dates[0], dates[1]);
		if(issuerPaymentList == null || issuerPaymentList.size() == 0)
		{
			System.out.println("******************************* Empty List *******************************");
		}
		
		for(IssuerPaymentDetail issuerPaymentDetail: issuerPaymentList)
		{
			
			//payment.setReceiverBankAcctNum(issuerPaymentDetail.getPaymentMethods().getFinancialInfo().getBankInfo().getAccountNumber());
			//payment.setABAtransitRoutingNumDFI(issuerPaymentDetail.getPaymentMethods().getFinancialInfo().getBankInfo().getRoutingNumber());
			payment.setPayeeOrganizationName(issuerPaymentDetail.getIssuerPayment().getIssuerPaymentInvoice().getIssuerRemittance().getIssuer().getCompanyLegalName());
			payment.setPayeeFederalTIN(issuerPaymentDetail.getIssuerPayment().getIssuerPaymentInvoice().getIssuerRemittance().getIssuer().getFederalEin());
			//payment.setExchgAssignedEmployerGrpId(GhixConfiguration.STATE_CODE);
			
			List<IssuerRemittanceLineItem> issuerRemittanceLineItemsList = issuerRemittanceLineItemService.getIssuerRemittanceLineItems(issuerPaymentDetail.getIssuerPayment().getIssuerPaymentInvoice().getIssuerRemittance());
			Finance.RemittanceInfo remittanceInfo = null;
			Finance.RemittanceInfo.RemittanceDetail remittanceDetail = null;
			List<Finance.RemittanceInfo> remittanceInfoList = new ArrayList<Finance.RemittanceInfo>();
			for(IssuerRemittanceLineItem issuerInvoicesLineItems: issuerRemittanceLineItemsList)
			{
				remittanceInfo = new Finance.RemittanceInfo();
				remittanceDetail = new Finance.RemittanceInfo.RemittanceDetail();
				/*remittanceInfo.setIndivFirstName(issuerInvoicesLineItems.getEmployee().getUser().getFirstName());
				remittanceInfo.setIndivLastName(issuerInvoicesLineItems.getEmployee().getUser().getLastName());
*/				//remittanceInfo.setIndivExchgAssignedPolicyNum(issuerInvoicesLineItems.getPolicyNumber());
				
	//			remittanceDetail.setPymtAmt(Float.toString(issuerInvoicesLineItems.getAmtPaidToIssuer()));
				remittanceDetail.setPymtAmt(issuerInvoicesLineItems.getAmtPaidToIssuer().stripTrailingZeros().toPlainString());
				remittanceInfo.setRemittanceDetail(remittanceDetail);
				remittanceInfoList.add(remittanceInfo);
			}
			payment.setRemittanceInfo(remittanceInfoList);
			//payment.setPayerName(GhixConfiguration.STATE_NAME);
			//payment.setPayerOriginatingCompanyNum(GhixConfiguration.STATE_NAME);
			
			finance.setPayment(payment);
			/*DateFormat df = new SimpleDateFormat("DD-MMM-YYYY");
			Date date = new TSDate();*/
			//Date date = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss").parse(new java.util.Date().toString());
			DateFormat df_time = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss"); 
			finance.setTxnCreateDateTime(df_time.format(new java.util.Date()));
			finance.setHIOSIssuerID(issuerPaymentDetail.getIssuerPayment().getIssuerPaymentInvoice().getIssuerRemittance().getIssuer().getHiosIssuerId());
			try
			{
				DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
				
				JAXBContext newcontext = JAXBContext.newInstance(Finance.class);
				Marshaller marshaller = newcontext.createMarshaller();
				marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
				//marshaller.marshal(finance, System.out);
				String jaxb_fileName = "remittance_jaxb_"+df.format(new java.util.Date())+issuerPaymentDetail.getId()+".xml";
				marshaller.marshal(finance,new StreamResult(new File("D:\\"+jaxb_fileName)));
				
				
				InputStream strXSLPath = new FileInputStream("D://RemittanceInfo.xsl");
				//GenerateRemittanceInfo.class.getClassLoader().getResourceAsStream("D://RemittanceInfo.xsl");
				/*if(strXSLPath.available() == 0)
				{
					System.out.println("******************************* Empty******************");
				}*/
				Source xslSource = new StreamSource(strXSLPath);
				TransformerFactory factory = null;
				factory = TransformerFactory.newInstance();
				
				Templates template = factory.newTemplates(xslSource);
				Transformer transformer = template.newTransformer();
				//String strXML = null;
				
				
				JAXBContext context = JAXBContext.newInstance(Finance.class, Finance.class, ArrayList.class);
				Marshaller m = context.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);

				StringWriter writerEnroll = new StringWriter();
				StreamResult resultEnroll = new StreamResult(writerEnroll);
				m.marshal(finance, resultEnroll);
				String strEnrollTrans = new String(writerEnroll.toString());
				StreamSource xmlJAXB = new StreamSource(new StringReader(
						strEnrollTrans));
				
				/*StringWriter writerXSLT = new StringWriter();
				StreamResult resultXSLT = new StreamResult(writerXSLT);*/
				String fileName = "remittance_"+df.format(new java.util.Date())+issuerPaymentDetail.getId()+".xml";
				transformer.transform(xmlJAXB,  new StreamResult(new File("D:\\"+fileName)));

				/*strXML = new String(writerXSLT.toString());
				
				System.out.println("#####################################"+strXML);*/
				/*StreamSource xmlWithEmpty = new StreamSource(new StringReader(
						strXML));
				*/
				/*Templates templateEmpty = factory.newTemplates(xslEmptyTagSource);
				Transformer transformerEmpty = templateEmpty.newTransformer();
				transformerEmpty.transform(xmlWithEmpty, new StreamResult(new File(
						outputPath)));
				*/
				//System.out.println("Received data is : "+finance);
				
				//System.out.println("Received data is : "+marshaller.toString());
				
			}
			catch(TransformerConfigurationException tce)
			{
				System.out.println(tce.getMessage());
			}
			catch(TransformerException te)
			{
				System.out.println(te.getMessage());
			}
			catch(JAXBException ex)
			{
				System.out.println(ex.getMessage());
			}
			
			//System.out.println("#########################################"+issuerPaymentDetail.getResponse());
		}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	public Date[] getDateRange() 
	{
		Date begining, end;
		{
			Calendar calendar = getCalendarForNow();
			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
			begining = calendar.getTime();
		}

		{
			Calendar calendar = getCalendarForNow();
			calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			end = calendar.getTime();
		}

		Date[] dates = new Date[2];
		dates[0] = begining;
		dates[1] = end;

		return dates;
	}

	private static Calendar getCalendarForNow()
	{
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(new TSDate());
		return calendar;
	}

}
