package com.getinsured.hix.finance.employer.service;

import java.util.List;

import javax.naming.NamingException;

import oracle.jdbc.pool.OracleConnectionPoolDataSource;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;

public class EmployerPremiumInvoiceProcessingServiceTest extends BaseTest
{
	@Autowired private EmployerInvoicePdfGenerationService employerPremiumInvoiceProcessingService;
	@Autowired private EmployerInvoicesService employerInvoicesService;
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployerPremiumInvoiceProcessingServiceTest.class);

	@BeforeClass
	public static void setUpDataSource() throws Exception {
	    try {
	        SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();

	        OracleConnectionPoolDataSource ds = new OracleConnectionPoolDataSource();
	        ds.setURL("jdbc:oracle:thin:@oracle3db.ghixqa.com:1521:ghixdb");
	        ds.setUser("NM1DEV");
	        ds.setPassword("NWRlNzI4YjU4YjMz");
	        builder.bind("jdbc/ghixDS", ds);
	        builder.activate();
	    } catch (NamingException ex) {
	        ex.printStackTrace();
	    }
	}
	
	@Test
	public void testcreatePdf()
	{
		LOGGER.info("==================== testcreatePdf Started==================");
		try
		{
			List<EmployerInvoices> listInvoice = employerInvoicesService.findEmployerInvoicesWhereInvoiceNotCreated();
			if(listInvoice!=null)
			{
				for(EmployerInvoices invoice : listInvoice){
					employerPremiumInvoiceProcessingService.createPdfByInvoiceId(invoice.getId());
				}
			}
			else
			{
				LOGGER.info("No Invoice available for InvoicePdf generation");
			}
		}
		catch(NoticeServiceException ex)
		{
			LOGGER.error("Exception Caught"+ ex.toString());
			//ex.printStackTrace();
		} catch (Exception ex) {
			LOGGER.error("Exception Caught"+ ex.toString());
		}
	}

	/*@Test
	public void testcreatePdf()
	{
		LOGGER.info("==================== testcreatePdf Started==================");
		try
		{
			List<EmployerInvoices> listInvoice = employerInvoicesService.findEmployerInvoicesWhereInvoiceNotCreated();
			if(listInvoice!=null)
			{
				for(EmployerInvoices invoice : listInvoice){
					employerPremiumInvoiceProcessingService.createPdfByInvoiceId(invoice.getId());
				}
			}
			else
			{
				LOGGER.info("No Invoice available for InvoicePdf generation");
			}
		}
		catch(GIException ex)
		{
			LOGGER.error("Exception Caught"+ ex.toString());
			ex.printStackTrace();
		}
	}
*/
   
}
