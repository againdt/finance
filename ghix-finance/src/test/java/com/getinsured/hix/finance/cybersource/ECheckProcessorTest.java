package com.getinsured.hix.finance.cybersource;

import static org.junit.Assert.fail;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.cybersource.serviceimpl.ECheckProcessor;
import com.getinsured.hix.finance.cybersource.utils.FinancialInfoMapper;
import com.getinsured.hix.model.BankInfo;
import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;

public class ECheckProcessorTest extends BaseTest {

	@Autowired
	ECheckProcessor ecp;
	
	
	
	
	@Test
	public void testDebit() {
		BankInfo bi = new BankInfo();
		bi.setId(1);
		bi.setAccountNumber("4100");
		bi.setAccountType("c");
		bi.setRoutingNumber("071923284");
		bi.setBankName("AnyBank");
		
		FinancialInfo finInfo = new FinancialInfo();
		
		finInfo.setBankInfo(bi);
		finInfo.setCreditCardInfo(null);
		finInfo.setAddress1("1295 Charleston Rd.");
		finInfo.setCity("Mountain View");
		finInfo.setCountry("US");
		finInfo.setEmail("jdoe@example.com");
		finInfo.setFirstName("John");
		finInfo.setLastName("Doe");
		finInfo.setPaymentType(FinancialInfo.PaymentType.BANK);
		finInfo.setState("CA");
		finInfo.setZipcode("94043");
		
		FinancialInfoMapper fiMapper = new FinancialInfoMapper();
		fiMapper.setPayRequest(finInfo, CyberSourceKeyConstants.MERCHANT_CODE);
		System.out.println("ECHECK DEBIT START==========================================================");
		ecp.debit(fiMapper.getPayRequest(), "11");
		System.out.println("ECHECK DEBIT END==========================================================");
	}

	@Test
	public void testCredit() {
		BankInfo bi = new BankInfo();
		bi.setId(1);
		bi.setAccountNumber("4100");
		bi.setAccountType("c");
		bi.setRoutingNumber("071923284");
		bi.setBankName("AnyBank");
		
		FinancialInfo finInfo = new FinancialInfo();
		
		finInfo.setBankInfo(bi);
		finInfo.setCreditCardInfo(null);
		finInfo.setAddress1("1295 Charleston Rd.");
		finInfo.setCity("Mountain View");
		finInfo.setCountry("US");
		finInfo.setEmail("jdoe@example.com");
		finInfo.setFirstName("John");
		finInfo.setLastName("Doe");
		finInfo.setPaymentType(FinancialInfo.PaymentType.BANK);
		finInfo.setState("CA");
		finInfo.setZipcode("94043");
		
		FinancialInfoMapper fiMapper = new FinancialInfoMapper();
		fiMapper.setPayRequest(finInfo, CyberSourceKeyConstants.MERCHANT_CODE);
		System.out.println("ECHECK CREDIT START==========================================================");
		ecp.credit(fiMapper.getPayRequest(), "11");
		System.out.println("ECHECK CREDIT END==========================================================");
	}

	@Test
	@Ignore
	public void testAuthorize() {
		fail("Not yet implemented"); // TODO
	}

	/*@Test
	public void testRecurringPayment() {
		BankInfo bi = new BankInfo();
		bi.setId(1);
		bi.setAccountNumber("4100");
		bi.setAccountType("c");
		bi.setRoutingNumber("071923284");
		bi.setCreated(new Date());
		bi.setUpdated(null);
		bi.setBankName("AnyBank");
		
		FinancialInfo finInfo = new FinancialInfo();
		
		finInfo.setBankInfo(bi);
		finInfo.setCreditCardInfo(null);
		finInfo.setAddress1("1295 Charleston Rd.");
		finInfo.setCity("Mountain View");
		finInfo.setCountry("US");
		finInfo.setEmail("jdoe@example.com");
		finInfo.setFirstName("John");
		finInfo.setLastName("Doe");
		finInfo.setPaymentType(FinancialInfo.PaymentType.BANK);
		finInfo.setState("CA");
		finInfo.setZipcode("94043");
		
		FinancialInfoMapper fiMapper = new FinancialInfoMapper();
		fiMapper.setPayRequest(finInfo);
		System.out.println("RECURRING PAYMENT START==========================================================");
		ecp.recurringPayment(fiMapper.getPayRequest(), "11","monthly");
		System.out.println("RECURRING PAYMENT END==========================================================");
	}*/
}
