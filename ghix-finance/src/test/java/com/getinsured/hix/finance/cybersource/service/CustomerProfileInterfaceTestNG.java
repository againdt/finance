package com.getinsured.hix.finance.cybersource.service;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @since 27th March 2015
 * @author Sharma_k
 * @version 1.0
 * @jira ID: HIX-63236
 * 
 * Test Case class to test PCI Compliance Customer Profile Creation. 
 *
 */
@SuppressWarnings("rawtypes")
@TransactionConfiguration
@Transactional
public class CustomerProfileInterfaceTestNG extends BaseTest
{
	private Map requestDataMap = null;
	
	public CustomerProfileInterfaceTestNG(Map requestData)
	{
		this.requestDataMap = requestData;
	}
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerProfileInterfaceTestNG.class);
	@Autowired private CustomerProfileInterface customerProfileInterface;
	
	
	/**
	 * 
	 * @throws GIException
	 */
	@Test(enabled=true, groups = { "CyberSourceTest"})
	public void createCustomerProfileTest()throws GIException
	{
		LOGGER.info("=============== Started Executing createCustomerProfile(-) test case ============");
		
		LOGGER.info("Request for createCustomerProfileTest(): "+requestDataMap);
		
		/*
		 * Calling service to verify test results.	
		 */
		Map receivedResponse  = customerProfileInterface.createCustomerProfile(requestDataMap);
		
		LOGGER.info("Received response from createCustomerProfileTest()"+receivedResponse);
		
		/*
		 * Verification of Received Response
		 */
		Assert.assertNotNull(receivedResponse);
		Assert.assertEquals((String)receivedResponse.get(FinanceConstants.CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE), Integer.toString(FinanceConstants.NUMERICAL_100));
		Assert.assertEquals((String)receivedResponse.get(FinanceConstants.CyberSourceResponseMapKeys.CYBERSOURCE_DECISION), FinanceConstants.CYBERSOURCE_ACCEPT);
		LOGGER.info("=============== createCustomerProfileTest() test ENDS ============");
	}
}