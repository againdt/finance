package com.getinsured.hix.finance.cybersource;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.finance.BaseTest;
import com.getinsured.hix.finance.cybersource.serviceimpl.CreditCardProcessor;
import com.getinsured.hix.finance.cybersource.utils.FinancialInfoMapper;
import com.getinsured.hix.model.CreditCardInfo;
import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;

public class CreditCardProcessorTest extends BaseTest{

	 @Autowired
	 CreditCardProcessor ccp;
	
	/*@Before
	public void TestALL(){
		FinancialInfo finInfo;
		CCInfo ci;
		FinancialInfoMapper fiMapper;
		HashMap mapperInfo;
		ci = new CCInfo();
		ci.setId(1);
		ci.setCardNumber("");
		ci.setCardType("001");
		ci.setExpirationMonth("12");
		ci.setCreated(new Date());
		ci.setUpdated(null);
		ci.setExpirationMonth("12");
		
		finInfo = new FinancialInfo();
		
		finInfo.setCcInfo(ci);
		finInfo.setBankInfo(null);
		finInfo.setAddress("1295 Charleston Rd.");
		finInfo.setCity("Mountain View");
		finInfo.setCountry("US");
		finInfo.setEmail("jdoe@example.com");
		finInfo.setFirstName("John");
		finInfo.setLastName("Doe");
		finInfo.setPaymentType(FinancialInfo.PaymentType.CREDITCARD);
		finInfo.setState("CA");
		finInfo.setZipcode("94043");
		
		fiMapper = new FinancialInfoMapper();
		mapperInfo = fiMapper.mapFinancialInfo(finInfo);
	}
	
	@After
	public void TestAfter()
	{
		System.out.println("ENDENDENDENDENDENDENDEND");
	}*/
	
	@Test
	
	public void testDebit() {
		
		CreditCardInfo ci = new CreditCardInfo();
		ci.setId(1);
		ci.setCardNumber("4111111111111111");
		ci.setCardType("visa");
		ci.setExpirationMonth("12");
		ci.setExpirationYear("12");
		
		FinancialInfo finInfo = new FinancialInfo();
		
		finInfo.setCreditCardInfo(ci);
		finInfo.setBankInfo(null);
		finInfo.setAddress1("1295 Charleston Rd.");
		finInfo.setCity("Mountain View");
		finInfo.setCountry("US");
		finInfo.setEmail("jdoe@example.com");
		finInfo.setFirstName("John");
		finInfo.setLastName("Doe");
		finInfo.setPaymentType(FinancialInfo.PaymentType.CREDITCARD);
		finInfo.setState("CA");
		finInfo.setZipcode("94043");
		
		FinancialInfoMapper fiMapper = new FinancialInfoMapper();
		fiMapper.setPayRequest(finInfo, CyberSourceKeyConstants.MERCHANT_CODE);
		System.out.println("CREDIT CARD DEBIT START==================================================================");
		ccp.debit(fiMapper.getPayRequest(), "11");
		System.out.println("CREDIT CARD DEBIT END====================================================================");
	}

	@Test
	
	public void testCredit() {
	
		CreditCardInfo ci = new CreditCardInfo();
		ci.setId(1);
		ci.setCardNumber("4111111111111111");
		ci.setCardType("visa");
		ci.setExpirationMonth("12");
		ci.setExpirationYear("2020");
		
		FinancialInfo finInfo = new FinancialInfo();
		
		finInfo.setCreditCardInfo(ci);
		finInfo.setBankInfo(null);
		finInfo.setAddress1("1295 Charleston Rd.");
		finInfo.setCity("Mountain View");
		finInfo.setCountry("US");
		finInfo.setEmail("jdoe@example.com");
		finInfo.setFirstName("John");
		finInfo.setLastName("Doe");
		finInfo.setPaymentType(FinancialInfo.PaymentType.CREDITCARD);
		finInfo.setState("CA");
		finInfo.setZipcode("94043");
		
		FinancialInfoMapper fiMapper = new FinancialInfoMapper();
		fiMapper.setPayRequest(finInfo, CyberSourceKeyConstants.MERCHANT_CODE);
		System.out.println("CREDIT CARD CREDIT START=================================================================");
		ccp.credit(fiMapper.getPayRequest(), "11");
		System.out.println("CREDIT CARD CREDIT END===================================================================");
	}

	@Test
	public void testAuthorize() {
		
		CreditCardInfo ci = new CreditCardInfo();
		ci.setId(1);
		ci.setCardNumber("4111111111111111");
		ci.setCardType("visa");
		ci.setExpirationMonth("12");
		ci.setExpirationYear("2020");
		
		FinancialInfo finInfo = new FinancialInfo();
		
		finInfo.setCreditCardInfo(ci);
		finInfo.setBankInfo(null);
		finInfo.setAddress1("1295 Charleston Rd.");
		finInfo.setCity("Mountain View");
		finInfo.setCountry("US");
		finInfo.setEmail("jdoe@example.com");
		finInfo.setFirstName("John");
		finInfo.setLastName("Doe");
		finInfo.setPaymentType(FinancialInfo.PaymentType.CREDITCARD);
		finInfo.setState("CA");
		finInfo.setZipcode("94043");
		
		FinancialInfoMapper fiMapper = new FinancialInfoMapper();
		fiMapper.setPayRequest(finInfo, CyberSourceKeyConstants.MERCHANT_CODE);
		System.out.println("CREDIT CARD AUTHENTICATION START==========================================================");
		ccp.authorize(fiMapper.getPayRequest(), "11");
		System.out.println("CREDIT CARD AUTHENTICATION START==========================================================");
	}

	/*@Test
	public void testRecurringPayment() {
		
		CreditCardInfo ci = new CreditCardInfo();
		ci.setId(1);
		ci.setCardNumber("4111111111111111");
		ci.setCardType("visa");
		ci.setExpirationMonth("12");
		ci.setExpirationYear("2020");
		
		FinancialInfo finInfo = new FinancialInfo();
		
		finInfo.setCreditCardInfo(ci);
		finInfo.setAddress1("1295 Charleston Rd.");
		finInfo.setCity("Mountain View");
		finInfo.setCountry("US");
		finInfo.setEmail("jdoe@example.com");
		finInfo.setFirstName("John");
		finInfo.setLastName("Doe");
		finInfo.setPaymentType(FinancialInfo.PaymentType.CREDITCARD);
		finInfo.setState("CA");
		finInfo.setZipcode("94043");
		
		FinancialInfoMapper fiMapper = new FinancialInfoMapper();
		fiMapper.setPayRequest(finInfo);
		System.out.println("CREDIT CARD RECURRING START===============================================================");
		ccp.recurringPayment(fiMapper.getPayRequest(), "11","monthly");
		System.out.println("CREDIT CARD RECURRING END=================================================================");
	}*/

}
