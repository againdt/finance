package com.getinsured.hix.webservice.finance.endpoints;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.getinsured.hix.finance.issuer.service.IssuerRemittanceReportService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.finance.issuerRemittance.IssuerRemittanceRequest;
import com.getinsured.hix.webservice.finance.issuerRemittance.IssuerRemittanceResponse;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;

/**
 * 
 * @author Sharma_k
 * @since 29th July 2012
 * Endpoint for IND42, Receiving Issuer data for Remittance report.
 */
@Endpoint
public class IssuerRemittanceEndpoints 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerRemittanceEndpoints.class);
	private static final String TARGET_NAMESPACE = "http://webservice.hix.getinsured.com/finance/issuerRemittance";
	@Autowired private IssuerRemittanceReportService issuerRemittanceReportService;
	
	/**
	 * 
	 * @param issuerRemittanceRequest
	 * @return
	 */
	@PayloadRoot(localPart = "issuerRemittanceRequest", namespace = TARGET_NAMESPACE)
	@ResponsePayload 
	public IssuerRemittanceResponse saveIssuerRemittanceDetail(@RequestPayload IssuerRemittanceRequest issuerRemittanceRequest)
	{
		long startTime = TSCalendar.getInstance().getTimeInMillis();
		LOGGER.info("IND-42 ISSUER REMITTANCE ENDPOINTS INVOKED ...");
		IssuerRemittanceResponse issuerRemittanceResponse = new IssuerRemittanceResponse();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		
		try
		{
			LOGGER.info("============ Start of IssuerRemittanceRequest ==========");
			LOGGER.info("Request: "+xstream.toXML(issuerRemittanceRequest));
			LOGGER.info("============ End of IssuerRemittanceRequest ==========");
			//validateRequest(issuerRemittanceRequest);
			issuerRemittanceResponse = issuerRemittanceReportService.processIssuerRemittanceRequest(issuerRemittanceRequest);
		}
		catch(GIException ex)
		{
			LOGGER.error("IND-42: GIException occured while processing request");
			LOGGER.error("GIException IND42: " +ex);
			issuerRemittanceResponse.setResponseCode(""+ex.getErrorCode());
			issuerRemittanceResponse.setResponseDescription(ex.getErrorMsg());
			return issuerRemittanceResponse;
		}
		catch(Exception ex)
		{
			LOGGER.error("IND42: Exception caught while processing IssuerRemittanceResponse "+ex);
			LOGGER.error("Exception IND42: "+ex);
			issuerRemittanceResponse.setResponseCode(Integer.toString(FinanceConstants.ERROR_CODE_209));
			issuerRemittanceResponse.setResponseDescription("Error occured while processing remittance request, Error cause :"+ex.getCause());
			return issuerRemittanceResponse;
		}
		long endTime = TSCalendar.getInstance().getTimeInMillis();
		LOGGER.debug("IND42: Time taken to complete the main process "+(endTime-startTime));
		return issuerRemittanceResponse;
	}
}
