package com.getinsured.hix.finance.paymentmethod.repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.CreditCardInfo;

public interface ICreditCardInfoRepository extends JpaRepository<CreditCardInfo, Integer> {

}
