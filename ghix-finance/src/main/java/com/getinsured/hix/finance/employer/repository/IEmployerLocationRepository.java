package com.getinsured.hix.finance.employer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.EmployerLocation;

/**
 */
public interface IEmployerLocationRepository extends JpaRepository<EmployerLocation, Integer> {
	/**
	 * Method findByEmployer.
	 * @param employer Employer
	 * @return List<EmployerLocation>
	 */
	List<EmployerLocation> findByEmployer(Employer employer);

}
