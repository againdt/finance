package com.getinsured.hix.finance.bank.service.jpa;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.finance.bank.repository.IBankHolidayRepository;
import com.getinsured.hix.finance.bank.service.BankHolidayService;

/**
 * 
 * @author Sharma_k
 *
 */
@Service("bankHolidayService")
public class BankHolidayServiceImpl implements BankHolidayService
{
	@Autowired private IBankHolidayRepository bankHolidayRepository;

	@Override
	public long findNoOfBankHolidaysByYear(String year) 
	{
		return bankHolidayRepository.findNoOfBankHolidaysByYear(year);
	}

	@Override
	public List<Date> findBankHolidaysBetween(Date startDate, Date endDate)
	{
		return bankHolidayRepository.findBankHolidaysBetween(startDate, endDate);
	}
	
}
