package com.getinsured.hix.finance.employer.service.jpa;

import java.io.File;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.DataFormatException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.dto.finance.FinanceResponse;
import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.finance.cybersource.utils.CyberSourceKeyException;
import com.getinsured.hix.finance.cybersource.utils.FinancialInfoMapper;
import com.getinsured.hix.finance.cybersource.utils.PaymentProcessorStrategy;
import com.getinsured.hix.finance.employer.repository.IEmployerFinanceRepository;
import com.getinsured.hix.finance.employer.repository.IEmployerLocationRepository;
import com.getinsured.hix.finance.employer.repository.IEmployerPaymentRepository;
import com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService;
import com.getinsured.hix.finance.employer.service.EmployerInvoicesService;
import com.getinsured.hix.finance.employer.service.EmployerPaymentInvoiceService;
import com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService;
import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.finance.payment.service.PaymentEventLogService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceConstants.CyberSourceResponseMapKeys;
import com.getinsured.hix.finance.utils.FinanceGeneralUtility;
import com.getinsured.hix.finance.utils.FinancialMgmtGenericUtil;
import com.getinsured.hix.finance.utils.FinancialMgmtUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.BankInfo;
import com.getinsured.hix.model.CreditCardInfo;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.EmployerEnrollment;
import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.EmployerInvoiceLineItems.IsActiveEnrollment;
import com.getinsured.hix.model.EmployerInvoiceLineItems.Status;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.EmployerInvoices.InvoiceType;
import com.getinsured.hix.model.EmployerInvoices.PaidStatus;
import com.getinsured.hix.model.EmployerLocation;
import com.getinsured.hix.model.EmployerPaymentInvoice;
import com.getinsured.hix.model.EmployerPaymentInvoice.IsActive;
import com.getinsured.hix.model.EmployerPayments;
import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.PaymentEventLog;
import com.getinsured.hix.model.PaymentEventLog.PaymentStatus;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.PaymentType;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.config.SHOPConfiguration;
import com.getinsured.hix.platform.payment.util.CyberSrcResponseCodes;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;

/**
 * This service implementation class is for GHIX-FINANCE module for processing employer payments.
 * 
 * @author sharma_k
 * @since 28-Jun-2013
 */
@Service("employerPaymentProcessingService")
public class EmployerPaymentProcessingServiceImpl implements EmployerPaymentProcessingService{

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployerPaymentProcessingServiceImpl.class);

	@Autowired private EmployerInvoiceLineItemsService employerInvoiceLineItemsService;
	@Autowired private EmployerInvoicesService employerInvoicesService;
	@Autowired private UserService userService;
	@Autowired private EmployerPaymentInvoiceService employerPaymentInvoiceService;
	@Autowired private FinancialMgmtUtils financialMgmtUtils;
	@Autowired private IEmployerLocationRepository employerLocationRepository;
	@Autowired private IEmployerFinanceRepository employerRepository;
	@Autowired private PaymentEventLogService paymentEventLogService;
	@Autowired private IEmployerPaymentRepository iEmployerPaymentRepository;

	/*private static final int INVOICE_GLCODE= 1100;*/
	private static final int PAYMENT_GLCODE= 1000;

	private static final String DECISION_YES =  "YES";
	private static final String DECISION_NO =  "NO";
	private static final String PAYMENT_TYPE_REJECT= "reject";
	private static final String PAYMENT_TYPE_EXCESS= "excess";
	private static final String PAYMENT_TYPE_FULL= "fullpayment";
	private static final String PAYMENT_TYPE_PARTIAL= "partial";

	private static final int DIVISIBLE_100 = 100;
	private static final long CONFIRM_NUMBER = 100000;
	private static final String COUNTRY = "USA";


	/**
	 * Method authorizeEmployerCreditCard.
	 * @param pCust Map
	 * @param amount String
	 * @param isPaymentGateway String
	 * @return HashMap
	 * @throws GIException
	 * @see com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService#authorizeEmployerCreditCard(Map, String, String)
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	@Override	
	public Map authorizeEmployerCreditCard(Map pCust, String amount, String isPaymentGateway)throws GIException
	{
		LOGGER.info(" authorizeEmployerCreditCard(-,-,-,-) method started with amount:"+amount+FinanceConstants.IS_PAYMENT_GATEWAY_KEY+isPaymentGateway);
		if(isPaymentGateway != null && isPaymentGateway.equalsIgnoreCase(FinanceConstants.TRUE))
		{
			LOGGER.info(" authorizeEmployerCreditCard(-,-,-,-) ends successfully with pciAuthorize =");
			return PaymentProcessorStrategy.creditCardPaymentProcessor.pciAuthorize(pCust, amount);
		}
		else
		{
			LOGGER.info(" authorizeEmployerCreditCard(-,-,-,-) ends successfully with Authorize =");
			return PaymentProcessorStrategy.creditCardPaymentProcessor.authorize(pCust, amount);
		}
	}

	/**
	 * Method captureEmployerPremiumUsingCreditCard.
	 * @param pCust Map
	 * @param amount String
	 * @param isPaymentGateway String
	 * @return HashMap
	 * @throws GIException
	 * @see com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService#captureEmployerPremiumUsingCreditCard(Map, String, String)
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	@Override	
	public Map captureEmployerPremiumUsingCreditCard(Map pCust, String amount, String isPaymentGateway)throws GIException
	{
		LOGGER.info(" captureEmployerPremiumUsingCreditCard(-,-,-,-) method started with amount:"+amount+FinanceConstants.IS_PAYMENT_GATEWAY_KEY+isPaymentGateway);
		if(isPaymentGateway != null && isPaymentGateway.equalsIgnoreCase(FinanceConstants.TRUE))
		{
			LOGGER.info(" captureEmployerPremiumUsingCreditCard(-,-,-,-) ends successfully with pciDebit =");
			return PaymentProcessorStrategy.creditCardPaymentProcessor.pciDebit(pCust, amount);
		}
		else
		{
			LOGGER.info(" captureEmployerPremiumUsingCreditCard(-,-,-,-) ends successfully with Debit =");
			return PaymentProcessorStrategy.creditCardPaymentProcessor.debit(pCust, amount);
		}	
	}

	/**
	 * Method authorizeEmployerEcheck.
	 * @param pCust Map
	 * @param amount String
	 * @param isPaymentGateway String
	 * @return HashMap
	 * @throws GIException
	 * @see com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService#authorizeEmployerEcheck(Map, String, String)
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	@Override
	public Map authorizeEmployerEcheck(Map pCust, String amount, String isPaymentGateway)throws GIException
	{
		LOGGER.info(" authorizeEmployerEcheck(-,-,-,-) method started with amount:"+amount+FinanceConstants.IS_PAYMENT_GATEWAY_KEY+isPaymentGateway);
		if(isPaymentGateway != null && isPaymentGateway.equalsIgnoreCase(FinanceConstants.TRUE))
		{
			LOGGER.info(" authorizeEmployerEcheck(-,-,-,-) ends successfully with pciAuthorize =");
			return PaymentProcessorStrategy.echeckPaymentProcessor.pciAuthorize(pCust, amount);
		}
		else
		{
			LOGGER.info(" authorizeEmployerEcheck(-,-,-,-) ends successfully with Authorize =");
			return PaymentProcessorStrategy.echeckPaymentProcessor.authorize(pCust, amount);
		}
	}

	/**
	 * Method captureEmployerEcheck.
	 * @param pCust Map
	 * @param amount String
	 * @param isPaymentGateway String
	 * @return HashMap
	 * @throws GIException
	 * @see com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService#captureEmployerEcheck(Map, String, String)
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	@Override	
	public Map captureEmployerEcheck(Map pCust, String amount, String isPaymentGateway)throws GIException
	{
		LOGGER.info(" captureEmployerEcheck(-,-,-,-) method started with amount:"+amount+FinanceConstants.IS_PAYMENT_GATEWAY_KEY+isPaymentGateway);
		if(isPaymentGateway != null && isPaymentGateway.equalsIgnoreCase(FinanceConstants.TRUE))
		{
			LOGGER.info(" captureEmployerEcheck(-,-,-,-) ends successfully with pciDebit =");
			return PaymentProcessorStrategy.echeckPaymentProcessor.pciDebit(pCust, amount);
		}
		else
		{
			LOGGER.info(" captureEmployerEcheck(-,-,-,-) ends successfully with Debit =");
			return PaymentProcessorStrategy.echeckPaymentProcessor.debit(pCust, amount);
		}
	}

	/**
	 * Method refundUsingCreditCard.
	 * @param pCust Map
	 * @param amount String
	 * @param isPaymentGateway String
	 * @return HashMap
	 * @throws GIException
	 * @see com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService#refundUsingCreditCard(Map, String, String)
	 */
	@Override
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	public Map refundUsingCreditCard(Map pCust, String amount, String isPaymentGateway)throws GIException
	{
		LOGGER.info(" refundUsingCreditCard(-,-,-,-) method started with amount:"+amount+FinanceConstants.IS_PAYMENT_GATEWAY_KEY+isPaymentGateway);
		if(isPaymentGateway != null && isPaymentGateway.equalsIgnoreCase(FinanceConstants.TRUE))
		{
			LOGGER.info(" refundUsingCreditCard(-,-,-,-) ends successfully with pciCredit =");
			return PaymentProcessorStrategy.creditCardPaymentProcessor.pciCredit(pCust, amount);
		}
		else
		{
			LOGGER.info(" refundUsingCreditCard(-,-,-,-) ends successfully with Credit =");
			return PaymentProcessorStrategy.creditCardPaymentProcessor.credit(pCust, amount);
		}
	}

	/**
	 * Method refundUsingEcheck.
	 * @param pCust Map
	 * @param amount String
	 * @param isPaymentGateway String
	 * @return HashMap
	 * @throws GIException
	 * @see com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService#refundUsingEcheck(Map, String, String)
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	@Override	
	public Map refundUsingEcheck(Map pCust, String amount, String isPaymentGateway)throws GIException
	{
		LOGGER.info(" refundUsingEcheck(-,-,-,-) method started with amount:"+amount+FinanceConstants.IS_PAYMENT_GATEWAY_KEY+isPaymentGateway);
		if(isPaymentGateway != null && isPaymentGateway.equalsIgnoreCase(FinanceConstants.TRUE))
		{
			LOGGER.info(" refundUsingEcheck(-,-,-,-) ends successfully with pciCredit =");
			return PaymentProcessorStrategy.echeckPaymentProcessor.pciCredit(pCust, amount);
		}
		else
		{
			LOGGER.info(" refundUsingEcheck(-,-,-,-) ends successfully with Credit =");
			return PaymentProcessorStrategy.echeckPaymentProcessor.credit(pCust, amount);
		}
	}






	/**
	 * Method processPayment.
	 * @param invoiceid int
	 * @param paidAmt float
	 * @param paymentType PaymentMethods
	 * @return FinanceResponse
	 * @throws CyberSourceKeyException
	 * @throws FinanceException 
	 * @see com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService#processPayment(int, float, PaymentMethods)
	 */
	@Override	
	public FinanceResponse processPayment(int invoiceid,float paidAmt,PaymentMethods paymentType) throws CyberSourceKeyException, FinanceException{

		LOGGER.info(" processPayment(-,-,-) method started with paid amount:"+paidAmt+" received for invoice id: "+invoiceid+" =");
		FinanceResponse financeRes = new FinanceResponse();
		financeRes.setProcessPaymentResposne("");
		String supportPartialPayment =  DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.PARTIAL_PAYMENT_SUPPORTED);
		String minimumPaymentLimit = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MINIMUM_PAYMENT_THRESHOLD);
		BigDecimal paidAmount = FinancialMgmtGenericUtil.getBigDecimalObj(paidAmt);
		//	try {
		EmployerInvoices  employerInvoices = employerInvoicesService.findEmployerInvoicesById(invoiceid);
		if(employerInvoices!=null && (employerInvoices.getPaidStatus()==null || employerInvoices.getPaidStatus()!= PaidStatus.PAID)){

			EmployerInvoices tempInvId = employerInvoicesService.findEmployerInvoicesById(invoiceid);
			if(tempInvId != null && tempInvId.getTotalAmountDue() != null && tempInvId.getTotalAmountDue().compareTo(paidAmount) != 0)
			{
				Exception ex=new GIException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				throw new GIRuntimeException(ex , ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.FINANCE.toString(), null);
			}

			String paymenttype =  "";

			EmployerInvoices previousEmployerInvoices = employerInvoicesService.findLastPaidInvoice(employerInvoices.getEmployer().getId());
			List<EmployerPaymentInvoice> listofemployerpayment = null;
			if(previousEmployerInvoices!=null){
				listofemployerpayment = employerPaymentInvoiceService.findPaymentInvoiceByEmployerInvoices(previousEmployerInvoices);
			}
			BigDecimal previousExcessAmtPaid = FinancialMgmtGenericUtil.getDefaultBigDecimal();
			//if(listofemployerpayment!=null && listofemployerpayment.size()>0){
			if(listofemployerpayment!=null && !listofemployerpayment.isEmpty()){
				for (EmployerPaymentInvoice employerPaymentInvoice : listofemployerpayment){
					//as of now since only one payment happens against a invoice, this condition works, but in future
					if(FinanceConstants.DECISION_N.equalsIgnoreCase(employerPaymentInvoice.getEmployerPayments().getIsRefund())){
						previousExcessAmtPaid = employerPaymentInvoice.getExcessAmount();
					}
				}
			}
			BigDecimal totalPaidAmt  = FinancialMgmtGenericUtil.add(previousExcessAmtPaid, paidAmount);
			if(totalPaidAmt.compareTo(employerInvoices.getTotalAmountDue())==1){
				paymenttype =  PAYMENT_TYPE_EXCESS;
			}
			else if(totalPaidAmt.compareTo(employerInvoices.getTotalAmountDue())==0){
				paymenttype = PAYMENT_TYPE_FULL;
			}
			else if(totalPaidAmt.compareTo(employerInvoices.getTotalAmountDue())==-1){
				if((supportPartialPayment.equalsIgnoreCase(DECISION_YES) && Integer.parseInt(minimumPaymentLimit)==DIVISIBLE_100) || supportPartialPayment.equalsIgnoreCase(DECISION_NO)){
					paymenttype = PAYMENT_TYPE_REJECT;
				}
				else if(supportPartialPayment.equalsIgnoreCase(DECISION_YES) && Integer.parseInt(minimumPaymentLimit)<DIVISIBLE_100){
					BigDecimal  minPaymentLimitObj = FinancialMgmtGenericUtil.getBigDecimalObj(Double.parseDouble(minimumPaymentLimit)/DIVISIBLE_100);
					if((FinancialMgmtGenericUtil.divide(totalPaidAmt, employerInvoices.getTotalAmountDue()).compareTo(minPaymentLimitObj)==1) || (FinancialMgmtGenericUtil.divide(totalPaidAmt, employerInvoices.getTotalAmountDue()).compareTo(minPaymentLimitObj)==0)){	
						paymenttype =  PAYMENT_TYPE_PARTIAL;
					}
					else{
						paymenttype =  PAYMENT_TYPE_REJECT;
					}
				}
			}
			financeRes = doPayment(employerInvoices,totalPaidAmt,paidAmount,paymenttype,paymentType);
			//employee and employer enrollment order should be changed to active and payment made		    	
		}
		/*	} catch (Exception e) {
			throw new FinanceException(e); 
		}*/
		LOGGER.info(" processPayment(-,-,-) method completed successfully=");
		return financeRes;
	}


	/**
	 * Method getFinancialInfo.
	 * @param paymentMethods PaymentMethods
	 * @return FinancialInfo
	 */
	public FinancialInfo getFinancialInfo(PaymentMethods paymentMethods){
		LOGGER.info(" getFinancialInfo(-) method started with paymentMethods:"+paymentMethods.getPaymentMethodName());
		FinancialInfo financialobj = new FinancialInfo();
		List<ModuleUser> moduleUserlist = userService.getModuleUsers(paymentMethods.getModuleId(),ModuleUserService.EMPLOYER_MODULE);
		//currently, in employer module, since for one moduleid and modulename combination, only one module user will be there
		//i am getting first item in list and assuming the first item as module user related to employer
		//in future when multiple moduleuser will be there for one employer, then this below line of module user retrival code will have to be changed accordingly
		ModuleUser moduleUser = moduleUserlist.get(0);

		financialobj.setFirstName(moduleUser.getUser().getFirstName());
		financialobj.setLastName(moduleUser.getUser().getLastName());
		financialobj.setEmail(moduleUser.getUser().getEmail());
		Employer employer = employerRepository.findOne(paymentMethods.getModuleId());
		List<EmployerLocation> locations = employerLocationRepository.findByEmployer(employer); 

		EmployerLocation employerLocation = null;
		//if(locations.size()>0){
		if(locations !=null && !locations.isEmpty()){
			for (EmployerLocation location : locations){
				if((location.getPrimaryLocation().toString()).equals(DECISION_YES)){
					employerLocation = location;
				}
			}
			if(employerLocation != null)
			{
				financialobj.setAddress1(employerLocation.getLocation().getAddress1());
				financialobj.setCity(employerLocation.getLocation().getCity());
				financialobj.setState(employerLocation.getLocation().getState());
				financialobj.setZipcode(employerLocation.getLocation().getZip());
			}
		}

		financialobj.setCountry(COUNTRY);
		financialobj.setContactNumber(employer.getContactNumber());

		if(paymentMethods.getFinancialInfo().getCreditCardInfo()!=null){
			CreditCardInfo creditCardInfo = new CreditCardInfo();
			creditCardInfo.setCardNumber(paymentMethods.getFinancialInfo().getCreditCardInfo().getCardNumber());
			creditCardInfo.setCardType(paymentMethods.getFinancialInfo().getCreditCardInfo().getCardType());
			creditCardInfo.setExpirationMonth(paymentMethods.getFinancialInfo().getCreditCardInfo().getExpirationMonth());
			creditCardInfo.setExpirationYear(paymentMethods.getFinancialInfo().getCreditCardInfo().getExpirationYear());
			financialobj.setCreditCardInfo(creditCardInfo);
		}
		else if(paymentMethods.getFinancialInfo().getBankInfo()!=null){
			BankInfo bankInfo = new BankInfo();
			bankInfo.setAccountNumber(paymentMethods.getFinancialInfo().getBankInfo().getAccountNumber());
			bankInfo.setAccountType(paymentMethods.getFinancialInfo().getBankInfo().getAccountType());
			bankInfo.setRoutingNumber(paymentMethods.getFinancialInfo().getBankInfo().getRoutingNumber());
			financialobj.setBankInfo(bankInfo);
		}
		LOGGER.info(" getFinancialInfo(-) ends successfully with financialobj:"+financialobj.getId());
		return financialobj;
	}


	/**
	 * Method insertPaymentProcessingData.
	 * @param employerInvoices EmployerInvoices
	 * @param paymentMethods PaymentMethods
	 * @param reply HashMap
	 * @param transactionId String
	 * @param paymenttype String
	 * @param excessamt float
	 * @return int
	 */
	//	public int insertPaymentProcessingData(EmployerInvoices employerInvoices,PaymentMethods paymentMethods,@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES) HashMap reply,String transactionId,String paymenttype,float excessamt){
	public EmployerPaymentInvoice insertPaymentProcessingData(EmployerInvoices employerInvoices,PaymentMethods paymentMethods,@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES) Map reply,String transactionId,String paymenttype,BigDecimal excessamt, BigDecimal paidAmt, AccountUser user){
		LOGGER.info(" insertPaymentProcessingData(-,-,-,-) method started with transactionId:"+transactionId+"paymenttype:"+paymenttype+"excessamt:"+excessamt+"paidAmt:"+paidAmt);
		int paymentid = 0;
		updateEmployerPaymentInvoice(employerInvoices);
		EmployerPaymentInvoice employerPaymentInvoice = new EmployerPaymentInvoice();
		employerPaymentInvoice.setEmployerInvoices(employerInvoices);
		employerPaymentInvoice.setAmount(paidAmt);
		employerPaymentInvoice.setExcessAmount(excessamt);
		/*
		 * GL_Code is hard coded value 1000 for Employer_Payment_invoice for Exchange GL_coding
		 */
		employerPaymentInvoice.setGlCode(PAYMENT_GLCODE);
		employerPaymentInvoice.setIsActive(IsActive.Y);
		employerPaymentInvoice.setLastUpdatedBy(user.getId());

		EmployerPayments employerPayments = new EmployerPayments();
		employerPayments.setCaseId(employerInvoices.getCaseId());
		employerPayments.setStatementDate(employerInvoices.getStatementDate());
		employerPayments.setInvoiceNumber(employerInvoices.getInvoiceNumber());
		employerPayments.setPeriodCovered(employerInvoices.getPeriodCovered());
		employerPayments.setPaymentDueDate(employerInvoices.getPaymentDueDate());
		employerPayments.setAmountDueFromLastInvoice(employerInvoices.getAmountDueFromLastInvoice());


		employerPayments.setAmountEnclosed(employerInvoices.getAmountEnclosed());

		employerPayments.setPremiumThisPeriod(employerInvoices.getPremiumThisPeriod());
		employerPayments.setAdjustments(employerInvoices.getAdjustments());
		employerPayments.setExchangeFees(employerInvoices.getExchangeFees());
		employerPayments.setTotalAmountDue(employerInvoices.getTotalAmountDue());

		if(paymentMethods.getPaymentType().toString().equalsIgnoreCase(PaymentType.BANK.toString()))
		{
			employerPayments.setTotalPaymentReceived(FinancialMgmtGenericUtil.getDefaultBigDecimal());
			employerPayments.setStatus(employerInvoices.getPaidStatus().toString());
			//IN-Progress payment status
			employerPayments.setIsPartialPayment('P');
			employerInvoices.setTotalPaymentReceived(FinancialMgmtGenericUtil.getDefaultBigDecimal());
			employerInvoices.setPaidDateTime(new TSDate());
		}
		else
		{
			employerPayments.setTotalPaymentReceived(employerInvoices.getTotalPaymentReceived());
			employerPayments.setStatus(employerInvoices.getPaidStatus().toString());
			if("PARTIALLY_PAID".equalsIgnoreCase(employerInvoices.getPaidStatus().toString())){
				employerPayments.setIsPartialPayment('Y');
			}
			else{
				employerPayments.setIsPartialPayment('N');
			}
			employerInvoices.setPaidDateTime(new TSDate());
			employerInvoices.setSettlementDateTime(new TSDate());
		}


		employerPayments.setTransactionId(transactionId);
		employerPayments.setPaymentMethods(paymentMethods);
		employerPayments.setEmployerPaymentInvoice(employerPaymentInvoice);
		employerPayments.setIsRefund(FinanceConstants.DECISION_N);

		if(reply.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_MERCHANTREFERENCECODE))
		{
			employerPayments.setMerchantRefCode((String)reply.get(CyberSourceResponseMapKeys.CYBERSOURCE_MERCHANTREFERENCECODE));
		} 
		/*if(reply.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_ECDEBIT_RECONCILIATIONID))
			{
				employerPayments.setReconciliationId((String)reply.get(CyberSourceResponseMapKeys.CYBERSOURCE_ECDEBIT_RECONCILIATIONID));
			}
			if(reply.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_ECDEBIT_PROCESSOR_TRANSACTION_ID)) 
			{
				employerPayments.setProcessorTransactionID((String)reply.get(CyberSourceResponseMapKeys.CYBERSOURCE_ECDEBIT_PROCESSOR_TRANSACTION_ID));
			}*/

		employerPaymentInvoice.setEmployerPayments(employerPayments);
		EmployerPaymentInvoice newEmployerPaymentInvoice = employerPaymentInvoiceService.saveEmployerPaymentInvoice(employerPaymentInvoice);
		LOGGER.info(" Save Employer Payment Invoice with value:"+employerPaymentInvoice.getId());
		// number to increment and set to confirmNumber of employerPaymentInvoice by adding id
		long tempNumber=0;
		paymentid=newEmployerPaymentInvoice.getId();
		tempNumber=CONFIRM_NUMBER+paymentid;
		newEmployerPaymentInvoice.setConfirmNumber(tempNumber);
		employerPaymentInvoiceService.saveEmployerPaymentInvoice(newEmployerPaymentInvoice);
		employerInvoices.setLastUpdatedBy(user.getId());
		employerInvoicesService.saveEmployerInvoices(employerInvoices);

		paymentid = newEmployerPaymentInvoice.getId();
		LOGGER.info(" insertPaymentProcessingData(-) ends successfully with newEmployerPaymentInvoice confirm no:"+newEmployerPaymentInvoice.getConfirmNumber());
		return newEmployerPaymentInvoice;
	}

	/**
	 * Method getMapValue.
	 * @param map Map
	 * @return String
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	private String getMapValue(Map map) {

		//StringBuffer dest = new StringBuffer();
		StringBuilder dest = new StringBuilder();

		if (map != null && !map.isEmpty()) {
			Iterator itr = map.entrySet().iterator();
			String key, val;
			while (itr.hasNext()) {
				Map.Entry entry = (Map.Entry)itr.next();
				key = (String) entry.getKey();
				val = (String) entry.getValue();
				dest.append(key + "=" + val + ",");
			}
		}

		return dest.toString();
	}

	/**
	 * Method refundExcessAmt.
	 * @param employerInvoices EmployerInvoices
	 * @param totalPaidAmt BigDecimal
	 * @param paymenttype String
	 * @return BigDecimal[]
	 * 
	 */
	private BigDecimal[] refundExcessAmt(EmployerInvoices employerInvoices, String paymenttype, BigDecimal totalPaidAmt)
	{
		String maximumPaymentLimit = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MAXIMUM_PAYMENT_THRESHOLD);
		BigDecimal refundamt = FinancialMgmtGenericUtil.getDefaultBigDecimal();
		BigDecimal[] bigDecimalArray = new BigDecimal[FinanceConstants.NUMERICAL_2];
		BigDecimal excessamtToUseForNextBill = FinancialMgmtGenericUtil.getDefaultBigDecimal();
		if(paymenttype.equalsIgnoreCase(FinanceConstants.PAYMENT_TYPE_EXCESS))
		{
			BigDecimal excessamt = totalPaidAmt.subtract(employerInvoices.getTotalPaymentReceived());
			BigDecimal maxPaymentLimObj = FinancialMgmtGenericUtil.getBigDecimalObj(Double.parseDouble(maximumPaymentLimit)/DIVISIBLE_100);
			if((FinancialMgmtGenericUtil.divide(excessamt, employerInvoices.getTotalAmountDue()).compareTo(maxPaymentLimObj) == FinanceConstants.NUMERICAL_NEG_1) || (FinancialMgmtGenericUtil.divide(excessamt, employerInvoices.getTotalAmountDue()).compareTo(maxPaymentLimObj) == FinanceConstants.NUMERICAL_0)){			  
				excessamtToUseForNextBill = excessamt;
			}
			else
			{
				excessamtToUseForNextBill = FinancialMgmtGenericUtil.multiply(employerInvoices.getTotalAmountDue(), FinancialMgmtGenericUtil.getBigDecimalObj(Double.parseDouble(maximumPaymentLimit)/DIVISIBLE_100));				
				refundamt = FinancialMgmtGenericUtil.subtract(FinancialMgmtGenericUtil.subtract(totalPaidAmt, employerInvoices.getTotalAmountDue()), excessamtToUseForNextBill);		
			}
		}
		bigDecimalArray[FinanceConstants.NUMERICAL_0] = refundamt;
		bigDecimalArray[FinanceConstants.NUMERICAL_1] = excessamtToUseForNextBill;
		return bigDecimalArray;
	}
	/**
	 * Method doPayment.
	 * @param employerInvoices EmployerInvoices
	 * @param totalPaidAmt float
	 * @param paidAmt float
	 * @param paymenttype String
	 * @param paymentType PaymentMethods
	 * @return FinanceResponse
	 * @throws FinanceException, CyberSourceKeyException
	 */
	@SuppressWarnings({ FinanceConstants.SUPPRESS_WARNING_RAWTYPES})
	private FinanceResponse doPayment(EmployerInvoices employerInvoices,BigDecimal totalPaidAmt,BigDecimal paidAmt,String paymenttype,PaymentMethods paymentType) throws FinanceException, CyberSourceKeyException{    	

		if(employerInvoices != null)
		{
			LOGGER.info(" doPayment(-,-,-,-,-) method started with totalPaidAmt:"+totalPaidAmt+" paidAmt:"+paidAmt+" paymentType:"+paymentType);
			String statusOfPayment = "";
			int paymentId = 0;
			EmployerPaymentInvoice employerPaymentInvoice = null;
			FinanceResponse financeRes = new FinanceResponse();


			String transactionType = null;
			BigDecimal paidAmount = paidAmt;
			String previousInvoiceNumber= null;    	
			if(!paymenttype.equalsIgnoreCase(PAYMENT_TYPE_REJECT)){
				//only if paidamt is greater than 0 then we will hit cybersource
				Map cybersourceResponse = null;
				AccountUser user = FinanceGeneralUtility.getAccountUser(userService);
				
				File keyFilePath = new File(DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.UPLOAD_PATH)+"/"+DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MERCHANT_ID)+".p12");
				if(paidAmt.compareTo(BigDecimal.ZERO)==1){
					if(keyFilePath.exists()){
						transactionType = FinanceConstants.TRANSACTION_TYPE_DEBIT;    				
					}else {
						LOGGER.error("=Cyber source key file is not found=");
						throw new CyberSourceKeyException("Cybersource security key not found "); 
					}
				}else if (paidAmt.compareTo(BigDecimal.ZERO) == FinanceConstants.NUMERICAL_NEG_1) {
					if(keyFilePath.exists()){
						EmployerInvoices previousInvoice = employerInvoicesService.findPreviousInvoice(employerInvoices.getEmployer().getId(), employerInvoices.getStatementDate());
						if(previousInvoice!=null && (previousInvoice.getPaidStatus().equals(PaidStatus.PAID) || previousInvoice.getPaidStatus().equals(PaidStatus.PARTIALLY_PAID))){
							transactionType = FinanceConstants.TRANSACTION_TYPE_CREDIT;
							paidAmount = paidAmt.negate();
							previousInvoiceNumber= previousInvoice.getInvoiceNumber();
						}
						else if(previousInvoice!=null){
						
							LOGGER.info("Employer id:"
									+ employerInvoices.getEmployer().getId()
									+ " Current invoice "
									+ employerInvoices.getInvoiceNumber()
									+ " amount can't be refunded until last invoice "
									+ previousInvoice.getInvoiceNumber()
									+ " payment is cleared/paid");
							financeRes.setProcessPaymentResposne("Current invoice "+employerInvoices.getInvoiceNumber()+" amount can't be refunded until last invoice "+previousInvoice.getInvoiceNumber()+" payment is cleared/paid");
							financeRes.setPaymentId(paymentId);
							return financeRes;
						}
					}else {
						LOGGER.error("=Cyber source key file is not found=");
						throw new CyberSourceKeyException("Cybersource security key not found "); 
					}
				}

				BigDecimal[] refundAmtArray = refundExcessAmt(employerInvoices, paymenttype, totalPaidAmt);
				String replyDecission = "";
				String transactionId = "";

				synchronized(this)
				{
					EmployerInvoices  emplInvoices = employerInvoicesService.findEmployerInvoicesById(employerInvoices.getId());
					if(emplInvoices!=null && (emplInvoices.getPaidStatus().equals(PaidStatus.DUE) || emplInvoices.getPaidStatus().equals(PaidStatus.PARTIALLY_PAID)))
					{    			
						cybersourceResponse = callCyberSourceForPayment(paidAmount,paymentType, transactionType, employerInvoices.getInvoiceNumber(), previousInvoiceNumber);
						//if((cybersourceResponse!=null && cybersourceResponse.size()>0) || paidAmt.compareTo(BigDecimal.ZERO)==0)
						if((cybersourceResponse!=null && !cybersourceResponse.isEmpty()) || paidAmt.compareTo(BigDecimal.ZERO)==0)
						{    			
							//if(cybersourceResponse!=null && cybersourceResponse.size()>0){
							if(cybersourceResponse!=null && !cybersourceResponse.isEmpty()){
								replyDecission = (String) cybersourceResponse.get(FinanceConstants.CyberSourceResponseMapKeys.CYBERSOURCE_DECISION);
								transactionId = (String) cybersourceResponse.get(FinanceConstants.CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID);
							}
							//when the amount of processing is greater than 1000 then cybersource is giving error response
							//just for time being for testing purpose, we are setting decision as 'ACCEPT' in order to show all payment as success
							//in future, this line has to be removed, i.e. only when we get proper response from cybersource
							//we have to go for processing.

							//For Jira HIX-22741 below line is commented
							//replyDecission = FinanceConstants.CYBERSOURCE_ACCEPT;

							//even though currently no amt paid but if previouse excess is sufficient for invoice payment then we should process payment 
							//thus, paidamt zero checking is done
							if(replyDecission.equalsIgnoreCase(FinanceConstants.CYBERSOURCE_ACCEPT) || paidAmt.compareTo(BigDecimal.ZERO)== FinanceConstants.NUMERICAL_0)
							{
								if(paymentType.getPaymentType().toString().equalsIgnoreCase(PaymentType.BANK.toString()))
								{
									updateLineItemStatus(employerInvoices, totalPaidAmt, user);
								}
								else
								{
									calculateEachLineItemPayment(employerInvoices,totalPaidAmt,paidAmt,paymenttype, user);
								}
								//	if(BigDecimal.ZERO != refundAmtArray[0])
								if(BigDecimal.ZERO.compareTo(refundAmtArray[FinanceConstants.NUMERICAL_0]) != FinanceConstants.NUMERICAL_0)
								{
									refundExcessAmt(refundAmtArray[FinanceConstants.NUMERICAL_0],paymentType,employerInvoices); 
								}        			
								employerPaymentInvoice = insertPaymentProcessingData(employerInvoices,paymentType,cybersourceResponse,transactionId,paymenttype,refundAmtArray[FinanceConstants.NUMERICAL_1], paidAmt,user);
								//after paying current invoice, if total paid amt i.e. last times excess amt and
								//and this times paidamt is less than or equal to maximum limit then we will use that amt to use for settling next bill
								String changeEnrollmentStatusEarly =  DynamicPropertiesUtil.getPropertyValue(SHOPConfiguration.SHOPConfigurationEnum.EMPLOYER_ENROLLMENTSTATUS_EARLY);
								if (("YES".equalsIgnoreCase(changeEnrollmentStatusEarly) || !paymentType.getPaymentType().toString().equalsIgnoreCase(PaymentType.BANK.toString()))  
										&&  "BINDER_INVOICE".equalsIgnoreCase(employerInvoices.getInvoiceType().toString()))
								{
									try
									{
										updateEnrollmentStatus(employerInvoices);
									}
									catch (GIException ex)
									{
										LOGGER.error(" processPayment(-,-,-) method failed while updating enrollment status for employerId:"+employerInvoices.getEmployer().getId()+" and Invoice Number: "+employerInvoices.getId());
										LOGGER.error("Exception caught while updating enrollment status: "+ ex);
										//and rest of the amt will be refunded
									}
								}

								paymentId = employerPaymentInvoice.getId();
								statusOfPayment = "success";
							}
							else
							{
								String reason = (String) cybersourceResponse.get("reasonCode");
								statusOfPayment = CyberSrcResponseCodes.resultCodes().get(reason);
							}
							EmployerPayments employerPayments = null;
							if(employerPaymentInvoice!=null){
								employerPayments = employerPaymentInvoice.getEmployerPayments();
							}
							insertPaymentLog(employerPayments, cybersourceResponse, employerInvoices, paymentType.getPaymentType().toString(),user);
						}
						else if((paidAmt.compareTo(BigDecimal.ZERO)==FinanceConstants.NUMERICAL_1) && cybersourceResponse==null){
							statusOfPayment = "noactivepaymenttype";
						}
					}
					else
					{
						LOGGER.error("Payment is already done for the invoice id "+employerInvoices.getId());
					}
				}
			}
			else{
				statusOfPayment =  PAYMENT_TYPE_REJECT;
			}
			financeRes.setProcessPaymentResposne(statusOfPayment);
			financeRes.setPaymentId(paymentId);
			LOGGER.info(" doPayment(-,-,-,-,-) ends successfully=");
			return financeRes;
		}
		else
		{
			throw new FinanceException("Received null or Empty EmployerInvoice for Payment");
		}
	}

	/**
	 * Method callCyberSourceForPayment.
	 * @param processAmt float
	 * @param paymentType PaymentMethods
	 * @param transactionType String
	 * @param invoiceId String
	 * @return HashMap
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Map callCyberSourceForPayment(BigDecimal processAmt,PaymentMethods paymentType,String transactionType, String invoiceId, String previousInvoiceNumber){	
		String isPaymentGateWay = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_PAYMENT_GATEWAY);
		LOGGER.info(" callCyberSourceForPayment(-,-,-) started  with processAmt:"+processAmt);
		Map capturereply =  null;
		if(paymentType!=null){
			FinancialInfo financialobj =  getFinancialInfo(paymentType);
			FinancialInfoMapper fiMapper = new FinancialInfoMapper();

			String timeStamp = new SimpleDateFormat("_yyyyMMdd_HHmmss").format(TSCalendar.getInstance().getTime());
			String merchantRefCode = "EMP_"+invoiceId + timeStamp;

			if(isPaymentGateWay != null && isPaymentGateWay.equalsIgnoreCase(FinanceConstants.TRUE))
			{
				fiMapper.setPciPayRequest(paymentType.getSubscriptionId(), merchantRefCode);
			}
			else
			{
				fiMapper.setPayRequest(financialobj, merchantRefCode);
			}
			try
			{
				if( FinanceConstants.TRANSACTION_TYPE_DEBIT.equalsIgnoreCase(transactionType)){
					if(financialobj.getCreditCardInfo()!=null){
						capturereply = captureEmployerPremiumUsingCreditCard(fiMapper.getPayRequest(), processAmt.toPlainString(), isPaymentGateWay);
					}
					else if(financialobj.getBankInfo()!=null){
						capturereply = captureEmployerEcheck(fiMapper.getPayRequest(), processAmt.toPlainString(), isPaymentGateWay);
					}
				}
				else if( FinanceConstants.TRANSACTION_TYPE_CREDIT.equalsIgnoreCase(transactionType)){
					if(financialobj.getCreditCardInfo()!=null){
						capturereply = refundUsingCreditCard(fiMapper.getPayRequest(), processAmt.toPlainString(), isPaymentGateWay);
					}
					else if(financialobj.getBankInfo()!=null){
						Map<Object, Object> request = fiMapper.getPayRequest();
						if(previousInvoiceNumber!=null){
							List<EmployerPayments> employerPayments = iEmployerPaymentRepository.findPaidEmployerPaymentsByInvoiceNumber(previousInvoiceNumber);
							if(employerPayments!=null && !employerPayments.isEmpty()){
								request.put(FinanceConstants.CYBERSOURCE_ECCREDIT_SERVICE_DEBIT_REQUESTID, employerPayments.get(FinanceConstants.NUMERICAL_0).getTransactionId());
							}
						}
						capturereply = refundUsingEcheck(request, processAmt.toPlainString(), isPaymentGateWay);
					}
				}
			}
			catch(GIException gi)
			{
				LOGGER.info(" callCyberSourceForPayment(-,-,-) failed for invoice id: "+invoiceId+" =");
				LOGGER.error("Exception caught: "+gi);
			}
		}
		LOGGER.info(" callCyberSourceForPayment(-,-,-) ends successfully  =");
		return capturereply;
	}

	//settlementAmt is actual amt paid and excess amt of last payment
	//paidAmt is actual amt paid
	/**
	 * Method calculateEachLineItemPayment.
	 * @param employerInvoices EmployerInvoices
	 * @param settlementAmt float
	 * @param paidAmt float
	 * @param paymenttype String
	 * @return EmployerInvoices
	 */
	private EmployerInvoices calculateEachLineItemPayment(EmployerInvoices employerInvoices,BigDecimal settlementAmout,BigDecimal paidAmt,String paymenttype, AccountUser user){
		LOGGER.info(" calculateEachLineItemPayment(-,-,-,-,-) method started with settlementAmt: "+settlementAmout+" paidAmt: "+paidAmt+" paymentType: "+paymenttype);
		BigDecimal settlementAmt = settlementAmout;
		List<EmployerInvoiceLineItems> employerInvoiceLineItemsList = null;
		String minimumPaymentLimit = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MINIMUM_PAYMENT_THRESHOLD);
		String paymentAllocationType = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.PARITIAL_PAYMENT_ALLOCATION_TYPE);
		if(paymenttype.equalsIgnoreCase(PAYMENT_TYPE_FULL) || paymenttype.equalsIgnoreCase(PAYMENT_TYPE_EXCESS)){
			employerInvoices.setTotalPaymentReceived(employerInvoices.getTotalAmountDue());
			employerInvoices.setPaidStatus(PaidStatus.PAID);
			EmployerInvoices latestInvoice = employerInvoicesService.findLastActiveInvoice(employerInvoices.getEmployer().getId());
			if(latestInvoice!=null && !(latestInvoice.getInvoiceNumber().equals(employerInvoices.getInvoiceNumber())))
			{
				employerInvoices.setIsActive(FinanceConstants.DECISION_N);
			}
			//we store actual amt paid in amountenclosed field
			employerInvoices.setAmountEnclosed(paidAmt);
			employerInvoiceLineItemsList = employerInvoiceLineItemsService.getInvoiceLineItem(employerInvoices);
			for (EmployerInvoiceLineItems employerInvoiceLineItems : employerInvoiceLineItemsList){
				employerInvoiceLineItems.setPaymentReceived(employerInvoiceLineItems.getNetAmount());
				employerInvoiceLineItems.setPaidStatus(Status.PAID);
				employerInvoiceLineItems.setPaidDate(new TSDate());
				employerInvoiceLineItems.setLastUpdatedBy(user);
			}
		}
		else if(paymenttype.equalsIgnoreCase(PAYMENT_TYPE_PARTIAL)){
			double minimumthreshold = Double.parseDouble(minimumPaymentLimit);
			BigDecimal totalLineitemOnminithreshold = FinancialMgmtGenericUtil.getDefaultBigDecimal();
			employerInvoices.setTotalPaymentReceived(settlementAmt);
			employerInvoices.setPaidStatus(PaidStatus.PARTIALLY_PAID);
			//we store actual amt paid in amountenclosed field
			employerInvoices.setAmountEnclosed(paidAmt);
			employerInvoiceLineItemsList = employerInvoiceLineItemsService.getInvoiceLineItem(employerInvoices);
			for (EmployerInvoiceLineItems employerInvoiceLineItems : employerInvoiceLineItemsList){
				BigDecimal lineitemminimumamt = FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getNetAmount(), FinancialMgmtGenericUtil.getBigDecimalObj(minimumthreshold/DIVISIBLE_100));
				employerInvoiceLineItems.setPaymentReceived(lineitemminimumamt);
				employerInvoiceLineItems.setPaidStatus(Status.PARTIALLY_PAID);
				employerInvoiceLineItems.setPaidDate(new TSDate());
				employerInvoiceLineItems.setLastUpdatedBy(user);
				settlementAmt = FinancialMgmtGenericUtil.subtract(settlementAmt, lineitemminimumamt);
				totalLineitemOnminithreshold = FinancialMgmtGenericUtil.add(totalLineitemOnminithreshold, lineitemminimumamt);
			}
			if((settlementAmt.compareTo(BigDecimal.ZERO)==FinanceConstants.NUMERICAL_1) && "ratio".equalsIgnoreCase(paymentAllocationType)){
				for (EmployerInvoiceLineItems employerInvoiceLineItems : employerInvoiceLineItemsList){
					BigDecimal lineitempendingamt = FinancialMgmtGenericUtil.subtract(employerInvoiceLineItems.getNetAmount(), employerInvoiceLineItems.getPaymentReceived());
					BigDecimal totalinvoicependingamt = FinancialMgmtGenericUtil.subtract(employerInvoices.getTotalAmountDue(), totalLineitemOnminithreshold);
					BigDecimal ratioallocationamt = FinancialMgmtGenericUtil.divide(FinancialMgmtGenericUtil.multiply(lineitempendingamt, settlementAmt), totalinvoicependingamt);
					employerInvoiceLineItems.setPaymentReceived(FinancialMgmtGenericUtil.add(employerInvoiceLineItems.getPaymentReceived(), ratioallocationamt));
				}
			}
		}
		if(employerInvoiceLineItemsList!=null){
			employerInvoices.setEmployerInvoiceLineItems(employerInvoiceLineItemsList);
		}
		LOGGER.info(" calculateEachLineItemPayment(-,-,-,-) ends successfully with employerInvoices:"+employerInvoices+" =");
		return employerInvoices;
	}






	/**
	 * Method refundExcessAmt.
	 * @param refundamt float
	 * @param paymentMethods PaymentMethods
	 * @param employerInvoices EmployerInvoices
	 * @return int
	 * @see com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService#refundExcessAmt(float, PaymentMethods, EmployerInvoices)
	 */
	@Override	
	public int refundExcessAmt(BigDecimal refundamt,PaymentMethods paymentMethods,EmployerInvoices employerInvoices){
		LOGGER.info(" refundExcessAmt(-) method started with refundamt:"+refundamt);
		int refundid = 0;
		@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
		Map cybersourceResponse = callCyberSourceForPayment(refundamt,paymentMethods,"Credit", employerInvoices.getInvoiceNumber(), employerInvoices.getInvoiceNumber()); 

		String replyDecission = "";
		String transactionId = "";

		//if(cybersourceResponse!=null && cybersourceResponse.size()>0){
		if(cybersourceResponse!=null && !cybersourceResponse.isEmpty()){
			replyDecission = (String) cybersourceResponse.get(FinanceConstants.CyberSourceResponseMapKeys.CYBERSOURCE_DECISION);
			transactionId = (String) cybersourceResponse.get(FinanceConstants.CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID);
		}
		//when the amount of processing is greater than 1000 then cybersource is giving error response
		//just for time being for testing purpose, we are setting decision as 'ACCEPT' in order to show all payment as success
		//in future, this line has to be removed, i.e. only when we get proper response from cybersource
		//we have to go for processing.
		replyDecission = FinanceConstants.CYBERSOURCE_ACCEPT;
		if(replyDecission.equalsIgnoreCase(FinanceConstants.CYBERSOURCE_ACCEPT)){
			EmployerPaymentInvoice employerPaymentInvoice = new EmployerPaymentInvoice();
			employerPaymentInvoice.setEmployerInvoices(employerInvoices);
			employerPaymentInvoice.setCreationDate(new TSDate());

			employerPaymentInvoice.setAmount(refundamt);
			employerPaymentInvoice.setExcessAmount(FinancialMgmtGenericUtil.getDefaultBigDecimal());
			EmployerPayments employerPayments = new EmployerPayments();
			employerPayments.setIsRefund(FinanceConstants.DECISION_Y);
			employerPayments.setTransactionId(transactionId);
			employerPayments.setRefundAmt(refundamt);

			employerPaymentInvoice.setEmployerPayments(employerPayments);

			EmployerPaymentInvoice newEmployerPaymentInvoice = employerPaymentInvoiceService.saveEmployerPaymentInvoice(employerPaymentInvoice);
			refundid = newEmployerPaymentInvoice.getId();
		}
		LOGGER.info(" refundExcessAmt(-) ends successfully with refundid:"+refundid);
		return refundid;
	}

	/**
	 * Method updateEnrollmentStatusToActive.
	 * @param employerid int
	 * @throws GIException
	 * @see com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService#updateEnrollmentStatusToActive(int)
	 * @Revision Jira Id: HIX-44681, Ignoring the case when EmployerEnrollment and enrollment are in Terimated status.
	 */
	@Override
	public void updateEnrollmentStatusToActive(int employerEnrollmentId)throws GIException
	{

		if (financialMgmtUtils.updateEmployerEnrollmentStatus(
				EmployerEnrollment.Status.ACTIVE.toString(),
				employerEnrollmentId).equalsIgnoreCase(
						GhixConstants.RESPONSE_SUCCESS))
		{
			financialMgmtUtils.updateEnrollmentStatus(employerEnrollmentId,
					EnrollmentStatus.PAYMENT_RECEIVED);
		}
	}

	/**
	 * Method disEnrollEmployers.
	 * @throws GIException
	 * @see com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService#disEnrollEmployers()
	 */
	@Override
	public void disEnrollEmployers(InvoiceType invoiceType) throws FinanceException {
		LOGGER.info(" disEnrollEmployers(-) method started with invoiceType:"+invoiceType);
		List<EmployerInvoices> disEnrollEmployerList = null;
		//String status = null;
		Date currentDate = new TSDate();
		if(InvoiceType.NORMAL == invoiceType){ // for Normal employer invoice termination
			String noOfPaymentGraceDays = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.PAYMENT_GRACE_PERIOD);
			int paymentGracePeroid = Integer.parseInt(noOfPaymentGraceDays);
			DateTime curJodaTime = TSDateTime.getInstance();
			curJodaTime = curJodaTime.minusDays(paymentGracePeroid);
			curJodaTime = setJodaTime(curJodaTime);
			Date dateNew = curJodaTime.toDate();
			//status = EmployerEnrollment.Status.TERMINATED.toString();
			disEnrollEmployerList = employerInvoicesService.disEnrollEmployers(FinanceConstants.DECISION_Y, invoiceType, dateNew);	
		}else // for Binder employer invoice cancellation
		{
			DateTime paymentDueDate = TSDateTime.getInstance();
			paymentDueDate = paymentDueDate.withDayOfMonth(FinanceConstants.NUMERICAL_15);
			//status = EmployerEnrollment.Status.CANCELLED.toString();
			disEnrollEmployerList = employerInvoicesService.findDueEmployerInvoices(FinanceConstants.DECISION_Y,  invoiceType, currentDate);
		}

		if(disEnrollEmployerList!=null && !disEnrollEmployerList.isEmpty()){
			LOGGER.info(" disEnrollEmployers(-) method : dis enroll invoice list size :"+disEnrollEmployerList.size());
			List<Integer> employerIdList = new ArrayList<Integer>();
			Map<String, Exception> exceptionDetails = null;
			for (EmployerInvoices disInvoice : disEnrollEmployerList) {
				if(!employerIdList.contains(disInvoice.getEmployer().getId())){
					try
					{
						Date terminationDate = null;
						String invoiceCreatedDate = null;
						List<Long> enrollmentid = null;
						int employerEnrollId = this.getActiveEmployerEnrollmentId(disInvoice);
						
						if (employerEnrollId>0) {
							Object employerInvoices = employerInvoicesService.findPeriodCoveredAndInvoiceCreationDate(employerEnrollId);
							if (employerInvoices != null) {
								Object[] invDtl = (Object[])employerInvoices;
								if(invDtl.length==3){
									String periodCoveredString = (String)invDtl[1];
								String lastPaymentDate = periodCoveredString.substring(periodCoveredString.lastIndexOf("to")+FinanceConstants.NUMERICAL_2).trim();
									SimpleDateFormat dateFormat = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MM_DD_YYYY);
									SimpleDateFormat datetimeFormat = new SimpleDateFormat(FinanceConstants.REINSTIATION_TIME_FORMAT);
								terminationDate = dateFormat.parse(lastPaymentDate);
									Date invoiceCreation = (Date)invDtl[2];
								invoiceCreatedDate = datetimeFormat.format(invoiceCreation);
									enrollmentid = employerInvoiceLineItemsService.getInvoiceEnrollmentID((Integer)invDtl[0]);
								}
							}
						}else {
							throw new FinanceException("There is no active employer enrollment present for employer id: "+disInvoice.getEmployer().getId());
						}
						ShopResponse shopResponse = financialMgmtUtils.employerEnrollmentTermination(disInvoice.getEmployer().getId(),employerEnrollId,terminationDate,invoiceCreatedDate,enrollmentid);
						/*
						 * HIX-59432 verifying on Error Code instead of Status
						 * 200- Means Success, 202- No Employer Enrollment, 204- No Enrollment exists but employer Enrollment exists
						 */
						if(shopResponse != null)
						{
							if(shopResponse.getErrCode()==FinanceConstants.ERROR_CODE_202 ||
									shopResponse.getErrCode()==FinanceConstants.ERROR_CODE_204 ||
									shopResponse.getErrCode()==FinanceConstants.SUCCESS_CODE_200)
							{
								cancelInvoices(disInvoice.getEmployer().getId(),employerEnrollId,FinanceConstants.TERMINATON_REASON_STRING);
								employerIdList.add(disInvoice.getEmployer().getId());
							}
							else
							{
								LOGGER.error("Disenrollment failed for employer id: "+disInvoice.getEmployer().getId()+" invoice id"+disInvoice.getId());
								if(exceptionDetails==null){
									exceptionDetails = new HashMap<String, Exception>();
								}
								exceptionDetails.put(disInvoice.getInvoiceNumber(), new FinanceException(shopResponse.getErrMsg()+"FAILURE ERROR CODE: "+shopResponse.getErrCode()));
							}
						}
						else
						{
							if(exceptionDetails==null){
								exceptionDetails = new HashMap<String, Exception>();
							}
							exceptionDetails.put(disInvoice.getInvoiceNumber(), new FinanceException("Null or Empty ShopResponse Received"));

						}
					}
					catch(Exception ex)
					{
						LOGGER.error("Exception caught while disenrolling employer invoice: " +ex);
						if(exceptionDetails==null){
							exceptionDetails = new HashMap<String, Exception>();
						}
						exceptionDetails.put(disInvoice.getInvoiceNumber(), ex);
					}
				}
			}

			if(exceptionDetails!=null && !exceptionDetails.isEmpty()){
				final String exceptionMessage = FinanceGeneralUtility.constructSingleMessageForExceptionDetails("EXCEPTION OCCURRED WHILE CANCEL/TERMINATING FOLLOWING DUE INVOICES:", "Invoice Number", exceptionDetails);
				throw new FinanceException(exceptionMessage);
			}
		}else{
			LOGGER.warn("No employer invoices found as paidStatus is 'DUE' and exceeds payment grace period");
		}
	}

	/**
	 * 
	 * @param employerInvoices
	 */
	@Override
	public void updateEnrollmentStatus(EmployerInvoices employerInvoices)throws GIException
	{
		LOGGER.info(" updateEnrollmentStatus(-) method started with employerInvoices no:"+employerInvoices.getInvoiceNumber());
		if(FinanceConstants.BINDER_INVOICE.equalsIgnoreCase(employerInvoices.getInvoiceType().toString()))
		{
			List<EmployerInvoiceLineItems> employerInvoiceLineItems = employerInvoiceLineItemsService.findByEmployerInvoicesNIsActiveEnroll(employerInvoices.getId(), IsActiveEnrollment.Y);
			if (!employerInvoiceLineItems.isEmpty()) {
				int employerEnrollmentId = employerInvoiceLineItems.get(FinanceConstants.NUMERICAL_0).getEmployerEnrollmentId();
				updateEnrollmentStatusToActive(employerEnrollmentId);
			}
		}
		LOGGER.info(" updateEnrollmentStatus(-) method End =");
	}

	@SuppressWarnings("rawtypes")
	private PaymentEventLog insertPaymentLog(EmployerPayments employerPayments, Map cybersourceResponse, EmployerInvoices employerInvoices, String paymentType, AccountUser user)
	{
		LOGGER.info(" insertPaymentLog(-) method Start =");
		PaymentEventLog paymentEventLog = new PaymentEventLog();
		paymentEventLog.setResponse(getMapValue(cybersourceResponse));
		if(employerPayments != null)
		{
			paymentEventLog.setEmployerPayments(employerPayments);
		}
		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_MERCHANTREFERENCECODE))
		{
			paymentEventLog.setMerchantRefCode((String)cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_MERCHANTREFERENCECODE));
		}

		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_ECDEBIT_RECONCILIATIONID))
		{
			paymentEventLog.setTransactionId((String)cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_ECDEBIT_RECONCILIATIONID));
		}
		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_ECCREDIT_RECONCILIATIONID))
		{
			paymentEventLog.setTransactionId((String)cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_ECCREDIT_RECONCILIATIONID));
		}

		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_CCCREDITREPLY_RECONCILIATIONID))
		{
			paymentEventLog.setTransactionId((String) cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_CCCREDITREPLY_RECONCILIATIONID));
		}
		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_CCCAPTUREREPLY_RECONCILIATIONID))
		{
			paymentEventLog.setTransactionId((String) cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_CCCAPTUREREPLY_RECONCILIATIONID));
		}

		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE))
		{
			paymentEventLog.setTxnReasonCode((String)cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE));
		}
		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_DECISION))
		{
			paymentEventLog.setTxnStatus((String)cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_DECISION));
			if("ACCEPT".equalsIgnoreCase(cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_DECISION).toString()))
			{
				if(StringUtils.isNotEmpty(paymentType) &&  paymentType.equalsIgnoreCase(PaymentType.CREDITCARD.toString()))
				{
					/*
					 * marking CreditCard Payment as Confirmed.
					 */
					paymentEventLog.setPaymentStatus(PaymentStatus.CONFIRMED);
				}
				else
				{
					paymentEventLog.setPaymentStatus(PaymentStatus.PENDING);
				}
			}
			else
			{
				paymentEventLog.setPaymentStatus(PaymentStatus.FAILED);
			}
		}
		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID))
		{
			paymentEventLog.setRequestId((String)cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID));
		}
		paymentEventLog.setEmployerInvoices(employerInvoices);
		paymentEventLog.setLastUpdatedBy(user.getId());
		LOGGER.info(" insertPaymentLog(-) method End =");
		return paymentEventLogService.savePaymentEventLog(paymentEventLog);
	}

	private EmployerInvoices updateLineItemStatus(EmployerInvoices employerInvoices, BigDecimal settlementAmt, AccountUser user){
		LOGGER.info(" updateLineItemStatus(-) method started with settlementAmt:"+settlementAmt);
		List<EmployerInvoiceLineItems> employerInvoiceLineItemsList = null;

		employerInvoices.setPaidStatus(PaidStatus.IN_PROCESS);
		employerInvoices.setTotalPaymentReceived(FinancialMgmtGenericUtil.getDefaultBigDecimal());
		employerInvoices.setAmountEnclosed(FinancialMgmtGenericUtil.getDefaultBigDecimal());

		employerInvoiceLineItemsList = employerInvoiceLineItemsService.getInvoiceLineItem(employerInvoices);
		if(employerInvoiceLineItemsList != null && !employerInvoiceLineItemsList.isEmpty())
		{
			for (EmployerInvoiceLineItems employerInvoiceLineItems : employerInvoiceLineItemsList)
			{
				employerInvoiceLineItems.setPaidStatus(Status.IN_PROCESS);
				employerInvoiceLineItems.setLastUpdatedBy(user);
			}
			employerInvoices.setEmployerInvoiceLineItems(employerInvoiceLineItemsList);
		}
		LOGGER.info(" updateLineItemStatus(-) method End =");
		return employerInvoices;
	}

	private void updateEmployerPaymentInvoice(EmployerInvoices employerInvoices)
	{
		LOGGER.info(" updateEmployerPaymentInvoice(-) method started =");
		EmployerPaymentInvoice employerPaymentInvoice = employerPaymentInvoiceService.findActiveEmployerPaymentInvoice(employerInvoices);
		if(employerPaymentInvoice != null)
		{
			employerPaymentInvoiceService.updateEmployerPaymentInvoice(employerPaymentInvoice.getId(), IsActive.N);
		}
		else
		{
			LOGGER.info("No EmployerPaymentInvoice found for employerInvoice: "+employerInvoices.getId());
		}
		LOGGER.info(" updateEmployerPaymentInvoice(-) method end =");
	}

	/**
	 * 
	 * @param invoiceId
	 * @param employerInvoiceLineItemsList
	 * @throws GIException
	 * Setting Employer_Invoices as Cancel for Non payment of Invoice within given due date
	 */
	@Override
	public void cancelInvoices(int  employerId, int employerEnrollId, String cancelReason){
		LOGGER.info(" cancleInvoices(-) method started ="); 
		AccountUser user = FinanceGeneralUtility.getAccountUser(userService);
		//	 try{
		//List<EmployerInvoices> disInvoices = employerInvoicesService.findByEmployerAndIsActive(employerId, FinanceConstants.DECISION_Y);
		/*List<EmployerInvoices> disInvoices = employerInvoicesService.findInvoiceByEmployerIdNIsAcitveNIsActiveEmpEnrollId(employerId, IsActiveEnrollment.Y, FinanceConstants.DECISION_Y, employerEnrollId);
		if(disInvoices!=null && !disInvoices.isEmpty()){
			for(EmployerInvoices employerInvoices: disInvoices){
				employerInvoices.setIsActive(FinanceConstants.DECISION_N);
				employerInvoices.setPaidStatus(EmployerInvoices.PaidStatus.CANCEL);
				employerInvoices.setTerminationReason(cancelReason);
				employerInvoices.setTerminationDate(new TSDate());
				employerInvoices.setLastUpdatedBy(user.getId());
				employerInvoicesService.saveEmployerInvoices(employerInvoices);
				List<EmployerInvoiceLineItems> lineItems = employerInvoiceLineItemsService.getInvoiceLineItem(employerInvoices);
				for(EmployerInvoiceLineItems lineItem: lineItems){
					lineItem.setPaidStatus(EmployerInvoiceLineItems.Status.CANCEL);
					lineItem.setLastUpdatedBy(user);
					employerInvoiceLineItemsService.saveEmployerInvoiceLineItems(lineItem);
				}
			}
		}*/
		List<Object> disInvoices = employerInvoicesService.getInvoiceToBeCancelled(employerId, employerEnrollId);
		if(disInvoices!=null && !disInvoices.isEmpty()){
			for(Object employerInvoices: disInvoices){
				employerInvoiceLineItemsService.updateCancelStatus(((Number)employerInvoices).intValue(), user);
				employerInvoicesService.updateCancelStatus(cancelReason, new TSDate(), user.getId(), ((Number)employerInvoices).intValue());
			}
		}
		else{
			LOGGER.info("===cancelInvoices(-): No active invoices found for the employer id:"+employerId+" for cancelling");
		}

		/*  }catch(Exception e){
	        	LOGGER.error("Employer Invoices Cancellation fail for employer id: "+employerId +""+e);
	        	throw new GIException(e);
	        }  */
		LOGGER.info(" cancleInvoices(-) method End =");
	}	
	
	@Override
	public int getActiveEmployerEnrollmentId(EmployerInvoices employerInvoices){
	    int activeEmployerEnrollmentId = 0;
		List<EmployerInvoiceLineItems> employerInvoiceLineItemsList = employerInvoices.getEmployerInvoiceLineItems();
		for (EmployerInvoiceLineItems employerInvoiceLineItems : employerInvoiceLineItemsList) {
			if (employerInvoiceLineItems.getIsActiveEnrollment().equals(IsActiveEnrollment.Y)) {
				activeEmployerEnrollmentId = employerInvoiceLineItems.getEmployerEnrollmentId();
				break;
			}
		}
		return activeEmployerEnrollmentId;
	}
	
	@Override
	public void triggerEnrollmentStatusChange(String paymentDueDate) throws FinanceException {
		
		LOGGER.info(" triggerEnrollmentStatusChange(-) method started");
		Map<String, Exception> exceptionDetails = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		try
		{
		Date dueDate = dateFormat.parse(paymentDueDate);
		List<EmployerInvoices> inProcessInvoiceList = employerInvoicesService.findInvoicesByIsActiveAndPaidStatusAndInvoiceTypeAndPaymentDueDate(FinanceConstants.Y, PaidStatus.IN_PROCESS, InvoiceType.BINDER_INVOICE,dueDate);
		
		if(!CollectionUtils.isEmpty(inProcessInvoiceList))
		{
			for (EmployerInvoices employerInvoices : inProcessInvoiceList) {
				
					try
					{
						updateEnrollmentStatus(employerInvoices);
					}
					catch(Exception ex)
					{
						LOGGER.error("Exception caught while triggering enrollmetn status change: " +ex);
						if(exceptionDetails==null){
							exceptionDetails = new HashMap<String, Exception>();
						}
						exceptionDetails.put(employerInvoices.getInvoiceNumber(), ex);
					}
			}

			if(exceptionDetails!=null && !exceptionDetails.isEmpty()){
				final String exceptionMessage = FinanceGeneralUtility.constructSingleMessageForExceptionDetails("EXCEPTION OCCURRED WHILE changing enrollment status for inprocess invoice:", "Invoice Number", exceptionDetails);
				throw new FinanceException(exceptionMessage);
			}
		}else{
			LOGGER.warn("No employer invoices found as paidStatus is 'IN_PROCESS' for the provided due date");
		}
		}
		catch(ParseException dateEx)
		{
			LOGGER.error("Exception caught while parsing date: " +dateEx);
			throw new FinanceException(dateEx);
		}
	}
	
	private DateTime setJodaTime(DateTime time){
		DateTime localtime = null;
		localtime = time.withMinuteOfHour(0);
		localtime = localtime.withSecondOfMinute(0);
		localtime = localtime.withMillisOfSecond(0);
		return localtime;
	}
}
