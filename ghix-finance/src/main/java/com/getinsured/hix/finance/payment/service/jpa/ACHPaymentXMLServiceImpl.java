package com.getinsured.hix.finance.payment.service.jpa;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import com.getinsured.hix.finance.payment.dto.PaymentInfo;
import com.getinsured.hix.finance.payment.dto.PaymentInfo.IndividualInformation;
import com.getinsured.hix.finance.payment.dto.StandardClassCodeType;
import com.getinsured.hix.finance.payment.service.ACHPaymentXMLService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration.FinanceConfigurationEnum;
import com.getinsured.hix.platform.util.GhixConstants;

@Component
public class ACHPaymentXMLServiceImpl implements ACHPaymentXMLService
{
	private final SimpleDateFormat dateFormat = new SimpleDateFormat(GhixConstants.FILENAME_DATE_FORMAT);
	private static final Logger LOGGER = LoggerFactory.getLogger(ACHPaymentXMLServiceImpl.class);

	@Override
	public void validateACHXMLFile(PaymentInfo paymentInfo)throws SAXException,JAXBException,IOException
	{
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.XML_NS_PREFIX);
		Schema schema = factory.newSchema(new File(saveACHPaymentXMLFile(paymentInfo)));
		Validator validator = schema.newValidator();
		validator.validate(new StreamSource(this.getClass().getClassLoader().getResourceAsStream("ACHPayment.xsd")));
	}	

	@Override
	public String saveACHPaymentXMLFile(PaymentInfo paymentInfo) throws JAXBException
	{	
		LOGGER.info("================== generateACHPaymentXML started ==========================");
		JAXBContext newcontext = JAXBContext.newInstance(PaymentInfo.class);
		Marshaller marshaller = newcontext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		//marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, );
		String outputPath = DynamicPropertiesUtil.getPropertyValue(FinanceConfigurationEnum.UPLOAD_PATH) + File.separator+"PaymentFile_"+
		dateFormat.format(new java.util.Date())+".xml";
		LOGGER.info("====================OUTPUT PATH: "+outputPath);
		//StringWriter stringWriter = new StringWriter();
		marshaller.marshal(paymentInfo,  new StreamResult(new File(outputPath)));
		//marshaller.marshal(paymentInfo,  stringWriter);
		
		return outputPath;//stringWriter.toString();
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public PaymentInfo generatePaymentInfoObject(Map<String, Object>paymentData) 
	{
		PaymentInfo aCHPaymentInfo = new PaymentInfo();
		
		aCHPaymentInfo.setCompanyId((String) paymentData.get(FinanceConstants.ACHPaymentConstants.COMPANY_ID));
		aCHPaymentInfo.setCompanyName((String) paymentData.get(FinanceConstants.ACHPaymentConstants.COMPANY_NAME));
		aCHPaymentInfo.setEffectiveEntryDate((String) paymentData.get(FinanceConstants.ACHPaymentConstants.EFFECTIVE_ENTRY_DATE));
		aCHPaymentInfo.setEntryDescription((String) paymentData.get(FinanceConstants.ACHPaymentConstants.ENTRY_DESCRIPTION));
		aCHPaymentInfo.setServiceClassCode((String)paymentData.get(FinanceConstants.ACHPaymentConstants.SERVICE_CLASS_CODE));
		aCHPaymentInfo.setStandardClassCode((StandardClassCodeType)paymentData.get(FinanceConstants.ACHPaymentConstants.STANDARD_CLASS_CODE));
		
		aCHPaymentInfo.setIndividualInformation((List<IndividualInformation>) paymentData.get(FinanceConstants.ACHPaymentConstants.INDIVIDUAL_INFO));
		
		return aCHPaymentInfo;
	}
}
