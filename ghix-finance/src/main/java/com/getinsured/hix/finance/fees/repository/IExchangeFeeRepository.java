package com.getinsured.hix.finance.fees.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.ExchangeFee;

/**
 */
public interface IExchangeFeeRepository extends JpaRepository<ExchangeFee, Integer> 
{
	/**
	 * Method findExchangeFeeByIssuerRemittanceID.
	 * @param invoiceid int
	 * @return ExchangeFee
	 */
	@Query("FROM ExchangeFee as an where an.issuerRemittance.id = :remittanceId")
	ExchangeFee findExchangeFeeByIssuerRemittanceID(@Param("remittanceId") int remittanceId);
	
}
