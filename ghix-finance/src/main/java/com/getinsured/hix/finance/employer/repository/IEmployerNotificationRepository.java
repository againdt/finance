package com.getinsured.hix.finance.employer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.EmployerNotification;

/**
 */
public interface IEmployerNotificationRepository extends JpaRepository<EmployerNotification, Integer> {

}
