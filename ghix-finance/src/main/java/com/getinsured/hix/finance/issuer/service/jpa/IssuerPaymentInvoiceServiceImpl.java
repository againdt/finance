package com.getinsured.hix.finance.issuer.service.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.finance.issuer.repository.IIssuerPaymentInvoiceRepository;
import com.getinsured.hix.finance.issuer.service.IssuerPaymentInvoiceService;
import com.getinsured.hix.model.IssuerRemittance;
import com.getinsured.hix.model.IssuerPaymentInvoice;

/**
 * This service implementation class is for GHIX-FINANCE module for issuer payment invoice.
 * This class has various methods to create, modify and query the IssuerPaymentInvoice model object. 
 *
 * @author sharma_k
 * @since 28-Jun-2013
 */
@Service("IssuerPaymentInvoiceService")
public class IssuerPaymentInvoiceServiceImpl implements IssuerPaymentInvoiceService
{
	@Autowired private IIssuerPaymentInvoiceRepository issuerPaymentInvoiceRepository;

	/**
	 * Method saveIssuerPaymentInvoice.
	 * @param issuerPaymentInvoice IssuerPaymentInvoice
	 * @return IssuerPaymentInvoice
	 * @see com.getinsured.hix.finance.issuer.service.IssuerPaymentInvoiceService#saveIssuerPaymentInvoice(IssuerPaymentInvoice)
	 */
	@Override
	@Transactional
	public IssuerPaymentInvoice saveIssuerPaymentInvoice(
			IssuerPaymentInvoice issuerPaymentInvoice) {
		return issuerPaymentInvoiceRepository.save(issuerPaymentInvoice);
	}
	
	/**
	 * Method updateIssuerPaymentInvoice.
	 * @param invoice IssuerPaymentInvoice
	 * @return IssuerPaymentInvoice
	 * @see com.getinsured.hix.finance.issuer.service.IssuerPaymentInvoiceService#updateIssuerPaymentInvoice(IssuerPaymentInvoice)
	 */
	@Override
	@Transactional
	public IssuerPaymentInvoice updateIssuerPaymentInvoice(IssuerPaymentInvoice invoice)
	{
		IssuerPaymentInvoice issuerPaymentInvoice = issuerPaymentInvoiceRepository.findOne(invoice.getId());
		issuerPaymentInvoice.setAmount(invoice.getAmount());
		issuerPaymentInvoice.setIssuerPayments(invoice.getIssuerPayments());
		return issuerPaymentInvoiceRepository.save(issuerPaymentInvoice);
	}
	
	/**
	 * Method findIssuerPaymentInvoiceByInvoiceID.
	 * @param invoiceID int
	 * @return IssuerPaymentInvoice
	 * @see com.getinsured.hix.finance.issuer.service.IssuerPaymentInvoiceService#findIssuerPaymentInvoiceByInvoiceID(int)
	 */
	@Override
	@Transactional
	public IssuerPaymentInvoice findIssuerPaymentInvoiceByInvoiceID(int issuerInvoiceID)
	{
		IssuerRemittance issuerRemittance = new IssuerRemittance();
		issuerRemittance.setId(issuerInvoiceID);
		return issuerPaymentInvoiceRepository.findByIssuerRemittance(issuerRemittance);
	}
	
}
