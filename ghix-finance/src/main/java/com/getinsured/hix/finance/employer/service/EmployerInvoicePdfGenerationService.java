package com.getinsured.hix.finance.employer.service;

import java.util.List;

import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;



/**
 */
public interface EmployerInvoicePdfGenerationService{
	
	/**
	 * Method createPdf.
	 * @throws NoticeServiceException
	 * @throws FinanceException 
	 */
	void createPdf(List<EmployerInvoices> listInvoice) throws FinanceException;
	/**
	 * Method createPdfByInvoiceId.
	 * @param invoiceid int
	 * @throws NoticeServiceException
	 * @throws FinanceException 
	 */
	void createPdfByInvoiceId(int invoiceid) throws NoticeServiceException, FinanceException;
	
	/**
	 * Method to create PDF from Pre updated EmployerInvoice and List<EmployerInvoiceLineItems>
	 * @since 18th September 2014
	 * @param employerInvoices Object<EmployerInvoices>
	 * @param employerInvLineItemsList List<Object<EmployerInvoiceLineItems>>
	 * @return String ECM_DOC_ID
	 * @throws NoticeServiceException
	 */
	String createPdfByEmployerInvoiceAndLineItems(EmployerInvoices employerInvoices,
			List<EmployerInvoiceLineItems> employerInvLineItemsList, boolean isNotificationRequired)throws NoticeServiceException;
}
