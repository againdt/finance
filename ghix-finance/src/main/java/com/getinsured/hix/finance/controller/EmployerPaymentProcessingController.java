package com.getinsured.hix.finance.controller;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.finance.FinanceRequest;
import com.getinsured.hix.dto.finance.FinanceResponse;
import com.getinsured.hix.finance.cybersource.utils.CyberSourceKeyException;
import com.getinsured.hix.finance.employer.service.EmployerInvoiceCreationService;
import com.getinsured.hix.finance.employer.service.EmployerInvoicePdfGenerationService;
import com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceErrorCodes;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.thoughtworks.xstream.XStream;

/**
 * This controller class is for GHIX-FINANCE module to generate the invoice PDF, reissue invoice and to process the employer payments.
 * 
 * @author kuldeep
 * @since 26-June-2013
 *
 */
@Controller
@RequestMapping("/employerpayment")
public class EmployerPaymentProcessingController 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployerPaymentProcessingController.class);
	
	@Autowired private EmployerInvoicePdfGenerationService employerInvoicePdfGenerationService;
	@Autowired private EmployerPaymentProcessingService employerPaymentProcessingService;
	@Autowired private EmployerInvoiceCreationService employerInvoiceCreationService;
	
	/**
	 * Create invoice PDF for parameter invoices id.
	 * @param invoiceId
	 * @return String - FinanceResponse object as String	
	 */
	@RequestMapping(value = "/createpdf/{invoiceId}", method = RequestMethod.GET)
	@ResponseBody public String createPDF(@PathVariable String invoiceId)
	{
		LOGGER.info("============ Start :: Inside createPDF "+invoiceId+ "=============");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		if (invoiceId == null || "0".equalsIgnoreCase(invoiceId)) 
		{
			financeResponse.setErrMsg(FinanceConstants.ERR_MSG_ID_CANNOT_BE_ZERO);
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		else
		{
			try
			{
				employerInvoicePdfGenerationService.createPdfByInvoiceId(Integer.parseInt(invoiceId));
				financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
			catch(Exception ex)
			{
				LOGGER.error("============ Exception :: Inside createPDF "+invoiceId+ "=============", ex);
				financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.CEPDF.getCode());
				financeResponse.setErrMsg(ex.toString());
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		LOGGER.info("============ End :: Inside createPDF "+invoiceId+ "=============");
		return xstream.toXML(financeResponse);
	}
	
	/**
	 * Reissue invoice based on the search criteria.
	 * @param searchCriteria
	 * @return String - FinanceResponse object as String
	 * @throws GIException
	 */
	@RequestMapping(value = "/reissueinvoice", method = RequestMethod.POST)
	@ResponseBody public String reissueInvoice(@RequestBody Map<String, Object> searchCriteria)throws GIException
	{
		LOGGER.info("=============== Start :: Inside reissueInvoice ==============");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		if(searchCriteria == null || searchCriteria.isEmpty())
		{
			financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		else if(searchCriteria.get(FinanceConstants.EMPLOYERID) == null || StringUtils.isEmpty(searchCriteria.get(FinanceConstants.EMPLOYERID).toString()))
		{
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			financeResponse.setErrMsg(FinanceConstants.ERR_MSG_EMPLOYER_ID_NULL);
		}
		else if(searchCriteria.get(FinanceConstants.INVOICEID) == null || StringUtils.isEmpty(searchCriteria.get(FinanceConstants.INVOICEID).toString()))
		{
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			financeResponse.setErrMsg(FinanceConstants.ERR_MSG_INVOICE_ID_NULL);
		}
		else
		{
			LOGGER.debug("Received criteria for reissueInvoice "+searchCriteria);
			try
			{
				int employerId = Integer.parseInt(searchCriteria.get(FinanceConstants.EMPLOYERID).toString());
				int invoiceId = Integer.parseInt(searchCriteria.get(FinanceConstants.INVOICEID).toString());
				financeResponse.setReissueInvoiceResponse(employerInvoiceCreationService.reissueInvoice(employerId, invoiceId));
				financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
			catch(Exception ex)
			{
				LOGGER.error("=============== Exception :: Inside reissueInvoice ==============", ex);
				financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.REINE.getCode());
				financeResponse.setErrMsg(ex.toString());
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		LOGGER.info("=============== End :: Inside reissueInvoice ==============");
		return xstream.toXML(financeResponse);
	}
	
	
	/**
	 * Process employer payment. 
	 * Communicates with cybersource(third party API).
	 * @param financeReq
	 * @return String - FinanceResponse object as String
	 */
	@RequestMapping(value = "/processpayment", method = RequestMethod.POST)
	@ResponseBody
	public String processPayment(@RequestBody FinanceRequest financeReq)
	{
		LOGGER.info("=============== Inside processPayment ==================");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		float paidAmt = 0.0f;
		int id = 0;
		PaymentMethods paymentMethods = null;

		if (financeReq != null)
		{
			id = financeReq.getId();
			paidAmt = financeReq.getPaidAmt();
			paymentMethods = financeReq.getPaymentMethods();
			if (isValidRequest(id, paidAmt, paymentMethods))
			{
				try{
					financeResponse=employerPaymentProcessingService.processPayment(id, paidAmt, paymentMethods);
					financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
				catch(CyberSourceKeyException exception){
					LOGGER.error("=============== Exception :: Inside processPayment ==============",exception);
					financeResponse.setProcessPaymentResposne(FinanceConstants.ERR_KEY_NOT_FOUND);
					financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				} catch (GIRuntimeException e) {
					LOGGER.error("=============== Exception :: Inside processPayment ==============", e);
					financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.PRPAY.getCode());
					financeResponse.setErrMsg(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
					financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
				catch (Exception e) {
					LOGGER.error("=============== Exception :: Inside processPayment ==============", e);
					financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.PRPAY.getCode());
					financeResponse.setErrMsg(e.toString());
					financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
			else
			{
				
				financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				financeResponse.setProcessPaymentResposne(getResponseMsgForEmptyRequestParams(id, paidAmt, paymentMethods));
			}
		}
		else
		{
			financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return xstream.toXML(financeResponse);
	}
	
	private boolean isValidRequest(int id, float paidAmt, PaymentMethods paymentMethods){
		boolean isValid = false;
		//if (id != 0 && !(new BigDecimal(paidAmt).compareTo(BigDecimal.ZERO)==0)  && paymentMethods != null){
		if (id != 0 && !(BigDecimal.valueOf(paidAmt).compareTo(BigDecimal.ZERO)==0)  && paymentMethods != null){
			isValid = true;
		}
		return isValid;
	}
	
	private String getResponseMsgForEmptyRequestParams(int id, float paidAmt, PaymentMethods paymentMethods){
		String message = FinanceConstants.EMPTY;
		//if(new BigDecimal(paidAmt).compareTo(BigDecimal.ZERO)==0)
		if(BigDecimal.valueOf(paidAmt).compareTo(BigDecimal.ZERO)==0)	
		{
			message = "Paid amount should not be zero";
		}else if(paymentMethods == null){
			message = "Payment method should not be empty";
		}
		else if(id == 0){
			message = "Invoice Id can not be zero";
		}
		return message;
	}
}
