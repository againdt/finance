package com.getinsured.hix.finance.cybersource.serviceimpl;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.getinsured.hix.finance.cybersource.service.ReportDownloader;
import com.getinsured.hix.finance.payment.service.jpa.URLAuthenticator;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceGeneralUtility;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.util.exception.GIException;

@Component
public class ReportDownloaderImpl extends ReportDownloader
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportDownloaderImpl.class);

	@Override
	public String downloadPaymentEventReport()throws GIException 
	{
		String pymtEventRptDownloadURL = createReportDownloadUrl();
		/*
		 * No QueryString for PaymentEventReport, so null passed as parameter.
		 */
		return downloadCyberSourceReport(null, pymtEventRptDownloadURL);
	}

	/**
	 * Casting IOException to GIException
	 */
	@Override
	public String downloadSingleTxnReport(String requestId, String merchantReferenceNumber, String reportDownloadUrl)throws GIException 
	{
		try
		{
			String queryString = prepareQueryStringForSingleTxnReport(requestId, merchantReferenceNumber);
			return downloadCyberSourceReport(queryString, reportDownloadUrl+"/Query");
		}
		catch(Exception ex)
		{
			throw new GIException(ex);
		}
	}
	
	private String downloadCyberSourceReport(String queryString, String reportDownloadUrl)throws GIException
	{
		
		OutputStream outputStream = null;
		InputStream receivedInputStream = null;
		HttpURLConnection connection = null;
		String reportResponse = StringUtils.EMPTY;
		try
		{
			if(StringUtils.isNotEmpty(reportDownloadUrl))
			{
				/*
				 * URL for downloading SingleTXNReport
				 */
				
				
				URL url = new URL(reportDownloadUrl);
				/*Authenticator.setDefault(new URLAuthenticator(DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.CYBERSOURCE_USERNAME),DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.CYBERSOURCE_PASSWORD)));*/
				Authenticator.setDefault(new URLAuthenticator(FinanceConstants.getCybersourceReportAccUserName(), FinanceConstants.getCybersourceReportAccPassword()));
				connection = (HttpURLConnection) url.openConnection(); 
				connection.setDoOutput(true); // Triggers POST.
				connection.setRequestProperty("Accept-Charset", FinanceConstants.ENCODING_UTF_8);
				connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + FinanceConstants.ENCODING_UTF_8);
				if(StringUtils.isNotEmpty(queryString))
				{
					outputStream = connection.getOutputStream();
					outputStream.write(queryString.getBytes(FinanceConstants.ENCODING_UTF_8));
				}
				
				receivedInputStream = connection.getInputStream();
				reportResponse = FinanceGeneralUtility.convertStreamToString(receivedInputStream, FinanceConstants.ENCODING_UTF_8);
				
			}
			else
			{
					throw new GIException(FinanceConstants.SingleTxnReportDownloadConstant.INVALID_URL_ERRORCODE,
							FinanceConstants.SingleTxnReportDownloadConstant.INVALID_URL_ERRORMSG, FinanceConstants.EXCEPTION_SEVERITY_HIGH);
			}
		}
		catch(Exception ex)
		{
			throw new GIException(ex);
		}
		finally
		{
			/*
			 * Closing InputStream, OutPutStream, and HTTPConnection
			 */
			try
			{
				if(outputStream != null)
				{
					outputStream.close();
				}
				if(receivedInputStream != null)
				{
					receivedInputStream.close();
				}
				if(connection != null)
				{
					connection.disconnect();
				}
			}
			catch(IOException ioe)
			{
				throw new GIException(ioe);
			}	
		}
		return reportResponse;
	}

	/*@Override
	public String downloadReportFileManually(String reportFileName) throws GIException
	{
		LOGGER.info("REPORT_DOWNLOAD:: Downloading File: "+reportFileName);
		
		File file = new File(
				DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.UPLOAD_PATH)
						+ File.separator + reportFileName);
		
		LOGGER.info("=========File Path for PaymentEventReport: "
				+ DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.UPLOAD_PATH)
				+ File.separator + reportFileName);
		FileInputStream fis = null;
		try
		{
			fis = new FileInputStream(file);
			return FinanceGeneralUtility.convertStreamToString(fis, "UTF-8");
		}
		catch(Exception ex)
		{
			LOGGER.error("Demo Payment Event Exception caught: "+ex);
			throw new GIException(ex);
		}
	}*/
	
	
	/**
	 * For SingleTxnReport Preparing QueryString
	 * Method to prepare QueryString from the passed parameter
	 * @param String requestId
	 * @param String merchantReferenceNumber
	 * @return String
	 * @throws UnsupportedEncodingException
	 */
	private String prepareQueryStringForSingleTxnReport(String requestId, String merchantReferenceNumber)throws UnsupportedEncodingException
	{
		LOGGER.info("REPORT_DOWNLOAD:: IndividualPymtRptDownload :: PrepareQueryString Received Request for RequestId: "+requestId+" MerchantReferenceNuber: "+merchantReferenceNumber);		
		
		String query = String.format("merchantID=%s&merchantReferenceNumber=%s&type=%s&subtype=%s&requestID=%s&versionNumber=%s", 
		     URLEncoder.encode(DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MERCHANT_ID), FinanceConstants.ENCODING_UTF_8), 
		     URLEncoder.encode(merchantReferenceNumber, FinanceConstants.ENCODING_UTF_8),
		     URLEncoder.encode(FinanceConstants.SINGLE_TXN_DOWNLOD_REPORT_TYPE, FinanceConstants.ENCODING_UTF_8),
		     URLEncoder.encode(FinanceConstants.SINGLE_TXN_DOWNLOD_REPORT_SUBTYPE, FinanceConstants.ENCODING_UTF_8),
		     URLEncoder.encode(requestId, FinanceConstants.ENCODING_UTF_8),
		     URLEncoder.encode(FinanceConstants.SINGLE_TXN_DOWNLOD_REPORT_VERSIONNUMBER, FinanceConstants.ENCODING_UTF_8));
		
		LOGGER.debug("REPORT_DOWNLOAD:: IndividualPymtRptDownload :: generated queryString: "+query);
		
		return query;
	}
	
	/**
	 * 
	 * @return String
	 * Create URL for downloading the PaymentEventReport from Cybersource.
	 */
	private String createReportDownloadUrl()
	{
		LOGGER.info("REPORT_DOWNLOAD:: PAYMENT_EVENT_REPORT:: STARTED - createReportDownloadUrl() method");
		//minus one day from current date since, cybersource provides previous days report
		DateTime dateTime = TSDateTime.getInstance();
		dateTime = dateTime.minusDays(1);
		LOGGER.info("PAYMENT_REPORT:: in createReportDownloadUrl");
		String url ="";
		
		/*
		 * // JIRA-HIX-27095
		 */
		StringBuilder stringBuilder = new StringBuilder(
				DynamicPropertiesUtil
						.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.PAYMENT_EVENT_REPORT_URL));
		stringBuilder.append("/DownloadReport");
		stringBuilder.append("/");

		stringBuilder.append(dateTime.toString("yyyy"));
		stringBuilder.append("/");
		
		stringBuilder.append(dateTime.toString("MM"));
		stringBuilder.append("/");
		
		stringBuilder.append(dateTime.toString("dd"));
		stringBuilder.append("/");
		
		stringBuilder.append(DynamicPropertiesUtil
						.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MERCHANT_ID));
		stringBuilder.append("/");
		stringBuilder.append("PaymentEventsReport.xml");
		url = stringBuilder.toString();
		LOGGER.info("PAYMENT_REPORT:: ==== Created URL ===="+url);
		LOGGER.info("END- saveReportFileAtAlfresco() method end with URL:"+url);
		return url;
	}
	
}
