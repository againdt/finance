package com.getinsured.hix.finance.issuer.service.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.finance.issuer.repository.IIssuerPaymentsRepository;
import com.getinsured.hix.finance.issuer.service.IssuerPaymentService;
import com.getinsured.hix.model.IssuerPayments;

/**
 * This service implementation class is for GHIX-FINANCE module for issuer payment.
 * This class has the methods to save and query the IssuerPayments model.
 * 
 * @author sharma_k
 * @since 28-Jun-2013
 */
@Service("IssuerPaymentService")
public class IssuerPaymentServiceImpl implements IssuerPaymentService 
{
	@Autowired private IIssuerPaymentsRepository issuerPaymentsRepository;

	/**
	 * Method saveIssuerPayments.
	 * @param issuerPayments IssuerPayments
	 * @return IssuerPayments
	 * @see com.getinsured.hix.finance.issuer.service.IssuerPaymentService#saveIssuerPayments(IssuerPayments)
	 */
	@Override
	public IssuerPayments saveIssuerPayments(IssuerPayments issuerPayments) {
		return issuerPaymentsRepository.save(issuerPayments);
	}

	/**
	 * Method findIssuerInvoiceNumber.
	 * @param invoiceNumber String
	 * @return IssuerPayments
	 * @see com.getinsured.hix.finance.issuer.service.IssuerPaymentService#findIssuerInvoiceNumber(String)
	 */
	@Override
	public IssuerPayments findIssuerInvoiceNumber(String invoiceNumber)
	{
		return issuerPaymentsRepository.findByInvoiceNumber(invoiceNumber);
	}

}
