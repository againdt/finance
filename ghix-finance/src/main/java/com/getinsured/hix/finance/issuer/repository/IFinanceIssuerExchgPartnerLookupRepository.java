package com.getinsured.hix.finance.issuer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.ExchgPartnerLookup;

/**
 * 
 * @author Sharma_k
 *
 */
public interface IFinanceIssuerExchgPartnerLookupRepository extends JpaRepository<ExchgPartnerLookup, Integer> 
{
	/**
	 * 
	 * @param hiosIssuerID
	 * @param market
	 * @param direction
	 * @param st01
	 * @return
	 */
	@Query("FROM ExchgPartnerLookup exchg WHERE (exchg.hiosIssuerId = :hiosIssuerID AND exchg.st01 = :st01 AND exchg.market = :market AND exchg.direction = :direction)")
	ExchgPartnerLookup findExchgPartnerByHiosIssuerID(@Param("hiosIssuerID") String hiosIssuerID,@Param("market") String market,@Param("direction") String direction, @Param("st01") String st01);

}
