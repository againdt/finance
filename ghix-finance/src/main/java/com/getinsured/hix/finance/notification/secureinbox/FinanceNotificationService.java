package com.getinsured.hix.finance.notification.secureinbox;

import java.math.BigDecimal;
import java.util.Map;

import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.EmployerNotification;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.model.EmployeeDetails;

/**
 */
public interface FinanceNotificationService {
//	String sendNotification() throws NoticeServiceException;
	void invoicePaymentDueNotification() throws NoticeServiceException, FinanceException;
	void binderInvoicePaymentDueNotification() throws NoticeServiceException, FinanceException;
	/**
	 * Method sendNotification.
	 * @param templateName String
	 * @param replaceableObject Map<String,Object>
	 * @param employerInvoices EmployerInvoices
	 * @return String
	 */
	String sendNotification(String templateName, Map<String, Object> replaceableObject, EmployerInvoices employerInvoices) throws NoticeServiceException;
	/**
	 * Method saveEmployerNotification.
	 * @param employerNotification EmployerNotification
	 * @return EmployerNotification
	 */
	EmployerNotification saveEmployerNotification(EmployerNotification employerNotification);
	
	/**
	 * 
	 * @param empInvoices
	 * @param failureReason
	 * @param nsfFlatFeeNote
	 * @param paidAmount
	 * @return
	 * @throws NoticeServiceException
	 */
	String createPaymentFailureNotice(EmployerInvoices empInvoices, String failureReason, String nsfFlatFeeNote, BigDecimal paidAmount) throws NoticeServiceException;
	
	/**
	 * This is for sending email notice to employees of employers whose due date is over
	 * @throws NoticeServiceException
	 * @throws FinanceException
	 */
	void sendEmployeeInvoiceDueNotification() throws NoticeServiceException, FinanceException;
	
	/**
	 * 
	 * @param templateData
	 * @return Notice
	 * @throws NoticeServiceException
	 */
	Notice sendNotification(Map<String, Object> templateData) throws NoticeServiceException;
	
	/**
	 * 
	 * @throws FinanceException
	 */
	void sendEmployerReminderEmailToPayBinderInvoice() throws FinanceException;
	
	/**
	 * 
	 * @throws FinanceException
	 */
    void sendNotificationToEmployerForAutopayApproachng() throws FinanceException;
	
    /**
     * 
     * @throws FinanceException
     */
	void sendNotificationForEmployerWhenAutopaymentMade() throws FinanceException;
	
    void sendEmployerReminderNotice() throws NoticeServiceException, FinanceException;
	
	void sendGroupGoodStandingNotice() throws NoticeServiceException, FinanceException;
	
	/**
	 * Method sendNotification.
	 * @param templateName String
	 * @param replaceableObject Map<String,Object>
	 * @param employeeDetails EmployeeDetails
	 * @return String
	 */
	String sendNotificationToEmployee(String templateName, Map<String, Object> replaceableObject, EmployeeDetails employeeDetails, int employeeID) throws NoticeServiceException;
	
}
