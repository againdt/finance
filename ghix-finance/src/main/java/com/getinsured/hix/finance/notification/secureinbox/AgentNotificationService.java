package com.getinsured.hix.finance.notification.secureinbox;

import com.getinsured.hix.finance.exception.FinanceException;

/**
 * 
 * Services related to Agent Notification.
 * @Since 01st December 2014
 * @author Sharma_k
 *
 */
public interface AgentNotificationService 
{
	/**
	 * HIX-55931 Consolidated Employer Payment Past Due notice for Agents
	 * service for Normal Invoice
	 * @throws FinanceException
	 */
	void agentNotificationForNormalDueInvoice() throws FinanceException;
}
