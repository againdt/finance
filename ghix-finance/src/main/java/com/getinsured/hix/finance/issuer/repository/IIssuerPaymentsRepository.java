package com.getinsured.hix.finance.issuer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.IssuerPayments;

/**
 */
public interface IIssuerPaymentsRepository extends JpaRepository<IssuerPayments, Integer>
{	
	/**
	 * Method findByInvoiceNumber.
	 * @param invoiceNumber String
	 * @return IssuerPayments
	 */
	IssuerPayments findByInvoiceNumber(String invoiceNumber);
}
