package com.getinsured.hix.finance.employer.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.EmployerInvoiceLineItems.IsActiveEnrollment;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.EmployerInvoices.InvoiceType;
import com.getinsured.hix.model.EmployerInvoices.IsManualAdjustment;
import com.getinsured.hix.model.EmployerInvoices.PaidStatus;

/**
 */
public interface EmployerInvoicesService {

	/**
	 * Method saveEmployerInvoices.
	 * @param employerInvoices EmployerInvoices
	 * @return EmployerInvoices
	 */
	EmployerInvoices saveEmployerInvoices(EmployerInvoices employerInvoices);

	/**
	 * Method findEmployerInvoicesByStatementDateAndPeriodCoveredAndEmployer.
	 * @param statementDate Date
	 * @param periodCovered String
	 * @param employerid int
	 * @return List<EmployerInvoices>
	 */
	List<EmployerInvoices> findEmployerInvoicesByStatementDateAndPeriodCoveredAndEmployer(
			Date statementDate, String periodCovered, int employerid);

	/**
	 * Method findEmployerInvoicesById.
	 * @param invoiceid int
	 * @return EmployerInvoices
	 * @throws FinanceException 
	 */
	EmployerInvoices findEmployerInvoicesById(int invoiceid);

	/**
	 * Method searchEmployerInvoices.
	 * @param searchCriteria Map<String,Object>
	 * @return Map<String,Object>
	 */
	Map<String, Object> searchEmployerInvoices(
			Map<String, Object> searchCriteria) throws FinanceException;

	/**
	 * Method searchNotPaidEmployerInvoices.
	 * @param searchCriteria Map<String,Object>
	 * @return Map<String,Object>
	 */
	//Map<String, Object> searchNotPaidEmployerInvoices(Map<String, Object> searchCriteria) throws FinanceException;

	/**
	 * Method findEmployerInvoicesWhereInvoiceNotCreated.
	 * @return List<EmployerInvoices>
	 */
	List<EmployerInvoices> findEmployerInvoicesWhereInvoiceNotCreated();
	
	/**
	 * Method searchPaidEmployerInvoices.
	 * @param searchCriteria Map<String,Object>
	 * @return Map<String,Object>
	 */
	//Map<String, Object> searchPaidEmployerInvoices(Map<String, Object> searchCriteria) throws FinanceException;
	
	/**
	 * 
	 * @param employer
	 * @param invoicetype
	 * @param isActive
	 * @return
	 */
	EmployerInvoices findEmployerInvoicesByEmployerAndInvoiceType(Employer employer, EmployerInvoices.InvoiceType invoicetype, String isActive);
	
	/**
	 * Method updateInvoiceNumber.
	 * @param invoiceNumber String
	 * @param invoiceid int
	 * @return EmployerInvoices
	 */
	EmployerInvoices updateInvoiceNumber(String invoiceNumber, int invoiceid);
	
	/**
	 * Method findPreviousInvoice.
	 * @param employerid int
	 * @return EmployerInvoices
	 */
    EmployerInvoices findLastPaidInvoice( int employerid);
	
	/**
	 * Method findLastPaidInvoiceByEnrollId.
	 * @param employerEnrollmentId int
	 * @return EmployerInvoices
	 *//*
    EmployerInvoices findLastPaidInvoices(int employerEnrollmentId);*/
    
	/**
	 * Method findLastInvoice.
	 * @param employerid int
	 * @return EmployerInvoices
	 */
	EmployerInvoices findLastActiveInvoice( int employerid);
	
	/**
	 * Method findPreviousInvoiceForReissue.
	 * @param employerid int
	 * @param invoiceid int
	 * @return EmployerInvoices
	 */
	EmployerInvoices findLastInvoiceForReissue(int employerid,int invoiceid);
	
	/**
	 * Method findDueEmeployerInvoices.
	 * @param isActive String
	 * @param paymentDueDate Date
	 * @return List<EmployerInvoices>
	 * @throws FinanceException 
	 */
	List<EmployerInvoices> findDueEmeployerInvoices(String isActive, Date paymentDueDate);
	
	/**
	 * Method getBinderInvoicePaidStatusOfEmployer.
	 * @param employerid int
	 * @return EmployerInvoices.PaidStatus
	 */
	EmployerInvoices.PaidStatus getBinderInvoicePaidStatusOfEmployer(int employerid);
	
	/**
	 * Added to return Map constains list of invoice based on IsActive 'Y' | 'N'
	 * add the criteria ("ISACTIVE", "Value");key:value pair  
	
	 * @param searchCriteria Map<String,Object>
	 * @return Map<String,Object>
	 * @throws FinanceException 
	 */
	Map<String, Object> searchEmpInvoicesByIsActive(Map<String, Object> searchCriteria) throws FinanceException;
	
	/**
	 * Method disEnrollEmployers.
	 * @return List<EmployerInvoices>
	 */
	List<EmployerInvoices> disEnrollEmployers(String isActive, InvoiceType invoiceType,Date currentDate);
	
	/**
	 * Method updateECMDocId.
	 * @param ecmDocId String
	 * @param invoiceId int
	 * @param userId int
	 * @return EmployerInvoices
	 */
	EmployerInvoices updateECMDocId(String ecmDocId, int invoiceId, int userId);
	
	/**
	 * 
	 * @param isActive
	 * @param paymentDueDate
	 * @param invoiceType
	  * @return EmployerInvoices whose invoiceType 'BINDER_INVOICE' or 'NORMAL' and isActive 'Y' */
	List<EmployerInvoices> findDueEmployerInvoices(String isActive, EmployerInvoices.InvoiceType invoiceType, Date currentDate);
	
	EmployerInvoices getGivenPeriodCoveredEmployerInvoice(String periodCovered, int employerId);
	
	/**
	 * 
	 * @param issuerInvoicesGeneratedFlag
	 * @return
	 */
//	EmployerInvoices updateIssuerInvoicesGeneratedFlag(int invoiceId, IssuerInvoicesGeneratedFlag issuerInvoicesGeneratedFlag);
	
	/**
	 * 
	 * @param isActive
	 * @param paymentDueDate
	 * @param invoiceType
	 * @param invoiceType
	  * @return EmployerInvoices whose invoiceType 'BINDER_INVOICE' or 'NORMAL' and isActive 'Y' and paymentDueDate */
	List<EmployerInvoices> findDueEmployerInvoicesForAutoPay(String isActive, Date paymentDueDate, EmployerInvoices.InvoiceType invoiceType);

	/**
	 * 
	 * @param invoiceNumber
	 * @param employerId
	 * @return EmployerInvoices 
	 */
	EmployerInvoices findPreviousActiveDueInProcessInvoice(int employerId);
	
	/**
	 * 
	 * @param employerId
	 * @param isActive
	 * @return List<EmployerInvoices>
	 */
	List<EmployerInvoices> findByEmployerAndIsActive(int employerId, String isActive);
	
	/**
	 * 
	 * @param paidStatus
	 * @param paidDate
	 * @return
	 */
	List<EmployerInvoices> findInProcessInvoiceForReconcilation(EmployerInvoices.PaidStatus paidStatus, Date paidDate);
	
	/**
	 * 
	 * @param employerId
	 * @return
	 */
	List<EmployerInvoices> findActiveDueInvoicesByEmpIdNInvType(int employerId,  EmployerInvoices.InvoiceType invoiceType);
	
	/**
	 * 
	 * @param employerId
	 * @param statementDate
	 * @return EmployerInvoices
	 */
	EmployerInvoices findPreviousInvoice(int employerId, Date statementDate);
	
	/**
	 * 
	 * @param employerId
	 * @param invoicetype
	 * @param isActive
	 * @param periodCovered
	 * @return
	 */
	EmployerInvoices findInvoiceByTypeAndPeriodCovered(int employerId, EmployerInvoices.InvoiceType invoicetype, String isActive, String periodCovered);
	
	/**
	 * 
	 * @param employerId
	 * @param employerEnrollmentId
	 * @return
	 */
	List<Object[]> findEmployerInvoiceForPaidInvoice(int employerId, int employerEnrollmentId);
	
	/**
	 * 
	 * @param searchCriteria
	 * @return
	 * @throws FinanceException
	 */
	//Map<String, Object> searchPaymentHistory(Map<String, Object> searchCriteria) throws FinanceException;
	
	/**
	 * Method findInvoiceByEmployerIdNIsAcitveNIsActiveEmpEnrollId.
	 * @param employerId int
	 * @param isActiveEnrollment IsActiveEnrollment
	 * @param isActive String
	 * @param employerEnrollmentId int
	 * @return List<EmployerInvoices>
	 */
	List<EmployerInvoices> findInvoiceByEmployerIdNIsAcitveNIsActiveEmpEnrollId(int employerId, IsActiveEnrollment isActiveEnrollment, String isActive, int employerEnrollmentId);
	
	/**
	 * Method findPeriodCoveredAndInvoiceCreationDate.
	 * @param employerEnrollmentId int
	 * @return
	 */
	Object findPeriodCoveredAndInvoiceCreationDate(int employerEnrollmentId);
	
	/**
	 * Method to get the count of Employer Invoice in Grace Period
	 * @param employerId int EmployerID
	 * @param employerEnrollmentId int EmployerEnrollmentID
	 * @param currentDate Date Current System date
	 * @return long
	 */
	long getCountNoOfInvoiceInGracePeriod(int employerId, int employerEnrollmentId, Date currentDate);
	
	/**
	 * returns list of active DUE/IN_PROCESS invoices except 'invoiceNumber' invoice and which are less than current invoiceNumber's statementDate
	 * @param employer
	 * @param isActive
	 * @param invoiceNumber
	 * @param paidStatus
	 * @param statementDate
	 * @return 
	 */
	List<EmployerInvoices> findActiveInvoicesBeforeCurrentInvoiceDateAndStatusIn(Employer employer, String isActive, String invoiceNumber, List<PaidStatus> paidStatus, Date statementDate);
	
	/**
	 * returns list of active negative(-ve) DUE EmployerInvoices 
	 * @param isActive 'Y'
	 * @param paidStatus EmployerInvoices.PaidStatus.DUE
	 * @param totalAmountDue BigDecimal.ZERO to check totalAmountDue < 0.
	 * @return List<EmployerInvoices>
	 */
    List<EmployerInvoices> findActiveNegativeDueInvoices(String isActive, PaidStatus paidStatus, BigDecimal totalAmountDue);
		
    /**
	 * Method searchEmployerInvoicesByECMIdNEmployerId.
	 * @param employerId
     * @param ecmDocId 
	 * @return EmployerInvoices
	 */
    EmployerInvoices searchEmployerInvoicesByECMIdNEmployerId(int employerId, String ecmDocId);
    
    /**
     * @since 26th August 2014
     * Counts the no of Binder invoice exists for a given EmployerEnrollment, Count may vary for re-issued EmployerInvoice
     * This service call is required to verify whether we billed BinderInvoice for a EmployerEnrollment or not.
     * In given time there can be only one Active Binder_Invoice needs to generated for a new Employer Enrollment.
     * @param employerID
     * @param employerEnrollmentID
     * @return
     */
    long noOfBinderInvoiceForEmployerEnrollment(int employerID, int employerEnrollmentID);
    
    
    /**
     * To verify invoice is Active and DUE before reissuing that invoice, Same flow is restricted from UI
     * this service call is just secondary level check.
     * @param invoiceID
     * @param isActive
     * @param paidStatus
     * @return
     */
    EmployerInvoices findEmployerInvoiceByIdAndPaidStatusAndIsActiveAndIsAdjusted(int invoiceID, PaidStatus paidStatus, String isActive, IsManualAdjustment isAdjusted);
    
    /**
     * 
     * @param employer
     * @param isActive
     * @param paidStatus
     * @param invoiceType
     * @return
     */
    List<EmployerInvoices> findInvoicesByEmployerAndIsActiveAndPaidStatusInAndAndInvoiceTypeIn(Employer employer, String isActive, List<PaidStatus> paidStatus, List<InvoiceType> invoiceType);
    
    List<EmployerInvoices> findInvoicesByIsActiveAndPaidStatusAndInvoiceTypeAndPaymentDueDate(String isActive, PaidStatus paidStatus, InvoiceType invoiceType, Date paymentDueDate);
    
    List<EmployerInvoices> findInprocessInvoicesWhenAutoPayHasDone(String isActive, InvoiceType invoiceType, Date autoPayDate);
    
    List<EmployerInvoices> findPositiveDueEmployerInvoicesForAutoPayNotice(String isActive, InvoiceType invoiceType, Date paymentDueDate, BigDecimal zero);
    
    /**
	 * Method findByInvoiceTypePDFNotCreated.
	 * @return List<EmployerInvoices>
	 */
	List<EmployerInvoices> findByInvoiceTypePDFNotCreated(InvoiceType invoiceType);
	
	/**
	 * Method findCancelledInvoiceForTermiantedReason.
	 * @param terminatedReason String
	 * @param paymentDueDate Date
	 * @param currentDate Date
	 * @return List<EmployerInvoices>
	 * @throws FinanceException 
	 */
	List<EmployerInvoices> findCancelledInvoiceForTermiantedReason(String terminatedReason, EmployerInvoices.InvoiceType invoiceType, Date paymentDueDate, Date currentDate);
	
	List<EmployerInvoices> findDueEmployerInvoicesForNotification(String isActive,EmployerInvoices.InvoiceType invoiceType, Date paymentDueDate);
	
	List<EmployerInvoices> findEmployerInvoicesBasedOnPaymentDateAndSettlementDate(EmployerInvoices.InvoiceType invoiceType, Date settlementDate);
	
	List<Object> getInvoiceToBeCancelled(int  employerId, int employerEnrollId);
	
	int updateCancelStatus(String cancelReason,Date terminationDate,int userId,int invoiceId);
}
