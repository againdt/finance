package com.getinsured.hix.finance.saml.utils;

import java.io.ByteArrayInputStream;
import java.security.Key;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xml.security.keys.keyresolver.KeyResolverException;
import org.apache.xml.security.signature.XMLSignature;
import org.opensaml.common.SignableSAMLObject;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.io.Unmarshaller;
import org.opensaml.xml.io.UnmarshallingException;
import org.opensaml.xml.security.SecurityHelper;
import org.opensaml.xml.security.keyinfo.KeyInfoHelper;
import org.opensaml.xml.security.x509.BasicX509Credential;
import org.opensaml.xml.signature.KeyInfo;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.SignatureValidator;
import org.opensaml.xml.signature.impl.SignatureImpl;
import org.opensaml.xml.util.Base64;
import org.opensaml.xml.validation.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Help to Decode the SAML2.0 pay load response
 * @author Sahoo_s
 * @since 21 August 2014
 */
public final class EmployeePaymentSAMLDecoder {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeePaymentSAMLDecoder.class);
	
	private static final String ASSERTION_NSPACE = "urn:oasis:names:tc:SAML:2.0:assertion";
	private static final String PROTOCOL_NSPACE = "urn:oasis:names:tc:SAML:2.0:protocol";
	
	private static final String ASSERTION = "Assertion";
	private static final String REQUEST = "Request";
	private static final String RESPONSE = "Response";
	
	private static boolean responseFlag = false;
	private static boolean assertionFlag = false;
	
	private static X509Certificate x509Certificate;
	
	private EmployeePaymentSAMLDecoder()
	{
		
	}
	
	/**
	 * Returns the decoded SAML string which 
	 * will passed to the Carrier for Payment.
	 * @param samlEncodedResponse
	 * 					{@link String}
	 * @return isValidated
	 * 					{@link Boolean}
	 */
	public static boolean generateDecodedSAML(String samlEncodedResponse){
		
		boolean isValidated = false;
		Document document = buildDocumentResponse(samlEncodedResponse);
		
		NodeList nodeList = getRequestElements(document);
		/*for (int i = 0; i < nodeList.getLength(); i++) {
			requestFlag = verifySignature((Element) nodeList.item(i));
		}*/
		
		nodeList = getResponseElements(document);
		for (int i = 0; i < nodeList.getLength(); i++) {
			responseFlag = verifySignature((Element) nodeList.item(i));
		}
		
		nodeList = getAssertionElements(document);
		for (int i = 0; i < nodeList.getLength(); i++) {
			assertionFlag = verifySignature((Element) nodeList.item(i));
		}
		
		if (responseFlag && assertionFlag) {
			isValidated = true;
		}
		
		return isValidated;
	}

	/**
	 * Build Response Document XML Object
	 * to the Carrier for Payment.
	 * @param samlEncodedResponse
	 * 					{@link String}
	 * @return document
	 * 					{@link Document}
	 */
	public static Document buildDocumentResponse(String samlEncodedResponse){
		Document document = null;
		try {
			byte[] base64DecodedResponse = Base64.decode(samlEncodedResponse);
			ByteArrayInputStream is = new ByteArrayInputStream(base64DecodedResponse);

			DocumentBuilderFactory documentBuilderFactory = GhixUtils.getDocumentBuilderFactoryInstance();
			documentBuilderFactory.setNamespaceAware(true);
			DocumentBuilder docBuilder = documentBuilderFactory.newDocumentBuilder();

			document = docBuilder.parse(is);
		}catch (Exception e) {
			LOGGER.info("Not able to decode the SAML Response.", e);
		}
		return document;
	}
	
	/**
	 * Return the XML Node value with Request
	 * @param document
	 * 					{@link Document}
	 * @return requestNodeList
	 * 					{@link NodeList}
	 */
	private static NodeList getRequestElements(Document document) {
		return document.getElementsByTagNameNS(PROTOCOL_NSPACE, REQUEST);
	}
	
	/**
	 * Return the XML Node value with Response
	 * @param document
	 * 					{@link Document}
	 * @return requestNodeList
	 * 					{@link NodeList}
	 */
	private static NodeList getResponseElements(Document document) {
		return document.getElementsByTagNameNS(PROTOCOL_NSPACE, RESPONSE);
	}

	/**
	 * Return the XML Node value with Assertion
	 * @param document
	 * 					{@link Document}
	 * @return requestNodeList
	 * 					{@link NodeList}
	 */
	private static NodeList getAssertionElements(Document document) {
		return document.getElementsByTagNameNS(ASSERTION_NSPACE, ASSERTION);
	}
	
	/**
	 * Verify the Signature present in 
	 * SAML2.0 with Security key files.
	 * @param signature
	 * 					{@link Signature}
	 * @return true/false
	 * 					{@link Boolean}
	 * @throws CertificateException 
	 * @throws ValidationException 
	 * @throws KeyResolverException 
	 * @throws GIException 
	 */
	private static boolean verifySignature(Signature signature) throws CertificateException {
		BasicX509Credential signatureCredential = null; 
		KeyInfo keyInfo = signature.getKeyInfo();
		if (x509Certificate != null) {
			signatureCredential = SecurityHelper.getSimpleCredential(x509Certificate, null);
		}else if (keyInfo != null) {
				List<X509Certificate> x509certificates = KeyInfoHelper.getCertificates(keyInfo);
				if (x509certificates != null && !x509certificates.isEmpty()) {
					signatureCredential = SecurityHelper.getSimpleCredential(x509certificates.get(0), null);			
				}
		}
		try {
			if (signatureCredential == null) {
				throw new IllegalArgumentException("No X.509 certificate to verify signature.");
			}
			SignatureValidator signatureValidator = new SignatureValidator(signatureCredential);
			Key validationKey = SecurityHelper.extractVerificationKey(signatureCredential);
			LOGGER.info("Validation credential key algorithm '{}', key instance class '{}'" + 
	                validationKey.getAlgorithm()+ " "+ validationKey.getClass().getName());
			
			XMLSignature xmlSig = ((SignatureImpl) signature).getXMLSignature();
			xmlSig.getKeyInfo().getX509Certificate();
			signatureValidator.validate(signature);
		}
		
		catch (Exception exception) {
			LOGGER.info("Exception occured while verifying security signature details.", exception);
			return false;
		}
		return true;
	}
	
	/**
	 * Verify the Signature present in 
	 * SAML2.0 with Security key files.
	 * @param samlElement
	 * 					{@link Element}
	 * @return true/false
	 * 					{@link Boolean}
	 */
	public static boolean verifySignature(Element samlElement) {
		try {
			XMLObject xmlObject = unmarshall(samlElement);
			
			if (!(xmlObject instanceof SignableSAMLObject)) {
				LOGGER.info("Cannot verify signature on this object type: %1$s", xmlObject.getClass().getName());
				SignableSAMLObject signableSAMLObject = (SignableSAMLObject) xmlObject;
				
				if (!signableSAMLObject.isSigned()) {
					LOGGER.info("The SAML element %1$s is not signed.", samlElement.getLocalName());
					return false;
				}
				LOGGER.info("Verifying signature on %1$s", samlElement.getLocalName());
				boolean verified = verifySignature(signableSAMLObject.getSignature());
				
				LOGGER.info("Signature on %1$s verified: %2$b", samlElement.getLocalName(), verified);
			}
		}
		
		catch (Exception exception) {
			LOGGER.info("Cannot Validate the Security certificate details : 1$s", exception);
			return false;
		}
		return true;
	}
	
	/**
	 * UnMarshall the SAML2.0 element
	 * @param samlElement
	 * 					{@link Element}
	 * @return xmlObject
	 * 					{@link XMLObject}
	 * @throws UnmarshallingException 
	 */
	private static XMLObject unmarshall(Element samlElement) throws UnmarshallingException {
        Unmarshaller unmarshaller = Configuration.getUnmarshallerFactory().getUnmarshaller(samlElement);
        
        if (unmarshaller == null) {
        	throw new IllegalArgumentException("Failed to unmarshall XML to SAML object.");
        }
		
		return unmarshaller.unmarshall(samlElement);
	}

}
