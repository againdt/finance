package com.getinsured.hix.finance.cybersource.serviceimpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;





import com.getinsured.hix.finance.cybersource.service.PaymentProcessor;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 *  Handles e cheque processing requests. 
 * 
 * @author Sharma_k
 * @since 24-Sep-2013
 */
@Component
public  class ECheckProcessor implements PaymentProcessor {

	@Autowired private OneTimeECheck oneTimeEC;
	
	/**
	 * Method debit
	 * @param pCust Map
	 * @param amount String
	 * @return Map
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map debit(Map pCust, String amount) {
			return oneTimeEC.debitAccount(pCust, amount);
	}

	/**
	 * Method credit
	 * @param pCust Map
	 * @param amount String
	 * @return Map
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map credit(Map pCust, String amount) {
            return oneTimeEC.creditAccount(pCust, amount);
	}

	/**
	 * Method authorize
	 * @param pCust Map
	 * @param amount String
	 * @return Map
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map authorize(Map pCust, String amount) {
	
		return oneTimeEC.authenticateAccount(pCust);
	}

	/**
	 * Method pciAuthorize
	 * @param pCust Map
	 * @param amount String
	 * @return Map
	 * @throws GIException
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map pciAuthorize(Map pCust, String amount) throws GIException {
		return oneTimeEC.pciAuthenticateAccount(pCust);
	}

	/**
	 * Method pciCredit
	 * @param pCust Map
	 * @param amount String
	 * @return Map
	 * @throws GIException
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map pciCredit(Map pCust, String amount) throws GIException {
		return oneTimeEC.pciCreditAccount(pCust, amount);
	}

	/**
	 * Method pciDebit
	 * @param pCust Map
	 * @param amount String
	 * @return Map
	 * @throws GIException
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map pciDebit(Map pCust, String amount) throws GIException {
		return oneTimeEC.pciDebitAccount(pCust, amount);
	}
}
