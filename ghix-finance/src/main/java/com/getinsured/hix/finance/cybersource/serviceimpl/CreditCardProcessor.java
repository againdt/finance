package com.getinsured.hix.finance.cybersource.serviceimpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.finance.cybersource.service.PaymentProcessor;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Handles credit card processing requests.
 * @author panda_p
 * @since 20-Nov-2012
 */
@Component
public  class CreditCardProcessor implements PaymentProcessor {

	@Autowired private OneTimeCreditCard oneTimeCC;
	
	/**
	 * Method debit
	 * @param pCust Map
	 * @param amount String
	 * @return Map
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map debit(Map pCust, String amount) {
		
		Map authReply = oneTimeCC.authorizeCard(pCust, amount);
		
		if ("ACCEPT".equalsIgnoreCase(authReply.get("decision").toString()))
        {
			String merchantCode = (String)pCust.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY);
            return oneTimeCC.captureCard(authReply.get("requestID").toString(), amount, merchantCode);
        }
		else{
			return authReply;
		}
		
	}

	/**
	 * Method credit
	 * @param pCust Map
	 * @param amount String
	 * @return Map
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map credit(Map pCust, String amount) {
            return oneTimeCC.creditToCard(pCust, amount);
	}

	/**
	 * Method authorize
	 * @param pCust Map
	 * @param amount String
	 * @return Map
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map authorize(Map pCust, String amount) {
		System.out.println("validateCreditCard6");
		return oneTimeCC.authorizeCard(pCust, amount);
	}

	/**
	 * Method pciAuthorize
	 * @param pCust Map
	 * @param amount String
	 * @return Map
	 * @throws GIException
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map pciAuthorize(Map pCust, String amount) throws GIException 
	{
		return oneTimeCC.pciAuthorizeCard(pCust, amount);
	}

	/**
	 * Method pciCredit
	 * @param pCust Map
	 * @param amount String
	 * @return Map
	 * @throws GIException 
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map pciCredit(Map pCust, String amount) throws GIException {
		return oneTimeCC.pciCreditToCard(pCust, amount);
	}

	/**
	 * Method pciDebit
	 * @param pCust Map
	 * @param amount String
	 * @return Map
	 * @throws GIException 
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map pciDebit(Map pCust, String amount) throws GIException {
		Map authReply = oneTimeCC.pciAuthorizeCard(pCust, amount);
		
		if ("ACCEPT".equalsIgnoreCase(authReply.get("decision").toString()))
        {
			String merchantCode = (String)pCust.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY);
            return oneTimeCC.pciCaptureCard(authReply.get("requestID").toString(), amount, merchantCode);
        }
		else{
			return authReply;
		}
	}
}
