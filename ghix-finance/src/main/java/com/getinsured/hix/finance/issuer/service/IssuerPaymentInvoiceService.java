package com.getinsured.hix.finance.issuer.service;

import com.getinsured.hix.model.IssuerPaymentInvoice;

public interface IssuerPaymentInvoiceService
{
	IssuerPaymentInvoice saveIssuerPaymentInvoice(IssuerPaymentInvoice issuerPaymentInvoice);
	
	IssuerPaymentInvoice updateIssuerPaymentInvoice(IssuerPaymentInvoice invoice);
	
	IssuerPaymentInvoice findIssuerPaymentInvoiceByInvoiceID(int invoiceID);

}
