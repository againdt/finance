package com.getinsured.hix.finance.employer.service;

import java.util.List;

import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.EmployerPaymentInvoice;
import com.getinsured.hix.model.EmployerPaymentInvoice.IsActive;

/**
 */
public interface EmployerPaymentInvoiceService {

	/**
	 * Method saveEmployerPaymentInvoice.
	 * @param employerPaymentInvoice EmployerPaymentInvoice
	 * @return EmployerPaymentInvoice
	 */
	EmployerPaymentInvoice saveEmployerPaymentInvoice(EmployerPaymentInvoice employerPaymentInvoice);
	
	/**
	 * Method findEmployerPaymentInvoiceById.
	 * @param paymentid int
	 * @return EmployerPaymentInvoice
	 * @throws FinanceException 
	 */
	EmployerPaymentInvoice findEmployerPaymentInvoiceById(int paymentid) throws FinanceException;
	
	/**
	 * Method findPaymentInvoiceByEmployerInvoices.
	 * @param employerInvoices EmployerInvoices
	 * @return List<EmployerPaymentInvoice>
	 */
	List<EmployerPaymentInvoice> findPaymentInvoiceByEmployerInvoices(EmployerInvoices employerInvoices);
	
	
	/**
	 * 
	 * @param employerPaymentInvId
	 * @param isActive
	 * @return
	 */
	EmployerPaymentInvoice updateEmployerPaymentInvoice(int employerPaymentInvId, IsActive isActive);
	
	/**
	 * 
	 * @param employerInvoices
	 * @return
	 */
	EmployerPaymentInvoice findActiveEmployerPaymentInvoice(EmployerInvoices employerInvoices);
	
}
