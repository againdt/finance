package com.getinsured.hix.finance.cybersource.mock.serviceimpl;


import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.finance.payment.report.singletxn.Report;
import com.getinsured.hix.finance.cybersource.service.ReportDownloader;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @since 04th February
 * @author Sharma_k
 *
 */
@Component
public class ReportDownloaderMockImpl extends ReportDownloader
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportDownloaderMockImpl.class);

	@Override
	public String downloadSingleTxnReport(String requestId,
			String merchantReferenceNumber, String reportDownloadUrl)
			throws GIException 
	{
		LOGGER.info("Received SingleTxnReport Mock Request for MerchantRefNo: "+merchantReferenceNumber+" RequestID: "+requestId);
		String singleTxnReportResponse =  downloadReportFileManually(FinanceConstants.DEFAULT_SINGLE_PYMTTXN_REPORT);
		StringWriter sw = new StringWriter();
		try
		{
			JAXBContext jc = JAXBContext.newInstance(Report.class);
			
			//Unmarshaller unmarshaller = jc.createUnmarshaller();
			Report  singleTxnReport = (Report) GhixUtils.inputStreamToObject(IOUtils.toInputStream(singleTxnReportResponse, FinanceConstants.ENCODING_UTF_8), Report.class);
			//Report  singleTxnReport = (Report)unmarshaller.unmarshal(new StringReader(singleTxnReportResponse));
			
			for(Report.Requests requests : singleTxnReport.getRequests())
			{
				for(Report.Requests.Request request : requests.getRequest())
				{
					request.setMerchantReferenceNumber(merchantReferenceNumber);
					request.setRequestID(requestId);
				}
			}
			
			/*
			 *Marshalling the edited report back to XML response 
			 */
			Marshaller marshaller = jc.createMarshaller();
			marshaller.marshal(singleTxnReport, sw);
		}
		catch(JAXBException jax)
		{
			throw new GIException(jax);
		}
		catch(Exception e)
		{
			throw new GIException(e);
		}
		return sw.toString();
	}

	
	@Override
	public String downloadPaymentEventReport() throws GIException {
		// TODO Auto-generated method stub
		return null;
	}

	/*@Override
	public String downloadReportFileManually(
			String reportFileName) throws GIException 
	{
		LOGGER.info("REPORT_DOWNLOAD:: Downloading File: "+reportFileName);
		
		File file = new File(
				DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.UPLOAD_PATH)
						+ File.separator + reportFileName);
		
		LOGGER.info("=========File Path for PaymentEventReport: "
				+ DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.UPLOAD_PATH)
				+ File.separator + reportFileName);
		FileInputStream fis = null;
		try
		{
			fis = new FileInputStream(file);
			return FinanceGeneralUtility.convertStreamToString(fis, "UTF-8");
		}
		catch(Exception ex)
		{
			LOGGER.error("Demo Payment Event Exception caught: "+ex);
			throw new GIException(ex);
		}
	}*/

}
