package com.getinsured.hix.finance.logger;

import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a logger factory class, this class returns the logger as per log configuration. 
 * @author khimls_s
 * @since 22-Aug-2014
 */

public final class GHIXLoggerFactory{
	private GHIXLoggerFactory()
	{
		
	}
	
	
	/**
	* Return a logger named corresponding to the class passed as parameter, using
	* the statically bound {@link ILoggerFactory} instance.
	*
	* @param clazz the returned logger will be named after clazz
	* @return logger GHIXLogger
	*/
	public static Logger getLogger(Class<?> clazz) {
		return getLogger(clazz.getName());		
	}
	
	/**
	* Return a logger named according to the name parameter using the statically
	* bound {@link ILoggerFactory} instance.
	* 
	* @param name The name of the logger.
	* @return logger
	*/
	public static Logger getLogger(String name) {
	    ILoggerFactory iLoggerFactory = LoggerFactory.getILoggerFactory();	   
	    return iLoggerFactory.getLogger(name);	     
	}	
	
}
