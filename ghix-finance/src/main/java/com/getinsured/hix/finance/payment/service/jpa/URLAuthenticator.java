package com.getinsured.hix.finance.payment.service.jpa;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is for GHIX-FINANCE module for authenticating URL and password.
 * 
 * @author Sharma_k
 * @since 22-Oct-2013 
 */
public class URLAuthenticator extends Authenticator
{
	private String username;
	private String password;
	private static final Logger LOGGER = LoggerFactory.getLogger(URLAuthenticator.class);
	public URLAuthenticator(String userId, String password)
	{
		this.username = userId;
		this.password = password;
	}
	
	/**
	 * Method getPasswordAuthentication. 
	 * @return PasswordAuthentication 
	 */
	@Override
	protected PasswordAuthentication getPasswordAuthentication()
	{
		LOGGER.info("================= In getPasswordAuthentication() ======================");
	    LOGGER.info("PAYMENT_REPORT: RequestingPrompt: "+getRequestingPrompt() + "  RequestingHost: " +getRequestingHost()+ " RequestingSite: "+getRequestingSite());
	    return new PasswordAuthentication(username, password.toCharArray());
	  }
}
