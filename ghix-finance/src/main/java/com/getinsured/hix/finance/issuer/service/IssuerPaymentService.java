package com.getinsured.hix.finance.issuer.service;

import com.getinsured.hix.model.IssuerPayments;

public interface IssuerPaymentService {
	IssuerPayments saveIssuerPayments(IssuerPayments issuerPayments);
	
	IssuerPayments findIssuerInvoiceNumber(String invoiceNumber);
}
