package com.getinsured.hix.finance.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.finance.FinanceResponse;
import com.getinsured.hix.finance.payment.service.PaymentEventLogService;
import com.getinsured.hix.finance.payment.service.PaymentMonitorReportService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceErrorCodes;
import com.getinsured.hix.model.PaymentEventLog;
import com.getinsured.hix.platform.util.GhixUtils;
import com.thoughtworks.xstream.XStream;

/**
 * 
 * @author reddy_h
 *
 */
@Controller
@RequestMapping("/finance")
public class PaymentMonitorController {

	@Autowired
	private PaymentMonitorReportService paymentMonitorReportService;
	@Autowired
	private PaymentEventLogService paymentEventLogService;

	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentMonitorController.class);
	
	@RequestMapping(value = "/findpaymenteventlogbycriteria", method = RequestMethod.POST)
	@ResponseBody
	public String getPaymentStatusDetails(@RequestBody Map<String, Object> searchCriteria){
		
		LOGGER.info("=======Request Received for getPaymentStatusDetails(-) method : START=====");
		FinanceResponse response = new FinanceResponse();
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		if(searchCriteria == null || searchCriteria.isEmpty()){
			
			response.setStatus(FinanceConstants.FAILURE);
			response.setErrMsg(FinanceConstants.EMPTY_REQUEST);
		}else{
			
			try{
				Map<String, Object> paymentEventRecords = paymentMonitorReportService.getPaymentReportDetails(searchCriteria);
				response.setDataObject(paymentEventRecords);
				response.setStatus(FinanceConstants.SUCCESS);
			}
			catch(Exception exception){
				LOGGER.error("Error in getPaymentStatusDetails(-) method", exception);
				response.setStatus(FinanceConstants.FAILURE);
				response.setErrMsg(exception.toString());
				response.setErrCode(FinanceErrorCodes.ERRORCODE.SPADE.getCode());
			}
		}
		LOGGER.info("=======Request Received for getPaymentStatusDetails(-) method : END=====");
		return xstream.toXML(response);
	}
	
	@RequestMapping(value = "/findpaymenteventlogbymerchantrefno", method = RequestMethod.POST)
	@ResponseBody
	public String findPaymentEventLogByMerchantRefNo(@RequestBody String merchantRefNo){
		
		LOGGER.info("=======Request Received for findPaymentEventLogByMerchantRefNo(-) method======");
		FinanceResponse response = new FinanceResponse();
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		if(StringUtils.isEmpty(merchantRefNo)){
			response.setStatus(FinanceConstants.FAILURE);
			response.setErrMsg(FinanceConstants.EMPTY_REQUEST);
		}else{
		
			try{
				List<PaymentEventLog> paymentEventLogList = paymentEventLogService.getPymtEventLogByMerchantRefNo(merchantRefNo);
				Map<String, Object> paymentEventRecords = new HashMap<String, Object> ();
				paymentEventRecords.put("paymentEventLogList", paymentEventLogList);
				response.setDataObject(paymentEventRecords);
				response.setStatus(FinanceConstants.SUCCESS);
				
			}catch(Exception exception){
				LOGGER.error("Exception occurred in findPaymentEventLogByMerchantRefNo(-) method", exception);
				response.setStatus(FinanceConstants.FAILURE);
				response.setErrMsg(exception.toString());
				response.setErrCode(FinanceErrorCodes.ERRORCODE.SPABYMER.getCode());
			}
			
		}
		LOGGER.info("=====Request Received for findPaymentEventLogByMerchantRefNo(-) method : END=====");
		return xstream.toXML(response);
	}
}
