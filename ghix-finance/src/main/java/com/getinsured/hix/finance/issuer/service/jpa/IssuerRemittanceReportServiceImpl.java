package com.getinsured.hix.finance.issuer.service.jpa;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.finance.employer.repository.IEmployerFinanceRepository;
import com.getinsured.hix.finance.issuer.service.FinanceIssuerService;
import com.getinsured.hix.finance.issuer.service.IssuerRemittanceLineItemService;
import com.getinsured.hix.finance.issuer.service.IssuerRemittanceService;
import com.getinsured.hix.finance.issuer.service.IssuerPaymentDetailService;
import com.getinsured.hix.finance.issuer.service.IssuerPaymentInvoiceService;
import com.getinsured.hix.finance.issuer.service.IssuerPaymentService;
import com.getinsured.hix.finance.issuer.service.IssuerRemittanceReportService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinancialMgmtUtils;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRemittance;
import com.getinsured.hix.model.IssuerRemittanceLineItem;
import com.getinsured.hix.model.IssuerPaymentDetail;
import com.getinsured.hix.model.IssuerPaymentDetail.EDIGeneratedFlag;
import com.getinsured.hix.model.IssuerPaymentDetail.ExchangePaymentType;
import com.getinsured.hix.model.IssuerPaymentInvoice;
import com.getinsured.hix.model.IssuerPayments;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.finance.issuerRemittance.IssuerRemittanceRequest;
import com.getinsured.hix.webservice.finance.issuerRemittance.IssuerRemittanceResponse;

/**
 * This service implementation class is for GHIX-FINANCE module for issuer remittance.
 * @author Sharma_k
 * @since 29 July, 2013
 * @version $Revision: 1.0 $
 */
@Service("issuerRemittanceReportService")
public class IssuerRemittanceReportServiceImpl implements IssuerRemittanceReportService
{
	@Autowired private FinanceIssuerService financeIssuerService;
	@Autowired private FinancialMgmtUtils financialMgmtUtils;
	@Autowired private IssuerRemittanceService issuerRemittanceService;
	@Autowired private IssuerRemittanceLineItemService issuerRemittanceLineItemService;
	@Autowired private IssuerPaymentService issuerPaymentService;
	@Autowired private IssuerPaymentInvoiceService issuerPaymentInvoiceService;
	@Autowired private IssuerPaymentDetailService issuerPaymentDetailService;
	@Autowired private IEmployerFinanceRepository iEmployerFinanceRepository;
	//@Autowired private EmployerPremiumPaymentProcessingService employerPremiumPaymentProcessingService;

	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerRemittanceReportServiceImpl.class);

	/**
	 * Method processIssuerRemittanceRequest.
	 * @param issuerRemittanceRequest IssuerRemittanceRequest
	 * @return IssuerRemittanceResponse
	 * @throws GIException
	 * @see com.getinsured.hix.finance.issuer.service.IssuerRemittanceReportService#processIssuerRemittanceRequest(IssuerRemittanceRequest)
	 */
	@Override
	@Transactional
	public IssuerRemittanceResponse processIssuerRemittanceRequest(IssuerRemittanceRequest issuerRemittanceRequest) throws GIException
	{
		long startTime = TSCalendar.getInstance().getTimeInMillis();
		DateFormat outputFormatter = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MM_DD_YYYY);
		DateFormat inputFormatter = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MMDDYYYY);
		IssuerRemittanceResponse issuerRemittanceResponse = new IssuerRemittanceResponse();
		/**
		 * As discussed HIOS_ISSUER_ID value will be retrieved from Issuer_Id
		 * request field
		 */
		String hiosIssuerId = issuerRemittanceRequest.getIssuerId();
		String issuerName = issuerRemittanceRequest.getIssuerName();
		LOGGER.info("IND42 :: Received IND42 request for Issuer Name :"+issuerName+" HIOS_ISSUER_ID: "+hiosIssuerId);
		Issuer issuer = financeIssuerService.getIssuerByHiosIDAndIssuerName(hiosIssuerId, issuerName);
		/**
		 * Issuer exists in GI DB Check.
		 */
		if(issuer == null)
		{
			throw new GIException(FinanceConstants.ERROR_CODE_201,FinanceConstants.ERR_MSG_ISSUER_ID_NOT_EXISTS + hiosIssuerId,FinanceConstants.EXCEPTION_SEVERITY_HIGH);
		}
		List<IssuerRemittanceRequest.Employee> employeeList = issuerRemittanceRequest.getEmployee(); 
		if(employeeList != null && !employeeList.isEmpty())
		{
			for(IssuerRemittanceRequest.Employee employee : employeeList)
			{
				int enrollmentId = Integer.parseInt(employee.getEnrollmentId());
				Employer employer = iEmployerFinanceRepository.findByExternaId(employee.getEmployerCaseId());
				if(employer == null)
				{
					throw new GIException(FinanceConstants.ERROR_CODE_202,FinanceConstants.ERR_MSG_EMPLOYER_NOT_EXISTS + employee.getEmployerCaseId() ,FinanceConstants.EXCEPTION_SEVERITY_HIGH);
				}
				if(!financialMgmtUtils.isEnrollmentExists(enrollmentId))
				{
					throw new GIException(FinanceConstants.ERROR_CODE_203,FinanceConstants.ERR_MSG_ENROLLMENT_NOT_EXISTS + employee.getEnrollmentId() ,FinanceConstants.EXCEPTION_SEVERITY_HIGH);
				}
				IssuerRemittance issuerRemittance = new IssuerRemittance();
				IssuerRemittanceLineItem issuerRemittanceLineItem = new IssuerRemittanceLineItem();
				IssuerPaymentDetail issuerPaymentDetail = new IssuerPaymentDetail();
				IssuerPaymentInvoice issuerPaymentInvoice = new IssuerPaymentInvoice();
				IssuerPayments issuerPayments = new IssuerPayments();
				issuerRemittance.setIssuer(issuer);
				Date startDate = null;
				Date endDate = null;
				try
				{
					startDate = inputFormatter.parse(employee.getPremiumStartDate());
				}
				catch (ParseException pe)
				{
					LOGGER.error("Exception caught: "+pe);
					throw new GIException(FinanceConstants.ERROR_CODE_204, FinanceConstants.ERR_MSG_PREMIUM_START_DATE
							+ employee.getPremiumStartDate(), FinanceConstants.EXCEPTION_SEVERITY_HIGH);
				}
				try
				{
					endDate = inputFormatter.parse(employee.getPremiumEndDate());
				}
				catch (ParseException pex)
				{
					LOGGER.error("Exception caught: "+pex);
					throw new GIException(FinanceConstants.ERROR_CODE_205, FinanceConstants.ERR_MSG_PREMIUM_END_DATE
							+ employee.getPremiumEndDate(), FinanceConstants.EXCEPTION_SEVERITY_HIGH);
				}
				String periodCovered = outputFormatter.format(startDate) + " to " + outputFormatter.format(endDate);
				issuerRemittance.setPeriodCovered(periodCovered);
				IssuerRemittance newIssuerRemittance = issuerRemittanceService.saveIssuerRemittance(issuerRemittance);
				issuerRemittanceLineItem.setIssuerRemittance(newIssuerRemittance);
				issuerRemittanceLineItem.setEnrollmentId(enrollmentId);
				/*issuerRemittanceLineItem.setPeriodCovered(periodCovered);*/
				issuerRemittanceLineItem.setAmountReceivedFromEmployer(employee.getGrossPremium());
				issuerRemittanceLineItem.setExternalEmployeeId(employee.getEmployeeCaseId());
				issuerRemittanceLineItem.setEmployer(employer);
				issuerRemittanceLineItemService.saveIssuerRemittanceLineItems(issuerRemittanceLineItem);

				issuerPaymentInvoice.setIssuerRemittance(newIssuerRemittance);
				IssuerPaymentInvoice newIssuerPaymentInvoice = issuerPaymentInvoiceService.saveIssuerPaymentInvoice(issuerPaymentInvoice);

				issuerPayments.setIssuerPaymentInvoice(newIssuerPaymentInvoice);
				issuerPayments.setPeriodCovered(periodCovered);
				IssuerPayments newIssuerPayments = issuerPaymentService.saveIssuerPayments(issuerPayments);

				issuerPaymentDetail.setAmount(employee.getGrossPremium());
				issuerPaymentDetail.setIssuerPayment(newIssuerPayments);
				issuerPaymentDetail.setIsEdiGenerated(EDIGeneratedFlag.N);
				issuerPaymentDetail.setIssuer(issuer);
				if(employee.getGrossPremium().compareTo(BigDecimal.ZERO) == 1)
				{
					issuerPaymentDetail.setExchangePaymentType(ExchangePaymentType.PREM);
				}
				else
				{
					issuerPaymentDetail.setExchangePaymentType(ExchangePaymentType.PREMADJ);
				}
				issuerPaymentDetailService.saveIssuerPaymentDetail(issuerPaymentDetail);

				LOGGER.info("IND42 :Persisted all the records required for remittance report");

				/* Because of enrollment renewal story the updateEnrollmentStatusToActive() call need to be 
				 * changed but the current file is only required for CA and for CA we are using different branch CA-MAIN
				 * So commenting the update call for Story :: JIRA-HIX-41143*/
				/*employerPremiumPaymentProcessingService.updateEnrollmentStatusToActive(employer.getId(),enrollmentId);*/
				LOGGER.info("IND42 :Successfully updated Enrollement & Shop_enrollment status ...");
				issuerRemittanceResponse.setResponseCode(Integer.toString(FinanceConstants.SUCCESS_CODE_200));
				issuerRemittanceResponse.setResponseDescription("Successfully Saved Issuer Payment Related Information");
			}
		}
		long endTime = TSCalendar.getInstance().getTimeInMillis();
		LOGGER.debug("IND42: Time consumed to process a request object"+(endTime-startTime));
		return 	issuerRemittanceResponse;
	}
}
