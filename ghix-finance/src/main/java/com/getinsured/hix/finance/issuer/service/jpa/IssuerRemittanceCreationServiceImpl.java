package com.getinsured.hix.finance.issuer.service.jpa;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService;
import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.finance.fees.service.ExchangeFeeService;
import com.getinsured.hix.finance.issuer.service.IssuerPaymentDetailService;
import com.getinsured.hix.finance.issuer.service.IssuerPaymentInvoiceService;
import com.getinsured.hix.finance.issuer.service.IssuerPaymentService;
import com.getinsured.hix.finance.issuer.service.IssuerRemittanceCreationService;
import com.getinsured.hix.finance.issuer.service.IssuerRemittanceLineItemService;
import com.getinsured.hix.finance.issuer.service.IssuerRemittanceService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceGeneralUtility;
import com.getinsured.hix.finance.utils.FinancialMgmtGenericUtil;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.ExchangeFee;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerPaymentDetail;
import com.getinsured.hix.model.IssuerPaymentDetail.EDIGeneratedFlag;
import com.getinsured.hix.model.IssuerPaymentDetail.ExchangePaymentType;
import com.getinsured.hix.model.IssuerPaymentInvoice;
import com.getinsured.hix.model.IssuerPayments;
import com.getinsured.hix.model.IssuerRemittance;
import com.getinsured.hix.model.IssuerRemittance.IsActive;
import com.getinsured.hix.model.IssuerRemittance.PaidStatus;
import com.getinsured.hix.model.IssuerRemittanceLineItem;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.security.service.UserService;

/**
 * 
 * @author Sharma_k
 *
 */
@Service("issuerRemittanceCreationService")
public class IssuerRemittanceCreationServiceImpl implements IssuerRemittanceCreationService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerRemittanceCreationServiceImpl.class);
	@Autowired private EmployerInvoiceLineItemsService employerInvoiceLineItemsService;
	@Autowired private IssuerRemittanceService issuerRemittanceService;
	@Autowired private IssuerRemittanceLineItemService issuerRemittanceLineItemService;
	@Autowired private IssuerPaymentInvoiceService issuerPaymentInvoiceService;
	@Autowired private IssuerPaymentDetailService issuerPaymentDetailService;
	@Autowired private ExchangeFeeService exchangeFeeService;
	@Autowired private IssuerPaymentService issuerPaymentService;
	@Autowired private UserService userService;

	/**
	 * Method createIssuerInvoice from Employer_Invoice_Lineitems.
	 * @throws FinanceException
	 * @see com.getinsured.hix.finance.issuer.service.IssuerPaymentProcessingService#createIssuerRemittance()
	 */
	@Override
	public void createIssuerRemittance() throws FinanceException
	{
		LOGGER.info("STARTED-createIssuerInvoice() method");
		int invoiceId = 0;
		IssuerRemittance issuerRemittance = null;
		IssuerRemittanceLineItem issuerRemittanceLineItem = null;
		Issuer issuer = new Issuer();
		Map<Integer, Exception> exceptionDetails = null;
		List<Integer> issuerIdList = employerInvoiceLineItemsService.getNonInvoiceGeneratedIssuerId();
		String exchangeRemittence = DynamicPropertiesUtil
			 	.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.EXCHANGE_DEDUCT_REMITTENCE);
		AccountUser user = FinanceGeneralUtility.getAccountUser(userService);
		LOGGER.info("The value of Exchage Remittence: " + exchangeRemittence);
		for (int id : issuerIdList)
		{
			String periodCovered = null;
			LOGGER.info("Started Processing each record for Issuer Id: " + id);
			BigDecimal totalAmt = FinancialMgmtGenericUtil.getDefaultBigDecimal();
			issuer.setId(id);
			List<IssuerRemittanceLineItem> issuerRemittanceLineItemList = new ArrayList<IssuerRemittanceLineItem>();
			LOGGER.info("Get Invoice Line Items By Issuer: " + issuer.getId());
			List<EmployerInvoiceLineItems> employerInvoiceLineItemsList = employerInvoiceLineItemsService
					.findByIssuerId(issuer.getId());

			if (employerInvoiceLineItemsList != null && !employerInvoiceLineItemsList.isEmpty())
			{
				LOGGER.info(" The Size of the Employer Invoice Line Items List: "+ employerInvoiceLineItemsList.size());
				issuerRemittance = new IssuerRemittance();
				List<Integer> empInvItemIds = new ArrayList<Integer>();
				try
				{
					for (EmployerInvoiceLineItems empInvoiceLineItems : employerInvoiceLineItemsList)
					{
						empInvItemIds.add(empInvoiceLineItems.getId());
						issuerRemittanceLineItem = issuerRemittanceLineItemService
								.createIssuerRemittanceLineItemsfromEmployerInvoiceLineItems(empInvoiceLineItems);

						issuerRemittanceLineItem.setIssuerRemittance(issuerRemittance);
						issuerRemittanceLineItemList.add(issuerRemittanceLineItem);
						totalAmt = FinancialMgmtGenericUtil.add(totalAmt, issuerRemittanceLineItem.getNetAmount());
						periodCovered = empInvoiceLineItems.getPeriodCovered();
					}

					/*
					 *HIX-63624 Adding synchronized block for saving Issuer Invoice Creation.  
					 */
					synchronized(this)
					{
						if(empInvItemIds.size() == employerInvoiceLineItemsService.countNonIssuerRemittanceGeneratedLineItems(empInvItemIds))
						{
							issuerRemittance.setStatementDate(new TSDate());
							issuerRemittance.setLastUpdatedBy(user.getId());
							LocalDate monthEnd = new LocalDate().plusMonths(1).withDayOfMonth(1).minusDays(1);
							LOGGER.info(" Payment Due Date for Issuer: "+ monthEnd.toDate());
							issuerRemittance.setPaymentDueDate(monthEnd.toDate());
							issuerRemittance.setPeriodCovered(periodCovered);
							issuerRemittance.setAmountDueFromLastRemittance(FinancialMgmtGenericUtil
									.getDefaultBigDecimal());
							/*
							 * Amount Due from Last invoices changes done.
							 */
							LOGGER.info(" Get Previous Issuer Invoices for Issuer Id: "+ id);
							IssuerRemittance previousIssuerInvoices = issuerRemittanceService
									.findPreviousIssuerRemittance(id);
							List<IssuerRemittanceLineItem> modifiedPreviousLineItems = null;

							if (previousIssuerInvoices != null) 
							{
								/*
								 * this code is for adding the existing
								 * issuerinvoicelineitems of previous invoice into the current invoice
								 */
								BigDecimal amtDueFromLastInvoice = FinancialMgmtGenericUtil
										.subtract(previousIssuerInvoices
												.getTotalAmountDue(), previousIssuerInvoices.getTotalPaymentPaid());

								String invoicePaidStatus = previousIssuerInvoices
										.getPaidStatus() != null ? previousIssuerInvoices.getPaidStatus().toString() : "";

										if (amtDueFromLastInvoice.compareTo(BigDecimal.ZERO) != FinanceConstants.NUMERICAL_0
												&& (IssuerRemittance.PaidStatus.DUE.toString().equalsIgnoreCase(invoicePaidStatus)
														|| IssuerRemittance.PaidStatus.PARTIALLY_PAID
														.toString().equalsIgnoreCase(invoicePaidStatus)))
										{
											LOGGER.info("Get Issuer Invoice Line Items for Issuer previousIssuerInvoices: "+ previousIssuerInvoices.getId());
											issuerRemittance.setAmountDueFromLastRemittance(FinancialMgmtGenericUtil.subtract(
													previousIssuerInvoices.getTotalAmountDue(), previousIssuerInvoices.getTotalPaymentPaid()));

											List<IssuerRemittanceLineItem> previousInvList = issuerRemittanceLineItemService.getIssuerRemittanceLineItems(previousIssuerInvoices);
											if (previousInvList != null && !previousInvList.isEmpty()) 
											{
												modifiedPreviousLineItems = new ArrayList<IssuerRemittanceLineItem>();

												for (IssuerRemittanceLineItem invItem : previousInvList)
												{
													IssuerRemittanceLineItem item = getNewInvLineItem(invItem);
													item.setIssuerRemittance(issuerRemittance);
													modifiedPreviousLineItems.add(item);
												}
											}
										} // end of adding previous invoice line items

										if (previousIssuerInvoices.getIsActive().equals(IsActive.Y))
										{
											LOGGER.info(" Update Issuer Invoices Active Status for previousIssuerInvoicesId: "
													+ previousIssuerInvoices.getId()+ " IsActive.N ");
											issuerRemittanceService.updateIssuerRemittanceActiveStatus(
													previousIssuerInvoices.getId(), IsActive.N);
										}
							}

							/*
							 * float percentageRate = userFeesService.getPercentageRate("EXCHANGE", String.valueOf(total_amt));
							 * As per jira HIX-24649 fetching Exchange fee from
							 * Configuration instead of Fee_Type table
							 */
							float percentageRate = Float
									.parseFloat((DynamicPropertiesUtil
											.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.EXCHANGE_FEE) != null) ? DynamicPropertiesUtil
													.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.EXCHANGE_FEE) : "0.0");

							BigDecimal userFee = BigDecimal.ZERO;
							BigDecimal totalDueAmoutWithCurrentUserFee = BigDecimal.ZERO;
							/*
							 * Calculating user fee when the ExchangeDeductRemmitance Flag is YES			
							 */
							if(StringUtils.isNotEmpty(exchangeRemittence) && exchangeRemittence.equalsIgnoreCase(FinanceConstants.YES))
							{
								//userFee = getUserFee(new BigDecimal(percentageRate), total_amt);
								userFee = getUserFee(BigDecimal.valueOf(percentageRate), totalAmt);								
								if (issuerRemittanceLineItemList != null && !issuerRemittanceLineItemList.isEmpty()) 
								{
									for (IssuerRemittanceLineItem issuerInvoicesLineItemsItr : issuerRemittanceLineItemList)
									{
										BigDecimal userFees = FinancialMgmtGenericUtil.getDefaultBigDecimal();

										if (userFee.compareTo(BigDecimal.ZERO) == FinanceConstants.NUMERICAL_1
												&& issuerInvoicesLineItemsItr.getNetAmount().compareTo(
														BigDecimal.ZERO) != FinanceConstants.NUMERICAL_0)
										{
											userFees = FinancialMgmtGenericUtil.multiply(userFee,
													issuerInvoicesLineItemsItr.getNetAmount()
													.divide(totalAmt, new MathContext(
															FinanceConstants.NUMERICAL_6,
															RoundingMode.HALF_UP)));
											issuerInvoicesLineItemsItr.setUserFee(userFees);
											issuerInvoicesLineItemsItr.setNetAmount(FinancialMgmtGenericUtil.subtract(
													issuerInvoicesLineItemsItr.getNetAmount(), userFees));
										}
									}
								}

								totalDueAmoutWithCurrentUserFee = FinancialMgmtGenericUtil.subtract(totalAmt, userFee);

								if (previousIssuerInvoices != null)
								{
									LOGGER.info(" Get Exchange Fee by Issuer Invoice Id: "+ previousIssuerInvoices.getId());
									ExchangeFee exchangeFee = exchangeFeeService.findExchangeFeeByIssuerRemittance(previousIssuerInvoices.getId());

									if (exchangeFee != null && (exchangeFee.getPaymentReceived()
											.compareTo(exchangeFee.getTotalFeeBalanceDue()) == -1))
									{
										userFee = FinancialMgmtGenericUtil.add(userFee,
												FinancialMgmtGenericUtil.subtract(exchangeFee.getTotalFeeBalanceDue(),
														exchangeFee.getPaymentReceived()));
									}
								}
							}
							else
							{
								totalDueAmoutWithCurrentUserFee = totalAmt;
							}

							issuerRemittance.setTotalPaymentPaid(FinancialMgmtGenericUtil.getDefaultBigDecimal());
							issuerRemittance.setPremiumThisPeriod(totalDueAmoutWithCurrentUserFee);

							issuerRemittance.setAdjustments(FinancialMgmtGenericUtil.getDefaultBigDecimal());
							issuerRemittance.setExchangeFees(userFee);

							issuerRemittance.setTotalAmountDue(FinancialMgmtGenericUtil
									.add(totalDueAmoutWithCurrentUserFee, issuerRemittance.getAmountDueFromLastRemittance()));

							issuerRemittance.setAmountEnclosed(FinancialMgmtGenericUtil.getDefaultBigDecimal());

							issuerRemittance.setIsActive(IsActive.Y);
							issuerRemittance.setIssuer(issuer);
							// below conditional block is added for HIX-23701
							if (modifiedPreviousLineItems != null) 
							{
								/*
								 * appending previous line items to the current line items list
								 */
								issuerRemittanceLineItemList.addAll(modifiedPreviousLineItems);
							}

							issuerRemittance.setIssuerRemittanceLineItem(issuerRemittanceLineItemList);
							issuerRemittance.setAmountReceivedFromEmployer(FinancialMgmtGenericUtil
									.getDefaultBigDecimal());
							/*
							 * GL_Code is hard coded value 2000 for Issuer_Invoices for
							 * Exchange GL_coding
							 */
							issuerRemittance.setGlCode(FinanceConstants.ISSUER_INVOICE_GLCODE);
							/*
							 * Changed the conditional statement to support create invoice in case of TotalAmountDue == 0 :: HIX-30471
							 */
							if (issuerRemittance != null
									&& !issuerRemittance.getIssuerRemittanceLineItem().isEmpty())
							{
								/*
								 * added the conditional statement if invoice TotalAmountDue == 0 set the PaidStatus=PAID :: HIX-30471
								 */
								if (issuerRemittance.getTotalAmountDue().compareTo(BigDecimal.ZERO) == FinanceConstants.NUMERICAL_0)
								{
									issuerRemittance.setPaidStatus(PaidStatus.PAID);
									for (IssuerRemittanceLineItem issuerInvoicesLineItem : issuerRemittanceLineItemList)
									{
										issuerInvoicesLineItem.setAmountReceivedFromEmployer(issuerInvoicesLineItem
												.getNetAmount());
										issuerInvoicesLineItem.setAmtPaidToIssuer(issuerInvoicesLineItem
												.getNetAmount());
									}
								}
								else
								{
									issuerRemittance.setPaidStatus(PaidStatus.DUE);
								}
								IssuerRemittance newissuerInvoices = issuerRemittanceService.saveIssuerRemittance(issuerRemittance);
								invoiceId = newissuerInvoices.getId();
								LOGGER.info("Invoice Id : " + invoiceId);
								DateFormat df = new SimpleDateFormat("MMM");
								Date date = new TSDate();
								String invoiceNumber = "INV-" + df.format(date).toUpperCase()
										+ (TSCalendar.getInstance().get(Calendar.YEAR)) + "-" + invoiceId;

								issuerRemittanceService.updateIssuerRemittanceNumber(invoiceNumber, invoiceId);
								LOGGER.info("Update Issuer Invoices Generated: "+ empInvItemIds);
								int noOfRecordsUpdated = employerInvoiceLineItemsService.updateIssuerInvoiceGenerated(empInvItemIds);
								LOGGER.debug(noOfRecordsUpdated+ "records have been updated in EmployerInvoiceLineItems table");
								/*
								 * Create dummy record in issuer payment related table for 0 amount invoice :: HIX-30471
								 */
								if (issuerRemittance.getTotalAmountDue().compareTo(BigDecimal.ZERO) == FinanceConstants.NUMERICAL_0)
								{
									insertIssuerPaymentDetail(issuer, newissuerInvoices);
								}
								insertExchangeFee(issuer, newissuerInvoices, userFee, exchangeRemittence);
							}
						}
						else
						{
							LOGGER.warn("IssuerRemittance creation service :: Unable to create Issuer Remittance for Issuer ID: "+id);
						}
					}
				}
				catch (Exception ex)
				{
					if (issuerRemittance != null)
					{
						LOGGER.info("Issuer Invoices Data: ", issuerRemittance.toString());
					}
					LOGGER.error("Exception caught in issuerInvoices creation for Issuer Id:"
							+ issuer.getId() + " and Invoice Id: " + invoiceId);
					LOGGER.error("Exception caught in issuerInvoices creation: ", ex);
					if (exceptionDetails == null) 
					{
						exceptionDetails = new HashMap<Integer, Exception>();
					}
					exceptionDetails.put(id, ex);
				}
			}
		}
		LOGGER.info("END: :createIssuerInvoice(-) ends successfully ==========");
		if (exceptionDetails != null && !exceptionDetails.isEmpty())
		{
			String exceptionMessage = FinanceGeneralUtility.constructSingleMessageForExceptionDetails("EXCEPTION OCCURRED WHILE CREATING INVOICE FOR THE FOLLOWING ISSUERS:", "Issuer Id", exceptionDetails);
			throw new FinanceException(exceptionMessage);
		}

	}


	/**
	 * 
	 * @param percentageRange
	 * @param amount
	 * @return
	 */
	private BigDecimal getUserFee(BigDecimal percentageRange, BigDecimal amount) {
		BigDecimal fee = FinancialMgmtGenericUtil.getDefaultBigDecimal();
		if(!(percentageRange.compareTo(BigDecimal.ZERO)==0) && !(amount.compareTo(BigDecimal.ZERO)==0)){
			fee = FinancialMgmtGenericUtil.multiply(percentageRange.divide(new BigDecimal(FinanceConstants.NUMERICAL_100)), amount);
		}
		return fee;
	}

	/**
	 * Method getNewInvLineItem.
	 * @param invItem IssuerInvoicesLineItems
	 * @return IssuerInvoicesLineItems
	 */
	private IssuerRemittanceLineItem getNewInvLineItem(IssuerRemittanceLineItem invItem){
		LOGGER.info("START-getNewInvLineItem(-) method started with IssuerInvoicesLineItems: "+invItem.getAmountReceivedFromEmployer());
		IssuerRemittanceLineItem item = new IssuerRemittanceLineItem();
		item.setAmountReceivedFromEmployer(invItem.getAmountReceivedFromEmployer());
		item.setAmtPaidToIssuer(invItem.getAmtPaidToIssuer());
		item.setEmployer(invItem.getEmployer());
		item.setEmployerInvoicesLineItems(invItem.getEmployerInvoicesLineItems());
		item.setEnrollmentId(invItem.getEnrollmentId());
		item.setExternalEmployeeId(invItem.getExternalEmployeeId());
		item.setNetAmount(invItem.getNetAmount());
		item.setRetroAdjustments(invItem.getRetroAdjustments());
		item.setTotalPremium(invItem.getTotalPremium());
		item.setUserFee(invItem.getUserFee());
		LOGGER.info("END::getNewInvLineItem(-) ends successfully with IssuerInvoicesLineItems: "+item);
		return item;
	}

	/**
	 * 
	 * @param issuer
	 * @param issuerInvoices
	 */
	private void insertIssuerPaymentDetail(Issuer issuer,IssuerRemittance issuerRemittance) {
		IssuerPaymentDetail issuerPaymentDetail = new IssuerPaymentDetail();
		IssuerPaymentInvoice issuerPaymentInvoice = new IssuerPaymentInvoice();
		IssuerPayments issuerPayments = new IssuerPayments();
		issuerPaymentInvoice.setIssuerRemittance(issuerRemittance);
		IssuerPaymentInvoice newIssuerPaymentInvoice = issuerPaymentInvoiceService.saveIssuerPaymentInvoice(issuerPaymentInvoice);


		issuerPayments.setIssuerPaymentInvoice(newIssuerPaymentInvoice);
		issuerPayments.setPeriodCovered(issuerRemittance.getPeriodCovered());
		issuerPayments.setInvoiceNumber(issuerRemittance.getRemittanceNumber());
		IssuerPayments newIssuerPayments = issuerPaymentService.saveIssuerPayments(issuerPayments);

		issuerPaymentDetail.setAmount(issuerRemittance.getTotalAmountDue());
		issuerPaymentDetail.setIssuerPayment(newIssuerPayments);
		issuerPaymentDetail.setIsEdiGenerated(EDIGeneratedFlag.N);
		issuerPaymentDetail.setIssuer(issuer);
		issuerPaymentDetail.setExchangePaymentType(ExchangePaymentType.PREM);

		issuerPaymentDetailService.saveIssuerPaymentDetail(issuerPaymentDetail);

	}

	/**
	 * Method to persist ExchangeFees in to DB
	 * @param Object<Issuer> issuer
	 * @param Object<IssuerInvoices> issuerInvoices
	 * @param BigDecimal userFee
	 * @param String exchageDeductRemittenceFlag
	 * @return Object<ExchangeFee> | NULL if exchageDeductRemittenceFlag is NO
	 * @revision Jira Id HIX-44185
	 */
	private ExchangeFee insertExchangeFee(Issuer issuer, IssuerRemittance issuerRemittance, BigDecimal userFee, String exchageDeductRemittenceFlag)
	{
		ExchangeFee exchangeFee = null;
		if(exchageDeductRemittenceFlag !=null && exchageDeductRemittenceFlag.equalsIgnoreCase(FinanceConstants.YES))
		{
			LOGGER.info("START-insertExchangeFee(-) method started with userFee: "+ userFee
					+ " for Issuer: "+ issuer.getId()
					+ " Issuer Invoice No: "+ issuerRemittance.getRemittanceNumber());
			exchangeFee = new ExchangeFee();
			exchangeFee.setIssuer(issuer);
			exchangeFee.setIssuerRemittance(issuerRemittance);
			exchangeFee.setPaymentReceived(FinancialMgmtGenericUtil.getDefaultBigDecimal());
			exchangeFee.setTotalFeeBalanceDue(userFee);

			exchangeFee.setTotalPayment(FinancialMgmtGenericUtil.getDefaultBigDecimal());
			exchangeFee.setTotalRefund(FinancialMgmtGenericUtil.getDefaultBigDecimal());
			exchangeFee.setStatus(ExchangeFee.PaidStatus.DUE);

			/*Date[] dates = getDateRange();

			exchangeFee.setStartingDate(dates[0]);
			exchangeFee.setEndingDate(dates[1]);*/

			/*
			 * Fix for sonar violations
			 */
			DateTime monthStartDate = TSDateTime.getInstance();

			exchangeFee.setStartingDate(monthStartDate.dayOfMonth().withMinimumValue().toDate());
			exchangeFee.setEndingDate(monthStartDate.dayOfMonth().withMaximumValue().toDate());

			LOGGER.info("END::insertExchangeFee(-) method end successfully with set exchangeFee for issuer: "+exchangeFee.getIssuer());
			return exchangeFeeService.saveExchangeFee(exchangeFee);
		}
		return null;
	}

}
