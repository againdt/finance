package com.getinsured.hix.finance.issuer.service.jpa;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.finance.issuer.repository.IIssuerPaymentDetailRepository;
import com.getinsured.hix.finance.issuer.service.IssuerPaymentDetailService;
import com.getinsured.hix.model.IssuerPaymentDetail;
import com.getinsured.hix.model.IssuerPaymentDetail.EDIGeneratedFlag;

/**
 * This service implementation class is for GHIX-FINANCE module for issuer payment detail.
 * This class has various methods to create, modify and query the IssuerPaymentDetail model object. 
 *
 * @author Sharma_k
 * @since 28-Jun-2013
 */
@Service("IssuerPaymentDetailService")
public class IssuerPaymentDetailServiceImpl implements
		IssuerPaymentDetailService {
	@Autowired private IIssuerPaymentDetailRepository issuerPaymentDetailRepository;

	/**
	 * Method saveIssuerPaymentDetail.
	 * @param issuerPaymentDetail IssuerPaymentDetail
	 * @return IssuerPaymentDetail
	 * @see com.getinsured.hix.finance.issuer.service.IssuerPaymentDetailService#saveIssuerPaymentDetail(IssuerPaymentDetail)
	 */
	@Override
	public IssuerPaymentDetail saveIssuerPaymentDetail(
			IssuerPaymentDetail issuerPaymentDetail) {
		return issuerPaymentDetailRepository.save(issuerPaymentDetail);
	}

	/**
	 * Method getCurrentMonthPayments.
	 * @param startDate Date
	 * @param endDate Date
	 * @return List<IssuerPaymentDetail>
	 * @see com.getinsured.hix.finance.issuer.service.IssuerPaymentDetailService#getCurrentMonthPayments(Date, Date)
	 */
	@Override
	public List<IssuerPaymentDetail> getCurrentMonthPayments(Date startDate, Date endDate)
	{		
		return issuerPaymentDetailRepository.getCurrentMonthPaymentDetail(startDate, endDate);
	}

	/**
	 * Method listOfNonEDIGeneratedPayments.
	 * @param ediGeneratedFlag EDIGeneratedFlag
	 * @return List<IssuerPaymentDetail>
	 * @see com.getinsured.hix.finance.issuer.service.IssuerPaymentDetailService#listOfNonEDIGeneratedPayments(EDIGeneratedFlag)
	 */
	@Override
	public List<IssuerPaymentDetail> listOfNonEDIGeneratedPayments(EDIGeneratedFlag ediGeneratedFlag)
	{
		return issuerPaymentDetailRepository.findByIsEdiGenerated(ediGeneratedFlag);
	}

	/**
	 * Method findByMerchantRefCode.
	 * @param merchantRefCode String
	 * @return IssuerPaymentDetail
	 * @see com.getinsured.hix.finance.issuer.service.IssuerPaymentDetailService#findByMerchantRefCode(String)
	 */
	@Override
	public IssuerPaymentDetail findByMerchantRefCode(String merchantRefCode)
	{
		return issuerPaymentDetailRepository.findIssuerPaymentDetailByMerchantRefCode(merchantRefCode);
	}
	
	/**
	 * Method findPaymentDtlByIssuer.
	 * @param issuerId int
	 * @return List<IssuerPaymentDetail>
	 * @see com.getinsured.hix.finance.issuer.service.IssuerPaymentDetailService#findPaymentDtlByIssuer(int)
	 */
	@Override
	public List<IssuerPaymentDetail> findPaymentDtlByIssuer(int issuerId) 
	{
		return issuerPaymentDetailRepository.findPaymentDtlByIssuer(issuerId);
	}
	
	/**
	 * Method saveIssuerPymtDtlList.
	 * @param issuerPaymentDetailList List<IssuerPaymentDetail>
	 * @return List<IssuerPaymentDetail>
	 * @see com.getinsured.hix.finance.issuer.service.IssuerPaymentDetailService#saveIssuerPymtDtlList(List)
	 */
	@Override
	public List<IssuerPaymentDetail> saveIssuerPymtDtlList(List<IssuerPaymentDetail> issuerPaymentDetailList)
	{
		return issuerPaymentDetailRepository.save(issuerPaymentDetailList);
	}
	
	/**
	 * Method getDistinctIssuerIdEDINotGenerated.
	 * @return List<Integer>
	 * @see com.getinsured.hix.finance.issuer.service.IssuerPaymentDetailService#getDistinctIssuerIdEDINotGenerated()
	 */
	@Override
	public List<Integer> getDistinctIssuerIdEDINotGenerated() {
		return issuerPaymentDetailRepository.getDistinctIssuerEDINotGenerated();
	}

	@Override
	public List<Object[]> getRequestIdNMerchantRefCode(Integer employerInvoicesId) 
	{
		return issuerPaymentDetailRepository.findIssuerPaymentDetailByRemittance(employerInvoicesId);
	}
}
