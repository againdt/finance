package com.getinsured.hix.finance.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.finance.EmployerInvoicesDTO;
import com.getinsured.hix.dto.finance.FinanceResponse;
import com.getinsured.hix.dto.finance.PaymentMethodResponse;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.thoughtworks.xstream.XStream;

@Component
public final class FinanceGeneralUtility 
{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FinanceGeneralUtility.class);
	private FinanceGeneralUtility()
	{
		
	}
	
	/**
	 * 
	 * @param requestParameters
	 * @return
	 * @throws GIException
	 */
	public static String generateQueryString(Map<String, String> requestParameters) throws GIException
	{
		StringBuilder sb = new StringBuilder();
		try
		{
			for(Map.Entry<String, String> e : requestParameters.entrySet())
			{
			      if(sb.length() > FinanceConstants.NUMERICAL_0)
			      {
			          sb.append('&');
			      }
			      sb.append(URLEncoder.encode(e.getKey(), "UTF-8")).append('=').append(URLEncoder.encode(e.getValue(), "UTF-8"));
			  }
		}
		catch(UnsupportedEncodingException uee)
		{
			LOGGER.error("Exception Caught: "+uee.toString());
			throw new GIException("Unable to generate Query String using Map.",uee);
		}
		
		if(sb.length() > FinanceConstants.NUMERICAL_0)
		{
			sb.insert(FinanceConstants.NUMERICAL_0, "?");
		}
		return sb.toString();
	}
	
	/**
	 * Constructs single exception message by comprising details of Map<key, value> exceptionDetails object
	 * @param headerMessage message to show as heading relative to all exception objects
	 * @param keyMessage to show as key's message
	 * @param exceptionDetails Map<? extends Object, ? extends Exception> contains multiple exception objects.  Here key is preferably either Integer or String even though it accepts Object and value is Exception or subclasses of Exception
	 * @return String comprised of all exception details.
	 */
	public static String constructSingleMessageForExceptionDetails(String headerMessage, String keyMessage, Map<? extends Object, ? extends Exception> exceptionDetails)
	{
		StringBuilder exceptionInfo = new StringBuilder();
		if(exceptionDetails != null && !exceptionDetails.isEmpty())
		{
			exceptionInfo.append(headerMessage);
			/*for (Object key : exceptionDetails.keySet())
			{
				exceptionInfo.append(System.getProperty(FinanceConstants.LINE_DOT_SEPARATOR));
				exceptionInfo.append(keyMessage+": " + key + "------------>"+exceptionDetails.get(key));
				exceptionInfo.append(System.getProperty(FinanceConstants.LINE_DOT_SEPARATOR));
				exceptionInfo.append(getStackTraceInfo((exceptionDetails.get(key) != null) ? exceptionDetails.get(key).getStackTrace() : null));
				exceptionInfo.append(System.getProperty(FinanceConstants.LINE_DOT_SEPARATOR));
				exceptionInfo.append(System.getProperty("------------------------------------------------------------"));
			}*/
			for (Map.Entry<? extends Object, ? extends Exception> entry : exceptionDetails.entrySet())
			{
				exceptionInfo.append(System.getProperty(FinanceConstants.LINE_DOT_SEPARATOR));
				exceptionInfo.append(keyMessage+": " + entry.getKey() + "------------>"+exceptionDetails.get(entry.getKey()));
				exceptionInfo.append(System.getProperty(FinanceConstants.LINE_DOT_SEPARATOR));
				exceptionInfo.append(getStackTraceInfo((exceptionDetails.get(entry.getKey()) != null) ? exceptionDetails.get(entry.getKey()).getStackTrace() : null));
				exceptionInfo.append(System.getProperty(FinanceConstants.LINE_DOT_SEPARATOR));
				exceptionInfo.append(System.getProperty("------------------------------------------------------------"));
			}
			
		}
		else
		{
			exceptionInfo.append("No exception information available");
		}
		return exceptionInfo.toString();
   }
	
	/**
	 * gives error occurred location details includes className, methodName and lineNumber
	 */
   private static StringBuilder getStackTraceInfo(StackTraceElement[] stackTrace)
   {
	   StringBuilder errorDetails = new StringBuilder();
	   if(stackTrace != null)
		{
		    System.getProperty(FinanceConstants.LINE_DOT_SEPARATOR);
			errorDetails.append("======= Exception Stack Trace Starts ========");
			errorDetails.append(System.getProperty(FinanceConstants.LINE_DOT_SEPARATOR));
			for(int i = FinanceConstants.NUMERICAL_0; i<stackTrace.length && i<FinanceConstants.NUMERICAL_2; i++)
			{
				errorDetails.append(" Class Name: "+stackTrace[i].getClassName() + " Method Name: "+stackTrace[i].getMethodName() + " Line Number: "+stackTrace[i].getLineNumber());
				errorDetails.append(System.getProperty(FinanceConstants.LINE_DOT_SEPARATOR));
			}
			errorDetails.append("======= Exception Stack Trace Ends ========"); 
		} 
	   else
	   {
		   errorDetails.append("No Stack Trace");
	   }
	   return errorDetails;
   }
   
   /**
    * It is intended to send failure finance response(status = FAILURE and errorMsg = Search criteria is null or empty) when the received request details are null or empty. 
    * @return FinanceResponse
    */
   public static String sendXtreamFailureFinanceResponseWhenEmptyRequest()
	{
		FinanceResponse financeResponse = new FinanceResponse();
		financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
		financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		return xstream.toXML(financeResponse);
	}
   
   /**
    * It Checks whether the given map object is null/empty or not.
    * @param searchCriteria 
    * @return returns true if the input map object is null/empty otherwise false
    */
   public static boolean isEmptyMap(Map<? extends Object, ? extends Object> searchCriteria)
	{
		boolean isEmpty = false;
		if(searchCriteria==null || searchCriteria.isEmpty())
		{
			isEmpty = true;
		}
		return isEmpty;
	}
   
   /**
    * It converts finance response object to string XML format using xStreamHibernateXmlMashaling
    * @param financeResponse
    * @return
    */
   public static String sendXtreamFinanceResponse(FinanceResponse financeResponse)
   {
	   XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
	   return xstream.toXML(financeResponse);
   }
   
   /**
    * It is used to send failure finance response with exception details as string XML format using xStreamHibernateXmlMashaling
    * when exception occurred
    * @param exception
    * @return
    */
   public static String sendXtreamFailureFinanceResponseWhenExceptionEvent(Exception exception)
   {
	   FinanceResponse financeResponse = new FinanceResponse();
	   financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.INVERR.getCode());
	   financeResponse.setErrMsg(exception.toString());
	   financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
	   XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
	   return xstream.toXML(financeResponse);
   }
   
   /**
    * Returns EmployerInvoicesDTO object by populating DTO from main object and provides default values 
    * for amount and string values if their values are null.
    * @param employerInvoices
    * @return returns EmployerInvoicesDTO if employerInvoices is not null otherwise null.
    */
   public static EmployerInvoicesDTO populateInvoicesDTOFromEmployerInvoice(EmployerInvoices employerInvoices)
	{
	   EmployerInvoicesDTO invoiceDTO = null;
		if(employerInvoices != null){
			invoiceDTO = new EmployerInvoicesDTO();
			invoiceDTO.setEmployerID(employerInvoices.getEmployer().getId());
			invoiceDTO.setAdjustments((employerInvoices.getAdjustments())!=null?employerInvoices.getAdjustments():BigDecimal.ZERO);
			invoiceDTO.setAmountDueFromLastInvoice((employerInvoices.getAmountDueFromLastInvoice())!=null?employerInvoices.getAmountDueFromLastInvoice():BigDecimal.ZERO);
			invoiceDTO.setExchangeFees((employerInvoices.getExchangeFees())!=null?employerInvoices.getExchangeFees():BigDecimal.ZERO);
			invoiceDTO.setInvoiceNumber((employerInvoices.getInvoiceNumber())!=null?employerInvoices.getInvoiceNumber():"");
			invoiceDTO.setManualAdjustmentAmt((employerInvoices.getManualAdjustmentAmt())!=null?employerInvoices.getManualAdjustmentAmt():BigDecimal.ZERO);
			invoiceDTO.setAmountDueFromLastInvoice((employerInvoices.getAmountDueFromLastInvoice())!=null?employerInvoices.getAmountDueFromLastInvoice():BigDecimal.ZERO);
			invoiceDTO.setPaymentDueDate(employerInvoices.getPaymentDueDate());
			invoiceDTO.setPeriodCovered((employerInvoices.getPeriodCovered())!=null?employerInvoices.getPeriodCovered():"");
			invoiceDTO.setPremiumThisPeriod((employerInvoices.getPremiumThisPeriod())!=null?employerInvoices.getPremiumThisPeriod():BigDecimal.ZERO);
			invoiceDTO.setStatementDate(employerInvoices.getStatementDate());
			invoiceDTO.setTotalAmountDue((employerInvoices.getTotalAmountDue())!=null?employerInvoices.getTotalAmountDue():BigDecimal.ZERO);
			invoiceDTO.setTotalPaymentReceived((employerInvoices.getTotalPaymentReceived())!=null?employerInvoices.getTotalPaymentReceived():BigDecimal.ZERO);
			BigDecimal invoiceTotAdjRegAdjManualAdj = FinancialMgmtGenericUtil.add(invoiceDTO.getAdjustments(), invoiceDTO.getManualAdjustmentAmt());
			invoiceDTO.setInvoice_TotAdj_RegAdj_ManualAdj(invoiceTotAdjRegAdjManualAdj);
		}
		return invoiceDTO;
	}
   
   /**
    * It is intended to send failure payment method response(status = FAILURE and error message) when the received request details are null or empty. 
    * @return XML string representation of PaymentMethodResponse  
    */
   public static String sendXtreamFailurePaymentMethodResponseWhenEmptyRequest(String errorMessage)
	{
		PaymentMethodResponse response = new PaymentMethodResponse();
		response.setErrMsg(StringUtils.isNotBlank(errorMessage)?errorMessage:FinanceConstants.ERROR_NULL_REQUEST);
		response.setStatus(GhixConstants.RESPONSE_FAILURE);
		return sendXtreamPaymentMethodResponse(response);
	}
   
   /**
    * It is intended to send failure payment method response(status = FAILURE and error message) when no results found 
    * @return XML string representation of PaymentMethodResponse  
    */
   public static String sendXtreamFailurePaymentMethodResponseWhenResultsEmpty(String message)
	{
		PaymentMethodResponse response = new PaymentMethodResponse();
		response.setErrMsg(StringUtils.isNotBlank(message)?message:GhixConstants.EXCEPTION_NO_OBJECTS_WERE_FOUND_DESC);
		response.setStatus(GhixConstants.RESPONSE_FAILURE);
		return sendXtreamPaymentMethodResponse(response);
	}
   
   /**
    * It is used to send failure payment method response(status = FAILURE and error message) when no results found 
    * @return XML string representation of PaymentMethodResponse  
    */
   public static String sendXtreamFailurePaymentMethodResponseWhenException(String errorMessage, int errorCode)
	{
		PaymentMethodResponse response = new PaymentMethodResponse();
		response.setErrMsg(StringUtils.isNotBlank(errorMessage)?errorMessage:GhixConstants.EXCEPTION_NO_OBJECTS_WERE_FOUND_DESC);
		response.setStatus(GhixConstants.RESPONSE_FAILURE);
		response.setErrCode((errorCode != FinanceConstants.NUMERICAL_0)? errorCode:GhixConstants.EXCEPTION_INTERNAL_APPLICATION_ERROR_CODE);
		return sendXtreamPaymentMethodResponse(response);
	}
   
   /**
    * It is used to send success paymentMehtodRespnse when there is no error and data is available
    * @param response
    * @return
    */
   public static String sendXtreamPaymentMethodResponseWhenSuccess(PaymentMethodResponse response){
	   if(response != null){
		   response.setStatus(GhixConstants.RESPONSE_SUCCESS);
		   XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
			return xstream.toXML(response);
	   }
	   else{
		   return "";
	   }
   }
   
   /**
    * It is used by other methods in this class
    */
   private static String sendXtreamPaymentMethodResponse(PaymentMethodResponse response){
		   XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
			return xstream.toXML(response);
   }
   
   /**
	 * Displays the content of the Map object.
	 * 
	 * @param header
	 *            Header text.
	 * @param map
	 *            Map object to display.
	 */
	@SuppressWarnings("rawtypes")
	public static void displayMap(String header, Map map) {
		LOGGER.info(header);

		StringBuilder dest = new StringBuilder();

		if (map != null && !map.isEmpty()) {
			Iterator itr = map.entrySet().iterator();
			String key, val;
			while (itr.hasNext()) {
				Map.Entry entry = (Map.Entry)itr.next();
				key = (String) entry.getKey();
				val = (String) entry.getValue();
				dest.append(key + "=" + val + "\n");
			}
		}
		
		LOGGER.info(dest.toString());
	}	
	
	/**
	 * This methods removes the "$" currency symbol from the amount and return the amount as java BigDecimal value.
	 * 
	 * @param configurationEnum
	 *            FinanceConfigurationEnum configurationEnum.
	 * @return BigDecimal
	 *           amount BigDecimal.
	 * @throws GIException
	 */
	public static BigDecimal getConfigBigDecimalAmount(final FinanceConfiguration.FinanceConfigurationEnum configurationEnum) throws GIException
	{
		String amount = DynamicPropertiesUtil.getPropertyValue(configurationEnum);
		BigDecimal amountBigDecimal = FinancialMgmtGenericUtil.getDefaultBigDecimal();
		try
		{
			amount = amount.substring(amount.indexOf('$')+FinanceConstants.NUMERICAL_1, amount.length()).trim();
			amountBigDecimal = FinancialMgmtGenericUtil.getBigDecimalObj(Float.valueOf(amount));
		}
		catch(Exception ex)
		{
			LOGGER.error("Invalid format, Expected format is '$XX..", ex);
			throw new GIException("Received Invalid"+configurationEnum+" :"
							+ DynamicPropertiesUtil
									.getPropertyValue(configurationEnum)+ " Expected format is '$XX..'");
		}
		return amountBigDecimal;
	}
	
	/**
	 * 
	 * @param is InputStream
	 * @param encoding String
	 * @return String
	 * @throws IOException
	 * InputStream to String conversion, InputStream and Encoding type are parameter for this method. 
	 */
	public static String convertStreamToString(InputStream is, String encoding) throws IOException 
	{
		LOGGER.info("STARTED-convertStreamToString() method with encoding:"+encoding);
	    StringWriter writer = new StringWriter();
	    IOUtils.copy(is, writer, encoding);
		LOGGER.info("END : convertStreamToString() method");
	    return writer.toString();
	}
	
	public static AccountUser getAccountUser(UserService userService)
	{
		AccountUser user = null; 
		try{
			user = userService.getLoggedInUser();
		}
		catch(InvalidUserException ex){
			LOGGER.info("No logged in user");
		}
		if(user==null){
			user = userService.findByUserName(DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER));
		}
		return user;
	}
}
