package com.getinsured.hix.finance.employer.service.jpa;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.joda.time.Months;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.enrollment.EnrolleeCurrentMonthDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeDetailsDto;
import com.getinsured.hix.dto.enrollment.EnrollmentCurrentMonthDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentSubscriberEventDTO;
import com.getinsured.hix.finance.employer.service.EmployerExchangeFeeService;
import com.getinsured.hix.finance.employer.service.EmployerInvoiceCreationService;
import com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService;
import com.getinsured.hix.finance.employer.service.EmployerInvoicesService;
import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceGeneralUtility;
import com.getinsured.hix.finance.utils.FinancialMgmtGenericUtil;
import com.getinsured.hix.finance.utils.FinancialMgmtUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.EmployerEnrollment;
import com.getinsured.hix.model.EmployerExchangeFees;
import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.EmployerInvoiceLineItems.Status;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.EmployerInvoices.InvoiceType;
import com.getinsured.hix.model.EmployerInvoices.IsManualAdjustment;
import com.getinsured.hix.model.EmployerInvoices.PaidStatus;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.exception.GIException;

@Service("employerInvoiceCreationService")
public class EmployerInvoiceCreationServiceImpl implements EmployerInvoiceCreationService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployerInvoiceCreationServiceImpl.class);
	@Autowired private FinancialMgmtUtils financialMgmtUtils;
	@Autowired private EmployerInvoiceLineItemsService employerInvoiceLineItemsService;
	@Autowired private EmployerInvoicesService employerInvoicesService;
	@Autowired private EmployerExchangeFeeService employerExchangeFeeService;	
	@Autowired private UserService userService;
	private static final int MIDTIME = FinanceConstants.NUMERICAL_12;
	private static final String ENROLLMENT_STATUS_TERM = "TERM";

	/**
	 * Method createEmployerInvoice.
	 * @param employerid int
	 * @return int
	 * @see com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService#createEmployerInvoice(int)
	 */
	@Override	
	public int createEmployerInvoice(int employerid) throws FinanceException{
		LOGGER.info(" createEmployerInvoice(-) method started for employerid: "+employerid);
		int invoiceid = 0;
		Employer employer = new Employer();
		employer.setId(employerid);
		try
		{
			EmployerInvoices lastEmployerInvoices = null;
			EmployerInvoices newEmployerInvoices = null;
			boolean isEmployerEnrollmentExist = false;
			Integer employerEnrollmentID = 0;
			Date firstOfNextMonth = DateTime.now().plusMonths(FinanceConstants.NUMERICAL_1).dayOfMonth().withMinimumValue().toDate();
			Map<String, Object> employerEnrollmentDetails = null;
			/**
			 * Suppressing GHIX-FAILURE response as after coverage termination there may be special enrollment
			 * Or after Employer Termination special enrollment can be reported by employee.
			 */
			try
			{
				employerEnrollmentDetails = financialMgmtUtils.findEmployerEnrollmentDetails(employerid, EmployerEnrollment.Status.ACTIVE, firstOfNextMonth);
			}
			catch(GIException gie)
			{
				if(gie != null && gie.getErrorCode() == FinanceConstants.EMPLOYER_ENROLLMENT_NOT_PRESENT)
				{
					/**
					 * Suppressing GHIX-FAILURE for Normal Invoice Where Special Enrollment can be created after Enrollment termination.
					 */
					LOGGER.info("No Active Employer Enrollment found for employer_Id: "+employerid+" Where next Month Start Date "+firstOfNextMonth+" Falls in coverage. Exception :"+gie);
				}
				else
				{
					throw new GIException(gie.getErrorMsg());
				}

			}

			if(employerEnrollmentDetails !=null && !employerEnrollmentDetails.isEmpty())
			{
				employerEnrollmentID = (Integer) employerEnrollmentDetails.get(FinanceConstants.EMPLOYER_ENROLLMENT_ID_KEY);
			}
			lastEmployerInvoices = employerInvoicesService.findLastActiveInvoice(employerid);

			if(employerEnrollmentID != null && employerEnrollmentID != 0 )
			{
				/*
				 * Check whether active employerEnrollment is available or not for employerEnrollmentID which received from SHOP API, if not throw exception. 
				 */
				if(lastEmployerInvoices != null)
				{
					List<EmployerInvoiceLineItems> lastEmployerInvoiceLineItems = employerInvoiceLineItemsService.getInvoiceLineItem(lastEmployerInvoices);
					for(EmployerInvoiceLineItems lineItem : lastEmployerInvoiceLineItems)
					{
						if(employerEnrollmentID.equals(lineItem.getEmployerEnrollmentId()))
						{
							isEmployerEnrollmentExist = true;
							break;
						}
					}
				}

				if(!isEmployerEnrollmentExist)
				{
					throw new FinanceException("No active employerEnrollment found to proceed further for employerID:"+employerid+" with Employer_ErnollmentID:"+employerEnrollmentID);
				}
			}
			/*else
			{*/
			/*
			 * If employer is not eligible for next month coverage then we will not receive employerEnrollmentID from SHOP.
			 * Then check last active invoice date if it is more than 3 months then we will log the message and stops proceeding further
			 * otherwise as usual will send last invoice date to enrollment API in order to get special enrollment events happened if any.
			 */
			/*employerEnrollmentID = 0;
				if(lastEmployerInvoices != null)
				{
					if(!isOlderThanLastThreeMonths(lastEmployerInvoices.getCreatedOn()))
					{
						LOGGER.info("For employerID: "+employerid+" the current invoice "+lastEmployerInvoices.getInvoiceNumber()+" is older than 3 months and hence not proceeds further due to businees reasons");
						return FinanceConstants.NUMERICAL_0;
					}
				}
			}*/

			if(lastEmployerInvoices!=null){
				LOGGER.info(" Last Employer Invoice Number: "+lastEmployerInvoices.getInvoiceNumber());
				DateTime currentDate = TSDateTime.getInstance(); 
				String periodCovered = "";
				if(currentDate.getDayOfMonth()==1){
					DateTime dt = TSDateTime.getInstance();
					DateTime dt1 = TSDateTime.getInstance();
					DateTimeFormatter fmt = DateTimeFormat.forPattern(FinanceConstants.DATE_FORMAT_MM_DD_YYYY);
					dt = dt.withDayOfMonth(1);
					dt1 = dt.plusMonths(1).withDayOfMonth(1).minusDays(1);
					periodCovered = (fmt.print(dt)+" to "+fmt.print(dt1));
				}
				else{
					DateTime dt = TSDateTime.getInstance();
					DateTime dt1 = TSDateTime.getInstance();
					DateTimeFormatter fmt = DateTimeFormat.forPattern(FinanceConstants.DATE_FORMAT_MM_DD_YYYY);
					dt = dt.plusMonths(1).withDayOfMonth(1);
					dt1 = dt.plusMonths(1).withDayOfMonth(1).minusDays(1);
					periodCovered = (fmt.print(dt)+" to "+fmt.print(dt1));
				}
				LOGGER.info("Getting givenperiodinvoice for periodcovered: "+periodCovered+" and employerid: "+employerid);
				EmployerInvoices givenperiodinvoice = employerInvoicesService.getGivenPeriodCoveredEmployerInvoice(periodCovered,employerid);
				if(givenperiodinvoice==null){

					int employerFeeCount = FinanceConstants.NUMERICAL_0;
					List<String> statuslist = new ArrayList<String>();
					statuslist.add(FinanceConstants.CONFIRM);
					statuslist.add(FinanceConstants.TERM);
					statuslist.add(FinanceConstants.PAYMENT_RECEIVED);
					statuslist.add(FinanceConstants.CANCEL);
					DateTime dateTime = TSDateTime.getInstance();
					dateTime = dateTime.plusMonths(1).dayOfMonth().withMaximumValue();
					Date nextmonthdate = dateTime.toDate();
					List<EnrollmentCurrentMonthDTO> enrollmentCurrentMnthList = financialMgmtUtils.getEnrollmentByCurrentMonth(lastEmployerInvoices.getCreatedOn(), nextmonthdate, statuslist, employerid, FinanceConstants.NUMERICAL_0);
					if(enrollmentCurrentMnthList != null && !enrollmentCurrentMnthList.isEmpty())
					{
						//List<EmployerInvoiceLineItems> employerInvoiceLineItemsList = getNormalEmployerInvoiceData(enrollmentCurrentMnthList, lastEmployerInvoices.getStatementDate(),FinanceConstants.DECISION_N,null,lastEmployerInvoices.getId());
						Map<String, Object> objectMap = getNormalEmployerInvoiceData(enrollmentCurrentMnthList, lastEmployerInvoices.getStatementDate(),FinanceConstants.DECISION_N,null,lastEmployerInvoices.getId());

						employerFeeCount = objectMap.get(FinanceConstants.EMPLOYER_FEE_COUNT) == null ? employerFeeCount : (Integer)objectMap.get(FinanceConstants.EMPLOYER_FEE_COUNT);				    		
						List<EmployerInvoiceLineItems> employerInvoiceLineItemsList = objectMap.get(FinanceConstants.LINE_ITEM_LIST) == null ? null : (List<EmployerInvoiceLineItems>)objectMap.get(FinanceConstants.LINE_ITEM_LIST);				    	

						if(employerInvoiceLineItemsList!=null && !employerInvoiceLineItemsList.isEmpty())
						{
							newEmployerInvoices = getEmployerInvoicesByLineItems(employerInvoiceLineItemsList, InvoiceType.NORMAL, employerEnrollmentID);
							newEmployerInvoices.setEmployer(employer);
						}
					}
					else
					{
						LOGGER.info("No Enrollment Found for current Month for employerID: "+employerid);
					}

					//Changed the conditional statement to support create invoice in case of TotalAmountDue == 0 :: HIX-30471
					if(newEmployerInvoices!=null){
						LOGGER.info("New Employer Invoice Number: "+newEmployerInvoices.getInvoiceNumber());

						//added the conditional statement if invoice TotalAmountDue == 0 set the PaidStatus=PAID :: HIX-30471
						if (newEmployerInvoices.getTotalAmountDue().compareTo(BigDecimal.ZERO) == FinanceConstants.NUMERICAL_0) {
							newEmployerInvoices.setPaidStatus(PaidStatus.PAID);
							newEmployerInvoices.setPaidDateTime(new TSDate());
							List<EmployerInvoiceLineItems> employerInvoiceLineItems =  newEmployerInvoices.getEmployerInvoiceLineItems();
							for (EmployerInvoiceLineItems employerInvoiceLineItem : employerInvoiceLineItems) {
								employerInvoiceLineItem.setPaidStatus(Status.PAID);
								employerInvoiceLineItem.setPaidDate(new TSDate());
								employerInvoiceLineItem.setPaymentReceived(employerInvoiceLineItem.getNetAmount());
							}
							newEmployerInvoices.setEmployerInvoiceLineItems(employerInvoiceLineItems);
						}
						synchronized(employerInvoicesService)
						{
							EmployerInvoices periodCoveredInvoice = employerInvoicesService.getGivenPeriodCoveredEmployerInvoice(periodCovered,employerid);
							if(periodCoveredInvoice==null)
							{
								final BigDecimal employerFee = calculateEmployerFee(newEmployerInvoices, employerFeeCount);
								EmployerInvoices newemployerInvoices = employerInvoicesService.saveEmployerInvoices(newEmployerInvoices);
								invoiceid = newemployerInvoices.getId();

								DateFormat df = new SimpleDateFormat("MMM");
								Date date = new TSDate();
								String invoiceNumber = "INV-"+df.format(date).toUpperCase()+(TSCalendar.getInstance().get(Calendar.YEAR))+"-"+invoiceid;
								LOGGER.info("Update invoice number with invoice no value:"+invoiceNumber+"invoice id:"+invoiceid);
								employerInvoicesService.updateInvoiceNumber(invoiceNumber, invoiceid);

								//making last PAID invoice as inactive 
								if(lastEmployerInvoices.getPaidStatus().equals(EmployerInvoices.PaidStatus.PAID)){
									lastEmployerInvoices.setIsActive(FinanceConstants.DECISION_N);
									employerInvoicesService.saveEmployerInvoices(lastEmployerInvoices);
								}

								if(employerFee.compareTo(BigDecimal.ZERO) != 0)
								{
									createEmployerExchangeFeesWithNewInvoiceIdNEmployerFee(employerFee, newemployerInvoices);
								}							    
							}
						}
					}
				}
			}
		}
		catch(GIException e){
			LOGGER.error("createEmployerInvoice(-) failed for employerid: "+employerid);
			LOGGER.error("",e);
			throw new FinanceException(e.getMessage(), e);
		}
		LOGGER.info("createEmployerInvoice(-) ends successfully with invoiceid: "+invoiceid+" for employerid: "+employerid);
		return invoiceid;
	}

	private BigDecimal calculateEmployerFee(final EmployerInvoices newEmployerInvoices, int employerFeeCount) throws GIException
	{
		BigDecimal employerFee = FinancialMgmtGenericUtil.getDefaultBigDecimal();
		BigDecimal employerFlatFeeBigDecimal = FinanceGeneralUtility.getConfigBigDecimalAmount(FinanceConfiguration.FinanceConfigurationEnum.EMPLOYER_FLAT_FEE_PER_ENROLLMENT);
		/*
		 * If IS_EMPLOYER_FEE_REQUIRED is TRUE & EMPLOYER_FLAT_FEE_PER_ENROLLMENT is > than 0 then calculate Employer Fee and add it to Employer Invoice 
		 */
		if(FinanceConstants.TRUE.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_EMPLOYER_FEE_REQUIRED)) &&
				employerFlatFeeBigDecimal.compareTo(BigDecimal.ZERO) == 1)
		{	employerFee = FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(employerFeeCount), employerFlatFeeBigDecimal);			

		newEmployerInvoices.setExchangeFees(newEmployerInvoices.getExchangeFees() == null ? employerFee : FinancialMgmtGenericUtil.add(employerFee, newEmployerInvoices.getExchangeFees()));
		newEmployerInvoices.setTotalAmountDue(FinancialMgmtGenericUtil.add(newEmployerInvoices.getExchangeFees(), newEmployerInvoices.getPremiumThisPeriod()));
		}
		return employerFee;
	}

	private EmployerExchangeFees createEmployerExchangeFeesWithNewInvoiceIdNEmployerFee(final BigDecimal employerFee, final EmployerInvoices newEmployerInvoices) throws GIException
	{
		BigDecimal employerFlatFeeBigDecimal = FinanceGeneralUtility.getConfigBigDecimalAmount(FinanceConfiguration.FinanceConfigurationEnum.EMPLOYER_FLAT_FEE_PER_ENROLLMENT);

		if(FinanceConstants.TRUE.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_EMPLOYER_FEE_REQUIRED)) &&
				employerFlatFeeBigDecimal.compareTo(BigDecimal.ZERO) == 1)
		{
			EmployerExchangeFees employerExchangeFees = new EmployerExchangeFees();
			employerExchangeFees.setAmount(employerFee);
			employerExchangeFees.setEmployerInvoices(newEmployerInvoices);
			employerExchangeFees.setFeeTypeCode(EmployerExchangeFees.ExchangeFeeTypeCode.EMPLOYER_USER_FEE);

			return employerExchangeFeeService.saveEmployerExchangeFees(employerExchangeFees);
		}
		return null;
	}


	/**
	 * Service Implementation to create Employer Binder Invoice.
	 * @param employerid int
	 * @param nextMonthStartDate String
	 * @param periodCovered String
	 * @return
	 * @throws FinanceException
	 */
	@Override	
	public int createEmployerBinderInvoice(int employerId, int employerEnrollmentId, Date nextMonthStartDate, String periodCovered) throws FinanceException
	{
		LOGGER.info("createEmployerBinderInvoice(-) method started with employerid: "
				+ employerId
				+ " NextMonthStartDate: "
				+ nextMonthStartDate
				+ " PeriodCovered: " + periodCovered);

		Employer employer = new Employer();
		employer.setId(employerId);
		/*EmployerInvoices employerInvoices = employerInvoicesService.findEmployerInvoicesByEmployerAndInvoiceType(employer, InvoiceType.BINDER_INVOICE, "Y");*/
		EmployerInvoices lastActiveEmployerInvoice = employerInvoicesService.findLastActiveInvoice(employerId);
		/*if (lastActiveEmployerInvoice == null || (lastActiveEmployerInvoice != null && 
				!lastActiveEmployerInvoice.getInvoiceType().equals(EmployerInvoices.InvoiceType.BINDER_INVOICE) 
				 && !lastActiveEmployerInvoice.getPeriodCovered().equals(periodCovered))) 
		{*/
		LOGGER.info("There is no EmployerBinderInvoice present for employerid: "
				+ employerId + " PeriodCovered: " + periodCovered);
		int invoiceid = 0;
		String binderInvoice = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_BINDER_INVOICE);

		LOGGER.info("Default BinderInvoice Value/Employerid: "+ binderInvoice + "/" + employerId);
		try
		{
			EmployerInvoices newEmployerInvoices = null;

			if (employerEnrollmentId != FinanceConstants.NUMERICAL_0 && FinanceConstants.YES.equalsIgnoreCase(binderInvoice) )
			{
				/**
				 * Condition check to find if Binder Invoice Pre-Exists or not.
				 */
				if(lastActiveEmployerInvoice == null || !isBinderInvoiceExistForEnrollment(employerId, employerEnrollmentId))
				{
					/*
					 * Fetching LastActiveInvoice for covering special event reported between last invoice and current one.
					 */
					/*EmployerInvoices lastActiveInvoice = employerInvoicesService.findLastActiveInvoice(employerId);*/

					List<String> statusList = new ArrayList<String>();
					Date lastInvoiceCreationTimeStamp = null;
					Date nextMonthEndDate = null;
					if (lastActiveEmployerInvoice != null)
					{
						/*
						 * Setting List<String> StatusList, NextMonthEndDate and LastInvoiceTimeStamp when LastActiveInvoice is not null
						 */
						statusList.add(FinanceConstants.CONFIRM);
						statusList.add(FinanceConstants.TERM);
						statusList.add(FinanceConstants.PAYMENT_RECEIVED);
						statusList.add(FinanceConstants.CANCEL);

						nextMonthEndDate = TSDateTime.getInstance().plusMonths(1).dayOfMonth().withMaximumValue().toDate();

						if (lastActiveEmployerInvoice != null && lastActiveEmployerInvoice.getCreatedOn() != null)
							/*&& isOlderThanLastThreeMonths(lastActiveEmployerInvoice.getCreatedOn())) */
						{
							lastInvoiceCreationTimeStamp = lastActiveEmployerInvoice.getCreatedOn();
						}
					}

					Map<String, Set<EnrollmentCurrentMonthDTO>> enrollmentCurrentMonthDTOMap = financialMgmtUtils
							.findEnrollmentByEmployerDetails(employerId, employerEnrollmentId, statusList,
									lastInvoiceCreationTimeStamp, nextMonthEndDate);

					int employerFeeCount = FinanceConstants.NUMERICAL_0;
					if (enrollmentCurrentMonthDTOMap != null && !enrollmentCurrentMonthDTOMap.isEmpty()) 
					{
						LOGGER.info("Received EnrollmentCurrentMonthDTOMAP from Enrollment Rest call .....");
						SortedSet<EnrollmentCurrentMonthDTO> enrollmentNewSet = 
								(SortedSet<EnrollmentCurrentMonthDTO>) enrollmentCurrentMonthDTOMap.get(FinanceConstants.NEW);
						SortedSet<EnrollmentCurrentMonthDTO> enrollmentOldSet = 
								(SortedSet<EnrollmentCurrentMonthDTO>) enrollmentCurrentMonthDTOMap.get(FinanceConstants.OLD);

						List<EnrollmentCurrentMonthDTO> newPendingList = new ArrayList<EnrollmentCurrentMonthDTO>();
						if (enrollmentNewSet != null && !enrollmentNewSet.isEmpty())
						{
							LOGGER.info("Enrollment present for Employerid: " + employerId);
							int noOfEsigApplication = 0;

							for (EnrollmentCurrentMonthDTO enrollmentCurrentMonthDTO : enrollmentNewSet)
							{
								if (FinanceConstants.PENDING.equalsIgnoreCase(enrollmentCurrentMonthDTO.getEnrollmentStatusValue())
										|| FinanceConstants.CANCEL.equalsIgnoreCase(enrollmentCurrentMonthDTO.getEnrollmentStatusValue())
										|| FinanceConstants.TERM.equalsIgnoreCase(enrollmentCurrentMonthDTO.getEnrollmentStatusValue()))
								{
									LOGGER.info("Checking Enrollment Status for Pending or Cancel");
									noOfEsigApplication++;
									if (FinanceConstants.PENDING.equalsIgnoreCase(enrollmentCurrentMonthDTO.getEnrollmentStatusValue()))
									{
										newPendingList.add(enrollmentCurrentMonthDTO);
									}
								}
							}

							/*
							 * If Received EnrollmentNewSet Contains only Pending|Cancel|Terminated enrollment 
							 * then only proceed with invoice generation. 
							 */
							if (noOfEsigApplication == enrollmentNewSet.size())
							{
								LOGGER.info("Number of ESIG application matched with Enrollment Size: "+noOfEsigApplication);
								List<EmployerInvoiceLineItems> oldEmployerInvoiceLineItemsList = new ArrayList<EmployerInvoiceLineItems>();
								/*
								 * If enrollmentOldSet is not Empty then Process the received
								 * special enrollment reported Enrollment by getNormalEmployerInvoiceData(-,-,-,-,-)
								 */
								if (enrollmentOldSet != null && !enrollmentOldSet.isEmpty())
								{
									LOGGER.info("Enrollment OLD Set exists for EmployerID: "+employerId);
									List<EnrollmentCurrentMonthDTO> enrollmentCurrentMonthDTOList = 
											new ArrayList<EnrollmentCurrentMonthDTO>(enrollmentOldSet);
									//oldEmployerInvoiceLineItemsList = getNormalEmployerInvoiceData(enrollmentCurrentMonthDTOList,lastActiveEmployerInvoice.getStatementDate(), FinanceConstants.DECISION_N, null,lastActiveEmployerInvoice.getId());


									Map<String, Object> objectMap = getNormalEmployerInvoiceData(enrollmentCurrentMonthDTOList,lastActiveEmployerInvoice.getStatementDate(), FinanceConstants.DECISION_N, null,lastActiveEmployerInvoice.getId());

									employerFeeCount = objectMap.get(FinanceConstants.EMPLOYER_FEE_COUNT) == null ? employerFeeCount : (Integer)objectMap.get(FinanceConstants.EMPLOYER_FEE_COUNT);				    		
									oldEmployerInvoiceLineItemsList = objectMap.get(FinanceConstants.LINE_ITEM_LIST) == null ? oldEmployerInvoiceLineItemsList : (List<EmployerInvoiceLineItems>)objectMap.get(FinanceConstants.LINE_ITEM_LIST);
								}

								LOGGER.info("Calling GetBinderEmployerInvoiceData for Emploryer: " + employerId);
								List<EmployerInvoiceLineItems> employerInvoiceLineItemsList = getBinderEmployerInvoiceData(
										newPendingList, FinanceConstants.DECISION_N, null, periodCovered);

								if (employerInvoiceLineItemsList != null && !employerInvoiceLineItemsList.isEmpty())
								{
									employerInvoiceLineItemsList.addAll(oldEmployerInvoiceLineItemsList);
									newEmployerInvoices = getEmployerInvoicesByLineItems(
											employerInvoiceLineItemsList, InvoiceType.BINDER_INVOICE, employerEnrollmentId);

									newEmployerInvoices.setEmployer(employer);

									employerFeeCount = employerFeeCount + employerInvoiceLineItemsList.size();
								}
							}
						}
					}							
					if (newEmployerInvoices != null)
					{
						/*
						 * Synchronized block added to be sure that at a single instance only one Invoice is generated.
						 */
						synchronized (employerInvoicesService)
						{
							EmployerInvoices employerBinderInvoicesRecheck = employerInvoicesService
									.findInvoiceByTypeAndPeriodCovered(employerId, InvoiceType.BINDER_INVOICE, FinanceConstants.DECISION_Y, periodCovered);

							if (employerBinderInvoicesRecheck == null) 
							{
								LOGGER.info("The Id value of newEmployerInvoices:"
										+ newEmployerInvoices.getId());
								final BigDecimal employerFee = calculateEmployerFee(newEmployerInvoices, employerFeeCount);
								EmployerInvoices newemployerInvoices = employerInvoicesService.saveEmployerInvoices(newEmployerInvoices);
								invoiceid = newemployerInvoices.getId();

								DateFormat df = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MMM);
								Date date = new TSDate();
								String invoiceNumber = "INV-" + df.format(date).toUpperCase()
										+ (TSCalendar.getInstance().get(Calendar.YEAR)) + "-"
										+ invoiceid;
								employerInvoicesService.updateInvoiceNumber(invoiceNumber, invoiceid);

								/*
								 * Marking previous active invoice as InActive when the invoice is PAID.
								 */
								if (lastActiveEmployerInvoice != null && lastActiveEmployerInvoice.getPaidStatus().equals(EmployerInvoices.PaidStatus.PAID)) 
								{
									LOGGER.info("Marking previous Employerinvoice with ID: "+lastActiveEmployerInvoice.getId() +" as InActive ");
									lastActiveEmployerInvoice.setIsActive(FinanceConstants.DECISION_N);
									employerInvoicesService.saveEmployerInvoices(lastActiveEmployerInvoice);
								}										

								if(employerFee.compareTo(BigDecimal.ZERO) != 0)
								{
									createEmployerExchangeFeesWithNewInvoiceIdNEmployerFee(employerFee, newemployerInvoices);
								}
							}
						}
					}
				}
				else
				{
					/**
					 * Suppressing this condition as it is known
					 */
					LOGGER.info("For EmployerID: " +employerId+ " Binder Invoice already exists for this EmployerEnrollment: "+employerEnrollmentId);
				}
			}
			//}

		}
		catch (GIException gie) 
		{
			if(gie != null && gie.getErrorCode() == FinanceConstants.EMPLOYER_ENROLLMENT_NOT_PRESENT)
			{
				/**
				 * Suppressing GHIX-FAILURE for Normal Invoice Where Special Enrollment can be created after Enrollment termination.
				 */
				LOGGER.info("No Active Employer Enrollment found for employer_Id: "+employerId+" Where next Month Start Date "+nextMonthStartDate+" Falls in coverage. Exception :"+gie);
			}
			else
			{
				LOGGER.error("createEmployerBinderInvoice(-) failed for Employer: "+ employerId + " cause: "+gie);
				throw new FinanceException(gie.getMessage(), gie);
			}
		}
		LOGGER.info("createEmployerBinderInvoice(-) ends successfully with invoiceid: " + invoiceid + " Emploryer: " + employerId);
		return invoiceid;
		/*}

		 * FinanceException is thrown when already EmployerInvoice exists for same Period Covered.

		else
		{
			LOGGER.warn("For Employer_ID : " + employerId + " already BinderInvoice exists with InvoiceID: "
					+ lastActiveEmployerInvoice.getId());
			throw new FinanceException("For Employer_ID : " + employerId + " already BinderInvoice exists with InvoiceID: "
					+ lastActiveEmployerInvoice.getId());
		}*/
	}

	/**
	 * Method reissueInvoice.
	 * @param employerid int
	 * @param invoiceid int
	 * @return String
	 * @throws GIException
	 * @throws FinanceException 
	 * @see com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService#reissueInvoice(int, int)
	 */
	@Override	
	public String reissueInvoice(int employerid, int invoiceid) throws GIException, FinanceException
	{
		LOGGER.info("reissueInvoice(-,-) method started with employerid:"
				+ employerid + " invoiceid:" + invoiceid);
		String response = "";
		Employer employer = new Employer();
		employer.setId(employerid);
		Integer employerEnrollmentID = 0;
		String binderInvoice = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_BINDER_INVOICE);
		// it is assumed that only last active invoice will be allowed for reissue
		EmployerInvoices lastEmployerInvoicesBeforeReissue = employerInvoicesService
				.findLastInvoiceForReissue(employerid, invoiceid);

		int employerFeeCount = FinanceConstants.NUMERICAL_0;
		try
		{
			EmployerInvoices reissueInvoice = employerInvoicesService.findEmployerInvoicesById(invoiceid);

			EmployerInvoices newEmployerInvoices = null;
			if(reissueInvoice != null)
			{

			// only for due status invoice we allow reissue
			if (InvoiceType.NORMAL == reissueInvoice.getInvoiceType())
			{
				boolean isEmployerEnrollmentExist = false;
				Date firstOfNextMonth = DateTime.now()
						.plusMonths(FinanceConstants.NUMERICAL_1).dayOfMonth().withMinimumValue().toDate();
				/*
				 * Map<String, Object> employerEnrollmentDetails= financialMgmtUtils.findEmployerEnrollmentDetails(employerid, 
				 *  EmployerEnrollment.Status.ACTIVE, firstOfNextMonth);
				 */
				Map<String, Object> employerEnrollmentDetails = null;
				/**
				 * Suppressing GHIX-FAILURE response as after coverage
				 * termination there may be special enrollment Or after Employer
				 * Termination special enrollment can be reported by employee.
				 */
				try {
					employerEnrollmentDetails = financialMgmtUtils.findEmployerEnrollmentDetails(employerid,
							EmployerEnrollment.Status.ACTIVE, firstOfNextMonth);
				}
				catch (GIException gie)
				{
					if (gie != null	&& gie.getErrorCode() == FinanceConstants.EMPLOYER_ENROLLMENT_NOT_PRESENT)
					{
						/**
						 * Suppressing GHIX-FAILURE for Normal Invoice Where
						 * Special Enrollment can be created after Enrollment
						 * termination.
						 */
						LOGGER.info("No Active Employer Enrollment found for employer_Id: "+ employerid
								+ " Where next Month Start Date "+ firstOfNextMonth
								+ " Falls in coverage. Exception :" + gie);
					}
					else
					{
						throw new GIException(gie.getErrorMsg());
					}

				}

				if (employerEnrollmentDetails != null && !employerEnrollmentDetails.isEmpty())
				{
					employerEnrollmentID = (Integer) employerEnrollmentDetails
							.get(FinanceConstants.EMPLOYER_ENROLLMENT_ID_KEY);
				}

				if (employerEnrollmentID != 0) {
					// Check whether active employerEnrollment is available or
					// not for employerEnrollmentID which received from SHOP
					// API, if not throw exception.
					List<EmployerInvoiceLineItems> lastEmployerInvoiceLineItems = employerInvoiceLineItemsService.getInvoiceLineItem(reissueInvoice);
						for (EmployerInvoiceLineItems lineItem : lastEmployerInvoiceLineItems)
						{
							if (employerEnrollmentID.equals(lineItem.getEmployerEnrollmentId()))
							{
								isEmployerEnrollmentExist = true;
								break;
							}
						}

					if (!isEmployerEnrollmentExist)
					{
						throw new FinanceException("No active employerEnrollment found to proceed further for employerID:"
								+ employerid + " with Employer_ErnollmentID:" + employerEnrollmentID);
					}
				}
				else
				{
					/*
					 * If employer is not eligible for next month coverage then we will not receive employerEnrollmentID from SHOP. Then
					 * check last active invoice date if it is more than 3 months then we will log the message and stops proceeding
					 * further otherwise as usual will send last invoice date to enrollment API in order to get special enrollment events
					 * happened if any.
					 */
					employerEnrollmentID = 0;
					if (!isOlderThanLastThreeMonths(reissueInvoice.getCreatedOn()))
					{
						/*if (!isOlderThanLastThreeMonths(reissueInvoice.getCreatedOn()))
						{*/
						LOGGER.info("For employerID: "+ employerid + " the current invoice "
								+ reissueInvoice.getInvoiceNumber() + " is older than 3 months and hence not proceeds further due to businees reasons");
						return "For employerID: " + employerid + " the current invoice "
						+ reissueInvoice.getInvoiceNumber() + " is older than 3 months and hence not proceeds further due to businees reasons";
						/*}*/
					}
				}

				List<String> statuslist = new ArrayList<String>();
				statuslist.add(FinanceConstants.CONFIRM);
				statuslist.add(FinanceConstants.TERM);
				statuslist.add(FinanceConstants.PAYMENT_RECEIVED);
				statuslist.add(FinanceConstants.CANCEL);
				DateTime currentdate = TSDateTime.getInstance();
				DateTime reissuedate = new DateTime(
						reissueInvoice.getStatementDate());
				DateTime dateTime = TSDateTime.getInstance();
				Date nextmonthdate = null;

				if (currentdate.getMonthOfYear() == reissuedate.getMonthOfYear())
				{
					dateTime = dateTime.plusMonths(1).dayOfMonth().withMaximumValue();
					nextmonthdate = dateTime.toDate();
				}
				else
				{
					dateTime = dateTime.dayOfMonth().withMaximumValue();
					nextmonthdate = dateTime.toDate();
				}

				List<EnrollmentCurrentMonthDTO> enrollmentCurrentMonthDTOList = financialMgmtUtils
						.getEnrollmentByCurrentMonth(lastEmployerInvoicesBeforeReissue.getCreatedOn(), 
								nextmonthdate, statuslist, employerid, FinanceConstants.NUMERICAL_0);
				if (enrollmentCurrentMonthDTOList != null && !enrollmentCurrentMonthDTOList.isEmpty())
				{
					/*List<EmployerInvoiceLineItems> employerInvoiceLineItemsList = getNormalEmployerInvoiceData(
							enrollmentCurrentMonthDTOList, lastEmployerInvoicesBeforeReissue.getStatementDate(),
							FinanceConstants.DECISION_Y, reissueInvoice.getStatementDate(), lastEmployerInvoicesBeforeReissue.getId());*/

					Map<String, Object> objectMap = getNormalEmployerInvoiceData(
							enrollmentCurrentMonthDTOList, lastEmployerInvoicesBeforeReissue.getStatementDate(),
							FinanceConstants.DECISION_Y, reissueInvoice.getStatementDate(), lastEmployerInvoicesBeforeReissue.getId());

					employerFeeCount = objectMap.get(FinanceConstants.EMPLOYER_FEE_COUNT) == null ? employerFeeCount : (Integer)objectMap.get(FinanceConstants.EMPLOYER_FEE_COUNT);				    		
					List<EmployerInvoiceLineItems> employerInvoiceLineItemsList = objectMap.get(FinanceConstants.LINE_ITEM_LIST) == null ? null : (List<EmployerInvoiceLineItems>)objectMap.get(FinanceConstants.LINE_ITEM_LIST);

					if (employerInvoiceLineItemsList != null && !employerInvoiceLineItemsList.isEmpty())
					{
						newEmployerInvoices = getEmployerInvoicesByLineItems(employerInvoiceLineItemsList,
								InvoiceType.NORMAL, employerEnrollmentID);
						newEmployerInvoices.setEmployer(employer);
					}
				}
			}
			else if (FinanceConstants.YES.equalsIgnoreCase(binderInvoice))
			{
				LOGGER.info("in Binder Invoice reissue flow for EmployerId: "
						+ employerid);
				Date nextMonthStartDate = DateTime.parse(TSDateTime.getInstance().plusMonths(1).withDayOfMonth(1)
						.toString()).toDate();

				Map<String, Object> responseDataMap = financialMgmtUtils
						.findEmployerEnrollmentDetails(employerid, EmployerEnrollment.Status.PENDING, nextMonthStartDate);
				if (responseDataMap != null && !responseDataMap.isEmpty())
				{
					String isOpenenrollmentOver = (String) responseDataMap.get(FinanceConstants.OPEN_ENROLLMENT_OVER_KEY);
					Integer employerEnrollmentId = (Integer) responseDataMap .get(FinanceConstants.EMPLOYER_ENROLLMENT_ID_KEY);

					if (employerEnrollmentId != null && !employerEnrollmentId.equals(FinanceConstants.NUMERICAL_0)
							&& StringUtils.isNotEmpty(isOpenenrollmentOver) && FinanceConstants.YES.equalsIgnoreCase(isOpenenrollmentOver)) 
					{
						List<String> statusList = new ArrayList<String>();
						statusList.add(FinanceConstants.CONFIRM);
						statusList.add(FinanceConstants.TERM);
						statusList.add(FinanceConstants.PAYMENT_RECEIVED);
						statusList.add(FinanceConstants.CANCEL);

						Date nextMonthEndDate = TSDateTime.getInstance().plusMonths(1)
								.dayOfMonth().withMaximumValue().toDate();
						Date lastInvoiceCreationTimeStamp = null;

						if (lastEmployerInvoicesBeforeReissue != null
								&& isOlderThanLastThreeMonths(lastEmployerInvoicesBeforeReissue.getCreatedOn()))
						{
							lastInvoiceCreationTimeStamp = lastEmployerInvoicesBeforeReissue.getCreatedOn();
						}
						Map<String, Set<EnrollmentCurrentMonthDTO>> enrollmentCurrentMonthDTOMap = financialMgmtUtils
								.findEnrollmentByEmployerDetails(employerid, employerEnrollmentId, statusList,
										lastInvoiceCreationTimeStamp, nextMonthEndDate);

						if (enrollmentCurrentMonthDTOMap != null && !enrollmentCurrentMonthDTOMap.isEmpty()) 
						{
							SortedSet<EnrollmentCurrentMonthDTO> enrollmentNewSet = (SortedSet<EnrollmentCurrentMonthDTO>) enrollmentCurrentMonthDTOMap
									.get(FinanceConstants.NEW);
							SortedSet<EnrollmentCurrentMonthDTO> enrollmentOldSet = (SortedSet<EnrollmentCurrentMonthDTO>) enrollmentCurrentMonthDTOMap
									.get(FinanceConstants.OLD);

							List<EnrollmentCurrentMonthDTO> newPendingList = new ArrayList<EnrollmentCurrentMonthDTO>();
							if (enrollmentNewSet != null && !enrollmentNewSet.isEmpty())
							{
								int noOfEsigApplication = 0;
								for (EnrollmentCurrentMonthDTO application : enrollmentNewSet) 
								{
									if (FinanceConstants.PENDING.equalsIgnoreCase(application.getEnrollmentStatusValue())
											|| FinanceConstants.CANCEL.equalsIgnoreCase(application.getEnrollmentStatusValue())
											|| FinanceConstants.TERM.equalsIgnoreCase(application.getEnrollmentStatusValue()))
									{
										LOGGER.info("Checking Enrollment Status for Pending or Cancel...");
										noOfEsigApplication++;
										if (FinanceConstants.PENDING.equalsIgnoreCase(application.getEnrollmentStatusValue()))
										{
											newPendingList.add(application);
										}
									}
								}
								// if all enrollment status esign done then we prepare binder invoice
								if (noOfEsigApplication == enrollmentNewSet.size())
								{
									List<EmployerInvoiceLineItems> employerInvoiceLineItemsList = getBinderEmployerInvoiceData(
											newPendingList, FinanceConstants.DECISION_Y, reissueInvoice, reissueInvoice.getPeriodCovered());
									List<EmployerInvoiceLineItems> oldEmployerInvoiceLineItemsList = new ArrayList<EmployerInvoiceLineItems>();
									if (enrollmentOldSet != null && !enrollmentOldSet.isEmpty())
									{
										List<EnrollmentCurrentMonthDTO> enrollmentCurrentMnthList = new ArrayList<EnrollmentCurrentMonthDTO>(
												enrollmentOldSet);
										/*oldEmployerInvoiceLineItemsList = getNormalEmployerInvoiceData(enrollmentCurrentMnthList,
												lastEmployerInvoicesBeforeReissue.getStatementDate(),
												FinanceConstants.DECISION_Y, reissueInvoice.getStatementDate(),
												lastEmployerInvoicesBeforeReissue.getId());*/

										Map<String, Object> objectMap = getNormalEmployerInvoiceData(enrollmentCurrentMnthList,
												lastEmployerInvoicesBeforeReissue.getStatementDate(),
												FinanceConstants.DECISION_Y, reissueInvoice.getStatementDate(),
												lastEmployerInvoicesBeforeReissue.getId());

										employerFeeCount = objectMap.get(FinanceConstants.EMPLOYER_FEE_COUNT) == null ? employerFeeCount : (Integer)objectMap.get(FinanceConstants.EMPLOYER_FEE_COUNT);				    		
										oldEmployerInvoiceLineItemsList = objectMap.get(FinanceConstants.LINE_ITEM_LIST) == null ? oldEmployerInvoiceLineItemsList : (List<EmployerInvoiceLineItems>)objectMap.get(FinanceConstants.LINE_ITEM_LIST);
									}
									if (employerInvoiceLineItemsList != null && !employerInvoiceLineItemsList.isEmpty())
									{
										employerInvoiceLineItemsList.addAll(oldEmployerInvoiceLineItemsList);

										newEmployerInvoices = getEmployerInvoicesByLineItems(employerInvoiceLineItemsList,
												InvoiceType.BINDER_INVOICE, employerEnrollmentId);
										newEmployerInvoices.setEmployer(employer);

										employerFeeCount = employerFeeCount + employerInvoiceLineItemsList.size();
									}
								}
							}
						}
					}
				}
			}


			synchronized (employerInvoicesService) 
			{
				reissueInvoice = employerInvoicesService.findEmployerInvoiceByIdAndPaidStatusAndIsActiveAndIsAdjusted(invoiceid, PaidStatus.DUE, FinanceConstants.Y, IsManualAdjustment.N);
				if(reissueInvoice != null && reissueInvoice.getIsActive().equalsIgnoreCase(FinanceConstants.Y))
				{
					if (newEmployerInvoices != null 
							&& !(newEmployerInvoices.getTotalAmountDue().compareTo(BigDecimal.ZERO) == FinanceConstants.NUMERICAL_0))
					{						

						AccountUser user = FinanceGeneralUtility.getAccountUser(userService);
						List<EmployerExchangeFees> employerExchangeFeesList = employerExchangeFeeService.findEmpExchangeFeesByInvIdExceptEmpUserFee(reissueInvoice.getId());
						setNewExchangeFeeAndTotalAmountDue(newEmployerInvoices, employerExchangeFeesList);

						setOriginalInvoicePeriod(newEmployerInvoices, reissueInvoice);
						EmployerInvoices newemployerInvoices = employerInvoicesService
								.saveEmployerInvoices(newEmployerInvoices);

						String newinvoiceNumber = reissueInvoice.getInvoiceNumber();
						newinvoiceNumber = newinvoiceNumber.substring(0, newinvoiceNumber.lastIndexOf('-'));
						newinvoiceNumber = newinvoiceNumber + "-" + newemployerInvoices.getId();
						employerInvoicesService.updateInvoiceNumber(newinvoiceNumber, newemployerInvoices.getId());

						// making last invoice as inactive and set the reissued invoice id in old invoice
						reissueInvoice.setIsActive(FinanceConstants.DECISION_N);
						reissueInvoice.setReissueId(newemployerInvoices.getId());
						reissueInvoice.setLastUpdatedBy(user.getId());
						employerInvoicesService.saveEmployerInvoices(reissueInvoice);


						//populate employer_exchange_fees table with new invoice_id(after reissue). 
						createEmployerExchangeFeesWithNewInvoiceId(newemployerInvoices, employerExchangeFeesList);

						BigDecimal employerFee = calculateEmployerFee(newemployerInvoices, employerFeeCount);
						if(employerFee.compareTo(BigDecimal.ZERO) != FinanceConstants.NUMERICAL_0)
						{
							createEmployerExchangeFeesWithNewInvoiceIdNEmployerFee(employerFee, newemployerInvoices);
						}
						response = newemployerInvoices.getId() + "";
					}
					else
					{
						throw new FinanceException("For EmployerID: " + employerid
								+ " no proper enrollment is available for reissue.");
					}
				}
				else
				{
					LOGGER.info("For InvoiceID: "+invoiceid +" already invoice is reissued refer invoiceID: "+(reissueInvoice != null ? reissueInvoice.getReissueId(): null));
				}
			}
		  }
			else
			{
				throw new FinanceException("Unable to Find Invoice for Reissue with InvoiceId: "+invoiceid);
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Exception caught in Reissue invoice flow: ", e);
			throw new FinanceException(e);
		}
		LOGGER.info("reissueInvoice(-) ends successfully with response:"
				+ response);
		return response;
	}

	@SuppressWarnings("all")
	private void createEmployerExchangeFeesWithNewInvoiceId(final EmployerInvoices newemployerInvoices,  final List<EmployerExchangeFees> employerExchangeFeesList)
	{
		EmployerExchangeFees newEmployerExchangeFees = null;
		final List<EmployerExchangeFees> newEmployerExchangeFeesList = new ArrayList();   	

		if(employerExchangeFeesList != null && !employerExchangeFeesList.isEmpty())
		{
			for(EmployerExchangeFees eachEmployerExchangeFees : employerExchangeFeesList)
			{
				newEmployerExchangeFees = new EmployerExchangeFees();
				newEmployerExchangeFees.setAmount(eachEmployerExchangeFees.getAmount());
				newEmployerExchangeFees.setEmployerInvoices(newemployerInvoices);
				newEmployerExchangeFees.setFeeTypeCode(eachEmployerExchangeFees.getFeeTypeCode());

				newEmployerExchangeFeesList.add(newEmployerExchangeFees);
			}
			employerExchangeFeeService.saveEmployerExchangeFeesList(newEmployerExchangeFeesList);    		
		}
	}

	/**
	 * Method setOriginalInvoicePeriod.
	 * @param newEmployerInvoices EmployerInvoices
	 * @param reissueInvoice EmployerInvoices
	 * @return EmployerInvoices
	 */
	private EmployerInvoices setOriginalInvoicePeriod(EmployerInvoices newEmployerInvoices,EmployerInvoices reissueInvoice){
		LOGGER.info("setOriginalInvoicePeriod(-,-) method started with newEmployerInvoices:"+newEmployerInvoices+" reissueInvoice:"+reissueInvoice);
		newEmployerInvoices.setPeriodCovered(reissueInvoice.getPeriodCovered());
		newEmployerInvoices.setPaymentDueDate(reissueInvoice.getPaymentDueDate());
		List<EmployerInvoiceLineItems> employerInvoiceLineItemsList = newEmployerInvoices.getEmployerInvoiceLineItems();
		for (int i=0;i<employerInvoiceLineItemsList.size();i++){
			EmployerInvoiceLineItems newemployerInvoiceLineItems = employerInvoiceLineItemsList.get(i);
			newemployerInvoiceLineItems.setPeriodCovered(reissueInvoice.getPeriodCovered());
		}
		LOGGER.info("setOriginalInvoicePeriod(-) ends successfully with newEmployerInvoices:"+newEmployerInvoices.getInvoiceNumber());
		return newEmployerInvoices;
	}

	/**
	 * Method set new exchange fee and new total amount due.
	 * @param newEmployerInvoices EmployerInvoices
	 * @param employerExchangeFeesList List<EmployerExchangeFees>
	 * @return EmployerInvoices
	 */
	private EmployerInvoices setNewExchangeFeeAndTotalAmountDue(final EmployerInvoices newEmployerInvoices, final List<EmployerExchangeFees> employerExchangeFeesList){
		LOGGER.info("setNewExchangeFeeAndTotalAmountDue(-,-) method started with newEmployerInvoices id:"+newEmployerInvoices.getId());

		//BigDecimal totalExchangeFee = employerExchangeFeeService.getTotalExchangeFeesByInvoiceId(reissueInvoice.getId());

		BigDecimal totalExchangeFee = FinancialMgmtGenericUtil.getDefaultBigDecimal();
		if(employerExchangeFeesList != null && !employerExchangeFeesList.isEmpty())
		{
			for(EmployerExchangeFees eachEmployerExchangeFees: employerExchangeFeesList)
			{
				totalExchangeFee = FinancialMgmtGenericUtil.add(totalExchangeFee, eachEmployerExchangeFees.getAmount());
			}

			BigDecimal newExchangeFee = FinancialMgmtGenericUtil.add(totalExchangeFee, newEmployerInvoices.getExchangeFees());
			BigDecimal newTotalAmountDue =  FinancialMgmtGenericUtil.add(newExchangeFee, newEmployerInvoices.getPremiumThisPeriod());

			newEmployerInvoices.setTotalAmountDue(newTotalAmountDue);
			newEmployerInvoices.setExchangeFees(newExchangeFee);
		}
		LOGGER.info("setNewExchangeFeeAndTotalAmountDue(-) ends successfully...");
		return newEmployerInvoices;
	}

	/**
	 * Constructs new EmployerInvoices (newEmployerInvoices) object from line items
	 * by calculating sum of each line item net_amount, retro_adjustment_amount if any and sets to employer invoice object.
	 * Sets payment due date based on invoice type(Binder or Normal).
	 */
	private EmployerInvoices getEmployerInvoicesByLineItems(List<EmployerInvoiceLineItems> employerInvoiceLineItemsList, InvoiceType invoiceType, int employerEnrollmentID)
	{
		AccountUser user = FinanceGeneralUtility.getAccountUser(userService);
		BigDecimal invoiceAmountDue = BigDecimal.ZERO;
		BigDecimal adjustment = BigDecimal.ZERO;
		EmployerInvoices newEmployerInvoices = new EmployerInvoices();
		for(EmployerInvoiceLineItems employerInvoiceLineItems : employerInvoiceLineItemsList)
		{
			invoiceAmountDue = FinancialMgmtGenericUtil.add(invoiceAmountDue, employerInvoiceLineItems.getNetAmount());
			adjustment = FinancialMgmtGenericUtil.add(adjustment, employerInvoiceLineItems.getRetroAdjustments());
			if(employerInvoiceLineItems.getEmployerEnrollmentId() == employerEnrollmentID)
			{
				employerInvoiceLineItems.setIsActiveEnrollment(EmployerInvoiceLineItems.IsActiveEnrollment.Y);
			}
			else
			{
				employerInvoiceLineItems.setIsActiveEnrollment(EmployerInvoiceLineItems.IsActiveEnrollment.N);
			}
			employerInvoiceLineItems.setLastUpdatedBy(user);
			employerInvoiceLineItems.setEmployerInvoices(newEmployerInvoices);
		}

		newEmployerInvoices.setEmployerInvoiceLineItems(employerInvoiceLineItemsList);
		newEmployerInvoices.setPremiumThisPeriod(invoiceAmountDue);
		newEmployerInvoices.setAdjustments(adjustment);
		newEmployerInvoices.setTotalAmountDue(invoiceAmountDue);
		newEmployerInvoices.setAmountDueFromLastInvoice(BigDecimal.ZERO);
		newEmployerInvoices.setAmountEnclosed(BigDecimal.ZERO);
		newEmployerInvoices.setExchangeFees(BigDecimal.ZERO);
		newEmployerInvoices.setAmountEnclosed(BigDecimal.ZERO);
		newEmployerInvoices.setTotalPaymentReceived(BigDecimal.ZERO);
		newEmployerInvoices.setStatementDate(new TSDate());
		//while creating invoice, invoice paid status will be 'due'
		newEmployerInvoices.setPaidStatus(PaidStatus.DUE);
		newEmployerInvoices.setIsActive(FinanceConstants.DECISION_Y);
		newEmployerInvoices.setIsAdjusted(IsManualAdjustment.N);

		/*
		 * GL_Code is hard coded value 1100 for Employer_Invoices for Exchange GL_coding
		 */
		newEmployerInvoices.setGlCode(FinanceConstants.EMPLOYER_INVOICE_GLCODE);
		newEmployerInvoices.setPeriodCovered(employerInvoiceLineItemsList.get(0).getPeriodCovered());
		newEmployerInvoices.setInvoiceType(invoiceType);
		newEmployerInvoices.setLastUpdatedBy(user.getId());

		/*
		 * This condition check is for setting payment due date based on invoice type.
		 */
		if(InvoiceType.BINDER_INVOICE == invoiceType)
		{
			Date duedate = new TSDate();
			DateTime currentdate = TSDateTime.getInstance();
			if(currentdate.getDayOfMonth()<=FinanceConstants.NUMERICAL_14)
			{
				Calendar calendar = TSCalendar.getInstance();
				calendar.set(Calendar.DATE,FinanceConstants.NUMERICAL_15);
				duedate = calendar.getTime();
			}
			else
			{
				Calendar calendar = TSCalendar.getInstance();
				calendar.set(Calendar.MONTH,calendar.get(Calendar.MONTH)+FinanceConstants.NUMERICAL_1);
				calendar.set(Calendar.DATE,FinanceConstants.NUMERICAL_15);					
				duedate = calendar.getTime();
			}

			newEmployerInvoices.setPaymentDueDate(duedate);
		}
		else
		{
			DateTimeFormatter fmt = DateTimeFormat.forPattern(FinanceConstants.DATE_FORMAT_MM_DD_YYYY);
			DateFormat df = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MM_DD_YYYY);
			DateTime dt2 = TSDateTime.getInstance();

			if(dt2.getDayOfMonth()==1)
			{
				dt2 = dt2.withDayOfMonth(FinanceConstants.NUMERICAL_1);
			}
			else
			{
				dt2 = dt2.plusMonths(1).withDayOfMonth(FinanceConstants.NUMERICAL_1);
			}

			try
			{
				Date today = df.parse(fmt.print(dt2));
				newEmployerInvoices.setPaymentDueDate(today);
			}
			catch (ParseException e)
			{
				LOGGER.info("getNormalEmployerInvoiceData(-) paymentDueDate parsing Exception");
				LOGGER.error("",e);
			}	
		}

		return newEmployerInvoices;
	}

	/**
	 * Checks whether the passed date is older than 3 months or not.
	 * @param lastInvoiceCreationTimeStamp
	 * @return boolean
	 */
	private boolean isOlderThanLastThreeMonths(Date lastInvoiceCreationTimeStamp)
	{
		DateTime lastInvoiceCreatedDateTime =  new DateTime(lastInvoiceCreationTimeStamp);
		LOGGER.info("Received lastInvoiceCreationTimeStamp as : "+lastInvoiceCreationTimeStamp);
		return lastInvoiceCreatedDateTime.isAfter(DateTime.now().minusMonths(FinanceConstants.NUMERICAL_3).dayOfMonth().withMinimumValue().withTimeAtStartOfDay());
	}


	/**
	 * @since 26th August 2014
	 * Boolean returns TRUE if BINDER_INVOICE for EmployerEnrollment Exists 
	 * @param employerID
	 * @param employerEnrollmentID
	 * @return
	 */
	private boolean isBinderInvoiceExistForEnrollment(int employerID, int employerEnrollmentID)
	{
		LOGGER.info("Verifying Employer Binder exists or not for EmployerID: "+employerID+" EmployerEnrollmentID: "+employerEnrollmentID);
		boolean isBinderGenerated = Boolean.FALSE;
		long countOfBinderInvoice = employerInvoicesService.noOfBinderInvoiceForEmployerEnrollment(employerID, employerEnrollmentID);
		if(countOfBinderInvoice > 0)
		{
			LOGGER.info("BinderInvoice Exists for EmployerEnrollmentId: "+employerEnrollmentID);
			isBinderGenerated = Boolean.TRUE;
		}
		return isBinderGenerated;
	}

	/**
	 * Method getNormalEmployerInvoiceData.
	 * @param enrollmentCurrentMnthList List<EnrollmentCurrentMonthDTO>
	 * @param employerid int
	 * @param lastInvoicedate Date
	 * @param reissue String
	 * @param lastEmployerInvoicesBeforeReissue EmployerInvoices
	 * @param reissueInvoice EmployerInvoices
	 * @return EmployerInvoices
	 */
	public Map<String, Object> getNormalEmployerInvoiceData(List<EnrollmentCurrentMonthDTO> enrollmentCurrentMnthList,Date lastInvoicedate,String reissue,Date reissueInvoiceStatementDate,int lastInvoiceId){
		boolean newHireEnrollmentCase = false;
		LOGGER.info("getNormalEmployerInvoiceData(-,-) method started lastInvoicedate: "+lastInvoicedate+" reissue: "+reissue);
		List<EmployerInvoiceLineItems> employerInvoiceLineItemsList = new ArrayList<EmployerInvoiceLineItems>();
		Map<String, Object> returnMap = new HashMap<String, Object>();
		String proRatioPaymentSupported = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.PRO_RATIO_SUPPORTED);
		LOGGER.info(" The value of proRatioPaymentSupported: "+proRatioPaymentSupported);

		int employerFeeCount = FinanceConstants.NUMERICAL_0;
		if(enrollmentCurrentMnthList != null && !enrollmentCurrentMnthList.isEmpty()){
			LOGGER.info(" The size of enrollments: "+enrollmentCurrentMnthList.size());
			Set<Integer> enrollmentIDs = new HashSet<>();
			for (EnrollmentCurrentMonthDTO enrollmentCurrentMonthDTO : enrollmentCurrentMnthList){

				// This condition is to prevent processing of duplicate enrollment records if any.
				if(!enrollmentIDs.add(enrollmentCurrentMonthDTO.getEnrollmentId()))
				{
					continue;
				}
				DateTime currentdate = TSDateTime.getInstance();

				DateTime reissuedate = null;
				if(reissueInvoiceStatementDate!=null){
					reissuedate = new DateTime(reissueInvoiceStatementDate);
				}

				DateTime terminationdate = new DateTime(enrollmentCurrentMonthDTO.getTerminationDate());
				DateTime lastinvoicedate = new DateTime(lastInvoicedate);
				DateTime actualenrollmentdate = new DateTime(enrollmentCurrentMonthDTO.getEnrollmentCreatedDate());
				List<EnrolleeCurrentMonthDTO> enrolleeCurrentMonthDTOList = enrollmentCurrentMonthDTO.getEnrolleeCurrentMonthDTOList();
				List<EnrollmentSubscriberEventDTO> enrollmentSubscriberEventDtoList = enrollmentCurrentMonthDTO.getEnrollmentSubscriberEventDtoList();
				DateTime benefitenddate = null;
				if(enrollmentCurrentMonthDTO.getBenefitEndDate()!=null){
					benefitenddate = new DateTime(enrollmentCurrentMonthDTO.getBenefitEndDate());
				}

				DateFormat dateTimeFormat = new SimpleDateFormat(FinanceConstants.REINSTIATION_TIME_FORMAT);
				boolean isReInstate = false; 
				if(actualenrollmentdate.isBefore(lastinvoicedate)){
					for (EnrollmentSubscriberEventDTO subscriberEventDto : enrollmentSubscriberEventDtoList) 
					{
						if((FinanceConstants.EVENT_REASON_REENROLLMENT).equalsIgnoreCase(subscriberEventDto.getEventReasonCode())
								&& (FinanceConstants.EVENT_TYPE_REINSTATEMENT).equalsIgnoreCase(subscriberEventDto.getEventTypeCode()) &&
								subscriberEventDto.getEventCreatedOn() != null)
						{
							/*if(subscriberEventDto.getEventCreatedOn()!=null){*/
								try{
									Date reinstiationDate = dateTimeFormat.parse(subscriberEventDto.getEventCreatedOn());
									DateTime reinitiateDateTime = new DateTime(reinstiationDate);
									if(reinitiateDateTime.isAfter(lastinvoicedate)){
										isReInstate = true;
										break;
									}
								}
								catch(ParseException e){
									LOGGER.error(" Error while parsing reinsitation date =");
								}
								/*}*/
						}
					}
				}

				boolean newSpecialEvent = false;
				//since enrollment creation happened after last invoice date and it is also cancelled, we need not consider this in billing
				if(FinanceConstants.CANCEL.equalsIgnoreCase(enrollmentCurrentMonthDTO.getEnrollmentStatusValue())){
					if( actualenrollmentdate.isAfter(lastinvoicedate) && terminationdate.isAfter(lastinvoicedate) && enrollmentCurrentMonthDTO.getBenefitEndDate()!=null){
						continue;
					}
					else if(actualenrollmentdate.isBefore(lastinvoicedate) && terminationdate.isBefore(lastinvoicedate) && enrollmentCurrentMonthDTO.getBenefitEndDate()!=null){
						continue;
					}
				}
				else if(enrollmentCurrentMonthDTO.getBenefitEndDate()!=null && actualenrollmentdate.isBefore(lastinvoicedate)){
					if(enrolleeCurrentMonthDTOList!=null && !enrolleeCurrentMonthDTOList.isEmpty()){
						for (EnrolleeCurrentMonthDTO enrolleeCurrentMonthDTO : enrolleeCurrentMonthDTOList) {
							DateTime specialeventdate = new DateTime(enrolleeCurrentMonthDTO.getEventDate());
							if(specialeventdate.isAfter(lastinvoicedate)){
								newSpecialEvent = true;
								break;
							}
						}
					}

					//add additional if below conditon if there is no special enrollment || the special event date is before last invoice date (new)
					if(!newSpecialEvent){

						if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
								FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
							if((terminationdate.isBefore(lastinvoicedate)) && benefitenddate.getMonthOfYear()==currentdate.getMonthOfYear() && !isReInstate){
								continue;
							}
							else if((terminationdate.isBefore(lastinvoicedate)) && isPreviousMonth(currentdate,benefitenddate)){
								continue;
							}
						}
						else{//in case of reissue in next month
							if((terminationdate.isBefore(lastinvoicedate)) && benefitenddate.getMonthOfYear()==reissuedate.getMonthOfYear() && !isReInstate){
								continue;
							}
							else if((terminationdate.isBefore(lastinvoicedate)) && isPreviousMonth(reissuedate,benefitenddate)){
								continue;
							}
						}
					}
				}

				//Rest call is required over here 
				EmployerInvoiceLineItems employerInvoiceLineItems = employerInvoiceLineItemsService.createEmployerInvoiceLineItemsFromEnrollmentForNormalInvoice(enrollmentCurrentMonthDTO);
				BigDecimal totalPremium = employerInvoiceLineItems.getTotalPremium();
				String isSpecialEnrollment = "NO";
				boolean isContributionSet = false;
				//if((enrolleeCurrentMonthDTOList!=null && !enrolleeCurrentMonthDTOList.isEmpty()) && enrolleeCurrentMonthDTOList.size()>0){
				if(enrolleeCurrentMonthDTOList!=null && !enrolleeCurrentMonthDTOList.isEmpty()){
					isSpecialEnrollment = FinanceConstants.YES;
				}
				int individualEmployerFeeCount = FinanceConstants.NUMERICAL_1;
				//Reinitiation call starts
				if(isReInstate){
					individualEmployerFeeCount = calculateRetroForReinitiation(lastInvoiceId,enrollmentCurrentMonthDTO.getBenefitEffectiveDate(),enrollmentCurrentMonthDTO.getBenefitEndDate(),
							enrollmentCurrentMonthDTO.getEnrolleeCurrentMonthDTOList(),reissue,reissueInvoiceStatementDate,enrollmentCurrentMonthDTO.getEnrollmentId(),isSpecialEnrollment,employerInvoiceLineItems,lastinvoicedate, individualEmployerFeeCount);
				}
				//Reinitiation call ends 

				/*if(FinanceConstants.YES.equalsIgnoreCase(proRatioPaymentSupported)){
					if(FinanceConstants.CANCEL.equalsIgnoreCase(enrollmentCurrentMonthDTO.getEnrollmentStatusValue())){
						individualEmployerFeeCount = calculateNewNormalPremiumForCancel(employerInvoiceLineItems,enrollmentCurrentMonthDTO.getBenefitEffectiveDate(),enrollmentCurrentMonthDTO.getBenefitEndDate(),lastInvoicedate,reissue,reissueInvoiceStatementDate,enrollmentCurrentMonthDTO.getEnrollmentCreatedDate(),enrollmentCurrentMonthDTO.getTerminationDate(),isSpecialEnrollment,lastInvoiceId,isContributionSet);	
					}
					else{
						individualEmployerFeeCount = calculateNewProRataPremium(employerInvoiceLineItems,enrollmentCurrentMonthDTO.getBenefitEffectiveDate(),enrollmentCurrentMonthDTO.getBenefitEndDate(),lastInvoicedate,reissue,reissueInvoiceStatementDate,enrollmentCurrentMonthDTO.getEnrollmentCreatedDate(),enrollmentCurrentMonthDTO.getTerminationDate(),isSpecialEnrollment,enrolleeCurrentMonthDTOList,enrollmentCurrentMonthDTO.getEnrollmentStatusValue(),lastInvoiceId);	
					}
				}
				else{*/
					if(FinanceConstants.CANCEL.equalsIgnoreCase(enrollmentCurrentMonthDTO.getEnrollmentStatusValue())){
						individualEmployerFeeCount = calculateNewNormalPremiumForCancel(employerInvoiceLineItems,enrollmentCurrentMonthDTO.getBenefitEffectiveDate(),enrollmentCurrentMonthDTO.getBenefitEndDate(),lastInvoicedate,reissue,reissueInvoiceStatementDate,enrollmentCurrentMonthDTO.getEnrollmentCreatedDate(),enrollmentCurrentMonthDTO.getTerminationDate(),isSpecialEnrollment,lastInvoiceId,isContributionSet);
					} 
					else{
						individualEmployerFeeCount = calculateNewNormalPremium(employerInvoiceLineItems,enrollmentCurrentMonthDTO.getBenefitEffectiveDate(),enrollmentCurrentMonthDTO.getBenefitEndDate(),
								lastInvoicedate,reissue,reissueInvoiceStatementDate,enrollmentCurrentMonthDTO.getEnrollmentCreatedDate(),
								enrollmentCurrentMonthDTO.getTerminationDate(),isSpecialEnrollment,enrolleeCurrentMonthDTOList,
								enrollmentCurrentMonthDTO.getEnrollmentStatusValue(),lastInvoiceId);	
					}
				//}

				boolean neweventplusrefundtermination = false;

				//this will be except special case for termination (new)
				if(!isContributionSet && employerInvoiceLineItems.getRetroAdjustments().compareTo(BigDecimal.ZERO) == -FinanceConstants.NUMERICAL_1 && "NO".equalsIgnoreCase(isSpecialEnrollment))
				{
					employerInvoiceLineItems.setEmployerContribution(new BigDecimal(enrollmentCurrentMonthDTO.getEmployerContribution()).negate());
					employerInvoiceLineItems.setEmployeeContribution(new BigDecimal(enrollmentCurrentMonthDTO.getEmployeeContribution()).negate());
				}
				else if(!isContributionSet && employerInvoiceLineItems.getRetroAdjustments().compareTo(BigDecimal.ZERO) == -FinanceConstants.NUMERICAL_1 && newSpecialEvent){
					neweventplusrefundtermination = true;
				}
				else if(employerInvoiceLineItems.getRetroAdjustments().compareTo(BigDecimal.ZERO) == FinanceConstants.NUMERICAL_1)
				{
					newHireEnrollmentCase = true;
				}

				boolean specialtermiscurrentorbefore = false;
				boolean specialtermisfuture = false;
				boolean specialtermisbefore = false;
				if(newSpecialEvent){
					if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
							FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
						if(currentdate.isAfter(benefitenddate) || isCurrentMonth(currentdate, benefitenddate)){
							specialtermiscurrentorbefore = true;
							if(currentdate.isAfter(benefitenddate))
							{
								specialtermisbefore = true;
							}
						}
						else if(isGreaterThanNextMonth(currentdate, benefitenddate)){
							specialtermisfuture = true;
						}
					}
					else{
						if(reissuedate.isAfter(benefitenddate) || isCurrentMonth(reissuedate, benefitenddate)){
							specialtermiscurrentorbefore = true;
							if(reissuedate.isAfter(benefitenddate))
							{
								specialtermisbefore = true;
							}
						}
						else if(isGreaterThanNextMonth(reissuedate, benefitenddate)){
							specialtermisfuture = true;
						}
					}
				}

				BigDecimal monthlyPremiumAmt = FinancialMgmtGenericUtil.getDefaultBigDecimal();

				BigDecimal specialCaseTotalPremium = FinancialMgmtGenericUtil.getDefaultBigDecimal();
				BigDecimal specialCaseMonthlyEmployerContri = FinancialMgmtGenericUtil.getDefaultBigDecimal();
				boolean doSpecialCalculation = true;
				//if term and there is special enrollment and its date is after last invoice date (new)
				if(FinanceConstants.CANCEL.equalsIgnoreCase(enrollmentCurrentMonthDTO.getEnrollmentStatusValue()) 
						|| (ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrollmentCurrentMonthDTO.getEnrollmentStatusValue()) && !newSpecialEvent)){
					doSpecialCalculation = false;
				}

				if((employerInvoiceLineItems.getTotalPremium().compareTo(BigDecimal.ZERO)==FinanceConstants.NUMERICAL_0) 
						&& (employerInvoiceLineItems.getRetroAdjustments().compareTo(BigDecimal.ZERO)==FinanceConstants.NUMERICAL_0))
				{
					individualEmployerFeeCount = FinanceConstants.NUMERICAL_0;
				}

				//Special Enrollment Logic Starts//
				boolean specialCaseTermination = false;
				if(doSpecialCalculation && enrolleeCurrentMonthDTOList!=null && !enrolleeCurrentMonthDTOList.isEmpty()){
					LOGGER.info(" Inside Special Enrollment start:=");
					BigDecimal premiumAmt = FinancialMgmtGenericUtil.getDefaultBigDecimal();
                    int noOfSpecialCaseEnrollee = 0;
                    boolean notCalculated = false;
					for (EnrolleeCurrentMonthDTO enrolleeCurrentMonthDTO : enrolleeCurrentMonthDTOList) 
					{
						//Employer, employee contribution calculation begins
						BigDecimal indSpecialEnrolleePremiumAmt = FinancialMgmtGenericUtil.getDefaultBigDecimal();
						indSpecialEnrolleePremiumAmt = newSpecialEnrollmentPremium(enrolleeCurrentMonthDTO,lastInvoicedate,reissue,reissueInvoiceStatementDate,benefitenddate,lastInvoiceId,enrollmentCurrentMonthDTO.getEnrollmentId(),enrollmentCurrentMonthDTO.getEnrollmentStatusValue(),benefitenddate,notCalculated);
						if(notCalculated){
							noOfSpecialCaseEnrollee++;
						}
						premiumAmt=FinancialMgmtGenericUtil.add(premiumAmt, indSpecialEnrolleePremiumAmt);
						BigDecimal employerResponsibilityAmt = FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEmployerResponsibilityAmt());
						if(indSpecialEnrolleePremiumAmt.compareTo(BigDecimal.ZERO)==FinanceConstants.NUMERICAL_1)
						{
							monthlyPremiumAmt = FinancialMgmtGenericUtil.add(monthlyPremiumAmt, new BigDecimal(enrolleeCurrentMonthDTO.getEnrollmentAmount()));
							BigDecimal specialCaseRetroPremAmt = null;
							if(newSpecialEvent && specialtermiscurrentorbefore){
								specialCaseRetroPremAmt = indSpecialEnrolleePremiumAmt;
							}
							else{
								specialCaseRetroPremAmt = FinancialMgmtGenericUtil.add(new BigDecimal(enrolleeCurrentMonthDTO.getEnrollmentAmount()), indSpecialEnrolleePremiumAmt);
							}
							specialCaseTotalPremium = FinancialMgmtGenericUtil.add(specialCaseTotalPremium, specialCaseRetroPremAmt);
							specialCaseMonthlyEmployerContri = FinancialMgmtGenericUtil.add(specialCaseMonthlyEmployerContri, employerResponsibilityAmt);
						}
						else if(indSpecialEnrolleePremiumAmt.compareTo(BigDecimal.ZERO)==FinanceConstants.NUMERICAL_NEG_1){
							monthlyPremiumAmt = FinancialMgmtGenericUtil.add(monthlyPremiumAmt, new BigDecimal(enrolleeCurrentMonthDTO.getEnrollmentAmount()));
							specialCaseTotalPremium = FinancialMgmtGenericUtil.add(specialCaseTotalPremium,indSpecialEnrolleePremiumAmt);
							specialCaseMonthlyEmployerContri = FinancialMgmtGenericUtil.add(specialCaseMonthlyEmployerContri, employerResponsibilityAmt);
						}
						notCalculated = false;
						/*else{
   							specialCaseMonthlyEmployerContri = FinancialMgmtGenericUtil.add(specialCaseMonthlyEmployerContri, employerResponsibilityAmt);
   						}*/
					}
					if(noOfSpecialCaseEnrollee==enrolleeCurrentMonthDTOList.size()){
						specialCaseTermination = true;
					}
					employerInvoiceLineItems.setRetroAdjustments(FinancialMgmtGenericUtil.add(premiumAmt, employerInvoiceLineItems.getRetroAdjustments()));
					LOGGER.info(" Inside Special Enrollment end : Special premium adjustment amt: "+premiumAmt);
				}
				employerInvoiceLineItems.setNetAmount(FinancialMgmtGenericUtil.add(employerInvoiceLineItems.getTotalPremium(), employerInvoiceLineItems.getRetroAdjustments()));

				//if both total premium and retro adjustment is zero then removing the line item from list
				if((employerInvoiceLineItems.getTotalPremium().compareTo(BigDecimal.ZERO)==FinanceConstants.NUMERICAL_0) && 
						(employerInvoiceLineItems.getRetroAdjustments().compareTo(BigDecimal.ZERO)==FinanceConstants.NUMERICAL_0)){
					continue;
				}
				
				//below is case of termination of entire enrollment due to birth special event
				if(specialCaseTermination){
					BigDecimal employerContributionforspecialEnrollee = FinancialMgmtGenericUtil.getDefaultBigDecimal();
					if(monthlyPremiumAmt.compareTo(BigDecimal.ZERO)==FinanceConstants.NUMERICAL_1){
						employerContributionforspecialEnrollee = FinancialMgmtGenericUtil.divide(FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getEmployerContribution(), employerInvoiceLineItems.getRetroAdjustments()),totalPremium);
					}
					employerInvoiceLineItems.setEmployerContribution(employerContributionforspecialEnrollee);
					employerInvoiceLineItems.setEmployeeContribution(FinancialMgmtGenericUtil.subtract(employerInvoiceLineItems.getNetAmount(), employerInvoiceLineItems.getEmployerContribution()));
				}
                else if(newHireEnrollmentCase || neweventplusrefundtermination)
				{
					BigDecimal employerContributionforspecialEnrollee = FinancialMgmtGenericUtil.getDefaultBigDecimal();
					if(monthlyPremiumAmt.compareTo(BigDecimal.ZERO)==FinanceConstants.NUMERICAL_1){
						employerContributionforspecialEnrollee = FinancialMgmtGenericUtil.divide(FinancialMgmtGenericUtil.multiply(specialCaseMonthlyEmployerContri, specialCaseTotalPremium),monthlyPremiumAmt);
					}
					BigDecimal monthlyBasedEmployerContriWOSE = null;
					//below condition is for entire family refund due to termination and one special add event
					if(specialCaseTotalPremium.compareTo(BigDecimal.ZERO)==FinanceConstants.NUMERICAL_1){
						monthlyPremiumAmt = FinancialMgmtGenericUtil.subtract(new BigDecimal(enrollmentCurrentMonthDTO.getGrossPremiumAmt()), monthlyPremiumAmt);
						monthlyBasedEmployerContriWOSE =  FinancialMgmtGenericUtil.subtract(new BigDecimal(enrollmentCurrentMonthDTO.getEmployerContribution()), specialCaseMonthlyEmployerContri);
					}//below condition is for entire family refund due to termination and one special delete event
					else{
						monthlyPremiumAmt = new BigDecimal(enrollmentCurrentMonthDTO.getGrossPremiumAmt());
						monthlyBasedEmployerContriWOSE =  new BigDecimal(enrollmentCurrentMonthDTO.getEmployerContribution());
					}
					BigDecimal totalPremiumWOSE = FinancialMgmtGenericUtil.subtract(employerInvoiceLineItems.getNetAmount(), specialCaseTotalPremium);

					BigDecimal finalValueWOSE = FinancialMgmtGenericUtil.divide(FinancialMgmtGenericUtil.multiply( monthlyBasedEmployerContriWOSE, totalPremiumWOSE),monthlyPremiumAmt);//nonSpecialCaseValue = monthlyPremiumAmt;

					employerInvoiceLineItems.setEmployerContribution(FinancialMgmtGenericUtil.add(employerContributionforspecialEnrollee, finalValueWOSE));
					employerInvoiceLineItems.setEmployeeContribution(FinancialMgmtGenericUtil.subtract(employerInvoiceLineItems.getNetAmount(), employerInvoiceLineItems.getEmployerContribution()));
				}
				else if((employerInvoiceLineItems.getRetroAdjustments().compareTo(BigDecimal.ZERO) == FinanceConstants.NUMERICAL_1) && 
						!newHireEnrollmentCase)
				{
					BigDecimal employerContribution = FinancialMgmtGenericUtil.divide(FinancialMgmtGenericUtil.multiply(specialCaseMonthlyEmployerContri, specialCaseTotalPremium),monthlyPremiumAmt);
					BigDecimal monthlyBasedEmployerContriWOSE = null;
					if(newSpecialEvent && specialtermiscurrentorbefore){
						monthlyBasedEmployerContriWOSE =  FinancialMgmtGenericUtil.getDefaultBigDecimal();
					}
					else{
						monthlyBasedEmployerContriWOSE = FinancialMgmtGenericUtil.subtract(new BigDecimal(enrollmentCurrentMonthDTO.getEmployerContribution()), specialCaseMonthlyEmployerContri);
					}
					employerInvoiceLineItems.setEmployerContribution(FinancialMgmtGenericUtil.add(monthlyBasedEmployerContriWOSE, employerContribution));
					employerInvoiceLineItems.setEmployeeContribution(FinancialMgmtGenericUtil.subtract(employerInvoiceLineItems.getNetAmount(), employerInvoiceLineItems.getEmployerContribution()));
				}
				//if it is term and future date then also below loop has to be executed
				else if((employerInvoiceLineItems.getRetroAdjustments().compareTo(BigDecimal.ZERO) == FinanceConstants.NUMERICAL_NEG_1) && !newHireEnrollmentCase && !neweventplusrefundtermination && !enrollmentCurrentMonthDTO.getEnrollmentStatusValue().equals(FinanceConstants.CANCEL) 
						&& (!enrollmentCurrentMonthDTO.getEnrollmentStatusValue().equals(ENROLLMENT_STATUS_TERM) || (enrollmentCurrentMonthDTO.getEnrollmentStatusValue().equals(ENROLLMENT_STATUS_TERM) && specialtermisfuture)))
				{
					BigDecimal employerContribution = FinancialMgmtGenericUtil.divide(FinancialMgmtGenericUtil.multiply(specialCaseMonthlyEmployerContri, specialCaseTotalPremium),monthlyPremiumAmt);
					BigDecimal monthlyBasedEmployerContriWOSE =  new BigDecimal(enrollmentCurrentMonthDTO.getEmployerContribution());
					if(employerContribution != null && monthlyBasedEmployerContriWOSE != null)
					{
						employerInvoiceLineItems.setEmployerContribution(FinancialMgmtGenericUtil.add(monthlyBasedEmployerContriWOSE, employerContribution));
					}
					employerInvoiceLineItems.setEmployeeContribution(FinancialMgmtGenericUtil.subtract(employerInvoiceLineItems.getNetAmount(), employerInvoiceLineItems.getEmployerContribution()));
				}
				else if((employerInvoiceLineItems.getRetroAdjustments().compareTo(BigDecimal.ZERO) == FinanceConstants.NUMERICAL_NEG_1) && !newHireEnrollmentCase && !neweventplusrefundtermination && !enrollmentCurrentMonthDTO.getEnrollmentStatusValue().equals(FinanceConstants.CANCEL) 
						&& (!enrollmentCurrentMonthDTO.getEnrollmentStatusValue().equals(ENROLLMENT_STATUS_TERM) || (enrollmentCurrentMonthDTO.getEnrollmentStatusValue().equals(ENROLLMENT_STATUS_TERM) && specialtermiscurrentorbefore)))
				{
					BigDecimal employerContribution = FinancialMgmtGenericUtil.divide(FinancialMgmtGenericUtil.multiply(specialCaseMonthlyEmployerContri, specialCaseTotalPremium),monthlyPremiumAmt);
					BigDecimal monthlyBasedEmployerContriWOSE =  null;
					if(specialtermisbefore){
						monthlyBasedEmployerContriWOSE = FinancialMgmtGenericUtil.getBigDecimalObj(enrollmentCurrentMonthDTO.getEmployerContribution());
						monthlyBasedEmployerContriWOSE = monthlyBasedEmployerContriWOSE.negate();
					}
					else{
						monthlyBasedEmployerContriWOSE = FinancialMgmtGenericUtil.getDefaultBigDecimal();
					}
					if(employerContribution != null)
					{
						employerInvoiceLineItems.setEmployerContribution(FinancialMgmtGenericUtil.add(monthlyBasedEmployerContriWOSE, employerContribution));
					}
					employerInvoiceLineItems.setEmployeeContribution(FinancialMgmtGenericUtil.subtract(employerInvoiceLineItems.getNetAmount(), employerInvoiceLineItems.getEmployerContribution()));
				}

				employerFeeCount = employerFeeCount+ individualEmployerFeeCount;
				employerInvoiceLineItemsList.add(employerInvoiceLineItems);
			}
			returnMap.put(FinanceConstants.LINE_ITEM_LIST, employerInvoiceLineItemsList);
			returnMap.put(FinanceConstants.EMPLOYER_FEE_COUNT, employerFeeCount);
		}
		LOGGER.info(" getNormalEmployerInvoiceData(-) ends successfully =");
		return returnMap;
	}

	private int calculateNewNormalPremium(EmployerInvoiceLineItems employerInvoiceLineItems,Date effectiveDate, Date terminationDate,Date lastInvoicedate,String reissue,Date reissueInvoiceStatementDate,Date enrollmentdate,Date terminatDate,String isSpecialEnrollment,List<EnrolleeCurrentMonthDTO> enrolleeCurrentMonthDTOList,String enrollmentStatus,int lastInvoiceId){
		LOGGER.info(" calculateNormalPremium(-,-,-,-) method started with effectiveDate:"+effectiveDate+"+terminationDate:"+terminationDate+"lastInvoicedate:"+lastInvoicedate+"reissue:"+reissue+"isSpecialEnrollment:"+isSpecialEnrollment);
		int employerFee = FinanceConstants.NUMERICAL_1;
		DateTime effectivedate = new DateTime(effectiveDate);
		DateTime currentdate = TSDateTime.getInstance();
		currentdate = setJodaTime(currentdate);
		DateTime benefitenddate = null;
		if(terminationDate!=null){
			benefitenddate = new DateTime(terminationDate);
		}
		DateTime lastbillingdate = new DateTime(lastInvoicedate);
		DateTime reissuedate = null;
		if(reissueInvoiceStatementDate!=null){
			reissuedate = new DateTime(reissueInvoiceStatementDate);
		}
		DateTime actualenrollmentdate = new DateTime(enrollmentdate);
		DateTime terminateDate = new DateTime(terminatDate);

		DateTime terminateDateInDateTimeFormat = TSDateTime.getInstance();
		BigDecimal prorateamount = FinancialMgmtGenericUtil.getDefaultBigDecimal();

		double amount = 0.0d;
		//this calculation if for new hire and special enrollment
		if(actualenrollmentdate.isAfter(lastbillingdate) && FinanceConstants.YES.equalsIgnoreCase(isSpecialEnrollment)){
			//if(enrolleeCurrentMonthDTOList!=null && !enrolleeCurrentMonthDTOList.isEmpty() && enrolleeCurrentMonthDTOList.size()>0){
			if(enrolleeCurrentMonthDTOList!=null && !enrolleeCurrentMonthDTOList.isEmpty()){
				for (EnrolleeCurrentMonthDTO enrolleeCurrentMonthDTO : enrolleeCurrentMonthDTOList) {
					if(FinanceConstants.CONFIRM.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus()) || FinanceConstants.PAYMENT_RECEIVED.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus())
							|| FinanceConstants.PENDING.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus()) || FinanceConstants.ORDER_CONFIRMED.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus()))
					{
						amount=amount+enrolleeCurrentMonthDTO.getEnrollmentAmount();
					}
				}
			}
			employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.subtract(employerInvoiceLineItems.getTotalPremium(),BigDecimal.valueOf(amount)));
		}
		//the above calculation if for new hire and special enrollment

		//have to return only original ppl amt, if special enrollment date and enrollment termiantion is after last invoice date 
		//and enrollment is before last invoice (new)
		else if(enrollmentStatus.equals(ENROLLMENT_STATUS_TERM) && actualenrollmentdate.isBefore(lastbillingdate) 
				&& FinanceConstants.YES.equalsIgnoreCase(isSpecialEnrollment) && terminateDate.isAfter(lastbillingdate)){
			//if(enrolleeCurrentMonthDTOList!=null && !enrolleeCurrentMonthDTOList.isEmpty() && enrolleeCurrentMonthDTOList.size()>0){
			if(enrolleeCurrentMonthDTOList!=null && !enrolleeCurrentMonthDTOList.isEmpty()){    			
				for (EnrolleeCurrentMonthDTO enrolleeCurrentMonthDTO : enrolleeCurrentMonthDTOList) {
					DateTime actualenrolleedate = new DateTime(enrolleeCurrentMonthDTO.getCreatedOn());
					if(actualenrolleedate.isAfter(lastbillingdate)){
						amount=amount+enrolleeCurrentMonthDTO.getEnrollmentAmount();
					}
				}
			}
			employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.subtract(employerInvoiceLineItems.getTotalPremium(), BigDecimal.valueOf(amount)));
		} 
				
              if(actualenrollmentdate.isAfter(lastbillingdate)){
				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
					if(terminationDate==null || isGreaterThanNextMonth(currentdate,benefitenddate)){
						DateTime calculate = TSDateTime.getInstance();
						calculate = calculate.plusMonths(FinanceConstants.NUMERICAL_1);
						calculate = calculate.dayOfMonth().withMaximumValue();
						terminateDateInDateTimeFormat = calculate;	        		
					}
					else{ 
						terminateDateInDateTimeFormat = new DateTime(benefitenddate);
					}
				}
				else{
					if(terminationDate==null || isGreaterThanNextMonth(reissuedate,benefitenddate)){
						DateTime calculate = TSDateTime.getInstance();
						calculate = calculate.dayOfMonth().withMaximumValue();
						terminateDateInDateTimeFormat = calculate;
					}
					else{
						terminateDateInDateTimeFormat = new DateTime(benefitenddate);
					}
				}
				int noOfPremium = differenceInMonths(effectivedate,terminateDateInDateTimeFormat);
				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
					if(currentdate.isAfter(terminateDateInDateTimeFormat) || isCurrentMonth(currentdate, terminateDateInDateTimeFormat)){
						
						   noOfPremium= noOfPremium + 1;
					}
				}
				else{
					if(reissuedate.isAfter(terminateDateInDateTimeFormat) || isCurrentMonth(reissuedate, terminateDateInDateTimeFormat)){
                       
						    noOfPremium= noOfPremium + 1;
						
					}
				}
				employerFee = noOfPremium;
				
				if(!isEndOfMonth(terminateDateInDateTimeFormat)){
					LOGGER.info(" Inside New hire Premium Calculation TERM Pro ratio : noOfPremium "+noOfPremium);
					for(int i=0;i<(noOfPremium-1);i++){
						prorateamount = FinancialMgmtGenericUtil.add(prorateamount, employerInvoiceLineItems.getTotalPremium());
					}
					
					LOGGER.info(" Inside New hire Premium Calculation TERM Pro ratio: premiumAdjustment "+prorateamount);
					if(effectivedate.getMonthOfYear()!=terminateDateInDateTimeFormat.getMonthOfYear() && effectivedate.getDayOfMonth()!=1){
						//this is for special case for e.g. when we bill on 5th March and the coverage start date is 4th Feb and end date is 4th March
						if(isNextMonth(effectivedate, terminateDateInDateTimeFormat)){
							prorateamount = FinancialMgmtGenericUtil.getDefaultBigDecimal();
						}
						int totalnoofdays = effectivedate.dayOfMonth().getMaximumValue();
						int actualnoofdays =  totalnoofdays - effectivedate.dayOfMonth().get() +1;
						LOGGER.info(" Inside New hire Premium Calculation TERM Pro ratio : totalnoofdays "+totalnoofdays+"---actualnoofdays--"+actualnoofdays);
						prorateamount = FinancialMgmtGenericUtil.add(prorateamount,FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getTotalPremium(),BigDecimal.valueOf((double)actualnoofdays / (double)totalnoofdays) ));
						
						totalnoofdays = terminateDateInDateTimeFormat.dayOfMonth().getMaximumValue();
						actualnoofdays =  terminateDateInDateTimeFormat.dayOfMonth().get();
						LOGGER.info(" Inside New hire Premium Calculation TERM Pro ratio : totalnoofdays "+totalnoofdays+"---actualnoofdays--"+actualnoofdays);
						prorateamount = FinancialMgmtGenericUtil.add(prorateamount,FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getTotalPremium(),BigDecimal.valueOf((double)actualnoofdays / (double)totalnoofdays) ));
					}
					else if(effectivedate.getMonthOfYear()!=terminateDateInDateTimeFormat.getMonthOfYear() && effectivedate.getDayOfMonth()==1){
						
						int totalnoofdays = terminateDateInDateTimeFormat.dayOfMonth().getMaximumValue();
						int actualnoofdays =  terminateDateInDateTimeFormat.dayOfMonth().get();
						LOGGER.info(" Inside New hire Premium Calculation TERM Pro ratio : totalnoofdays "+totalnoofdays+"---actualnoofdays--"+actualnoofdays);
						prorateamount = FinancialMgmtGenericUtil.add(prorateamount,FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getTotalPremium(),BigDecimal.valueOf((double)actualnoofdays / (double)totalnoofdays) ));
											
					}
					else{
						
						int totalnoofdays = terminateDateInDateTimeFormat.dayOfMonth().getMaximumValue();
						int actualnoofdays = (int)(terminateDateInDateTimeFormat.dayOfMonth().get() - effectivedate.dayOfMonth().get())+1;
						LOGGER.info(" Inside New hire Premium Calculation TERM Pro ratio : totalnoofdays "+totalnoofdays+"---actualnoofdays--"+actualnoofdays);
						prorateamount = FinancialMgmtGenericUtil.add(prorateamount,FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getTotalPremium(),BigDecimal.valueOf((double)actualnoofdays / (double)totalnoofdays) ));
						
					}
					LOGGER.info(" Inside New hire Premium Calculation TERM Pro ratio : final premiumAdjustment "+prorateamount);
				}
				else if(effectivedate.getDayOfMonth()!=1){
					LOGGER.info(" Inside New hire Premium Calculation TERM Pro ratio : noOfPremium "+noOfPremium);
					for(int i=0;i<(noOfPremium-1);i++){
						prorateamount = FinancialMgmtGenericUtil.add(prorateamount, employerInvoiceLineItems.getTotalPremium());
					}
					LOGGER.info(" Inside New hire Premium Calculation TERM Pro ratio: premiumAdjustment "+prorateamount);
					int totalnoofdays = effectivedate.dayOfMonth().getMaximumValue();
					int actualnoofdays =  totalnoofdays - effectivedate.dayOfMonth().get() +1;
					LOGGER.info(" Inside New hire Premium Calculation TERM Pro ratio : totalnoofdays "+totalnoofdays+"---actualnoofdays--"+actualnoofdays);

					//premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment,FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()),new BigDecimal((float)actualnoofdays/(float)totalnoofdays)));
					prorateamount = FinancialMgmtGenericUtil.add(prorateamount,FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getTotalPremium(),BigDecimal.valueOf((double)actualnoofdays / (double)totalnoofdays) ));

					LOGGER.info(" Inside New hire Premium Calculation TERM Pro ratio : final premiumAdjustment "+prorateamount);
				}
				else{
					LOGGER.info(" Inside New hire Premium Calculation TERM : noOfPremium "+noOfPremium);
					for(int i=0;i<noOfPremium;i++){
						prorateamount = FinancialMgmtGenericUtil.add(prorateamount, employerInvoiceLineItems.getTotalPremium());
					}
					LOGGER.info(" Inside New hire Premium Calculation TERM : final premiumAdjustment "+prorateamount);
				}
				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
					if(currentdate.isAfter(terminateDateInDateTimeFormat) || isCurrentMonth(currentdate, terminateDateInDateTimeFormat)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
					else 
					{
						employerFee = employerFee + FinanceConstants.NUMERICAL_1;
					}
					
				}
				else{
					if(reissuedate.isAfter(terminateDateInDateTimeFormat) || isCurrentMonth(reissuedate, terminateDateInDateTimeFormat)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
					else
					{
						employerFee = employerFee + FinanceConstants.NUMERICAL_1;
					}
					
				}
				employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.add(employerInvoiceLineItems.getTotalPremium(), BigDecimal.valueOf(amount)));
				employerInvoiceLineItems.setRetroAdjustments(prorateamount);
			}

			else if(terminateDate.isAfter(lastbillingdate) && terminationDate!=null){
				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
					if(isEnrollmentBilledInLastInvoice(lastInvoiceId,employerInvoiceLineItems.getEnrollmentId(),currentdate)){
						employerFee = setRefundAndPremiumForTermination(employerInvoiceLineItems,currentdate,effectivedate,benefitenddate,employerFee);
					}
					//is current month end date then set total premium zero(new)
					else if(isCurrentMonth(currentdate, benefitenddate) || isPreviousMonth(currentdate,benefitenddate) || isOlderThanPreviousMonth(currentdate,benefitenddate)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
				}
				else{
					if(isEnrollmentBilledInLastInvoice(lastInvoiceId,employerInvoiceLineItems.getEnrollmentId(),reissuedate)){
						employerFee = setRefundAndPremiumForTermination(employerInvoiceLineItems,reissuedate,effectivedate,benefitenddate,employerFee);
					}
					//is current month end date then set total premium zero(new)
					else if(isCurrentMonth(reissuedate, benefitenddate) || isPreviousMonth(reissuedate,benefitenddate) || isOlderThanPreviousMonth(reissuedate,benefitenddate)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
				}
			}

			//termination is before last invoice date then set total prem is 0 (new)
			else if(terminateDate.isBefore(lastbillingdate) && terminationDate!=null){
				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
					if(currentdate.isAfter(benefitenddate) || isCurrentMonth(currentdate, benefitenddate)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
				}
				else{
					if(reissuedate.isAfter(benefitenddate) || isCurrentMonth(reissuedate, benefitenddate)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
				}
			}
			//this below condition is if there is no termination or cancellation of enrollment but end date is reached and special event happened
			else{
				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
					if(currentdate.isAfter(benefitenddate) || isCurrentMonth(currentdate, benefitenddate)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
				}
				else{
					if(reissuedate.isAfter(benefitenddate) || isCurrentMonth(reissuedate, benefitenddate)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
				}
			}
		
		LOGGER.info(" calculateNormalPremium(-) ends successfully ="+employerFee);
		return employerFee;
	}

	private int calculateNewProRataPremium(EmployerInvoiceLineItems employerInvoiceLineItems,Date effectiveDate, Date terminationDate,Date lastInvoicedate,String reissue,Date reissueInvoiceStatementDate,Date enrollmentdate,Date terminatDate,String isSpecialEnrollment,List<EnrolleeCurrentMonthDTO> enrolleeCurrentMonthDTOList,String enrollmentStatus,int lastInvoiceId ){
		LOGGER.info(" calculateNewProRataPremium(-,-,-,-) method started with effectiveDate:"+effectiveDate+"+terminationDate:"+terminationDate+"lastInvoicedate:"+lastInvoicedate+"reissue:"+reissue+"isSpecialEnrollment:"+isSpecialEnrollment);
		int employerFee = FinanceConstants.NUMERICAL_1;
		DateTime effectivedate = new DateTime(effectiveDate);
		DateTime currentdate = TSDateTime.getInstance();
		currentdate = setJodaTime(currentdate);
		DateTime benefitenddate = null;
		if(terminationDate!=null){
			benefitenddate = new DateTime(terminationDate);
		}
		DateTime lastbillingdate = new DateTime(lastInvoicedate);
		DateTime reissuedate = null;
		if(reissueInvoiceStatementDate!=null){
			reissuedate = new DateTime(reissueInvoiceStatementDate);
		}
		DateTime actualenrollmentdate = new DateTime(enrollmentdate);
		DateTime terminateDate = new DateTime(terminatDate);

		DateTime terminateDateInDateTimeFormat = TSDateTime.getInstance();
		BigDecimal prorateamount = FinancialMgmtGenericUtil.getDefaultBigDecimal();

		boolean doCalculation = true;
		double amount = 0.0d;
		//this calculation if for new hire and special enrollment
		if(actualenrollmentdate.isAfter(lastbillingdate) && FinanceConstants.YES.equalsIgnoreCase(isSpecialEnrollment)){
			//if(enrolleeCurrentMonthDTOList!=null && !enrolleeCurrentMonthDTOList.isEmpty() && enrolleeCurrentMonthDTOList.size()>0){
			if(enrolleeCurrentMonthDTOList!=null && !enrolleeCurrentMonthDTOList.isEmpty()){
				for (EnrolleeCurrentMonthDTO enrolleeCurrentMonthDTO : enrolleeCurrentMonthDTOList) {
					if(FinanceConstants.CONFIRM.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus()) || FinanceConstants.PAYMENT_RECEIVED.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus())
							|| FinanceConstants.PENDING.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus()) || FinanceConstants.ORDER_CONFIRMED.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus()))
					{
						amount=amount+enrolleeCurrentMonthDTO.getEnrollmentAmount();
					}
				}
			}
			employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.subtract(employerInvoiceLineItems.getTotalPremium(),BigDecimal.valueOf(amount)));
		}
		//the above calculation if for new hire and special enrollment

		//have to return only original ppl amt, if special enrollment date and enrollment termiantion is after last invoice date 
		//and enrollment is before last invoice (new)
		else if(enrollmentStatus.equals(ENROLLMENT_STATUS_TERM) && actualenrollmentdate.isBefore(lastbillingdate) 
				&& FinanceConstants.YES.equalsIgnoreCase(isSpecialEnrollment) && terminateDate.isAfter(lastbillingdate)){
			//if(enrolleeCurrentMonthDTOList!=null && !enrolleeCurrentMonthDTOList.isEmpty() && enrolleeCurrentMonthDTOList.size()>0){
			if(enrolleeCurrentMonthDTOList!=null && !enrolleeCurrentMonthDTOList.isEmpty()){
				for (EnrolleeCurrentMonthDTO enrolleeCurrentMonthDTO : enrolleeCurrentMonthDTOList) {
					DateTime actualenrolleedate = new DateTime(enrolleeCurrentMonthDTO.getCreatedOn());
					if(actualenrolleedate.isAfter(lastbillingdate)){
						amount=amount+enrolleeCurrentMonthDTO.getEnrollmentAmount();
					}
				}
			}
			//employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.subtract(employerInvoiceLineItems.getTotalPremium(),new BigDecimal(amount)));
			employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.subtract(employerInvoiceLineItems.getTotalPremium(),BigDecimal.valueOf(amount)));
		} 

		if("DENTAL".equalsIgnoreCase(employerInvoiceLineItems.getPlayType()) && FinanceConstants.YES.equalsIgnoreCase(isSpecialEnrollment)){
			doCalculation = false;
			//if(ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrollmentStatus)){
			if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
					FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
				if(terminateDate.isAfter(lastbillingdate) && benefitenddate!=null && isEnrollmentBilledInLastInvoice(lastInvoiceId,employerInvoiceLineItems.getEnrollmentId(),currentdate)){
					employerFee = setRefundAndPremiumForTermination(employerInvoiceLineItems,currentdate,effectivedate,benefitenddate,employerFee);
				}
				else if(benefitenddate!=null && (isCurrentMonth(currentdate,benefitenddate) || isPreviousMonth(currentdate,benefitenddate) || isOlderThanPreviousMonth(currentdate,benefitenddate))){
					employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
				}
			}
			else{
				if(terminateDate.isAfter(lastbillingdate) && benefitenddate!=null && isEnrollmentBilledInLastInvoice(lastInvoiceId,employerInvoiceLineItems.getEnrollmentId(),reissuedate)){
					employerFee = setRefundAndPremiumForTermination(employerInvoiceLineItems,reissuedate,effectivedate,benefitenddate,employerFee);
				}
				else if(benefitenddate!=null && (isCurrentMonth(reissuedate,benefitenddate) || isPreviousMonth(reissuedate,benefitenddate) || isOlderThanPreviousMonth(reissuedate,benefitenddate))){
					employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
				}
			}
			//}
		}
		if(doCalculation)
		{

			if(actualenrollmentdate.isAfter(lastbillingdate)){
				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
					if(terminationDate==null || isGreaterThanNextMonth(currentdate,benefitenddate)){
						DateTime calculate = TSDateTime.getInstance();
						calculate = calculate.plusMonths(FinanceConstants.NUMERICAL_1);
						calculate = calculate.dayOfMonth().withMaximumValue();
						terminateDateInDateTimeFormat = calculate;
					}
					else{ 
						terminateDateInDateTimeFormat = benefitenddate;
					}
				}
				else{
					if(terminationDate==null || (terminationDate!=null && isGreaterThanNextMonth(reissuedate,benefitenddate))){
						DateTime calculate = TSDateTime.getInstance();
						calculate = calculate.dayOfMonth().withMaximumValue();
						terminateDateInDateTimeFormat = calculate;
					}
					else{ 
						terminateDateInDateTimeFormat = benefitenddate;
					}
				}
				int noOfPremium = differenceInMonths(effectivedate,terminateDateInDateTimeFormat);
				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
					if(currentdate.isAfter(terminateDateInDateTimeFormat) || isCurrentMonth(currentdate, terminateDateInDateTimeFormat)){
						if(!isEndOfMonth(terminateDateInDateTimeFormat) && terminateDateInDateTimeFormat.getMonthOfYear() == effectivedate.getMonthOfYear()){
							int actualnoofdays = (int)( terminateDateInDateTimeFormat.dayOfMonth().get() - effectivedate.dayOfMonth().get()) +1;
							int totalnoofdays = terminateDateInDateTimeFormat.dayOfMonth().getMaximumValue();
							prorateamount = FinancialMgmtGenericUtil.add(prorateamount,FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getTotalPremium(), BigDecimal.valueOf((double)actualnoofdays/(double)totalnoofdays)));
						}
						else{
						    noOfPremium = noOfPremium+1;
						}
					}
				}
				else{
					if(reissuedate.isAfter(terminateDateInDateTimeFormat) || isCurrentMonth(reissuedate, terminateDateInDateTimeFormat)){
						if(!isEndOfMonth(terminateDateInDateTimeFormat) && terminateDateInDateTimeFormat.getMonthOfYear() == effectivedate.getMonthOfYear()){
							int actualnoofdays = (int)( terminateDateInDateTimeFormat.dayOfMonth().get() - effectivedate.dayOfMonth().get()) +1;
							int totalnoofdays = terminateDateInDateTimeFormat.dayOfMonth().getMaximumValue();
							prorateamount = FinancialMgmtGenericUtil.add(prorateamount,FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getTotalPremium(), BigDecimal.valueOf((double)actualnoofdays/(double)totalnoofdays)));
						}
						else{
						    noOfPremium = noOfPremium+1;
						}
					}
				}
				employerFee = noOfPremium;
				for(int i=0;i<noOfPremium;i++){
					prorateamount = FinancialMgmtGenericUtil.add(prorateamount, employerInvoiceLineItems.getTotalPremium());
				}

				//Since in case of new hire the effective start date will always be 1st of a month, commenting below code
				/*int totalnoofdays = effectivedate.dayOfMonth().getMaximumValue();
        	if(effectivedate.getDayOfMonth()!=1){
        		int actualnoofdays = 0;
        		if(benefitenddate!=null && noOfPremium==0)
        		{
	        	   actualnoofdays = (int)( benefitenddate.dayOfMonth().get() - effectivedate.dayOfMonth().get()) +1;
        		}
        		else{
        			if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
                			FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
        				if(isNextMonth(currentdate, effectivedate)){
        					nextmontheffectivedate = true;
        				}
        			}
        			else{
        				if(isNextMonth(reissuedate, effectivedate)){
        					nextmontheffectivedate = true;
        				}
        			}
        			actualnoofdays = (int)( totalnoofdays - effectivedate.dayOfMonth().get()) +1;
        		}
        		prorateamount = FinancialMgmtGenericUtil.add(prorateamount,FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getTotalPremium(), new BigDecimal((float)actualnoofdays/(float)totalnoofdays)));
        	}*/

				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
					if(currentdate.isAfter(terminateDateInDateTimeFormat) || isCurrentMonth(currentdate, terminateDateInDateTimeFormat)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
					else
					{
						employerFee = employerFee + FinanceConstants.NUMERICAL_1;
					}
				}
				else{
					if(reissuedate.isAfter(terminateDateInDateTimeFormat) || isCurrentMonth(reissuedate, terminateDateInDateTimeFormat)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
					else
					{
						employerFee = employerFee + FinanceConstants.NUMERICAL_1;
					}
				}
				//employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.add(employerInvoiceLineItems.getTotalPremium(),new BigDecimal(amount)));
				employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.add(employerInvoiceLineItems.getTotalPremium(), BigDecimal.valueOf(amount)));
				employerInvoiceLineItems.setRetroAdjustments(prorateamount);
			}

			else if(terminateDate.isAfter(lastbillingdate) && terminationDate!=null){
				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
					if(isEnrollmentBilledInLastInvoice(lastInvoiceId,employerInvoiceLineItems.getEnrollmentId(),currentdate)){
						employerFee = setRefundAndPremiumForTermination(employerInvoiceLineItems,currentdate,effectivedate,benefitenddate,employerFee);
					}
					//is current month end date then set total premium zero(new)
					else if(isCurrentMonth(currentdate, benefitenddate) || isPreviousMonth(currentdate,benefitenddate) || isOlderThanPreviousMonth(currentdate,benefitenddate)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
				}
				else{
					if(isEnrollmentBilledInLastInvoice(lastInvoiceId,employerInvoiceLineItems.getEnrollmentId(),reissuedate)){
						employerFee = setRefundAndPremiumForTermination(employerInvoiceLineItems,reissuedate,effectivedate,benefitenddate,employerFee);
					}
					//is current month end date then set total premium zero(new)
					else if(isCurrentMonth(reissuedate, benefitenddate) || isPreviousMonth(reissuedate,benefitenddate) || isOlderThanPreviousMonth(reissuedate,benefitenddate)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
				}
			}
			//termination is before last invoice date then set total prem is 0 (new)
			else if(terminateDate.isBefore(lastbillingdate) && terminationDate!=null){
				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
					if(currentdate.isAfter(benefitenddate) || isCurrentMonth(currentdate, benefitenddate)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
				}
				else{
					if(reissuedate.isAfter(benefitenddate) || isCurrentMonth(reissuedate, benefitenddate)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
				}
			}
			//this below condition is if there is no termination or cancellation of enrollment but end date is reached and special event happened
			else{
				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
					if(currentdate.isAfter(benefitenddate) || isCurrentMonth(currentdate, benefitenddate)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
				}
				else{
					if(reissuedate.isAfter(benefitenddate) || isCurrentMonth(reissuedate, benefitenddate)){
						employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
					}
				}
			}
		}
		LOGGER.info(" calculateNewProRataPremium(-) ends successfully ="+employerFee);
		return employerFee;
	}

	private void calculateNewProRataPremiumForBinder(EmployerInvoiceLineItems employerInvoiceLineItems,Date effectiveDate, Date terminationDate){
		LOGGER.info(" calculateNewProRataPremiumForBinder(-,-,-,-) method started with effectiveDate:"+effectiveDate+"+terminationDate:"+terminationDate);
		BigDecimal prorateamount = FinancialMgmtGenericUtil.getDefaultBigDecimal();
		DateTime effectivedate = new DateTime(effectiveDate);
		DateTime currentdate = TSDateTime.getInstance();
		currentdate = setJodaTime(currentdate);
		DateTime benefitenddate = null;
		if(terminationDate!=null){
			benefitenddate = new DateTime(terminationDate);
		}

		int totalnoofdays = effectivedate.dayOfMonth().getMaximumValue();

		if(effectivedate.getDayOfMonth()!=1){
			if(benefitenddate==null){
				int actualnoofdays = ( totalnoofdays - effectivedate.dayOfMonth().get()) +1;
				//prorateamount = FinancialMgmtGenericUtil.add(prorateamount,FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getTotalPremium(), new BigDecimal((float)actualnoofdays/(float)totalnoofdays)));
				prorateamount = FinancialMgmtGenericUtil.add(prorateamount,FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getTotalPremium(), BigDecimal.valueOf((double)actualnoofdays/(double)totalnoofdays)));
			}
			employerInvoiceLineItems.setTotalPremium(prorateamount);
			employerInvoiceLineItems.setNetAmount(prorateamount);
		}     

		LOGGER.info(" calculateNewProRataPremiumForBinder(-) ends successfully =");
	}

	private int calculateNewNormalPremiumForCancel(EmployerInvoiceLineItems employerInvoiceLineItems,Date effectiveDate, Date terminationDate,Date lastInvoicedate,String reissue,Date reissueInvoiceStatementDate,Date enrollmentdate,Date terminatDate,String isSpecialEnrollment,int lastInvoiceId,boolean isContributionSet){
		LOGGER.info(" calculateNewNormalPremiumForCancel(-,-,-,-) method started with effectiveDate:"+effectiveDate+"+terminationDate:"+terminationDate+"lastInvoicedate:"+lastInvoicedate+"reissue:"+reissue+"isSpecialEnrollment:"+isSpecialEnrollment);
		int employerFee = FinanceConstants.NUMERICAL_0;
		DateTime currentdate = TSDateTime.getInstance();
		currentdate = setJodaTime(currentdate);
		DateTime lastbillingdate = new DateTime(lastInvoicedate);
		DateTime reissuedate = null;
        if(reissueInvoiceStatementDate!=null){
        	reissuedate = new DateTime(reissueInvoiceStatementDate);
        }
		DateTime actualenrollmentdate = new DateTime(enrollmentdate);
		DateTime terminateDate = new DateTime(terminatDate);
		EmployerInvoiceLineItems lastInvoiceEmployerInvoiceLineItems = null;
		
		if(terminateDate.isAfter(lastbillingdate) && terminationDate!=null && actualenrollmentdate.isBefore(lastbillingdate)){
			if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
					FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
				lastInvoiceEmployerInvoiceLineItems = getLastLineItemForEnrollment(lastInvoiceId,employerInvoiceLineItems.getEnrollmentId(),currentdate);
			}
			else{
				lastInvoiceEmployerInvoiceLineItems = getLastLineItemForEnrollment(lastInvoiceId,employerInvoiceLineItems.getEnrollmentId(),reissuedate);
			}
			if(lastInvoiceEmployerInvoiceLineItems!=null){
				isContributionSet =  true;
				
				employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
				employerInvoiceLineItems.setRetroAdjustments(lastInvoiceEmployerInvoiceLineItems.getNetAmount().negate());
				employerInvoiceLineItems.setEmployerContribution(lastInvoiceEmployerInvoiceLineItems.getEmployerContribution().negate());
				employerInvoiceLineItems.setEmployeeContribution(lastInvoiceEmployerInvoiceLineItems.getEmployeeContribution().negate());
	
				//in future in order to do proper calculation of employer fee, we need to store the count at line item level 
				//and retrieve it and set the negation of the same to employerFee
				employerFee = FinanceConstants.NUMERICAL_NEG_1;
			}
		}
        LOGGER.info(" calculateNewNormalPremiumForCancel(-) ends successfully =");
		return employerFee;
	}

	/**
	 * method to create EmployerInvoiceLineItems list for Binder Invoice 
	 * @param enrollmentCurrentMonthDTOList List<EnrollmentCurrentMonthDTO>
	 * @param reissue String
	 * @param reissueInvoice Object<EmployerInvoices> 
	 * @param periodCovered String
	 * @return
	 */
	public List<EmployerInvoiceLineItems> getBinderEmployerInvoiceData(List<EnrollmentCurrentMonthDTO> enrollmentCurrentMonthDTOList,
			String reissue, EmployerInvoices reissueInvoice, String periodCovered){
		/*		LOGGER.info("======== getBinderEmployerInvoiceData(-,-) method started with employerid: "+employerid);*/

		String noOfGraceDayForBinderInvoice = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.BINDER_GRACE_DAYS);
		String proRatioPaymentSupported = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.PRO_RATIO_SUPPORTED);
		LOGGER.info(" The value of noOfGraceDayForBinderInvoice: "+noOfGraceDayForBinderInvoice);
		List<EmployerInvoiceLineItems> employerInvoiceLineItemsList = new ArrayList<EmployerInvoiceLineItems>();

		//LOGGER.info("= The value of invoiceAmountDue: "+invoiceAmountDue+" for employerid: "+employerid);
		EmployerInvoices employerInvoices = null;

		//if(enrollmentCurrentMonthDTOList.size()>0){
		if(enrollmentCurrentMonthDTOList != null && !enrollmentCurrentMonthDTOList.isEmpty()){
			LOGGER.info(" The size of enrollmentlist: "+enrollmentCurrentMonthDTOList.size()+" ==");
			employerInvoices = new EmployerInvoices();
			for (EnrollmentCurrentMonthDTO enrollmentCurrentMonthDTO : enrollmentCurrentMonthDTOList){

				DateTime effectivedate = new DateTime(enrollmentCurrentMonthDTO.getBenefitEffectiveDate());
				DateTime currentdate = TSDateTime.getInstance();
				currentdate = setJodaTime(currentdate);

				DateTime reissuedate = null;
				if(reissueInvoice!=null){
					reissuedate = new DateTime(reissueInvoice.getStatementDate());
				}

				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){

					if(isGreaterThanNextMonth(currentdate,effectivedate)){
						continue;
					}

				}
				else if(isGreaterThanNextMonth(reissuedate,effectivedate)){
					continue;
				}

				EmployerInvoiceLineItems employerInvoiceLineItems = employerInvoiceLineItemsService
						.createEmployerInvoiceLineItemsFromEnrollmentForBinderInvoice(
								enrollmentCurrentMonthDTO,
								noOfGraceDayForBinderInvoice, periodCovered);

				if(FinanceConstants.YES.equalsIgnoreCase(proRatioPaymentSupported)){
					calculateNewProRataPremiumForBinder(employerInvoiceLineItems,enrollmentCurrentMonthDTO.getBenefitEffectiveDate(),enrollmentCurrentMonthDTO.getBenefitEndDate());
				}

				employerInvoiceLineItems.setEmployerInvoices(employerInvoices);
				employerInvoiceLineItemsList.add(employerInvoiceLineItems);
			}	
		}
		LOGGER.info(" getBinderEmployerInvoiceData(-) ends successfully =");
		return employerInvoiceLineItemsList;
	}

	private boolean isNextMonth(DateTime currentdate, DateTime terminateidate){
		boolean iscurrent = false; 
		DateTime terminateiDate = terminateidate.dayOfMonth().withMaximumValue();
		DateTime currentDate = currentdate.plusMonths(1);
		currentDate = currentDate.dayOfMonth().withMaximumValue();
		//System.out.println("test t e1 "+currentdate);
		if(terminateiDate.toDateMidnight().isEqual(currentDate.toDateMidnight())){
			iscurrent = true;
		}
		return iscurrent;
	}



	private boolean isEnrollmentBilledInLastInvoice(int lastInvoiceid, int enrollmentId, DateTime coveredDate) 
	{
		boolean isEnrollmentBilled = false;
		try {
			EmployerInvoices lastInvoice = employerInvoicesService.findEmployerInvoicesById(lastInvoiceid);
			if(lastInvoice!=null && isCurrentMonth(lastInvoice.getPeriodCovered(),coveredDate)) {
				EmployerInvoiceLineItems employerInvoiceLineItems = employerInvoiceLineItemsService.findByInvoiceAndEnrollmentId(lastInvoiceid, enrollmentId);
				if(employerInvoiceLineItems!=null && employerInvoiceLineItems.getTotalPremium().compareTo(BigDecimal.ZERO) == FinanceConstants.NUMERICAL_1){
					isEnrollmentBilled = true;
				}
			}
		} catch (Exception e) {
			LOGGER.error("====== Exception :: Inside findEmployerInvoicesById ======", e);
		}
		return isEnrollmentBilled;
	}
	
	private EmployerInvoiceLineItems getLastLineItemForEnrollment(int lastInvoiceid, int enrollmentId, DateTime coveredDate) 
	{
		EmployerInvoiceLineItems employerInvoiceLineItems = null;
		try {
			EmployerInvoices lastInvoice = employerInvoicesService.findEmployerInvoicesById(lastInvoiceid);
			if(lastInvoice!=null && isCurrentMonth(lastInvoice.getPeriodCovered(),coveredDate)) {
				employerInvoiceLineItems = employerInvoiceLineItemsService.findByInvoiceAndEnrollmentId(lastInvoiceid, enrollmentId);
			}
		} catch (Exception e) {
			LOGGER.error("====== Exception :: Inside findEmployerInvoicesById ======", e);
		}
		return employerInvoiceLineItems;
	}

	private boolean isCurrentMonth(String periodCovered, final DateTime coveredDate){
		String endDate = periodCovered.substring(periodCovered.lastIndexOf("to")+FinanceConstants.NUMERICAL_2).trim();
		String month = endDate.substring(0,endDate.indexOf('-')).trim();
		String date = endDate.substring(endDate.indexOf('-')+FinanceConstants.NUMERICAL_1,endDate.lastIndexOf('-')).trim();
		String year = endDate.substring(endDate.lastIndexOf('-')+FinanceConstants.NUMERICAL_1,endDate.length()).trim();
		DateTime dt = new DateTime(Integer.parseInt(year),Integer.parseInt(month),Integer.parseInt(date),MIDTIME,MIDTIME,MIDTIME);
		DateTime receivedCoveredDateTime = coveredDate;
		dt = dt.dayOfMonth().withMaximumValue();
		receivedCoveredDateTime = receivedCoveredDateTime.dayOfMonth().withMaximumValue();
		return receivedCoveredDateTime.toDateMidnight().isEqual(dt.toDateMidnight());
	}


	private BigDecimal newSpecialEnrollmentPremium(EnrolleeCurrentMonthDTO enrolleeCurrentMonthDTO,Date lastInvoicedate,String reissue,Date reissueInvoiceStatementDate, DateTime benefitenddate,int lastInvoiceId,int enrollmentId, String enrollmentStatus, DateTime enrollmentEndDate,boolean notCalculated){
		LOGGER.info(" Inside Special Enrollment Premium Calculation Start : Enrollee id "+enrolleeCurrentMonthDTO.getEnrolleeId());
		BigDecimal premiumAdjustment = FinancialMgmtGenericUtil.getDefaultBigDecimal();
		DateTime currentdate = TSDateTime.getInstance();
		DateTime effectiveDate = new DateTime(enrolleeCurrentMonthDTO.getCoverageStartDate());
		DateTime actualenrollmentdate = new DateTime(enrolleeCurrentMonthDTO.getCreatedOn());
		DateTime lastbillingdate = new DateTime(lastInvoicedate);
		DateTime coverageendDate = new DateTime(enrolleeCurrentMonthDTO.getCoverageEndtDate());
		DateTime reissuedate = null;
		boolean doCalculation = true;
		if(ENROLLMENT_STATUS_TERM.equalsIgnoreCase(enrollmentStatus) && isCurrentMonth(coverageendDate, enrollmentEndDate)){
			doCalculation = false;
			notCalculated = true;
		}
		
		if(reissueInvoiceStatementDate!=null){
			reissuedate = new DateTime(reissueInvoiceStatementDate);
		}
		if(actualenrollmentdate.isAfter(lastbillingdate) && effectiveDate.isBefore(benefitenddate) && (FinanceConstants.CONFIRM.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus()) || FinanceConstants.PAYMENT_RECEIVED.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus())
				|| FinanceConstants.PENDING.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus()) || FinanceConstants.ORDER_CONFIRMED.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus()))){
			//DateTime coverageendDate = new DateTime(enrolleeCurrentMonthDTO.getCoverageEndtDate());
			Date terminationDate = enrolleeCurrentMonthDTO.getCoverageEndtDate();
			DateTime terminateDateInDateTimeFormat = TSDateTime.getInstance();
			if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
					FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
				if(terminationDate==null || (terminationDate!=null && isGreaterThanNextMonth(currentdate,coverageendDate))){
					DateTime calculate = TSDateTime.getInstance();
					calculate = calculate.plusMonths(1);
					calculate = calculate.dayOfMonth().withMaximumValue();
					terminateDateInDateTimeFormat = calculate;
				}
				else{ 
					terminateDateInDateTimeFormat = coverageendDate;
				}
			}
			else{
				if(terminationDate==null || (terminationDate!=null && isGreaterThanNextMonth(reissuedate,coverageendDate))){
					DateTime calculate = TSDateTime.getInstance();
					calculate = calculate.dayOfMonth().withMaximumValue();
					terminateDateInDateTimeFormat = calculate;

				}
				else{
					terminateDateInDateTimeFormat = coverageendDate;
				}
			}
			int noOfPremium = 0;
			noOfPremium = differenceInMonths(effectiveDate,terminateDateInDateTimeFormat);

			//doing below addition in case enrollment end date is over then there won't be regular premium, but all premium will be retro, to consider that doing below addition
			if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
					FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
				if(currentdate.isAfter(coverageendDate) || isCurrentMonth(currentdate, coverageendDate)){
					noOfPremium = noOfPremium+1;
				}
			}
			else{
				if(reissuedate.isAfter(coverageendDate) || isCurrentMonth(reissuedate, coverageendDate)){
					noOfPremium = noOfPremium+1;
				}
			}
			if(effectiveDate.getDayOfMonth()!=1){
				LOGGER.info(" Inside Special Enrollment Premium Calculation CONFIRM Pro ratio : noOfPremium "+noOfPremium);
				for(int i=0;i<(noOfPremium-1);i++){
					premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment, FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()));
				}
				LOGGER.info(" Inside Special Enrollment Premium Calculation CONFIRM Pro ratio: premiumAdjustment "+premiumAdjustment);
				int totalnoofdays = effectiveDate.dayOfMonth().getMaximumValue();
				int actualnoofdays =  totalnoofdays - effectiveDate.dayOfMonth().get() +1;
				LOGGER.info(" Inside Special Enrollment Premium Calculation CONFIRM Pro ratio : totalnoofdays "+totalnoofdays+"---actualnoofdays--"+actualnoofdays);
				//premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment,FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()), new BigDecimal((float)actualnoofdays/(float)totalnoofdays)));
				premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment,FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()), BigDecimal.valueOf((double)actualnoofdays/(double)totalnoofdays)));
				LOGGER.info(" Inside Special Enrollment Premium Calculation CONFIRM Pro ratio : final premiumAdjustment "+premiumAdjustment);
			}
			else{
				LOGGER.info(" Inside Special Enrollment Premium Calculation CONFIRM : noOfPremium "+noOfPremium);
				for(int i=0;i<noOfPremium;i++){
					premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment, FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()));
				}
				LOGGER.info(" Inside Special Enrollment Premium Calculation CONFIRM : final premiumAdjustment "+premiumAdjustment);
			}
		}
		else if(FinanceConstants.TERM.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus()) && doCalculation){
			//DateTime coverageendDate = new DateTime(enrolleeCurrentMonthDTO.getCoverageEndtDate());
			Date terminationDate = enrolleeCurrentMonthDTO.getCoverageEndtDate();
			DateTime terminateDateInDateTimeFormat = TSDateTime.getInstance();
			if(actualenrollmentdate.isAfter(lastbillingdate) && effectiveDate.isBefore(benefitenddate)){
				LOGGER.info(" Inside Special Enrollment Premium Calculation TERM : enrollment after  last billing date reissue.."+reissue+"..reissue date"+reissuedate);
				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
					if(terminationDate==null || (terminationDate!=null && isGreaterThanNextMonth(currentdate,coverageendDate))){
						DateTime calculate = TSDateTime.getInstance();
						calculate = calculate.plusMonths(1);
						calculate = calculate.dayOfMonth().withMaximumValue();
						terminateDateInDateTimeFormat = calculate;
					}
					else{ 
						terminateDateInDateTimeFormat = coverageendDate;
					}
				}
				else{
					if(terminationDate==null || (terminationDate!=null && isGreaterThanNextMonth(reissuedate,coverageendDate))){
						DateTime calculate = TSDateTime.getInstance();
						calculate = calculate.dayOfMonth().withMaximumValue();
						terminateDateInDateTimeFormat = calculate;

					}
					else{
						terminateDateInDateTimeFormat = coverageendDate;
					}
				}
				int noOfPremium = differenceInMonths(effectiveDate,terminateDateInDateTimeFormat);
				//doing below addition in case enrollment end date is over then there won't be regular premium, but all premium will be retro, to consider that doing below addition
				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
					if(currentdate.isAfter(coverageendDate) || isCurrentMonth(currentdate, coverageendDate)){
						noOfPremium = noOfPremium+1;
					}
				}
				else{
					if(reissuedate.isAfter(coverageendDate) || isCurrentMonth(reissuedate, coverageendDate)){
						noOfPremium = noOfPremium+1;
					}
				}
				//below if case is applicable only for death event when coverage end date can be in between of the month
				if(!isEndOfMonth(coverageendDate)){
					LOGGER.info(" Inside Special Enrollment Premium Calculation TERM Pro ratio : noOfPremium "+noOfPremium);
					for(int i=0;i<(noOfPremium-1);i++){
						premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment, FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()));
					}
					
					LOGGER.info(" Inside Special Enrollment Premium Calculation TERM Pro ratio: premiumAdjustment "+premiumAdjustment);
					if(effectiveDate.getMonthOfYear()!=coverageendDate.getMonthOfYear() && effectiveDate.getDayOfMonth()!=1){
						//this is for special case for e.g. when we bill on 5th March and the coverage start date is 4th Feb and end date is 4th March
						if(isNextMonth(effectiveDate, coverageendDate)){
							premiumAdjustment = FinancialMgmtGenericUtil.getDefaultBigDecimal();
						}
						int totalnoofdays = effectiveDate.dayOfMonth().getMaximumValue();
						int actualnoofdays =  totalnoofdays - effectiveDate.dayOfMonth().get() +1;
						LOGGER.info(" Inside Special Enrollment Premium Calculation TERM Pro ratio : totalnoofdays "+totalnoofdays+"---actualnoofdays--"+actualnoofdays);
						premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment,FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()),BigDecimal.valueOf((double)actualnoofdays / (double)totalnoofdays) ));
					    
						totalnoofdays = coverageendDate.dayOfMonth().getMaximumValue();
						actualnoofdays =  coverageendDate.dayOfMonth().get();
						LOGGER.info(" Inside Special Enrollment Premium Calculation TERM Pro ratio : totalnoofdays "+totalnoofdays+"---actualnoofdays--"+actualnoofdays);
						premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment,FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()),BigDecimal.valueOf((double)actualnoofdays / (double)totalnoofdays) ));
					}
					else if(effectiveDate.getMonthOfYear()!=coverageendDate.getMonthOfYear() && effectiveDate.getDayOfMonth()==1){
						int totalnoofdays = coverageendDate.dayOfMonth().getMaximumValue();
						int actualnoofdays =  coverageendDate.dayOfMonth().get();
						LOGGER.info(" Inside Special Enrollment Premium Calculation TERM Pro ratio : totalnoofdays "+totalnoofdays+"---actualnoofdays--"+actualnoofdays);
						premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment,FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()),BigDecimal.valueOf((double)actualnoofdays / (double)totalnoofdays) ));
					}
					else{
						int totalnoofdays = coverageendDate.dayOfMonth().getMaximumValue();
						int actualnoofdays = (int)(coverageendDate.dayOfMonth().get() - effectiveDate.dayOfMonth().get())+1;
						LOGGER.info(" Inside Special Enrollment Premium Calculation TERM Pro ratio : totalnoofdays "+totalnoofdays+"---actualnoofdays--"+actualnoofdays);
						premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment,FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()),BigDecimal.valueOf((double)actualnoofdays / (double)totalnoofdays) ));
					}
					LOGGER.info(" Inside Special Enrollment Premium Calculation TERM Pro ratio : final premiumAdjustment "+premiumAdjustment);
				}
				else if(effectiveDate.getDayOfMonth()!=1){
					LOGGER.info(" Inside Special Enrollment Premium Calculation TERM Pro ratio : noOfPremium "+noOfPremium);
					for(int i=0;i<(noOfPremium-1);i++){
						premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment, FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()));
					}
					LOGGER.info(" Inside Special Enrollment Premium Calculation TERM Pro ratio: premiumAdjustment "+premiumAdjustment);
					int totalnoofdays = effectiveDate.dayOfMonth().getMaximumValue();
					int actualnoofdays =  totalnoofdays - effectiveDate.dayOfMonth().get() +1;
					LOGGER.info(" Inside Special Enrollment Premium Calculation TERM Pro ratio : totalnoofdays "+totalnoofdays+"---actualnoofdays--"+actualnoofdays);

					//premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment,FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()),new BigDecimal((float)actualnoofdays/(float)totalnoofdays)));
					premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment,FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()),BigDecimal.valueOf((double)actualnoofdays / (double)totalnoofdays) ));

					LOGGER.info(" Inside Special Enrollment Premium Calculation TERM Pro ratio : final premiumAdjustment "+premiumAdjustment);
				}
				else{
					LOGGER.info(" Inside Special Enrollment Premium Calculation TERM : noOfPremium "+noOfPremium);
					for(int i=0;i<noOfPremium;i++){
						premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment, FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()));
					}
					LOGGER.info(" Inside Special Enrollment Premium Calculation TERM : final premiumAdjustment "+premiumAdjustment);
				}
			}
			else if(actualenrollmentdate.isBefore(lastbillingdate)){
				LOGGER.info(" Inside Special Enrollment Premium Calculation TERM : enrollment before  last billing date reissue.."+reissue+"..reissue date"+reissuedate);
				if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
						FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
					if(isEnrollmentBilledInLastInvoice(lastInvoiceId,enrollmentId,currentdate)){
						if(isPreviousMonth(currentdate,coverageendDate)){
							premiumAdjustment = FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount());
							if(!isEndOfMonth(coverageendDate)){
								int noofdaystorefund = 0;
								int totalnoofdays = coverageendDate.dayOfMonth().getMaximumValue();
								//for cases where both start date and end date is same month but the enrollment was already billed e.f. 4th Feb to 6th Feb and last month
								//on 5th Feb it was billed and now we are refunding on 5th march
								if(isCurrentMonth(effectiveDate, coverageendDate)){
									int noofdaytocharge = (int)(coverageendDate.dayOfMonth().get() - effectiveDate.dayOfMonth().get())+1;
									int noofdaycharged = totalnoofdays - effectiveDate.dayOfMonth().get() +1;
									noofdaystorefund = noofdaycharged - noofdaytocharge;
								}
								else{
									noofdaystorefund = (int)((getEndDateOfMonth(coverageendDate)).dayOfMonth().get() - coverageendDate.dayOfMonth().get());
								}
								premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment,FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()), BigDecimal.valueOf((double)noofdaystorefund/(double)totalnoofdays)));
							}
							premiumAdjustment = premiumAdjustment.negate();
							LOGGER.info(" Inside Special Enrollment Premium Calculation term : previousmonth "+premiumAdjustment);
						}
						else if(isOlderThanPreviousMonth(currentdate,coverageendDate)){
							premiumAdjustment = FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()),new BigDecimal(FinanceConstants.NUMERICAL_2));
							if(!isEndOfMonth(coverageendDate)){
								int noofdaystorefund = 0;
								int totalnoofdays = coverageendDate.dayOfMonth().getMaximumValue();
								if(isCurrentMonth(effectiveDate, coverageendDate)){
									int noofdaytocharge = (int)(coverageendDate.dayOfMonth().get() - effectiveDate.dayOfMonth().get())+1;
									int noofdaycharged = totalnoofdays - effectiveDate.dayOfMonth().get() +1;
									noofdaystorefund = noofdaycharged - noofdaytocharge;
								}
								else{
								    noofdaystorefund = (int)((getEndDateOfMonth(coverageendDate)).dayOfMonth().get() - coverageendDate.dayOfMonth().get());
								}
								premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment,FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()), BigDecimal.valueOf((double)noofdaystorefund/(double)totalnoofdays)));
							}
							premiumAdjustment = premiumAdjustment.negate();
							LOGGER.info(" Inside Special Enrollment Premium Calculation term : OlderThanPreviousMonth "+premiumAdjustment);
						}
						else if(isCurrentMonth(currentdate, coverageendDate)){
							if(!isEndOfMonth(coverageendDate)){
								int noofdaystorefund = (int)((getEndDateOfMonth(coverageendDate)).dayOfMonth().get() - coverageendDate.dayOfMonth().get());
								int totalnoofdays = coverageendDate.dayOfMonth().getMaximumValue();
								premiumAdjustment = FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()), BigDecimal.valueOf((double)noofdaystorefund/(double)totalnoofdays));
								premiumAdjustment = premiumAdjustment.negate();
							}
							LOGGER.info(" Inside Special Enrollment Premium Calculation term : CurrentMonth "+premiumAdjustment);
						}
					}
				}
				else{
					if(isEnrollmentBilledInLastInvoice(lastInvoiceId,enrollmentId,reissuedate)){
						if(isPreviousMonth(reissuedate,coverageendDate)){
							premiumAdjustment = FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount());
							if(!isEndOfMonth(coverageendDate)){
								int noofdaystorefund = 0;
								int totalnoofdays = coverageendDate.dayOfMonth().getMaximumValue();
								if(isCurrentMonth(effectiveDate, coverageendDate)){
									int noofdaytocharge = (int)(coverageendDate.dayOfMonth().get() - effectiveDate.dayOfMonth().get())+1;
									int noofdaycharged = totalnoofdays - effectiveDate.dayOfMonth().get() +1;
									noofdaystorefund = noofdaycharged - noofdaytocharge;
								}
								else{
								    noofdaystorefund = (int)((getEndDateOfMonth(coverageendDate)).dayOfMonth().get() - coverageendDate.dayOfMonth().get());
								}
								premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment,FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()), BigDecimal.valueOf((double)noofdaystorefund/(double)totalnoofdays)));
							}
							premiumAdjustment = premiumAdjustment.negate();
							LOGGER.info(" Inside Special Enrollment Premium Calculation term : previousmonth reissue "+premiumAdjustment);
						}
						else if(isOlderThanPreviousMonth(reissuedate,coverageendDate)){
							premiumAdjustment = FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()),new BigDecimal(FinanceConstants.NUMERICAL_2));
							if(!isEndOfMonth(coverageendDate)){
								int noofdaystorefund = 0;
								int totalnoofdays = coverageendDate.dayOfMonth().getMaximumValue();
								if(isCurrentMonth(effectiveDate, coverageendDate)){
									int noofdaytocharge = (int)(coverageendDate.dayOfMonth().get() - effectiveDate.dayOfMonth().get())+1;
									int noofdaycharged = totalnoofdays - effectiveDate.dayOfMonth().get() +1;
									noofdaystorefund = noofdaycharged - noofdaytocharge;
								}
								else{
								   noofdaystorefund = (int)((getEndDateOfMonth(coverageendDate)).dayOfMonth().get() - coverageendDate.dayOfMonth().get());
								}
								premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment,FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()), BigDecimal.valueOf((double)noofdaystorefund/(double)totalnoofdays)));
							}
							premiumAdjustment = premiumAdjustment.negate();
							LOGGER.info(" Inside Special Enrollment Premium Calculation term : OlderThanPreviousMonth "+premiumAdjustment);
						}
						else if(isCurrentMonth(reissuedate, coverageendDate)){
							if(!isEndOfMonth(coverageendDate)){
								int noofdaystorefund = (int)((getEndDateOfMonth(coverageendDate)).dayOfMonth().get() - coverageendDate.dayOfMonth().get());
								int totalnoofdays = coverageendDate.dayOfMonth().getMaximumValue();
								premiumAdjustment = FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()), BigDecimal.valueOf((double)noofdaystorefund/(double)totalnoofdays));
								premiumAdjustment = premiumAdjustment.negate();
							}
							LOGGER.info(" Inside Special Enrollment Premium Calculation term : CurrentMonth "+premiumAdjustment);
						}
					}
				}
			}
			LOGGER.info(" Inside Special Enrollment Premium Calculation TERM : final premiumAdjustment "+premiumAdjustment);
		}
		else if(FinanceConstants.CANCEL.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus())){
			if(actualenrollmentdate.isBefore(lastbillingdate)){
				try{
					EnrolleeDetailsDto enrolleeDetailsDto = financialMgmtUtils.findEnrolleeDtlBeforeLastInvoice(enrolleeCurrentMonthDTO.getEnrolleeId(),lastInvoicedate);
					coverageendDate = new DateTime(enrolleeDetailsDto.getEffectiveEndDate());
					Date terminationDate = enrolleeDetailsDto.getEffectiveEndDate();
					DateTime enrolleeCoverageStartDate = new DateTime(enrolleeDetailsDto.getEffectiveStartDate());
					DateTime terminateDateInDateTimeFormat = TSDateTime.getInstance();
					if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
							FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
						if(terminationDate==null || (terminationDate!=null && isGreaterThanCurrentMonth(currentdate,coverageendDate))){
							DateTime calculate = TSDateTime.getInstance();
							calculate = calculate.dayOfMonth().withMaximumValue();
							terminateDateInDateTimeFormat = calculate;
						}
						else{ 
							terminateDateInDateTimeFormat = coverageendDate;
						}
					}
					else{
						if(terminationDate==null || (terminationDate!=null && isGreaterThanCurrentMonth(reissuedate,coverageendDate))){
							DateTime calculate = TSDateTime.getInstance();
							calculate = calculate.minusMonths(1);
							calculate = calculate.dayOfMonth().withMaximumValue();
							terminateDateInDateTimeFormat = calculate;
		                }
						else{
							terminateDateInDateTimeFormat = coverageendDate;
						}
					}
					int noOfPremium = 0;
					noOfPremium = differenceInMonths(enrolleeCoverageStartDate,terminateDateInDateTimeFormat);
					//doing below addition in case enrollment end date is over then there won't be regular premium, but all premium will be retro, to consider that doing below addition
					if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
							FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
						if(currentdate.isAfter(terminateDateInDateTimeFormat) || isCurrentMonth(currentdate, terminateDateInDateTimeFormat)){
							noOfPremium = noOfPremium+1;
						}
					}
					else{
						if(reissuedate.isAfter(terminateDateInDateTimeFormat) || isCurrentMonth(reissuedate, terminateDateInDateTimeFormat)){
							noOfPremium = noOfPremium+1;
						}
					}
					if(enrolleeCoverageStartDate.getDayOfMonth()!=1){
						LOGGER.info(" Inside Special Enrollment Premium Calculation CANCEL Pro ratio : noOfPremium "+noOfPremium);
						for(int i=0;i<(noOfPremium-1);i++){
							premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment, FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()));
						}
						LOGGER.info(" Inside Special Enrollment Premium Calculation CANCEL Pro ratio: premiumAdjustment "+premiumAdjustment);
						int totalnoofdays = enrolleeCoverageStartDate.dayOfMonth().getMaximumValue();
						int actualnoofdays =  totalnoofdays - enrolleeCoverageStartDate.dayOfMonth().get() +1;
						LOGGER.info(" Inside Special Enrollment Premium Calculation CANCEL Pro ratio : totalnoofdays "+totalnoofdays+"---actualnoofdays--"+actualnoofdays);
		
						//premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment,FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()), new BigDecimal((float)actualnoofdays/(float)totalnoofdays)));
						premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment,FinancialMgmtGenericUtil.multiply(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()), BigDecimal.valueOf((double)actualnoofdays/(double)totalnoofdays)));
						LOGGER.info(" Inside Special Enrollment Premium Calculation CANCEL Pro ratio : final premiumAdjustment "+premiumAdjustment);
					}
					else{
						LOGGER.info(" Inside Special Enrollment Premium Calculation CANCEL : noOfPremium "+noOfPremium);
						for(int i=0;i<noOfPremium;i++){
							premiumAdjustment = FinancialMgmtGenericUtil.add(premiumAdjustment, FinancialMgmtGenericUtil.getBigDecimalObj(enrolleeCurrentMonthDTO.getEnrollmentAmount()));
						}
						LOGGER.info(" Inside Special Enrollment Premium Calculation CANCEL : final premiumAdjustment "+premiumAdjustment);
					}
					premiumAdjustment = premiumAdjustment.negate();
				}
				catch(GIException e){
					LOGGER.error("newSpecialEnrollmentPremium(-) failed for EnrolleeId: "+enrolleeCurrentMonthDTO.getEnrolleeId());
					LOGGER.error("",e);
				}
			}
			LOGGER.info(" Inside Special Enrollment Premium Calculation TERM : CANCEL premiumAdjustment "+premiumAdjustment);
		}

		LOGGER.info(" Inside Special Enrollment Premium Calculation End : Enrollee id "+enrolleeCurrentMonthDTO.getEnrolleeId());
		return premiumAdjustment;
	}

	private int calculateRetroForReinitiation(int lastInvoiceId,
			Date startDate,Date endDate,
			List<EnrolleeCurrentMonthDTO> enrolleeCurrentMonthDTOList,String reissue,Date reissueInvoiceStatementDate,int enrollmentId,
			String isSpecialEnrollment,EmployerInvoiceLineItems employerInvoiceLineItems,DateTime lastbillingdate, int individualEmployerFeeCount){

		double amount = 0.0d;
		DateTime currentdate = TSDateTime.getInstance();

		DateTime reissuedate = null;
		if(reissueInvoiceStatementDate!=null){
			reissuedate = new DateTime(reissueInvoiceStatementDate);
		}


		DateTime benefitstartdate = new DateTime(startDate);
		DateTime benefitenddate = null;
		if(endDate!=null){
			benefitenddate = new DateTime(endDate);
		}

		if(FinanceConstants.YES.equalsIgnoreCase(isSpecialEnrollment)){
			if(enrolleeCurrentMonthDTOList!=null && !enrolleeCurrentMonthDTOList.isEmpty()){
				for (EnrolleeCurrentMonthDTO enrolleeCurrentMonthDTO : enrolleeCurrentMonthDTOList) {
					if(FinanceConstants.CONFIRM.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus()) || FinanceConstants.PAYMENT_RECEIVED.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus())
							|| FinanceConstants.PENDING.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus()) || FinanceConstants.ORDER_CONFIRMED.equalsIgnoreCase(enrolleeCurrentMonthDTO.getEnrolleeStatus()))
					{
						DateTime actualenrolleedate = new DateTime(enrolleeCurrentMonthDTO.getCreatedOn());
						if(actualenrolleedate.isAfter(lastbillingdate)){
							amount=amount+enrolleeCurrentMonthDTO.getEnrollmentAmount();
						}
					}
				}
			}
			//employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.subtract(employerInvoiceLineItems.getTotalPremium(),new BigDecimal(amount)));
			employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.subtract(employerInvoiceLineItems.getTotalPremium(), BigDecimal.valueOf(amount)));
		}

		if((FinanceConstants.DECISION_Y.equalsIgnoreCase(reissue) && currentdate.getMonthOfYear()==reissuedate.getMonthOfYear()) || 
				FinanceConstants.DECISION_N.equalsIgnoreCase(reissue)){
			if(benefitstartdate.isBefore(currentdate) && !isEnrollmentBilledInLastInvoice(lastInvoiceId, enrollmentId, currentdate)
					&& (isCurrentMonth(currentdate, benefitenddate) || isNextMonth(currentdate,benefitenddate) || isGreaterThanNextMonth(currentdate,benefitenddate))){

				employerInvoiceLineItems.setRetroAdjustments(employerInvoiceLineItems.getTotalPremium());

				if (isNextMonth(currentdate,benefitenddate) || isGreaterThanNextMonth(currentdate,benefitenddate))
				{
					individualEmployerFeeCount = individualEmployerFeeCount + FinanceConstants.NUMERICAL_1;
				}

			}
		}
		else{
			if(benefitstartdate.isBefore(reissuedate) && !isEnrollmentBilledInLastInvoice(lastInvoiceId, enrollmentId, reissuedate)
					&& (isCurrentMonth(reissuedate, benefitenddate) || isNextMonth(reissuedate,benefitenddate) || isGreaterThanNextMonth(reissuedate,benefitenddate))){

				employerInvoiceLineItems.setRetroAdjustments(employerInvoiceLineItems.getTotalPremium());

				if (isNextMonth(reissuedate,benefitenddate) || isGreaterThanNextMonth(reissuedate,benefitenddate))
				{
					individualEmployerFeeCount = individualEmployerFeeCount + FinanceConstants.NUMERICAL_1;
				}
			}
		}
		return individualEmployerFeeCount;
	}

	private boolean isPreviousMonth(DateTime currentDate, DateTime terminateDate){
		boolean isprevious = false; 
		DateTime localTerminateDate = terminateDate.dayOfMonth().withMaximumValue();
		DateTime localCurrentDate = currentDate.minusMonths(1);
		localCurrentDate = localCurrentDate.dayOfMonth().withMaximumValue();
		if(localCurrentDate.toDateMidnight().isEqual(localTerminateDate.toDateMidnight())){
			isprevious = true;
		}
		return isprevious;
	}

	private boolean isOlderThanPreviousMonth(DateTime currentDate, DateTime terminateDate){
		boolean isprevious = false; 
		DateTime localTerminateDate = terminateDate.dayOfMonth().withMaximumValue();
		DateTime localCurrentDate = currentDate.minusMonths(FinanceConstants.NUMERICAL_2);
		localCurrentDate = localCurrentDate.dayOfMonth().withMaximumValue();
		if(localCurrentDate.toDateMidnight().isEqual(localTerminateDate.toDateMidnight()) || localTerminateDate.toDateMidnight().isBefore(localCurrentDate.toDateMidnight())){
			isprevious = true;
		}
		return isprevious;
	}

	private boolean isCurrentMonth(DateTime currentDate, DateTime terminateDate){
		boolean isprevious = false; 
		DateTime localTerminateDate = terminateDate.dayOfMonth().withMaximumValue();
		DateTime localCurrentDate = currentDate;
		localCurrentDate = localCurrentDate.dayOfMonth().withMaximumValue();
		if(localCurrentDate.toDateMidnight().isEqual(localTerminateDate.toDateMidnight())){
			isprevious = true;
		}
		return isprevious;
	}

	private boolean isGreaterThanNextMonth(DateTime currentDate, DateTime terminateDate){
		boolean isgreater = false; 
		DateTime localTerminateDate = terminateDate.dayOfMonth().withMaximumValue();
		DateTime localCurrentDate = currentDate.plusMonths(FinanceConstants.NUMERICAL_1);
		localCurrentDate = localCurrentDate.dayOfMonth().withMaximumValue();

		if(localTerminateDate.toDateMidnight().isAfter(localCurrentDate.toDateMidnight())){
			isgreater = true;
		}
		return isgreater;
	}
	
	private boolean isGreaterThanCurrentMonth(DateTime currentDate, DateTime terminateDate){
		boolean isgreater = false; 
		DateTime localTerminateDate = terminateDate.dayOfMonth().withMaximumValue();
		DateTime localCurrentDate = currentDate;
		localCurrentDate = localCurrentDate.dayOfMonth().withMaximumValue();

		if(localTerminateDate.toDateMidnight().isAfter(localCurrentDate.toDateMidnight())){
			isgreater = true;
		}
		return isgreater;
	}


	private int differenceInMonths(DateTime d1, DateTime d2){
		Months mt = Months.monthsBetween(d1, d2);
		return mt.getMonths();
	}
	
	private boolean isEndOfMonth(DateTime terminateDate){
		boolean isend = false; 
		DateTime d1 = new DateTime(terminateDate.getYear(),terminateDate.getMonthOfYear(),1,0,0,0);
		DateTime localDate = d1.dayOfMonth().withMaximumValue();
		if(localDate.toDateMidnight().isEqual(terminateDate.toDateMidnight())){
			isend = true;
		}
		return isend;
	}
	
	private DateTime getEndDateOfMonth(DateTime terminateDate){
		DateTime endDate = new DateTime(terminateDate.getYear(),terminateDate.getMonthOfYear(),1,0,0,0);
		return endDate.dayOfMonth().withMaximumValue();
	}

	private DateTime setJodaTime(DateTime time){
		DateTime localtime = null;
		localtime = time.withMinuteOfHour(0);
		localtime = localtime.withSecondOfMinute(0);
		localtime = localtime.withMillisOfSecond(0);
		return localtime;
	}
	
	private int setRefundAndPremiumForTermination(EmployerInvoiceLineItems employerInvoiceLineItems, DateTime currentdate,DateTime effectivedate,DateTime benefitenddate, int employerFee){
		BigDecimal prorateamount = FinancialMgmtGenericUtil.getDefaultBigDecimal();
		int noofdaystorefund = 0;
		int totalnoofdays = 0;
		if(isPreviousMonth(currentdate,benefitenddate)){
			prorateamount = FinancialMgmtGenericUtil.add(prorateamount, employerInvoiceLineItems.getTotalPremium());
			if(!isEndOfMonth(benefitenddate)){
				totalnoofdays = benefitenddate.dayOfMonth().getMaximumValue();
				if(isCurrentMonth(effectivedate, benefitenddate)){
					int noofdaytocharge = (int)(benefitenddate.dayOfMonth().get() - effectivedate.dayOfMonth().get())+1;
					int noofdaycharged = totalnoofdays - effectivedate.dayOfMonth().get() +1;
					noofdaystorefund = noofdaycharged - noofdaytocharge;
				}
				else{
				    noofdaystorefund = (int)((getEndDateOfMonth(benefitenddate)).dayOfMonth().get() - benefitenddate.dayOfMonth().get());
				}
				prorateamount = FinancialMgmtGenericUtil.add(prorateamount,FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getTotalPremium(), BigDecimal.valueOf((double)noofdaystorefund/(double)totalnoofdays)));
			}
			employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
			employerInvoiceLineItems.setRetroAdjustments(prorateamount.negate());

			employerFee = FinanceConstants.NUMERICAL_NEG_1;
		}
		else if(isOlderThanPreviousMonth(currentdate,benefitenddate)){
			prorateamount = FinancialMgmtGenericUtil.add(prorateamount, employerInvoiceLineItems.getTotalPremium());
			prorateamount = FinancialMgmtGenericUtil.multiply(prorateamount,new BigDecimal(FinanceConstants.NUMERICAL_2));
			if(!isEndOfMonth(benefitenddate)){
				totalnoofdays = benefitenddate.dayOfMonth().getMaximumValue();
				if(isCurrentMonth(effectivedate, benefitenddate)){
					int noofdaytocharge = (int)(benefitenddate.dayOfMonth().get() - effectivedate.dayOfMonth().get())+1;
					int noofdaycharged = totalnoofdays - effectivedate.dayOfMonth().get() +1;
					noofdaystorefund = noofdaycharged - noofdaytocharge;
				}
				else{
				    noofdaystorefund = (int)((getEndDateOfMonth(benefitenddate)).dayOfMonth().get() - benefitenddate.dayOfMonth().get());
				}
				prorateamount = FinancialMgmtGenericUtil.add(prorateamount,FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getTotalPremium(), BigDecimal.valueOf((double)noofdaystorefund/(double)totalnoofdays)));
			}
			employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
			employerInvoiceLineItems.setRetroAdjustments(prorateamount.negate());

			employerFee = FinanceConstants.NUMERICAL_NEG_2;
		}
		else if(isCurrentMonth(currentdate,benefitenddate)){
			if(!isEndOfMonth(benefitenddate)){
				noofdaystorefund = (int)((getEndDateOfMonth(benefitenddate)).dayOfMonth().get() - benefitenddate.dayOfMonth().get());
				totalnoofdays = benefitenddate.dayOfMonth().getMaximumValue();
				prorateamount = FinancialMgmtGenericUtil.add(prorateamount,FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getTotalPremium(), BigDecimal.valueOf((double)noofdaystorefund/(double)totalnoofdays)));
				employerInvoiceLineItems.setRetroAdjustments(prorateamount.negate());
			}
			employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getDefaultBigDecimal());
		}
		return employerFee;
	}
}
