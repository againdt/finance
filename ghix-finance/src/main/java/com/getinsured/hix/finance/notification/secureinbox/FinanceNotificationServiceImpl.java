package com.getinsured.hix.finance.notification.secureinbox;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.joda.time.Days;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.finance.emailnotification.EmployeeInvoiceDueEmailNotification;
import com.getinsured.hix.finance.emailnotification.EmployerPassedDueDateNotice;
import com.getinsured.hix.finance.emailnotification.EmployerPassedEndDueDateNotice;
import com.getinsured.hix.finance.emailnotification.EmployerPassedMiddleDueDateNotice;
import com.getinsured.hix.finance.emailnotification.EmployerPaymentRemainder;
import com.getinsured.hix.finance.emailnotification.EmployerReminderToPayBinderInvoiceEmail;
import com.getinsured.hix.finance.employer.repository.IEmployerNotificationRepository;
import com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService;
import com.getinsured.hix.finance.employer.service.EmployerInvoicesService;
import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceConstants.EmailNotificationConstant;
import com.getinsured.hix.finance.utils.FinanceGeneralUtility;
import com.getinsured.hix.finance.utils.FinancialMgmtUtils;
import com.getinsured.hix.model.EmployeeDetails;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.EmployerInvoices.InvoiceType;
import com.getinsured.hix.model.EmployerInvoices.PaidStatus;
import com.getinsured.hix.model.EmployerLocation;
import com.getinsured.hix.model.EmployerNotification;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.config.FinanceConfiguration.FinanceConfigurationEnum;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SHOPConfiguration;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
/*import com.getinsured.hix.platform.security.repository.IUserRepository;*/
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * This service implementation class is for GHIX-FINANCE module to send email notifications. 
 * @author reddy_h
 * @since 23-Aug-2013
 */
@Service("financeNotificationService")
public class FinanceNotificationServiceImpl implements
FinanceNotificationService {

	@Autowired private UserService userService;
	@Autowired private NoticeService noticeService;
	@Autowired private IEmployerNotificationRepository employerNotificationRepository;
	/*@Autowired private IUserRepository iUserRepository;*/
	@Autowired private EmployerInvoicesService employerInvoicesService;	
	/*@Autowired private ApplicationContext appContext;*/
	@Autowired private FinancialMgmtUtils financialMgmtUtils;	
	@Autowired private EmployerInvoiceLineItemsService employerInvoiceLineItemsService;
	@Autowired private EmployerReminderToPayBinderInvoiceEmail employerReminderToPayBinderInvoiceEmail;
	private static final Logger LOGGER = LoggerFactory.getLogger(FinanceNotificationServiceImpl.class);
	private static final String NOTICE_FAILURE = "Fail";
	@Autowired private EmployerPassedDueDateNotice employerPassedDueDateNotice;
	@Autowired private EmployerPassedMiddleDueDateNotice employerPassedMiddleDueDateNotice;
	@Autowired private EmployerPassedEndDueDateNotice employerPassedEndDueDateNotice;
	@Autowired private EmployeeInvoiceDueEmailNotification employeeInvoiceDueEmailNotification;
	@Autowired private EmployerPaymentRemainder employerPaymentRemainder;
	/**
	 * Method populateEmployerNotification.
	 * @param employerInvoices EmployerInvoices
	 * @param notice Notice
	 * @param noOfAttempts int
	 * @return EmployerNotification
	 */
	private EmployerNotification populateEmployerNotification(EmployerInvoices employerInvoices, Notice notice, int noOfAttempts)
	{
		EmployerNotification employerNotification = new EmployerNotification();
		employerNotification.setDateCreated(new TSDate());
		employerNotification.setEmployerInvoiceId(employerInvoices);
		employerNotification.setNoOfAttempts(noOfAttempts);
		employerNotification.setNoticesId(notice);
		employerNotification.setUpdatedDate(new TSDate());
		employerNotification.setStatus(EmailNotificationConstant.MAIL_STATUS);
		employerNotification.setAdditionalComments(EmailNotificationConstant.MAIL_STATUS_COMMENT);
		
		return employerNotification;
	}
	
	/**
	 * Method saveEmployerNotification.
	 * @param employerNotification EmployerNotification
	 * @return EmployerNotification
	 * @see com.getinsured.hix.finance.employer.service.EmployerNotificationService#saveEmployerNotification(EmployerNotification)
	 */
	@Override
	@Transactional
	public EmployerNotification saveEmployerNotification(EmployerNotification employerNotification) 
	{
		return employerNotificationRepository.save(employerNotification);	
	}
	
	/**
	 * Method invoicePaymentDueNotification.
	 * @see com.getinsured.hix.finance.employer.service.EmployerNotificationService#invoicePaymentDueNotification()
	 */
	@Override
	public void invoicePaymentDueNotification() throws NoticeServiceException, FinanceException {
		DateTime paymentDueDate= TSDateTime.getInstance();
		paymentDueDate = paymentDueDate.withDayOfMonth(FinanceConstants.NUMERICAL_1);
		DateTime lastMontPaymentDueDate= TSDateTime.getInstance();
		lastMontPaymentDueDate = lastMontPaymentDueDate.minusMonths(1);
		lastMontPaymentDueDate = lastMontPaymentDueDate.withDayOfMonth(FinanceConstants.NUMERICAL_1);
		DateTime currentDate= TSDateTime.getInstance();
		int toDay = currentDate.getDayOfMonth();
		Map<String, Exception> exceptionMap = new HashMap<>();
		String notificationName = "";
		
			List<EmployerInvoices> employerInvoicesList = null;
			if(toDay==15){
			   employerInvoicesList = employerInvoicesService.findDueEmployerInvoicesForNotification(FinanceConstants.DECISION_Y, InvoiceType.NORMAL,paymentDueDate.toDate());
			   notificationName = EmailNotificationConstant.EMPLOYER_PASSED_MIDDLE_DUE_DATE;
			}
			else if(toDay==1||toDay==3||toDay==4){
			   employerInvoicesList = employerInvoicesService.findCancelledInvoiceForTermiantedReason(FinanceConstants.TERMINATON_REASON_STRING, InvoiceType.NORMAL,lastMontPaymentDueDate.toDate(),currentDate.toDate());
			   notificationName = EmailNotificationConstant.EMPLOYER_PASSED_END_DUE_DATE; 
			}
			else if(toDay==2){
			   employerInvoicesList = employerInvoicesService.findDueEmployerInvoicesForNotification(FinanceConstants.DECISION_Y, InvoiceType.NORMAL,paymentDueDate.toDate());
			   notificationName = EmailNotificationConstant.EMPLOYER_PASSED_DUE_DATE; 
			   sendNonPaymentNotification(employerInvoicesList,notificationName,exceptionMap);
			   employerInvoicesList = null;
			   employerInvoicesList = employerInvoicesService.findCancelledInvoiceForTermiantedReason(FinanceConstants.TERMINATON_REASON_STRING, InvoiceType.NORMAL,lastMontPaymentDueDate.toDate(),currentDate.toDate());
			   notificationName = EmailNotificationConstant.EMPLOYER_PASSED_END_DUE_DATE; 
			}
			sendNonPaymentNotification(employerInvoicesList,notificationName,exceptionMap);
			if(!exceptionMap.isEmpty())
			{
				final String exceptionMessage = FinanceGeneralUtility.constructSingleMessageForExceptionDetails("EXCEPTIONS OCCURRED WHILE SENDING EMAIL FOR THE FOLLWING INVOICES:", "Invoice Number", exceptionMap);
				throw new FinanceException(exceptionMessage);
			}
	}
	
	/**
	 * Method binderInvoicePaymentDueNotification.
	 * @see com.getinsured.hix.finance.employer.service.EmployerNotificationService#binderInvoicePaymentDueNotification()
	 */
	@Override
	public void binderInvoicePaymentDueNotification() throws NoticeServiceException, FinanceException{
		
		/*DateTime paymentDueDate = TSDateTime.getInstance();
		paymentDueDate = paymentDueDate.withDayOfMonth(FinanceConstants.NUMERICAL_15);*/
		Date currentDate = new TSDate();
		String response = null;
		try{
			List<EmployerInvoices> dueInvoicesList = employerInvoicesService.findDueEmployerInvoices(FinanceConstants.DECISION_Y, EmployerInvoices.InvoiceType.BINDER_INVOICE, currentDate);
			if(dueInvoicesList != null && !dueInvoicesList.isEmpty())
			{
				for(EmployerInvoices employerInvoices : dueInvoicesList)
				{	
					Map<String, Object> replaceableObject = new HashMap<>();					
					//replaceableObject.put("employerInvoices",employerInvoices);
					response = sendNotification(EmailNotificationConstant.EMPLOYER_BINDER_INVOICE_DUE, replaceableObject, employerInvoices);
					if( response == null || NOTICE_FAILURE.equals(response) ){
						LOGGER.error("Sending notification for the employer with invoice id:"+employerInvoices.getId() +"is failed");
					}
					//this.sendBrokerNotification(EmailNotificationConstant.BROKER_BINDER_INVOICE_DUE, replaceableObject, employerInvoices);
				}
			}else{
				LOGGER.info("No invoice available for notification");
			}
		}catch(NoticeServiceException noticeEx){
			LOGGER.error("Notice Exception: ", noticeEx);
			throw noticeEx;
		}
		catch(Exception exception){
			LOGGER.error(FinanceConstants.ERR_MSG_WHILE_SENDING_EMPLOYER_NOTIFICATION, exception);	
			throw new FinanceException(FinanceConstants.ERR_MSG_WHILE_SENDING_EMPLOYER_NOTIFICATION, exception);
		}
	}
	
	
	/**
	 * Method sendBrokerNotification.
	 * @param templateName String
	 * @param replaceableObject Map<String,Object>
	 * @param employerInvoices EmployerInvoices
	 * @return String
	 * @see com.getinsured.hix.finance.employer.service.EmployerNotificationService#sendNotification(String, Map<String,Object>, EmployerInvoices)
	 */
	
	//Marking this API as unused because this feature will not go in SHOP7 as confirmed by Rajesh
	/*@SuppressWarnings("unused")
	private String sendBrokerNotification(String templateName, Map<String, Object> replaceableObject, EmployerInvoices employerInvoices) throws NoticeServiceException{
    	String message = null;
    	if(employerInvoices!=null){
		LOGGER.info("####################### Send Broker Notification started #####################"+templateName);
		try{
			replaceableObject.putAll(getReplaceableObject(employerInvoices));			
			BrokerDTO brokerResponseDTO = financialMgmtUtils.getBrokerByEmployerId(employerInvoices.getEmployer().getId());
			if(brokerResponseDTO != null && brokerResponseDTO.getId() != FinanceConstants.NUMERICAL_0)
			{
				replaceableObject.put(FinanceConstants.BROKER_F_NAME, brokerResponseDTO.getFirstName());
				replaceableObject.put(FinanceConstants.BROKER_L_NAME, brokerResponseDTO.getLastName());
				List<ModuleUser> moduleUserlist = userService.getModuleUsers(brokerResponseDTO.getId(),ModuleUserService.BROKER_MODULE);
				String fileName = "Notice-"+employerInvoices.getInvoiceNumber()+"-"+new TSDate().getTime()+"-Broker.pdf";
				ModuleUser moduleUser = moduleUserlist.get(0);
				Notice notice = null;
				try{
					String firstName = brokerResponseDTO.getFirstName();
					String lastName = brokerResponseDTO.getLastName();
					firstName = (firstName!=null) ? firstName : "";
					lastName = (lastName!=null) ? lastName : "";
					String contactMail =brokerResponseDTO.getEmail();
					List<String> email = null;
					if(contactMail!=null){
						email = new ArrayList<String>();
						email.add(contactMail);
					}		
			//		notice = noticeService.createModuleNotice(templateName, GhixLanguage.US_EN, replaceableObject, moduleUser.getUser().getId() + "/notification", fileName, moduleUser.getModuleName(), moduleUser.getModuleId(), email, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME), firstName+" "+lastName);
					notice = noticeService.createModuleNotice(templateName, GhixLanguage.US_EN, replaceableObject, moduleUser.getUser().getId() + "/notification", fileName, moduleUser.getModuleName(), moduleUser.getModuleId(), email, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME), firstName+" "+lastName, null, GhixNoticeCommunicationMethod.Email);
				}catch(NoticeServiceException exception){
					LOGGER.error("Notice service exception occurred while sending the broker notification", exception);
					return NOTICE_FAILURE;
				}
				message = "Generated pass due date notification for broker : ";
				message += moduleUser.getUser().getFullName() +",";				
				message = message.substring(0, message.length() -1);
			}
			else
			{
				LOGGER.info("Unable to get Broker details by employer id: "+employerInvoices.getEmployer().getId());				
			}
			LOGGER.info("###################### Send Broker Notification ends ##############################");
		}catch(NoticeServiceException noticeEx){
			LOGGER.error("Exception", noticeEx);
			throw noticeEx;
		}
		catch(Exception exception){
			LOGGER.error("Error occurred while sending the broker notification for invoice id: "+employerInvoices.getId(), exception);
			message = NOTICE_FAILURE;		
		}
    }
    return message;
	}*/	
	
	/**
	 * Method sendNotification.
	 * @param templateName String
	 * @param replaceableObject Map<String,Object>
	 * @param employerInvoices EmployerInvoices
	 * @return String
	 * @see com.getinsured.hix.finance.employer.service.EmployerNotificationService#sendNotification(String, Map<String,Object>, EmployerInvoices)
	 */
	@Override
    public String sendNotification(String templateName, Map<String, Object> replaceableObject, EmployerInvoices employerInvoices) throws NoticeServiceException{
    	String message = null;
    	if(employerInvoices!=null){
		LOGGER.info("####################### Send Notification started #####################"+templateName);
		try{
			replaceableObject.putAll(getReplaceableObject(employerInvoices));
			List<ModuleUser> moduleUserlist = userService.getModuleUsers(employerInvoices.getEmployer().getId(),ModuleUserService.EMPLOYER_MODULE);
			String fileName = "Notice-"+employerInvoices.getInvoiceNumber()+"-"+new TSDate().getTime()+".pdf";
			ModuleUser moduleUser = moduleUserlist.get(0);
			Notice notice = null;
			try{
				/*String firstName = employerInvoices.getEmployer().getContactFirstName();
				String lastName = employerInvoices.getEmployer().getContactLastName();
				firstName = (firstName!=null) ? firstName : "";
				lastName = (lastName!=null) ? lastName : "";*/
				String employerFirstAndLastName = employerInvoices.getEmployer().getContactFirstName()+" "+employerInvoices.getEmployer().getContactLastName();
				replaceableObject.put("employerContactFullName", employerFirstAndLastName);
				String contactMail = employerInvoices.getEmployer().getContactEmail();
				List<String> email = null;
				if(contactMail!=null){
					email = new ArrayList<>();
					email.add(contactMail);
				}				
		//		notice = noticeService.createModuleNotice(templateName, GhixLanguage.US_EN, replaceableObject, moduleUser.getUser().getId() + "/notification", fileName, moduleUser.getModuleName(), moduleUser.getModuleId(), email, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME), employerFirstAndLastName);
				notice = noticeService.createModuleNotice(templateName, GhixLanguage.US_EN, replaceableObject, moduleUser.getUser().getId() + "/notification", fileName, moduleUser.getModuleName(), moduleUser.getModuleId(), email, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME), employerFirstAndLastName, null, GhixNoticeCommunicationMethod.Email);
			}catch(NoticeServiceException exception){
				LOGGER.error("Notice service exception occurred while sending the employer notification", exception);
				return NOTICE_FAILURE;
			}
			message = "Generated notification for Employer : ";
			message += moduleUser.getUser().getFullName() +",";
			int noOfAttempts = 1;
			EmployerNotification employernotification = saveEmployerNotification(populateEmployerNotification(employerInvoices, notice, noOfAttempts));
			LOGGER.info("Generated Notification id: "+ employernotification.getId());
			//added to remove the extra comma added at the end.
			message = message.substring(0, message.length() -1);
			LOGGER.info("###################### Send Notification ends ##############################");
		}catch(NoticeServiceException noticeEx){
			LOGGER.error("Exception", noticeEx);
			throw noticeEx;
		}
		catch(Exception exception){
			LOGGER.error(FinanceConstants.ERR_MSG_WHILE_SENDING_EMPLOYER_NOTIFICATION + " for invoice id: "+employerInvoices.getId(), exception);
			message = NOTICE_FAILURE;		
		}
    }
    return message;
   }
	
	private Map<String, Object> getReplaceableObject(EmployerInvoices employerInvoices) throws NoticeServiceException{
		Map<String, Object> replaceableObject = new HashMap<> ();
		SimpleDateFormat simpleFormatter = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MMMMM_DD_YYYY);
		replaceableObject.put(FinanceConstants.SYSTEM_DATE, simpleFormatter.format(new java.util.Date()));
		replaceableObject.put(FinanceConstants.BINDER_DUE_DATE, simpleFormatter.format(employerInvoices.getPaymentDueDate()));
		replaceableObject.put(FinanceConstants.EMPLOYER_INVOICES, employerInvoices);
		replaceableObject.put(FinanceConstants.DUE_AMOUNT, employerInvoices.getTotalAmountDue());
		DateTime dueMonth = new DateTime(employerInvoices.getPaymentDueDate());
		replaceableObject.put(FinanceConstants.DUEMONTH, dueMonth.monthOfYear().getAsText()+" "+dueMonth.getYear());
		if(EmployerInvoices.InvoiceType.BINDER_INVOICE.equals(employerInvoices.getInvoiceType()))
		{
			replaceableObject.put(FinanceConstants.EFFECTIVEDATE, dueMonth.plusMonths(1).monthOfYear().getAsText() +" 1, "+dueMonth.getYear());
		}
		else
		{
			replaceableObject.put(FinanceConstants.EFFECTIVEDATE, dueMonth.monthOfYear().getAsText()+" 1, "+dueMonth.getYear());
		}
		DateTime graceDate = new DateTime(employerInvoices.getPaymentDueDate());
		graceDate = graceDate.plusDays(FinanceConstants.NUMERICAL_30);
		replaceableObject.put(FinanceConstants.GRACEDATE, graceDate.toDate());
		replaceableObject.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));		
		replaceableObject.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		replaceableObject.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		replaceableObject.put(TemplateTokens.STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
		replaceableObject.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		replaceableObject.put(FinanceConstants.EMPLOYER_BUSINESS_NAME, employerInvoices.getEmployer().getName());
		replaceableObject.put(FinanceConstants.EMPLOYER_CONTACT_FULL_NAME, employerInvoices.getEmployer().getContactFirstName()+" "+employerInvoices.getEmployer().getContactLastName());
		replaceableObject.put(TemplateTokens.SHOP_DASHBOARD,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL)+DynamicPropertiesUtil.getPropertyValue(SHOPConfiguration.SHOPConfigurationEnum.EMPLOYER_DASHBOARD_KEY));
		List<EmployerLocation> empLocList = employerInvoices.getEmployer().getLocations();
		Location empLocation = null;
		if(empLocList!=null && !empLocList.isEmpty()){
			for(EmployerLocation employerLocation : empLocList){
				if(employerLocation.getPrimaryLocation().equals(EmployerLocation.PrimaryLocation.YES)){
					empLocation = employerLocation.getLocation();
				}
			}
		}
		if(empLocation!=null){
			replaceableObject.put(FinanceConstants.CONTACT_ADDRESS, empLocation.getAddress1());
			replaceableObject.put(FinanceConstants.ADDRESS1, empLocation.getAddress1());
			if(StringUtils.isNotEmpty(empLocation.getAddress2()))
			{
				replaceableObject.put(FinanceConstants.ADDRESS2, empLocation.getAddress2()+",<br>");
			}
			else
			{
				replaceableObject.put(FinanceConstants.ADDRESS2, "");
			}
			replaceableObject.put(FinanceConstants.CONTACT_CITY, empLocation.getCity());
			replaceableObject.put(FinanceConstants.CONTACT_STATE, empLocation.getState());
			replaceableObject.put(FinanceConstants.CONTACT_ZIPCODE, empLocation.getZip());
		}
		
		return replaceableObject;
	}
	
	/**
	 * 
	 * @param invoiceid
	 * @return
	 * @throws NoticeServiceException
	 */
	@Override
	public String createPaymentFailureNotice(EmployerInvoices empInvoices, String failureReason, String nsfFlatFeeNote, BigDecimal paidAmount) throws NoticeServiceException
	{
		String response = "";
		try{
			if(empInvoices !=null)
			{
				
				Map<String, Object> noticeReplaceableObject = new HashMap<> ();
				noticeReplaceableObject.put("reason", failureReason);
				String flatFee = "";
				if(StringUtils.isNotBlank(nsfFlatFeeNote)){
				   flatFee = nsfFlatFeeNote+"<br><br>";
				}
				noticeReplaceableObject.put("NSFFlatFeeNote", flatFee);
				noticeReplaceableObject.put("paidAmount", (paidAmount!=null) ? paidAmount.toPlainString() : "0.00");
				response = sendNotification(EmailNotificationConstant.PAYMENT_FAILURE_NOTICE, noticeReplaceableObject, empInvoices);
				if("Fail".equals(response) || response ==null){
					LOGGER.error("Sending Payment Failure notification for the employer with invoice id:"+empInvoices.getId() +"is failed");
				}
			}
			else
			{
				LOGGER.info("========= Unable to process createPaymentFailureNotice since received invoice object is null ========");
			}
		}
		catch(Exception exception){
			LOGGER.error("unknown error occurred while creating Payment Failure notification for employer"+exception.getMessage(), exception);
			throw new NoticeServiceException("unknown error occurred while creating the employer invoice pdf"+exception.getMessage(), exception);
		}
		return response;
	}

	@Override
	public void sendEmployeeInvoiceDueNotification() throws NoticeServiceException, FinanceException
	{
		StringBuilder allArrorMessages = new StringBuilder();
		DateTime paymentDueDate= TSDateTime.getInstance();
		paymentDueDate = paymentDueDate.withDayOfMonth(FinanceConstants.NUMERICAL_1);
		List<EmployerInvoices> employerInvoices = employerInvoicesService.findDueEmployerInvoicesForNotification(FinanceConstants.Y, InvoiceType.NORMAL, paymentDueDate.toDate());
		if(employerInvoices != null && !employerInvoices.isEmpty())
		{
			Map<String, String> notificationData = getTemplateTokens();
			notificationData.put(FinanceConstants.MODULE_NAME, ModuleUserService.EMPLOYEE_MODULE);
			notificationData.put(FinanceConstants.TEMPLATENAME_KEY, FinanceConstants.EmailNotificationConstant.EMPLOYEE_INVOICE_DUE_EMAIL_NOTICE);
				for(EmployerInvoices employerInvoice : employerInvoices)
				{
					String errorMessagesDetails = sendNoticeToEmployeesForInvoice(employerInvoice, notificationData);
					if(StringUtils.isNotEmpty(errorMessagesDetails))
					{
						allArrorMessages.append(errorMessagesDetails);
					}
				}
		}
		if(StringUtils.isNotEmpty(allArrorMessages.toString()))
		{
			throw new FinanceException(allArrorMessages.toString());
		}
	}
	
	/**
	 * 
	 * @param employerInvoice
	 * @param notificationData
	 * @return
	 */
	private String sendNoticeToEmployeesForInvoice(EmployerInvoices employerInvoice, Map<String, String> notificationData)
	{
			Map<String, Exception> exceptionMap = new HashMap<>();
			String errorMessagesDetails = "";
			try
			{
				List<EmployerInvoiceLineItems> employerInvoiceLineItems = employerInvoiceLineItemsService.getInvoiceLineItem(employerInvoice);
				if(employerInvoiceLineItems!=null && !employerInvoiceLineItems.isEmpty())
				{
				List<Integer> employeesIDs = getEmployeesIds(employerInvoiceLineItems);
				Map<Integer, EmployeeDetails> employeesDetails = financialMgmtUtils.getEmployeeDetails(employeesIDs);
				if(employeesDetails != null && !employeesDetails.isEmpty())
				{
					Set<Integer> uniqueEmployeeIds = new HashSet<>(employeesIDs.size());
					for(EmployerInvoiceLineItems employeeInvoiceItem : employerInvoiceLineItems)
					{
						// This condition is to prevent to send duplicate notices to employee for the same invoice
						if(uniqueEmployeeIds.add(employeeInvoiceItem.getEmployee().getId())){
							try
							{
								EmployeeDetails employeeDetails = employeesDetails.get(employeeInvoiceItem.getEmployee().getId());
								EmployerInvoices employerInvoices = employerInvoicesService.findEmployerInvoicesById(employeeInvoiceItem.getEmployerInvoices().getId());
								int paymentGracePeriod = Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.PAYMENT_GRACE_PERIOD));
			                    Calendar c = TSCalendar.getInstance();
			                    c.setTime(employerInvoices.getPaymentDueDate()); 
			                    c.add(Calendar.DATE, paymentGracePeriod);
			                    notificationData.put("LastDayOfGracePeriod", new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MMMMM_DD_YYYY).format(c.getTime()));
			                    EmployerInvoices lastPaidEmployerInvoices = employerInvoicesService.findLastPaidInvoice(employerInvoices.getEmployer().getId());
			                    if(lastPaidEmployerInvoices!=null){
				                    String periodCovered = lastPaidEmployerInvoices.getPeriodCovered();
									if (StringUtils.isNotBlank(periodCovered)) {
										//String periodCovered = employerInvoice.getPeriodCovered();
										String lastPaymentDate = periodCovered.substring(periodCovered.lastIndexOf("to")+FinanceConstants.NUMERICAL_2).trim();
										SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
										Date lastDate = dateFormat.parse(lastPaymentDate);
										DateTime dueMonth = new DateTime(lastDate);
										notificationData.put("lastCoverageDate",dueMonth.monthOfYear().getAsText()+" "+dueMonth.getDayOfMonth()+", "+dueMonth.getYear());
									}
									String employeeEmail = "";		
									if(StringUtils.isNotBlank(employeeDetails.getEmail()))
									{
										employeeEmail=employeeDetails.getEmail();
									}
									else
									{
										LOGGER.info("Email is not present for Employee ID: "+employeeDetails.getId());
										continue;
									}
									
									notificationData.put(FinanceConstants.CONTACT_EMAIL, employeeEmail);
									notificationData.put(FinanceConstants.FILENAME_KEY, "Notice-EmployeeInvoiceDueEmailNotice-"+employerInvoices.getId()+"-"+new TSDate().getTime()+".pdf");
									notificationData.put(FinanceConstants.MODULE_ID, employeeInvoiceItem.getEmployee().getId()+"");
									
									notificationData.put("employeeFullName", employeeDetails.getFirstName()+" "+employeeDetails.getLastName());	
															
									notificationData.put(FinanceConstants.TO_FULLNAME_KEY, employeeInvoiceItem.getEmployee().getName());
									employeeInvoiceDueEmailNotification.setTemplateData(notificationData);
								    Notice notice = employeeInvoiceDueEmailNotification.generateEmail();
								    employeeInvoiceDueEmailNotification.sendEmail(notice);
			                    }
			                    else{
			                    	LOGGER.info("Last paid invoice is null "+employerInvoices.getId());
			     	            }
							}
							catch(Exception ex)
							{
								LOGGER.error("Error while sending email notice to employee Id:"+employeeInvoiceItem.getEmployee().getId(), ex);
								exceptionMap.put(""+employeeInvoiceItem.getEmployee().getId(), ex);
							}
						}
					}
				}
			}
				else{
					exceptionMap.put(employerInvoice.getInvoiceNumber(), new GIException("Null or Empty line items"));
				}
			}
			catch(Exception exception)
			{
				exceptionMap.put(employerInvoice.getInvoiceNumber(), exception);
			}
			
			if(!exceptionMap.isEmpty())
			{
				errorMessagesDetails = FinanceGeneralUtility.constructSingleMessageForExceptionDetails("Exception occurred while sending email notice to employees for the Invoice"+employerInvoice.getInvoiceNumber(), "EmployeeID/InvoiceNum:", exceptionMap);
			}	
			return errorMessagesDetails;
	}
	
	/**
	 * returns the list of employeeIDs
	 */
	private List<Integer> getEmployeesIds(List<EmployerInvoiceLineItems> employerInvoiceLineItems)
	{
		
		Set<Integer> employeeSet = new HashSet<>();
		if(employerInvoiceLineItems !=null && !employerInvoiceLineItems.isEmpty())
		{
			for(EmployerInvoiceLineItems employerInvoiceLineItem : employerInvoiceLineItems)
			{
				employeeSet.add(employerInvoiceLineItem.getEmployee().getId());
			}
		} 
		return new ArrayList<>(employeeSet);
	}
	
	/**
	 * This will return the basic template required values like header, footer and other common values across all templates
	 * @return 
	 * @throws NoticeServiceException
	 */
	private Map<String, Object> getBasicTemplateTokens()
	{
		Map<String, Object> templateData = new HashMap<> ();
		templateData.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));		
		templateData.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		templateData.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		templateData.put(TemplateTokens.STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
		templateData.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		SimpleDateFormat simpleFormatter = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MMMMM_DD_YYYY);
		templateData.put(FinanceConstants.SYSTEM_DATE, simpleFormatter.format(new java.util.Date()));
		return templateData;
	}
	
	/**
	 * It is same as getBasicTemplateTokens() method but returns Map<String, String> data
	 */
	private Map<String, String> getTemplateTokens(){
		Map<String, String> templateData = new HashMap<> ();
		templateData.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));		
		templateData.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		templateData.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		templateData.put(TemplateTokens.STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
		String exchangeURL = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL);
		templateData.put(TemplateTokens.EXCHANGE_URL, exchangeURL);
		templateData.put(TemplateTokens.SHOP_DASHBOARD, exchangeURL+DynamicPropertiesUtil.getPropertyValue(SHOPConfiguration.SHOPConfigurationEnum.EMPLOYER_DASHBOARD_KEY));
		SimpleDateFormat simpleFormatter = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MMMMM_DD_YYYY);
		templateData.put(FinanceConstants.SYSTEM_DATE, simpleFormatter.format(new java.util.Date()));
		return templateData;
	}
	
	@Override
	public void sendEmployerReminderEmailToPayBinderInvoice() throws FinanceException
	{
		DateTime paymentDueDate= TSDateTime.getInstance();
		paymentDueDate = paymentDueDate.withDayOfMonth(FinanceConstants.NUMERICAL_15);
		Map<String, Exception> exceptionMap = new HashMap<>();
			List<EmployerInvoices> dueInvoicesList = employerInvoicesService.findInvoicesByIsActiveAndPaidStatusAndInvoiceTypeAndPaymentDueDate(FinanceConstants.Y, PaidStatus.DUE, InvoiceType.BINDER_INVOICE, paymentDueDate.toDate());
			if(!CollectionUtils.isEmpty(dueInvoicesList))
			{
				Map<String, String> templateData = getTemplateTokens();
					for(EmployerInvoices employerInvoices : dueInvoicesList)
					{
						try
						{
							includeEmployerAddress(employerInvoices.getEmployer(), templateData);
							SimpleDateFormat simpleFormatter = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MMMMM_DD_YYYY);
							templateData.put(FinanceConstants.SYSTEM_DATE, simpleFormatter.format(new java.util.Date()));
							templateData.put("statementDate", simpleFormatter.format(employerInvoices.getStatementDate()));
							templateData.put("paymentDueDate", simpleFormatter.format(employerInvoices.getPaymentDueDate()));
							templateData.put("coverageStartDate", simpleFormatter.format(new DateTime(employerInvoices.getStatementDate()).plusMonths(FinanceConstants.NUMERICAL_1).withDayOfMonth(FinanceConstants.NUMERICAL_1).toDate()));
							String exchangeURL = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL);
							templateData.put("exchangeURL", exchangeURL);
							templateData.put("exchangeShopERLoginPage", exchangeURL+DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_LOGIN_URL));
							templateData.put("employerDashboardURL", exchangeURL+DynamicPropertiesUtil.getPropertyValue(SHOPConfiguration.SHOPConfigurationEnum.EMPLOYER_DASHBOARD_KEY));
							employerReminderToPayBinderInvoiceEmail.setTemplateData(templateData);
							Notice notice = employerReminderToPayBinderInvoiceEmail.generateEmail();
							employerReminderToPayBinderInvoiceEmail.sendEmail(notice);
						}
						catch(Exception e)
						{
							LOGGER.error("Error at sendEmployerReminderPayBinderInvoiceEmailNotification() for invoice:"+employerInvoices.getInvoiceNumber(), e);
							exceptionMap.put(employerInvoices.getInvoiceNumber(), e);
						}
					}
			}
			else
			{
				LOGGER.debug("No DUE binder invoices found");
			}
			
			if(!exceptionMap.isEmpty())
			{
				final String exceptionMessage = FinanceGeneralUtility.constructSingleMessageForExceptionDetails("EXCEPTIONS OCCURRED WHILE SENDING EMAIL FOR THE FOLLWING INVOICES:", "Invoice Number", exceptionMap);
				throw new FinanceException(exceptionMessage);
			}
	}
	
	@Override
	public  void sendNotificationForEmployerWhenAutopaymentMade() throws FinanceException
	{
		DateTime currentDate = TSDateTime.getInstance();
		String configAutoPayDay = DynamicPropertiesUtil.getPropertyValue(FinanceConfigurationEnum.AUTOPAY_DAY_OF_MONTH);
		List<EmployerInvoices> employerInvoicesList = null;
		if(!StringUtils.isBlank(configAutoPayDay))
		{
			int autoPayDay = Integer.parseInt(configAutoPayDay);
			if((currentDate.getDayOfMonth() > autoPayDay) && (currentDate.getDayOfMonth()-autoPayDay==FinanceConstants.NUMERICAL_1))
			{
				currentDate = currentDate.withDayOfMonth(autoPayDay);
				employerInvoicesList = employerInvoicesService.findInprocessInvoicesWhenAutoPayHasDone(FinanceConstants.Y, InvoiceType.NORMAL, currentDate.toDate());
			}
			else
			{
				DateTime autoPayDayInLastMonth = DateTime.now().minusDays(FinanceConstants.NUMERICAL_1).withDayOfMonth(autoPayDay);
				if(Days.daysBetween(autoPayDayInLastMonth, currentDate).getDays()==FinanceConstants.NUMERICAL_1)
				{
					employerInvoicesList = employerInvoicesService.findInprocessInvoicesWhenAutoPayHasDone(FinanceConstants.Y, InvoiceType.NORMAL, autoPayDayInLastMonth.toDate());
				}
			}
			/*if((currentDate.getDayOfMonth() > autoPayDay) && (currentDate.getDayOfMonth()-autoPayDay==1))
			{
				currentDate = currentDate.withDayOfMonth(autoPayDay);
				List<EmployerInvoices> employerInvoicesList = employerInvoicesService.findInprocessInvoicesWhenAutoPayHasDone(FinanceConstants.Y, InvoiceType.NORMAL, currentDate.toDate());
			*/	if(!CollectionUtils.isEmpty(employerInvoicesList))
				{
					Map<String, Exception> exceptionMap = new HashMap<>();
					Map<String, Object> templateData;
					templateData = getBasicTemplateTokens();
					templateData.put(FinanceConstants.MODULE_NAME, ModuleUserService.EMPLOYER_MODULE);
					templateData.put(FinanceConstants.TEMPLATENAME_KEY, FinanceConstants.EmailNotificationConstant.EMPLOYER_AUTOPAY_MADE_NOTICE);
				    for(EmployerInvoices employerInvoice : employerInvoicesList)
					 {
						try 
						 {
							templateData.put(FinanceConstants.FILENAME_KEY, "AutoPaymentDone-"+
									employerInvoice.getInvoiceNumber()+"-"+new TSDate().getTime()+".pdf");
						    templateData.put(FinanceConstants.MODULE_ID, employerInvoice.getEmployer().getId());
							templateData.put(FinanceConstants.EMPLOYER_CONTACT_FULL_NAME, employerInvoice.getEmployer().getContactFirstName()+" "+employerInvoice.getEmployer().getContactLastName());
							List<String> employerEmail = new ArrayList<>();
							employerEmail.add(employerInvoice.getEmployer().getContactEmail());
							templateData.put(FinanceConstants.TOEMAIL_LIST_KEY, employerEmail);
							templateData.put(FinanceConstants.TO_FULLNAME_KEY, employerInvoice.getEmployer().getName());
							DateTime dueMonth = new DateTime(employerInvoice.getPaymentDueDate());
							templateData.put(FinanceConstants.DUEMONTH, dueMonth.monthOfYear().getAsText()+" "+dueMonth.getYear());
							templateData.put("dueAmount", employerInvoice.getTotalAmountDue());
							sendNotification(templateData);
						 }
						catch (Exception e)
						{
								LOGGER.error("Error while sending notification for invoice "+employerInvoice.getInvoiceNumber(), e);
								exceptionMap.put(employerInvoice.getInvoiceNumber(), e);
						}
					 }
						if(!exceptionMap.isEmpty())
						{
							throw new FinanceException(FinanceGeneralUtility.constructSingleMessageForExceptionDetails("Errors occurred while sending notification for the invoices ","InvoiceNumber:", exceptionMap));
						}	
				}
			
		}
		else
		{
			LOGGER.error("Autopay Day is not properly configured "+FinanceConfigurationEnum.AUTOPAY_DAY_OF_MONTH+" "+configAutoPayDay);
		}
		
	}
	
	@Override
	public void sendNotificationToEmployerForAutopayApproachng() throws FinanceException
	{
		DateTime currentDate = TSDateTime.getInstance();
		String configAutoPayDay = DynamicPropertiesUtil.getPropertyValue(FinanceConfigurationEnum.AUTOPAY_DAY_OF_MONTH);
		int preBillingNoticeDays = Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(FinanceConfigurationEnum.PRE_BILLING_NOTIFICATION_DAYS_FOR_AUTOPAY));
    	Date paymentDueDate;
    	paymentDueDate = DateTime.now().plusMonths(FinanceConstants.NUMERICAL_1).withDayOfMonth(FinanceConstants.NUMERICAL_1).toDate();
    	List<EmployerInvoices> employerInvoiceList = null;
		if(!StringUtils.isBlank(configAutoPayDay))
		{
			int autoPayDay = Integer.parseInt(configAutoPayDay);
			if((currentDate.getDayOfMonth() < autoPayDay) && (autoPayDay-currentDate.getDayOfMonth()==preBillingNoticeDays))
			{
				employerInvoiceList = employerInvoicesService.findPositiveDueEmployerInvoicesForAutoPayNotice(FinanceConstants.Y, InvoiceType.NORMAL, paymentDueDate, BigDecimal.ZERO);
			}
			else
			{
				DateTime autoPayDate = TSDateTime.getInstance();
				autoPayDate = autoPayDate.plusMonths(FinanceConstants.NUMERICAL_1).withDayOfMonth(autoPayDay);
				if(Days.daysBetween(currentDate, autoPayDate).getDays()==preBillingNoticeDays)
				{
					employerInvoiceList = employerInvoicesService.findPositiveDueEmployerInvoicesForAutoPayNotice(FinanceConstants.Y, InvoiceType.NORMAL, paymentDueDate, BigDecimal.ZERO);
				}
			}
		}
		else
		{
			LOGGER.error("Autopay Day is not properly configured "+FinanceConfigurationEnum.AUTOPAY_DAY_OF_MONTH+" "+configAutoPayDay);
		}
		
		if(!CollectionUtils.isEmpty(employerInvoiceList))
		{
			Map<String, Exception> exceptionMap = new HashMap<>();
			Map<String, Object> templateData;
				templateData = getBasicTemplateTokens();
				templateData.put("preBillingNotficationDaysForAutoPay", DynamicPropertiesUtil.getPropertyValue(FinanceConfigurationEnum.PRE_BILLING_NOTIFICATION_DAYS_FOR_AUTOPAY));
				templateData.put(FinanceConstants.MODULE_NAME, ModuleUserService.EMPLOYER_MODULE);
				templateData.put(FinanceConstants.TEMPLATENAME_KEY, FinanceConstants.EmailNotificationConstant.EMPLOYER_AUTOPAY_APPROACHING_NOTICE);
				templateData.put("preBillingNoticeDays", preBillingNoticeDays);
				for(EmployerInvoices employerInvoice : employerInvoiceList)
				{
					try {
						templateData.put(FinanceConstants.FILENAME_KEY, "AutoPaymentApproaching-"+new TSDate().getTime()+".pdf");
						templateData.put(FinanceConstants.MODULE_ID, employerInvoice.getEmployer().getId());
						templateData.put(FinanceConstants.EMPLOYER_CONTACT_FULL_NAME, employerInvoice.getEmployer().getContactFirstName()+" "+employerInvoice.getEmployer().getContactLastName());
						List<String> employerEmail = new ArrayList<>();
						employerEmail.add(employerInvoice.getEmployer().getContactEmail());
						templateData.put(FinanceConstants.TOEMAIL_LIST_KEY, employerEmail);
						templateData.put(FinanceConstants.TO_FULLNAME_KEY, employerInvoice.getEmployer().getName());
						DateTime dueMonth = new DateTime(employerInvoice.getPaymentDueDate());
						templateData.put(FinanceConstants.DUEMONTH, dueMonth.monthOfYear().getAsText()+" "+dueMonth.getYear());
						templateData.put("dueAmount", employerInvoice.getTotalAmountDue());
						sendNotification(templateData);
					}catch (Exception e) {
						LOGGER.error("Error while sending notification for invoice "+employerInvoice.getInvoiceNumber(), e);
						exceptionMap.put(employerInvoice.getInvoiceNumber(), e);
					}
				}
				if(!exceptionMap.isEmpty())
				{
					throw new FinanceException(FinanceGeneralUtility.constructSingleMessageForExceptionDetails("Errors occurred while sending notification for the invoices ","InvoiceNumber:", exceptionMap));
				}	
				
		}
		
	}
	
    private Map<String, String> includeEmployerAddress(Employer employer, Map<String, String> templateData){
		
		templateData.put(FinanceConstants.EMPLOYER_BUSINESS_NAME, employer.getName());
		templateData.put(FinanceConstants.CONTACT_EMAIL, employer.getContactEmail());
		templateData.put(FinanceConstants.EMPLOYER_CONTACT_FULL_NAME, employer.getContactFirstName()+" "+employer.getContactLastName());
		List<EmployerLocation> empLocList = employer.getLocations();
		Location primaryLocation = null;
		if(empLocList!=null && !empLocList.isEmpty()){
			for(EmployerLocation employerLocation : empLocList){
				if(employerLocation.getPrimaryLocation().equals(EmployerLocation.PrimaryLocation.YES)){
					primaryLocation = employerLocation.getLocation();
				}
			}
		}
		
		if(primaryLocation!=null){
			templateData.put(FinanceConstants.CONTACT_ADDRESS, primaryLocation.getAddress1());
			templateData.put(FinanceConstants.ADDRESS2, primaryLocation.getAddress2()!=null?primaryLocation.getAddress2():FinanceConstants.EMPTY);
			templateData.put(FinanceConstants.CONTACT_CITY, primaryLocation.getCity());
			templateData.put(FinanceConstants.CONTACT_STATE, primaryLocation.getState());
			templateData.put(FinanceConstants.CONTACT_ZIPCODE, primaryLocation.getZip());
		}
		return templateData;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Notice sendNotification(Map<String, Object> templateData) throws NoticeServiceException
	{
		return noticeService.createModuleNotice(templateData.get(FinanceConstants.TEMPLATENAME_KEY).toString(),
					GhixLanguage.US_EN,
					templateData,
					"notification/"+templateData.get(FinanceConstants.MODULE_NAME).toString()+"/"+templateData.get(FinanceConstants.MODULE_ID),
					templateData.get(FinanceConstants.FILENAME_KEY).toString(), 
					templateData.get(FinanceConstants.MODULE_NAME).toString(),
					Long.parseLong(templateData.get(FinanceConstants.MODULE_ID).toString()),
					(List<String>)templateData.get(FinanceConstants.TOEMAIL_LIST_KEY),
					templateData.get(TemplateTokens.EXCHANGE_NAME).toString(),
					templateData.get(FinanceConstants.TO_FULLNAME_KEY).toString(), null, GhixNoticeCommunicationMethod.Email);
	}
	
	private void sendNonPaymentNotification(List<EmployerInvoices> employerInvoicesList,String notificationName,Map<String, Exception> exceptionMap) {
		
		String response = null;
		
			if(employerInvoicesList != null && !employerInvoicesList.isEmpty())
			{
				for(EmployerInvoices employerInvoices : employerInvoicesList)
				{
					try{
					Map<String, Object> replaceableObject = new HashMap<>();
					//replaceableObject.put("employerInvoices",employerInvoices);
					int paymentGracePeriod = Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.PAYMENT_GRACE_PERIOD));
	                Calendar c = TSCalendar.getInstance();
	                c.setTime(employerInvoices.getPaymentDueDate()); 
	                c.add(Calendar.DATE, paymentGracePeriod);
	                replaceableObject.put("paymentGracePeriod", paymentGracePeriod);
	                replaceableObject.put("LastDayOfGracePeriod", new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MMMMM_DD_YYYY).format(c.getTime()));
	                replaceableObject.put(FinanceConstants.CONTACT_EMAIL, employerInvoices.getEmployer().getContactEmail());
	                EmployerInvoices lastPaidEmployerInvoices = employerInvoicesService.findLastPaidInvoice(employerInvoices.getEmployer().getId());
	                
	                if(lastPaidEmployerInvoices!=null){
	                	String  periodCovered = lastPaidEmployerInvoices.getPeriodCovered();
	                    if (StringUtils.isNotBlank(periodCovered)) {
							//String periodCovered = employerInvoice.getPeriodCovered();
							String lastPaymentDate = periodCovered.substring(periodCovered.lastIndexOf("to")+FinanceConstants.NUMERICAL_2).trim();
							SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
							Date lastDate = dateFormat.parse(lastPaymentDate);
							DateTime dueMonth = new DateTime(lastDate);
							replaceableObject.put("lastCoverageDate",dueMonth.monthOfYear().getAsText()+" "+dueMonth.getDayOfMonth()+", "+dueMonth.getYear());
						}
						
							response = sendNotification(notificationName, replaceableObject, employerInvoices);
							//this.sendBrokerNotification(EmailNotificationConstant.BROKER_PASSED_DUE_DATE, replaceableObject, employerInvoices);
							sendEmployerInvoiceDueEmail(replaceableObject,notificationName,employerInvoices.getInvoiceNumber(),exceptionMap);
						if( response == null || NOTICE_FAILURE.equals(response) ){
							LOGGER.error("Sending notification for the employer with invoice id:"+employerInvoices.getId() +"is failed");
						}
	                }
	                else{
	                	LOGGER.info("Last paid invoice is null "+employerInvoices.getId());
	                }
					}
					catch(Exception exception){
						LOGGER.error(FinanceConstants.ERR_MSG_WHILE_SENDING_EMPLOYER_NOTIFICATION, exception);	
						exceptionMap.put(employerInvoices.getInvoiceNumber(), exception);
					}
				}
		     }
			else{
				LOGGER.info("No invoice available for notification");
			}
		}
	
	
	private void sendEmployerInvoiceDueEmail(Map<String, Object> replaceableObject,String notificationName,String invoiceNumber,Map<String, Exception> exceptionMap)
	{
		Map<String, String> templateData = getTemplateTokens();	
						try
						{
							convertMapOfObjectToString(replaceableObject,templateData); 
							if(EmailNotificationConstant.EMPLOYER_PASSED_DUE_DATE.equalsIgnoreCase(notificationName)){
								employerPassedDueDateNotice.setTemplateData(templateData);
							    Notice notice = employerPassedDueDateNotice.generateEmail();
							    employerPassedDueDateNotice.sendEmail(notice);
							}
							else if(EmailNotificationConstant.EMPLOYER_PASSED_MIDDLE_DUE_DATE.equalsIgnoreCase(notificationName)){
								employerPassedMiddleDueDateNotice.setTemplateData(templateData);
							    Notice notice = employerPassedMiddleDueDateNotice.generateEmail();
							    employerPassedMiddleDueDateNotice.sendEmail(notice);
							}
							else if(EmailNotificationConstant.EMPLOYER_PASSED_END_DUE_DATE.equalsIgnoreCase(notificationName)){
								employerPassedEndDueDateNotice.setTemplateData(templateData);
							    Notice notice = employerPassedEndDueDateNotice.generateEmail();
							    employerPassedEndDueDateNotice.sendEmail(notice);
							}
						}
						catch(Exception e)
						{
							LOGGER.error("Error at sendEmployerInvoiceDueEmail() for invoice:"+invoiceNumber, e);
							exceptionMap.put(invoiceNumber, e);
						}
	}
	
	private void convertMapOfObjectToString(Map<String, Object> replaceableObject,Map<String, String> templateData){
		for(Map.Entry<String, Object> pair : replaceableObject.entrySet()){
	        if("employerContactFullName".equalsIgnoreCase(pair.getKey())){
	        	templateData.put("employerContactFullName", pair.getValue().toString());
	        }
	        else if(FinanceConstants.DUE_AMOUNT.equalsIgnoreCase(pair.getKey())){
	        	BigDecimal bigDecimal = (BigDecimal)pair.getValue();
	        	templateData.put(FinanceConstants.DUE_AMOUNT,  String.valueOf(bigDecimal.doubleValue()));
	        }
	        else if(FinanceConstants.DUEMONTH.equalsIgnoreCase(pair.getKey())){
	        	templateData.put(FinanceConstants.DUEMONTH,  pair.getValue().toString());
	        }
	        else if("LastDayOfGracePeriod".equalsIgnoreCase(pair.getKey())){
	        	templateData.put("LastDayOfGracePeriod",  pair.getValue().toString());
	        }
	        else if("paymentGracePeriod".equalsIgnoreCase(pair.getKey())){
	        	templateData.put("paymentGracePeriod",  pair.getValue().toString());
	        }
	        else if("lastCoverageDate".equalsIgnoreCase(pair.getKey())){
	        	templateData.put("lastCoverageDate",  pair.getValue().toString());
	        }
	        else if(FinanceConstants.CONTACT_EMAIL.equalsIgnoreCase(pair.getKey())){
	        	templateData.put(FinanceConstants.CONTACT_EMAIL,  pair.getValue().toString());
	        }
	    } 
	}
	
	@Override
	public void sendEmployerReminderNotice() throws NoticeServiceException, FinanceException {
		DateTime paymentDueDate= TSDateTime.getInstance();
		paymentDueDate = paymentDueDate.plusMonths(1);
		paymentDueDate = paymentDueDate.withDayOfMonth(FinanceConstants.NUMERICAL_1);
		
		Map<String, Exception> exceptionMap = new HashMap<>();
		String notificationName = "";
		
			List<EmployerInvoices> employerInvoicesList = null;
			employerInvoicesList = employerInvoicesService.findDueEmployerInvoicesForNotification(FinanceConstants.DECISION_Y, InvoiceType.NORMAL,paymentDueDate.toDate());
			notificationName = EmailNotificationConstant.EMPLOYER_REMINDER_NOTICE;
			sendEmployerRemainderNotification(employerInvoicesList,notificationName,exceptionMap);
			if(!exceptionMap.isEmpty())
			{
				final String exceptionMessage = FinanceGeneralUtility.constructSingleMessageForExceptionDetails("EXCEPTIONS OCCURRED WHILE SENDING EMAIL FOR THE FOLLWING INVOICES:", "Invoice Number", exceptionMap);
				throw new FinanceException(exceptionMessage);
			}
	}
	
     private void sendEmployerRemainderNotification(List<EmployerInvoices> employerInvoicesList,String notificationName,Map<String, Exception> exceptionMap) {
		
		String response = null;
		
			if(employerInvoicesList != null && !employerInvoicesList.isEmpty())
			{
				for(EmployerInvoices employerInvoices : employerInvoicesList)
				{
					try{
					Map<String, Object> replaceableObject = new HashMap<>();
					//replaceableObject.put("employerInvoices",employerInvoices);
					replaceableObject.put("paymentDueDate", new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MMMMM_DD_YYYY).format(employerInvoices.getPaymentDueDate()));
					replaceableObject.put("statementDate", new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MMMMM_DD_YYYY).format(employerInvoices.getStatementDate()));
	                replaceableObject.put(FinanceConstants.CONTACT_EMAIL, employerInvoices.getEmployer().getContactEmail());
	                
							response = sendNotification(notificationName, replaceableObject, employerInvoices);
							Map<String, String> templateData = getTemplateTokens();
							SimpleDateFormat simpleFormatter = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MMMMM_DD_YYYY);
							templateData.put("paymentDueDate", simpleFormatter.format(employerInvoices.getPaymentDueDate()));
							templateData.put("statementDate", simpleFormatter.format(employerInvoices.getStatementDate()));
							templateData.put(FinanceConstants.CONTACT_EMAIL, employerInvoices.getEmployer().getContactEmail());
							templateData.put(FinanceConstants.EMPLOYER_CONTACT_FULL_NAME, employerInvoices.getEmployer().getContactFirstName()+" "+employerInvoices.getEmployer().getContactLastName());
							employerPaymentRemainder.setTemplateData(templateData);
						    Notice notice = employerPaymentRemainder.generateEmail();
						    employerPaymentRemainder.sendEmail(notice);
						if( response == null || NOTICE_FAILURE.equals(response) ){
							LOGGER.error("Sending notification for the employer with invoice id:"+employerInvoices.getId() +"is failed");
						}
	                
					}
					catch(Exception exception){
						LOGGER.error(FinanceConstants.ERR_MSG_WHILE_SENDING_EMPLOYER_NOTIFICATION, exception);	
						exceptionMap.put(employerInvoices.getInvoiceNumber(), exception);
					}
				}
		     }
			else{
				LOGGER.info("No invoice available for notification");
			}
		}
     
    @Override
 	public void sendGroupGoodStandingNotice() throws NoticeServiceException, FinanceException {
 		DateTime settlementDate = TSDateTime.getInstance();
 		settlementDate = settlementDate.minusDays(1);
 		
 		Map<String, Exception> exceptionMap = new HashMap<>();
 		String notificationName = "";
 		
 			List<EmployerInvoices> employerInvoicesList = null;
 			employerInvoicesList = employerInvoicesService.findEmployerInvoicesBasedOnPaymentDateAndSettlementDate(InvoiceType.NORMAL,settlementDate.toDate());
 			notificationName = EmailNotificationConstant.GROUP_GOOD_STANDINGNOTICE;
 			if(employerInvoicesList != null && !employerInvoicesList.isEmpty())
 			{
 				    for(EmployerInvoices employerInvoice : employerInvoicesList)
 					{
 				    	sendGroupGoodStandingNotification(employerInvoice,notificationName,exceptionMap);
 					}
 			}
 			if(!exceptionMap.isEmpty())
 			{
 				final String exceptionMessage = FinanceGeneralUtility.constructSingleMessageForExceptionDetails("EXCEPTIONS OCCURRED WHILE SENDING EMAIL FOR THE FOLLWING INVOICES:", "Invoice Number", exceptionMap);
 				throw new FinanceException(exceptionMessage);
 			}
 	}
    
    private void sendGroupGoodStandingNotification(EmployerInvoices employerInvoice,String notificationName,Map<String, Exception> exceptionMap) {
		
		String response = null;
		try
		{
			List<EmployerInvoiceLineItems> employerInvoiceLineItems = employerInvoiceLineItemsService.getInvoiceLineItem(employerInvoice);
			if(employerInvoiceLineItems!=null && !employerInvoiceLineItems.isEmpty())
			{
			List<Integer> employeesIDs = getEmployeesIds(employerInvoiceLineItems);
			Map<Integer, EmployeeDetails> employeesDetails = financialMgmtUtils.getEmployeeDetails(employeesIDs);
			if(employeesDetails != null && !employeesDetails.isEmpty())
			{
				Set<Integer> uniqueEmployeeIds = new HashSet<>(employeesIDs.size());
				for(EmployerInvoiceLineItems employeeInvoiceItem : employerInvoiceLineItems)
				{
					// This condition is to prevent to send duplicate notices to employee for the same invoice
					if(uniqueEmployeeIds.add(employeeInvoiceItem.getEmployee().getId())){
						
						try
						{
							EmployeeDetails employeeDetails = employeesDetails.get(employeeInvoiceItem.getEmployee().getId());
							
							Map<String, Object> replaceableObject = new HashMap<>();
							String employeeEmail = "";		
							if(StringUtils.isNotBlank(employeeDetails.getEmail()))
							{
								employeeEmail=employeeDetails.getEmail();
							}
							else
							{
								LOGGER.info("Email is not present for Employee ID: "+employeeInvoiceItem.getEmployee().getId());
								continue;
							}
							
							replaceableObject.put(FinanceConstants.EMPLOYEE_NAME, employeeDetails.getFirstName()+" "+employeeDetails.getLastName());
							replaceableObject.put(FinanceConstants.CONTACT_EMAIL, employeeEmail);
							
							response = sendNotificationToEmployee(notificationName, replaceableObject, employeeDetails,employeeInvoiceItem.getEmployee().getId());
									
								if( response == null || NOTICE_FAILURE.equals(response) ){
									LOGGER.error("Sending notification for the employee for Group good standing with employee id: "+employeeInvoiceItem.getEmployee().getId() +"is failed");
								}
			                
						}
						catch(Exception ex)
						{
							LOGGER.error("Error while sending Group good standing notice to employee Id:"+employeeInvoiceItem.getEmployee().getId(), ex);
							exceptionMap.put(""+employeeInvoiceItem.getEmployee().getId(), ex);
						}
					}
				}
			}
			}
			else{
				exceptionMap.put(employerInvoice.getInvoiceNumber(), new GIException("Null or Empty line items"));
			}
        }
		catch(Exception exception)
		{
			exceptionMap.put(employerInvoice.getInvoiceNumber(), exception);
		}
			
	}
    
    /**
	 * Method sendNotificationToEmployee.
	 * @param templateName String
	 * @param replaceableObject Map<String,Object>
	 * @param employeeDetails EmployeeDetails
	 * @return String
	 * @see com.getinsured.hix.finance.employer.service.EmployerNotificationService#sendNotificationToEmployee(String, Map<String,Object>, employeeDetails)
	 */
	@Override
    public String sendNotificationToEmployee(String templateName, Map<String, Object> replaceableObject, EmployeeDetails employeeDetails,int employeeID) throws NoticeServiceException{
    	String message = null;
    	if(employeeDetails!=null){
		LOGGER.info("####################### Send Notification started #####################"+templateName);
		try{
			String employeeName = employeeDetails.getFirstName()+" "+employeeDetails.getLastName();
			replaceableObject.putAll(getReplaceableObjectForEmployee());
			List<ModuleUser> moduleUserlist = userService.getModuleUsers(employeeID,ModuleUserService.EMPLOYEE_MODULE);
			String fileName = "NoticeGroupGoodStanding-"+employeeDetails.getFirstName()+"_"+employeeDetails.getLastName()+"-"+new TSDate().getTime()+".pdf";
			ModuleUser moduleUser = moduleUserlist.get(0);
			Notice notice = null;
			try{
				String contactMail = employeeDetails.getEmail();
				List<String> email = null;
				if(contactMail!=null){
					email = new ArrayList<>();
					email.add(contactMail);
				}				
		//		notice = noticeService.createModuleNotice(templateName, GhixLanguage.US_EN, replaceableObject, moduleUser.getUser().getId() + "/notification", fileName, moduleUser.getModuleName(), moduleUser.getModuleId(), email, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME), employerFirstAndLastName);
				notice = noticeService.createModuleNotice(templateName, GhixLanguage.US_EN, replaceableObject, moduleUser.getUser().getId() + "/notification", fileName, moduleUser.getModuleName(), moduleUser.getModuleId(), email, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME), employeeName, null, GhixNoticeCommunicationMethod.Email);
			}catch(NoticeServiceException exception){
				LOGGER.error("Notice service exception occurred while sending the employer notification", exception);
				return NOTICE_FAILURE;
			}
			message = "Generated notification for Employee : ";
			message += moduleUser.getUser().getFullName() +",";
			//added to remove the extra comma added at the end.
			message = message.substring(0, message.length() -1);
			LOGGER.info("###################### Send Notification ends ##############################");
		}catch(NoticeServiceException noticeEx){
			LOGGER.error("Exception", noticeEx);
			throw noticeEx;
		}
		catch(Exception exception){
			LOGGER.error(FinanceConstants.ERR_MSG_WHILE_SENDING_EMPLOYEE_NOTIFICATION + " for employee id: "+employeeDetails.getId(), exception);
			message = NOTICE_FAILURE;		
		}
    }
    return message;
   }
	
	private Map<String, Object> getReplaceableObjectForEmployee() throws NoticeServiceException{
		Map<String, Object> replaceableObject = new HashMap<> ();
		SimpleDateFormat simpleFormatter = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MMMMM_DD_YYYY);
		replaceableObject.put(FinanceConstants.SYSTEM_DATE, simpleFormatter.format(new java.util.Date()));
		
		replaceableObject.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));		
		replaceableObject.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		replaceableObject.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		replaceableObject.put(TemplateTokens.STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
		replaceableObject.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		
		return replaceableObject;
	}
	
}
