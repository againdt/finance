package com.getinsured.hix.finance.cybersource.utils;

/**
 * Thrown to indicate that a service has failed to locate the ' cybersource key file'.
 *
 */

public class CyberSourceKeyException extends Exception {
	/** Serial version UID required for safe serialization. */
	private static final long serialVersionUID = -1053471180051791761L;

	public CyberSourceKeyException() {
		super();
	}

	public CyberSourceKeyException(String message, Throwable cause) {
		super(message, cause);
	}

	public CyberSourceKeyException(String message) {
		super(message);
	}

	public CyberSourceKeyException(Throwable cause) {
		super(cause);
	}
}
