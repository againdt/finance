package com.getinsured.hix.finance.employer.service;

import java.util.List;

import com.getinsured.hix.model.AdjustmentReasonCode;

/**
 * @since 12th September 2014
 * @author Sharma_k
 *
 */
public interface AdjustmentReasonCodeService 
{
	/**
	 * 
	 * @param id
	 * @return
	 */
	AdjustmentReasonCode findById(Integer id);
	/**
	 * 
	 * @return
	 */
	List<AdjustmentReasonCode> findAllAdjustmentReasonCode();
	
	/**
	 * 
	 * @return
	 */
	List<String> findAllReasonType();

}
