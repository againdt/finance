package com.getinsured.hix.finance.issuer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.IssuerRemittance;


/**
 */
public interface IIssuerRemittanceRepository extends JpaRepository<IssuerRemittance, Integer> 
{
	
	/**
	 * 
	 * @param id
	 * @return IssuerRemittance
	 */
	IssuerRemittance findIssuerRemittanceByIssuerId(int id);
	
	/**
	 * Method getUnpaidIssuerRemittance.
	 * @param startDate Date
	 * @param endDate Date
	 * @return List<IssuerRemittance>
	 */
	@Query("FROM IssuerRemittance as an where an.paidStatus IN ('DUE','PARTIALLY_PAID') and an.isActive = 'Y'")
	List<IssuerRemittance> getUnpaidIssuerRemittance();
	
	/**
	 * Method findLastInvoice.
	 * @param issuerId int
	 * @return IssuerInvoices
	 */
	@Query("FROM IssuerRemittance a where a.issuer.id = :issuerid and a.isActive = 'Y' and a.statementDate = " +
			" (select MAX (statementDate) from IssuerRemittance inv where inv.issuer.id = :issuerid and inv.isActive = 'Y') ")
	IssuerRemittance findLastInvoice(@Param("issuerid") int issuerId);
	
	@Query("select inv.id from IssuerRemittance inv where (LOWER (inv.issuer.name) LIKE LOWER(:issuerName) || '%' )")
	List<Integer> findIssuerinvByIssuername(@Param("issuerName") String issuerName);
	
	@Query("select id from IssuerRemittance where remittanceNumber = UPPER(:remittanceNumber)")
	Integer findIssuerinvByInvNumber(@Param("remittanceNumber") String remittanceNumber);
	
	@Query("select inv.id from IssuerRemittance inv  where inv.remittanceNumber = UPPER(:remittanceNumber) AND (LOWER (inv.issuer.name) LIKE LOWER(:issuerName) || '%' ) ")
	Integer findInvIdByIssuerNameAndInvNum(@Param("issuerName") String issuerName, @Param("remittanceNumber") String remittanceNumber);
	
	/**
	 * 
	 * @param paidStatus
	 * @param invoicePaidDate
	 * @return
	 */
	@Query("FROM IssuerRemittance as issInv WHERE issInv.paidStatus IN(:paidStatus) AND issInv.paidDateTime <= :invoicePaidDate")
	List<IssuerRemittance> findInProcessInvoiceForReconcilation(@Param("paidStatus") IssuerRemittance.PaidStatus paidStatus, @Param("invoicePaidDate") java.util.Date invoicePaidDate);
	
}
