package com.getinsured.hix.finance.saml.utils;

import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObjectBuilderFactory;

/**
 * @since 06th May 2015
 * @author Sharma_k
 * Adding this class to load XmlObjectBuilderFactory via Spring bean, Passing constructor argument 
 *
 */
public class XmlObjectBuilderFactory 
{
	private static XMLObjectBuilderFactory builderFactory;
	
	/**
	 * Private constructor for singleton class
	 */
	private XmlObjectBuilderFactory()
	{
		
	}
	
	
	/**
	 * Get the BuilderFactory Object
	 * 
	 * @return builderFactory
	 * 			{@link XMLObjectBuilderFactory}
	 */
	public static XMLObjectBuilderFactory getSAMLBuilder()throws ConfigurationException 
	{
		if (builderFactory == null)
		{
			DefaultBootstrap.bootstrap();
			builderFactory = Configuration.getBuilderFactory();
		}
		return builderFactory;
	}

}
