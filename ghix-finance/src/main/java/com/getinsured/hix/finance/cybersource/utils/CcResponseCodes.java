package com.getinsured.hix.finance.cybersource.utils;

import java.util.HashMap;
import java.util.Map;

public final class CcResponseCodes {
	
	public static final Map<String, String> RCODE = new HashMap<String,String>();
	public static final Map<String, String> CARDTYPE = new HashMap<String,String>();
	public static final Map<String, String> TESTCARDNO = new HashMap<String,String>();
	public static final Map<String, String> CVNCODE = new HashMap<String,String>();
	public static final Map<String, String> AVSCODE = new HashMap<String,String>();
	
	
	
	/**
	 * Private constructor for Final Class.
	 */
	private CcResponseCodes() {
	}
	
	public static Map<String, String> resultCodes(){
		
		RCODE.put("100", "Successful transaction.");
		RCODE.put("101", "The request is missing one or more required fields.");
		RCODE.put("102", "One or more fields in the request contains invalid data.");
		RCODE.put("110", "Only a partial amount was approved.");
		RCODE.put("150", "Error: General system failure.");
		RCODE.put("151", "Error: The request was received but there was a server timeout.");
		RCODE.put("152", "Error: The request was received, but a service did not finish running in time.");
		RCODE.put("200", "The authorization request was approved by the issuing bank but declined by CyberSource because " +
						 "it did not pass the Address Verification Service (AVS) check.");
		RCODE.put("201", "The issuing bank has questions about the request.");
		RCODE.put("202", "Expired card.");
		RCODE.put("203", "General decline of the card.");
		RCODE.put("204", "Insufficient funds in the account.");
		RCODE.put("205", "Stolen or lost card.");
		RCODE.put("207", "Issuing bank unavailable.");
		RCODE.put("208", "Inactive card or card not authorized for card-not-present transactions.");
		RCODE.put("209", "American Express Card Identification Digits (CID) did not match.");
		RCODE.put("210", "The card has reached the credit limit.");
		RCODE.put("211", "Invalid CVN.");
		RCODE.put("221", "The customer matched an entry on the processor\'s negative file.");
		RCODE.put("230", "The authorization request was approved by the issuing bank but declined by CyberSource because" +
						 " it did not pass the CVN check.");
		RCODE.put("231", "Invalid credit card number.");
		RCODE.put("232", "The card type is not accepted by the payment processor.");
		RCODE.put("233", "General decline by the processor.");
		RCODE.put("234", "There is a problem with your CyberSource merchant configuration.");
		RCODE.put("235", "The requested amount exceeds the originally authorized amount.");
		RCODE.put("236", "Processor failure.");
		RCODE.put("237", "The authorization has already been reversed.");
		RCODE.put("238", "The authorization has already been captured.");
		RCODE.put("239", "The requested transaction amount must match the previous transaction amount.");
		RCODE.put("240", "The card type sent is invalid or does not correlate with the credit card number.");
		RCODE.put("241", "The request ID is invalid.");
		RCODE.put("242", "You requested a capture, but there is no corresponding, unused authorization record.");
		RCODE.put("243", "The transaction has already been settled or reversed.");
		RCODE.put("246", "The capture or credit is not voidable because the capture or credit information has laready been" +
				         " submitted to your processor. Or, you requested a void for a type of transaction that cannot be voided.");
		RCODE.put("247", "You requested a credit for a capture that was previously voided.");
		RCODE.put("250", "Error: The request was received, but there was a timeout at the payment processor.");
		RCODE.put("520", "The authorization request was approved by the issuing bank but declined by CyberSource based on your" +
				         " Smart Authorization settings.");
		
		return RCODE;
	}
	
	
	public static Map<String, String> cardTypes(){
		
		
		CARDTYPE.put("Visa", "001");
		CARDTYPE.put("MasterCard", "002");
		CARDTYPE.put("AmericanExpress", "003");
		CARDTYPE.put("Discover", "004");
		CARDTYPE.put("DinersClub", "005");
		CARDTYPE.put("CarteBlanche", "006");
		CARDTYPE.put("JCB", "007");
		
		return CARDTYPE;
	}
	
	public static Map<String, String> testCardNumbers(){
		
		
		TESTCARDNO.put("amex", "378282246310005");
		TESTCARDNO.put("discover", "6011111111111117");
		TESTCARDNO.put("mastercard", "5555555555554444");
		TESTCARDNO.put("visa", "4111111111111111");
		
		return TESTCARDNO;
	}
	
	public static Map<String, String> cvnCodes(){
		
		
		CVNCODE.put("D", "'The transaction was determined to be suspicious by the issuing bank.");
		CVNCODE.put("I", "'The CVN failed the processor data validation check.");
		CVNCODE.put("M", "'The CVN matched.");
		CVNCODE.put("N", "'The CVN did not match.");
		CVNCODE.put("P", "'The CVN was not processed by the processor for an unspecified reason.");
		CVNCODE.put("S", "'The CVN is on the card but waqs not included in the request.");
		CVNCODE.put("U", "'Card verification is not supported by the issuing bank.");
		CVNCODE.put("X", "'Card verification is not supported by the card association.");
		CVNCODE.put("1", "'Card verification is not supported for this processor or card type.");
		CVNCODE.put("2", "'An unrecognized result code was returned by the processor for the card verification response.");
		CVNCODE.put("3", "'No result code was returned by the processor.");
		
		return CVNCODE;
	}
	
	public static Map<String, String> avsCodes(){
		
		
		AVSCODE.put("A", "Partial match: Street address matches, but 5-digit and 9-digit postal codes do not match.");
		AVSCODE.put("B", "Partial match: Street address matches, but postal code is not verified.");
		AVSCODE.put("C", "No match: Street address and postal code do not match.");
		AVSCODE.put("D", "Match: Street address and postal code match.");
		AVSCODE.put("E", "Invalid: AVS data is invalid or AVS is not allowed for this card type.");
		AVSCODE.put("F", "Partial match: Card member\'s name does not match, but billing postal code matches.");
		AVSCODE.put("G", "Not supported: Non-U.S. issuing bank does not support AVS.");
		AVSCODE.put("H", "Partial match: Card member\'s name does not match, but street address and postal code match.");
		AVSCODE.put("I", "No match: Address not verified.");
		AVSCODE.put("K", "Partial match: Card member\'s name matches, but billing address and billing postal code do not match.");
		AVSCODE.put("L", "Partial match: Card member\'s name and billing postal code match, but billing address does not match.");
		AVSCODE.put("M", "Match: Street address and postal code match.");
		AVSCODE.put("N", "No match: Card member\'s name, street address, or postal code do not match.");
		AVSCODE.put("O", "Partial match: Card member\'s name and billing address match, but billing postal code does not match.");
		AVSCODE.put("P", "Partial match: Postal code matches, but street address not verified.");
		AVSCODE.put("R", "System unavailable.");
		AVSCODE.put("S", "Not supported: U.S. issuing bank does not support AVS.");
		AVSCODE.put("T", "Partial match: Card member\'s name does not match, but street address matches.");
		AVSCODE.put("U", "System unavailable: Address information is unavailable because either the U.S. bank does not support" +
						 " non-U.S. AVS or AVS in a U.S. bank is not functioning properly.");
		AVSCODE.put("V", "Match: Card member\'s name, billing address, and billing postal code match.");
		AVSCODE.put("W", "Partial match: Street address does not match, but 9-digit postal code matches.");
		AVSCODE.put("X", "Match: Street address and 9-digit postal code match.");
		AVSCODE.put("Y", "Match: Street address and 5-digit postal code match.");
		AVSCODE.put("Z", "Partial match: Street address does not match, but 5-digit postal code matches.");
		AVSCODE.put("1", "Not supported: AVS is not supported for this processor or card type.");
		AVSCODE.put("2", "Unrecognized: The processor returned an unrecognized value for the AVS response.");
		
		return AVSCODE;
	}
	
}
