package com.getinsured.hix.finance.employer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.EmployerPayments;

public interface IEmployerPaymentRepository extends JpaRepository<EmployerPayments, Integer> 
{
	EmployerPayments findEmployerPaymentsByMerchantRefCode(String merchantRefCode);

	/**
	 * 
	 * @param employerInvoicesId
	 * @return
	 * Below written query expected to fetch only single Active record.
	 */
	@Query("FROM EmployerPayments empPymt WHERE employerPaymentInvoice.employerInvoices.id = :employerInvoicesId AND employerPaymentInvoice.isActive = 'Y'")
	EmployerPayments findActiveEmpPymtByEmpInvoices(@Param("employerInvoicesId") Integer employerInvoicesId);

	@Query("FROM EmployerPayments a where a.invoiceNumber = UPPER(:invoiceNumber) AND a.status IN ('PAID', 'PARTIALLY_PAID')")
	List<EmployerPayments> findPaidEmployerPaymentsByInvoiceNumber(@Param("invoiceNumber") String invoiceNumber);
}
