package com.getinsured.hix.finance.issuer.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.IssuerPaymentDetail;
import com.getinsured.hix.model.IssuerPaymentDetail.EDIGeneratedFlag;

/**
 */
public interface IIssuerPaymentDetailRepository extends
		JpaRepository<IssuerPaymentDetail, Integer> {
	
	/**
	 * Method getCurrentMonthPaymentDetail.
	 * @param startDate Date
	 * @param endDate Date
	 * @return List<IssuerPaymentDetail>
	 */
	@Query("FROM IssuerPaymentDetail as an where an.paymentDate >= :startDate and an.paymentDate <= :endDate")
	List<IssuerPaymentDetail> getCurrentMonthPaymentDetail(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
	
	/**
	 * Method findByIsEdiGenerated.
	 * @param isEdiGenerated EDIGeneratedFlag
	 * @return List<IssuerPaymentDetail>
	 */
	List<IssuerPaymentDetail> findByIsEdiGenerated(EDIGeneratedFlag isEdiGenerated);
	
	IssuerPaymentDetail findIssuerPaymentDetailByMerchantRefCode(String merchantRefCode);
	
	/**
	 * 
	 * @return
	 */
	@Query("SELECT DISTINCT(i.id) FROM IssuerPaymentDetail AS dtl, Issuer i  WHERE dtl.issuer.id = i.id AND dtl.isEdiGenerated = 'N'")
	List<Integer> getDistinctIssuerEDINotGenerated();
	
	/**
	 * 
	 * @param issuerId
	 * @return
	 */
	@Query("FROM IssuerPaymentDetail AS dtl WHERE dtl.isEdiGenerated = 'N' AND dtl.issuer.id =:issuerId")
	List<IssuerPaymentDetail> findPaymentDtlByIssuer(@Param("issuerId") int issuerId);
	
	/**
	 * 
	 * @param employerInvoiceId
	 * @return
	 */
	@Query("SELECT pymtDtl.transactionId, pymtDtl.merchantRefCode FROM IssuerPaymentDetail AS pymtDtl, IssuerPayments pymt, IssuerPaymentInvoice inv"
			+ " WHERE pymtDtl.issuerPayment.id = pymt.id AND pymt.issuerPaymentInvoice.id = inv.id AND inv.issuerRemittance.id =:issuerRemittanceId")
	List<Object[]> findIssuerPaymentDetailByRemittance(@Param("issuerRemittanceId") Integer issuerRemittanceId);
}
