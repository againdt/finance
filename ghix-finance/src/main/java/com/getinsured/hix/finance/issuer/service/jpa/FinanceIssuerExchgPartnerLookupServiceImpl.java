package com.getinsured.hix.finance.issuer.service.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.finance.issuer.repository.IFinanceIssuerExchgPartnerLookupRepository;
import com.getinsured.hix.finance.issuer.service.FinanceIssuerExchgPartnerLookupService;
import com.getinsured.hix.model.ExchgPartnerLookup;

/**
 * This service implementation class is for GHIX-FINANCE module for finance issuer exchange partnet lookup. 
 * @author sharma_k
 * @since 25-Oct-2013
 */
@Service("financeIssuerExchgPartnerLookupService")
public class FinanceIssuerExchgPartnerLookupServiceImpl implements FinanceIssuerExchgPartnerLookupService
{
	@Autowired
	private IFinanceIssuerExchgPartnerLookupRepository iFinIssExchgPartnerLookupRepository;
	
	/**
	 * Method findExchgPartnerByHiosIssuerID.
	 * @param hiosIssuerID String
	 * @param market String
	 * @param direction String
	 * @param st01 String
	 * @return ExchgPartnerLookup
	 * @see com.getinsured.hix.finance.issuer.service.FinanceIssuerExchgPartnerLookupService#findExchgPartnerByHiosIssuerID(String, String, String, String)
	 */
	@Override                 
	public ExchgPartnerLookup findExchgPartnerByHiosIssuerID(String hiosIssuerID, String market, String direction, String st01)
	{
		return iFinIssExchgPartnerLookupRepository.findExchgPartnerByHiosIssuerID(hiosIssuerID, market, direction, st01);
	}

}
