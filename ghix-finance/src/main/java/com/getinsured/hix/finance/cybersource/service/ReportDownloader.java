package com.getinsured.hix.finance.cybersource.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.finance.utils.FinanceGeneralUtility;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.util.exception.GIException;


/**
 * @since 22nd January 2015
 * @author Sharma_k
 * Interface to download reports from Cybersource Account
 */
public abstract class ReportDownloader 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(ReportDownloader.class);

	/**
	 * 
	 * @return
	 */
	public abstract String  downloadPaymentEventReport()throws GIException;
	
	/**
	 * 
	 * @param requestId
	 * @param merchantReferenceNumber
	 * @param reportDownloadUrl
	 * @return
	 */
	public abstract String downloadSingleTxnReport(String requestId, String merchantReferenceNumber, String reportDownloadUrl)throws GIException;
	
	/**
	 * Method to download Report manually for QA and PROD environment.
	 * @param reportFileName
	 * @return
	 * @throws GIException
	 */
	public String downloadReportFileManually(final String reportFileName)throws GIException
	{
		LOGGER.info("REPORT_DOWNLOAD:: Downloading File: "+reportFileName);
		
		File file = new File(
				DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.UPLOAD_PATH)
						+ File.separator + reportFileName);
		
		LOGGER.info("=========File Path for PaymentEventReport: "
				+ DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.UPLOAD_PATH)
				+ File.separator + reportFileName);
		FileInputStream fis = null;
		try
		{
			fis = new FileInputStream(file);
			return FinanceGeneralUtility.convertStreamToString(fis, "UTF-8");
		}
		catch(Exception ex)
		{
			/*LOGGER.error("Demo Payment Event Exception caught: "+ex);*/
			throw new GIException(ex);
		}
		finally {
			try { if (fis != null) fis.close(); } catch(IOException e) {throw new GIException(e);}
        }
	}
}
