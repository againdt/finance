package com.getinsured.hix.finance.issuer.service;

import com.getinsured.hix.finance.exception.FinanceException;

public interface IssuerRemittanceCreationService 
{
	/**
	 * 
	 * @throws FinanceException
	 */
	void createIssuerRemittance() throws FinanceException;

}
