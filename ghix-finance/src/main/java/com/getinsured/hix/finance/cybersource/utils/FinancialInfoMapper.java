package com.getinsured.hix.finance.cybersource.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;
import com.getinsured.hix.platform.payment.util.PaymentUtil;


/**
 *  This class is responsible to map the FinancialInfo object.
 * 
 * @author panda_p
 * @since 20-Nov-2013
 */
public class FinancialInfoMapper {
	
	@Autowired private PaymentUtil pUtil;
	
	@SuppressWarnings("rawtypes")
	private Map payRequest = new HashMap();
	
	@SuppressWarnings("rawtypes")
	public Map getPayRequest() {
		return payRequest;
	}

	/**
	 * Method setPayRequest
	 * @param finInfo FinancialInfo	 
	 */
	@SuppressWarnings({ "unchecked", "static-access" })
	public void setPayRequest(FinancialInfo finInfo, String merchantRefrenceCode)
	{	
		this.payRequest.put("lastName", finInfo.getLastName());
		this.payRequest.put("firstName", finInfo.getFirstName());
		this.payRequest.put("street1", finInfo.getAddress1());
		this.payRequest.put("city", finInfo.getCity());
		this.payRequest.put("state", finInfo.getState());
		this.payRequest.put("zip", finInfo.getZipcode());
		this.payRequest.put("country", finInfo.getCountry());
		this.payRequest.put("email", finInfo.getEmail());
		if(finInfo.getContactNumber()!=null)
		{
		  this.payRequest.put("contactnumber", finInfo.getContactNumber());
		}
		/*If Credit Card Payment*/	
		if(finInfo.getCreditCardInfo()!=null)
		{
			
			payRequest.put("cardNumber", finInfo.getCreditCardInfo().getCardNumber());
			payRequest.put("cardType", pUtil.getCardTypeCode(finInfo.getCreditCardInfo().getCardType()));
			payRequest.put("expirationMonth", finInfo.getCreditCardInfo().getExpirationMonth());
			payRequest.put("expirationYear", finInfo.getCreditCardInfo().getExpirationYear());
		}
	
		/*If Electronic Check Payment*/
		if(finInfo.getBankInfo()!=null)
		{
			payRequest.put("accountNumber", finInfo.getBankInfo().getAccountNumber());
			payRequest.put("accountType", finInfo.getBankInfo().getAccountType());
			payRequest.put("routingNumber", finInfo.getBankInfo().getRoutingNumber());
		}
		
		/*
		 * Adding merchant Ref. No in the request
		 */
		this.payRequest.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, merchantRefrenceCode);
		
	}
	
	/**
	 * Adding Parameter check_secCode for Credit Transactions
	 * 
	 * @param finInfo
	 * @param checkSecCode
	 */
	@SuppressWarnings("unchecked")
	public void setPayRequestWithSecCode(FinancialInfo finInfo , String checkSecCode, String merchantRefrenceCode)
	{
		setPayRequest(finInfo, merchantRefrenceCode);
		if(StringUtils.isNotEmpty(checkSecCode))
		{
			this.payRequest.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, checkSecCode);
		}
	}
	
	/**
	 * Method setPciPayRequest
	 * @param subscriptionId String
	 * @param merchantRefrenceCode String	 
	 */
	@SuppressWarnings("unchecked")
	public void setPciPayRequest(String subscriptionId, String merchantRefrenceCode)
	{	this.payRequest.clear();
		this.payRequest.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, subscriptionId);
		this.payRequest.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, merchantRefrenceCode);
	}
	
	/**
	 * Adding Parameter check_secCode for Credit Transactions
	 * 
	 * @param subscriptionId
	 * @param merchantRefrenceCode
	 * @param checkSecCode
	 */
	@SuppressWarnings("unchecked")
	public void setPciPayRequestWithSecCode(String subscriptionId, String merchantRefrenceCode, String checkSecCode)
	{
		this.payRequest.clear();
		this.payRequest.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, subscriptionId);
		this.payRequest.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, merchantRefrenceCode);
		if(StringUtils.isNotEmpty(checkSecCode))
		{
			this.payRequest.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, checkSecCode);
		}
		
	}
}
