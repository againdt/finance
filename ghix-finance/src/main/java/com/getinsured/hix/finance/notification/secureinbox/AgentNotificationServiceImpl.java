package com.getinsured.hix.finance.notification.secureinbox;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.broker.BrokerDTO;
import com.getinsured.hix.dto.finance.AgentEmpInvoiceDueNotificationDTO;
import com.getinsured.hix.dto.finance.EmployersDueInvoiceDTO;
import com.getinsured.hix.finance.employer.service.EmployerInvoicesService;
import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceGeneralUtility;
import com.getinsured.hix.finance.utils.FinancialMgmtUtils;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Agent Notification service Implementation
 * @author Sharma_k
 *
 */
@Service("agentNotificationService")
public class AgentNotificationServiceImpl implements AgentNotificationService
{
	@Autowired private EmployerInvoicesService employerInvoicesService;

	@Autowired private FinancialMgmtUtils financialMgmtUtils;

	@Autowired private FinanceNotificationService financeNotificationService;

	@Autowired private UserService userService;
	private static final Logger LOGGER = LoggerFactory.getLogger(AgentNotificationServiceImpl.class);

	@Override
	public void agentNotificationForNormalDueInvoice() throws FinanceException 
	{
		LOGGER.info("AGENT NOTIFICATION:  agentNotificationForNormalDueInvoice starts ...");
		List<EmployerInvoices> dueEmployerInvoiceList = employerInvoicesService
				.findDueEmployerInvoices(FinanceConstants.Y, EmployerInvoices.InvoiceType.NORMAL,
						new java.util.Date());

		if(dueEmployerInvoiceList != null && !dueEmployerInvoiceList.isEmpty())
		{
			LOGGER.info("AGENT NOTIFICATION: No. of Employer Invoices found for notification are: "+dueEmployerInvoiceList.size());
			try
			{
				Map<String, AgentEmpInvoiceDueNotificationDTO> agentNotificationDTOMap = 
						prepareAgentNotificationDTOMap(prepareEmployerInvoiceDataForNotification(dueEmployerInvoiceList));
				/*
				 * Preparing template for Agent Notification 
				 */
				sendNotification(agentNotificationDTOMap);
			}
			catch(GIException ex)
			{
				throw new FinanceException(ex);
			}
		}
		else
		{
			LOGGER.info("AGENT NOTIFICATION: No EmployerInvoice found in DUE status ...");
		}
	}

	/**
	 * 
	 * @param agentNotificationDTOMap
	 * @throws NoticeServiceException
	 */
	private void sendNotification(Map<String, AgentEmpInvoiceDueNotificationDTO> agentNotificationDTOMap)throws FinanceException
	{
		Map<String, Exception> notificationExceptionMap = new HashMap<>();
		for (Map.Entry<String, AgentEmpInvoiceDueNotificationDTO> agentNotificationDTO : agentNotificationDTOMap.entrySet())
		{
			try
			{
				LOGGER.info("AGENT NOTIFICATION: Sending secure inbox email to Broker ID: "+ agentNotificationDTO.getKey());

				List<ModuleUser> moduleUserlist = userService.getModuleUsers(agentNotificationDTO.getValue().getBrokerID(), ModuleUserService.BROKER_MODULE);
				if(moduleUserlist != null && !moduleUserlist.isEmpty())
				{	
					ModuleUser moduleUser = moduleUserlist.get(FinanceConstants.NUMERICAL_0);
					if(moduleUser != null)
					{ 
						Map<String, Object> replaceableObjectMap = new HashMap<>();
						replaceableObjectMap.put(FinanceConstants.FILENAME_KEY, FinanceConstants.AGENT_NOTIFICATION_FILENAME+new TSDate().getTime()+".pdf");
						SimpleDateFormat simpleFormatter = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MMMMM_DD_YYYY);
						replaceableObjectMap.put(FinanceConstants.SYSTEM_DATE, simpleFormatter.format(new java.util.Date()));
						replaceableObjectMap.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));					
						replaceableObjectMap.put(FinanceConstants.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
						replaceableObjectMap.put(TemplateTokens.STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
						replaceableObjectMap.put("ModuleName", moduleUser.getModuleName());
						replaceableObjectMap.put("ModuleID", moduleUser.getModuleId());
						replaceableObjectMap.put(FinanceConstants.BROKER_FIRSTNAME_KEY, agentNotificationDTO.getValue().getBrokerFirstName());
						replaceableObjectMap.put(FinanceConstants.BROKER_LASTNAME_KEY, agentNotificationDTO.getValue().getBrokerLastName());
						List<String> brokerEmailList = new ArrayList<>();
						brokerEmailList.add(agentNotificationDTO.getKey());
						replaceableObjectMap.put(FinanceConstants.TOEMAIL_LIST_KEY, brokerEmailList);
						replaceableObjectMap.put(FinanceConstants.TO_FULLNAME_KEY, agentNotificationDTO
								.getValue().getBrokerFirstName()
								+ " "
								+ agentNotificationDTO.getValue().getBrokerLastName());
						//	replaceableObjectMap.put("employerDetail", agentNotificationDTO.getValue().getEmployersDueInvoiceDTOList());
						replaceableObjectMap.put(FinanceConstants.TOTAL_COUNT_KEY, agentNotificationDTO.getValue().getEmployersDueInvoiceDTOList().size());
						replaceableObjectMap.put(FinanceConstants.TEMPLATENAME_KEY, FinanceConstants.NOTIFICATION_AGENTEMPINVOICEDUE);
						replaceableObjectMap.put("paymentDueDate", agentNotificationDTO.getValue().getEmployersDueInvoiceDTOList().get(FinanceConstants.NUMERICAL_0).getDueDate());
						replaceableObjectMap.put("employerDetailArray", agentNotificationDTO.getValue().getEmployersDueInvoiceDTOList().toArray());
						int loopIterates = agentNotificationDTO.getValue().getEmployersDueInvoiceDTOList().size()/FinanceConstants.NUMERICAL_2;
						loopIterates = loopIterates+(agentNotificationDTO.getValue().getEmployersDueInvoiceDTOList().size()%FinanceConstants.NUMERICAL_2);
						replaceableObjectMap.put("loopIterates", loopIterates);
						financeNotificationService.sendNotification(replaceableObjectMap);
					}
				}
				else
				{
					LOGGER.warn("AGENT NOTIFICATION: No Module User mapping found for Broker ID: "+agentNotificationDTO.getValue().getBrokerID());
				}
			}
			catch(Exception ex)
			{
				notificationExceptionMap.put(agentNotificationDTO.getKey(), ex);
			}
		}
		if(!notificationExceptionMap.isEmpty())
		{
			String exceptionMessage = FinanceGeneralUtility
					.constructSingleMessageForExceptionDetails(
							"EXCEPTIONS OCCURRED WHILE SENDING NOTIFICATION TO", "AGENT ID: ", notificationExceptionMap);
			throw new FinanceException(exceptionMessage);
		}
	}

	/**
	 * 
	 * @param dueEmployerInvoicesList
	 * @return
	 * @throws FinanceException
	 */
	private Map<Integer, EmployersDueInvoiceDTO> prepareEmployerInvoiceDataForNotification(List<EmployerInvoices> dueEmployerInvoicesList)throws FinanceException
	{
		Map<Integer, EmployersDueInvoiceDTO> employersDueInvoiceDTOMap = new HashMap<>();
		LOGGER.info("AGENT NOTIFICATION: Preparing EmployersDueInvoiceDTO for Notification email.");
		for(EmployerInvoices employerInvoices: dueEmployerInvoicesList)
		{
			EmployersDueInvoiceDTO employersDueInvoiceDTO = new EmployersDueInvoiceDTO();
			Employer employer = employerInvoices.getEmployer();
			if(employer != null)
			{
				employersDueInvoiceDTO.setEmployerName(employer.getName());
				employersDueInvoiceDTO.setFirstNameNLastName(employer.getContactFirstName() + " "+employer.getContactLastName());
				employersDueInvoiceDTO.setPhoneNumber(employer.getContactNumber());
				employersDueInvoiceDTO.setEmailID(employer.getContactEmail());
				employersDueInvoiceDTO.setDueDate(employerInvoices.getPaymentDueDate());
				employersDueInvoiceDTO.setTotalAmountDue(employerInvoices.getTotalAmountDue());
				employersDueInvoiceDTOMap.put(employer.getId(), employersDueInvoiceDTO);
			}
		}
		return employersDueInvoiceDTOMap;
	}

	/**
	 * 
	 * @param employersDueInvoiceDTOMap
	 * @return
	 * @throws GIException
	 */
	private Map<String, AgentEmpInvoiceDueNotificationDTO> prepareAgentNotificationDTOMap(
			Map<Integer, EmployersDueInvoiceDTO> employersDueInvoiceDTOMap)throws GIException	
			{
		Map<String, AgentEmpInvoiceDueNotificationDTO> agentNotificationDTOMap = new HashMap<>();
		List<Integer> employerIDList = new ArrayList<>();
		employerIDList.addAll(employersDueInvoiceDTOMap.keySet());

		List<BrokerDTO> brokerDTOList = financialMgmtUtils
				.getDesignatedBrokersForListOfEmployers(employerIDList);
		if(brokerDTOList != null && !brokerDTOList.isEmpty())
		{
			LOGGER.info("AGENT NOTIFICATION: Received Designated Broker data for list of EmployerID sent: "+employerIDList);
			for (BrokerDTO brokerDTO : brokerDTOList) 
			{
				if (!agentNotificationDTOMap.containsKey(brokerDTO.getEmail())) 
				{
					AgentEmpInvoiceDueNotificationDTO agentNotificationDTO = new AgentEmpInvoiceDueNotificationDTO();
					agentNotificationDTO.setBrokerFirstName(brokerDTO.getFirstName());
					agentNotificationDTO.setBrokerLastName(brokerDTO.getLastName());
					agentNotificationDTO.setBrokerID(brokerDTO.getId());
					List<EmployersDueInvoiceDTO> employersDueInvoiceDTOList = new ArrayList<>();
					employersDueInvoiceDTOList.add(employersDueInvoiceDTOMap
							.get(brokerDTO.getEmployerId()));
					agentNotificationDTO.setEmployersDueInvoiceDTOList(employersDueInvoiceDTOList);
					agentNotificationDTOMap.put(brokerDTO.getEmail(), agentNotificationDTO);
				}
				else
				{
					agentNotificationDTOMap.get(brokerDTO
							.getEmail()).getEmployersDueInvoiceDTOList()
							.add(employersDueInvoiceDTOMap.get(brokerDTO.getEmployerId()));
				}
			}
		}
		else
		{
			/*
			 *As required logging no broker found message. 
			 */
			LOGGER.warn("AGENT NOTIFICATION: No Broker found for the List of Employer ID's: "+ employerIDList);
		}
		return agentNotificationDTOMap;
			}
}
