package com.getinsured.hix.finance.issuer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.IssuerRemittance;
import com.getinsured.hix.model.IssuerRemittanceLineItem;

/**
 */
public interface IIssuerRemittanceLineItemRepository extends JpaRepository<IssuerRemittanceLineItem, Integer> 
{
	/**
	 * Method findByIssuerInvoices.
	 * @param issuerInvoices IssuerInvoices
	 * @return List<IssuerRemittanceLineItem>
	 */
	List<IssuerRemittanceLineItem> findByIssuerRemittance(IssuerRemittance issuerInvoices);
	
	//IssuerRemittanceLineItem findByIssuerInvoicesAndEmployerAndEmployeeAndPlanType(IssuerInvoices issuerInvoices, Employer employer, Employee employee, String planType);
	
	/**
	 * Method findByEmployerInvoiceLineItems.
	 * @param employerInvoiceLineItems EmployerInvoiceLineItems
	 * @return IssuerRemittanceLineItem
	 */
	IssuerRemittanceLineItem findByEmployerInvoiceLineItems(EmployerInvoiceLineItems employerInvoiceLineItems);
	
	/*@Query("Select lineItems FROM IssuerRemittanceLineItem as lineItems "+
			" inner join fetch lineItems.enrollment as enrollment"+
			" where lineItems.issuerInvoices.id  = :issuerInvoicesId ")
	List<IssuerRemittanceLineItem> getLineItemsWithEnrollment(@Param("issuerInvoicesId") Integer issuerInvoicesId);*/
	
	@Query("SELECT issLineItems FROM IssuerRemittanceLineItem issLineItems, EmployerInvoiceLineItems empLineItems "
			+ " WHERE issLineItems.employerInvoiceLineItems.id = empLineItems.id "
			+ " AND empLineItems.paidToIssuer ='N' AND empLineItems.paidStatus IN('PAID','PARTIALLY_PAID') AND issLineItems.issuerRemittance.id = :remittanceId"
			+ " AND empLineItems.issuer.id = :issuerId ")
	List<IssuerRemittanceLineItem> getIssLineItemsByPaidEmpLineItems(@Param("remittanceId") int remittanceId, @Param("issuerId")int issuerId);
	
	
	//@Query("FROM IssuerRemittanceLineItem issLineItems WHERE issLineItems.amtPaidToIssuer > 0  AND issLineItems.issuerInvoices.id = :invoiceId")
	@Query("FROM IssuerRemittanceLineItem issLineItems WHERE issLineItems.issuerRemittance.id = :remittanceId")
	List<IssuerRemittanceLineItem> getPaidIssuerLineItems(@Param("remittanceId") int remittanceId);
	
	IssuerRemittanceLineItem findByIssuerRemittanceAndEmployerInvoiceLineItems(IssuerRemittance issuerRemittance, EmployerInvoiceLineItems employerInvoiceLineItems);
}
