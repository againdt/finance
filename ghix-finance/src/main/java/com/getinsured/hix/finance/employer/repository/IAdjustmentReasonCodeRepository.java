package com.getinsured.hix.finance.employer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.getinsured.hix.model.AdjustmentReasonCode;

public interface IAdjustmentReasonCodeRepository extends JpaRepository<AdjustmentReasonCode, Integer>
{
	@Query("SELECT adjReasonCode.reasonType FROM AdjustmentReasonCode as adjReasonCode")
	List<String> findAllReasonType();

}
