package com.getinsured.hix.finance.payment.service;

import java.io.IOException;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.xml.sax.SAXException;

import com.getinsured.hix.finance.payment.dto.PaymentInfo;

/**
 * 
 * @author Sharma_k
 * @since 28th April 2014
 *
 */
public interface ACHPaymentXMLService 
{
	/**
	 * 
	 * @param paymentInfo
	 * @return
	 * @throws JAXBException
	 * Saves ACH XML file and returns file saved location
	 */
	String saveACHPaymentXMLFile(PaymentInfo paymentInfo)throws JAXBException;
	
	/**
	 * 
	 * @param paymentInfo
	 * @return
	 * Validating the generated xml file.
	 */
	void validateACHXMLFile(PaymentInfo paymentInfo)throws SAXException,JAXBException,IOException;
	
	/**
	 * 
	 * @param paymentData
	 * @return
	 * generate PaymentInfo object from Map<String, Object> 	
	 */
	PaymentInfo generatePaymentInfoObject(Map<String, Object>paymentData);
}
