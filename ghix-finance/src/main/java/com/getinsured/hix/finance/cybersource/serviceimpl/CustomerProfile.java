package com.getinsured.hix.finance.cybersource.serviceimpl;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cybersource.ws.client.Client;
import com.getinsured.hix.finance.cybersource.utils.CyberSourceProperties;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceGeneralUtility;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * 
 * @author sharma_k
 * @since 24th September 2013
 * This class serves for both Credit Card and Echeck Create, Update, retrieve and delete 
 */
public class CustomerProfile 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerProfile.class);
	
	private Properties cybProperties;
	
	public CustomerProfile(CyberSourceProperties cyberSourceProperties)
	{
		this.cybProperties  = cyberSourceProperties.getCybProperties();
	}
	

	/**
	 * @author Sharma_k
	 * @since 02 Sept 2013
	 * @param createRequest
	 * @return Map
	 * @throws GIException
	 */
	@SuppressWarnings("rawtypes")
	public Map createCustomerProfile(Map createRequest)throws GIException
	{
		FinanceGeneralUtility.displayMap("CREATE CUSTOMER PROFILE:",createRequest);
		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;
		try
		{
			request.put(CyberSourceKeyConstants.PAYSUBSCRIPTIONCREATESERVICE_RUN_KEY,Boolean.TRUE.toString());
			request.put(CyberSourceKeyConstants.MERCHANTID_KEY, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MERCHANT_ID));
			request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, CyberSourceKeyConstants.MERCHANT_CODE);
			
			request.put(CyberSourceKeyConstants.BILLTO_FIRSTNAME_KEY, createRequest.get(CyberSourceKeyConstants.BILLTO_FIRSTNAME_KEY).toString());
			request.put(CyberSourceKeyConstants.BILLTO_LASTNAME_KEY, createRequest.get(CyberSourceKeyConstants.BILLTO_LASTNAME_KEY).toString());
			request.put(CyberSourceKeyConstants.BILLTO_STREET1_KEY, createRequest.get(CyberSourceKeyConstants.BILLTO_STREET1_KEY).toString());
			request.put(CyberSourceKeyConstants.BILLTO_CITY_KEY,  createRequest.get(CyberSourceKeyConstants.BILLTO_CITY_KEY).toString());
			request.put(CyberSourceKeyConstants.BILLTO_STATE_KEY, createRequest.get(CyberSourceKeyConstants.BILLTO_STATE_KEY).toString());
			request.put(CyberSourceKeyConstants.BILLTO_POSTALCODE_KEY, createRequest.get(CyberSourceKeyConstants.BILLTO_POSTALCODE_KEY).toString());
			request.put(CyberSourceKeyConstants.BILLTO_COUNTRY_KEY, createRequest.get(CyberSourceKeyConstants.BILLTO_COUNTRY_KEY).toString());
			request.put(CyberSourceKeyConstants.BILLTO_EMAIL_KEY, createRequest.get(CyberSourceKeyConstants.BILLTO_EMAIL_KEY).toString());
			request.put(CyberSourceKeyConstants.BILLTO_PHONENUMBER_KEY, createRequest.get(CyberSourceKeyConstants.BILLTO_PHONENUMBER_KEY).toString());
			request.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, createRequest.get(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY).toString());
			request.put(CyberSourceKeyConstants.SUBSCRIPTION_TITLE_KEY, createRequest.get(CyberSourceKeyConstants.SUBSCRIPTION_TITLE_KEY).toString());
			request.put(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY, createRequest.get(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY).toString());
			
			if("credit card".equalsIgnoreCase(createRequest.get(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY).toString()))
			{
				request.put(CyberSourceKeyConstants.CARD_CARDTYPE_KEY, createRequest.get(CyberSourceKeyConstants.CARD_CARDTYPE_KEY).toString());
				request.put(CyberSourceKeyConstants.CARD_ACCOUNTNUMBER_KEY, createRequest.get(CyberSourceKeyConstants.CARD_ACCOUNTNUMBER_KEY).toString());
				request.put(CyberSourceKeyConstants.CARD_EXPIRATIONMONTH_KEY, createRequest.get(CyberSourceKeyConstants.CARD_EXPIRATIONMONTH_KEY).toString());
				request.put(CyberSourceKeyConstants.CARD_EXPIRATIONYEAR_KEY, createRequest.get(CyberSourceKeyConstants.CARD_EXPIRATIONYEAR_KEY).toString());
				if(createRequest.containsKey(CyberSourceKeyConstants.IS_RECURRING_KEY) && createRequest.get(CyberSourceKeyConstants.IS_RECURRING_KEY).toString().equalsIgnoreCase(Boolean.TRUE.toString()))
				{
					request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_FREQUENCY_KEY, createRequest.get(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_FREQUENCY_KEY).toString());
					request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_AMOUNT_KEY, createRequest.get(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_AMOUNT_KEY).toString());
					request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_STARTDATE_KEY, createRequest.get(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_STARTDATE_KEY).toString());
					request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_NUMBEROFPAYMENTS_KEY, createRequest.get(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_NUMBEROFPAYMENTS_KEY).toString());
				}
				else
				{
					request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_FREQUENCY_KEY, createRequest.get(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_FREQUENCY_KEY).toString());
				}
			
			}
			else if("check".equalsIgnoreCase(createRequest.get(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY).toString()))
			{
				request.put(CyberSourceKeyConstants.CHECK_ACCOUNTNUMBER_KEY, createRequest.get(CyberSourceKeyConstants.CHECK_ACCOUNTNUMBER_KEY).toString());
				request.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, createRequest.get(CyberSourceKeyConstants.CHECK_SECCODE_KEY).toString());
				request.put(CyberSourceKeyConstants.CHECK_ACCOUNTTYPE_KEY, createRequest.get(CyberSourceKeyConstants.CHECK_ACCOUNTTYPE_KEY).toString());
				request.put(CyberSourceKeyConstants.CHECK_BANKTRANSITNUMBER_KEY, createRequest.get(CyberSourceKeyConstants.CHECK_BANKTRANSITNUMBER_KEY).toString());
				if(createRequest.containsKey("IS_RECURRING") && createRequest.get("IS_RECURRING").toString().equalsIgnoreCase(Boolean.TRUE.toString()))
				{
					request.put("recurringSubscriptionInfo_frequency", createRequest.get("recurringSubscriptionInfo_frequency").toString());
					request.put("recurringSubscriptionInfo_amount", createRequest.get("recurringSubscriptionInfo_amount").toString());
					request.put("recurringSubscriptionInfo_startDate", createRequest.get("recurringSubscriptionInfo_startDate").toString());
					request.put("recurringSubscriptionInfo_numberOfPayments", createRequest.get("recurringSubscriptionInfo_numberOfPayments").toString());
				}
				else
				{
					request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_FREQUENCY_KEY, createRequest.get(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_FREQUENCY_KEY).toString());
				}
			}
			FinanceGeneralUtility.displayMap("CREATE CUSTOMERPROFILE REQUEST:",request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("CREATE CUSTOMERPROFILE RESPONSE:", reply);
		}
		catch(Exception ex)
		{
			LOGGER.error("PCI: Exception caught while customer subscription "+ex);
			throw new GIException(FinanceConstants.ERROR_CODE_201,ex.toString(),FinanceConstants.EXCEPTION_SEVERITY_HIGH);		
		}
		return reply;
	}
	
	/**
	 * @author Sharma_k
	 * @since 02 Sept 2013
	 * @param updateRequest Map
	 * @return Map
	 * @throws GIException
	 * 
	 */
	@SuppressWarnings("rawtypes")
	public Map updateCustomerProfile(Map updateRequest)throws GIException 
	{
		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;
		try
		{
			request.put(CyberSourceKeyConstants.PAYSUBSCRIPTIONUPDATESERVICE_RUN_KEY, Boolean.TRUE.toString());
			request.put(CyberSourceKeyConstants.MERCHANTID_KEY, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MERCHANT_ID));
			request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, CyberSourceKeyConstants.MERCHANT_CODE);
		
			request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, updateRequest.get(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY).toString());
				if(updateRequest.containsKey(CyberSourceKeyConstants.BILLTO_STREET1_KEY))
				{
					request.put(CyberSourceKeyConstants.BILLTO_STREET1_KEY, updateRequest.get(CyberSourceKeyConstants.BILLTO_STREET1_KEY).toString());
				}
				if(updateRequest.containsKey(CyberSourceKeyConstants.BILLTO_CITY_KEY))
				{
					request.put(CyberSourceKeyConstants.BILLTO_CITY_KEY,  updateRequest.get(CyberSourceKeyConstants.BILLTO_CITY_KEY).toString());
				}
				if(updateRequest.containsKey(CyberSourceKeyConstants.BILLTO_STATE_KEY))
				{
					request.put(CyberSourceKeyConstants.BILLTO_STATE_KEY, updateRequest.get(CyberSourceKeyConstants.BILLTO_STATE_KEY).toString());
				}
				if(updateRequest.containsKey(CyberSourceKeyConstants.BILLTO_POSTALCODE_KEY))
				{
					request.put(CyberSourceKeyConstants.BILLTO_POSTALCODE_KEY, updateRequest.get(CyberSourceKeyConstants.BILLTO_POSTALCODE_KEY).toString());
				}
				if(updateRequest.containsKey(CyberSourceKeyConstants.BILLTO_EMAIL_KEY))
				{
					request.put(CyberSourceKeyConstants.BILLTO_EMAIL_KEY, updateRequest.get(CyberSourceKeyConstants.BILLTO_EMAIL_KEY).toString());
				}
				if(updateRequest.containsKey(CyberSourceKeyConstants.BILLTO_PHONENUMBER_KEY))
				{
					request.put(CyberSourceKeyConstants.BILLTO_PHONENUMBER_KEY, updateRequest.get(CyberSourceKeyConstants.BILLTO_PHONENUMBER_KEY).toString());
				}
				if(updateRequest.containsKey(CyberSourceKeyConstants.SUBSCRIPTION_TITLE_KEY))
				{
					request.put(CyberSourceKeyConstants.SUBSCRIPTION_TITLE_KEY, updateRequest.get(CyberSourceKeyConstants.SUBSCRIPTION_TITLE_KEY).toString());
				}
				
			if(updateRequest.containsKey(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY) &&
					"credit card".equalsIgnoreCase(updateRequest.get(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY).toString()))
			{
				if(updateRequest.containsKey(CyberSourceKeyConstants.CARD_ACCOUNTNUMBER_KEY))
				{
					request.put(CyberSourceKeyConstants.CARD_ACCOUNTNUMBER_KEY, updateRequest.get(CyberSourceKeyConstants.CARD_ACCOUNTNUMBER_KEY).toString());
				}
				if(updateRequest.containsKey(CyberSourceKeyConstants.CARD_CARDTYPE_KEY))
				{
					request.put(CyberSourceKeyConstants.CARD_CARDTYPE_KEY, updateRequest.get(CyberSourceKeyConstants.CARD_CARDTYPE_KEY).toString());
				}
				if(updateRequest.containsKey(CyberSourceKeyConstants.CARD_EXPIRATIONMONTH_KEY))
				{	
					request.put(CyberSourceKeyConstants.CARD_EXPIRATIONMONTH_KEY, updateRequest.get(CyberSourceKeyConstants.CARD_EXPIRATIONMONTH_KEY).toString());
				}
				if(updateRequest.containsKey(CyberSourceKeyConstants.CARD_EXPIRATIONYEAR_KEY))
				{
					request.put(CyberSourceKeyConstants.CARD_EXPIRATIONYEAR_KEY, updateRequest.get(CyberSourceKeyConstants.CARD_EXPIRATIONYEAR_KEY).toString());
				}
				
			}
			else if(updateRequest.containsKey(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY) &&
					"check".equalsIgnoreCase(updateRequest.get(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY).toString()))
			{
				if(updateRequest.containsKey(CyberSourceKeyConstants.CHECK_ACCOUNTNUMBER_KEY))
				{
					request.put(CyberSourceKeyConstants.CHECK_ACCOUNTNUMBER_KEY, updateRequest.get(CyberSourceKeyConstants.CHECK_ACCOUNTNUMBER_KEY).toString());
				}
				if(updateRequest.containsKey(CyberSourceKeyConstants.CHECK_ACCOUNTTYPE_KEY))
				{
					request.put(CyberSourceKeyConstants.CHECK_ACCOUNTTYPE_KEY, updateRequest.get(CyberSourceKeyConstants.CHECK_ACCOUNTTYPE_KEY).toString());
				}
				if(updateRequest.containsKey(CyberSourceKeyConstants.CHECK_BANKTRANSITNUMBER_KEY))
				{
					request.put(CyberSourceKeyConstants.CHECK_BANKTRANSITNUMBER_KEY, updateRequest.get(CyberSourceKeyConstants.CHECK_BANKTRANSITNUMBER_KEY).toString());
				}
				//Adding this field for bank Update as Cybersource require this field with value as zero. 
				request.put(CyberSourceKeyConstants.PURCHASETOTALS_GRANDTOTALAMOUNT_KEY,"0");
			}
			if(!request.isEmpty())
			{
				FinanceGeneralUtility.displayMap("UPDATE CUSTOMERPROFILE REQUEST:", request);
				reply = Client.runTransaction(request, cybProperties);
				FinanceGeneralUtility.displayMap("UPDATE CUSTOMERPROFILE RESPONSE:",reply);
			}
			else
			{
				throw new GIException(FinanceConstants.ERROR_CODE_201,"No response received from Cybersource for the requested info.",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
			}
		}
		catch(Exception ex)
		{
			LOGGER.error("PCI: Exception caught while updating customer profile "+ex);
			throw new GIException(FinanceConstants.ERROR_CODE_201, ex.toString(), FinanceConstants.EXCEPTION_SEVERITY_HIGH);
		}
		return reply;
	}
	
	/**
	 * @author Sharma_k
	 * @since 02 Sept 2013
	 * @param payRequest
	 * @param amount
	 * @return Map
	 * @throws GIException
	 */
	@SuppressWarnings("rawtypes")
	public Map eCheckDebitAccount(Map payRequest, String amount)throws GIException
	{
		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;
		
		request.put(CyberSourceKeyConstants.ECDEBITSERVICE_RUN_KEY, "true");
		request.put(CyberSourceKeyConstants.MERCHANTID_KEY, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MERCHANT_ID));
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, CyberSourceKeyConstants.MERCHANT_CODE);
		request.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, CyberSourceKeyConstants.PT_CURRENCY);
		request.put(CyberSourceKeyConstants.PURCHASETOTALS_GRANDTOTALAMOUNT_KEY, amount);
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, (String)payRequest.get(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY));
		
		try
		{
			FinanceGeneralUtility.displayMap("RECEIVED DEBITACCOUNT REQUEST:",request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("DEBITACCOUNT RESPONSE:",reply);
		}
		catch(Exception ex)
		{
			LOGGER.error("PCI: eCheckDebitAccount exception caught: "+ex);
			throw new GIException(ex);
		}
		return reply;
	}
	
	/**
	 * 
	 * @param subscriptionID
	 * @return Map
	 * @throws GIException
	 */
	@SuppressWarnings("rawtypes")
	public Map retrieveSubScriptionInfo(String subscriptionID)throws GIException 
	{
		// Required only when converting an existing credit card authorization into a customer profile.
		LOGGER.debug("PCI: received retrieveSubScriptionInfo request for Subscription Id: "+subscriptionID);
		Map<String, String> request = new HashMap<String, String>();
		request.put(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVESERVICE_RUN_KEY, "true");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, CyberSourceKeyConstants.MERCHANT_CODE);
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, subscriptionID);

		Map reply = null;
		try 
		{
			FinanceGeneralUtility.displayMap("RETRIEVE SUBSCRIPTIONINFO REQUEST:",request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("RETRIEVE SUBSCRIPTIONINFO RESPONSE:",reply);
		} 
		catch (Exception e)
		{
			LOGGER.error("PCI: retrieveSubScriptionInfo exception caught: "+ e);
			throw new GIException(e);
		}

		return reply;
	}
	
	/**
	 * 
	 * @param subscriptionID
	 * @return Map
	 * @throws GIException
	 */
	@SuppressWarnings("rawtypes")
	public Map pciDeleteSubScriptionInfo(String subscriptionID)throws GIException
	{
		LOGGER.debug("PCI: received Delete request for Subscription Id: "+subscriptionID);
		Map<String, String> request = new HashMap<String, String>();
		request.put(CyberSourceKeyConstants.PAYSUBSCRIPTIONDELETESERVICE_RUN_KEY, "true");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, CyberSourceKeyConstants.MERCHANT_CODE);
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, subscriptionID);
		Map reply = null;
		try
		{
			FinanceGeneralUtility.displayMap("PCIDELETE SUBSCRIPTIONINFO REQUEST:",request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("PCIDELETE SUBSCRIPTIONINFO RESPONSE:",reply);
		}
		catch (Exception ex)
		{
			LOGGER.error("PCI: pciDeleteSubScriptionInfo exception Caught: "+ ex);
			throw new GIException(ex);
		}
		return reply;
	}
	
	/**
	 * Only recurring and installment customer subscriptions can be cancelled.
	 * @Since 16th October 2014
	 * @param subscriptionID
	 * @return
	 * @throws GIException
	 */
	@SuppressWarnings("rawtypes")
	public Map pciCancelSubscription(String subscriptionID)throws GIException
	{
		LOGGER.debug("PCI: received Cancel Subscription request for Subscription Id: "+subscriptionID);
		Map<String, String> request = new HashMap<String, String>();
		request.put(CyberSourceKeyConstants.MERCHANTID_KEY, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MERCHANT_ID));
		request.put(CyberSourceKeyConstants.PAYSUBSCRIPTIONUPDATESERVICE_RUN_KEY, "true");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, CyberSourceKeyConstants.MERCHANT_CODE);
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, subscriptionID);
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_STATUS_KEY, CyberSourceKeyConstants.SUBSCRIPTION_STATUS_CANCEL);
		Map reply = null;
		try
		{
			FinanceGeneralUtility.displayMap("PCICANCEL SUBSCRIPTION REQUEST:", request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("PCICANCEL SUBSCRIPTIONINFO RESPONSE:", reply);
		}
		catch (Exception ex)
		{
			LOGGER.error("PCI: pciCancelSubscription exception Caught: "+ ex);
			throw new GIException(ex);
		}
		return reply;
	}
	
	@SuppressWarnings("rawtypes")
	public Map retrieveRecurringSubscription(Map payRequest, String amount)throws GIException
	{
		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;
		
		request.put(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVESERVICE_RUN_KEY, "true");
		request.put(CyberSourceKeyConstants.MERCHANTID_KEY, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MERCHANT_ID));
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, CyberSourceKeyConstants.MERCHANT_CODE);		
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, (String)payRequest.get(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY));
		request.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, amount);
		try
		{
			FinanceGeneralUtility.displayMap("RETRIEVE RECURRING SUBSCRIPTION REQUEST:",request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("RETRIEVE RECURRING SUBSCRIPTION RESPONSE:",reply);
		}
		catch(Exception ex)
		{
			LOGGER.error("PCI: eCheckDebitAccount exception caught: "+ex);
			throw new GIException(ex);
		}

		return reply;
	}
}
