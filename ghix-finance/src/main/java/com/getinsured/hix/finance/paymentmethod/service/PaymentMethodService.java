package com.getinsured.hix.finance.paymentmethod.service;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.getinsured.hix.dto.finance.PaymentMethodDTO;
import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.ModuleName;
import com.getinsured.hix.model.PaymentMethods.PaymentIsDefault;
import com.getinsured.hix.model.PaymentMethods.PaymentStatus;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Service class for PaymentMethod model related exposed services.
 * @since 23rd September 2014
 * @author Sharma_k
 *
 */
public interface PaymentMethodService 
{
	/**
	 * To retrieve data from Cybersource, functionality - Payment Tokenization
	 * @param paymentId
	 * @return
	 * @throws GIException 
	 */
	PaymentMethods getPaymentMethodByPCI(final int paymentId)throws GIException;	
	void migrateIssuerWithPci() throws GIException;
	PaymentMethods savePaymentMethods(PaymentMethods employerPaymentTypes)throws GIException;
	PaymentMethods getPaymentMethodsDetail(int paymentId);
	
	/**
	 * 
	 * @param paymentId
	 * @param isdefault
	 * @param status
	 * @param lastUpdatedBy
	 * @return
	 */
	PaymentMethods modifyPaymentstatus(int paymentId, PaymentIsDefault isdefault, PaymentStatus status, Integer lastUpdatedBy);
	
	/**
	 * 
	 * @param paymentId
	 * @param status
	 * @param lastUpdatedBy
	 * @return
	 */
	PaymentMethods updateStatus(int paymentId, PaymentStatus status, Integer lastUpdatedBy);
	
	/**
	 * 
	 * @param paymentId
	 * @param isdefault
	 * @param lastUpdatedBy
	 * @return
	 */
	PaymentMethods changeDefaultSetting(int paymentId, PaymentIsDefault isdefault, Integer lastUpdatedBy);
	List<Map<Integer, Object>> migratePaymentMethodPci(List<PaymentMethodDTO> paymentMethodDTOList) throws GIException;
	/**
	 * 
	 * @param moduleId
	 * @param moduleName
	 * @param pageable
	 * @return
	 */
	Page<PaymentMethods> getPaymentMethodByMouduleIdAndModuleNameAndPageable(int moduleId, ModuleName moduleName, Pageable pageable);
	
	/**
	 * 
	 * @param searchCriteria
	 * @return
	 */
	Map<String, Object> searchEmployerPaymentMethod(final PaymentMethodRequestDTO paymentMethodRequestDTO)throws GIException;
	List<PaymentMethods> searchByPaymentMethodName(String paymentMethodName);	
		
}
