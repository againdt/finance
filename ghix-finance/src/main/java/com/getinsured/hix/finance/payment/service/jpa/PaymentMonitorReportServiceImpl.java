package com.getinsured.hix.finance.payment.service.jpa;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.finance.employer.repository.IEmployerInvoicesRepository;
import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.finance.issuer.repository.IIssuerRemittanceRepository;
import com.getinsured.hix.finance.payment.service.PaymentEventLogService;
import com.getinsured.hix.finance.payment.service.PaymentMonitorReportService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceConstants.PaymentEventReport;
import com.getinsured.hix.model.PaymentEventLog;
import com.getinsured.hix.model.PaymentEventLog.PaymentStatus;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;

@Service("paymentMonitorReportService")
public class PaymentMonitorReportServiceImpl implements PaymentMonitorReportService{

	@SuppressWarnings("rawtypes")
	@Autowired private ObjectFactory<QueryBuilder> delegateFactory;
	@Autowired private IEmployerInvoicesRepository iEmployerInvoicesRepository;
	@Autowired private IIssuerRemittanceRepository iIssuerInvoicesRepository;
	@Autowired private PaymentEventLogService paymentEventLogService;

	private static final String END_DAY_TIME = "23:59:59";
	private static final String DOWNLOAD_TYPE     = "csv";
	private static final String PAYMENT_EVENT_REPORT_LIST = "paymentEventLogList";

	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentMonitorReportServiceImpl.class);

	@SuppressWarnings("all")
	@Override
	public Map<String, Object> getPaymentReportDetails(
			Map<String, Object> searchCriteria) throws FinanceException{

		LOGGER.info("getPaymentReportDetails(-) method : START");

		QueryBuilder<PaymentEventLog> searchQuery = delegateFactory.getObject();
		searchQuery.buildObjectQuery(PaymentEventLog.class);
		Map<String, Object> paymentEventRecords = new HashMap<String, Object> ();
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat endDateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		try {
			if(searchCriteria!=null && !searchCriteria.isEmpty()){
				String strStartDate = (String)searchCriteria.get(FinanceConstants.START_DATE);
				String strEndDate = (String)searchCriteria.get(FinanceConstants.END_DATE);
				Date startDate = (!StringUtils.isEmpty(strStartDate)) ? formatter.parse(strStartDate) : null;
				Date endDate = (!StringUtils.isEmpty(strEndDate)) ? endDateFormat.parse(strEndDate+" "+END_DAY_TIME) : null;
				String transactionStatus = (String)searchCriteria.get(FinanceConstants.TRANSACTION_STATUS);
				String requestId = (String)searchCriteria.get(PaymentEventReport.REQUEST_ID);
				String merchantId = (String)searchCriteria.get(FinanceConstants.MERCHANT_ID);
				String issuerName = (String)searchCriteria.get(FinanceConstants.CARRIER_NAME);
				String employerName = (String)searchCriteria.get(FinanceConstants.EMPLOYER_NAME);
				String invoiceNumber = (String)searchCriteria.get(FinanceConstants.INVOICE_NUMBER);
				String csvDownload = (String)searchCriteria.get(DOWNLOAD_TYPE);
				String noOfDaysPending = (String)searchCriteria.get(FinanceConstants.NO_OF_DAYS_PENDING);
				String criteria = (String) searchCriteria.get(FinanceConstants.CRITERIA);
               
				long recordCount = 0;
				List<PaymentEventLog> paymentEventLogsList = null;

				if(startDate!=null){
					searchQuery.applyWhere(FinanceConstants.CREATION_TIMESTAMP, startDate, DataType.DATE, ComparisonType.GE);
				}
				if(endDate!=null){
					searchQuery.applyWhere(FinanceConstants.CREATION_TIMESTAMP, endDate, DataType.DATE, ComparisonType.LE);
				}

				if(StringUtils.isNotEmpty(transactionStatus))
				{
					if("ACCEPTED".equalsIgnoreCase(transactionStatus))
					{
						searchQuery.applyWhere(PaymentEventReport.TXN_STATUS, "ACCEPT", DataType.STRING, ComparisonType.EQUALS_CASE_IGNORE);
					}
					else if("REJECTED".equalsIgnoreCase(transactionStatus))
					{
						searchQuery.applyWhere(PaymentEventReport.TXN_STATUS, "REJECT", DataType.STRING, ComparisonType.EQUALS_CASE_IGNORE);
					}
				}

				if(!StringUtils.isEmpty(requestId)){
					searchQuery.applyWhere(PaymentEventReport.REQUEST_ID, requestId, DataType.STRING, ComparisonType.EQUALS);
				}

				if(!StringUtils.isEmpty(merchantId)){
					searchQuery.applyWhere(PaymentEventReport.MERCHANT_REF_CODE, merchantId, DataType.STRING, ComparisonType.EQUALS);
				}

				if(!StringUtils.isEmpty(employerName) && StringUtils.isEmpty(invoiceNumber)){
					filterByEmployerName(searchQuery, employerName);
				}

				if(!StringUtils.isEmpty(issuerName) && StringUtils.isEmpty(invoiceNumber)){
					filterByIssuerName(searchQuery, issuerName);
				}

				if(!StringUtils.isEmpty(invoiceNumber)){
					filterByInvoiceNumber(searchQuery, invoiceNumber);
				}

				/*
				 * Adding new search criteria for no of days pending
				 */
				if(StringUtils.isNotEmpty(criteria) && StringUtils.isNotEmpty(noOfDaysPending))
				{
					int pendingDays = Integer.parseInt(noOfDaysPending);
					DateTime currentDateTime = TSDateTime.getInstance(); 
					Date startOfDay = null;
					Date endOfDay = null;
					searchQuery.applyWhere("paymentStatus", PaymentStatus.PENDING, DataType.ENUM, ComparisonType.EQUALS);
					if("Greater Than or Equal to".equalsIgnoreCase(criteria))
					{
						endOfDay = withEndOfDayTime(currentDateTime.minusDays(pendingDays)).toDate();
						searchQuery.applyWhere(FinanceConstants.CREATION_TIMESTAMP, endOfDay, DataType.DATE, ComparisonType.LE);
					}
					else if("Less Than or Equal to".equalsIgnoreCase(criteria))
					{
						startOfDay = currentDateTime.minusDays(pendingDays).withTimeAtStartOfDay().toDate();
						searchQuery.applyWhere(FinanceConstants.CREATION_TIMESTAMP, startOfDay, DataType.DATE, ComparisonType.GE);
					}
					else if("Equals To".equalsIgnoreCase(criteria))
					{
						startOfDay = currentDateTime.minusDays(pendingDays).withTimeAtStartOfDay().toDate();
						endOfDay = withEndOfDayTime(currentDateTime.minusDays(pendingDays)).toDate();
						searchQuery.applyWhere(FinanceConstants.CREATION_TIMESTAMP, startOfDay, DataType.DATE, ComparisonType.GE);
						searchQuery.applyWhere(FinanceConstants.CREATION_TIMESTAMP, endOfDay, DataType.DATE, ComparisonType.LE);
					}	
				}

				String sortBy = searchCriteria.get(FinanceConstants.SORT_BY).toString();
				searchQuery.applySort(sortBy, SortOrder.valueOf(searchCriteria.get(FinanceConstants.SORT_ORDER).toString()));
				searchQuery.applyWhere(PaymentEventReport.TXN_STATUS, transactionStatus, DataType.STRING, ComparisonType.ISNOTNULL);
				searchQuery.applyWhere("employerInvoices.id", 0, DataType.NUMERIC, ComparisonType.ISNOTNULL);
				if(StringUtils.isEmpty(csvDownload)){
					paymentEventLogsList = searchQuery.getRecords((Integer)searchCriteria.get("startRecord"), (Integer)searchCriteria.get("pageSize"));
					recordCount = searchQuery.getRecordCount();
					paymentEventRecords.put(PAYMENT_EVENT_REPORT_LIST, paymentEventLogsList);
				}
				else{
					paymentEventLogsList = searchQuery.getRecords(1, -1);
					if(paymentEventLogsList!=null && !paymentEventLogsList.isEmpty()){
						List<PaymentEventLog> csvPaymentEventLogList = getPaymentRecordsForCSV(paymentEventLogsList);
						recordCount = csvPaymentEventLogList.size();
						paymentEventRecords.put(PAYMENT_EVENT_REPORT_LIST, csvPaymentEventLogList);
					}
				}
				paymentEventRecords.put("recordCount", recordCount);
				LOGGER.info("getPaymentReportDetails(-) method : END");
			}
		} catch (Exception e) {
			throw new FinanceException("Error in getPaymentReportDetails(-) method ", e);
		}
		return paymentEventRecords;
	}

	private List<PaymentEventLog> getPaymentRecordsForCSV(List<PaymentEventLog> paymentEventLogsList){
		List<String> merchantRefCodeList = new ArrayList<String> ();
		List<PaymentEventLog> errorAndRejectList = new ArrayList<PaymentEventLog> ();
		List<PaymentEventLog> paymentEventRecords = null;
		for(PaymentEventLog paymentEventLog: paymentEventLogsList){
			if("ACCEPT".equalsIgnoreCase(paymentEventLog.getTxnStatus())){	
				merchantRefCodeList.add(paymentEventLog.getMerchantRefCode());
			}
			else{
				errorAndRejectList.add(paymentEventLog);
			}		
		}
		if(!merchantRefCodeList.isEmpty()){
			paymentEventRecords = paymentEventLogService.getPaymentEventLogByMerchantRefCodeList(merchantRefCodeList);
			paymentEventRecords.addAll(errorAndRejectList);
			return paymentEventRecords;
		}
		return errorAndRejectList;
	}


	private void filterByInvoiceNumber(QueryBuilder<PaymentEventLog> searchQuery, String invoiceNumber)
	{
		if(StringUtils.isNotEmpty(invoiceNumber))
		{
			Integer eInvId  = iEmployerInvoicesRepository.findEmpinvByInvNumber(invoiceNumber);
			eInvId = (eInvId != null) ? eInvId : 0;
			searchQuery.applyWhere(PaymentEventReport.EMPLOYER_INVOICES_ID, eInvId, DataType.NUMERIC, ComparisonType.EQ);
		}
	}

	/*private void filterByInvoiceNumberAnd_EmpNameOrIssName(QueryBuilder<PaymentEventLog> searchQuery, String invoiceNumber, String employerName, String issuerName)
	{
		int	empInvId = 0;
		int	issInvId = 0;
		if(StringUtils.isEmpty(employerName) && StringUtils.isEmpty(issuerName)){
			Integer eInvId  = iEmployerInvoicesRepository.findEmpinvByInvNumber(invoiceNumber);
			Integer iInvId = iIssuerInvoicesRepository.findIssuerinvByInvNumber(invoiceNumber);
			empInvId = (eInvId!=null) ? eInvId : 0;
			issInvId =  (iInvId!=null) ? iInvId : 0;

			if(empInvId!=0 && issInvId ==0){
				searchQuery.applyWhere(PaymentEventReport.EMPLOYER_INVOICES_ID, empInvId, DataType.NUMERIC, ComparisonType.EQ);
			}else if(empInvId==0 && issInvId!=0){
				searchQuery.applyWhere(PaymentEventReport.ISSUER_INVOICES_ID, issInvId, DataType.NUMERIC, ComparisonType.EQ);
			}else{
				searchQuery.applyWhere(PaymentEventReport.EMPLOYER_INVOICES_ID, empInvId, DataType.NUMERIC, ComparisonType.EQ);
				searchQuery.applyWhere(PaymentEventReport.ISSUER_INVOICES_ID, issInvId, DataType.NUMERIC, ComparisonType.EQ);
			}
		}else{
			if(!StringUtils.isEmpty(employerName)){
				Integer eInvId = iEmployerInvoicesRepository.findInvIdByEmpNameAndInvNum(employerName, invoiceNumber);
				empInvId = (eInvId!=null) ? eInvId : 0;
				searchQuery.applyWhere(PaymentEventReport.EMPLOYER_INVOICES_ID, empInvId, DataType.NUMERIC, ComparisonType.EQ);
			}else{
				Integer iInvId = iIssuerInvoicesRepository.findInvIdByIssuerNameAndInvNum(issuerName, invoiceNumber);
				issInvId =  (iInvId!=null) ? iInvId : 0;
				searchQuery.applyWhere(PaymentEventReport.ISSUER_INVOICES_ID, issInvId, DataType.NUMERIC, ComparisonType.EQ);
			}
		}
	}*/

	private void filterByEmployerName(QueryBuilder<PaymentEventLog> searchQuery, String employerName)
	{
		List<Integer> invIds = iEmployerInvoicesRepository.findEmpinvByEmpname(employerName);
		if(invIds==null || invIds.isEmpty()){
			searchQuery.applyWhere(PaymentEventReport.EMPLOYER_INVOICES_ID, 0, DataType.NUMERIC, ComparisonType.EQ);
		}else{
			searchQuery.applyWhere(PaymentEventReport.EMPLOYER_INVOICES_ID, invIds, DataType.LIST, ComparisonType.EQ);
		}
	}

	private void filterByIssuerName(QueryBuilder<PaymentEventLog> searchQuery, String issuerName)
	{
		List<Integer> invIds = iIssuerInvoicesRepository.findIssuerinvByIssuername(issuerName);
		if(invIds==null || invIds.isEmpty()){
			searchQuery.applyWhere(PaymentEventReport.ISSUER_INVOICES_ID, 0, DataType.NUMERIC, ComparisonType.EQ);
		}else{
			searchQuery.applyWhere(PaymentEventReport.ISSUER_INVOICES_ID, invIds, DataType.LIST, ComparisonType.EQ);
		}
	}

	private DateTime withEndOfDayTime(DateTime dateTime){
		return dateTime.withTime(FinanceConstants.NUMERICAL_23, FinanceConstants.NUMERICAL_59, FinanceConstants.NUMERICAL_59, FinanceConstants.NUMERICAL_999);
	}
}
