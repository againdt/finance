package com.getinsured.hix.finance.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.finance.FinanceRequest;
import com.getinsured.hix.dto.finance.FinanceResponse;
import com.getinsured.hix.finance.employer.service.AdjustmentReasonCodeService;
import com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService;
import com.getinsured.hix.finance.employer.service.EmployerInvoicePdfGenerationService;
import com.getinsured.hix.finance.employer.service.EmployerInvoicesService;
import com.getinsured.hix.finance.employer.service.EmployerPaymentInvoiceService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceErrorCodes;
import com.getinsured.hix.finance.utils.FinanceGeneralUtility;
import com.getinsured.hix.finance.utils.FinancialMgmtGenericUtil;
import com.getinsured.hix.finance.utils.FinancialMgmtUtils;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.EmployerEnrollment;
import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.EmployerInvoiceLineItems.Status;
import com.getinsured.hix.model.EmployerInvoiceLineItemsDetail;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.EmployerInvoices.InvoiceType;
import com.getinsured.hix.model.EmployerInvoices.PaidStatus;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.thoughtworks.xstream.XStream;


/**
 * This controller class is for GHIX-FINANCE module
 * @author Sharma_k
 * @since 28-Jun-2013   
 *
 */
@Controller
@RequestMapping("/finance")
public class EmployerInvoicesController 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployerInvoicesController.class);

	@Autowired private EmployerInvoicesService employerInvoicesService;
	@Autowired private EmployerPaymentInvoiceService employerPaymentInvoiceService;
	@Autowired private EmployerInvoiceLineItemsService employerInvoiceLineItemsService;
	@Autowired private FinancialMgmtUtils financialMgmtUtils;
	@Autowired private EmployerInvoicePdfGenerationService employerInvoicePdfGenerationService;
	@Autowired private AdjustmentReasonCodeService adjustmentReasonCodeService;

	/**This method is used to test service controller.
	 * @return String - Welcome Message
	 * @throws GIException
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody public String welcome()throws GIException
	{
		LOGGER.info("Welcome to GHIX-FINANCE module");

		return "Welcome to GHIX-FINANCE module";
	}


	/**
	 * Search for the employer invoices which are not paid(PAID_STATUS='DUE')
	 * @param searchCriteria  Map<String, Object>
	 * @return String - FinanceResponse object as String.
	 */
	/*@RequestMapping(value = "/notpaidemployerinvoices", method = RequestMethod.POST)
	@ResponseBody public String searchNotPaidEmployerInvoices(@RequestBody Map<String, Object> searchCriteria)
	{
		LOGGER.info("============= Start :: Inside searchNotPaidEmployerInvoices ==================");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		try {
			if(searchCriteria == null || searchCriteria.isEmpty())
			{
				financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else
			{
				LOGGER.info("Received criteria for searchNotPaidEmployerInvoices : "+searchCriteria);
				Map<String, Object> employerInvoicesMap = employerInvoicesService.searchNotPaidEmployerInvoices(searchCriteria);
				if(employerInvoicesMap == null || employerInvoicesMap.isEmpty())
				{
					financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_RESPONSE);
				}
				else
				{
					financeResponse.setEmployerInvoicesMap(employerInvoicesMap);
					financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}	
			}
		} catch (Exception e) {
			LOGGER.error("============= Exception :: Inside searchNotPaidEmployerInvoices ==================", e);
			financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.SNPEIN.getCode());
			financeResponse.setErrMsg(e.getStackTrace().toString());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		LOGGER.info("============= End :: Inside searchNotPaidEmployerInvoices ==================");
		return xstream.toXML(financeResponse);
	}*/

	/**
	 * Search for employer invoices based on the search criteria.
	 * @param searchCriteria
	 * @return String - FinanceResponse object as String.
	 */
	@RequestMapping(value = "/searchemployerinvoices", method = RequestMethod.POST)
	@ResponseBody public String searchEmployerInvoices(@RequestBody Map<String, Object> searchCriteria)
	{
		LOGGER.info("============= Start :: Inside searchEmployerInvoices ==================");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		try {
			if(searchCriteria == null || searchCriteria.isEmpty())
			{
				financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else
			{
				LOGGER.debug("Received criteria for searchEmployerInvoices "+searchCriteria);
				Map<String, Object> employerInvoicesMap = null;
				employerInvoicesMap = employerInvoicesService.searchEmployerInvoices(searchCriteria);
				if(employerInvoicesMap == null || employerInvoicesMap.isEmpty())
				{
					financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_RESPONSE);
				}
				else
				{
					financeResponse.setEmployerInvoicesMap(employerInvoicesMap);
					financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}	
			}
		} catch (Exception e) {
			LOGGER.error("============= Exception :: Inside searchEmployerInvoices ==================", e);
			financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.SEIN.getCode());
			financeResponse.setErrMsg(e.toString());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		LOGGER.info("============= End :: Inside searchEmployerInvoices ==================");
		return xstream.toXML(financeResponse);
	}

	/**
	 * Search for paid employer invoices(PAID_STATUS!='DUE') based on the search criteria.
	 * @param searchCriteria
	 * @return String - FinanceResponse object as String.
	 */
	/*@RequestMapping(value = "/searchpaidemployerinvoices", method = RequestMethod.POST)
	@ResponseBody public String searchPaidEmployerInvoices(@RequestBody Map<String, Object> searchCriteria)
	{
		LOGGER.info("================ Start :: Inside searchPaidEmployerInvoices ==================");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		try {
			if(searchCriteria == null || searchCriteria.isEmpty())
			{
				financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else
			{
				LOGGER.debug("Received criteria for searchPaidEmployerInvoices:  "+searchCriteria);
				Map<String, Object> employerInvoicesMap = employerInvoicesService.searchPaidEmployerInvoices(searchCriteria);
				if(employerInvoicesMap == null || employerInvoicesMap.isEmpty())
				{
					financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_RESPONSE);
				}
				else
				{
					financeResponse.setEmployerInvoicesMap(employerInvoicesMap);
					financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}	
			}
		} catch (Exception e) {
			LOGGER.error("================ Exception :: Inside searchPaidEmployerInvoices ==================", e);
			financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.SPEIN.getCode());
			financeResponse.setErrMsg(e.getStackTrace().toString());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		LOGGER.info("================ End :: Inside searchPaidEmployerInvoices ==================");
		return xstream.toXML(financeResponse);
	}*/

	/**
	 * Search employer invoices by invoice id.
	 * @param int - invoiceId
	 * @return String - FinanceResponse object as String.
	 */
	@RequestMapping(value = "/findemployerinvoicesbyid/{id}", method = RequestMethod.GET)
	@ResponseBody public String findEmployerInvoicesById(@PathVariable(value = "id") int invoiceId)
	{
		LOGGER.debug("=============== Start :: Inside findEmployerInvoicesById ::"+invoiceId+" ===============");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		try {
			if(invoiceId == 0)
			{
				financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else
			{
				financeResponse.setEmployerInvoices(employerInvoicesService.findEmployerInvoicesById(invoiceId));
				financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
		} catch (Exception e) {
			LOGGER.error("=============== Exception :: Inside findEmployerInvoicesById ===============", e);
			financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.SEIBYID.getCode());
			financeResponse.setErrMsg(e.toString());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		LOGGER.info("=============== End :: Inside findEmployerInvoicesById ===============");
		return xstream.toXML(financeResponse);
	}

	/**
	 * Search due employer invoices(PAID_STATUS='DUE') based FinanceRequest parameter..
	 * @param FinanceRequest
	 * @return String - FinanceResponse object as String.
	 */
	@RequestMapping(value = "/finddueemployerinvoices", method = RequestMethod.POST)
	@ResponseBody public String findDueEmployerInvoices(@RequestBody FinanceRequest financeReq)
	{
		LOGGER.info("=============== Start :: Inside findDueEmployerInvoices ===============");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		try {
			if(financeReq == null)
			{
				financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);

			}
			else if(financeReq.getIsActiveKey() == null || StringUtils.isEmpty(financeReq.getIsActiveKey()))
			{
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				financeResponse.setErrMsg(FinanceConstants.ERR_MSG_IS_ACTIVE_KEY_NULL);
			}
			else if(financeReq.getPaymentDueDate() == null)
			{
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				financeResponse.setErrMsg(FinanceConstants.ERR_MSG_PAYMENT_DUE_DATE_NULL);
			}
			else
			{
				financeResponse.setEmployerInvoicesList(employerInvoicesService.findDueEmeployerInvoices(financeReq.getIsActiveKey(), financeReq.getPaymentDueDate()));
				financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
		} catch (Exception e) {
			LOGGER.error("=============== Exception :: Inside findDueEmployerInvoices ===============", e);
			financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.SDEIN.getCode());
			financeResponse.setErrMsg(e.toString());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		LOGGER.info("=============== End :: Inside findDueEmployerInvoices ===============");
		return xstream.toXML(financeResponse);
	}

	/**
	 * Search for active employer invoices.
	 * @param searchCriteria
	 * @return String - FinanceResponse object as String.
	 * @throws GIException
	 */
	@RequestMapping(value = "/findactiveemployerinvoices", method = RequestMethod.POST)
	@ResponseBody public String findActiveEmployerInvoices(@RequestBody Map<String, Object> searchCriteria)throws GIException
	{
		LOGGER.info("============== Start :: Inside findActiveEmployerInvoices ================");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		try {
			if(searchCriteria == null || searchCriteria.isEmpty())
			{
				financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else
			{
				LOGGER.debug("Received criteria for findActiveEmployerInvoices "+searchCriteria);
				Map<String, Object> employerInvoicesMap = employerInvoicesService.searchEmpInvoicesByIsActive(searchCriteria);
				if(employerInvoicesMap == null || employerInvoicesMap.isEmpty())
				{
					financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_RESPONSE);
				}
				else
				{
					financeResponse.setEmployerInvoicesMap(employerInvoicesMap);
					financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
			}
		} catch (Exception e) {
			LOGGER.error("============== Exception :: Inside findActiveEmployerInvoices ================", e);
			financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.SAEIN.getCode());
			financeResponse.setErrMsg(e.toString());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		LOGGER.info("============== End :: Inside findActiveEmployerInvoices ================");
		return xstream.toXML(financeResponse);
	}

	/**
	 * Search employer payment invoice by payment id.
	 * @param paymentId
	 * @return String - FinanceResponse object as String.
	 * @throws GIException 
	 */
	@RequestMapping(value="/findemployerpaymentinvoicebyid/{id}", method = RequestMethod.GET)
	@ResponseBody public String findEmployerPaymentInvoiceById(@PathVariable(value="id") int paymentId)throws GIException
	{
		LOGGER.debug("================== Start :: Inside findEmployerPaymentInvoiceById "+paymentId+" ======================");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		try {
			if(paymentId == 0)
			{
				financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else
			{
				financeResponse.setEmployerPaymentInvoice(employerPaymentInvoiceService.findEmployerPaymentInvoiceById(paymentId));
				financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
		} catch (Exception e) {
			LOGGER.error("============== Exception :: Inside findEmployerPaymentInvoiceById ================", e);
			financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.SEPINBYID.getCode());
			financeResponse.setErrMsg(e.toString());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		LOGGER.info("================== End :: Inside findEmployerPaymentInvoiceById ======================");
		return xstream.toXML(financeResponse);
	}

	@RequestMapping(value= "/findPreviousActiveDueInProcessInvoice")
	@ResponseBody 
	public String findPreviousActiveDueInProcessInvoice(@RequestBody String employerId){
		LOGGER.info("=============== Start :: Inside findPreviousActiveDueInProcessInvoice ===============");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		try{
			if(employerId!=null ){
				int empId = Integer.parseInt(employerId);
				EmployerInvoices employerInvoices = employerInvoicesService.findPreviousActiveDueInProcessInvoice(empId);
				financeResponse.setEmployerInvoices(employerInvoices);
				financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
		}
		catch(Exception e){
			LOGGER.info("=============== Exception :: Inside findPreviousActiveDueInProcessInvoice ===============",e);
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			financeResponse.setErrMsg(e.toString());
			financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.SPADINV.getCode());
		}
		LOGGER.info("=============== End :: Inside findPreviousActiveDueInProcessInvoice ===============");
		return xstream.toXML(financeResponse);
	}

	/**
	 * 
	 * @param searchCriteria
	 * @return
	 * @throws GIException
	 */
	@RequestMapping(value = "/cancelemployerinvoice", method = RequestMethod.POST)
	@ResponseBody public String cancelEmployerInvoice(@RequestBody Map<String, Object> searchCriteria)throws GIException
	{
		LOGGER.info("============== Start :: Inside cancelEmployerInvoice =================");
		FinanceResponse financeResponse = new FinanceResponse();
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		try
		{
			if(searchCriteria != null && !searchCriteria.isEmpty())
			{
				if(searchCriteria.containsKey(GhixConstants.FIN_CANCEL_INV_EMPLOYER_ID_KEY) && searchCriteria.containsKey(GhixConstants.FIN_CANCEL_INV_INVOICE_TYPE_KEY))
				{
					Integer employerId = (Integer)searchCriteria.get(GhixConstants.FIN_CANCEL_INV_EMPLOYER_ID_KEY);
					String invoiceType = (String)searchCriteria.get(GhixConstants.FIN_CANCEL_INV_INVOICE_TYPE_KEY);
					if(employerId == null || employerId.equals(0))
					{
						throw new GIException("Received null or invalid employerId for invoice cancellation: "+employerId);			
					}
					if(StringUtils.isEmpty(invoiceType))
					{
						throw new GIException("Received null or invalid InvoiceType: "+invoiceType);
					}
					LOGGER.info("Received CancelEmployerInvoice Request for EmployerId: "+employerId+" InvoiceType: "+invoiceType);
					List<EmployerInvoices> employerInvoicesList = null;
					if("BINDER_INVOICE".equalsIgnoreCase(invoiceType))
					{
						employerInvoicesList = employerInvoicesService.findActiveDueInvoicesByEmpIdNInvType(employerId, InvoiceType.BINDER_INVOICE);
					}
					else if("NORMAL".equalsIgnoreCase(invoiceType))
					{
						employerInvoicesList = employerInvoicesService.findActiveDueInvoicesByEmpIdNInvType(employerId, InvoiceType.NORMAL);
					}
					else
					{
						throw new GIException("Invalid invoice type received: "+invoiceType);
					}
					if(employerInvoicesList != null && !employerInvoicesList.isEmpty())
					{
						for(EmployerInvoices employerInvoices : employerInvoicesList)
						{
							employerInvoices.setIsActive("N");
							employerInvoices.setPaidStatus(PaidStatus.CANCEL);
							employerInvoicesService.saveEmployerInvoices(employerInvoices);

							List<EmployerInvoiceLineItems> lineItems = employerInvoiceLineItemsService.getInvoiceLineItem(employerInvoices);
							for (EmployerInvoiceLineItems lineItem : lineItems) 
							{
								lineItem.setPaidStatus(EmployerInvoiceLineItems.Status.CANCEL);
								employerInvoiceLineItemsService.saveEmployerInvoiceLineItems(lineItem);
							}
							//EmployerInvoices newEmployerInvoices = employerInvoicesService.saveEmployerInvoices(employerInvoices);
							/*if(!newEmployerInvoices.getPaidStatus().equals(PaidStatus.CANCEL) ||
									!newEmployerInvoices.getIsActive().equals("N"))
							{
								throw new GIException("Unable to save employer invoices");
							}*/
						}
						financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						financeResponse.setErrCode(0);
					}
					else
					{
						LOGGER.warn("No Employer Invoice found");
						throw new GIException("No Employer Invoices found for for EmployerId: "+employerId+" InvoiceType: "+invoiceType);
					}
				}
			}
			else
			{
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				financeResponse.setErrCode(1);
				financeResponse.setErrMsg("Received null or Empty serach criteria");
			}
		}
		catch(GIException ex)
		{
			LOGGER.error("Exception caught in cancelEmployerInvoice : "+ex);
			financeResponse.setErrCode(1);
			financeResponse.setErrMsg(ex.toString());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		catch(Exception ex)
		{
			LOGGER.error("Exception caught in cancelEmployerInvoice "+ex);
			financeResponse.setErrCode(1);
			financeResponse.setErrMsg(ex.toString());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}

		LOGGER.info("========== End :: Finished cancelEmployerInvoice ===============");
		return xstream.toXML(financeResponse);
	}

	/**
	 * 
	 * @param searchCriteria
	 * @return
	 * @revision revamp for Jira ID: HIX-62617
	 */
	@RequestMapping(value = "/searchemployerpaymenthistory", method = RequestMethod.POST)
	@ResponseBody public String searchEmployerPaymentHistory(@RequestBody Map<String, Object> searchCriteria)
	{
		LOGGER.info("============== Start :: Inside searchEmployerPaymentHistory ================");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		Map<String, Object> employerEnrollmentDetailsMap = null;
		try
		{
			if(searchCriteria == null || searchCriteria.isEmpty())
			{
				financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else
			{
				Integer employerID = Integer.parseInt((String)searchCriteria.get("EMPLOYER_ID"));
				Integer employerEnrollmentID = null;
				EmployerEnrollment.Status employerEnrollmentStatus = 
						EmployerEnrollment.Status.valueOf((String) searchCriteria.get("EMPLOYER_ENROLLMENT_STATUS"));
				/*
				 * Calling Shop Rest Call to Fetch Active employer Enrollment.
				 * Parameters EmployerID, EmployerEnrollmentStatus and Current System Date as received parameter.
				 */
				try
				{
					employerEnrollmentDetailsMap = financialMgmtUtils.findEmployerEnrollmentDetails(employerID,
							employerEnrollmentStatus, new TSDate());
				}
				catch(GIException gie)
				{
					financeResponse.setErrCode(gie.getErrorCode());
					financeResponse.setErrMsg(gie.getErrorMsg());
					financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
				if(employerEnrollmentDetailsMap != null && !employerEnrollmentDetailsMap.isEmpty())
				{
					employerEnrollmentID = (Integer)employerEnrollmentDetailsMap.get(FinanceConstants.EMPLOYER_ENROLLMENT_ID_KEY);
					List<Object[]> objList = employerInvoicesService.findEmployerInvoiceForPaidInvoice(employerID, employerEnrollmentID);
					BigDecimal totalPremiumPaid = FinancialMgmtGenericUtil.getDefaultBigDecimal();
					BigDecimal averageMonthlyPremium = FinancialMgmtGenericUtil.getDefaultBigDecimal();

					if(objList != null && !objList.isEmpty())
					{
						for(Object[] invoiceData : objList)
						{
							totalPremiumPaid = FinancialMgmtGenericUtil.add(totalPremiumPaid, (BigDecimal)invoiceData[1]);
						}
						averageMonthlyPremium = FinancialMgmtGenericUtil.divide(totalPremiumPaid, FinancialMgmtGenericUtil.getBigDecimalObj(objList.size()));

						financeResponse.setTotalPremiumAmount(totalPremiumPaid);
						financeResponse.setAverageMonthlyPremium(averageMonthlyPremium);
						financeResponse.setEmployerEnrollmentID(employerEnrollmentID);
						financeResponse.setCoverageStartDate((Date)employerEnrollmentDetailsMap.get(FinanceConstants.COVERAGE_START_DATE_KEY));
						financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						financeResponse.setErrCode(FinanceConstants.SUCCESS_CODE_200);
					}
					else
					{
						financeResponse.setErrCode(FinanceConstants.ERROR_CODE_201);
						financeResponse.setErrMsg("No active paid invoice found.");
						financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					}	
				}
			}	
		}
		catch(Exception ex)
		{
			LOGGER.error("============== Exception :: Inside searchEmployerPaymentHistory ================", ex);
			financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.SAEIN.getCode());
			financeResponse.setErrMsg(ex.toString());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		LOGGER.info("============== End :: Inside searchEmployerPaymentHistory ================");
		return xstream.toXML(financeResponse);
	}

	/**
	 * Rest Response will state whether Employer enrollment Can be terminated or not.
	 * @since 17th July 2014
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService #getCountNoOfInvoiceInGracePeriod(int, int, java.util.Date)
	 * @param searchCriteria {"employerid":"?", "employerEnrollmentId":"?"}
	 * @return XML in String form for Object<FinanceResponse>
	 */
	@RequestMapping(value = "/isterminationallowed", method = RequestMethod.POST)
	@ResponseBody 
	public String isTerminationAllowed(@RequestBody Map<String, Object> searchCriteria)
	{
		LOGGER.info("================ inside isEmployerInvoiceInGracePeriod(-) ==================");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		if(searchCriteria == null || searchCriteria.isEmpty())
		{
			LOGGER.warn("Received null or empty searchCriteria in /isinvoiceingraceperiod Rest call");
			financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		else
		{
			if (searchCriteria.get(FinanceConstants.EMPLOYER_ID) == null
					|| StringUtils.isEmpty(searchCriteria.get(FinanceConstants.EMPLOYER_ID).toString()))
			{
				financeResponse.setErrMsg(FinanceConstants.EMPLOYER_ID +" :"+ FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				return xstream.toXML(financeResponse);
			}
			if(searchCriteria.get(FinanceConstants.EMPLOYER_ENROLLMENT_ID) == null
					|| StringUtils.isEmpty(searchCriteria.get(FinanceConstants.EMPLOYER_ENROLLMENT_ID).toString()))
			{
				financeResponse.setErrMsg(FinanceConstants.EMPLOYER_ENROLLMENT_ID+" :"+FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				return xstream.toXML(financeResponse);
			}
			try
			{
				LOGGER.info("Received request parameter for /isinvoiceingraceperiod REST call: "+searchCriteria);
				/*
				 * Parameter Received EmployerId, EmployerEnrollmentId and CurrentDate
				 */
				long noOfGracePeriodInvoice = employerInvoicesService
						.getCountNoOfInvoiceInGracePeriod(Integer.parseInt(
								searchCriteria.get(FinanceConstants.EMPLOYER_ID).toString()),
								Integer.parseInt(searchCriteria.get(FinanceConstants.EMPLOYER_ENROLLMENT_ID).toString()), new java.util.Date());

				if(noOfGracePeriodInvoice != 0)
				{
					/*
					 * If counts comes out to be more than 0 then Employer Invoice exists in grace period
					 * setting flag FALSE means, Enrollment can't be terminated.
					 */
					financeResponse.setTerminationAllowed(Boolean.FALSE);
					financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
				else
				{
					/*
					 * If no employer invoice exists in grace period | If no invoice available then also the Termination flag will be TRUE 
					 */
					financeResponse.setTerminationAllowed(Boolean.TRUE);
					financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
			}
			catch(Exception ex)
			{
				LOGGER.error("=============== Exception occurred :: Inside isEmployerInvoiceInGracePeriod(-) ===== Cause: "+ex);
				financeResponse.setErrCode(0);
				financeResponse.setErrMsg(ex.toString());
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		return xstream.toXML(financeResponse);
	}

	/**
	 * Search employer invoice by employer id and ecm doc id.
	 * @return String - FinanceResponse object as String.
	 * @throws GIException 
	 */
	@RequestMapping(value="/searchinvoicebyecmdocid", method = RequestMethod.POST)
	@ResponseBody 
	public String searchIncoiceByEmployerNEcmdocId(@RequestBody Map<String, Object> searchCriteria)throws GIException
	{
		LOGGER.debug("================== Start :: Inside searchIncoiceByEmployerNEcmdocId ======================");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		try {
			if(searchCriteria.get(FinanceConstants.EMPLOYER_ID) == null || StringUtils.isEmpty(searchCriteria.get(FinanceConstants.EMPLOYER_ID).toString()))
			{
				financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else if (searchCriteria.get(FinanceConstants.ECM_DOC_ID) == null || StringUtils.isEmpty(searchCriteria.get(FinanceConstants.ECM_DOC_ID).toString())) {
				financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}else{
				int employerId = (int) searchCriteria.get(FinanceConstants.EMPLOYER_ID);
				String ecmDocId = (String) searchCriteria.get(FinanceConstants.ECM_DOC_ID);
				EmployerInvoices employerInvoices = employerInvoicesService.searchEmployerInvoicesByECMIdNEmployerId(employerId, ecmDocId);
				if (employerInvoices!=null) {
					financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}else {
					financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}

			}
		} catch (Exception e) {
			LOGGER.error("============== Exception :: Inside searchIncoiceByEmployerNEcmdocId ================", e);
			financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.SEPINBYID.getCode());
			financeResponse.setErrMsg(e.toString());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		LOGGER.info("================== End :: Inside searchIncoiceByEmployerNEcmdocId ======================");
		return xstream.toXML(financeResponse);
	}

	/**
	 * @since 12th September 2014
	 * to fetch all AdjustmentReasonCode.
	 * @return
	 */
	@RequestMapping(value="/findalladjustmentreasoncode", method = RequestMethod.GET)
	@ResponseBody
	public String findAllAdjustmentReasonCode()
	{
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		try
		{
			financeResponse.setAdjustmentReasonCodeList(adjustmentReasonCodeService.findAllAdjustmentReasonCode());
			financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			financeResponse.setErrCode(FinanceConstants.SUCCESS_CODE_200);
		}
		catch(Exception ex)
		{
			LOGGER.error("Exception occurred in findAllAdjustmentReasonCode()"+ex);
			financeResponse.setErrMsg(ex != null? ex.toString(): "Exception occurred while fetching AdjustmentReasonCode");
			financeResponse.setErrCode(FinanceConstants.ERROR_CODE_201);
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return xstream.toXML(financeResponse);

	}

	/**
	 * @since 11th September 2014
	 * @param financeReq
	 * @return
	 * @throws GIException
	 */
	@RequestMapping(value="/updateinvoiceformanualadjustment", method = RequestMethod.POST)
	@ResponseBody
	public String updateInvoiceForManualAdjustment(@RequestBody FinanceRequest financeReq)
	{
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		if(financeReq != null)
		{
			EmployerInvoices employerInvoice = employerInvoicesService.findEmployerInvoicesById(financeReq.getEmployer_invoiceID());
			List<EmployerInvoiceLineItems> employerInvLineItemsList = employerInvoiceLineItemsService.getInvoiceLineItem(employerInvoice);
			if(employerInvoice != null && employerInvLineItemsList != null && !employerInvLineItemsList.isEmpty())
			{
				try
				{
					if(financeReq.getManual_adjustment_amount() != null )
					{
						BigDecimal manualAdjustmentAmt = BigDecimal.ZERO;
						for(EmployerInvoiceLineItems employerInvoiceLineItems : employerInvLineItemsList)
						{
							if(financeReq.getEmployer_Invoice_lineitemID() == employerInvoiceLineItems.getId())
							{
								employerInvoiceLineItems.setManualAdjustmentAmount(financeReq.getManual_adjustment_amount());
								employerInvoiceLineItems.setManualEmployerContribution(financeReq.getManual_employer_contribution());
								employerInvoiceLineItems.setManualEmployeeContribution(financeReq.getManual_employee_contribution());
								employerInvoiceLineItems.setNetAmount(FinancialMgmtGenericUtil.add(
										FinancialMgmtGenericUtil.add(employerInvoiceLineItems.getTotalPremium(),
												employerInvoiceLineItems.getRetroAdjustments()), financeReq.getManual_adjustment_amount()));

								employerInvoiceLineItems.setComments(financeReq.getComments());
								employerInvoiceLineItems.setLastUpdatedBy(financeReq.getAccountUser());
								employerInvoiceLineItems.setReasonCodeID(financeReq.getAdjustmentReasonCode());
							}
							manualAdjustmentAmt = FinancialMgmtGenericUtil
									.add(employerInvoiceLineItems.getManualAdjustmentAmount() != null ? employerInvoiceLineItems
											.getManualAdjustmentAmount() : BigDecimal.ZERO, manualAdjustmentAmt);
						}

						if(manualAdjustmentAmt.compareTo(BigDecimal.ZERO) == 0)
						{
							employerInvoice.setIsAdjusted(EmployerInvoices.IsManualAdjustment.N);
						}
						else
						{
							employerInvoice.setIsAdjusted(EmployerInvoices.IsManualAdjustment.Y);
						}

						employerInvoice.setManualAdjustmentAmt(manualAdjustmentAmt);
						employerInvoice
						.setTotalAmountDue(FinancialMgmtGenericUtil.add(
								FinancialMgmtGenericUtil.add(employerInvoice.getPremiumThisPeriod(),
										employerInvoice.getExchangeFees()), manualAdjustmentAmt));

						if(BigDecimal.ZERO.compareTo(employerInvoice.getTotalAmountDue()) == FinanceConstants.NUMERICAL_0)
						{
							employerInvoice.setPaidStatus(PaidStatus.PAID);
							employerInvoice.setPaidDateTime(new TSDate());
							EmployerInvoices latestInvoice = employerInvoicesService.findLastActiveInvoice(employerInvoice.getEmployer().getId());
							if(latestInvoice!=null && !(latestInvoice.getInvoiceNumber().equals(employerInvoice.getInvoiceNumber())))
							{
								employerInvoice.setIsActive(FinanceConstants.DECISION_N);
							}
							for (EmployerInvoiceLineItems employerInvoiceLineItem : employerInvLineItemsList) {
								employerInvoiceLineItem.setPaidStatus(Status.PAID);
								employerInvoiceLineItem.setPaidDate(new TSDate());
								employerInvoiceLineItem.setPaymentReceived(employerInvoiceLineItem.getNetAmount());
							}
						}
						employerInvoice.setEcmDocId(null);
						employerInvoice.setEmployerInvoiceLineItems(employerInvLineItemsList);

						try
						{
							String ecmDocID = employerInvoicePdfGenerationService
									.createPdfByEmployerInvoiceAndLineItems(
											employerInvoice, employerInvLineItemsList, Boolean.FALSE);
							employerInvoice.setEcmDocId(ecmDocID);
						}
						catch(Exception nse)
						{
							LOGGER.error("Exception caught while generating pdf in updateInvoiceForManualAdjustment(-) "+nse);
							financeResponse.setErrMsg("Exception caught while generating pdf in updateInvoiceForManualAdjustment(-) "+nse);
							financeResponse.setErrCode(FinanceConstants.ERROR_CODE_204);
							financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
							return xstream.toXML(financeResponse);
						}

						employerInvoicesService.saveEmployerInvoices(employerInvoice);
						financeResponse.setErrCode(FinanceConstants.SUCCESS_CODE_200);
						financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						financeResponse.setEmployerInvoices(employerInvoice);
					}
					else
					{
						throw new GIException("Received Null or invalid Adjustment Amount");
					}
				}
				catch(Exception ex)
				{
					LOGGER.error("Exception caught while update of EmployerInvoice in updateInvoiceForManualAdjustment(-) "+ex);
					financeResponse.setErrMsg("Exception Occurred while updating EmployerInvoice Adjustment Cause: "+ex);
					financeResponse.setErrCode(FinanceConstants.ERROR_CODE_203);
					financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					return xstream.toXML(financeResponse);
				}
			}
			else
			{
				financeResponse.setErrMsg("No Employer Invoice OR LineItems found for EmployerInvoice Id: "+financeReq.getEmployer_invoiceID());
				financeResponse.setErrCode(FinanceConstants.ERROR_CODE_202);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		else
		{
			financeResponse.setErrMsg("Received null or Empty RequestData");
			financeResponse.setErrCode(FinanceConstants.ERROR_CODE_201);
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return xstream.toXML(financeResponse);
	}

	/**
	 * It gives list of employer invoices for particular employer based on the given search criteria.
	 * @param searchCriteria, is a map and it contains keys 'employerId' as type integer, 'isActive' type as String,
	 * 'invoiceType' type as List<InvoiceType> and 'paidStatus' as List<PaidStatus>. All these key values are mandatory
	 * otherwise failure response will be generated
	 * @return List<EmployerInvoices>
	 */
	@RequestMapping(value = "/searchactiveduenormalinvoicesforemployer", method = RequestMethod.POST)
	@ResponseBody
	public String findActiveDueNormalInvoicesForEmployer(@RequestBody int employerId)
	{
		LOGGER.info("================ searchInvoicesByCriteria(-) started execution =================");
		String response = null;
		if(employerId == 0)
		{
			response = FinanceGeneralUtility.sendXtreamFailureFinanceResponseWhenEmptyRequest();
		}
		else
		{
			try
			{
				String isActive = "Y";
				List<PaidStatus> paidStatus = new ArrayList<>(FinanceConstants.NUMERICAL_1);
				paidStatus.add(PaidStatus.DUE);
				List<InvoiceType> invoiceType = new ArrayList<>(FinanceConstants.NUMERICAL_2);
				//invoiceType.add(InvoiceType.BINDER_INVOICE);
				invoiceType.add(InvoiceType.NORMAL);
				Employer employer = new Employer();
				employer.setId(employerId);
				List<EmployerInvoices> employerInvoices = employerInvoicesService.findInvoicesByEmployerAndIsActiveAndPaidStatusInAndAndInvoiceTypeIn(employer, isActive, paidStatus, invoiceType);
				FinanceResponse financeResponse = new FinanceResponse();
				financeResponse.setEmployerInvoicesList(employerInvoices);
				financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				response = FinanceGeneralUtility.sendXtreamFinanceResponse(financeResponse);
			}
			catch(Exception exception)
			{
				response = FinanceGeneralUtility.sendXtreamFailureFinanceResponseWhenExceptionEvent(exception);
			}
		}
		return response;
	}
	
	/**
	 * It gives list of employer invoices for particular employer based on the given search criteria.
	 * @param searchCriteria, is a map and it contains keys 'employerId' as type integer, 'isActive' type as String,
	 * 'invoiceType' type as List<InvoiceType> and 'paidStatus' as List<PaidStatus>. All these key values are mandatory
	 * otherwise failure response will be generated
	 * @return List<EmployerInvoices>
	 */
	@RequestMapping(value = "/searchactivedueinvoicesforemployer", method = RequestMethod.POST)
	@ResponseBody
	public String findActiveDueAllInvoicesForEmployer(@RequestBody int employerId)
	{
		LOGGER.info("================ searchInvoicesByCriteria(-) started execution =================");
		String response = null;
		if(employerId == 0)
		{
			response = FinanceGeneralUtility.sendXtreamFailureFinanceResponseWhenEmptyRequest();
		}
		else
		{
			try
			{
				String isActive = "Y";
				List<PaidStatus> paidStatus = new ArrayList<>(FinanceConstants.NUMERICAL_1);
				paidStatus.add(PaidStatus.DUE);
				List<InvoiceType> invoiceType = new ArrayList<>(FinanceConstants.NUMERICAL_2);
				invoiceType.add(InvoiceType.BINDER_INVOICE);
				invoiceType.add(InvoiceType.NORMAL);
				Employer employer = new Employer();
				employer.setId(employerId);
				List<EmployerInvoices> employerInvoices = employerInvoicesService.findInvoicesByEmployerAndIsActiveAndPaidStatusInAndAndInvoiceTypeIn(employer, isActive, paidStatus, invoiceType);
				FinanceResponse financeResponse = new FinanceResponse();
				financeResponse.setEmployerInvoicesList(employerInvoices);
				financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				response = FinanceGeneralUtility.sendXtreamFinanceResponse(financeResponse);
			}
			catch(Exception exception)
			{
				response = FinanceGeneralUtility.sendXtreamFailureFinanceResponseWhenExceptionEvent(exception);
			}
		}
		return response;
	}


	@RequestMapping(value = "/invoicelineitemsdtobyinvoiceid", method = RequestMethod.POST)
	@ResponseBody
	public String findEmployerInvoiceLineItemsByInvoice(@RequestBody Map<String, Object> inputMap)
	{
		String response = null;
		if(FinanceGeneralUtility.isEmptyMap(inputMap))
		{
			response = FinanceGeneralUtility.sendXtreamFailureFinanceResponseWhenEmptyRequest();
		}
		else
		{
			try
			{
				Integer pageNumber = (Integer) inputMap.get("page");
				Integer pageSize = (Integer) inputMap.get("pageSize");
				Integer invoiceId = (Integer) inputMap.get("invoiceId");
				Pageable pageable = constructPageableObject(pageSize, pageNumber, "", "");
				EmployerInvoices employerInvoices = employerInvoicesService.findEmployerInvoicesById(invoiceId);
				Page<EmployerInvoiceLineItems> employerInvoiceLineItems = employerInvoiceLineItemsService.findPageEmployerInvoiceLineItemsByInvoice(employerInvoices, pageable);
				List<EmployerInvoiceLineItems> lineItems = employerInvoiceLineItems.getContent();
				List<EmployerInvoiceLineItemsDetail> lineItemDetails = new ArrayList<>(lineItems.size());
				if(!CollectionUtils.isEmpty(lineItems))
				{
					for(EmployerInvoiceLineItems lineItem : employerInvoiceLineItems)
					{
						EmployerInvoiceLineItemsDetail lineItemDetail = new EmployerInvoiceLineItemsDetail();
						lineItemDetail.setId(lineItem.getId());
						lineItemDetail.setEmployeeId(lineItem.getEmployee().getId());
						lineItemDetail.setPolicyNumber(lineItem.getPolicyNumber());
						lineItemDetail.setName(lineItem.getEmployee().getName());
						lineItemDetail.setCarrierName(lineItem.getCarrierName());
						lineItemDetail.setPlanType(lineItem.getPlayType());
						lineItemDetail.setPersonsCovered(lineItem.getPeriodCovered());
						lineItemDetail.setTotalPremium(lineItem.getTotalPremium());
						lineItemDetail.setRetroAdjustments(lineItem.getRetroAdjustments());
						lineItemDetail.setNetAmount(lineItem.getNetAmount());
						lineItemDetail.setManualAdjAmount(lineItem.getManualAdjustmentAmount());
						lineItemDetail.setManualEmployeeContriAdj(lineItem.getManualEmployeeContribution());
						lineItemDetail.setManualEmployerContriAdj(lineItem.getManualEmployerContribution());
						lineItemDetail.setComments(lineItem.getComments());
						lineItemDetail.setReasonCode(lineItem.getReasonCodeID());
						lineItemDetail.setInvoiceTotalAmountDue(employerInvoices.getTotalAmountDue());
						lineItemDetails.add(lineItemDetail);
					}
				}
				FinanceResponse financeResponse = new FinanceResponse();
				Map<String, Object> dataObject = new HashMap<>(FinanceConstants.NUMERICAL_2);
				dataObject.put("totalCount", employerInvoiceLineItems.getTotalElements());
				dataObject.put("employerInvoiceLineDetails", lineItemDetails);
				financeResponse.setDataObject(dataObject);
				financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				response = FinanceGeneralUtility.sendXtreamFinanceResponse(financeResponse);
			}
			catch(Exception exception)
			{
				response = FinanceGeneralUtility.sendXtreamFailureFinanceResponseWhenExceptionEvent(exception);
			}
		}
		return response;
	}

	private Pageable constructPageableObject(int pageSize, int pageNumber, String sortBy, String sortOrder){

		Sort sortObject = null;
		Pageable pageable = null;
		if(!StringUtils.isBlank(sortBy)){
			Sort.Direction direction = Sort.Direction.ASC;
			if("DESC".equalsIgnoreCase(sortOrder)){
				direction = Sort.Direction.DESC;
			}
			sortObject = new Sort(new Sort.Order(direction, sortBy));
		}
		if(sortObject!=null){
			pageable = new PageRequest(pageNumber-1, pageSize, sortObject);
		}else{
			pageable = new PageRequest(pageNumber-1, pageSize);
		}	
		return pageable;
	}
}
