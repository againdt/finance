package com.getinsured.hix.finance.cybersource.service;

import java.util.Map;

import com.getinsured.hix.platform.util.exception.GIException;

@SuppressWarnings("rawtypes")
public interface PaymentProcessor 
{

	/**
	 * 
	 * @param pCust
	 * @param amount
	 * @return
	 */
	Map authorize(Map pCust, String amount);

	/**
	 * 
	 * @param pCust
	 * @param amount
	 * @return
	 */
	Map credit(Map pCust, String amount);

	/**
	 * 
	 * @param pCust
	 * @param amount
	 * @return
	 */
	Map debit(Map pCust, String amount);

	/**
	 * 
	 * @param pCust
	 * @param amount
	 * @return
	 * @throws GIException
	 */
	Map pciAuthorize(Map pCust, String amount) throws GIException;

	/**
	 * 
	 * @param pCust
	 * @param amount
	 * @return
	 * @throws GIException
	 */
	Map pciCredit(Map pCust, String amount) throws GIException;

	/**
	 * 
	 * @param pCust
	 * @param amount
	 * @return
	 * @throws GIException
	 */
	Map pciDebit(Map pCust, String amount) throws GIException;
}
