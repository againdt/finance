package com.getinsured.hix.finance.employer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.EmployerPaymentInvoice;

/**
 */
public interface IEmployerPaymentInvoiceRepository extends JpaRepository<EmployerPaymentInvoice, Integer>{

	/**
	 * Method findPaymentInvoiceByEmployerInvoices.
	 * @param employerInvoices EmployerInvoices
	 * @return List<EmployerPaymentInvoice>
	 */
	List<EmployerPaymentInvoice> findPaymentInvoiceByEmployerInvoices(EmployerInvoices employerInvoices);


	@Query("FROM EmployerPaymentInvoice empPymtInv WHERE empPymtInv.isActive= 'Y' AND employerInvoices.id = :employerInvoiceId")
	EmployerPaymentInvoice findActiveEmployerPaymentInvoice(@Param("employerInvoiceId")int employerInvoiceId);
}
