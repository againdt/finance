package com.getinsured.hix.finance.employer.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.EmployerInvoiceLineItems.IsActiveEnrollment;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.EmployerInvoices.InvoiceType;
import com.getinsured.hix.model.EmployerInvoices.IsManualAdjustment;
import com.getinsured.hix.model.EmployerInvoices.PaidStatus;


/**
 */
public interface IEmployerInvoicesRepository extends JpaRepository<EmployerInvoices, Integer>{


	/**
	 * Method findEmployerInvoicesByStatementDateAndPeriodCoveredAndEmployer.
	 * @param statementDate Date
	 * @param periodCovered String
	 * @param employerid int
	 * @return List<EmployerInvoices>
	 */
	@Query("FROM EmployerInvoices as an "+
			" where to_timestamp(an.statementDate, 'dd-mm-yyyy') = to_timestamp(:statementDate, 'dd-mm-yyyy') "+
			" and an.periodCovered = :periodCovered "+
			" and an.employer.id = :employerid ")
	List<EmployerInvoices> findEmployerInvoicesByStatementDateAndPeriodCoveredAndEmployer(@Param("statementDate") Date statementDate,@Param("periodCovered") String periodCovered,@Param("employerid") int employerid);

	/**
	 * Method findByEcmDocIdIsNull.
	 * @return List<EmployerInvoices>
	 */
	List<EmployerInvoices> findByEcmDocIdIsNull();

	/**
	 * Method findEmployerInvoicesByEmployerAndInvoiceType.
	 * @param employer Employer
	 * @param invoicetype EmployerInvoices.InvoiceType
	 * @return EmployerInvoices
	 */
	EmployerInvoices findEmployerInvoicesByEmployerAndInvoiceTypeAndIsActive(Employer employer, EmployerInvoices.InvoiceType invoicetype, String isActive);

	/**
	 * Method findLastPaidInvoice.
	 * @param employerid int
	 * @return EmployerInvoices
	 */
	/*@Query("FROM EmployerInvoices a where a.employer.id = :employerid and " +
			" 2=(select count (*) from EmployerInvoices b where a.statementDate <= b.statementDate and b.employer.id = :employerid) ")*/

	//this query works for single payment, in case of multiple payment of employer invoice
	//this query might have to be changed
	@Query("FROM EmployerInvoices a where a.statementDate = " +
			" (select MAX (statementDate) from EmployerInvoices b where (b.paidStatus='PAID' or b.paidStatus='PARTIALLY_PAID') and b.employer.id = :employerid) "+
			" and  (a.paidStatus='PAID' or a.paidStatus='PARTIALLY_PAID') and a.employer.id = :employerid ")
	EmployerInvoices findLastPaidInvoice(@Param("employerid") int employerid);

	/**
	 * Method findLastActiveInvoice.
	 * @param employerid int
	 * @return EmployerInvoices
	 */
	/*@Query("FROM EmployerInvoices a where a.statementDate = " +
				" (select MAX (statementDate) from EmployerInvoices b where b.employer.id = :employerid) ")*/

	@Query("FROM EmployerInvoices a where a.statementDate = (SELECT MAX(b.statementDate) FROM EmployerInvoices b WHERE b.isActive = 'Y' AND b.employer.id = :employerid) " +
			"AND a.isActive = 'Y' AND  a.employer.id = :employerid ")
	EmployerInvoices findLastActiveInvoice(@Param("employerid") int employerid);


	/**
	 * Method findLastInvoiceForReissue.
	 * @param employerid int
	 * @param invoiceid int
	 * @return EmployerInvoices
	 */
	/*@Query("FROM EmployerInvoices a where a.employer.id = :employerid and " +
				" 1=(select count (*) from EmployerInvoices b where b.employer.id = :employerid "+
				" and a.statementDate <= b.statementDate and b.statementDate < "+
	      "(select statementDate from EmployerInvoices c where id=:invoiceid))")*/

	@Query("FROM EmployerInvoices a where a.statementDate = " +
			" (select MAX (statementDate) from EmployerInvoices b where (b.reissueId is null or b.reissueId=0) and b.employer.id = :employerid and b.id<>:invoiceid) "+
			" and  (a.reissueId is null or a.reissueId=0) and a.employer.id = :employerid and a.id<>:invoiceid ")
	EmployerInvoices findLastInvoiceForReissue(@Param("employerid") int employerid,@Param("invoiceid") int invoiceid);

	/**
	 * Method findDueEmployerInvoices.
	 * @param isActive String
	 * @param paymentDueDate Date
	 * @return List<EmployerInvoices>
	 */
	@Query("FROM EmployerInvoices inv WHERE inv.paidStatus = 'DUE' AND inv.isActive = :isActive AND to_timestamp(inv.paymentDueDate, 'dd-mm-yyyy')  = to_timestamp(:paymentDueDate, 'dd-mm-yyyy') ")
	List<EmployerInvoices> findDueEmployerInvoices(@Param("isActive") String isActive, @Param("paymentDueDate") Date paymentDueDate);


	/**
	 * Method findByInvoiceTypeAndEmployer.
	 * @param invoicetype EmployerInvoices.InvoiceType
	 * @param employer Employer
	 * @return EmployerInvoices
	 */
	EmployerInvoices findByInvoiceTypeAndEmployer(EmployerInvoices.InvoiceType invoicetype, Employer employer);

	/**
	 * Method findDisEnrollEmployers.
	 * @param paymentGracePeriod double
	 * @return List<EmployerInvoices>
	 */
	@Query("FROM EmployerInvoices e where e.paidStatus = 'DUE' AND e.isActive = :isActive and e.invoiceType = :invoiceType and :currentDate > e.paymentDueDate ")
	List<EmployerInvoices> findDisEnrollEmployers(@Param("isActive") String isActive, @Param("invoiceType") EmployerInvoices.InvoiceType invoiceType,@Param("currentDate") Date currentDate);


	/**
	 * Method findBinderDueEmployerInvoices.
	 * @param isActive String
	 * @param invoiceType EmployerInvoices.InvoiceType
	 * @param paymentDueDate Date
	 * @return List<EmployerInvoices>
	 */
	@Query("FROM EmployerInvoices invoice WHERE invoice.paidStatus = 'DUE' AND invoice.isActive = :isActive AND invoice.invoiceType = :invoiceType AND :currentDate > invoice.paymentDueDate ")
	List<EmployerInvoices> findDueEmployerInvoices(@Param("isActive") String isActive, @Param("invoiceType") EmployerInvoices.InvoiceType invoiceType, @Param("currentDate") Date currentDate);

	@Query("FROM EmployerInvoices invoice WHERE invoice.periodCovered = :periodcovered AND invoice.employer.id = :employerid ")
	EmployerInvoices findGivenPeriodCoveredEmployerInvoice(@Param("periodcovered") String periodcovered,@Param("employerid") int employerid);


	/**
	 * Method findBinderDueEmployerInvoices.
	 * @param isActive String
	 * @param invoiceType EmployerInvoices.InvoiceType
	 * @param paymentDueDate Date
	 * @return List<EmployerInvoices>
	 */
	@Query("FROM EmployerInvoices inv where inv.id in (select invoice.id FROM EmployerInvoices invoice,Employer employer " +
			"WHERE employer.id=invoice.employer.id AND " +
			"invoice.paidStatus in ('DUE','PARTIALLY_PAID') AND invoice.isActive = :isActive AND invoice.invoiceType = :invoiceType " +
			"AND invoice.paymentDueDate <= :paymentDueDate " +
			"AND employer.autoPay='Y') ")
	List<EmployerInvoices> findDueEmployerInvoicesForAutoPay(@Param("isActive") String isActive, @Param("invoiceType") EmployerInvoices.InvoiceType invoiceType, @Param("paymentDueDate") Date paymentDueDate);

	@Query("select inv.id from EmployerInvoices inv where (LOWER (inv.employer.name) LIKE LOWER(:employerName) || '%' ) ")
	List<Integer> findEmpinvByEmpname(@Param("employerName") String employerName);

	@Query("select id from EmployerInvoices  where invoiceNumber = UPPER(:invoiceNumber) ")
	Integer findEmpinvByInvNumber(@Param("invoiceNumber") String invoiceNumber);

	@Query("select inv.id from EmployerInvoices inv  where inv.invoiceNumber = UPPER(:invoiceNumber) AND (LOWER (inv.employer.name) LIKE LOWER(:employerName) || '%' ) ")
	Integer findInvIdByEmpNameAndInvNum(@Param("employerName") String employerName, @Param("invoiceNumber") String invoiceNumber);

	@Query("FROM EmployerInvoices a where a.statementDate = (SELECT MIN(b.statementDate) FROM EmployerInvoices b WHERE b.isActive = 'Y' AND b.employer.id = :employerid AND b.paidStatus IN ('DUE', 'PARTIALLY_PAID','IN_PROCESS')) " +
			"AND a.isActive = 'Y' AND  a.employer.id = :employerid AND a.paidStatus IN ('DUE', 'PARTIALLY_PAID','IN_PROCESS') ")
	EmployerInvoices findPreviousActiveDueInProcessInvoice(@Param("employerid") int employerid);

	List<EmployerInvoices> findByEmployerAndIsActive(Employer employer, String isActive);

	/**
	 * Method findMultipleActiveEmployerInvoices.	 
	 * @param isActive String 
	 * @return List<Object[]>
	 */
	@Query(" select  an.id , an.ecmDocId , an.invoiceNumber , "+
			" an.employer.name , an.employer.id , an.premiumThisPeriod , "+
			" an.paymentDueDate , an.paidStatus , an.totalAmountDue , "+
			" an.totalPaymentReceived , an.isActive "+
			" FROM EmployerInvoices as an,  Employer as e "+
			" where an.employer.id = e.id "+
			" and (an.isAdjusted = 'N' or an.isAdjusted is null) "+
			" and an.isActive = :isActive and an.paidStatus='DUE' "+
			" and an.statementDate = (select max(einv.statementDate) from EmployerInvoices as einv where einv.employer.id=an.employer.id and einv.isActive=:isActive) " +												
			" order by an.id desc")					 
	List<Object[]> findMultipleActiveDueEmployerInvoices(@Param("isActive") String isActive);


	/**
	 * Method findMultipleActiveEmployerInvoicesDate.
	 * @param fromDate Date
	 * @param toDate Date
	 * @param isActive String 
	 * @return List<Object[]>
	 */
	@Query(" select  an.id , an.ecmDocId , an.invoiceNumber , "+
			" an.employer.name , an.employer.id , an.premiumThisPeriod , "+
			" an.paymentDueDate , an.paidStatus , an.totalAmountDue , "+
			" an.totalPaymentReceived , an.isActive "+
			" FROM EmployerInvoices as an,  Employer as e "+
			" where an.employer.id = e.id "+
			" and (an.isAdjusted = 'N' or an.isAdjusted is null) "+
			" and an.isActive = :isActive and an.paidStatus='DUE' "+
			" and an.statementDate = (select max(einv.statementDate) from EmployerInvoices as einv where einv.employer.id=an.employer.id and  einv.isActive=:isActive)" +
			" and an.statementDate >= :fromDate "+
			" and an.statementDate <= :toDate "+						
			" order by an.id desc")					 
	List<Object[]> findMultipleActiveDueEmployerInvoicesDate(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("isActive") String isActive);

	/**
	 * 
	 * @param paidStatus
	 * @param invoicePaidDate
	 * @return
	 */
	@Query("FROM EmployerInvoices as empInv WHERE empInv.paidStatus IN(:paidStatus) AND empInv.paidDateTime <= :reconcilationDate ")
	List<EmployerInvoices> findInProcessInvoiceForReconcilation(@Param("paidStatus") EmployerInvoices.PaidStatus paidStatus, @Param("reconcilationDate") Date reconcilationDate);

	/**
	 * 
	 * @param employerId
	 * @return
	 */
	@Query("FROM EmployerInvoices AS empInv WHERE empInv.employer.id = :employerId AND empInv.invoiceType = :invoiceType " + 
	 " AND empInv.paidStatus IN('DUE') AND empInv.isActive = 'Y' ")
	List<EmployerInvoices> findActiveDueInvoicesByEmpIdNInvType(@Param("employerId") int employerId, @Param("invoiceType")EmployerInvoices.InvoiceType invoiceType);

	@Query("FROM EmployerInvoices b WHERE TRUNC(b.statementDate) = "+
			"(SELECT MAX(TRUNC(c.statementDate)) FROM EmployerInvoices c WHERE c.employer.id = :employerId AND c.reissueId = 0 AND c.statementDate < :statementDate) "+
			"AND b.employer.id = :employerId AND b.reissueId = 0")
	EmployerInvoices findPreviousInvoice(@Param("employerId") int employerId, @Param("statementDate") Date statementDate);

	/**
	 * 
	 * @param employerId
	 * @param invoicetype
	 * @param isActive
	 * @param periodCovered
	 * @return
	 */
	@Query("FROM EmployerInvoices AS empInv WHERE empInv.employer.id = :employerId "
			+ " AND empInv.invoiceType = :invoiceType "
			+ " AND empInv.isActive = :isActive AND empInv.periodCovered = :periodCovered ")
	EmployerInvoices findInvoiceByTypeAndPeriodCovered(@Param("employerId")int employerId,
			@Param("invoiceType")InvoiceType invoicetype, @Param("isActive")String isActive, @Param("periodCovered")String periodCovered);

	/**
	 * @param employerId
	 * @param isActiveEnrollment
	 * @param employerEnrollmentId
	 * @param isActive
	 * @return List<EmployerInvoices>
	 */
	@Query("FROM EmployerInvoices AS empInv WHERE empInv.id IN "
			+ "(SELECT empInvl.employerInvoices.id FROM EmployerInvoiceLineItems AS empInvl WHERE empInvl.employer.id = :employerId "
			+ "AND empInvl.isActiveEnrollment= :isActiveEnrollment AND empInvl.employerEnrollmentId = :employerEnrollmentId) "
			+ "AND empInv.isActive = :isActive AND empInv.employer.id = :employerId")
	List<EmployerInvoices> findInvoiceByEmployerIdNIsAcitveNIsActiveEmpEnrollId(@Param("employerId")int employerId,
			@Param("isActiveEnrollment")IsActiveEnrollment isActiveEnrollment, @Param("isActive")String isActive, @Param("employerEnrollmentId")int employerEnrollmentId);

	/**
	 * 
	 * @param enrollmentIDList
	 * @return
	 */
	@Query("SELECT empInv.id, empInv.totalPaymentReceived FROM EmployerInvoices AS empInv WHERE empInv.employer.id = :employerId AND empInv.paidStatus in ('PAID', 'PARTIALLY_PAID') "
			+ " AND empInv.id in (SELECT empLin.employerInvoices.id FROM EmployerInvoiceLineItems AS empLin WHERE empLin.employerEnrollmentId = :employerEnrollmentId) ")
	List<Object[]> findEmployerInvoiceForPaidInvoice(@Param("employerId")int employerId, @Param("employerEnrollmentId") int employerEnrollmentId);

	/**
	 * Method findPeriodCoveredAndInvoiceCreationDate.
	 * @param employerEnrollmentId int
	 * @return
	 */
	@Query("select distinct einv.id, einv.periodCovered, einv.createdOn FROM EmployerInvoices einv,EmployerInvoiceLineItems einvl where einv.id=einvl.employerInvoices.id and einv.statementDate= "+
			"(select MAX(inv.statementDate) from EmployerInvoices inv,EmployerInvoiceLineItems invl "+
			"where inv.id=invl.employerInvoices.id and (inv.paidStatus='PAID' or inv.paidStatus='PARTIALLY_PAID') and "+
			"invl.employerEnrollmentId= :employerEnrollmentId and invl.isActiveEnrollment = 'Y') and "+
			"(einv.paidStatus='PAID' or einv.paidStatus='PARTIALLY_PAID') and einvl.employerEnrollmentId= :employerEnrollmentId and einvl.isActiveEnrollment = 'Y' ")
	Object findPeriodCoveredAndInvoiceCreationDate(@Param("employerEnrollmentId") int employerEnrollmentId);


	/**
	 * To fetch the no. of Employer invoice which are in GracePeriod
	 * @param employerId EmployerID
	 * @param employerEnrollmentId EmployerEnrollmentID 
	 * @param currentDate Object(java.util.Date) current system date. 
	 * @return long {count of employer invoice in grace period exists}
	 */
	@Query("SELECT COUNT(*) FROM EmployerInvoices empInv WHERE empInv.id in (SELECT empInvLin.employerInvoices.id FROM EmployerInvoiceLineItems empInvLin "
			+ " WHERE empInvLin.employer.id = :employerId AND empInvLin.employerEnrollmentId= :employerEnrollmentId "
			+ " AND empInvLin.paidStatus IN('DUE', 'IN_PROCESS')"
			+ " AND empInvLin.isActiveEnrollment = 'Y') "
			+ " AND empInv.paymentDueDate <= :currentDate "
			+ " AND empInv.isActive = 'Y'")
	long countGracePeriodInvoice(@Param("employerId") int employerId, @Param("employerEnrollmentId") int employerEnrollmentId, @Param("currentDate") Date currentDate);

	/**
	 * returns list of active negative(-ve) DUE EmployerInvoices
	 * @param isActive
	 * @param paidStatus
	 * @param totalAmountDue
	 * @return
	 */
	List<EmployerInvoices> findByIsActiveAndPaidStatusAndTotalAmountDueLessThan(String isActive, PaidStatus paidStatus, BigDecimal totalAmountDue);

	/**
	 * returns list of active DUE/IN_PROCESS invoices except 'invoiceNumber' invoice and which are less than current invoiceNumber's statementDate
	 * @param employer
	 * @param isActive
	 * @param invoiceNumber
	 * @param paidStatus
	 * @param statementDate
	 * @return
	 */
	List<EmployerInvoices> findByEmployerAndIsActiveAndInvoiceNumberNotAndPaidStatusInAndStatementDateBefore(Employer employer, String isActive, String invoiceNumber, List<PaidStatus> paidStatus, Date statementDate);

	/**
	 * Method searchEmployerInvoicesByECMIdNEmployerId.
	 * @param employerId
	 * @param ecmDocId 
	 * @return EmployerInvoices
	 */
	@Query("FROM EmployerInvoices a where a.employer.id = :employerId and  a.ecmDocId = :ecmDocId ")
	EmployerInvoices searchEmployerInvoicesByECMIdNEmployerId(@Param("employerId")int employerId, @Param("ecmDocId")String ecmDocId);

	/**
	 * 
	 * @param employerID
	 * @param employerEnrollmentID
	 * @return
	 */
	@Query("SELECT COUNT(DISTINCT emp.id) FROM EmployerInvoices emp, EmployerInvoiceLineItems empLin "
			+ " WHERE emp.id = empLin.employerInvoices.id AND emp.invoiceType = 'BINDER_INVOICE' "
			+ " AND empLin.employerEnrollmentId = :employerEnrollmentID AND empLin.employer.id = :employerID")
	long countNoOfBinderInvoiceForEmployerEnrollment(@Param("employerID") int employerID, @Param("employerEnrollmentID") int employerEnrollmentID);


	/**
	 * 
	 * @param id
	 * @param paidStatus
	 * @param isActive
	 * @param isAdjusted
	 * @return
	 */

	@Query("from EmployerInvoices where id = :id and paidStatus = :paidStatus and isActive = :isActive and (isAdjusted = :isAdjusted or isAdjusted is null)")
	EmployerInvoices findEmployerInvoiceByIdAndPaidStatusAndIsActiveAndIsAdjusted(@Param("id") int id, @Param("paidStatus") PaidStatus paidStatus, @Param("isActive") String isActive, @Param("isAdjusted") IsManualAdjustment isAdjusted);

	/**
	 * 
	 * @param employer
	 * @param isActive
	 * @param paidStatus
	 * @param invoiceType
	 * @return
	 */
	List<EmployerInvoices> findByEmployerAndIsActiveAndPaidStatusInAndInvoiceTypeIn(Employer employer, String isActive, List<PaidStatus> paidStatus, List<InvoiceType> invoiceType);

	//	List<EmployerInvoices> findByIsActiveAndPaidStatusAndInvoiceTypeAndPaymentDueDate(String isActive, PaidStatus paidStatus, InvoiceType invoiceType, Date paymentDueDate);

	@Query("from EmployerInvoices where isActive= :isActive and paidStatus= :paidStatus and invoiceType= :invoiceType and to_timestamp(paymentDueDate, 'dd-mm-yyyy')  = to_timestamp(:paymentDueDate, 'dd-mm-yyyy')  ")
	List<EmployerInvoices> findByIsActiveAndPaidStatusAndInvoiceTypeAndPaymentDueDate(@Param("isActive") String isActive, @Param("paidStatus") PaidStatus paidStatus, @Param("invoiceType") InvoiceType invoiceType, @Param("paymentDueDate") Date paymentDueDate);

	@Query("FROM EmployerInvoices inv where inv.id in (select invoice.id FROM EmployerInvoices invoice,Employer employer " +
			"WHERE employer.id=invoice.employer.id AND " +
			"invoice.paidStatus in ('IN_PROCESS') AND invoice.isActive = :isActive AND invoice.invoiceType = :invoiceType " +
			"AND to_timestamp(invoice.paidDateTime, 'dd-mm-yyyy') <= to_timestamp(:autoPayDate, 'dd-mm-yyyy') " +
			"AND employer.autoPay='Y') ")
	List<EmployerInvoices> findInprocessInvoicesWhenAutoPayHasDone(@Param("isActive") String isActive, @Param("invoiceType") InvoiceType invoiceType, @Param("autoPayDate") Date autoPayDate);

	@Query("FROM EmployerInvoices inv where inv.id in (select invoice.id FROM EmployerInvoices invoice,Employer employer " +
			"WHERE employer.id=invoice.employer.id AND " +
			"invoice.paidStatus in ('DUE','PARTIALLY_PAID') AND invoice.isActive = :isActive AND invoice.invoiceType = :invoiceType " +
			"AND invoice.paymentDueDate <= :paymentDueDate AND (invoice.totalAmountDue-invoice.totalPaymentReceived) > :zero " +
			"AND employer.autoPay='Y') ")
	List<EmployerInvoices> findPositiveDueEmployerInvoicesForAutoPayNotice(@Param("isActive") String isActive, @Param("invoiceType") InvoiceType invoiceType, @Param("paymentDueDate") Date paymentDueDate, @Param("zero") BigDecimal zero);

	@Query("FROM EmployerInvoices inv where inv.invoiceType = :invoiceType AND inv.ecmDocId is null ")
	List<EmployerInvoices> findByInvoiceTypeAndEcmDocIdIsNull(@Param("invoiceType") InvoiceType invoiceType);
	
	@Query("FROM EmployerInvoices inv WHERE inv.paidStatus = 'CANCEL' AND to_timestamp(inv.paymentDueDate, 'dd-mm-yyyy') = to_timestamp(:paymentDueDate, 'dd-mm-yyyy') " +
			" AND inv.terminationReason = :terminatedReason AND to_timestamp(inv.terminationDate, 'dd-mm-yyyy') = to_timestamp(:currentDate, 'dd-mm-yyyy') " +
			" AND inv.invoiceType = :invoiceType ")
	List<EmployerInvoices> findCancelledInvoiceForTermiantedReason(@Param("terminatedReason") String terminatedReason,
			@Param("invoiceType") EmployerInvoices.InvoiceType invoiceType,
			@Param("paymentDueDate") Date paymentDueDate, @Param("currentDate") Date currentDate);

	@Query("FROM EmployerInvoices invoice WHERE invoice.paidStatus = 'DUE' AND invoice.isActive = :isActive AND invoice.invoiceType = :invoiceType AND to_timestamp(invoice.paymentDueDate, 'dd-mm-yyyy') = to_timestamp(:paymentDueDate, 'dd-mm-yyyy') ")
	List<EmployerInvoices> findDueEmployerInvoicesForNotification(@Param("isActive") String isActive, @Param("invoiceType") EmployerInvoices.InvoiceType invoiceType, @Param("paymentDueDate") Date paymentDueDate);

	@Query("FROM EmployerInvoices invoice WHERE invoice.paidStatus = 'PAID' AND invoice.invoiceType = :invoiceType AND invoice.paidDateTime > invoice.paymentDueDate " +
			"AND to_timestamp(invoice.settlementDateTime, 'dd-mm-yyyy') = to_timestamp(:settlementDate, 'dd-mm-yyyy') ")
	List<EmployerInvoices> findEmployerInvoicesBasedOnPaymentDateAndSettlementDate(@Param("invoiceType") EmployerInvoices.InvoiceType invoiceType, @Param("settlementDate") Date settlementDate);
	
	@Modifying
	@Transactional(readOnly=false) 
	@Query("update EmployerInvoices e set e.isActive = 'N', e.paidStatus = 'CANCEL', e.terminationReason= :cancelReason, e.terminationDate= :terminationDate, "
			+ "  e.lastUpdatedBy = :userId where e.id = :invoiceId ")
	int updateCancelStatus(@Param("cancelReason") String cancelReason,@Param("terminationDate") Date terminationDate,@Param("userId") int userId,@Param("invoiceId") int invoiceId);

}
