package com.getinsured.hix.finance.paymentmethod.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.ModuleName;
import com.getinsured.hix.model.PaymentMethods.PaymentIsDefault;
import com.getinsured.hix.model.PaymentMethods.PaymentStatus;

/**
 * JPA Repository interface for PaymentMethods model.
 * @since 23rd September 2014
 * @author Sharma_k
 *
 */
public interface IPaymentMethodRepository extends JpaRepository<PaymentMethods, Integer>
{

	PaymentMethods findById(int paymentId);
	
	@Query(" FROM PaymentMethods as pm "+
			" where pm.paymentType = :paymentType " +
			" and pm.moduleName = :moduleName " +
			" and pm.subscriptionId is null ")
	List<PaymentMethods> findPaymentMethodLstByModuleNameAndPaymentType(@Param("moduleName") PaymentMethods.ModuleName moduleName, @Param("paymentType") PaymentMethods.PaymentType paymentType);
	
	Page<PaymentMethods> findByModuleIdAndModuleName(int moduleId, ModuleName moduleName, Pageable pageable);
	List<PaymentMethods> findByPaymentMethodName(String paymentMethodName);
	PaymentMethods findByStatusAndIsDefaultAndModuleIdAndModuleName(PaymentStatus status,PaymentIsDefault isDefault, int moduleId, PaymentMethods.ModuleName moduleName);
	//Page<PaymentMethods> findByModuleIdAndModuleName(int moduleId, Pageable pageable, PaymentMethods.ModuleName moduleName);
	//PaymentMethods findByModuleIdAndModuleName(int moduleId, PaymentMethods.ModuleName moduleName);
	//PaymentMethods findByIsDefaultAndModuleIdAndModuleName(PaymentIsDefault isDefault,int moduleId, PaymentMethods.ModuleName moduleName);
	//List<PaymentMethods> findByStatusAndModuleIdAndModuleName(PaymentStatus status, int moduleId, PaymentMethods.ModuleName moduleName);
	//List<PaymentMethods> findPaymentMethodLstByModuleIdAndModuleName(int moduleId, PaymentMethods.ModuleName moduleName);
	/*@Query("delete from PaymentMethods p where p.moduleId = :moduleId")
	void deleteByModuleId(@Param("moduleId") int moduleId);*/
	/**
	 * HQL query to find the PaymentMethod by paymentMethod ID, ModuleID and ModuleName
	 * @since 19th June 2014
	 * @param paymentMethodId
	 * @param moduleId
	 * @param moduleName
	 * @return Object<PaymentMethods>
	 */
	/*@Query("FROM PaymentMethods as paymentMethod WHERE paymentMethod.id = :paymentMethodId"
			+ " AND paymentMethod.moduleId = :moduleId AND paymentMethod.moduleName = :moduleName")
	PaymentMethods findPaymentMethodByIdModuleIdNModuleName(@Param("paymentMethodId") Integer paymentMethodId, @Param("moduleId") Integer moduleId, 
			@Param("moduleName") PaymentMethods.ModuleName moduleName);*/
}
