package com.getinsured.hix.finance.employer.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.EmployerExchangeFees;
import com.getinsured.hix.model.EmployerExchangeFees.ExchangeFeeTypeCode;
import com.getinsured.hix.model.EmployerInvoices;

/**
 * 
 * @author Sharma_k
 * @since 08th January 2015
 * Repository class for EmployerExchangeFees Model.
 */
public interface IEmployerExchangeFeesRepository extends JpaRepository<EmployerExchangeFees, Integer> 
{
	List<EmployerExchangeFees> findByEmployerInvoices(EmployerInvoices invoice);
	
	@Query("FROM EmployerExchangeFees eef where eef.employerInvoices.id = :invoiceId and eef.feeTypeCode <> :feeTypeCode")
	List<EmployerExchangeFees> findByEmpInvExceptEmpUserFee(@Param("invoiceId") int invoiceId, @Param("feeTypeCode") ExchangeFeeTypeCode feeTypeCode);
	
}
