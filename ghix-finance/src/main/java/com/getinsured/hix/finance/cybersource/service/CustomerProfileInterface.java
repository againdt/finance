package com.getinsured.hix.finance.cybersource.service;


import java.util.Map;

import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author sharma_k
 * @since 09th September 2013
 *
 */
@SuppressWarnings("rawtypes")
public interface CustomerProfileInterface 
{
	/**
	 * 
	 * @param createRequest
	 * @return
	 * @throws GIException
	 * For creating Credit And Echeck Customer profile @ Cybersource 
	 */
	Map createCustomerProfile(Map createRequest)throws GIException;
	
	/**
	 * 
	 * @param updateRequest
	 * @return
	 * @throws GIException
	 * For updating Credit card and Echeck Customer profile info
	 */
	Map updateCustomerProfile(Map updateRequest)throws GIException;
	
	/**
	 * 
	 * @param subscriptionId
	 * @return
	 * @throws GIException
	 */
	Map retrieveCustomerProfile(String subscriptionId)throws GIException;
	
	/**
	 * 
	 * @param subscriptionID
	 * @return
	 * @throws GIException
	 */
	Map pciCancelSubscription(String subscriptionID)throws GIException;
}
