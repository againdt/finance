package com.getinsured.hix.finance.emailnotification;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.notification.NotificationAgent;

@Component
public class EmployerPaymentFailureNoticeForAdmin extends NotificationAgent {

	private Map<String, String> templateData;
	
	@Override
	public Map<String, String> getSingleData(){
		setTokens(this.templateData);
		Map<String, String> emailData = new HashMap<>();
		emailData.put("To", this.templateData.get("contactEmail"));
		return emailData;
	}
	
	public void setTemplateData(Map<String, String> templateData){
		this.templateData = templateData;
	}
	
	
}
