package com.getinsured.hix.finance.fees.service;

import com.getinsured.hix.model.ExchangeFee;

/**
 * Interface to manage ExchangeFees transactions
 *
 */
public interface ExchangeFeeService 
{
	 
	/**
	  * Method saveExchangeFee.
	  * @param exchangeFee ExchangeFee
	  * @return ExchangeFee
	  */
	 ExchangeFee saveExchangeFee(ExchangeFee exchangeFee);
	
	 /**
	  * 
	  * @param int issuerInvoiceId
	  * @return Object <ExchangeFee>
	  * @revision 1: As per HIX-19676, float data type changed to BigDecimal
	  * @revision 2: As per jira HIX-44185, removed paymentReceived parameter
	  */
	 ExchangeFee updateExchangeFee(int issuerInvoiceId);
	
	 /**
	  * Method findExchangeFeeByIssuerRemittance.
	  * @param issuerInvoiceId int
	  * @return ExchangeFee
	  */
	 ExchangeFee findExchangeFeeByIssuerRemittance(int issuerRemittanceId);
}
