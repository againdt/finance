package com.getinsured.hix.finance.saml.utils;

import java.util.Map;

import com.getinsured.hix.finance.utils.FinanceConstants;
/**
 * @author Sahoo_s
 * @since 30 April 2014
 */
public class EmployeePaymentSAMLInputContainer 
{
	private String strIssuer;
	//private String strNameID;
	private String strNameQualifier;
	private String sessionId;
	private int maxSessionTimeoutInMinutes = FinanceConstants.NUMERICAL_15;
	private Map<String, String> attributes;
	
	// To hold CA specific objects
	private Map<String, Object> caCustomAttributes; 
	
	public Map<String, Object> getCaCustomAttributes() {
		return caCustomAttributes;
	}

	public void setCaCustomAttributes(Map<String, Object> caCustomAttributes) {
		this.caCustomAttributes = caCustomAttributes;
	}

	/**
	 * Returns the strIssuer.
	 * 
	 * @return the strIssuer
	 */
	public String getStrIssuer() {
		return strIssuer;
	}

	/**
	 * Sets the strIssuer.
	 * 
	 * @param strIssuer
	 *            the strIssuer to set
	 */
	public void setStrIssuer(String strIssuer) {
		this.strIssuer = strIssuer;
	}

	/**
	 * Returns the strNameID.
	 * 
	 * @return the strNameID
	 */
	/*public String getStrNameID() {
		return strNameID;
	}*/

	/**
	 * Sets the strNameID.
	 * 
	 * @param strNameID
	 *            the strNameID to set
	 */
	/*public void setStrNameID(String strNameID) {
		this.strNameID = strNameID;
	}*/

	/**
	 * Returns the strNameQualifier.
	 * 
	 * @return the strNameQualifier
	 */
	public String getStrNameQualifier() {
		return strNameQualifier;
	}

	/**
	 * Sets the strNameQualifier.
	 * 
	 * @param strNameQualifier
	 *            the strNameQualifier to set
	 */
	public void setStrNameQualifier(String strNameQualifier) {
		this.strNameQualifier = strNameQualifier;
	}

	/**
	 * Sets the attributes.
	 * 
	 * @param attributes
	 *            the attributes to set
	 */
	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	/**
	 * Returns the attributes.
	 * 
	 * @return the attributes
	 */
	public Map<String, String> getAttributes() {
		return attributes;
	}

	/**
	 * Sets the sessionId.
	 * 
	 * @param sessionId
	 *            the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * Returns the sessionId.
	 * 
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * Sets the maxSessionTimeoutInMinutes.
	 * 
	 * @param maxSessionTimeoutInMinutes
	 *            the maxSessionTimeoutInMinutes to set
	 */
	public void setMaxSessionTimeoutInMinutes(int maxSessionTimeoutInMinutes) {
		this.maxSessionTimeoutInMinutes = maxSessionTimeoutInMinutes;
	}

	/**
	 * Returns the maxSessionTimeoutInMinutes.
	 * 
	 * @return the maxSessionTimeoutInMinutes
	 */
	public int getMaxSessionTimeoutInMinutes() {
		return maxSessionTimeoutInMinutes;
	}

}
