package com.getinsured.hix.finance.paymentmethod.service.jpa;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.finance.cybersource.serviceimpl.CreditCardProcessor;
import com.getinsured.hix.finance.cybersource.utils.FinancialInfoMapper;
import com.getinsured.hix.finance.paymentmethod.repository.IFinancialInfoRepository;
import com.getinsured.hix.finance.paymentmethod.service.FinancialInfoService;
import com.getinsured.hix.model.BankInfo;
import com.getinsured.hix.model.CreditCardInfo;
import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;



/**
 * Service Implementation class for FinancialInfoService
 * @since 17th December 2014
 * @author Khimla_S
 *
 */
@Service("financialInfoService")
public class FinancialInfoServiceImpl implements FinancialInfoService{
	private static final Logger LOGGER = LoggerFactory
			.getLogger(FinancialInfoServiceImpl.class);
	
	@Autowired private IFinancialInfoRepository financialInfoRepo;
	@Autowired private CreditCardProcessor ccp;
	
	@Override
	public String validateCreditCard(FinancialInfo financialInfo){
		Map reply = null;
		System.out.println("validateCreditCard55");
		FinancialInfoMapper fiMapper = new FinancialInfoMapper();
		fiMapper.setPayRequest(financialInfo, CyberSourceKeyConstants.MERCHANT_CODE);
		reply = ccp.authorize(fiMapper.getPayRequest(), "11");		
		return (String) reply.get("decision");
	}
	
	@Override	
	@Transactional(readOnly=true)
	public List<FinancialInfo> findAll(){
		return financialInfoRepo.findAll();
	}
	
	@Override	
	@Transactional(readOnly=true)
	public FinancialInfo findById(Integer id){
		return financialInfoRepo.findOne(id);
	}
	
	@Override	
	@Transactional(readOnly=true)
	public Map<Integer,BankInfo> getBankInfo(List<FinancialInfo> financialInfos){
		Map<Integer,BankInfo> banksMap = new HashMap<Integer,BankInfo>();
		BankInfo bankInfo;
		for(FinancialInfo finInfo : financialInfos)
		{
			bankInfo = finInfo.getBankInfo();
			if (bankInfo != null)
			{
				banksMap.put(finInfo.getId(),bankInfo);
			}
		}
		return banksMap;
	}
	
	@Override	
	@Transactional(readOnly=true)
	public Map<Integer,CreditCardInfo> getCreditCardInfo(List<FinancialInfo> financialInfos){
		Map<Integer,CreditCardInfo> creditsMap = new HashMap<Integer,CreditCardInfo>();
		CreditCardInfo creditCardInfo;
		for(FinancialInfo finInfo : financialInfos)
		{
			creditCardInfo = finInfo.getCreditCardInfo();
			if (creditCardInfo != null)
			{
				creditsMap.put(finInfo.getId(),creditCardInfo);
			}
		}
		return creditsMap;
	}
	
	@Override	
	@Transactional(readOnly=true)
	public String getBankJsonString(List<FinancialInfo> financialInfos){
		BankInfo bankInfo;
		JSONObject bankJasonObj = new JSONObject();
		String bankJsonText = "";
		try {
			for(FinancialInfo finInfo : financialInfos)
			{
				bankInfo = finInfo.getBankInfo();
				if (bankInfo != null)
				{
					bankJasonObj.put(String.valueOf(finInfo.getId()), finInfo.toJson());
					StringWriter out = new StringWriter();
					bankJasonObj.write(out);
					bankJsonText = out.toString();
				}
			}
		} catch (JSONException e) {
			LOGGER.error("Json Exception - ",e);
		}
		return bankJsonText;
	}
	
	@Override	
	@Transactional(readOnly=true)
	public String getCreditCardJsonString(List<FinancialInfo> financialInfos){
		CreditCardInfo creditCardInfo;
		JSONObject creditJasonObj = new JSONObject();
		String creditJsonText = "";
		try {
			for(FinancialInfo finInfo : financialInfos)
			{
				creditCardInfo = finInfo.getCreditCardInfo();
				if (creditCardInfo != null)
				{
					creditJasonObj.put(String.valueOf(finInfo.getId()), finInfo.toJson());
					StringWriter out = new StringWriter();
					creditJasonObj.write(out);
					creditJsonText = out.toString();
				}
			}
		} catch (JSONException e) {
			LOGGER.error("Json Exception - ",e);
		}
		
		return creditJsonText;
	}
	
	@Override	
	@Transactional()
	public FinancialInfo saveFinancialInfoData(FinancialInfo financialInfo){
		return financialInfoRepo.save(financialInfo);
	}
	
	
	
	

}
