package com.getinsured.hix.finance.employer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Employer;

/**
 */
public interface IEmployerFinanceRepository extends JpaRepository<Employer, Integer> 
{
	/**
	 * Method findByExternaId.
	 * @param externalId String
	 * @return Employer
	 */
	@Query("SELECT emp FROM Employer emp where externalId = :externalId")
	 Employer findByExternaId(@Param("externalId") String externalId);
}
