/**
 * 
 */
package com.getinsured.hix.finance.cybersource.mock;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.finance.cybersource.service.PaymentProcessor;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceConstants.CyberSourceResponseMapKeys;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author reddy_h
 *
 */
@SuppressWarnings("all")
@Component
public class ECheckMockPaymentProcessor implements PaymentProcessor {


	@Override
	public Map authorize(Map pCust, String amount) {

		return null;
	}

	/* (non-Javadoc)
	 * @see com.getinsured.hix.finance.cybersource.service.PaymentProcessor#credit(java.util.Map, java.lang.String)
	 */
	@Override
	public Map credit(Map pCust, String amount) {
		Map mockReply = null;
		if(!CollectionUtils.isEmpty(pCust)){
			mockReply = new HashMap();
			mockReply.put("ecCreditReply_amount", amount);
			/*mockReply.put("merchantReferenceCode", "MRC-14344");*/
			mockReply.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, (String)pCust.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY));
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTTOKEN, "");
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE, "100");
			mockReply.put("ecCreditReply_reasonCode", "100");
			mockReply.put("ecCreditReply_requestDateTime", DateTime.now().toString());
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_DECISION, FinanceConstants.CYBERSOURCE_ACCEPT);
			mockReply.put("ecCreditReply_settlementMethod", "B");
			mockReply.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, CyberSourceKeyConstants.PT_CURRENCY);
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID, RandomUtils.nextLong()+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9)+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9));
			mockReply.put("ecCreditReply_reconciliationID", RandomStringUtils.randomAlphanumeric(FinanceConstants.NUMERICAL_12));
			mockReply.put("ecCreditReply_processorResponse", "123456");
		}
		return mockReply;
	}

	/* (non-Javadoc)
	 * @see com.getinsured.hix.finance.cybersource.service.PaymentProcessor#debit(java.util.Map, java.lang.String)
	 */
	@Override
	public Map debit(Map pCust, String amount) {
		Map mockReply = null;
		if(!CollectionUtils.isEmpty(pCust)){
			mockReply = new HashMap();
			mockReply.put("ecDebitReply_amount", amount);
			mockReply.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, (String)pCust.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY));
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTTOKEN, "");
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE, "100");
			mockReply.put("ecDebitReply_requestDateTime", DateTime.now().toString());
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_DECISION, FinanceConstants.CYBERSOURCE_ACCEPT);
			mockReply.put("ecDebitReply_settlementMethod", "B");
			mockReply.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, CyberSourceKeyConstants.PT_CURRENCY);
			mockReply.put("ecDebitReply_avsCode", "1");
			mockReply.put("ecDebitReply_verificationLevel", "1");
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID, RandomUtils.nextLong()+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9)+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9));
			mockReply.put("ecDebitReply_reconciliationID", RandomStringUtils.randomAlphanumeric(FinanceConstants.NUMERICAL_12));
			mockReply.put("ecDebitReply_processorResponse", "123456");
		}
		return mockReply;
	}

	/* (non-Javadoc)
	 * @see com.getinsured.hix.finance.cybersource.service.PaymentProcessor#pciAuthorize(java.util.Map, java.lang.String)
	 */
	@Override
	public Map pciAuthorize(Map pCust, String amount) throws GIException {

		return null;
	}

	/* (non-Javadoc)
	 * @see com.getinsured.hix.finance.cybersource.service.PaymentProcessor#pciCredit(java.util.Map, java.lang.String)
	 */
	@Override
	public Map pciCredit(Map pCust, String amount) throws GIException {
		Map mockReply = null;
		if(!CollectionUtils.isEmpty(pCust)){
			mockReply = new HashMap();
			String merchantRefCode = (String) pCust.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY);
			mockReply.put("ecCreditReply_amount", amount);
			mockReply.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, merchantRefCode);
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTTOKEN, "");
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE, "100");
			mockReply.put("ecCreditReply_reasonCode", "100");
			mockReply.put("ecCreditReply_requestDateTime", DateTime.now().toString());
			mockReply.put("ecCreditReply_ownerMerchantID", DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MERCHANT_ID));
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_DECISION, FinanceConstants.CYBERSOURCE_ACCEPT);
			mockReply.put("ecCreditReply_settlementMethod", "B");
			mockReply.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, CyberSourceKeyConstants.PT_CURRENCY);
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID, RandomUtils.nextLong()+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9)+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9));
			mockReply.put("ecCreditReply_reconciliationID", RandomStringUtils.randomAlphanumeric(FinanceConstants.NUMERICAL_12));
			mockReply.put("ecCreditReply_processorResponse", "123456");
		}
		return mockReply;
	}

	/* (non-Javadoc)
	 * @see com.getinsured.hix.finance.cybersource.service.PaymentProcessor#pciDebit(java.util.Map, java.lang.String)
	 */
	@Override
	public Map pciDebit(Map pCust, String amount) throws GIException {
		Map mockReply = null;
		if(!CollectionUtils.isEmpty(pCust)){
			mockReply = new HashMap();
			String merchantRefCode = (String) pCust.get("merchantReferenceCode");
			mockReply.put("ecDebitReply_amount", amount);
			mockReply.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, merchantRefCode);
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTTOKEN, "");
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE, "100");
			mockReply.put("ecDebitReply_requestDateTime", DateTime.now().toString());
			mockReply.put("ecDebitReply_ownerMerchantID", DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MERCHANT_ID));
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_DECISION, FinanceConstants.CYBERSOURCE_ACCEPT);
			mockReply.put("ecDebitReply_settlementMethod", "B");
			mockReply.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, CyberSourceKeyConstants.PT_CURRENCY);
			mockReply.put("ecDebitReply_avsCode", "1");
			mockReply.put("ecDebitReply_verificationLevel", "1");
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID, RandomUtils.nextLong()+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9)+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9));
			mockReply.put("ecDebitReply_reconciliationID", RandomStringUtils.randomAlphanumeric(FinanceConstants.NUMERICAL_12));
			mockReply.put("ecDebitReply_processorResponse", "123456");
		}
		return mockReply;
	}

}
