package com.getinsured.hix.finance.paymentmethod.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.BankInfo;

public interface IBankInfoRepository extends JpaRepository<BankInfo, Integer> {

}
