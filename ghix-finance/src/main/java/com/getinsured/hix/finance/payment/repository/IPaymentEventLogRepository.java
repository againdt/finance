package com.getinsured.hix.finance.payment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PaymentEventLog;

public interface IPaymentEventLogRepository extends JpaRepository<PaymentEventLog, Integer> 
{
	/**
	 * 
	 * @param merchantRefCode
	 * @param eventType
	 * @return
	 */
	@Query("FROM PaymentEventLog as payEvtLog where payEvtLog.merchantRefCode = :merchantRefCode and payEvtLog.eventType = :eventType")
	List<PaymentEventLog> getPaymentEventLogByRefNoAndEvent(@Param("merchantRefCode") String merchantRefCode, @Param("eventType") String eventType);
	
	/**
	 * 
	 * @param merchantRefCode
	 * @return
	 */
	@Query ("FROM PaymentEventLog as payEvtLog WHERE payEvtLog.merchantRefCode = :merchantRefCode and payEvtLog.eventType IS NOT NULL ORDER BY payEvtLog.createdOn ASC ")
	List<PaymentEventLog> getPymtEventLogByMerchanantRefCode(@Param("merchantRefCode") String merchantRefCode);
	
	@Query("FROM PaymentEventLog WHERE merchantRefCode in (:merchantRefCode)")
	List<PaymentEventLog> getPaymentEventLogByMerchantRefCodeList(@Param("merchantRefCode") List<String> merchantRefCode);
	
	/**
	 * 
	 * @param merchantRefCode
	 * @param requestID
	 * @param txnStatus
	 * @return
	 */
	@Query("FROM PaymentEventLog WHERE merchantRefCode = (:merchantRefCode) AND requestId = :requestID AND txnStatus = :txnStatus")
	PaymentEventLog getPaymentEventLogByMerchantRefCodeNReqIdNTxnStatus(@Param("merchantRefCode")String  merchantRefCode,
			@Param("requestID")String requestID, @Param("txnStatus")String txnStatus);
}
