package com.getinsured.hix.finance.payment.service;

import com.getinsured.hix.platform.util.exception.GIException;

/**
 * 
 * @author Sharma_k
 *
 */
public interface PaymentReconciliationService 
{
	/**
	 * 
	 * @throws GIException
	 * Auto download file from Cybersource and reconcile payments.
	 */
	 void processPaymentReport()throws GIException;
	
	/**
	 * @since 25th March 2014
	 * @param paymentEventReportFileName
	 * @throws GIException
	 * Manually running the file given as batch job parameter.  
	 */
	 void processPytRptManually(String paymentEventReportFileName)throws GIException;
}
