package com.getinsured.hix.finance.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.dto.finance.PaymentMethodResponse;
import com.getinsured.hix.finance.cybersource.service.CustomerProfileInterface;
import com.getinsured.hix.finance.paymentmethod.service.PaymentMethodService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceErrorCodes;
import com.getinsured.hix.finance.utils.FinanceGeneralUtility;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.thoughtworks.xstream.XStream;

/**
 * Controller class for paymentMethod related services.
 * @since 24th September 2014
 * @author Sharma_k
 *
 */
@Controller
@RequestMapping("/paymentmethod")
public class PaymentMethodController 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentMethodController.class);
	
	@Autowired
	private PaymentMethodService financePaymentMethodService;
	@Autowired private CustomerProfileInterface customerProfileInterface;
	/**
	 * Search PCI payment method by payment id.
	 * @param int - paymentId
	 * @return String - PaymentMethodResponse object as String.
	 */
	@RequestMapping(value = "/findpaymentmethodbyPCI/{id}", method = RequestMethod.GET)
	@ResponseBody public String findPaymentmethodbyPCI(@PathVariable(value = "id") int paymentId)
	{
		LOGGER.debug("=============== Start :: Inside findPaymentmethodbyPCi ::"+paymentId+" ===============");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();		
		PaymentMethodResponse financeResponse = new PaymentMethodResponse();
		try {
			if(paymentId == 0)
			{
				financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else
			{	
				financeResponse.setPaymentMethods(financePaymentMethodService.getPaymentMethodByPCI(paymentId) );
				financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
		} catch (Exception e) {
			LOGGER.error("=============== Exception :: Inside findPaymentmethodbyPCi ===============", e);
			financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.FIND_PAY_METHOD_BY_PCI.getCode());
			financeResponse.setErrMsg(e.getMessage());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		LOGGER.info("=============== End :: Inside findPaymentmethodbyPCi ===============");
		return xstream.toXML(financeResponse);
	}

	/**
	 * Save Payment method.
	 * @param PaymentMethods - newPaymentMethods
	 * @return String - PaymentMethodResponse object as String.
	 */
	@RequestMapping(value = "/savepaymentmethod", method = RequestMethod.POST, headers = "Accept=*/*")
	@ResponseBody public String savePaymentMethod(@RequestBody PaymentMethods newPaymentMethods)
	{
		LOGGER.debug("=============== Start :: Inside savePaymentMethod ===============");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();		
		PaymentMethodResponse financeResponse = new PaymentMethodResponse();
		try {
			if(newPaymentMethods == null)
			{
				financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else
			{	
				financeResponse.setPaymentMethods(financePaymentMethodService.savePaymentMethods(newPaymentMethods) );
				financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
		} catch (Exception e) {
			LOGGER.error("=============== Exception :: Inside savePaymentMethod ===============", e);
			financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.FIND_PAY_METHOD_BY_PCI.getCode());
			financeResponse.setErrMsg(e.getMessage());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		LOGGER.info("=============== End :: Inside savePaymentMethod ===============");
		return xstream.toXML(financeResponse);
	}
	
	/**
	 * Modify payment status or default setting.	 * 
	 * Id is mandatory in request PaymentMethodRequestDTO. 
	 * -For updating default setting 'PaymentIsDefault' is required in PaymentMethodRequestDTO and PaymentStatus should be null.
	 * -For updating payment status 'PaymentStatus' is required in PaymentMethodRequestDTO and PaymentIsDefault should be null
	 * 
	 * @param PaymentMethodRequestDTO - paymentMethodRequestDTO
	 * @return String - PaymentMethodResponse object as String.
	 */
	@RequestMapping(value = "/modifypaymentstatus", method = RequestMethod.POST, headers = "Accept=*/*")
	@ResponseBody public String modifyPaymentstatus(@RequestBody PaymentMethodRequestDTO paymentMethodRequestDTO)
	{
		LOGGER.debug("=============== Start :: Inside modifyPaymentstatus ===============");
		String response = "";
		try {
			if(paymentMethodRequestDTO == null)
			{
				response = FinanceGeneralUtility.sendXtreamFailurePaymentMethodResponseWhenEmptyRequest("PaymentMethodRequestDTO should not be null");				
					}
			else if(paymentMethodRequestDTO.getId() != FinanceConstants.NUMERICAL_0)
					{
				PaymentMethodResponse financeResponse = new PaymentMethodResponse();
				financeResponse.setPaymentMethods(financePaymentMethodService
						.modifyPaymentstatus(paymentMethodRequestDTO.getId(),
								paymentMethodRequestDTO.getIsDefaultPaymentMethod(),
								paymentMethodRequestDTO.getPaymentMethodStatus(), paymentMethodRequestDTO.getLastUpdatedBy()));
				response = FinanceGeneralUtility.sendXtreamPaymentMethodResponseWhenSuccess(financeResponse);
					}					
		} catch (Exception e) {
			LOGGER.error("=============== Exception :: Inside modifyPaymentstatus ===============", e);
			response = FinanceGeneralUtility.sendXtreamFailurePaymentMethodResponseWhenException(e.getMessage(),FinanceErrorCodes.ERRORCODE.MODIFY_PAYMENT_STATUS_SETTING.getCode());
			
		}
		LOGGER.info("=============== End :: Inside modifyPaymentstatus ===============");
		return response;
	}
	
	@RequestMapping(value="/getPaymentMethodById", method=RequestMethod.POST)
	@ResponseBody
	public String getPaymentMethodById(@RequestBody int id)
	{
		String response = "";
		try{
			if(id == FinanceConstants.NUMERICAL_0){
				response = FinanceGeneralUtility.sendXtreamFailurePaymentMethodResponseWhenEmptyRequest("ID should not be null/zero");
			}
			else{
				PaymentMethods paymentMethods  = financePaymentMethodService.getPaymentMethodsDetail(id);
				if(paymentMethods != null){
					PaymentMethodResponse paymentResponse = new PaymentMethodResponse();
					paymentResponse.setPaymentMethods(paymentMethods);
					response = FinanceGeneralUtility.sendXtreamPaymentMethodResponseWhenSuccess(paymentResponse);
				}
				else{
					response = FinanceGeneralUtility.sendXtreamFailurePaymentMethodResponseWhenResultsEmpty("");
				}
			}
		}catch(Exception e){
			LOGGER.error("=============== Exception :: Inside getPaymentMethodById ===============", e);
			response = FinanceGeneralUtility.sendXtreamFailurePaymentMethodResponseWhenResultsEmpty(e.getMessage());
		}
		return response;
	}
	
	@RequestMapping(value="/getPagePaymentMethodsByMouduleIdAndModuleNameAndPageable", method = RequestMethod.POST)
	@ResponseBody
	public String getPagePaymentMethodsByMouduleIdAndModuleNameAndPageable(@RequestBody PaymentMethodRequestDTO requestDto)
	{
		String response = "";
		try{
			if(requestDto == null){
				response = FinanceGeneralUtility.sendXtreamFailurePaymentMethodResponseWhenEmptyRequest("Search criteria should not be null/empty");
			}
			else{
				
				response = this.getPaymentMethodByModuleIdNameAndPageable(requestDto);				
			}
		}catch(Exception e){
			LOGGER.error("=============== Exception :: Inside getPagePaymentMethodsByMouduleIdAndModuleNameAndPageable ===============", e);
			response = FinanceGeneralUtility.sendXtreamFailurePaymentMethodResponseWhenResultsEmpty(e.getMessage());
		}
		return response;
	}
	private String getPaymentMethodByModuleIdNameAndPageable(PaymentMethodRequestDTO requestDto)
	{	
		String response = "";		
		Integer pageSize = requestDto.getPageSize();
		Integer pageNumber = requestDto.getPageNumber();
					
		Pageable pageable = null;
		if(pageSize != null && pageNumber !=null)
		{
			pageable = constructPageableObject(pageSize, pageNumber, requestDto.getSortBy(), requestDto.getSortOrder());
		}
		if(requestDto.getModuleID() != null && requestDto.getModuleID() != 0 && requestDto.getModuleName() != null)
		{
			Page<PaymentMethods> pagePaymentMethods  = financePaymentMethodService.getPaymentMethodByMouduleIdAndModuleNameAndPageable(requestDto.getModuleID(), requestDto.getModuleName(), pageable);
					if(pagePaymentMethods != null){
						PaymentMethodResponse paymentResponse = new PaymentMethodResponse();
						paymentResponse.setPagePaymentMethods(pagePaymentMethods);
						response = FinanceGeneralUtility.sendXtreamPaymentMethodResponseWhenSuccess(paymentResponse);
					}
					else{
						response = FinanceGeneralUtility.sendXtreamFailurePaymentMethodResponseWhenResultsEmpty("No results found");
					}
				}
				else{
					response = FinanceGeneralUtility.sendXtreamFailurePaymentMethodResponseWhenEmptyRequest("One or more input fields are null/empty, all fields are mandatory");
				}
		return response;
	}
	
	
	private Pageable constructPageableObject(int pageSize, int pageNumber, String sortBy, String sortOrder){
		
		Sort sortObject = null;
		Pageable pageable = null;
		if(StringUtils.hasText(sortBy)){
			Sort.Direction direction = Sort.Direction.ASC;
			if("DESC".equalsIgnoreCase(sortOrder)){
				direction = Sort.Direction.DESC;
			}
			sortObject = new Sort(new Sort.Order(direction, sortBy));
		}
		if(sortObject!=null){
			pageable = new PageRequest(pageNumber-1, pageSize, sortObject);
		}else{
			pageable = new PageRequest(pageNumber-1, pageSize);
		}	
		return pageable;
	}
	
	/**
	 * Rest Controller Call for searchPaymentMethod Response Code{200- Success, 201- null request received, 202- Received no response from search criteria}
	 * Jira Id: HIX-50488
	 * @since 29th September 2014
	 * @param paymentMethodRequestDTO Object<PaymentMethodRequestDTO>
	 * @return
	 */
	@RequestMapping(value = "/searchpaymentmethod", method = RequestMethod.POST)
	@ResponseBody public String searchPaymentMethod(@RequestBody PaymentMethodRequestDTO paymentMethodRequestDTO)
	{
		PaymentMethodResponse paymentMethodResponse = new PaymentMethodResponse();
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		LOGGER.info("PaymentMethodController - Inside SearchPaymentMethod(-) controller...");
		if(paymentMethodRequestDTO == null)
		{
			LOGGER.warn("PaymentMethodController - Received Null Request for SearchPaymentMethod call.");
			paymentMethodResponse.setErrCode(FinanceConstants.ERROR_CODE_201);
			paymentMethodResponse.setErrMsg("Received Null Request for searchPaymentMethod call.");
			paymentMethodResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		else
		{
			try
			{
				Map<String, Object> paymentMethodResponseMap = financePaymentMethodService.searchEmployerPaymentMethod(paymentMethodRequestDTO);
				
				if(paymentMethodResponseMap != null && !paymentMethodResponseMap.isEmpty())
				{
					/**
					 * Response code 200 will be sent if Data found for the given searchCriteria
					 */
					paymentMethodResponse.setPaymentMethodMap(paymentMethodResponseMap);
					paymentMethodResponse.setErrCode(FinanceConstants.SUCCESS_CODE_200);
					paymentMethodResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
				else
				{
					/**
					 * Response will be sent with ErrorCode 202 if No Data found by given searchCriteria 
					 */
					paymentMethodResponse.setErrCode(FinanceConstants.ERROR_CODE_202);
					paymentMethodResponse.setPaymentMethodMap(new HashMap<String, Object>());
					paymentMethodResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					paymentMethodResponse.setErrMsg("No payment method found for the given searchCriteria");
				}
			}
			catch(GIException gie)
			{
				LOGGER.error("=============== Exception :: Inside searchPaymentMethod ===============", gie);
				paymentMethodResponse.setErrCode(FinanceConstants.ERROR_CODE_203);
				paymentMethodResponse.setErrMsg(gie.toString());
				paymentMethodResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}
		return xstream.toXML(paymentMethodResponse);
	}
	
	@RequestMapping(value = "/migratepaymentmethod", method = RequestMethod.POST)
	@ResponseBody public String migratePaymentMethod(@RequestBody PaymentMethodRequestDTO paymentMethodRequestDTO)
	{
		PaymentMethodResponse paymentMethodResponse = new PaymentMethodResponse();
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		LOGGER.info("PaymentMethodController - Inside migratePaymentMethod(-) controller...");
		if(paymentMethodRequestDTO == null)
		{
			LOGGER.warn("PaymentMethodController - Received Null Request for migratepaymentmethod call.");			
			paymentMethodResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
			paymentMethodResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		else if( paymentMethodRequestDTO.getPaymentMethodDTOList() == null || paymentMethodRequestDTO.getPaymentMethodDTOList().isEmpty() )
		{
			LOGGER.warn("PaymentMethodController - Received Null Request for migratepaymentmethod call.");			
			paymentMethodResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
			paymentMethodResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		else if(paymentMethodRequestDTO.getPaymentMethodDTOList() != null &&  paymentMethodRequestDTO.getPaymentMethodDTOList().size() > GhixConstants.PAYMENT_METHOD_MIGRATION_LIMIT)
		{
			LOGGER.warn("PaymentMethodController - Migration limit exceeds the expected count.");			
			paymentMethodResponse.setErrMsg("Migration limit exceeds the expected count: "+GhixConstants.PAYMENT_METHOD_MIGRATION_LIMIT);
			paymentMethodResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		else
		{
			try
			{
				List<Map<Integer, Object>> paymentMethodResponseList = financePaymentMethodService.migratePaymentMethodPci(paymentMethodRequestDTO.getPaymentMethodDTOList());
				
				if(paymentMethodResponseList != null && paymentMethodResponseList.isEmpty())
				{
					paymentMethodResponse.setErrCode(FinanceConstants.SUCCESS_CODE_200);
					paymentMethodResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
				else if(verifyPaymentMethodResponseList(paymentMethodResponseList, paymentMethodRequestDTO.getPaymentMethodDTOList().size()))
				{
					paymentMethodResponse.setErrCode(FinanceErrorCodes.ERRORCODE.MIGRATE_PAYMENT_METHOD.getCode());
					paymentMethodResponse.setPaymentMethodResponseList(paymentMethodResponseList);
					paymentMethodResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					paymentMethodResponse.setErrMsg("Migration Failed");
				}
				else
				{
					paymentMethodResponse.setErrCode(FinanceErrorCodes.ERRORCODE.MIGRATE_PAYMENT_METHOD.getCode());
					paymentMethodResponse.setPaymentMethodResponseList(paymentMethodResponseList);
					paymentMethodResponse.setStatus(GhixConstants.RESPONSE_PARTIAL_SUCCESS);
					paymentMethodResponse.setErrMsg("Migration Successfull Partially.");
				}
			}
			catch(GIException gie)
			{
				LOGGER.error("=============== Exception :: Inside migratePaymentMethod ===============", gie);
				paymentMethodResponse.setErrCode(FinanceErrorCodes.ERRORCODE.MIGRATE_PAYMENT_METHOD.getCode());
				paymentMethodResponse.setErrMsg(gie.toString());
				paymentMethodResponse.setStatus(GhixConstants.RESPONSE_FAILURE);				
			}
			catch(Exception ex)
			{
				LOGGER.error("=============== Exception :: Inside migratePaymentMethod ===============", ex);
				paymentMethodResponse.setErrCode(FinanceErrorCodes.ERRORCODE.MIGRATE_PAYMENT_METHOD.getCode());
				paymentMethodResponse.setErrMsg(ex.toString());
				paymentMethodResponse.setStatus(GhixConstants.RESPONSE_FAILURE);				
			}
		}
		return xstream.toXML(paymentMethodResponse);
	}
	
	private boolean verifyPaymentMethodResponseList(final List<Map<Integer, Object>> paymentMethodResponseList, final int paymentMethodRequestDTOSize)
	{
		if(paymentMethodResponseList != null && !paymentMethodResponseList.isEmpty())
		{
			Map<Integer, Object> exceptionMap =  paymentMethodResponseList.get(0);
			if(exceptionMap != null && !exceptionMap.isEmpty() && exceptionMap.size() == paymentMethodRequestDTOSize)
			{
				return true;
			}
		}
		
		return false;
	}
	
	
	@RequestMapping(value = "/retrieveCustomerProfile", method = RequestMethod.POST, headers = "Accept=*/*")
	@ResponseBody public String retrieveCustomerProfile(@RequestBody PaymentMethodRequestDTO paymentMethodRequestDTO)
	{
		LOGGER.debug("=============== Start :: Inside retrieveCustomerProfile ===============");
		String response = "";
		try {
			if(paymentMethodRequestDTO == null)
			{
				response = FinanceGeneralUtility.sendXtreamFailurePaymentMethodResponseWhenEmptyRequest("PaymentMethodRequestDTO should not be null");				
			}
			else if(paymentMethodRequestDTO.getSubscriptionID() != null)
			{
				PaymentMethodResponse financeResponse = new PaymentMethodResponse();
				financeResponse.setResponsedMap(customerProfileInterface.retrieveCustomerProfile(paymentMethodRequestDTO.getSubscriptionID()));
				response = FinanceGeneralUtility.sendXtreamPaymentMethodResponseWhenSuccess(financeResponse);
			}					
		} catch (Exception e) {
			LOGGER.error("=============== Exception :: Inside retrieveCustomerProfile ===============", e);
			response = FinanceGeneralUtility.sendXtreamFailurePaymentMethodResponseWhenException(e.getMessage(),FinanceErrorCodes.ERRORCODE.RETRIEVE_CUST_PROFILE.getCode());
		}
		LOGGER.info("=============== End :: Inside retrieveCustomerProfile ===============");
		return response;
	}
	
}
