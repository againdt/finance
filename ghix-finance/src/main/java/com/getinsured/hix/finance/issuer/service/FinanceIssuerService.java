package com.getinsured.hix.finance.issuer.service;

import com.getinsured.hix.model.Issuer;

public interface FinanceIssuerService 
{
	Issuer getIssuerByHiosIDAndIssuerName(String hiosIssuerID, String issuerName);
	
	/**
	 * 
	 * @param hiosIssuerID
	 * @return
	 */
	Issuer getIssuerByHiosIssuerID(String hiosIssuerID);
}
