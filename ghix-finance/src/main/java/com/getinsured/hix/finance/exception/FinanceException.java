package com.getinsured.hix.finance.exception;

public class FinanceException extends Exception {

	private static final long serialVersionUID = -8161278406005226590L;
	
	public FinanceException() {
		super();
	}
	public FinanceException(Throwable cause) {
		super(cause);
	}
	public FinanceException(String message, Throwable cause) {
		super(message, cause);
	}
	public FinanceException(String message) {
		super(message);
	}
}
