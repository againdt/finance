package com.getinsured.hix.finance.cybersource.utils;

import java.util.Properties;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;

/**
 * @author Sharma_k
 * @since 12th September 2013
 * class is written to make Cybersource properties available for all beans.
 */
public class CyberSourceProperties 
{
	private Properties cybProperties;

	public CyberSourceProperties()
	{
		Properties cyberSourceProperties = new Properties();
		cyberSourceProperties.setProperty(CyberSourceKeyConstants.MERCHANTID_KEY, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MERCHANT_ID));
		cyberSourceProperties.setProperty(CyberSourceKeyConstants.SEND_TO_PRODUCTION_FLAG, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.SEND_TO_PRODUCTION));
		cyberSourceProperties.setProperty(CyberSourceKeyConstants.TARGET_API_REVISION, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.TARGET_API_VERSION));
		cyberSourceProperties.setProperty(CyberSourceKeyConstants.KEYS_DIRECTORY, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.UPLOAD_PATH));
		this.cybProperties=cyberSourceProperties;
	}
	/**
	 * 
	 * @return
	 */
	public Properties getCybProperties()
	{
		return cybProperties;
	}	
}
