package com.getinsured.hix.finance.bank.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.BankHoliday;

/**
 * 
 * @author Sharma_k
 *
 */
public interface IBankHolidayRepository extends JpaRepository<BankHoliday, Integer>
{
	/**
	 * 
	 * @param year
	 * @return
	 */
	@Query("SELECT COUNT(*) FROM BankHoliday bkh WHERE bkh.holidayYear IN(:year)")
	long findNoOfBankHolidaysByYear(@Param("year")String year);
	
	/**
	 * 
	 * @param startDate
	 * @param endDate
	 * @return
	 * 
	 */
	//TODO verify if startDate or EnDate itself a bank holiday
	//@Query("SELECT holidayDate FROM BankHoliday bkh WHERE bkh.holidayDate BETWEEN TO_DATE(:startDate, 'mm-dd-yyyy') AND TO_DATE(:endDate, 'mm-dd-yyyy')")
	@Query("SELECT holidayDate FROM BankHoliday bkh WHERE bkh.holidayDate BETWEEN :startDate AND :endDate")
	List<Date> findBankHolidaysBetween(@Param("startDate") Date startDate,@Param("endDate") Date endDate);

}
