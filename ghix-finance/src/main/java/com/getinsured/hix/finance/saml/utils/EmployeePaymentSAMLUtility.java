package com.getinsured.hix.finance.saml.utils;

import java.io.StringWriter;
import java.security.KeyStoreException;
import java.security.cert.CertificateException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringUtils;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.Response;
import org.opensaml.xml.Configuration;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.io.Unmarshaller;
import org.opensaml.xml.io.UnmarshallerFactory;
import org.opensaml.xml.schema.XSString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.getinsured.hix.dto.enrollment.EnrolleePaymentDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentPaymentDTO;
import com.getinsured.hix.finance.saml.ca.utils.CarrierPayment;
import com.getinsured.hix.finance.saml.ca.utils.MemberInformation;
import com.getinsured.hix.finance.saml.ca.utils.ResponsibleParty;
import com.getinsured.hix.finance.utils.FinanceConstants.SAMLConstants;
import com.getinsured.hix.finance.utils.FinancialMgmtGenericUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.GhixConstants;

/**
 * Utility class which helps to create 
 * the SAML request for FFM check out
 * @author Sahoo_s
 * @since 30 April 2014
 */

public class EmployeePaymentSAMLUtility
{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeePaymentSAMLUtility.class);
	private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	/**
	 * Returns the encoded SAML string which will passed 
	 * to the payment redirect URL for FFM checkout.
	 * @param configParams
	 * 					Contain key store related information
	 * @param inputData
	 * 					Contain input parameters to prepare SAML assertion
	 * @return encodedSAMLRespone
	 * @throws KeyStoreException 
	 * @throws CertificateException 
	 */
	public String prepareSAMLOutput(Map<String, String> configParams, EnrollmentPaymentDTO inputData)
	{
		EmployeePaymentSAMLInputContainer samlInput = this.prepareInputForSAMLGeneration(configParams, inputData);
		String encodedSAMLRespone = EmployeePaymentSAMLGenerator.generateEncodedSAML(samlInput, configParams, inputData.getPaymentUrl());
		return encodedSAMLRespone;
	}
	
	
	/**
	 * Returns the specified object with subject information
	 * and attributes information for SAML Assertion preparation
	 * @param configParams
	 * 					Contain key store related information
	 * @param inputData
	 * 					Contain input parameters to prepare SAML assertion
	 * @return samlInput
	 * 					
	 */
	private EmployeePaymentSAMLInputContainer prepareInputForSAMLGeneration(Map<String, String> configParams, EnrollmentPaymentDTO inputData)
	{
		EmployeePaymentSAMLInputContainer samlInput = new EmployeePaymentSAMLInputContainer();
		samlInput.setStrNameQualifier("");//need to correct this or remove if it works fine
		samlInput.setSessionId(SAMLConstants.RANDOM_SESSION_ID);
		samlInput.setStrIssuer(configParams.get(SAMLConstants.ISSUER_NAME));
		
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		
		if(stateCode.equalsIgnoreCase("CA")){
			//called for CA only
			samlInput.setCaCustomAttributes(this.getCustomAttributesForCA(inputData));
		}
		else{
			samlInput.setAttributes(this.getCustomAttributes(inputData,stateCode));
		}
		return samlInput;
	}


	
	
	private Map<String,Object> getCustomAttributesForCA(EnrollmentPaymentDTO inputData){
		Map<String, Object> customAttributes = new HashMap<String, Object>();
		CarrierPayment carrierPayment = new CarrierPayment();
		carrierPayment.setAPTCAmount(FinancialMgmtGenericUtil.getBigDecimalObj(inputData.getAptcAmt()));
		carrierPayment.setLanguage(inputData.getPreferredLanguage());
		carrierPayment.setCaseID(inputData.getCaseId());
		carrierPayment.setPaymentTransactionId(inputData.getPaymentTxnId());
		carrierPayment.setPreviousPaymentTransactionId(inputData.getPreviousPaymentTxnId());
		carrierPayment.setProposedCoverageEffectiveDate(FinancialMgmtGenericUtil.stringToXMLGregorianCalendar(dateFormat.format(inputData.getBenefitEffectiveDate()).toString()));
		carrierPayment.setQHPPlanId(inputData.getCmsPlanId());
		carrierPayment.setResidenceCountyCode(inputData.getCountyCode());
		carrierPayment.setResidenceZipCode(inputData.getZip());
		carrierPayment.setSubscriberID(inputData.getExchgSubscriberIdentifier());
		carrierPayment.setTotalPremiumAmount(FinancialMgmtGenericUtil.getBigDecimalObj(inputData.getGrossPremiumAmt()));
		carrierPayment.setTotalPremiumOwed(FinancialMgmtGenericUtil.getBigDecimalObj(inputData.getNetPremiumAmt()));
		
		
		ResponsibleParty responsibleParty = new ResponsibleParty();
		// Name 
		responsibleParty.setFirstName(inputData.getFirstName());
		responsibleParty.setLastName(inputData.getLastName());
		responsibleParty.setMiddleName(inputData.getMiddleName());
		
		
		// Email
		responsibleParty.setResponsiblePartyEmailAddress(inputData.getPrefferedEmail());
		
		// Location
		responsibleParty.setStreetName1(inputData.getAddressLine1());
		responsibleParty.setStreetName2(inputData.getAddressLine2());
		responsibleParty.setCityName(inputData.getCity());
		responsibleParty.setState(inputData.getState());
		responsibleParty.setZipCode(inputData.getZip());
		
		carrierPayment.setResponsibleParty(responsibleParty);
		
		// Member Information
		if(!inputData.getEnrolleePaymentDtoList().isEmpty() && inputData.getEnrolleePaymentDtoList().size() > 0){
			List<EnrolleePaymentDTO> enrolleePaymentList = inputData.getEnrolleePaymentDtoList();
			for (EnrolleePaymentDTO enrolleePaymentDTO : enrolleePaymentList) {
				if(enrolleePaymentDTO != null){
					MemberInformation memberInfo = new MemberInformation();
					memberInfo.setFirstName(enrolleePaymentDTO.getFirstName());
					memberInfo.setLastName(enrolleePaymentDTO.getLastName());
					memberInfo.setMiddleName(enrolleePaymentDTO.getMiddleName());
					memberInfo.setRelationshipCode(enrolleePaymentDTO.getRelationshipCode());
					memberInfo.setAge(String.valueOf(enrolleePaymentDTO.getAge()));
					memberInfo.setMemberID(enrolleePaymentDTO.getMemberId());
					memberInfo.setTotalMemberPremiumAmount(FinancialMgmtGenericUtil.getBigDecimalObj(enrolleePaymentDTO.getTotalIndvResponsibilityAmt()));
					carrierPayment.getMemberInformation().add(memberInfo);
				}
			}
		}
				
		
		customAttributes.put("CARRIER_PAYMENT", carrierPayment);
		return customAttributes;
	}
	
	private Map<String,String> getCustomAttributes(EnrollmentPaymentDTO inputData,String stateCode){
		Map<String, String> customAttributes = new HashMap<String, String>();
		customAttributes.put("Enrollment ID", String.valueOf(inputData.getEnrollmentId()));
		customAttributes.put("Subscriber Identifier", String.valueOf(inputData.getExchgSubscriberIdentifier()));
		customAttributes.put("Market Indicator", "Individual");
		customAttributes.put("Payment Transaction ID", inputData.getPaymentTxnId());
		customAttributes.put("Assigned QHP Identifier", inputData.getCmsPlanId());
		if("PHIX".equalsIgnoreCase(stateCode)
				&& StringUtils.containsIgnoreCase(inputData.getName(), "Anthem")){
			customAttributes.put("Total Amount Owned", String.valueOf(FinancialMgmtGenericUtil.getBigDecimalObj(inputData.getNetPremiumAmt())));
		}else{
			customAttributes.put("Total Amount Owed", String.valueOf(FinancialMgmtGenericUtil.getBigDecimalObj(inputData.getNetPremiumAmt())));
		}
		customAttributes.put("Premium Amount Total", String.valueOf(FinancialMgmtGenericUtil.getBigDecimalObj(inputData.getGrossPremiumAmt())));
		customAttributes.put("APTC Amount", String.valueOf(FinancialMgmtGenericUtil.getBigDecimalObj(inputData.getAptcAmt())));
		customAttributes.put("Proposed Coverage Effective Date", dateFormat.format(inputData.getBenefitEffectiveDate()).toString());
		customAttributes.put("First Name", inputData.getFirstName());
		customAttributes.put("Middle Name", inputData.getMiddleName());
		customAttributes.put("Last Name", inputData.getLastName());
		customAttributes.put("Street Name 1", inputData.getAddressLine1());
		customAttributes.put("Street Name 2", inputData.getAddressLine2());
		customAttributes.put("City Name", inputData.getCity());
		customAttributes.put("State", inputData.getState());
		customAttributes.put("Zip Code", inputData.getZip());
		customAttributes.put("Contact Email Address", inputData.getContactEmail());
		customAttributes.put("Additional Information", inputData.getEnrolleeNames());
		return customAttributes;
	}
	
	/**
	 * Use to decode the SAML2.0 encoded 
	 * response while Payment Redirection.
	 * @param samlEncodedResponse
	 * 					Contains SAML2.0 encoded response
	 * @return Map<String,Object>
	 * @throws KeyStoreException 
	 * @throws CertificateException 
	 * @throws ParseException 
	 */
	public Map<String,Object> decodeSAMLResponse(String samlEncodedResponse)
	{
		LOGGER.info("=============== Inside EmployeePaymentSAMLUtility :: decodeSAMLResponse :: Start =================");
		Map<String,Object> outputData = null;
		boolean isValidated = EmployeePaymentSAMLDecoder.generateDecodedSAML(samlEncodedResponse);
		if (isValidated) {
			outputData = prepareOutputResponseForSAML(samlEncodedResponse);
		}
		LOGGER.info("=============== Inside EmployeePaymentSAMLUtility :: decodeSAMLResponse :: End =================");
		return outputData;
	}

	/**
	 * Use to Marshaling and UnMarshaling Object of SAML2.0 
	 * to decode the response and prepare the 
	 * EnrollmentPaymentDTO for end user
	 * @param samlEncodedResponse
	 * 					Contains SAML2.0 encoded response
	 * @return outputData
	 * 				 {@link Map<String,Object>}
	 * @throws ParseException 
	 */
	private Map<String,Object> prepareOutputResponseForSAML(String samlEncodedResponse) {
		LOGGER.info("=============== Inside EmployeePaymentSAMLUtility :: prepareOutputResponseForSAML :: Start =================");
		Map<String,Object> outputData = new HashMap<String, Object>();
		EnrollmentPaymentDTO responseObject = new EnrollmentPaymentDTO();
		try {
			//Generate Document/Element object from encoded SAML response string
			Document document = EmployeePaymentSAMLDecoder.buildDocumentResponse(samlEncodedResponse);
			Element element = document.getDocumentElement();
			
			//Unmarshall the document
			UnmarshallerFactory unmarshallerFactory = Configuration.getUnmarshallerFactory();
			Unmarshaller unmarshaller = unmarshallerFactory.getUnmarshaller(element);
			XMLObject responseXmlObj = unmarshaller.unmarshall(element);
			
			//Convert it in to SAML Response
			Response response = (Response) responseXmlObj;
			
			//Get all the Attribute present in Assertion levels
			List<Attribute> attributeList = response.getAssertions().get(0).getAttributeStatements().get(0).getAttributes();
			 
			//Set all the Attribute present in Assertion levels
			createEnrollmentPaymentDTO(responseObject, attributeList);
			
			String decodedSAMLXml = convertSAMLResponseToString(element);
			outputData.put(GhixConstants.SAML_RESPONSE, responseObject);
			outputData.put(GhixConstants.SAML_RESPONSE_XML, decodedSAMLXml);
			
		} catch (Exception e) {
			LOGGER.info("Cannot Transform the SAML response to decoded string", e);
		}
		LOGGER.info("=============== Inside EmployeePaymentSAMLUtility :: prepareOutputResponseForSAML :: End =================");
		return outputData;
	}


	/**
	 * Create response Object which holds all the 
	 * SAML2.0 Assertion level Attributes value
	 * @param responseObject
	 * 				 {@link EnrollmentPaymentDTO}
	 * @param attributeList
	 * 				 {@link List<Attribute>}
	 * @throws ParseException 
	 */
	private void createEnrollmentPaymentDTO(EnrollmentPaymentDTO responseObject, List<Attribute> attributeList)throws ParseException {
		
		for (Attribute attribute : attributeList) {
			if ("Enrollment ID".equals(attribute.getName())) {
				responseObject.setEnrollmentId(Integer.valueOf(((XSString)attribute.getAttributeValues().get(0)).getValue()));
			}else if ("Subscriber Identifier".equals(attribute.getName())) {
				responseObject.setExchgSubscriberIdentifier(((XSString)attribute.getAttributeValues().get(0)).getValue());
			}else if ("Assigned QHP Identifier".equals(attribute.getName())) {
				responseObject.setCmsPlanId(((XSString)attribute.getAttributeValues().get(0)).getValue());
			}else if ("Payment Transaction ID".equals(attribute.getName())) {
				responseObject.setPaymentTxnId(((XSString)attribute.getAttributeValues().get(0)).getValue());
			}else if ("Total Amount Owed".equals(attribute.getName())) {
				responseObject.setNetPremiumAmt(Float.valueOf(((XSString)attribute.getAttributeValues().get(0)).getValue()));
			}else if ("Premium Amount Total".equals(attribute.getName())) {
				responseObject.setGrossPremiumAmt(Float.valueOf(((XSString)attribute.getAttributeValues().get(0)).getValue()));
			}else if ("APTC Amount".equals(attribute.getName())) {
				responseObject.setAptcAmt(Float.valueOf(((XSString)attribute.getAttributeValues().get(0)).getValue()));
			}else if ("Proposed Coverage Effective Date".equals(attribute.getName())) {
				responseObject.setBenefitEffectiveDate(dateFormat.parse(((XSString)attribute.getAttributeValues().get(0)).getValue()));
			}else if ("First Name".equals(attribute.getName())) {
				responseObject.setFirstName(((XSString)attribute.getAttributeValues().get(0)).getValue());
			}else if ("Middle Name".equals(attribute.getName())) {
				responseObject.setMiddleName(((XSString)attribute.getAttributeValues().get(0)).getValue());
			}else if ("Last Name".equals(attribute.getName())) {
				responseObject.setLastName(((XSString)attribute.getAttributeValues().get(0)).getValue());
			}else if ("Street Name 1".equals(attribute.getName())) {
				responseObject.setAddressLine1(((XSString)attribute.getAttributeValues().get(0)).getValue());
			}else if ("Street Name 2".equals(attribute.getName())) {
				responseObject.setAddressLine2(((XSString)attribute.getAttributeValues().get(0)).getValue());
			}else if ("City Name".equals(attribute.getName())) {
				responseObject.setCity(((XSString)attribute.getAttributeValues().get(0)).getValue());
			}else if ("State".equals(attribute.getName())) {
				responseObject.setState(((XSString)attribute.getAttributeValues().get(0)).getValue());
			}else if ("Zip Code".equals(attribute.getName())) {
				responseObject.setZip(((XSString)attribute.getAttributeValues().get(0)).getValue());
			}else if ("Contact Email Address".equals(attribute.getName())) {
				responseObject.setContactEmail(((XSString)attribute.getAttributeValues().get(0)).getValue());
			}else if ("Additional Information".equals(attribute.getName())) {
				responseObject.setEnrolleeNames(((XSString)attribute.getAttributeValues().get(0)).getValue());
			}else if ("Market Indicator".equals(attribute.getName())) {
				LOGGER.info("No SAML Attribute Object present to print");
			}
		}
	}

	/**
	 * Transform SAML2.0 Response XML Object to String
	 * @param element
	 * 				 {@link Element}	
	 * @return decodedSAMLXml
	 * 				 {@link String}
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerConfigurationException
	 * @throws TransformerException
	 */
	private String convertSAMLResponseToString(Element element)
			throws TransformerFactoryConfigurationError, TransformerException {
		StringWriter writer = null;
		DOMSource source = new DOMSource(element);
		writer = new StringWriter();
		StreamResult result = new StreamResult(writer);
		TransformerFactory tFactory = TransformerFactory.newInstance();
		Transformer transformer = tFactory.newTransformer();
		transformer.transform(source, result);
		
		return writer.getBuffer().toString();
	}
}
