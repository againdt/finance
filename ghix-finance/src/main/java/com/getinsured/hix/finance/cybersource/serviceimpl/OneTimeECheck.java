/** This Class is mainly aimed at implementing Electronic Check Payment Service  
 *  Using Cybersource Simple Order API
 * 
 * 
 *  Cybersource test server URLs
 * 
 * Test server URL: 		"ics2wstest.ic3.com"
 * 
 *----------------------------------------------------------------------------------
 *Transaction Types
 *
 * AUTHENTICATION   = "ecAuthenticateService_run"
 * DEBIT      		= "ecDebitService_run"  
 * CREDIT	  	    = "ecCreditService_run" 
 */

package com.getinsured.hix.finance.cybersource.serviceimpl;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cybersource.ws.client.Client;
import com.cybersource.ws.client.ClientException;
import com.cybersource.ws.client.FaultException;
import com.getinsured.hix.finance.cybersource.utils.CyberSourceProperties;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceGeneralUtility;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * 
 * @author panda_p
 * @since 20-Nov-2012
 * This class handles the request to create, modify, query and authorize the eCheque. 
 */
public class OneTimeECheck {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(OneTimeECheck.class);

	private static final String PT_CURRENCY = "USD";
	private Properties cybProperties;

	public OneTimeECheck(CyberSourceProperties cyberSourceProperties)
	{
		this.cybProperties = cyberSourceProperties.getCybProperties();
	}
	
	/**
	 * Validates customer information such as name, address, and date of birth
	 * and returns information to you in the
	 * ecAuthenticateReply_checkpointSummary field. Also Provides you with
	 * consumer fraud information to help protect you against fraudulent
	 * transactions. Information is returned to you in the
	 * ecAuthenticateReply_fraudShieldIndicators field.
	 * 
	 * @param payRequest
	 *            - Details to be passed to request
	 * @return reply - HashMap Reply
	 * */

	@SuppressWarnings("rawtypes")
	public Map authenticateAccount(Map payRequest) {
		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;

		request.put("ecAuthenticateService_run", "true");
		request.put("check_secCode", "WEB");
		/*request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, CyberSourceKeyConstants.MERCHANT_CODE);*/
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, (String)payRequest.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY));
		request.put("billTo_firstName", payRequest.get("firstName").toString());
		request.put("billTo_lastName", payRequest.get("lastName").toString());
		request.put("billTo_street1", payRequest.get("street1").toString());
		request.put("billTo_city", payRequest.get("city").toString());
		request.put("billTo_state", payRequest.get("state").toString());
		request.put("billTo_postalCode", payRequest.get("zip").toString());
		request.put("billTo_country", payRequest.get("country").toString());
		if(payRequest.get(FinanceConstants.CONTACT_NUMBER_KEY)!=null){
			  request.put("billTo_phoneNumber", payRequest.get(FinanceConstants.CONTACT_NUMBER_KEY).toString());
		}
		
		try {
			FinanceGeneralUtility.displayMap("AUTHENTICATE CUSTOMER REQUEST:", request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("AUTHENTICATE CUSTOMER REPLY:", reply);
		} catch (ClientException e) {
			LOGGER.error("PCI: Exception caught at authenticateAccount:", e);
		} catch (FaultException e) {
			LOGGER.error("PCI: Exception caught at authenticateAccount:", e);
		}

		return reply;
	}

	/**
	 * This method debits money from customer account for order payment Here
	 * Customer account information has been validated. if valid, then debit
	 * will be processed Here payment processor does not contact to customer
	 * bank, it just performs fraud verification against account info
	 * 
	 * @param payRequest
	 * @return reply
	 */
	@SuppressWarnings("rawtypes")
	public Map debitAccount(Map payRequest, String amount) {
		
		LOGGER.info("========= DebitAccount(-,-) method started  ==========");

		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;

		request.put("ecDebitService_run", "true");
		/*request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, CyberSourceKeyConstants.MERCHANT_CODE);*/
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, (String)payRequest.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY));
		request.put("billTo_city", payRequest.get("city").toString());
		request.put("billTo_country", payRequest.get("country").toString());
		request.put("billTo_email", payRequest.get("email").toString());
		request.put("billTo_firstName", payRequest.get("firstName").toString());
		request.put("billTo_lastName", payRequest.get("lastName").toString());
		request.put("billTo_postalCode", payRequest.get("zip").toString());
		request.put("billTo_state", payRequest.get("state").toString());
		request.put("billTo_street1", payRequest.get("street1").toString());
		request.put("purchaseTotals_currency", PT_CURRENCY);
		request.put("purchaseTotals_grandTotalAmount", amount);
		if(payRequest.get(FinanceConstants.CONTACT_NUMBER_KEY)!=null){
		  request.put("billTo_phoneNumber", payRequest.get(FinanceConstants.CONTACT_NUMBER_KEY).toString());
		}
		
		request.put("check_accountNumber", payRequest.get("accountNumber")
				.toString());
		request.put("check_accountType", payRequest.get("accountType")
				.toString());
		request.put("check_bankTransitNumber", payRequest.get("routingNumber")
				.toString());
		request.put("check_secCode", "WEB");

		try {
			FinanceGeneralUtility.displayMap("CUSTOMER ACCOUNT DEBIT REQUEST:", request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("CUSTOMER ACCOUNT DEBIT REPLY:", reply);
		} catch (ClientException e) {
			LOGGER.error("ClientException caught at debitAccount:", e);
		} catch (FaultException e) {
			LOGGER.error("FaultException caught at debitAccount:", e);
		}

		LOGGER.info("========= DebitAccount(-,-) method "+"request object: "+request+"==================");
		LOGGER.info("========= DebitAccount(-,-) method "+"cybersource reply object: "+reply+"==============");
		LOGGER.info("========= DebitAccount(-,-) method ends successfully  ==========");
		return reply;
	}

	/**
	 * Method creditAccount
	 * @param payRequest Map
	 * @param amount String
	 * @return Map
	 */
	@SuppressWarnings("rawtypes")
	public Map creditAccount(Map payRequest, String amount) {

		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;

		request.put("ecCreditService_run", "true");
		/*request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, CyberSourceKeyConstants.MERCHANT_CODE);*/
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, (String)payRequest.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY));
		request.put("billTo_city", payRequest.get("city").toString());
		request.put("billTo_country", payRequest.get("country").toString());
		request.put("billTo_email", payRequest.get("email").toString());
		request.put("billTo_firstName", payRequest.get("firstName").toString());
		request.put("billTo_lastName", payRequest.get("lastName").toString());
		request.put("billTo_postalCode", payRequest.get("zip").toString());
		request.put("billTo_state", payRequest.get("state").toString());
		request.put("billTo_street1", payRequest.get("street1").toString());
		request.put("purchaseTotals_currency", PT_CURRENCY);
		request.put("purchaseTotals_grandTotalAmount", amount);
		if(payRequest.get(FinanceConstants.CONTACT_NUMBER_KEY)!=null){
			  request.put("billTo_phoneNumber", payRequest.get(FinanceConstants.CONTACT_NUMBER_KEY).toString());
		}

		request.put("check_accountNumber", payRequest.get("accountNumber")
				.toString());
		request.put("check_accountType", payRequest.get("accountType")
				.toString());
		request.put("check_bankTransitNumber", payRequest.get("routingNumber")
				.toString());
		
		/**
		 * Adding CheckSecCode CCD for Carrier's payment HIX-31785
		 */
		if(payRequest.containsKey(CyberSourceKeyConstants.CHECK_SECCODE_KEY) && payRequest.get(CyberSourceKeyConstants.CHECK_SECCODE_KEY) != null)
		{
			request.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, (String)payRequest.get(CyberSourceKeyConstants.CHECK_SECCODE_KEY));
		}
		else
		{
			request.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, "WEB");
		}

		try {
			FinanceGeneralUtility.displayMap("CUSTOMER ACCOUNT CREDIT REQUEST:", request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("CUSTOMER ACCOUNT CREDIT REPLY:", reply);
		} catch (ClientException e) {
			LOGGER.error("PCI: ClientException caught at debitAccount:", e);
		} catch (FaultException e) {
			LOGGER.error("PCI: FaultException caught at debitAccount:", e);
		}

		return reply;
	}

	/**
	 * A void cancels a debit or credit request that you submitted to
	 * CyberSource. A transaction can be voided only if CyberSource has not
	 * already submitted the debit or credit request to your processor.
	 * 
	 * @param requestID
	 * @param orderRequestToken
	 * @return HashMap reply
	 */
	@SuppressWarnings("rawtypes")
	public Map voidAccount(String requestID) {
		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;

		request.put("voidService_run", "true");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, CyberSourceKeyConstants.MERCHANT_CODE);
		request.put("voidService_voidRequestID", requestID);

		try {
			FinanceGeneralUtility.displayMap("ECHECK VOID REQUEST:", request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("ECHECK CARD VOID REPLY:", reply);
		} catch (ClientException e) {
			LOGGER.error("PCI: Exception caught at voidAccount:", e);
		} catch (FaultException e) {
			LOGGER.error("PCI: Exception caught at voidAccount:", e);
		}

		return reply;
	}
	
	/**
	 * @author Sharma_k
	 * @since 03 Sept 2013
	 * @param payRequest
	 * @param amount
	 * @return Map
	 * @throws ClientException
	 * @throws FaultException
	 */
	@SuppressWarnings("rawtypes")
	public Map pciCreditAccount(Map payRequest, String amount)throws GIException 
	{
		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;
		
		request.put(CyberSourceKeyConstants.ECCREDITSERVICE_RUN_KEY, "true");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, (String)payRequest.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY));
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, (String)payRequest.get(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY));
		request.put(CyberSourceKeyConstants.PURCHASETOTALS_GRANDTOTALAMOUNT_KEY, amount);
		if(payRequest.get(CyberSourceKeyConstants.CYBERSOURCE_ECCREDIT_SERVICE_DEBIT_REQUESTID)!=null){
			request.put(CyberSourceKeyConstants.CYBERSOURCE_ECCREDIT_SERVICE_DEBIT_REQUESTID, (String)payRequest.get(CyberSourceKeyConstants.CYBERSOURCE_ECCREDIT_SERVICE_DEBIT_REQUESTID));
		}
		
		/**
		 * Adding CheckSecCode CCD for Carrier's payment HIX-31785
		 */
		if(payRequest.containsKey(CyberSourceKeyConstants.CHECK_SECCODE_KEY) && payRequest.get(CyberSourceKeyConstants.CHECK_SECCODE_KEY) != null)
		{
			request.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, (String)payRequest.get(CyberSourceKeyConstants.CHECK_SECCODE_KEY));
		}
		
		try
		{
			FinanceGeneralUtility.displayMap("PCI CREDITACCOUNT REQUEST:", request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("PCI CREDITACCOUNT REPLY:", reply);
		}
		catch(Exception ex)
		{
			LOGGER.error("PCI: Exception caught at pciCreditAccount: "+ex);
			throw new GIException(ex);
		}
		return reply;
	}
	
	/**
	 * 
	 * @param payRequest
	 * @return Map
	 * @throws GIException
	 */
	@SuppressWarnings("rawtypes")
	public Map pciAuthenticateAccount(Map payRequest)throws GIException 
	{
		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;

		request.put(CyberSourceKeyConstants.ECAUTHENTICATESERVICE_RUN_KEY, "true");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, (String)payRequest.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY));
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, (String)payRequest.get(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY));
		
		try {
			FinanceGeneralUtility.displayMap("AUTHENTICATE CUSTOMER REQUEST:", request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("AUTHENTICATE CUSTOMER REPLY:", reply);
		}
		catch(Exception ex)
		{
			LOGGER.error("PCI: Exception caught at pciAuthenticateAccount: "+ex);
			throw new GIException(ex);
		}

		return reply;
	}
	
	/**
	 * 
	 * @param payRequest
	 * @param amount
	 * @return Map
	 * @throws GIException
	 */
	@SuppressWarnings("rawtypes")
	public Map pciDebitAccount(Map payRequest, String amount)throws GIException
	{
		LOGGER.info("========= DebitAccount(-,-) method started  ==========");

		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;

		request.put(CyberSourceKeyConstants.ECDEBITSERVICE_RUN_KEY, "true");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, (String)payRequest.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY));
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY,(String) payRequest.get(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY));
		request.put(CyberSourceKeyConstants.PURCHASETOTALS_GRANDTOTALAMOUNT_KEY, amount);
		try {
			FinanceGeneralUtility.displayMap("CUSTOMER ACCOUNT DEBIT REQUEST:", request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("CUSTOMER ACCOUNT DEBIT REPLY:", reply);
		}
		catch (Exception ex)
		{
			LOGGER.error("PCI: Exception caught at pciDebitAccount: "+ex);
			throw new GIException(ex); 
		}
		return reply;
	}
}
