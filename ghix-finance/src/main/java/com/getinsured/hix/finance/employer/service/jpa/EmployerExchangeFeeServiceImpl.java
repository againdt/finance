package com.getinsured.hix.finance.employer.service.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.finance.employer.repository.IEmployerExchangeFeesRepository;
import com.getinsured.hix.finance.employer.service.EmployerExchangeFeeService;
import com.getinsured.hix.model.EmployerExchangeFees;
import com.getinsured.hix.model.EmployerExchangeFees.ExchangeFeeTypeCode;
import com.getinsured.hix.model.EmployerInvoices;

/**
 * 
 * @author Sharma_k
 * @since 08th January 2015
 * Service Implementation class for EmployerExchangeFeeService Interface.
 */
@Service("employerExchangeFeeService")
public class EmployerExchangeFeeServiceImpl implements EmployerExchangeFeeService
{
	@Autowired private IEmployerExchangeFeesRepository iEmployerExchangeFeesRepository;

	@Override
	public EmployerExchangeFees saveEmployerExchangeFees(
			EmployerExchangeFees employerExchangeFees) 
	{
		return iEmployerExchangeFeesRepository.save(employerExchangeFees);
	}
	
	@Override
	public List<EmployerExchangeFees> findEmpExchangeFeesByInvoiceId(final int invoiceId)
	{
		EmployerInvoices employerInvoices = new EmployerInvoices();
		employerInvoices.setId(invoiceId);
		
		return iEmployerExchangeFeesRepository.findByEmployerInvoices(employerInvoices);	
	}
	@Override
	public List<EmployerExchangeFees> saveEmployerExchangeFeesList(List<EmployerExchangeFees> employerExchangeFeesList)
	{
		return iEmployerExchangeFeesRepository.save(employerExchangeFeesList);
	}
	
	@Override
	public List<EmployerExchangeFees> findEmpExchangeFeesByInvIdExceptEmpUserFee(final int invoiceId)
	{
		return iEmployerExchangeFeesRepository.findByEmpInvExceptEmpUserFee(invoiceId, ExchangeFeeTypeCode.EMPLOYER_USER_FEE);	
	}

}
