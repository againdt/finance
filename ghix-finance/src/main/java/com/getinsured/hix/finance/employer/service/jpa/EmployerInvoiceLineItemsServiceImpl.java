package com.getinsured.hix.finance.employer.service.jpa;

import java.sql.Date;
import java.util.List;

import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.EnrollmentCurrentMonthDTO;
import com.getinsured.hix.finance.employer.repository.IEmployerInvoiceLineItemsRepository;
import com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService;
import com.getinsured.hix.finance.utils.FinancialMgmtGenericUtil;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Employee;
import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.EmployerInvoiceLineItems.IsActiveEnrollment;
import com.getinsured.hix.model.EmployerInvoiceLineItems.IssuerInvoicesGeneratedFlag;
import com.getinsured.hix.model.EmployerInvoiceLineItems.PaidToIssuer;
import com.getinsured.hix.model.EmployerInvoiceLineItems.Status;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.Issuer;


/**
 */
@Service("EmployerInvoiceLineItemsService")
public class EmployerInvoiceLineItemsServiceImpl implements EmployerInvoiceLineItemsService{
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployerInvoiceLineItemsServiceImpl.class);
	
	@Autowired private IEmployerInvoiceLineItemsRepository employerInvoiceLineItemsRepository;
	
	/**
	 * Method saveEmployerInvoiceLineItems.
	 * @param employerInvoiceLineItems EmployerInvoiceLineItems
	 * @return EmployerInvoiceLineItems
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService#saveEmployerInvoiceLineItems(EmployerInvoiceLineItems)
	 */
	@Override
	@Transactional
	public EmployerInvoiceLineItems saveEmployerInvoiceLineItems(EmployerInvoiceLineItems employerInvoiceLineItems){
		return employerInvoiceLineItemsRepository.saveAndFlush(employerInvoiceLineItems);
	}
	
	/**
	 * Method createEmployerInvoiceLineItemsFromEnrollmentForNormalInvoice.
	 * @param enrollmentCurrentMonthDTO EnrollmentCurrentMonthDTO
	 * @return EmployerInvoiceLineItems
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService#createEmployerInvoiceLineItemsFromEnrollmentForNormalInvoice(EnrollmentCurrentMonthDTO)
	 */
	@Override
	public EmployerInvoiceLineItems createEmployerInvoiceLineItemsFromEnrollmentForNormalInvoice(EnrollmentCurrentMonthDTO enrollmentCurrentMonthDTO){
		EmployerInvoiceLineItems employerInvoiceLineItems = new EmployerInvoiceLineItems();
		
		//employerInvoiceLineItems.setCarrierName(enrollmentCurrentMonthDTO.getIssuer().getName());
		employerInvoiceLineItems.setCarrierName(enrollmentCurrentMonthDTO.getInsurerName());
		Employee employee = new Employee();
		employee.setId(enrollmentCurrentMonthDTO.getEmployeeId());
		employerInvoiceLineItems.setEmployee(employee);
		employerInvoiceLineItems.setEmployer(enrollmentCurrentMonthDTO.getEmployer());
		/*Float employeeContribution = enrollmentCurrentMonthDTO.getEmployeeContribution();
		employeeContribution = (employeeContribution!=null) ? employeeContribution : 0.0f;
		employerInvoiceLineItems.setEmployeeContribution(employeeContribution);*/
		employerInvoiceLineItems.setEmployeeContribution(FinancialMgmtGenericUtil.getBigDecimalObj(enrollmentCurrentMonthDTO.getEmployeeContribution()));
		employerInvoiceLineItems.setEmployerContribution(FinancialMgmtGenericUtil.getBigDecimalObj(enrollmentCurrentMonthDTO.getEmployerContribution()));
		Integer personsCovered = enrollmentCurrentMonthDTO.getNoOfPersons();
		String noOfPersons = (personsCovered!=null) ? personsCovered.toString() : "0" ;
		employerInvoiceLineItems.setPersonsCovered(noOfPersons);
		employerInvoiceLineItems.setPaidStatus(Status.DUE);
		
		/*DateTime dt = TSDateTime.getInstance();
		DateTime dt1 = TSDateTime.getInstance();
		DateTimeFormatter fmt = DateTimeFormat.forPattern("MM-dd-yyyy");
		dt = dt.plusMonths(1).withDayOfMonth(1);
		dt1 = dt.plusMonths(1).withDayOfMonth(1).minusDays(1);*/
		
		DateTime currentDate = TSDateTime.getInstance(); 
		String periodCovered = "";
		if(currentDate.getDayOfMonth()==1){
			DateTime dt = TSDateTime.getInstance();
			DateTime dt1 = TSDateTime.getInstance();
			DateTimeFormatter fmt = DateTimeFormat.forPattern("MM-dd-yyyy");
			dt = dt.withDayOfMonth(1);
			dt1 = dt.plusMonths(1).withDayOfMonth(1).minusDays(1);
			periodCovered = (fmt.print(dt)+" to "+fmt.print(dt1));
		}
		else{
			DateTime dt = TSDateTime.getInstance();
			DateTime dt1 = TSDateTime.getInstance();
			DateTimeFormatter fmt = DateTimeFormat.forPattern("MM-dd-yyyy");
			dt = dt.plusMonths(1).withDayOfMonth(1);
			dt1 = dt.plusMonths(1).withDayOfMonth(1).minusDays(1);
			periodCovered = (fmt.print(dt)+" to "+fmt.print(dt1));
		}
		
		employerInvoiceLineItems.setPeriodCovered(periodCovered);
		
		employerInvoiceLineItems.setPlayType(enrollmentCurrentMonthDTO.getInsuranceType().toString());
		employerInvoiceLineItems.setPolicyNumber(enrollmentCurrentMonthDTO.getGroupPolicyNumber());
		//employerInvoiceLineItems.setIssuer(enrollmentCurrentMonthDTO.getIssuer());
		if(enrollmentCurrentMonthDTO.getIssuerId()!=null) {
			Issuer issuer = new Issuer();
			issuer.setId(enrollmentCurrentMonthDTO.getIssuerId());
			employerInvoiceLineItems.setIssuer(issuer);
		}
		/*employerInvoiceLineItems.setTotalPremium(enrollmentCurrentMonthDTO.getGrossPremiumAmt());
		employerInvoiceLineItems.setRetroAdjustments(0);*/
		employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getBigDecimalObj(enrollmentCurrentMonthDTO.getGrossPremiumAmt()));
		employerInvoiceLineItems.setRetroAdjustments(FinancialMgmtGenericUtil.getDefaultBigDecimal());
		employerInvoiceLineItems.setNetAmount(employerInvoiceLineItems.getTotalPremium());
		employerInvoiceLineItems.setPaidToIssuer(PaidToIssuer.N);
		employerInvoiceLineItems.setPaymentReceived(FinancialMgmtGenericUtil.getDefaultBigDecimal());
		employerInvoiceLineItems.setIsIssuerInvoiceGenerated(IssuerInvoicesGeneratedFlag.N);
		/*Enrollment enrollment = new Enrollment();
		enrollment.setId(enrollmentCurrentMonthDTO.getEnrollmentId());*/
		employerInvoiceLineItems.setEnrollmentId(enrollmentCurrentMonthDTO.getEnrollmentId());
		employerInvoiceLineItems.setEmployerEnrollmentId(enrollmentCurrentMonthDTO.getEmployerEnrollmentId());
		
		return employerInvoiceLineItems;
	}
	/**
	 * Method createEmployerInvoiceLineItemsFromEnrollmentForBinderInvoice.
	 * @param enrollmentCurrentMonthDTO Object<EnrollmentCurrentMonthDTO>
	 * @param noOfGraceDayForBinderInvoice String
	 * @param periodCovered String
	 * @return Object<EmployerInvoiceLineItems>
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService#createEmployerInvoiceLineItemsFromEnrollmentForBinderInvoice(EnrollmentCurrentMonthDTO, String)
	 */
	@Override
	public EmployerInvoiceLineItems createEmployerInvoiceLineItemsFromEnrollmentForBinderInvoice(EnrollmentCurrentMonthDTO enrollmentCurrentMonthDTO,
			String noOfGraceDayForBinderInvoice, String periodCovered)
	{
		LOGGER.info("========= createEmployerInvoiceLineItemsFromEnrollmentForBinderInvoice(-,-) method started with employerid:"+enrollmentCurrentMonthDTO.getEmployeeId()+" ==========");
		EmployerInvoiceLineItems employerInvoiceLineItems = new EmployerInvoiceLineItems();
		
		//employerInvoiceLineItems.setCarrierName(enrollmentCurrentMonthDTO.getIssuer().getName());
		employerInvoiceLineItems.setCarrierName(enrollmentCurrentMonthDTO.getInsurerName());
		Employee employee = new Employee();
		employee.setId(enrollmentCurrentMonthDTO.getEmployeeId());
		employerInvoiceLineItems.setEmployee(employee);
		employerInvoiceLineItems.setEmployer(enrollmentCurrentMonthDTO.getEmployer());
		employerInvoiceLineItems.setEmployeeContribution(FinancialMgmtGenericUtil.getBigDecimalObj(enrollmentCurrentMonthDTO.getEmployeeContribution()));
		employerInvoiceLineItems.setEmployerContribution(FinancialMgmtGenericUtil.getBigDecimalObj(enrollmentCurrentMonthDTO.getEmployerContribution()));
		Integer personsCovered = enrollmentCurrentMonthDTO.getNoOfPersons();
		String noOfPersons = (personsCovered!=null) ? personsCovered.toString() : "0" ;
		LOGGER.info("========= Number of Persons Covered for Employer: "+noOfPersons+" ==========");
		employerInvoiceLineItems.setPersonsCovered(noOfPersons);
		employerInvoiceLineItems.setPaidStatus(Status.DUE);
		//15 days grace period for binder invoice
		employerInvoiceLineItems.setPeriodCovered(periodCovered);
		
		employerInvoiceLineItems.setPlayType(enrollmentCurrentMonthDTO.getInsuranceType().toString());
		employerInvoiceLineItems.setPolicyNumber(enrollmentCurrentMonthDTO.getGroupPolicyNumber());
		//employerInvoiceLineItems.setIssuer(enrollmentCurrentMonthDTO.getIssuer());
		if(enrollmentCurrentMonthDTO.getIssuerId()!=null) {
			Issuer issuer = new Issuer();
			issuer.setId(enrollmentCurrentMonthDTO.getIssuerId());
			employerInvoiceLineItems.setIssuer(issuer);
		}
		employerInvoiceLineItems.setTotalPremium(FinancialMgmtGenericUtil.getBigDecimalObj(enrollmentCurrentMonthDTO.getGrossPremiumAmt()));
		employerInvoiceLineItems.setRetroAdjustments(FinancialMgmtGenericUtil.getDefaultBigDecimal());
		employerInvoiceLineItems.setNetAmount(employerInvoiceLineItems.getTotalPremium());
		employerInvoiceLineItems.setPaidToIssuer(PaidToIssuer.N);
		employerInvoiceLineItems.setPaymentReceived(FinancialMgmtGenericUtil.getDefaultBigDecimal());
		employerInvoiceLineItems.setIsIssuerInvoiceGenerated(IssuerInvoicesGeneratedFlag.N);
		/*Enrollment enrollment = new Enrollment();
		enrollment.setId(enrollmentCurrentMonthDTO.getEnrollmentId());*/
		employerInvoiceLineItems.setEnrollmentId(enrollmentCurrentMonthDTO.getEnrollmentId());
		employerInvoiceLineItems.setEmployerEnrollmentId(enrollmentCurrentMonthDTO.getEmployerEnrollmentId());
		LOGGER.info("========= createEmployerInvoiceLineItemsFromEnrollmentForBinderInvoice(-,-) method end with employerid:"+employerInvoiceLineItems.getEmployee().getId()+" ==========");
		return employerInvoiceLineItems;
	}
	
	/**
	 * Method getInvoiceLineItem.
	 * @param employerInvoices EmployerInvoices
	 * @return List<EmployerInvoiceLineItems>
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService#getInvoiceLineItem(EmployerInvoices)
	 */
	@Override
	@Transactional
	public List<EmployerInvoiceLineItems> getInvoiceLineItem(EmployerInvoices employerInvoices){
		return employerInvoiceLineItemsRepository.findByEmployerInvoices(employerInvoices);
	}
	
	/**
	 * Method findInvoiceLineItemsByIssuerAndPeriodCovered.
	 * @param issuerId int
	 * @param periodCovered String
	 * @return List<EmployerInvoiceLineItems>
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService#findInvoiceLineItemsByIssuerAndPeriodCovered(int, String)
	 */
	@Override
	@Transactional
	public List<EmployerInvoiceLineItems> findInvoiceLineItemsByIssuerAndPeriodCovered(int issuerId, String periodCovered)
	{
		return employerInvoiceLineItemsRepository.findByIssuerAndPeriodCovered(issuerId, periodCovered);
	}
	
	/**
	 * Method getPaidEmployerInvoices.
	 * @param issuerId int
	 * @param startDate Date
	 * @param endDate Date
	 * @param paidToIssuer PaidToIssuer
	 * @return List<EmployerInvoiceLineItems>
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService#getPaidEmployerInvoices(int, Date, Date, PaidToIssuer)
	 */
	@Override
	public List<EmployerInvoiceLineItems> getPaidEmployerInvoices(int issuerId,	Date startDate, Date endDate, PaidToIssuer paidToIssuer)
	{
		return employerInvoiceLineItemsRepository.getPaidEmployerInvoices(startDate, endDate, issuerId, paidToIssuer);
	}
	
	/**
	 * Method getDistinctIssuerFromInvoice.
	 * @param periodCovered String
	 * @return List<Integer>
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService#getDistinctIssuerFromInvoice(String)
	 */
	@Override
	public List<Integer> getDistinctIssuerFromInvoice(String periodCovered)
	{
		return employerInvoiceLineItemsRepository.getDistinctIssuer(periodCovered);
	}
	
	/**
	 * Method updatePaidToIssuerStatus.
	 * @param id int
	 * @param paidToIssuer PaidToIssuer
	 * @return EmployerInvoiceLineItems
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService#updatePaidToIssuerStatus(int, PaidToIssuer)
	 */
	@Override
	@Transactional
	public EmployerInvoiceLineItems updatePaidToIssuerStatus(int id, PaidToIssuer paidToIssuer,AccountUser user)
	{
		EmployerInvoiceLineItems employerInvoiceLineItems = employerInvoiceLineItemsRepository.findOne(id);
		employerInvoiceLineItems.setPaidToIssuer(paidToIssuer);
		employerInvoiceLineItems.setLastUpdatedBy(user);
		return employerInvoiceLineItemsRepository.save(employerInvoiceLineItems);
	}

	/*@Override
	public List<EmployerInvoiceLineItems> findEmpLineItemsWithEnrollment(int issuerId, String periodCovered)
	{
		return employerInvoiceLineItemsRepository.getEmpLineItemsWithEnrollment(issuerId, periodCovered);
	}*/
	
	@Override
	@Transactional
	public int updateIssuerInvoiceGenerated(List<Integer> ids){
	return	employerInvoiceLineItemsRepository.updateIssuerInvoiceGenerated(ids);
	}

	@Override
	public List<EmployerInvoiceLineItems> getPaidEmployerInvoiceItems(
			int issuerId, Date startDate, Date endDate,
			PaidToIssuer paidToIssuer, List<Integer> empInvIds) {
		return employerInvoiceLineItemsRepository.getPaidEmployerInvoiceItems(startDate, endDate, issuerId, paidToIssuer, empInvIds);
	}

	@Override
	public List<Integer> getNonInvoiceGeneratedIssuerId()
	{
		return employerInvoiceLineItemsRepository.getNonInvoiceGeneratedIssuerId();
	}

	@Override
	public List<EmployerInvoiceLineItems> findByIssuerId(int issuerId)
	{
		return employerInvoiceLineItemsRepository.findByIssuerId(issuerId);
	}
	
	/**
	 * Method getInvoiceLineItem.
	 * @param employerInvoices EmployerInvoices
	 * @return List<EmployerInvoiceLineItems>
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService#getInvoiceLineItem(EmployerInvoices)
	 */
	@Override
	@Transactional
	public List<EmployerInvoiceLineItems> findByEmployerInvoicesNIsActiveEnroll(int invoiceId, IsActiveEnrollment isActiveEnrollment){
		return employerInvoiceLineItemsRepository.findByEmployerInvoicesNIsActiveEnroll(invoiceId, isActiveEnrollment);
	}
	
	/**
	 * Method findByInvoiceAndEnrollmentId.
	 * @param int invoiceId
	 * @param int enrollmentId
	 * @return EmployerInvoiceLineItems
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService#findByInvoiceAndEnrollmentId(int,int)
	 */
	@Override
	@Transactional
	public EmployerInvoiceLineItems findByInvoiceAndEnrollmentId(int invoiceId, int enrollmentId){
		return employerInvoiceLineItemsRepository.findByEmployerInvoicesAndEnrollmentId(invoiceId, enrollmentId);
	}

	@Override
	public EmployerInvoiceLineItems findEmpInvoiceLineItemsbyIdAndInvoiceId(int empInvLineItemID, int empInvoiceID)
	{
		return employerInvoiceLineItemsRepository.findByIdAndEmployerInvoiceId(empInvLineItemID, empInvoiceID);
	}

	@Override
	public Page<EmployerInvoiceLineItems> findPageEmployerInvoiceLineItemsByInvoice(
			EmployerInvoices employerInvoices, Pageable pageable) {
		return employerInvoiceLineItemsRepository.findByEmployerInvoices(employerInvoices, pageable);
	}

	@Override
	public long countNonIssuerRemittanceGeneratedLineItems(List<Integer> employerInvoiceLineItemIDList) 
	{
		return employerInvoiceLineItemsRepository.countNonIssuerRemittanceGeneratedLineItems(employerInvoiceLineItemIDList);
	}
	
	/**
	 * Method getInvoiceEnrollmentID.
	 * @param Integer invoiceId
	 * @return List<Integer> enrollmentID
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService#getInvoiceEnrollmentID(Integer invoiceId)
	 */
	@Override
	public List<Long> getInvoiceEnrollmentID(Integer invoiceId){
		return employerInvoiceLineItemsRepository.getInvoiceEnrollmentID(invoiceId);
	}
	
	@Override
	@Transactional
	public int updateCancelStatus(Integer id,AccountUser user){
	return	employerInvoiceLineItemsRepository.updateCancelStatus(id,user);
	}
}	
