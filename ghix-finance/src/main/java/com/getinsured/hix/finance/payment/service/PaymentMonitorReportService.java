package com.getinsured.hix.finance.payment.service;

import java.util.Map;

import com.getinsured.hix.finance.exception.FinanceException;

/**
 * 
 * @author reddy_h
 *
 */
public interface PaymentMonitorReportService 
{
	
	/**
	 * It gives status of payment and also filter the search based on the input search criteria.
	 * @param searchCriteria
	 * @return
	 * @throws FinanceException
	 */
	Map<String, Object>  getPaymentReportDetails(Map<String, Object> searchCriteria) throws FinanceException;
}
