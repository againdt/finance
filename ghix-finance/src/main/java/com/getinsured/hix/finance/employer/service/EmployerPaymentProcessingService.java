package com.getinsured.hix.finance.employer.service;

import java.math.BigDecimal;
import java.util.Map;

import com.getinsured.hix.dto.finance.FinanceResponse;
import com.getinsured.hix.finance.cybersource.utils.CyberSourceKeyException;
import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.EmployerInvoices.InvoiceType;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 */
public interface EmployerPaymentProcessingService{
	
	/**
	 * Method authorizeEmployerCreditCard.
	 * @param pCust Map
	 * @param amount String
	 * @param isPaymentGateway String
	 * @return HashMap
	 * @throws GIException
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	Map authorizeEmployerCreditCard(Map pCust, String amount, String isPaymentGateway)throws GIException;
	
	/**
	 * Method captureEmployerPremiumUsingCreditCard.
	 * @param pCust Map
	 * @param amount String
	 * @param isPaymentGateway String
	 * @return HashMap
	 * @throws GIException
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	Map captureEmployerPremiumUsingCreditCard(Map pCust, String amount, String isPaymentGateway)throws GIException;
	
	/**
	 * Method authorizeEmployerEcheck.
	 * @param pCust Map
	 * @param amount String
	 * @param isPaymentGateway String
	 * @return HashMap
	 * @throws GIException
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
    Map authorizeEmployerEcheck(Map pCust, String amount, String isPaymentGateway)throws GIException;
	
	/**
	 * Method captureEmployerEcheck.
	 * @param pCust Map
	 * @param amount String
	 * @param isPaymentGateway String
	 * @return HashMap
	 * @throws GIException
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	Map captureEmployerEcheck(Map pCust, String amount, String isPaymentGateway)throws GIException;
	
	/**
	 * Method refundUsingCreditCard.
	 * @param pCust Map
	 * @param amount String
	 * @param isPaymentGateway String
	 * @return HashMap
	 * @throws GIException
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
    Map refundUsingCreditCard(Map pCust, String amount, String isPaymentGateway)throws GIException;
	
	/**
	 * Method refundUsingEcheck.
	 * @param pCust Map
	 * @param amount String
	 * @param isPaymentGateway String
	 * @return HashMap
	 * @throws GIException
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	Map refundUsingEcheck(Map pCust, String amount, String isPaymentGateway)throws GIException;
	
		
	/**
	 * Method processPayment.
	 * @param invoiceid int
	 * @param paidAmt float
	 * @param paymenttype PaymentMethods
	 * @return FinanceResponse
	 * @throws CyberSourceKeyException
	 * @throws FinanceException 
	 */
	FinanceResponse processPayment(int invoiceid,float paidAmt,PaymentMethods paymenttype) throws CyberSourceKeyException, FinanceException;
	
	/**
	 * Method refundExcessAmt.
	 * @param refundamt float
	 * @param paymentType PaymentMethods
	 * @param employerInvoices EmployerInvoices
	 * @return int
	 */
//	int refundExcessAmt(float refundamt,PaymentMethods paymentType,EmployerInvoices employerInvoices);
	int refundExcessAmt(BigDecimal refundamt,PaymentMethods paymentType,EmployerInvoices employerInvoices);
	
	/**
	 * Method updateEnrollmentStatusToActive.
	 * @param employerEnrollmentId int
	 * @throws GIException
	 */
	void updateEnrollmentStatusToActive(int employerEnrollmentId)throws GIException;
	
	/**
	 * this method will disable all employers whose paid status is 'DUE' and exceeds the date (sysdate > paymentDueDate+30 days)
	 * @throws GIException
	 */
	void disEnrollEmployers(InvoiceType invoceType) throws FinanceException;
	
	void updateEnrollmentStatus(EmployerInvoices employerInvoices)throws GIException;
	
	int getActiveEmployerEnrollmentId(EmployerInvoices employerInvoices);
	
	void cancelInvoices(int  employerId, int employerEnrollId, String cancelReason);
	
	void triggerEnrollmentStatusChange(String paymentDueDate) throws FinanceException ;
}
