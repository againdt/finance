package com.getinsured.hix.finance.issuer.service.jpa;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.finance.issuer.repository.IIssuerRemittanceRepository;
import com.getinsured.hix.finance.issuer.service.IssuerRemittanceService;
import com.getinsured.hix.finance.utils.FinancialMgmtGenericUtil;
import com.getinsured.hix.model.IssuerRemittance;
import com.getinsured.hix.model.IssuerRemittance.IsActive;
import com.getinsured.hix.model.IssuerRemittance.PaidStatus;

/**
 * This service implementation class is for GHIX-FINANCE module for issuer invoices.
 * This class has various methods to create, modify and query the IssuerInvoices model object. 
 *
 * @author Sharma_k
 * @since 28-Jun-2013
 */
@Service("issuerRemittanceService")
public class IssuerRemittanceServiceImpl implements IssuerRemittanceService
{
	@Autowired private IIssuerRemittanceRepository iIssuerRemittanceRepository;

	/**
	 * Method saveIssuerInvoices.
	 * @param issuerInvoices IssuerInvoices
	 * @return IssuerInvoices
	 * @see com.getinsured.hix.finance.issuer.service.IssuerRemittanceService#saveIssuerInvoices(IssuerInvoices)
	 */
	@Override
	@Transactional
	public IssuerRemittance saveIssuerRemittance(IssuerRemittance issuerRemittance)
	{
		return iIssuerRemittanceRepository.save(issuerRemittance);
	}

	/**
	 * Method updateIssuerInvoiceNumber.
	 * @param invoiceNumber String
	 * @param invoiceId int
	 * @return IssuerInvoices
	 * @see com.getinsured.hix.finance.issuer.service.IssuerRemittanceService#updateIssuerRemittanceNumber(String, int)
	 */
	@Override
	@Transactional
	public IssuerRemittance updateIssuerRemittanceNumber(String remittanceNumber, int invoiceId)
	{
		IssuerRemittance issuerRemittance = iIssuerRemittanceRepository.findOne(invoiceId);
		issuerRemittance.setRemittanceNumber(remittanceNumber);
		return iIssuerRemittanceRepository.save(issuerRemittance);
	}

	/**
	 * Method findIssuerInvoicesById.
	 * @param id int
	 * @return IssuerInvoices
	 * @see com.getinsured.hix.finance.issuer.service.IssuerRemittanceService#findIssuerRemittanceById(int)
	 */
	@Override	
	@Transactional(readOnly=true)
	public IssuerRemittance findIssuerRemittanceById(int id)
	{
		IssuerRemittance issuerRemittance = null;
		if(iIssuerRemittanceRepository.exists(id))
		{
			issuerRemittance = iIssuerRemittanceRepository.findOne(id);
		}
		return issuerRemittance;
	}

	/**
	 * Method getUnpaidIssuerInvoices.
	 * @return List<IssuerInvoices>
	 * @see com.getinsured.hix.finance.issuer.service.IssuerRemittanceService#getUnpaidIssuerInvoices(Date, Date)
	 */
	@Override
	@Transactional(readOnly=true)
	public List<IssuerRemittance> getUnpaidIssuerRemittance() {
		return iIssuerRemittanceRepository.getUnpaidIssuerRemittance();
	}

	/**
	 * Method updateIssuerPaymentStatus.
	 * @param invoiceId int
	 * @param paidStatus PaidStatus
	 * @param totalPaymentpaid float
	 * @param amtReceivedFrmEmployer float
	 * @return IssuerInvoices
	 * @see com.getinsured.hix.finance.issuer.service.IssuerRemittanceService#updateIssuerPaymentStatus(int, PaidStatus, float, float)
	 */
	@Override
	public IssuerRemittance updateIssuerPaymentStatus(int invoiceId, PaidStatus paidStatus, float totalPaymentpaid, float amtReceivedFrmEmployer)
	{
		IssuerRemittance issuerRemittance = iIssuerRemittanceRepository.findOne(invoiceId);
		issuerRemittance.setPaidStatus(paidStatus);
		issuerRemittance.setTotalPaymentPaid(FinancialMgmtGenericUtil.getBigDecimalObj(totalPaymentpaid));
		issuerRemittance.setAmountReceivedFromEmployer(FinancialMgmtGenericUtil.getBigDecimalObj(amtReceivedFrmEmployer));
		return iIssuerRemittanceRepository.save(issuerRemittance);
	}

	@Override
	public IssuerRemittance updateIssuerPaymentStatus(int invoiceId, PaidStatus paidStatus, BigDecimal totalPaymentpaid,
			BigDecimal amtReceivedFrmEmployer) {
		IssuerRemittance issuerRemittance = iIssuerRemittanceRepository.findOne(invoiceId);
		issuerRemittance.setPaidStatus(paidStatus);
		issuerRemittance.setTotalPaymentPaid(totalPaymentpaid);
		issuerRemittance.setAmountReceivedFromEmployer(amtReceivedFrmEmployer);
		return iIssuerRemittanceRepository.save(issuerRemittance);
	}

	/**
	 * Method findIssuerInvoicesByIssuerId.
	 * @param issuerId int
	 * @return IssuerInvoices
	 * @see com.getinsured.hix.finance.issuer.service.IssuerRemittanceService#findIssuerRemittanceByIssuerId(int)
	 */
	@Override
	public IssuerRemittance findIssuerRemittanceByIssuerId(int issuerId)
	{
		return iIssuerRemittanceRepository.findIssuerRemittanceByIssuerId(issuerId);
	}

	/**
	 * Method updateIssuerInvoicesActiveStatus.
	 * @param invoiceId int
	 * @param isActive IsActive
	 * @return IssuerInvoices
	 * @see com.getinsured.hix.finance.issuer.service.IssuerRemittanceService#updateIssuerRemittanceActiveStatus(int, IsActive)
	 */
	@Override
	public IssuerRemittance updateIssuerRemittanceActiveStatus(int invoiceId, IsActive isActive)
	{
		IssuerRemittance issuerRemittance = iIssuerRemittanceRepository.findOne(invoiceId);
		issuerRemittance.setIsActive(isActive);
		return iIssuerRemittanceRepository.save(issuerRemittance);
	}

	/**
	 * Method findPreviousIssuerInvoices.
	 * @param issuerId int
	 * @return IssuerInvoices
	 * @see com.getinsured.hix.finance.issuer.service.IssuerRemittanceService#findPreviousIssuerRemittance(int)
	 */
	@Override
	public IssuerRemittance findPreviousIssuerRemittance(int issuerId)
	{
		return iIssuerRemittanceRepository.findLastInvoice(issuerId);
	}

	@Override
	public List<IssuerRemittance> findInProcessInvoiceForReconcilation(PaidStatus paidStatus, java.util.Date paidDate)
	{
		return iIssuerRemittanceRepository.findInProcessInvoiceForReconcilation(paidStatus, paidDate);
	}
}
