/** This Class is mainly aimed at implementing Credit Card Payment Service  
 *  Using Cybersource Simple Order API
 * 
 * 
 *  Cybersource test and production server URLs and namespace URI
 * 
 * Test server URL: 		"https://ics2wstest.ic3.com/commerce/1.x/transactionProcessor/"
 * Production server URL: 	"https://ics2ws.ic3.com/commerce/1.x/transactionProcessor/"
 * Namespace URI:			"urn:schemas-cybersource-com:transaction-data-1.18"
 *
 *----------------------------------------------------------------------------------
 *Transaction Types
 * AUTHORIZE  = "ccAuthService_run"
 * CREDIT     = "ccCreditService_run"
 * VOID       = "voidService_run"
 * CAPTURE    = "ccCaptureService_run"
 */

package com.getinsured.hix.finance.cybersource.serviceimpl;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cybersource.ws.client.Client;
import com.cybersource.ws.client.ClientException;
import com.cybersource.ws.client.FaultException;
import com.getinsured.hix.finance.cybersource.utils.CyberSourceProperties;
import com.getinsured.hix.finance.utils.FinanceGeneralUtility;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;

/**
 * 
 * @author panda_p
 * @since 20-Nov-2012
 * This class handles the request to create, modify, query and authorize the credit card. 
 */
public class OneTimeCreditCard {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(OneTimeCreditCard.class);

	private static final String PT_CURRENCY = "USD";
	
	private Properties cybProperties;
	
	public OneTimeCreditCard(CyberSourceProperties cyberSourceProperties)
	{
		this.cybProperties = cyberSourceProperties.getCybProperties();
	}

	/**
	 * This method authorizes the credit card and its available credit No Actual
	 * Money transaction is done here
	 * 
	 * @param payRequest
	 *            - Details to be passed to request
	 * @return reply - HashMap Reply
	 * */

	@SuppressWarnings("rawtypes")
	public Map authorizeCard(Map payRequest, String amount) 
	{

		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;
		System.out.println("inside authrize card");
		request.put("ccAuthService_run", "true");
		/*request.put("merchantReferenceCode", "MRC-14344");*/
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, (String)payRequest.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY));
		request.put("billTo_firstName", payRequest.get("firstName").toString());
		request.put("billTo_lastName", payRequest.get("lastName").toString());
		request.put("billTo_street1", payRequest.get("street1").toString());
		request.put("billTo_city", payRequest.get("city").toString());
		request.put("billTo_state", payRequest.get("state").toString());
		request.put("billTo_postalCode", payRequest.get("zip").toString());
		request.put("billTo_country", payRequest.get("country").toString());
		request.put("billTo_email", payRequest.get("email").toString());
		request.put("purchaseTotals_currency", PT_CURRENCY);
		request.put("purchaseTotals_grandTotalAmount", amount);
		if(payRequest.get("contactnumber")!=null){
			  request.put("billTo_phoneNumber", payRequest.get("contactnumber").toString());
		}
		request.put("card_accountNumber", payRequest.get("cardNumber")
				.toString());
		request.put("card_cardType", payRequest.get("cardType").toString());
		request.put("card_expirationMonth", payRequest.get("expirationMonth")
				.toString());
		request.put("card_expirationYear", payRequest.get("expirationYear")
				.toString());

		try {
			String path = cybProperties.getProperty("keysDirectory");
			System.out.println("keysDirectory "+path);
			FinanceGeneralUtility.displayMap("CREDIT CARD AUTHORIZATION REQUEST:", request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("CREDIT CARD AUTHORIZATION REPLY:", reply);
		} catch (ClientException e) {
			System.out.println("request ClientException "+e);
			LOGGER.error("PCI: Exception caught at authorizeCard: ", e);
		} catch (FaultException e) {
			System.out.println("request FaultException "+e);
			LOGGER.error("PCI: Exception caught at authorizeCard:", e);
		}

		return reply;

	}

	/**
	 * Runs Credit Card Capture, i.e. captures the order means card to merchant
	 * account money transaction is done Actual Transaction of money from Credit
	 * Card to merchant Account has been done here ITS A FOLLOW ON TRANSACTION,
	 * which requires requestid from authorization to process
	 * 
	 * @param authRequestId
	 *            - requestID from previous authorization
	 * @param amount
	 *            - BigDecimal amount to be processed
	 * @return reply - HashMap of actual Reply
	 * */
	@SuppressWarnings("rawtypes")
	public Map captureCard(String authRequestId, String amount, String merchantReferenceCode) {

		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;

		request.put("ccCaptureService_run", "true");
		request.put("merchantReferenceCode", merchantReferenceCode);
		request.put("ccCaptureService_authRequestID", authRequestId);
		request.put("purchaseTotals_currency", PT_CURRENCY);
		request.put("purchaseTotals_grandTotalAmount", amount);

		try {
			FinanceGeneralUtility.displayMap("FOLLOW-ON CAPTURE REQUEST:", request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("FOLLOW-ON CAPTURE REPLY:", reply);
		} catch (ClientException e) {
			LOGGER.error("PCI: Exception caught at captureCard:", e);
		} catch (FaultException e) {
			LOGGER.error("PCI: Exception caught at captureCard:", e);
		}

		return reply;
	}

	/**
	 * Credit money from merchants account into customer credit card. When your
	 * request for a credit is successful, the issuing bank for the credit card
	 * takes money out of your merchant bank account and returns it to the
	 * customer. It usually takes two to four days for your acquiring bank to
	 * transfer funds from your merchant bank account.
	 * 
	 * Actual Transaction of money from Merchant Account to Credit Card has been
	 * done here
	 * 
	 * @param orderRequestToken
	 * @param payRequest
	 * @return Map	 
	 */
	@SuppressWarnings("rawtypes")
	public Map creditToCard(Map payRequest, String amount) {
		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;

		request.put("ccCreditService_run", "true");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, (String)payRequest.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY));
		request.put("billTo_firstName", payRequest.get("firstName").toString());
		request.put("billTo_lastName", payRequest.get("lastName").toString());
		request.put("billTo_street1", payRequest.get("street1").toString());
		request.put("billTo_city", payRequest.get("city").toString());
		request.put("billTo_state", payRequest.get("state").toString());
		request.put("billTo_postalCode", payRequest.get("zip").toString());
		request.put("billTo_country", payRequest.get("country").toString());
		request.put("billTo_email", payRequest.get("email").toString());
		request.put("purchaseTotals_currency", PT_CURRENCY);
		request.put("purchaseTotals_grandTotalAmount", amount);

		request.put("card_accountNumber", payRequest.get("cardNumber")
				.toString());
		request.put("card_cardType", payRequest.get("cardType").toString());
		request.put("card_expirationMonth", payRequest.get("expirationMonth")
				.toString());
		request.put("card_expirationYear", payRequest.get("expirationYear")
				.toString());
		
		/**
		 * Adding CheckSecCode CCD for Carrier's payment HIX-31785
		 */
		if(payRequest.containsKey(CyberSourceKeyConstants.CHECK_SECCODE_KEY) && payRequest.get(CyberSourceKeyConstants.CHECK_SECCODE_KEY) != null)
		{
			request.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, (String)payRequest.get(CyberSourceKeyConstants.CHECK_SECCODE_KEY));
		}

		try {
			FinanceGeneralUtility.displayMap("CREDIT CARD CREDIT MONEY REQUEST:", request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("CREDIT CARD CREDIT MONEY REPLY:", reply);
		} catch (ClientException e) {
			LOGGER.error("PCI: Exception caught at creditToCard:", e);
		} catch (FaultException e) {
			LOGGER.error("PCI: Exception caught at creditToCard:", e);
		}

		return reply;
	}

	
	
	/**
	 * 
	 * @param payRequest
	 * @param amount
	 * @return Map
	 */
	@SuppressWarnings("rawtypes")
	public Map pciAuthorizeCard(Map payRequest, String amount) {

		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;

		request.put(CyberSourceKeyConstants.CCAUTHSERVICE_RUN_KEY, "true");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, (String)payRequest.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY));
		request.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, PT_CURRENCY);
		request.put(CyberSourceKeyConstants.PURCHASETOTALS_GRANDTOTALAMOUNT_KEY, amount);
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY,(String)payRequest.get(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY));
		try {
			FinanceGeneralUtility.displayMap("CREDIT CARD AUTHORIZATION REQUEST:", request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("CREDIT CARD AUTHORIZATION REPLY:", reply);
		} catch (ClientException e) {
			LOGGER.error("PCI: Exception caught at pciAuthorizeCard:", e);
		} catch (FaultException e) {
			LOGGER.error("PCI: Exception caught at pciAuthorizeCard:", e);
		}
		return reply;
	}
	
	/**
	 * Method pciCreditToCard
	 * @param payRequest
	 * @param amount
	 * @return Map
	 */
	@SuppressWarnings("rawtypes")
	public Map pciCreditToCard(Map payRequest, String amount) {
		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;

		request.put(CyberSourceKeyConstants.CCCREDITSERVICE_RUN_KEY, "true");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, (String)payRequest.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY));
		request.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, PT_CURRENCY);
		request.put(CyberSourceKeyConstants.PURCHASETOTALS_GRANDTOTALAMOUNT_KEY, amount);
		request.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY,(String)payRequest.get(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY));
		/**
		 * Adding CheckSecCode CCD for Carrier's payment HIX-31785
		 */
		if(payRequest.containsKey(CyberSourceKeyConstants.CHECK_SECCODE_KEY) && payRequest.get(CyberSourceKeyConstants.CHECK_SECCODE_KEY) != null)
		{
			request.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, (String)payRequest.get(CyberSourceKeyConstants.CHECK_SECCODE_KEY));
		}
		try {
			FinanceGeneralUtility.displayMap("CREDIT CARD CREDIT MONEY REQUEST:", request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("CREDIT CARD CREDIT MONEY REPLY:", reply);
		} catch (ClientException e) {
			LOGGER.error("PCI: Exception caught at pciCreditToCard:", e);
		} catch (FaultException e) {
			LOGGER.error("PCI: Exception caught at pciCreditToCard:", e);
		}

		return reply;
	}
	
	/**
	 * Method pciCaptureCard
	 * @param authRequestId
	 * @param amount
	 * @param merchantCode
	 * @return Map
	 */
	@SuppressWarnings("rawtypes")
	public Map pciCaptureCard(String authRequestId, String amount, String merchantCode) {

		Map<String, String> request = new HashMap<String, String>();
		Map reply = null;

		request.put(CyberSourceKeyConstants.CCCAPTURESERVICE_RUN_KEY, "true");
		request.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, merchantCode);
		request.put(CyberSourceKeyConstants.CCCAPTURESERVICE_AUTHREQUESTID_KEY, authRequestId);
		request.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, PT_CURRENCY);
		request.put(CyberSourceKeyConstants.PURCHASETOTALS_GRANDTOTALAMOUNT_KEY, amount);

		try {
			FinanceGeneralUtility.displayMap("FOLLOW-ON CAPTURE REQUEST:", request);
			reply = Client.runTransaction(request, cybProperties);
			FinanceGeneralUtility.displayMap("FOLLOW-ON CAPTURE REPLY:", reply);
		} catch (ClientException e) {
			LOGGER.error("PCI: Exception caught at pciCaptureCard:", e);
		} catch (FaultException e) {
			LOGGER.error("PCI: Exception caught at pciCaptureCard:", e);
		}

		return reply;
	}
}
