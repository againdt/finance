package com.getinsured.hix.finance.utils;

/**
 * Class for finance module error codes. 
 * @author Sharma_k
 * @since 24th June, 2013
 */
public class FinanceErrorCodes {

		/**
		 * Enum to set the error code for Exception handling
		 */
		public enum ERRORCODE {
			SNPEIN(FinanceConstants.NUMERICAL_20000),SEIN(FinanceConstants.NUMERICAL_20001),SPEIN(FinanceConstants.NUMERICAL_20002),
			SEIBYID(FinanceConstants.NUMERICAL_20003),SDEIN(FinanceConstants.NUMERICAL_20004),SAEIN(FinanceConstants.NUMERICAL_20005),
			SEPINBYID(FinanceConstants.NUMERICAL_20006), CEPDF(FinanceConstants.NUMERICAL_20007),REINE(FinanceConstants.NUMERICAL_20008),
			PRPAY(FinanceConstants.NUMERICAL_20009),SPADE(FinanceConstants.NUMERICAL_20010),SPABYMER(FinanceConstants.NUMERICAL_20011), 
			SPADINV(FinanceConstants.NUMERICAL_20012), SAMLERR(FinanceConstants.NUMERICAL_20020),INVERR(FinanceConstants.NUMERICAL_20021),
			FIND_PAY_METHOD_BY_PCI(FinanceConstants.NUMERICAL_20022),SAVE_PAYMENT_METHOD(FinanceConstants.NUMERICAL_20023),
			MODIFY_PAYMENT_STATUS_SETTING(FinanceConstants.NUMERICAL_20024),MIGRATE_PAYMENT_METHOD(FinanceConstants.NUMERICAL_20025), 
			VALIDATE_CREDITCARD(FinanceConstants.NUMERICAL_20026), GET_DEFLT_PAY_BY_MODULE_ID_NAME(FinanceConstants.NUMERICAL_20027), 
			GET_ACTIVE_PAY_BY_MODULE_ID_NAME(FinanceConstants.NUMERICAL_20028), RETRIEVE_CUST_PROFILE(FinanceConstants.NUMERICAL_20029), 
			FIND_PAY_METHOD_BY_ID_MODULE_ID_NAME(FinanceConstants.NUMERICAL_20027);

			private final int code;

			private ERRORCODE(int cd) {
				code = cd;
			}

			public int getCode() {
				return code;
			}
		}
}
