package com.getinsured.hix.finance.employer.service.jpa;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.finance.employer.repository.IEmployerPaymentInvoiceRepository;
import com.getinsured.hix.finance.employer.service.EmployerPaymentInvoiceService;
import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.EmployerPaymentInvoice;
import com.getinsured.hix.model.EmployerPaymentInvoice.IsActive;

/**
 * This service implementation class is for GHIX-FINANCE module for employer payment invoice.
 * This class has various methods to create/modify the EmployerPaymentInvoice model object. 
 * @author sharma_k
 * @since 28-Jun-2013
 */
@Service("EmployerPaymentInvoiceService")
public class EmployerPaymentInvoiceServiceImpl implements EmployerPaymentInvoiceService{
	
	@Autowired private IEmployerPaymentInvoiceRepository employerPaymentInvoiceRepository;
	
	/**
	 * Method saveEmployerPaymentInvoice.
	 * @param employerPaymentInvoice EmployerPaymentInvoice
	 * @return EmployerPaymentInvoice
	 * @see com.getinsured.hix.finance.employer.service.EmployerPaymentInvoiceService#saveEmployerPaymentInvoice(EmployerPaymentInvoice)
	 */
	@Override
	@Transactional
	public EmployerPaymentInvoice saveEmployerPaymentInvoice(EmployerPaymentInvoice employerPaymentInvoice){
		return employerPaymentInvoiceRepository.save(employerPaymentInvoice);
	}
	
	/**
	 * Method findEmployerPaymentInvoiceById.
	 * @param paymentid int
	 * @return EmployerPaymentInvoice
	 * @see com.getinsured.hix.finance.employer.service.EmployerPaymentInvoiceService#findEmployerPaymentInvoiceById(int)
	 */
	@Override	
	@Transactional(readOnly=true)
	public EmployerPaymentInvoice findEmployerPaymentInvoiceById(int paymentid)throws FinanceException {
		EmployerPaymentInvoice employerPaymentInvoice = null;
		if( employerPaymentInvoiceRepository.exists(paymentid) ){
			employerPaymentInvoice = employerPaymentInvoiceRepository.findOne(paymentid);
		}
		return employerPaymentInvoice;
	}
	
	/**
	 * Method findPaymentInvoiceByEmployerInvoices.
	 * @param employerInvoices EmployerInvoices
	 * @return List<EmployerPaymentInvoice>
	 * @see com.getinsured.hix.finance.employer.service.EmployerPaymentInvoiceService#findPaymentInvoiceByEmployerInvoices(EmployerInvoices)
	 */
	@Override
	public List<EmployerPaymentInvoice> findPaymentInvoiceByEmployerInvoices(EmployerInvoices employerInvoices){
		return employerPaymentInvoiceRepository.findPaymentInvoiceByEmployerInvoices(employerInvoices);
	}
	
	@Override
	public EmployerPaymentInvoice updateEmployerPaymentInvoice(int employerPaymentInvId, IsActive isActive) 
	{
		EmployerPaymentInvoice employerPaymentInvoice = employerPaymentInvoiceRepository.findOne(employerPaymentInvId);
		employerPaymentInvoice.setIsActive(isActive);
		return employerPaymentInvoiceRepository.save(employerPaymentInvoice);
	}

	@Override
	public EmployerPaymentInvoice findActiveEmployerPaymentInvoice(EmployerInvoices employerInvoices) 
	{
		return employerPaymentInvoiceRepository.findActiveEmployerPaymentInvoice(employerInvoices.getId());
	}
}	
