package com.getinsured.hix.finance.bank.service;

import java.util.Date;
import java.util.List;

public interface BankHolidayService 
{
	/**
	 * 
	 * @param year
	 * @return
	 * Find the count of Bank holiday 
	 */
	 long findNoOfBankHolidaysByYear(String year);
	
	 List<Date> findBankHolidaysBetween(Date startDate, Date endDate);

}
