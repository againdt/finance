package com.getinsured.hix.finance.employer.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.EmployerInvoiceLineItems.IsActiveEnrollment;
import com.getinsured.hix.model.EmployerInvoiceLineItems.PaidToIssuer;
import com.getinsured.hix.model.EmployerInvoices;


/**
 */
public interface IEmployerInvoiceLineItemsRepository extends JpaRepository<EmployerInvoiceLineItems, Integer>{

	/**
	 * Method findByEmployerInvoices.
	 * @param employerInvoices EmployerInvoices
	 * @return List<EmployerInvoiceLineItems>
	 */
	List<EmployerInvoiceLineItems> findByEmployerInvoices(EmployerInvoices employerInvoices);

	/**
	 * Method findByEmployerInvoices.
	 * @param employerInvoices EmployerInvoices
	 * @return List<EmployerInvoiceLineItems>
	 */
	@Query("FROM EmployerInvoiceLineItems as an where an.employerInvoices.id= :invoiceId and an.isActiveEnrollment= :isActiveEnrollment")
	List<EmployerInvoiceLineItems> findByEmployerInvoicesNIsActiveEnroll(@Param("invoiceId") int invoiceId,@Param("isActiveEnrollment") IsActiveEnrollment isActiveEnrollment);

	/**
	 * Method findByIssuerAndPeriodCovered.
	 * @param issuerId int
	 * @param periodCovered String
	 * @return List<EmployerInvoiceLineItems>
	 */
	/*@Query("SELECT emplin FROM EmployerInvoiceLineItems empLin, EmployerInvoices empInv"
			+ " where empLin.employerInvoices.id = empInv.id and empLin.issuer.id = :issuerId and"
			+ " empLin.periodCovered = :periodCovered and empInv.issuerInvoiceGenerated = 'N'")
	List<EmployerInvoiceLineItems> findByIssuerAndPeriodCovered(@Param("issuerId") int issuerId,@Param("periodCovered") String periodCovered);*/

	@Query("FROM EmployerInvoiceLineItems as an where an.issuer.id = :issuerId and an.periodCovered = :periodCovered and an.isIssuerInvoiceGenerated = 'N' and an.paidStatus in ('PAID', 'PARTIALLY_PAID')")
	List<EmployerInvoiceLineItems> findByIssuerAndPeriodCovered(@Param("issuerId") int issuerId,@Param("periodCovered") String periodCovered);

	/**
	 * @since 10th Feb 2014
	 * @param issuerId
	 * @param periodCoveredList
	 * @return
	 */
	@Query("FROM EmployerInvoiceLineItems as an where an.issuer.id = :issuerId and an.isIssuerInvoiceGenerated = 'N' and an.paidStatus in ('PAID', 'PARTIALLY_PAID')")
	List<EmployerInvoiceLineItems> findByIssuerId(@Param("issuerId") int issuerId);

	/**
	 * Method getPaidEmployerInvoices.
	 * @param startDate Date
	 * @param endDate Date
	 * @param issuerId int
	 * @param paidToIssuer PaidToIssuer
	 * @return List<EmployerInvoiceLineItems>
	 */
	@Query("FROM EmployerInvoiceLineItems as an where an.paidToIssuer = :paidToIssuer and an.issuer.id = :issuerId and trunc(an.paidDate) >= :startDate and trunc(an.paidDate) <= :endDate and an.paidStatus IN ('PAID','PARTIALLY_PAID')")
	List<EmployerInvoiceLineItems> getPaidEmployerInvoices(
			@Param("startDate") Date startDate, @Param("endDate") Date endDate,@Param("issuerId") int issuerId, @Param("paidToIssuer")PaidToIssuer paidToIssuer);

	@Query("FROM EmployerInvoiceLineItems as an where an.paidToIssuer = :paidToIssuer and an.issuer.id = :issuerId and trunc(an.paidDate) >= :startDate and trunc(an.paidDate) <= :endDate and an.paidStatus IN ('PAID','PARTIALLY_PAID') and an.id in (:empInvIds)")
	List<EmployerInvoiceLineItems> getPaidEmployerInvoiceItems(
			@Param("startDate") Date startDate, @Param("endDate") Date endDate,@Param("issuerId") int issuerId, @Param("paidToIssuer")PaidToIssuer paidToIssuer, @Param("empInvIds") List<Integer> empInvIds);

	/**
	 * Method getDistinctIssuer.
	 * @param periodCovered String
	 * @return List<Integer>
	 */
	@Query("SELECT DISTINCT e.issuer.id FROM EmployerInvoiceLineItems e, Issuer i  where e.issuer.id = i.id and e.periodCovered = :periodCovered")
	List<Integer> getDistinctIssuer(@Param("periodCovered") String periodCovered);

	/**
	 * @since 10th Feb 2014 
	 * @return
	 */
	@Query("SELECT DISTINCT e.issuer.id FROM EmployerInvoiceLineItems e, Issuer i WHERE e.issuer.id = i.id "
			+ "AND e.paidStatus IN ('PAID','PARTIALLY_PAID') AND e.isIssuerInvoiceGenerated = 'N' ")
	List<Integer> getNonInvoiceGeneratedIssuerId();


	/*@Query("SELECT lineItems FROM EmployerInvoiceLineItems as lineItems "+
			"inner join fetch lineItems.enrollment as enrollment "+
			"where lineItems.issuer.id = :issuerId and lineItems.periodCovered = :periodCovered and lineItems.employerInvoices.isActive='Y' and lineItems.paidToIssuer='N'")
	List<EmployerInvoiceLineItems> getEmpLineItemsWithEnrollment(@Param("issuerId") int issuerId,@Param("periodCovered") String periodCovered);*/

	@Modifying
	@Transactional(readOnly=false)
	@Query("update EmployerInvoiceLineItems e set e.isIssuerInvoiceGenerated = 'Y' where e.id in (:ids)")
	int updateIssuerInvoiceGenerated(@Param("ids") List<Integer> ids);

	/**
	 * Method findByEmployerInvoicesAndEnrollmentId.
	 * @param int invoiceId
	 * @param int enrollmentId
	 * @return EmployerInvoiceLineItems
	 */
	@Query("FROM EmployerInvoiceLineItems as an where an.employerInvoices.id= :invoiceId and an.enrollmentId= :enrollmentId")
	EmployerInvoiceLineItems findByEmployerInvoicesAndEnrollmentId(@Param("invoiceId")int invoiceId, @Param("enrollmentId")int enrollmentId);

	/**
	 * 
	 * @param empInvLineItemID
	 * @param empInvoiceID
	 * @return
	 */
	@Query("FROM EmployerInvoiceLineItems as empLin WHERE empLin.id = :empInvLineItemID AND empLin.employerInvoices.id= :empInvoiceID")
	EmployerInvoiceLineItems findByIdAndEmployerInvoiceId(@Param("empInvLineItemID")int empInvLineItemID, @Param("empInvoiceID")int empInvoiceID);

	Page<EmployerInvoiceLineItems> findByEmployerInvoices(EmployerInvoices employerInvoices, Pageable pageable);

	/**
	 * Count no of PAID | PARTIALLY_PAID EmployerInvoiceLineItems whose Issuer invoice is not generated.
	 * 
	 * @param issuerID
	 * @return
	 */
	@Query("SELECT COUNT(*) FROM EmployerInvoiceLineItems as an where an.id in (:employerInvoiceLineItemIDList)"
			+ " and an.isIssuerInvoiceGenerated = 'N' and an.paidStatus in ('PAID', 'PARTIALLY_PAID')")
	long countNonIssuerRemittanceGeneratedLineItems(@Param("employerInvoiceLineItemIDList") List<Integer> employerInvoiceLineItemIDList);
	
	/**
	 * Method getInvoiceEnrollmentID.
	 * @param invoiceId Integer
	 * @return List<Integer>
	 */
	@Query("SELECT e.enrollmentId FROM EmployerInvoiceLineItems e  where e.employerInvoices.id = :invoiceId")
	List<Long> getInvoiceEnrollmentID(@Param("invoiceId") Integer invoiceId);
	
	@Modifying
	@Transactional(readOnly=false)
	@Query("update EmployerInvoiceLineItems e set e.paidStatus = 'CANCEL', e.lastUpdatedBy = :user where e.employerInvoices.id = :invoiceId ")
	int updateCancelStatus(@Param("invoiceId") Integer invoiceId,@Param("user") AccountUser user);
}
