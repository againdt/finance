package com.getinsured.hix.finance.employer.service;

import java.sql.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.getinsured.hix.dto.enrollment.EnrollmentCurrentMonthDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.EmployerInvoiceLineItems.IsActiveEnrollment;
import com.getinsured.hix.model.EmployerInvoiceLineItems.PaidToIssuer;
import com.getinsured.hix.model.EmployerInvoices;

/**
 */
public interface EmployerInvoiceLineItemsService {

	/**
	 * Method saveEmployerInvoiceLineItems.
	 * @param employerInvoiceLineItems EmployerInvoiceLineItems
	 * @return EmployerInvoiceLineItems
	 */
	EmployerInvoiceLineItems saveEmployerInvoiceLineItems(EmployerInvoiceLineItems employerInvoiceLineItems);
	
	/**
	 * @since 11th September 2014
	 * Method to find EmployerInvoiceLineItem By LineItemID and EmployerInvoiceID
	 * @param empInvLineItemID
	 * @param empInvoiceID
	 * @return
	 */
	EmployerInvoiceLineItems findEmpInvoiceLineItemsbyIdAndInvoiceId(int empInvLineItemID, int empInvoiceID);
	
	/**
	 * Method createEmployerInvoiceLineItemsFromEnrollmentForNormalInvoice.
	 * @param enrollmentCurrentMonthDTO EnrollmentCurrentMonthDTO
	 * @return EmployerInvoiceLineItems
	 */
	EmployerInvoiceLineItems createEmployerInvoiceLineItemsFromEnrollmentForNormalInvoice(EnrollmentCurrentMonthDTO enrollmentCurrentMonthDTO);
	
	/**
	 * Method createEmployerInvoiceLineItemsFromEnrollmentForBinderInvoice.
	 * @param enrollmentCurrentMonthDTO Object<EnrollmentCurrentMonthDTO>
	 * @param noOfGraceDayForBinderInvoice String
	 * @param periodCovered String
	 * @return Object<EmployerInvoiceLineItems>
	 */
	EmployerInvoiceLineItems createEmployerInvoiceLineItemsFromEnrollmentForBinderInvoice(EnrollmentCurrentMonthDTO enrollmentCurrentMonthDTO,
			String noOfGraceDayForBinderInvoice, String periodCovered);
	
	/**
	 * Method getInvoiceLineItem.
	 * @param employerInvoices EmployerInvoices
	 * @return List<EmployerInvoiceLineItems>
	 */
	List<EmployerInvoiceLineItems> getInvoiceLineItem(EmployerInvoices employerInvoices);
	
	/**
	 * Method findInvoiceLineItemsByIssuerAndPeriodCovered.
	 * @param issuerId int
	 * @param periodCovered String
	 * @return List<EmployerInvoiceLineItems>
	 */
	List<EmployerInvoiceLineItems> findInvoiceLineItemsByIssuerAndPeriodCovered(int issuerId, String periodCovered);
	
	/**
	 * @since 11-September-2013
	 * @param issuerId
	 * @param startDate Date
	 * @param endDate Date
	 * @param paidToIssuer PaidToIssuer
	 * @return
	 * Added to implement lazy loading feature. */
	/*List<EmployerInvoiceLineItems> findEmpLineItemsWithEnrollment(int issuerId, String periodCovered);*/
	
	List<EmployerInvoiceLineItems> getPaidEmployerInvoices(int issuerId, Date startDate, Date endDate, PaidToIssuer paidToIssuer);
	
	/**
	 * Method getDistinctIssuerFromInvoice.
	 * @param periodCovered String
	 * @return List<Integer>
	 */
	List<Integer> getDistinctIssuerFromInvoice(String periodCovered);
	
	/**
	 * Method updatePaidToIssuerStatus.
	 * @param id int
	 * @param paidToIssuer PaidToIssuer
	 * @param user AccountUser
	 * @return EmployerInvoiceLineItems
	 */
	EmployerInvoiceLineItems updatePaidToIssuerStatus(int id, PaidToIssuer paidToIssuer, AccountUser user);
	
	int updateIssuerInvoiceGenerated(List<Integer> ids);
	
	List<EmployerInvoiceLineItems> getPaidEmployerInvoiceItems(int issuerId, Date startDate, Date endDate, PaidToIssuer paidToIssuer, List<Integer> empInvIds);
	
	/**
	 * Fetches distinct IssuerId on the basis of Paid Status and IsIssuerInvoiceGenerated
	 * @since 10th Feb 2014
	 * @return
	 */
	List<Integer> getNonInvoiceGeneratedIssuerId();
	
	/**
	 * Fetches all the EmployerInvoiceLineItems based on issuerId 
	 * @since 10th Feb 2014
	 * @param issuerId
	 * @return
	 */
	List<EmployerInvoiceLineItems> findByIssuerId(int issuerId);

	List<EmployerInvoiceLineItems> findByEmployerInvoicesNIsActiveEnroll(int invoiceId, IsActiveEnrollment isActiveEnrollment);
	
	EmployerInvoiceLineItems findByInvoiceAndEnrollmentId(int invoiceId, int enrollmentId);
	
	Page<EmployerInvoiceLineItems> findPageEmployerInvoiceLineItemsByInvoice(EmployerInvoices employerInvoices, Pageable pageable);
	
	/**
	 * Count no of PAID | PARTIALLY_PAID EmployerInvoiceLineItems whose Issuer invoice is not generated.
	 * @param employerInvoiceLineItemIDList List<Integer>
	 * @return
	 */
	long countNonIssuerRemittanceGeneratedLineItems(List<Integer> employerInvoiceLineItemIDList);
	
	/**
	 * Method getInvoiceEnrollmentID.
	 * @param Integer invoiceId
	 * @return List<Long> enrollmentID
	 */
	List<Long> getInvoiceEnrollmentID(Integer invoiceId);
	
	int updateCancelStatus(Integer id,AccountUser user);

}
