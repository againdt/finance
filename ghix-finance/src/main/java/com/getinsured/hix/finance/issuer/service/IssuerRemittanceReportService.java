package com.getinsured.hix.finance.issuer.service;

import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.finance.issuerRemittance.IssuerRemittanceRequest;
import com.getinsured.hix.webservice.finance.issuerRemittance.IssuerRemittanceResponse;

/**
 * 
 * @author Sharma_k
 * @rename since 02nd Feb 2015, for Jira ID: HIX-47375
 */
public interface IssuerRemittanceReportService 
{
	IssuerRemittanceResponse processIssuerRemittanceRequest(IssuerRemittanceRequest issuerRemittanceRequest)throws GIException;
}
