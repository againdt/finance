package com.getinsured.hix.finance.issuer.service;

import com.getinsured.hix.model.ExchgPartnerLookup;

public interface FinanceIssuerExchgPartnerLookupService 
{
	/**
	 * 
	 * @param hiosIssuerID
	 * @param market
	 * @param direction
	 * @param st01
	 * @return
	 */
	ExchgPartnerLookup findExchgPartnerByHiosIssuerID(String hiosIssuerID, String market, String direction, String st01);

}
