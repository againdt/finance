package com.getinsured.hix.finance.issuer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Issuer;

/**
 */
public interface IIssuerRepository extends JpaRepository<Issuer, Integer>
{
	/**
	 * Method getIssuerByHiosIDAndIssuerName.
	 * @param hiosIssuerID String
	 * @param issuerName String
	 * @return Issuer
	 */
	@Query("SELECT i FROM Issuer i WHERE (i.hiosIssuerId = :hiosIssuerID AND lower(i.name) = lower(:issuerName))")
	Issuer getIssuerByHiosIDAndIssuerName (@Param("hiosIssuerID") String hiosIssuerID, @Param("issuerName") String issuerName);
	
	/**
	 * 
	 * @param hiosIssuerID
	 * @return
	 */
	@Query("SELECT i FROM Issuer i WHERE (lower(i.hiosIssuerId) = lower(:hiosIssuerID))")
	Issuer getIssuerByHiosIssuerID (@Param("hiosIssuerID") String hiosIssuerID);
}
