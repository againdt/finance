package com.getinsured.hix.finance.cybersource.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.model.BankInfo;
import com.getinsured.hix.model.CreditCardInfo;
import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.PaymentType;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;
import com.getinsured.hix.platform.payment.util.PaymentUtil;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 *  This class is responsible to map the PaymentMethods object.
 * 
 * @author Sharma_k
 * @since 24-Sep-2013
 */
public class RequestMapper 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(RequestMapper.class);	
	
	/**
	 * Method createCustomerProfileRequest
	 * @param paymentMethod PaymentMethods
	 * @return Map
	 * @throws GIException	 
	 */
	@SuppressWarnings("rawtypes")
	public Map createCustomerProfileRequest(PaymentMethods paymentMethod)throws GIException
	{
		LOGGER.info("PCI: ================= In Create Customer Profile ======================");
		Map<String, String> requestMap = new HashMap<String, String>();
		if(paymentMethod != null)
		{
			FinancialInfo financialInfo = paymentMethod.getFinancialInfo();
			if(financialInfo != null)
			{
				Location location = financialInfo.getLocation();
				if(location != null)
				{
					requestMap.put(CyberSourceKeyConstants.BILLTO_FIRSTNAME_KEY, financialInfo.getFirstName());
					requestMap.put(CyberSourceKeyConstants.BILLTO_LASTNAME_KEY, financialInfo.getLastName());
					requestMap.put(CyberSourceKeyConstants.BILLTO_STREET1_KEY, location.getAddress1());
					requestMap.put(CyberSourceKeyConstants.BILLTO_CITY_KEY, location.getCity());
					requestMap.put(CyberSourceKeyConstants.BILLTO_STATE_KEY, location.getState());
					requestMap.put(CyberSourceKeyConstants.BILLTO_POSTALCODE_KEY, location.getZip());
					requestMap.put(CyberSourceKeyConstants.BILLTO_COUNTRY_KEY, "USA");
					requestMap.put(CyberSourceKeyConstants.BILLTO_EMAIL_KEY, financialInfo.getEmail());
					requestMap.put(CyberSourceKeyConstants.BILLTO_PHONENUMBER_KEY, financialInfo.getContactNumber());
					
					requestMap.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, CyberSourceKeyConstants.PT_CURRENCY);
					requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_TITLE_KEY, paymentMethod.getPaymentMethodName());
					requestMap.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_FREQUENCY_KEY, CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_FREQUENCY);
					
					if(paymentMethod.getPaymentType() == PaymentType.BANK 
							|| paymentMethod.getPaymentType() == PaymentType.EFT || paymentMethod.getPaymentType() == PaymentType.ACH)
					{
						BankInfo bankInfo = financialInfo.getBankInfo();
						if(bankInfo != null)
						{
							requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY, "check");
						
							requestMap.put(CyberSourceKeyConstants.CHECK_ACCOUNTNUMBER_KEY, bankInfo.getAccountNumber());
							requestMap.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, "WEB");
							requestMap.put(CyberSourceKeyConstants.CHECK_ACCOUNTTYPE_KEY, bankInfo.getAccountType());
							requestMap.put(CyberSourceKeyConstants.CHECK_BANKTRANSITNUMBER_KEY, bankInfo.getRoutingNumber());
						}
						else
						{
							throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception received bankInfo object for createCustomerProfileRequest is null",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
						}
					}
					else if(paymentMethod.getPaymentType().equals(PaymentType.CREDITCARD)){
						CreditCardInfo creditCardInfo = financialInfo.getCreditCardInfo();
						if(creditCardInfo != null)
						{
							requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY, "credit card");
						
							String cardTypeKey = PaymentUtil.getCardTypeCode(creditCardInfo.getCardType());
							requestMap.put(CyberSourceKeyConstants.CARD_CARDTYPE_KEY,cardTypeKey);
							requestMap.put(CyberSourceKeyConstants.CARD_ACCOUNTNUMBER_KEY, creditCardInfo.getCardNumber());
							requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONMONTH_KEY, creditCardInfo.getExpirationMonth());
							requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONYEAR_KEY, creditCardInfo.getExpirationYear());
						}
						else
						{
							throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception received creditCardInfo object for createCustomerProfileRequest is null",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
						}
						
					}
				}
				else
				{
					throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception received location object for createCustomerProfileRequest is null",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
				}
				
			}
			else
			{
				throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception received financialInfo object for createCustomerProfileRequest is null",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
			}
		}
		else
		{
			throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception received paymentMethod object for createCustomerProfileRequest is null",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
		}
		return requestMap;
	}
	
	
	/**
	 * Method updateCustomerProfileRequest
	 * @param paymentMethod PaymentMethods
	 * @param userProfile PaymentMethods
	 * @return Map
	 * @throws GIException	 
	 */
	@SuppressWarnings("rawtypes")
	public Map updateCustomerProfileRequest(PaymentMethods paymentMethods, Map userProfile)throws GIException
	{
		LOGGER.info("PCI: ================= In Update Customer Profile ======================");
		Map<String, String> requestMap = new HashMap<String, String>();
		
		requestMap.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_SUBSCRIPTIONID_KEY, paymentMethods.getSubscriptionId());
		if(userProfile != null && !userProfile.isEmpty())
		{
			FinancialInfo financialInfo = paymentMethods.getFinancialInfo();
			Location location = financialInfo.getLocation();
			if(userProfile.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_STREET1_KEY) 
					&& StringUtils.isNotEmpty(location.getAddress1()) 
					&& !location.getAddress1().equals((String)userProfile.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_STREET1_KEY)))
			{
				requestMap.put(CyberSourceKeyConstants.BILLTO_STREET1_KEY, location.getAddress1());
				financialInfo.setAddress1(location.getAddress1());
			}
			if(userProfile.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_STATE_KEY) 
					&& StringUtils.isNotEmpty(location.getState()) 
					&& !location.getState().equals((String)userProfile.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_STATE_KEY)))
			{
				requestMap.put(CyberSourceKeyConstants.BILLTO_STATE_KEY, location.getState());
				financialInfo.setState(location.getState());
			}
			if(userProfile.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CITY_KEY) 
					&& StringUtils.isNotEmpty(location.getCity()) 
					&& !location.getCity().equals((String)userProfile.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CITY_KEY)))
			{
				requestMap.put(CyberSourceKeyConstants.BILLTO_CITY_KEY, location.getCity());
				financialInfo.setCity(location.getCity());
			}
			if(userProfile.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_POSTALCODE_KEY) 
					&& StringUtils.isNotEmpty(location.getZip()) && !location.getZip().equals((String)userProfile.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_POSTALCODE_KEY)))
			{
				requestMap.put(CyberSourceKeyConstants.BILLTO_POSTALCODE_KEY, location.getZip());
				financialInfo.setZipcode(location.getZip());
			}
			if(userProfile.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_PHONENUMBER_KEY) 
					&& StringUtils.isNotEmpty(financialInfo.getContactNumber()) 
					&& !financialInfo.getContactNumber().equals((String)userProfile.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_PHONENUMBER_KEY)))
			{
				requestMap.put(CyberSourceKeyConstants.BILLTO_PHONENUMBER_KEY, financialInfo.getContactNumber());
			}
			if(userProfile.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_TITLE_KEY) 
					&& StringUtils.isNotEmpty(paymentMethods.getPaymentMethodName()) 
					&& !paymentMethods.getPaymentMethodName().equals((String)userProfile.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_TITLE_KEY)))
			{
				requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_TITLE_KEY, paymentMethods.getPaymentMethodName());
			}
			if(userProfile.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_EMAIL_KEY) 
					&& StringUtils.isNotEmpty(financialInfo.getEmail()) && !financialInfo.getEmail().equals((String)userProfile.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_EMAIL_KEY)))
			{
				requestMap.put(CyberSourceKeyConstants.BILLTO_EMAIL_KEY, financialInfo.getEmail());
			}
			if(paymentMethods.getPaymentType() == PaymentType.BANK 
					|| paymentMethods.getPaymentType() == PaymentType.EFT || paymentMethods.getPaymentType() == PaymentType.ACH)			
			{
				BankInfo bankInfo = financialInfo.getBankInfo();
				requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY, "check");
				requestMap.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, "WEB");
				if(userProfile.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CHECKACCOUNTTYPE_KEY) 
						&& StringUtils.isNotEmpty(bankInfo.getAccountType()) 
						&& !bankInfo.getAccountType().equals((String) userProfile.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CHECKACCOUNTTYPE_KEY)))
				{
					requestMap.put(CyberSourceKeyConstants.CHECK_ACCOUNTTYPE_KEY, bankInfo.getAccountType());
				}
				if(userProfile.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CHECKBANKTRANSITNUMBER_KEY) 
						&& StringUtils.isNotEmpty(bankInfo.getRoutingNumber()) 
						&& !bankInfo.getRoutingNumber().equals((String)userProfile.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CHECKBANKTRANSITNUMBER_KEY)))
				{
					requestMap.put(CyberSourceKeyConstants.CHECK_BANKTRANSITNUMBER_KEY, bankInfo.getRoutingNumber());
				}
				if(userProfile.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CHECKACCOUNTNUMBER_KEY) 
						&& StringUtils.isNotEmpty(bankInfo.getAccountNumber()) 
						&& !bankInfo.getAccountNumber().equals((String)userProfile.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CHECKACCOUNTNUMBER_KEY)))
				{
					requestMap.put(CyberSourceKeyConstants.CHECK_ACCOUNTNUMBER_KEY, bankInfo.getAccountNumber());
				}
			}
			else if(paymentMethods.getPaymentType().equals(PaymentType.CREDITCARD) && financialInfo.getCreditCardInfo() != null){
				CreditCardInfo creditCardInfo = financialInfo.getCreditCardInfo();
				
				requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY, "credit card");
			
				if(userProfile.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CARDEXPIRATIONMONTH_KEY) 
						&& StringUtils.isNotEmpty(creditCardInfo.getExpirationMonth()) 
						&& !creditCardInfo.getExpirationMonth().equals((String)userProfile.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CARDEXPIRATIONMONTH_KEY)))
				{
					requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONMONTH_KEY, creditCardInfo.getExpirationMonth());
				}
				if(userProfile.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CARDEXPIRATIONYEAR_KEY) 
						&& StringUtils.isNotEmpty(creditCardInfo.getExpirationYear()) 
						&& !creditCardInfo.getExpirationYear().equals((String)(String)userProfile.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CARDEXPIRATIONYEAR_KEY)))
				{
					requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONYEAR_KEY, creditCardInfo.getExpirationYear());
				}
				if(userProfile.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CARDTYPE_KEY) && StringUtils.isNotEmpty(creditCardInfo.getCardType()) )
				{
					String cardTypeKey = PaymentUtil.getCardTypeCode(creditCardInfo.getCardType());
					requestMap.put(CyberSourceKeyConstants.CARD_CARDTYPE_KEY,cardTypeKey);
				}
				if(userProfile.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CARDACCOUNTNUMBER_KEY) 
						&& StringUtils.isNotEmpty(creditCardInfo.getCardNumber()) 
						&& !creditCardInfo.getCardNumber().equals((String) userProfile.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CARDACCOUNTNUMBER_KEY)))
				{
					requestMap.put(CyberSourceKeyConstants.CARD_ACCOUNTNUMBER_KEY, creditCardInfo.getCardNumber());
				}
			}
		}
		else
		{
			throw new GIException(FinanceConstants.ERROR_CODE_201, "Unable to update customer payment info", FinanceConstants.EXCEPTION_SEVERITY_HIGH);
		}
		return requestMap;
	}
	
	
	/**
	 * Method createCustomerProfileReqNonLocation
	 * @param paymentMethod PaymentMethods
	 * @param isLocationExists boolean
	 * @return Map
	 * @throws GIException	 
	 */
	@SuppressWarnings("rawtypes")
	public Map createCustomerProfileReqNonLocation(PaymentMethods paymentMethod, boolean isLocationExists)throws GIException
	{
		Map<String, String> requestMap = new HashMap<String, String>();
		if(paymentMethod != null)
		{
			FinancialInfo financialInfo = paymentMethod.getFinancialInfo();
			if(financialInfo != null)
			{
					if(StringUtils.isNotEmpty(financialInfo.getFirstName()))
					{
						requestMap.put(CyberSourceKeyConstants.BILLTO_FIRSTNAME_KEY, financialInfo.getFirstName());
					}
					else{
						throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception First Name is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
					}
					if(StringUtils.isNotEmpty(financialInfo.getLastName()))
					{
						requestMap.put(CyberSourceKeyConstants.BILLTO_LASTNAME_KEY, financialInfo.getLastName());
					}
					else{
						throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Last Name is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
					}
					
					
					
					 //============== code changes start here
					if(isLocationExists)
					{
						Location location = financialInfo.getLocation();
						if(StringUtils.isNotEmpty(location.getAddress1()))
						{
							requestMap.put(CyberSourceKeyConstants.BILLTO_STREET1_KEY, location.getAddress1());
						}
						else
						{
							throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception address street1 is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
						}
						if(StringUtils.isNotEmpty(location.getCity()))
						{
							requestMap.put(CyberSourceKeyConstants.BILLTO_CITY_KEY, location.getCity());
						}
						else
						{
							throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception address city is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
						}
						if(StringUtils.isNotEmpty(location.getState()))
						{
							requestMap.put(CyberSourceKeyConstants.BILLTO_STATE_KEY, location.getState());
						}
						else
						{
							throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception address state is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
						}
						if(StringUtils.isNotEmpty(location.getZip()))
						{
							requestMap.put(CyberSourceKeyConstants.BILLTO_POSTALCODE_KEY, location.getZip());
						}
						else
						{
							throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception address zip code is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
						}
						
					}
					else
					{
						if(StringUtils.isNotEmpty(financialInfo.getAddress1()))
						{
							requestMap.put(CyberSourceKeyConstants.BILLTO_STREET1_KEY, financialInfo.getAddress1());
						}
						else{
							throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Street Address1 details is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
						}
						if(StringUtils.isNotEmpty(financialInfo.getCity())){
							requestMap.put(CyberSourceKeyConstants.BILLTO_CITY_KEY, financialInfo.getCity());
						}
						else{
							throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception city detail is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
						}
						if(StringUtils.isNotEmpty(financialInfo.getState())){
							requestMap.put(CyberSourceKeyConstants.BILLTO_STATE_KEY, financialInfo.getState());
						}
						else{
							throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception state detail is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
						}
						if(StringUtils.isNotEmpty(financialInfo.getZipcode())){
							requestMap.put(CyberSourceKeyConstants.BILLTO_POSTALCODE_KEY, financialInfo.getZipcode());
						}
						else{
							throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception ZipCode detail is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
						}
						//================ code changes end here
					
					}
					requestMap.put(CyberSourceKeyConstants.BILLTO_COUNTRY_KEY, "USA");
					if(StringUtils.isNotEmpty(financialInfo.getEmail())){
						requestMap.put(CyberSourceKeyConstants.BILLTO_EMAIL_KEY, financialInfo.getEmail());
					}
					else{
						throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception email detail is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
					}
					if(StringUtils.isNotEmpty(financialInfo.getContactNumber())){
						requestMap.put(CyberSourceKeyConstants.BILLTO_PHONENUMBER_KEY, financialInfo.getContactNumber());
					}else{
						throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception contact number detail is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
					}
					
					requestMap.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, CyberSourceKeyConstants.PT_CURRENCY);
					if(StringUtils.isNotEmpty(paymentMethod.getPaymentMethodName())){
						requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_TITLE_KEY, paymentMethod.getPaymentMethodName());
					}else{
						throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Payment Method Name detail is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
					}
					requestMap.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_FREQUENCY_KEY, CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_FREQUENCY);
					
					if(paymentMethod.getPaymentType() == PaymentType.BANK 
							|| paymentMethod.getPaymentType() == PaymentType.EFT || paymentMethod.getPaymentType() == PaymentType.ACH){
						BankInfo bankInfo = financialInfo.getBankInfo();
						if(bankInfo != null)
						{
							requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY, "check");
							if(StringUtils.isNotEmpty(bankInfo.getAccountNumber()))
							{
								requestMap.put(CyberSourceKeyConstants.CHECK_ACCOUNTNUMBER_KEY, bankInfo.getAccountNumber());
							}
							else
							{
								throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Bank Account Number is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
							}
							if(StringUtils.isNotEmpty(bankInfo.getAccountType()))
							{
								requestMap.put(CyberSourceKeyConstants.CHECK_ACCOUNTTYPE_KEY, bankInfo.getAccountType());
							}
							else
							{
								throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Bank Account Type is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
							}
							if(StringUtils.isNotEmpty(bankInfo.getRoutingNumber()))
							{
								requestMap.put(CyberSourceKeyConstants.CHECK_BANKTRANSITNUMBER_KEY, bankInfo.getRoutingNumber());
							}
							else
							{
								throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception ABA Routing Number is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
							}
							requestMap.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, "WEB");							
						}
						else
						{
							throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception received bankInfo object for createCustomerProfileRequest is null",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
						}
					}
					else if(paymentMethod.getPaymentType().equals(PaymentType.CREDITCARD)){
						CreditCardInfo creditCardInfo = financialInfo.getCreditCardInfo();
						if(creditCardInfo != null)
						{
							requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY, "credit card");
							if(StringUtils.isNotEmpty(creditCardInfo.getCardType()))
							{
								String cardTypeKey = PaymentUtil.getCardTypeCode(creditCardInfo.getCardType());
								requestMap.put(CyberSourceKeyConstants.CARD_CARDTYPE_KEY,cardTypeKey);
							}
							else
							{
								throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Credit card Type is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
							}
							if(StringUtils.isNotEmpty(creditCardInfo.getCardNumber()))
							{
								requestMap.put(CyberSourceKeyConstants.CARD_ACCOUNTNUMBER_KEY, creditCardInfo.getCardNumber());
							}
							else
							{
								throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Card Number is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
							}
							
							if(StringUtils.isNotEmpty(creditCardInfo.getExpirationMonth()))
							{
								requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONMONTH_KEY, creditCardInfo.getExpirationMonth());
							}
							else
							{
								throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Card Expiration Month is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
							}
							
							if(StringUtils.isNotEmpty(creditCardInfo.getExpirationYear()))
							{
								requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONYEAR_KEY, creditCardInfo.getExpirationYear());
							}
							else
							{
								throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Card Expiration Year is null or empty",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
							}							
						}
						else
						{
							throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception received creditCardInfo object for createCustomerProfileRequest is null",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
						}						
					}
			}
			else
			{
				throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception received financialInfo object for createCustomerProfileRequest is null",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
			}
		}
		else
		{
			throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception received paymentMethod object for createCustomerProfileRequest is null",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
		}
		return requestMap;
	}
	
	
}
