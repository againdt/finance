package com.getinsured.hix.finance.issuer.service.jpa;

import java.io.File;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.dto.finance.RemittanceInfoEDI;
import com.getinsured.hix.finance.cybersource.utils.FinancialInfoMapper;
import com.getinsured.hix.finance.cybersource.utils.PaymentProcessorStrategy;
import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.finance.fees.service.ExchangeFeeService;
import com.getinsured.hix.finance.issuer.repository.IIssuerRepository;
import com.getinsured.hix.finance.issuer.service.FinanceIssuerExchgPartnerLookupService;
import com.getinsured.hix.finance.issuer.service.IssuerPaymentDetailService;
import com.getinsured.hix.finance.issuer.service.IssuerPaymentInvoiceService;
import com.getinsured.hix.finance.issuer.service.IssuerPaymentProcessingService;
import com.getinsured.hix.finance.issuer.service.IssuerRemittanceLineItemService;
import com.getinsured.hix.finance.issuer.service.IssuerRemittanceService;
import com.getinsured.hix.finance.payment.service.PaymentEventLogService;
import com.getinsured.hix.finance.paymentmethod.service.PaymentMethodService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceConstants.CyberSourceResponseMapKeys;
import com.getinsured.hix.finance.utils.FinanceGeneralUtility;
import com.getinsured.hix.finance.utils.FinancialMgmtGenericUtil;
import com.getinsured.hix.finance.utils.FinancialMgmtUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.ExchangeFee;
import com.getinsured.hix.model.ExchgPartnerLookup;
import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerPaymentDetail;
import com.getinsured.hix.model.IssuerPaymentDetail.EDIGeneratedFlag;
import com.getinsured.hix.model.IssuerPaymentDetail.ExchangePaymentType;
import com.getinsured.hix.model.IssuerPaymentInvoice;
import com.getinsured.hix.model.IssuerPayments;
import com.getinsured.hix.model.IssuerRemittance;
import com.getinsured.hix.model.IssuerRemittance.PaidStatus;
import com.getinsured.hix.model.IssuerRemittanceLineItem;
import com.getinsured.hix.model.PaymentEventLog;
import com.getinsured.hix.model.PaymentEventLog.PaymentStatus;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.ModuleName;
import com.getinsured.hix.model.PaymentMethods.PaymentType;
import com.getinsured.hix.model.enrollment.EnrollmentRemittanceDTO;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;


/**
 * This service implementation class is for GHIX-FINANCE module for processing issuer payment.
 *
 * @author sharma_k
 * @since 28-Jun-2013
 */
@Service("issuerPaymentProcessingService")
public class IssuerPaymentProcessingServiceImpl implements IssuerPaymentProcessingService
{
	private final SimpleDateFormat dateFormat = new SimpleDateFormat(GhixConstants.FILENAME_DATE_FORMAT);
	@Autowired private IssuerRemittanceService issuerRemittanceService;
	@Autowired private IssuerRemittanceLineItemService issuerRemittanceLineItemService;
	@Autowired private IssuerPaymentInvoiceService issuerPaymentInvoiceService;
	@Autowired private IssuerPaymentDetailService issuerPaymentDetailService;
	//	@Autowired private ECheckProcessor eCheckProcessor;
	/*@Autowired private UserService userService;*/
	@Autowired private PaymentMethodService paymentMethodService;
	/*@Autowired private FinancialInfoService financialInfoService;*/
	/*@Autowired private IIssuerRepresentativeRepository issuerRepresentativeRepository;*/
	@Autowired private ExchangeFeeService exchangeFeeService;
	@Autowired private FinancialMgmtUtils financialMgmtUtils;
	@Autowired private PaymentEventLogService paymentEventLogService;
	@Autowired private FinanceIssuerExchgPartnerLookupService financeIssuerExchgPartnerLookupService;
	@Autowired private IIssuerRepository iIssuerRepository;
	@Autowired private UserService userService;

	private static final int PAYMENT_GLCODE = 1000;	

	private static final String COUNTRY = "USA";	
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerPaymentProcessingServiceImpl.class);


	/**
	 * 
	 * @param int issuerInvoiceID
	 * @return Object<ExchangeFee>
	 */
	private ExchangeFee updateExchangeFee(int issuerInvoiceID){

		return exchangeFeeService.updateExchangeFee(issuerInvoiceID);
	}

	/**
	 /**
	 * Method processPayment.
	 * @return void
	 * @see com.getinsured.hix.finance.issuer.service.IssuerPaymentProcessingService#processPayment()
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void processPayment() throws FinanceException
	{
		LOGGER.info("STARTED method - processPayment(-)");

		//SimpleDateFormat simpleFormatter = new SimpleDateFormat("yyyy/MM/dd");
		// Date range of 2 month is used if current month is jan-2013  returned data will be ex 12-01-2012 to 01-31-2013
		//Date[] dates = getRequiredDateRange();
		LOGGER.info(" Get Unpaid Issuer Invoices...");
		Map<String, Exception> exceptionDetails = null;
		List<IssuerRemittance> listOfIssuersRemittance = issuerRemittanceService.getUnpaidIssuerRemittance();
		String isPaymentGateWay = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_PAYMENT_GATEWAY);
		String isStandaloneCreditSupported = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_STANDALONE_CREDIT_SUPPORTED);
		LOGGER.info("The Value of PaymentGateWay : "+isPaymentGateWay);
		for (IssuerRemittance issuerInvoice : listOfIssuersRemittance)
		{
			String merchantCode = null;
			LOGGER.info("Started issuer invoice from list of issuers: "+issuerInvoice.getId());
			try{
				List<IssuerRemittanceLineItem> issInvList = issuerRemittanceLineItemService.getIssuerRemittanceLineItems(issuerInvoice);

				BigDecimal totalAmountToPay = FinancialMgmtGenericUtil.getDefaultBigDecimal();
				LOGGER.info("Get Employer Invoice Line Items Isuuer Id: "+issuerInvoice.getIssuer().getId());

				if (issInvList != null && !issInvList.isEmpty())
				{
					LOGGER.info("Get Employer Invoice Line Items for Isuuer Id: "+issuerInvoice.getIssuer().getId()+" and Size: "+issInvList.size());
					LOGGER.info(" Get PaymentMethodsByModuleIdAndModuleName: "+issuerInvoice.getIssuer().getId()+" : "+ModuleName.ISSUER);
					PaymentMethods paymentMethods = null;//paymentMethodService.findPaymentMethodsByModuleIdAndModuleName(issuerInvoice.getIssuer().getId(), ModuleName.ISSUER);

					Map<String, Object> financeResponseMap = paymentMethodService.searchEmployerPaymentMethod(this.populatePaymentMethodRequestDTO(issuerInvoice.getIssuer().getId(), ModuleName.ISSUER, PaymentMethods.PaymentStatus.Active, PaymentMethods.PaymentIsDefault.Y));
					if(financeResponseMap != null && !financeResponseMap.isEmpty() && financeResponseMap.containsKey(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY))
					{
						List<PaymentMethods> paymentMethodList = (List<PaymentMethods>) financeResponseMap.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY);
						if(paymentMethodList != null && !paymentMethodList.isEmpty() && paymentMethodList.size() == FinanceConstants.NUMERICAL_1)
						{
							paymentMethods = paymentMethodList.get(FinanceConstants.NUMERICAL_0);
						}
					}
					if (paymentMethods != null)
					{
						FinancialInfoMapper fiMapper = new  FinancialInfoMapper();
						FinancialInfo financialobj = paymentMethods.getFinancialInfo();

						String timeStamp = new SimpleDateFormat("_yyyyMMdd_HHmmss").format(TSCalendar.getInstance().getTime());
						merchantCode = "ISS_"+issuerInvoice.getRemittanceNumber()+timeStamp;
						if(isPaymentGateWay != null && isPaymentGateWay.equalsIgnoreCase(FinanceConstants.TRUE) && paymentMethods.getSubscriptionId() != null)
						{
							LOGGER.info("Merchant code for PciPayRequest: "+merchantCode);
							fiMapper.setPciPayRequestWithSecCode(paymentMethods.getSubscriptionId(),merchantCode , CyberSourceKeyConstants.CHECK_SECCODE_CCD);
						}
						else
						{
							if(financialobj.getCountry() == null)
							{
								financialobj.setCountry(COUNTRY);
							}
							fiMapper.setPayRequestWithSecCode(financialobj, CyberSourceKeyConstants.CHECK_SECCODE_CCD, merchantCode);
						}

						for (IssuerRemittanceLineItem issuerRemittanceLineitem : issInvList)
						{
							BigDecimal paymentReceived = issuerRemittanceLineitem.getNetAmount();
							paymentReceived = (paymentReceived!=null) ? paymentReceived : FinancialMgmtGenericUtil.getDefaultBigDecimal();
							totalAmountToPay = FinancialMgmtGenericUtil.add(totalAmountToPay, paymentReceived);
						}
						IssuerPaymentDetail issuerPaymentDetail = null;
						synchronized(this)
						{
							IssuerRemittance issuerRemittance = issuerRemittanceService.findIssuerRemittanceById(issuerInvoice.getId());
							/*
							 * Changes made in Sync code for Jira ID: HIX-63624
							 */
							if(issuerRemittance!=null && (issuerRemittance.getPaidStatus().equals(PaidStatus.DUE) || issuerRemittance.getPaidStatus().equals(PaidStatus.PARTIALLY_PAID)))
							{
								Map gatewayResponse = callCyberSource(financialobj, totalAmountToPay, isStandaloneCreditSupported, fiMapper, isPaymentGateWay);							
								AccountUser user = FinanceGeneralUtility.getAccountUser(userService);
								if (gatewayResponse != null && !gatewayResponse.isEmpty() )
								{
									if(gatewayResponse.get(FinanceConstants.CyberSourceResponseMapKeys.CYBERSOURCE_DECISION) != null 
											&& gatewayResponse.get(FinanceConstants.CyberSourceResponseMapKeys.CYBERSOURCE_DECISION).toString().equalsIgnoreCase(IssuerPaymentDetail.PaymentProcessingStatus.ACCEPT.toString()))
									{
										LOGGER.info(" Insert in to Issuer Payment Invoice====");
										IssuerPaymentInvoice issuerPaymentInvoice = insertIssuerPaymentInvoice(issuerInvoice, totalAmountToPay,user.getId());
										LOGGER.info("Save in Payment Detail...");
										issuerPaymentDetail = savePaymentDetail(gatewayResponse, paymentMethods, totalAmountToPay, issuerPaymentInvoice.getIssuerPayments(), issuerInvoice.getIssuer(),user.getId());
									}										
									updateExchangeFee(issuerInvoice.getId());
									LOGGER.info("=========Insert in to Payment Log==========");
									insertPaymentLog(issuerPaymentDetail, gatewayResponse, issuerInvoice, paymentMethods.getPaymentType(),user.getId());
								}
								else if(gatewayResponse == null && isStandaloneCreditSupported.equalsIgnoreCase(FinanceConstants.FALSE))
								{
									gatewayResponse = new HashMap();
									gatewayResponse.put(CyberSourceResponseMapKeys.CYBERSOURCE_MERCHANTREFERENCECODE, merchantCode);
									LOGGER.info("==StandaloneCredit not Supported==");
									IssuerPaymentInvoice issuerPaymentInvoice = insertIssuerPaymentInvoice(issuerInvoice, totalAmountToPay,user.getId());
									LOGGER.info("=========Save in Payment Detail==========");
									issuerPaymentDetail = savePaymentDetail(gatewayResponse, paymentMethods, totalAmountToPay, issuerPaymentInvoice.getIssuerPayments(), issuerInvoice.getIssuer(),user.getId());										
									updateExchangeFee(issuerInvoice.getId());
									LOGGER.info("=========Insert in to Payment Log==========");
									insertPaymentLog(issuerPaymentDetail, gatewayResponse, issuerInvoice, paymentMethods.getPaymentType(),user.getId());
								}
							}
							else
							{
								LOGGER.error("Payment is already done for the invoice id "+issuerInvoice.getId());
							}
						}
					}
				}
			}catch(Exception e){
				if(exceptionDetails==null){
					exceptionDetails = new HashMap<String, Exception>();
				}
				LOGGER.error("EXCEPTION OCCURRED WHILE PROCESSING ISSUER PAYMENT FOR THE FOLLWING INVOICE : "+issuerInvoice.getRemittanceNumber(), e);
				exceptionDetails.put(issuerInvoice.getRemittanceNumber(), e);
			}			
		}
		if(exceptionDetails!=null && !exceptionDetails.isEmpty()){
			String exceptionMessage = FinanceGeneralUtility.constructSingleMessageForExceptionDetails("EXCEPTIONS OCCURRED WHILE PROCESSING PAYMENT FOR THE FOLLWING INVOICES:", "Invoice Number", exceptionDetails);
			throw new FinanceException(exceptionMessage);
		}		
		LOGGER.info(" END - rocessPayment() method successfully");		
	}

	private PaymentMethodRequestDTO populatePaymentMethodRequestDTO(final int issuerid, final PaymentMethods.ModuleName moduleName, final PaymentMethods.PaymentStatus paymentMethodStatus, final PaymentMethods.PaymentIsDefault isDefaultPaymentMethod )
	{
		PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
		if(issuerid != FinanceConstants.NUMERICAL_0)
		{
			paymentMethodRequestDTO.setModuleID(issuerid);
		}
		if(moduleName != null)
		{
			paymentMethodRequestDTO.setModuleName(moduleName);
		}
		if(paymentMethodStatus != null)
		{
			paymentMethodRequestDTO.setPaymentMethodStatus(paymentMethodStatus);
		}
		if(isDefaultPaymentMethod != null)
		{
			paymentMethodRequestDTO.setIsDefaultPaymentMethod(isDefaultPaymentMethod);
		}
		return paymentMethodRequestDTO;
	}
	/**
	 * Method callCyberSource.
	 * @param financialobj FinancialInfo
	 * @param totalAmountToPay BigDecimal
	 * @param isStandaloneCreditSupported String
	 * @param financialInfoMapper FinancialInfoMapper
	 * @param isPaymentGateWay String
	 * @return Map
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	private Map callCyberSource(final FinancialInfo financialobj, final BigDecimal totalAmountToPay, final String isStandaloneCreditSupported, final FinancialInfoMapper fiMapper, final String isPaymentGateWay)
	{
		Map gatewayResponse = null;
		if (financialobj.getBankInfo() != null && (totalAmountToPay.compareTo(BigDecimal.ZERO) == FinanceConstants.NUMERICAL_1) && 
				isStandaloneCreditSupported.equalsIgnoreCase(FinanceConstants.TRUE))
		{
			try
			{
				gatewayResponse = payIssuerUsingEcheck(fiMapper.getPayRequest(),totalAmountToPay.toPlainString(), isPaymentGateWay);
			}
			catch(Exception ex)
			{
				LOGGER.error("Exception caught while receiving PaymentGatewayResponse for :");
				LOGGER.error("Exception caught while receiving PaymentGatewayResponse: "+ex);
			}
		}
		else if (financialobj.getBankInfo() != null && (totalAmountToPay.compareTo(BigDecimal.ZERO) == FinanceConstants.NUMERICAL_NEG_1)){
			try
			{
				gatewayResponse = refundIssuerUsingEcheck(fiMapper.getPayRequest(),totalAmountToPay.negate().toPlainString(), isPaymentGateWay);
			}
			catch(Exception ex)
			{
				LOGGER.error("Exception caught while receiving PaymentGatewayResponse for :");
				LOGGER.error("Exception caught while receiving PaymentGatewayResponse: "+ex);
			}			
		}
		return gatewayResponse;							
	}


	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	private PaymentEventLog insertPaymentLog(IssuerPaymentDetail issuerPaymentDetail, Map cybersourceResponse, IssuerRemittance issuerRemittance, PaymentMethods.PaymentType paymentType, int userId)
	{
		LOGGER.info("STARTED - insertPaymentLog(-,-) method");
		PaymentEventLog paymentEventLog = new PaymentEventLog();

		if(issuerPaymentDetail != null)
		{
			paymentEventLog.setIssuerPaymentDetail(issuerPaymentDetail);
		}
		paymentEventLog.setResponse(getMapValue(cybersourceResponse));
		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_MERCHANTREFERENCECODE))
		{
			paymentEventLog.setMerchantRefCode((String)cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_MERCHANTREFERENCECODE));
		}

		/*
		 * For Echeck reply 
		 */
		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_ECCREDIT_RECONCILIATIONID))
		{
			paymentEventLog.setTransactionId((String)cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_ECCREDIT_RECONCILIATIONID));
		}
		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_ECDEBIT_RECONCILIATIONID))
		{
			paymentEventLog.setTransactionId((String)cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_ECDEBIT_RECONCILIATIONID));
		}

		/*
		 * For Credit Card Reply
		 */
		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_CCCREDITREPLY_RECONCILIATIONID))
		{
			paymentEventLog.setTransactionId((String) cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_CCCREDITREPLY_RECONCILIATIONID));
		}
		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_CCCAPTUREREPLY_RECONCILIATIONID))
		{
			paymentEventLog.setTransactionId((String) cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_CCCAPTUREREPLY_RECONCILIATIONID));
		}


		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_CCCREDITREPLY_RECONCILIATIONID))
		{
			paymentEventLog.setTransactionId((String) cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_CCCREDITREPLY_RECONCILIATIONID));
		}
		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_CCCAPTUREREPLY_RECONCILIATIONID))
		{
			paymentEventLog.setTransactionId((String) cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_CCCAPTUREREPLY_RECONCILIATIONID));
		}



		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE))
		{
			paymentEventLog.setTxnReasonCode((String)cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE));
		}
		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_DECISION))
		{
			paymentEventLog.setTxnStatus((String)cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_DECISION));
			if("ACCEPT".equalsIgnoreCase(cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_DECISION).toString()))
			{
				if(paymentType.toString().equals(PaymentType.CREDITCARD.toString()))
				{
					paymentEventLog.setPaymentStatus(PaymentStatus.CONFIRMED);
				}
				else
				{
					paymentEventLog.setPaymentStatus(PaymentStatus.PENDING);
				}
			}
			else
			{
				paymentEventLog.setPaymentStatus(PaymentStatus.FAILED);
			}
		}
		if(cybersourceResponse.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID))
		{
			paymentEventLog.setRequestId((String)cybersourceResponse.get(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID));
		}

		paymentEventLog.setIssuerRemittance(issuerRemittance);
		paymentEventLog.setLastUpdatedBy(userId);
		LOGGER.info("END -insertPaymentLog() method.");
		return paymentEventLogService.savePaymentEventLog(paymentEventLog);
	}


	/**
	 * Method insertIssuerPaymentInvoice.
	 * @param issuerinvoice IssuerInvoices
	 * @param amount float
	 * @return IssuerPaymentInvoice
	 */
	private IssuerPaymentInvoice insertIssuerPaymentInvoice(IssuerRemittance issuerRemittance, BigDecimal amnt, int userId){
		LOGGER.info("========= insertIssuerPaymentInvoice(-,-) method started with amount: "+amnt+" ==========");
		BigDecimal amount = amnt;

		IssuerPaymentInvoice newIssuerPaymentInvoice = null;
		LOGGER.info(" Find Issuer Payment Invoice By Invoice ID: "+issuerRemittance.getId());
		newIssuerPaymentInvoice = issuerPaymentInvoiceService.findIssuerPaymentInvoiceByInvoiceID(issuerRemittance.getId());
		if(newIssuerPaymentInvoice != null)
		{
			issuerRemittance.setPaidStatus(PaidStatus.IN_PROCESS);
			newIssuerPaymentInvoice.setAmount(FinancialMgmtGenericUtil.add(newIssuerPaymentInvoice.getAmount(), amount));

			IssuerPayments newIssuerPayments = newIssuerPaymentInvoice.getIssuerPayments();

			newIssuerPayments.setIsPartialPayment('P');
			newIssuerPayments.setStatus(PaidStatus.IN_PROCESS.toString());
			newIssuerPaymentInvoice.setIssuerPayments(newIssuerPayments);
			newIssuerPaymentInvoice.setLastUpdatedBy(userId);
			newIssuerPaymentInvoice = issuerPaymentInvoiceService.updateIssuerPaymentInvoice(newIssuerPaymentInvoice);
		}
		else
		{
			issuerRemittance.setPaidStatus(PaidStatus.IN_PROCESS);

			IssuerPaymentInvoice issuerPaymentInvoice = new IssuerPaymentInvoice();
			issuerPaymentInvoice.setIssuerRemittance(issuerRemittance);
			issuerPaymentInvoice.setAmount(amount);
			issuerPaymentInvoice.setGlCode(PAYMENT_GLCODE);
			newIssuerPaymentInvoice = issuerPaymentInvoice;
			IssuerPayments newIssuerPayments = populateIssuerPayments(issuerRemittance, FinancialMgmtGenericUtil.add(issuerRemittance.getTotalPaymentPaid(), amount));
			newIssuerPayments.setIssuerPaymentInvoice(issuerPaymentInvoice);
			newIssuerPaymentInvoice.setIssuerPayments(newIssuerPayments);
			newIssuerPaymentInvoice.setLastUpdatedBy(userId);
			newIssuerPaymentInvoice = issuerPaymentInvoiceService.saveIssuerPaymentInvoice(issuerPaymentInvoice);
		}
		issuerRemittance.setPaidDateTime(new TSDate());
		issuerRemittance.setLastUpdatedBy(userId);
		issuerRemittanceService.saveIssuerRemittance(issuerRemittance);
		LOGGER.info("END-insertIssuerPaymentInvoice(-) method, ends successfully ");
		return newIssuerPaymentInvoice;
	}	

	/**
	 * Method populateIssuerPayments.
	 * @param issuerinvoice IssuerInvoices
	 * @param amount float
	 * @return IssuerPayments
	 */
	private IssuerPayments populateIssuerPayments(IssuerRemittance issuerRemittance, BigDecimal amount)
	{
		LOGGER.info("STARTED - populateIssuerPayments(-) method with amount: "+amount);
		IssuerPayments newIssuerPayments = new IssuerPayments();
		newIssuerPayments.setCaseId(issuerRemittance.getCaseId());
		newIssuerPayments.setStatementDate(issuerRemittance.getStatementDate());
		newIssuerPayments.setInvoiceNumber(issuerRemittance.getRemittanceNumber());
		newIssuerPayments.setPeriodCovered(issuerRemittance.getPeriodCovered());
		newIssuerPayments.setPaymentDueDate(issuerRemittance.getPaymentDueDate());
		newIssuerPayments.setAmountDueFromLastInvoice(issuerRemittance.getAmountDueFromLastRemittance());

		newIssuerPayments.setAmountEnclosed(issuerRemittance.getTotalAmountDue());
		newIssuerPayments.setPremiumThisPeriod(issuerRemittance.getPremiumThisPeriod());
		newIssuerPayments.setAdjustments(issuerRemittance.getAdjustments());
		newIssuerPayments.setExchangeFees(issuerRemittance.getExchangeFees());
		newIssuerPayments.setTotalAmountDue(issuerRemittance.getTotalAmountDue());
		newIssuerPayments.setAmountEnclosed(issuerRemittance.getTotalAmountDue());
		newIssuerPayments.setStatus(issuerRemittance.getPaidStatus().toString());
		newIssuerPayments.setIsPartialPayment('P');
		newIssuerPayments.setTotalPaymentReceived(FinancialMgmtGenericUtil.getDefaultBigDecimal());

		LOGGER.info("END - populateIssuerPayments(-) method successfully with newIssuerPayments:"+newIssuerPayments.getInvoiceNumber());
		return newIssuerPayments;
	}

	/**
	 * Method savePaymentDetail.
	 * @param reply HashMap
	 * @param paymentTypes PaymentMethods
	 * @param amount float
	 * @param issuerPayments IssuerPayments
	 * @return IssuerPaymentDetail
	 */
	private IssuerPaymentDetail savePaymentDetail(@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES) Map reply, PaymentMethods paymentTypes, BigDecimal amount, IssuerPayments issuerPayments, Issuer issuer, int userId)
	{	
		LOGGER.info("STARTED-savePaymentDetail(-) method with amount: "+amount);
		IssuerPaymentDetail paymentDetail = new IssuerPaymentDetail();
		if(reply.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID))
		{
			String transactionId = (String) reply.get(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID);
			paymentDetail.setTransactionId(transactionId);
		}
		paymentDetail.setAmount(amount);
		
		paymentDetail.setPaymentMethods(paymentTypes);
		paymentDetail.setIssuerPayment(issuerPayments);
		paymentDetail.setLastUpdatedBy(userId);
		
		//Setting flag as in process
		String isStandaloneCreditSupported = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_STANDALONE_CREDIT_SUPPORTED);
		if("TRUE".equalsIgnoreCase(isStandaloneCreditSupported)){
			paymentDetail.setIsEdiGenerated(EDIGeneratedFlag.N);
		}
		else{
			paymentDetail.setIsEdiGenerated(EDIGeneratedFlag.P);
		}

		/*if(reply.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_ECDEBIT_RECONCILIATIONID))
		{
			paymentDetail.setReconciliationId((String)reply.get(CyberSourceResponseMapKeys.CYBERSOURCE_ECDEBIT_RECONCILIATIONID));
		}*/
		if(reply.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_MERCHANTREFERENCECODE))
		{
			paymentDetail.setMerchantRefCode((String)reply.get(CyberSourceResponseMapKeys.CYBERSOURCE_MERCHANTREFERENCECODE));
		} 
		if(reply.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_ECCREDIT_PROCESSOR_TRANSACTION_ID))
		{
			paymentDetail.setTransactionId((String)reply.get(CyberSourceResponseMapKeys.CYBERSOURCE_ECCREDIT_PROCESSOR_TRANSACTION_ID));
		}
		// Setting this Payment PREM, as we don't deal with -ve amount for NM release   
		//Added Payment Type PREM,PREMADJ for both +ve/-ve amount
		if(amount.compareTo(BigDecimal.ZERO) == FinanceConstants.NUMERICAL_1){
			paymentDetail.setExchangePaymentType(ExchangePaymentType.PREM);
		}else{
			paymentDetail.setExchangePaymentType(ExchangePaymentType.PREMADJ);
		}
		paymentDetail.setIssuer(issuer);
		LOGGER.info("END - savePaymentDetail(-) method SUCCESSFULLY");
		return issuerPaymentDetailService.saveIssuerPaymentDetail(paymentDetail);
	}

	/**
	 * Method getFinancialInfo.
	 * @param issuer Issuer
	 * @param paymentMethods PaymentMethods
	 * @return FinancialInfo
	 */
	/*public FinancialInfo getFinancialInfo(Issuer issuer, PaymentMethods paymentMethods){
		FinancialInfo financialobj = null;

		if(paymentMethods!=null)
		{
			LOGGER.info("STARTED- getFinancialInfo(-) method started with paymentMethods: "+paymentMethods.getPaymentMethodName()+" ==========");
			financialobj = financialInfoService.findById(paymentMethods.getFinancialInfo().getId());
			//Email is set from Issuers Representative whose primary contact is Yes
			IssuerRepresentative issuerRepresentative = issuerRepresentativeRepository.findByIssuerAndPrimaryContact(issuer, PrimaryContact.YES);
			AccountUser user = userService.findById(issuerRepresentative.getUserRecord().getId());
			financialobj.setEmail(user.getEmail());

			if(financialobj.getBankInfo()!=null)
			{
				BankInfo bankInfo = new BankInfo();
				bankInfo.setAccountNumber(financialobj.getBankInfo().getAccountNumber());
				bankInfo.setAccountType(financialobj.getBankInfo().getAccountType());
				bankInfo.setRoutingNumber(financialobj.getBankInfo().getRoutingNumber());
				financialobj.setBankInfo(bankInfo);
			}
		}
		LOGGER.info("END- getFinancialInfo(-) ends successfully with financialobj: "+financialobj.getId()+" ==========");
		return financialobj;
	}*/

	/**
	 * Method getMapValue.
	 * @param map Map
	 * @return String
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	private String getMapValue(Map map) 
	{
		LOGGER.info("START- getMapValue(-) method started");
		StringBuilder dest = new StringBuilder();

		if (map != null && !map.isEmpty())
		{
			Iterator itr = map.entrySet().iterator();
			String key, val;
			while (itr.hasNext())
			{
				Map.Entry entry = (Map.Entry)itr.next();
				key = (String) entry.getKey();
				val = (String) entry.getValue();
				dest.append(key + "=" + val + ",");
			}
		}
		LOGGER.info("END- getMapValue(-) method End ");
		return dest.toString();
	}

	/**
	 * Method getRequiredDateRange.
	 * @return Date[]
	 */
	public Date[] getRequiredDateRange()
	{
		LOGGER.info("START - getRequiredDateRange(-) method started");
		//Two months date range is retuned ex 12-01-2012 to 31-01-2013
		DateTime begining = TSDateTime.getInstance();
		DateTime end = TSDateTime.getInstance();
		begining = begining.minusMonths(FinanceConstants.NUMERICAL_1).withDayOfMonth(FinanceConstants.NUMERICAL_1);
		end = begining.plusMonths(FinanceConstants.NUMERICAL_2).withDayOfMonth(1).minusDays(FinanceConstants.NUMERICAL_1);

		Date[] dates = new Date[FinanceConstants.NUMERICAL_2];
		dates[FinanceConstants.NUMERICAL_0] = begining.toDate();
		dates[FinanceConstants.NUMERICAL_1] = end.toDate();
		LOGGER.info("END - getRequiredDateRange(-) method End");
		return dates;
	}

	/**
	 * Method payIssuerUsingEcheck.
	 * @param pCust Map
	 * @param amount String
	 * @param isPaymentGateway String
	 * @return HashMap
	 * @throws GIException
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	private Map payIssuerUsingEcheck(Map pCust, String amount, String isPaymentGateway)throws GIException
	{
		LOGGER.info("START - payIssuerUsingEcheck(-,-,-,-) method started with amount:"+amount+"isPaymentGateway: "+isPaymentGateway);
		if(isPaymentGateway !=null && isPaymentGateway.equalsIgnoreCase(FinanceConstants.TRUE))
		{
			LOGGER.info("END - payIssuerUsingEcheck(-,-,-,-) ends successfully with pciCredit");
			return PaymentProcessorStrategy.echeckPaymentProcessor.pciCredit(pCust, amount);
		}
		else
		{
			LOGGER.info("END - payIssuerUsingEcheck(-,-,-,-) ends successfully with Credit");
			return PaymentProcessorStrategy.echeckPaymentProcessor.credit(pCust, amount);
		}
	}

	/**
	 * Method refundIssuerUsingEcheck.
	 * @param pCust Map
	 * @param amount String
	 * @param isPaymentGateway String
	 * @return HashMap
	 * @throws GIException
	 */
	@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_RAWTYPES)
	private Map refundIssuerUsingEcheck(Map pCust, String amount, String isPaymentGateway)throws GIException
	{
		LOGGER.info("START::payIssuerUsingEcheck(-,-,-,-) method started with amount:"+amount+"isPaymentGateway:"+isPaymentGateway);
		if(isPaymentGateway != null && isPaymentGateway.equalsIgnoreCase(FinanceConstants.TRUE))
		{
			LOGGER.info("END: captureEmployerEcheck(-,-,-,-) ends successfully with pciDebit");
			return PaymentProcessorStrategy.echeckPaymentProcessor.pciDebit(pCust, amount);
		}
		else
		{
			LOGGER.info("END:captureEmployerEcheck(-,-,-,-) ends successfully with Debit");
			return PaymentProcessorStrategy.echeckPaymentProcessor.debit(pCust, amount);
		}
	}

	/**
	 * Method generateRemittanceInfo.
	 * @throws GIException
	 * @see com.getinsured.hix.finance.issuer.service.IssuerPaymentProcessingService#generateRemittanceInfo()
	 */
	@Override
	public void generateRemittanceInfo()throws GIException 
	{
		LOGGER.info("START-generateRemittanceInfo() method started");
		Map<Integer, Exception> exceptionDetails = null;
		/**
		 * by this service call we will be fetching all paymentdetail data where EDIGeneratedFlag is 'N'
		 */
		String createdOnDate = getDateForRemittance("yyyyMMdd");
		LOGGER.info("EDI820: In Remittance report generation ============= Creation Date: "+createdOnDate);
		List<Integer> distinctIssuerList = issuerPaymentDetailService.getDistinctIssuerIdEDINotGenerated();
		if(distinctIssuerList != null && !distinctIssuerList.isEmpty())
		{
			exceptionDetails = new HashMap<Integer, Exception>();
			for(Integer issuerId : distinctIssuerList)
			{
				Issuer issuer =  iIssuerRepository.findOne(issuerId);
				try{
					generateRemittanceFile(issuer, createdOnDate);
				}catch(GIException giEx){
					LOGGER.error("EXCEPTION OCCURRED WHILE WHILE GENERATING REMITTANCE REPORT FOR THE ISSUER ID: "+issuerId, giEx);
					exceptionDetails.put(issuerId, giEx);

				}catch(Exception ex){
					LOGGER.error("EXCEPTION OCCURRED WHILE WHILE GENERATING REMITTANCE REPORT FOR THE ISSUER ID: "+issuerId, ex);
					exceptionDetails.put(issuerId, ex);
				}
			}
			if(!exceptionDetails.isEmpty()){
				String exceptionMessage = FinanceGeneralUtility.constructSingleMessageForExceptionDetails("EXCEPTIONS OCCURRED WHILE GENERATING REMITTANCE REPORT FOR THE FOLLWING ISSUERS:", "ISSUER ID", exceptionDetails);
				throw new GIException(exceptionMessage);
			}
		}
		else
		{
			LOGGER.info("EDI820: No IssuerPayment deatils exists with with EDIGeneratedFlag as 'N' ");
		}
		LOGGER.info("END::generateRemittanceInfo() method End");
	}


	/**
	 * Method generateRemittanceFile.
	 * @param issuerPaymentDetail IssuerPaymentDetail
	 * @param createdOnDate String
	 */
	private void generateRemittanceFile(Issuer issuer, String createdOnDate) throws GIException, JAXBException
	{
		LOGGER.info("START-GenerateRemittanceFile() method started with created on:"+createdOnDate);
		List<IssuerPaymentDetail> issuerPaymentDetailList =  issuerPaymentDetailService.findPaymentDtlByIssuer(issuer.getId());
		String hiosIssuerId = issuer.getHiosIssuerId();
		AccountUser user = FinanceGeneralUtility.getAccountUser(userService);
		if (issuerPaymentDetailList != null && !issuerPaymentDetailList.isEmpty())
		{
			LOGGER.info("EDI820: in generateRemittanceFile method ");
			List<IssuerPaymentDetail> issuerPymtDtlSaveList = new ArrayList<IssuerPaymentDetail>();
			String outboundReconXmlExtractPath = "";
			ExchgPartnerLookup exchgPartnerLookup = null;
			RemittanceInfoEDI paymentInfoEDI = new RemittanceInfoEDI();
			RemittanceInfoEDI.Payment payment = new RemittanceInfoEDI.Payment();

			exchgPartnerLookup = financeIssuerExchgPartnerLookupService.findExchgPartnerByHiosIssuerID(
					hiosIssuerId, GhixConstants.ENROLLMENT_TYPE_SHOP, GhixConstants.OUTBOUND, FinanceConstants.ST01_EDI20);
			LOGGER.info("EDI820: Generating Remittance report for Issuer: "+ issuer.getName() + " HIOS_ISSUER_ID: "+ hiosIssuerId);
			paymentInfoEDI.setHIOSIssuerID(hiosIssuerId);
			paymentInfoEDI.setTxnCreateDateTime(getDateForRemittance("yyyyMMddHHmmss"));
			payment.setCheckIssueOrEffectiveDate(createdOnDate);
			payment.setPayeeOrganizationName(issuer.getName());
			String federalEin = issuer.getFederalEin(); 
			if(StringUtils.isNotEmpty(federalEin) && federalEin.contains("-"))
			{
				federalEin = federalEin.replaceAll("-","");
			}
			payment.setPayeeFederalTIN(federalEin);
			payment.setPayerName(FinanceConstants.EDI820_PAYERNAME_SBE + DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
			payment.setPayerOriginatingCompanyNum(FinanceConstants.EDI820_PAYERNAME_SBE + DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));

			payment.setCreatedOn(createdOnDate);

			if (exchgPartnerLookup != null)
			{
				RemittanceInfoEDI.EDIInfo ediInfo = new RemittanceInfoEDI.EDIInfo();
				ediInfo.setGS02(exchgPartnerLookup.getGs02());
				ediInfo.setGS03(exchgPartnerLookup.getGs03());
				ediInfo.setISA05(exchgPartnerLookup.getIsa05());
				ediInfo.setISA06(exchgPartnerLookup.getIsa06());
				ediInfo.setISA07(exchgPartnerLookup.getIsa07());
				ediInfo.setISA08(exchgPartnerLookup.getIsa08());
				ediInfo.setISA15(exchgPartnerLookup.getIsa15());
				paymentInfoEDI.setEDIInfo(ediInfo);
				outboundReconXmlExtractPath = exchgPartnerLookup.getSourceDir();
			}

			List<RemittanceInfoEDI.Payment.RemittanceInfo> remittanceInfoList = new ArrayList<RemittanceInfoEDI.Payment.RemittanceInfo>();
			for(IssuerPaymentDetail issuerPaymentDetail : issuerPaymentDetailList)
			{
				IssuerPayments issuerPayments = issuerPaymentDetail.getIssuerPayment();
				if(issuerPayments == null)
				{
					throw new GIException(FinanceConstants.ERROR_CODE_201, "No Issuer Payment data found: IssuerPaymentDetail"+issuerPaymentDetail.toString(), FinanceConstants.EXCEPTION_SEVERITY_HIGH);
				}
				IssuerPaymentInvoice issuerPaymentInvoice = issuerPayments.getIssuerPaymentInvoice();
				if(issuerPaymentInvoice == null)
				{
					throw new GIException(FinanceConstants.ERROR_CODE_201, "No Issuer Payment Invoice data found: IssuerPaymentDetail"+issuerPaymentDetail.toString(), FinanceConstants.EXCEPTION_SEVERITY_HIGH);
				}
				IssuerRemittance issuerRemittance = issuerPaymentInvoice.getIssuerRemittance();
				if(issuerRemittance == null)
				{
					throw new GIException(FinanceConstants.ERROR_CODE_201, "No Issuer Invoice data found: IssuerPaymentDetail"+issuerPaymentDetail.toString(), FinanceConstants.EXCEPTION_SEVERITY_HIGH);
				}

				List<IssuerRemittanceLineItem> issuerRemittanceLineItemsList = issuerRemittanceLineItemService.getPaidIssuerLineItems(issuerRemittance.getId());
				for (IssuerRemittanceLineItem issuerRemittanceLineItem : issuerRemittanceLineItemsList) 
				{
					LOGGER.info("EDI:820 Looping IssuerInvoicesLineItems");
					RemittanceInfoEDI.Payment.RemittanceInfo remittanceInfo = new RemittanceInfoEDI.Payment.RemittanceInfo();
					RemittanceInfoEDI.Payment.RemittanceInfo.RemittanceDetail remittanceDetail = new RemittanceInfoEDI.Payment.RemittanceInfo.RemittanceDetail();
					EnrollmentRemittanceDTO enrollmentRemittanceDTO = financialMgmtUtils.getRemittanceDataByEnrollmentId(issuerRemittanceLineItem.getEnrollmentId());
					if (enrollmentRemittanceDTO != null)
					{
						/*remittanceInfo.setExchgAssignedPolicyID(enrollmentRemittanceDTO.getEnrollmentId());*/
						remittanceInfo.setIndivFirstName(enrollmentRemittanceDTO.getFirstName());
						remittanceInfo.setIndivLastName(enrollmentRemittanceDTO.getLastName());
						remittanceInfo.setIndivMiddleName(enrollmentRemittanceDTO.getMiddleName());
						remittanceInfo.setIndivNameSuffix(enrollmentRemittanceDTO.getSuffix());
						remittanceInfo.setIndivExchgAssignedSubscriberId(enrollmentRemittanceDTO.getExchgSubscriberIdentifier());
						remittanceInfo.setIndivExchgAssignedQHPid(enrollmentRemittanceDTO.getCMSPlanID());
						//Changes Done for Jira HIX-44617 to send enrollee.healthCoveragePolicyNo
						remittanceInfo.setIndivIssuerAssignedHealthInsurPolicyNum(enrollmentRemittanceDTO.getHealthCoveragePolicyNo());

						remittanceInfo.setIndivIssuerAssignedSubscriberNum(enrollmentRemittanceDTO.getIssuerSubscriberIdentifier());
						remittanceInfo.setIndivExchgAssignedEmployerGrpId( String.valueOf(issuerRemittanceLineItem.getEmployer().getId())); //THis change is for NM only
						remittanceInfo.setIndivExchgAssignedPolicyNum(Integer.toString(enrollmentRemittanceDTO.getEnrollmentId()));
						remittanceInfo.setCreatedOn(createdOnDate);
						//	if(issuerInvoicesLineItems.getAmtPaidToIssuer().compareTo(BigDecimal.ZERO) == 1)
						if(issuerRemittanceLineItem.getNetAmount().compareTo(BigDecimal.ZERO) == FinanceConstants.NUMERICAL_1)
						{
							remittanceDetail.setExchangePymtType(IssuerPaymentDetail.ExchangePaymentType.PREM.toString());
						}
						else
						{
							remittanceDetail.setExchangePymtType(IssuerPaymentDetail.ExchangePaymentType.PREMADJ.toString());
						}
						/*
						 * Period Covered updated from EmployerInvoiceLineItems
						 */
						remittanceDetail.setIndivCoveragePeriod(getPeriodCovered(issuerRemittanceLineItem.getEmployerInvoiceLineItems().getPeriodCovered()));
						//			remittanceDetail.setPymtAmt(getDecimalFormatValue(issuerInvoicesLineItems.getAmtPaidToIssuer()));
						remittanceDetail.setPymtAmt(getDecimalFormatValue(issuerRemittanceLineItem.getNetAmount()));
						remittanceDetail.setCreatedOn(createdOnDate);
						remittanceInfo.setRemittanceDetail(remittanceDetail);
						remittanceInfoList.add(remittanceInfo);
					}
				}
				payment.setCheckOrEFTtraceNum(Long.toString(FinanceConstants.EFTTRACE_NO_SERIES + issuerPaymentDetail.getId()));
				issuerPaymentDetail.setIsEdiGenerated(EDIGeneratedFlag.Y);
				issuerPaymentDetail.setLastUpdatedBy(user.getId());
				issuerPymtDtlSaveList.add(issuerPaymentDetail);
			}
			payment.setRemittanceInfo(remittanceInfoList);
			paymentInfoEDI.setPayment(payment);

			JAXBContext newcontext = JAXBContext.newInstance(RemittanceInfoEDI.class);
			Marshaller marshaller = newcontext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			if(StringUtils.isNotBlank(outboundReconXmlExtractPath) ){
				if (!(new File(outboundReconXmlExtractPath).exists())) {
					throw new GIException("EDI820 Generate Remittance SERVICE : No Source Directory Exists defined in ExchangePartner Lookup Table");
				}
			}else{
				throw new GIException("EDI820 Generate Remittance SERVICE :No Source Directory defined in ExchangePartner Lookup Table");
			}

			String outputPath =  outboundReconXmlExtractPath + File.separator+ "to_" + hiosIssuerId + "_" + 
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE) + FinanceConstants.EDI820_FILE_NAME_POSTFIX + "_"
					+ dateFormat.format(new TSDate()) + ".xml";
			LOGGER.info("EDI820:: Profile "+DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE) 
					+": Output FilePath and Filename: "+outputPath);


			marshaller.marshal(paymentInfoEDI, new StreamResult(new File(outputPath)));
			issuerPaymentDetailService.saveIssuerPymtDtlList(issuerPymtDtlSaveList);
			LOGGER.info("EDI820:: Completed generating EDI820 for Issuer HIOS_ISSUER_ID: "+hiosIssuerId);
		}
		else
		{
			LOGGER.info("EDI820:: No PaymentDetail found for Issuer HIOS_ISSUER_ID: "+hiosIssuerId);
		}
		LOGGER.info("END-GenerateRemittanceFile() method End.");
	}	

	/**
	 * Method getDateForRemittance.
	 * @param format String
	 * @return String
	 */
	private String getDateForRemittance(String format)
	{
		DateFormat dateFormatter = new SimpleDateFormat(format);

		return dateFormatter.format(new java.util.Date());
	}

	/**
	 * Method getPeriodCovered.
	 * @param input String
	 * @return String
	 */
	private String getPeriodCovered(String input)
	{
		LOGGER.info("START-getPeriodCovered(-) method started:");
		String formattedDate = "";
		try
		{
			String [] array = input.split("to");
			DateFormat inputformat = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MM_DD_YYYY);
			DateFormat outputformat = new SimpleDateFormat(FinanceConstants.EMP_ENRL_DATE_FORMAT);
			Date date;

			if(array.length > FinanceConstants.NUMERICAL_0)
			{
				for (int i = FinanceConstants.NUMERICAL_0; i < array.length; i++)
				{
					date = inputformat.parse(array[i].trim());
					formattedDate += outputformat.format(date) + "-";
				}
				formattedDate = formattedDate.substring(FinanceConstants.NUMERICAL_0, formattedDate.length() - 1);
			}

			LOGGER.info("Formatted Date :"+formattedDate);
		}
		catch(Exception ex)
		{
			LOGGER.error(""+ex);
		}
		LOGGER.info("END::getPeriodCovered(-) method end");
		return formattedDate;
	}

	/**
	 * Method getDecimalFormatValue.
	 * @param input float
	 * @return String
	 */
	private String getDecimalFormatValue(BigDecimal input)
	{
		return input.stripTrailingZeros().toPlainString();
	}
}
