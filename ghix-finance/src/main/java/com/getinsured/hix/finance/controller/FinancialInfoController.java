package com.getinsured.hix.finance.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.finance.PaymentMethodResponse;
import com.getinsured.hix.finance.paymentmethod.service.FinancialInfoService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceErrorCodes;
import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.thoughtworks.xstream.XStream;

/**
 * Controller class for financial info related services.
 * @since 17th December 2014
 * @author Khimla_S
 *
 */
@Controller
@RequestMapping("/financialinfo")
public class FinancialInfoController {

	private static final Logger LOGGER = LoggerFactory.getLogger(FinancialInfoController.class);
	
	@Autowired
	private FinancialInfoService financialInfoService;
	
	/**
	 * Validate Creditcard.
	 * @param FinancialInfo - financialInfo
	 * @return String - PaymentMethodResponse object as String.
	 */
	@RequestMapping(value = "/validatecreditcard", method = RequestMethod.POST, headers = "Accept=*/*")
	@ResponseBody public String validateCreditCard(@RequestBody FinancialInfo financialInfo)
	{
		LOGGER.debug("=============== Start :: Inside validatecreditcard ===============");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();		
		PaymentMethodResponse financeResponse = new PaymentMethodResponse();
		System.out.println("validateCreditCard3");
		try {
			if(financialInfo == null)
			{
				financeResponse.setErrMsg(FinanceConstants.ERROR_NULL_REQUEST);
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			else
			{	
				System.out.println("validateCreditCard4");
				financeResponse.setResponseString(financialInfoService.validateCreditCard(financialInfo));
				financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
		} catch (Exception e) {
			LOGGER.error("=============== Exception :: Inside validatecreditcard ===============", e);
			financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.VALIDATE_CREDITCARD.getCode());
			financeResponse.setErrMsg(e.getMessage());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		LOGGER.info("=============== End :: Inside validatecreditcard ===============");
		return xstream.toXML(financeResponse);
	}
	
}
