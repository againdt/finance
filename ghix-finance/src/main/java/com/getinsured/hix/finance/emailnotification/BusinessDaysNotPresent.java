package com.getinsured.hix.finance.emailnotification;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.notification.NotificationAgent;

@Component
public class BusinessDaysNotPresent extends NotificationAgent
{
	private Map<String, String> singleData;
	private Map<String, Object> emailData;
	public Map<String, Object> getEmailData() {
		return emailData;
	}

	public void setEmailData(Map<String, Object> emailData) {
		this.emailData = emailData;
	}

	public void setNotificationData(Map<String, Object> notificationData){
		this.notificationData = notificationData;
	}
	
	public Map<String, Object> getNotificationData(){
		return this.notificationData;
	}
	
	@Override
	public Map<String, String> getSingleData() {
		
		Map<String,String> bean = new HashMap<String, String>();

		DateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		
		bean.put("generatedDate", dateFormatter.format(new java.util.Date()));
		bean.put("title", "ALERT: Business holidays calendar not found.");
		//bean.putAll(singleData);
		setTokens(bean);
		
		Map<String, String> data = new HashMap<String, String>();
		data.put("To", (String)emailData.get("recipient"));
		//data.put("Subject", (String)singleData.get("subject"));
		
		if (singleData != null)
		{
			data.putAll(singleData);
		}
		return data;
		
	}
	
	public void updateSingleData(Map<String, String> singleData)
	{
		this.singleData = singleData;
	}
}
