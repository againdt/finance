package com.getinsured.hix.finance.payment.service.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.finance.payment.repository.IPaymentEventLogRepository;
import com.getinsured.hix.finance.payment.service.PaymentEventLogService;
import com.getinsured.hix.model.PaymentEventLog;
import com.getinsured.hix.model.PaymentEventLog.PaymentStatus;

/**
 * This service implementation class is for GHIX-FINANCE module for payment event report log.
 * This class has various methods to create, modify and query the PaymentEventLog model object.
 * @author Sharma_k
 * @since 22-Oct-2013
 *
 */
@Service("paymentEventLogService")
public class PaymentEventLogServiceImpl implements PaymentEventLogService 
{
	@Autowired
	private IPaymentEventLogRepository iPaymentEventLogRepository;
	
	@Override
	public PaymentEventLog savePaymentEventLog(PaymentEventLog paymentEventLog) 
	{
		return iPaymentEventLogRepository.save(paymentEventLog);
	}

	@Override
	public List<PaymentEventLog> getPaymentEventLogByEventAndRefNo(String merchantRefNo, String eventType)
	{
		return iPaymentEventLogRepository.getPaymentEventLogByRefNoAndEvent(merchantRefNo, eventType);
	}
	
	@Override
	public List<PaymentEventLog> getPymtEventLogByMerchantRefNo(String merchantRefNo)
	{
		return iPaymentEventLogRepository.getPymtEventLogByMerchanantRefCode(merchantRefNo);
	}

	@Override
	public List<PaymentEventLog> getPaymentEventLogByMerchantRefCodeList(
			List<String> merchantRefCode) {
		return iPaymentEventLogRepository.getPaymentEventLogByMerchantRefCodeList(merchantRefCode);
	}
	
	@Override
	public PaymentEventLog getPaymentEventLogForReconciliation(
			String merchantRefCode, String requestID, String transactionStatus) 
	{		
		return iPaymentEventLogRepository.getPaymentEventLogByMerchantRefCodeNReqIdNTxnStatus(merchantRefCode, requestID, transactionStatus);
	}

	@Override
	public PaymentEventLog updatePaymentStatus(Integer id,
			PaymentStatus paymentStatus) 
	{
		PaymentEventLog paymentEventLog = iPaymentEventLogRepository.findOne(id);
		if(paymentEventLog != null)
		{
			paymentEventLog.setPaymentStatus(paymentStatus);
			return iPaymentEventLogRepository.save(paymentEventLog);
		}
		return null;
	}
}
