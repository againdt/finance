package com.getinsured.hix.finance.employer.service.jpa;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.finance.employer.repository.IEmployerInvoicesRepository;
import com.getinsured.hix.finance.employer.service.EmployerInvoicesService;
import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.EmployerInvoiceLineItems.IsActiveEnrollment;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.EmployerInvoices.InvoiceType;
import com.getinsured.hix.model.EmployerInvoices.IsManualAdjustment;
import com.getinsured.hix.model.EmployerInvoices.PaidStatus;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;


/**
 * This service implementation class is for GHIX-FINANCE module for employer invoices.
 * This class has various methods to create/modify the EmployerInvoices model object.
 * 
 * @author sharma_k
 * @since 28-Jun-2013
 */
@Service("EmployerInvoicesService")
public class EmployerInvoicesServiceImpl implements EmployerInvoicesService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployerInvoicesServiceImpl.class);
	
	@Autowired private IEmployerInvoicesRepository employerInvoicesRepository;
	
	@SuppressWarnings("rawtypes")
	@Autowired private ObjectFactory<QueryBuilder> delegateFactory;
	
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	
	private static final String FILTER_STARTDATE = "startdate";
	private static final String FILTER_ENDDATE = "enddate";
	private static final String FILTER_PAYMENTDUEDATE = "paymentDueDate";
	private static final String FILTER_ID = "id";
	private static final String FILTER_PAIDSTATUS = "paidStatus";
	private static final String FILTER_DATEFORMAT = "MM/dd/yyyy";
	
	private static final String QUERY_SORTBY = "sortBy";
	private static final String QUERY_STARTRECORD = "startRecord";
	private static final String QUERY_PAGESIZE = "pageSize";
	
	private static final String RESPONSE_RECORDCOUNT = "recordCount";
	private static final String RESPONSE_INVOICELIST = "invoiceList";
	
	/*private static final String FILTER_ISACTIVE = "isActive";*/
	private static final String FILTER_KEY_ISACTIVE = "ISACTIVE";
	private static final String END_DATE = "12-31-2099";
	/*private static final String PAID_INVOICE_LIST ="paidInvoiceList";*/
	
	/**
	 * Method saveEmployerInvoices.
	 * @param employerInvoices EmployerInvoices
	 * @return EmployerInvoices
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#saveEmployerInvoices(EmployerInvoices)
	 */
	@Override
	@Transactional
	public EmployerInvoices saveEmployerInvoices(EmployerInvoices employerInvoices){
		return employerInvoicesRepository.saveAndFlush(employerInvoices);
	}
	
	/**
	 * Method findEmployerInvoicesByStatementDateAndPeriodCoveredAndEmployer.
	 * @param statementDate Date
	 * @param periodCovered String
	 * @param employerid int
	 * @return List<EmployerInvoices>
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#findEmployerInvoicesByStatementDateAndPeriodCoveredAndEmployer(Date, String, int)
	 */
	@Override
	@Transactional
	public List<EmployerInvoices> findEmployerInvoicesByStatementDateAndPeriodCoveredAndEmployer(Date statementDate,String periodCovered,int employerid){
		return employerInvoicesRepository.findEmployerInvoicesByStatementDateAndPeriodCoveredAndEmployer(statementDate, periodCovered, employerid);
	}
	
	/**
	 * Method findEmployerInvoicesById.
	 * @param invoiceid int
	 * @return EmployerInvoices
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#findEmployerInvoicesById(int)
	 */
	@Override	
	@Transactional(readOnly=true)
	public EmployerInvoices findEmployerInvoicesById(int invoiceid){
		/*try {*/
			if( employerInvoicesRepository.exists(invoiceid) ){
				return employerInvoicesRepository.findOne(invoiceid);
			}
		/*} catch (Exception e) {
			LOGGER.error("====== Exception occured during fetching employer invoice by id =========",e);
			throw new FinanceException(e);
		}*/
		return null;
	}
	
	/**
	 * Method searchEmployerInvoices.
	 * @param searchCriteria Map<String,Object>
	 * @return Map<String,Object>
	 * @throws FinanceException 
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#searchEmployerInvoices(Map<String,Object>)
	 */
	@Override	
    public  Map<String,Object> searchEmployerInvoices(Map<String,Object> searchCriteria) throws FinanceException{
		
		@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_UNCHECKED)
		QueryBuilder<EmployerInvoices> invoiceQuery = delegateFactory.getObject();
		invoiceQuery.buildObjectQuery(EmployerInvoices.class);
		Map<String,Object> employerInvoiceListAndRecordCount = new HashMap<String,Object>();
		List<EmployerInvoices> paymentTypes = null;
		DateFormat formatter;
	    formatter = new SimpleDateFormat(FILTER_DATEFORMAT);

		if(searchCriteria == null || searchCriteria.isEmpty())
		{
			employerInvoiceListAndRecordCount.put(RESPONSE_INVOICELIST, paymentTypes);
			employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, invoiceQuery.getRecordCount());
		}
		else
		{
			try {
				if(searchCriteria.get(FILTER_ID) != null && !StringUtils.isEmpty(searchCriteria.get(FILTER_ID).toString()))
				{
					int employerid = Integer.parseInt(searchCriteria.get(FILTER_ID).toString());
					invoiceQuery.applyWhere(FinanceConstants.EMPLOYER_DOT_ID, employerid, DataType.NUMERIC, ComparisonType.EQ);
				}
				if(searchCriteria.get(FILTER_STARTDATE) != null)
				{
					Date effectiveStartDate = formatter.parse((String) searchCriteria.get(FILTER_STARTDATE));
					invoiceQuery.applyWhere(FILTER_PAYMENTDUEDATE, effectiveStartDate, DataType.DATE, ComparisonType.GE);
				}
				if(searchCriteria.get(FILTER_ENDDATE) != null)
				{
					Date effectiveEndDate = formatter.parse((String) searchCriteria.get(FILTER_ENDDATE));
					invoiceQuery.applyWhere(FILTER_PAYMENTDUEDATE, effectiveEndDate, DataType.DATE, ComparisonType.LE);
				}
				if(searchCriteria.get(FILTER_PAIDSTATUS) != null)
				{					
					invoiceQuery.applyWhere(FILTER_PAIDSTATUS, PaidStatus.valueOf(searchCriteria.get(FILTER_PAIDSTATUS).toString()), DataType.ENUM, ComparisonType.EQUALS);
				}
				//the invoice against which reissue happened, those invoice should not be displayed to employer
				//only the newly reissued invoice and other normal invoice should be displayed to employer
				//thus adding this condition
				invoiceQuery.applyWhere("reissueId", FinanceConstants.NUMERICAL_0, DataType.NUMERIC, ComparisonType.EQ);
				invoiceQuery.applySort(searchCriteria.get(QUERY_SORTBY).toString(), SortOrder.valueOf(searchCriteria.get(FinanceConstants.SORT_ORDER).toString()));   
			
				invoiceQuery.setFetchDistinct(true);
				paymentTypes = invoiceQuery.getRecords((Integer)searchCriteria.get(QUERY_STARTRECORD), (Integer)searchCriteria.get(QUERY_PAGESIZE));
				employerInvoiceListAndRecordCount.put(RESPONSE_INVOICELIST, paymentTypes);
				employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, invoiceQuery.getRecordCount());
			} catch (ParseException e) {
				LOGGER.error("====== ParseException Exception occured during fetching employer invoices =========",e);
				employerInvoiceListAndRecordCount.put(RESPONSE_INVOICELIST, paymentTypes);
				employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, invoiceQuery.getRecordCount());
				throw new FinanceException(e);
			} catch (Exception e) {
				LOGGER.error("====== Exception occured during fetching employer invoices =========",e);
				employerInvoiceListAndRecordCount.put(RESPONSE_INVOICELIST, paymentTypes);
				employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, invoiceQuery.getRecordCount());
				throw new FinanceException(e);
			}
		}
		return employerInvoiceListAndRecordCount;
	}
	
	/**
	 * Method searchNotPaidEmployerInvoices.
	 * @param searchCriteria Map<String,Object>
	 * @return Map<String,Object>
	 * @throws FinanceException 
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#searchNotPaidEmployerInvoices(Map<String,Object>)
	 */
	/*@Override	
    public  Map<String,Object> searchNotPaidEmployerInvoices(Map<String,Object> searchCriteria) throws FinanceException{
		
		@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_UNCHECKED)
		QueryBuilder<EmployerInvoices> invoiceQuery = delegateFactory.getObject();
		invoiceQuery.buildObjectQuery(EmployerInvoices.class);
		Map<String,Object> employerInvoiceListAndRecordCount = new HashMap<String,Object>();
		String id = null;
		try {
			if(searchCriteria != null && searchCriteria.get(FILTER_ID) != null && !searchCriteria.get(FILTER_ID).equals("") ){
				id = (String) searchCriteria.get(FILTER_ID);
				int employerid = Integer.parseInt(id);
				
				invoiceQuery.applyWhere(FinanceConstants.EMPLOYER_DOT_ID, employerid, DataType.NUMERIC, ComparisonType.EQ);
			}
			
			//for make payment screen, we will retrieve all invoices whose status are "DUE" and who are active
			invoiceQuery.applyWhere(FILTER_PAIDSTATUS, PaidStatus.DUE, DataType.ENUM, ComparisonType.EQUALS);
			invoiceQuery.applyWhere(FILTER_ISACTIVE, FinanceConstants.DECISION_Y, DataType.STRING, ComparisonType.EQUALS);
			
			invoiceQuery.setFetchDistinct(true);
			List<EmployerInvoices> unpaidInvoicelist = invoiceQuery.getRecords((Integer)searchCriteria.get(QUERY_STARTRECORD), (Integer)searchCriteria.get(QUERY_PAGESIZE));
			employerInvoiceListAndRecordCount.put(RESPONSE_INVOICELIST, unpaidInvoicelist);
			employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, invoiceQuery.getRecordCount());
		} catch (Exception e) {
			LOGGER.error("======Exception occured during fetching not paid employer invoices=========",e);
			throw new FinanceException(e);
		}
		return employerInvoiceListAndRecordCount;
	}
	*/
	/**
	 * Method findEmployerInvoicesWhereInvoiceNotCreated.
	 * @return List<EmployerInvoices>
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#findEmployerInvoicesWhereInvoiceNotCreated()
	 */
	@Override	
	@Transactional(readOnly=true)
	public List<EmployerInvoices> findEmployerInvoicesWhereInvoiceNotCreated() {
		return employerInvoicesRepository.findByEcmDocIdIsNull();
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see com.getinsured.hix.shop.employer.service.EmployerInvoicesService#searchDueOnEmployerInvoices(java.util.Map)
	 * 
	 * This Implementation is written to retrieve data from EmployerInvoiced where Paid_status is "Due"
	 */
	
	/**
	 * Method searchPaidEmployerInvoices.
	 * @param searchCriteria Map<String,Object>
	 * @return Map<String,Object>
	 * @throws FinanceException 
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#searchPaidEmployerInvoices(Map<String,Object>)
	 */
	/*@Override
	public Map<String, Object> searchPaidEmployerInvoices(Map<String, Object> searchCriteria) throws FinanceException
	{
		@SuppressWarnings(FinanceConstants.SUPPRESS_WARNING_UNCHECKED)
		QueryBuilder<EmployerInvoices> invoiceQuery = delegateFactory.getObject();
		invoiceQuery.buildObjectQuery(EmployerInvoices.class);
		Map<String,Object> employerInvoiceListAndRecordCount = new HashMap<String,Object>();
		List<EmployerInvoices> paidInvoiceList = null;
		
		DateFormat formatter;
		formatter = new SimpleDateFormat(FILTER_DATEFORMAT);
		
		if(searchCriteria == null || searchCriteria.isEmpty())
		{
			employerInvoiceListAndRecordCount.put(PAID_INVOICE_LIST, paidInvoiceList);
			employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, invoiceQuery.getRecordCount());
		}
		else
		{
			try {
				if(searchCriteria.get(FILTER_ID) != null && !searchCriteria.get(FILTER_ID).equals("") )
				{
					int employerid = Integer.parseInt(searchCriteria.get(FILTER_ID).toString());
					invoiceQuery.applyWhere(FinanceConstants.EMPLOYER_DOT_ID, employerid, DataType.NUMERIC, ComparisonType.EQ);
				}
				if(searchCriteria.get(FILTER_STARTDATE) != null)
				{
					Date effectiveStartDate = formatter.parse((String) searchCriteria.get(FILTER_STARTDATE));
					invoiceQuery.applyWhere(FILTER_PAYMENTDUEDATE, effectiveStartDate, DataType.DATE, ComparisonType.GE);
				}
			
				if(searchCriteria.get(FILTER_ENDDATE) != null)
				{
					Date effectiveEndDate = formatter.parse((String) searchCriteria.get(FILTER_ENDDATE));
					invoiceQuery.applyWhere(FILTER_PAYMENTDUEDATE, effectiveEndDate, DataType.DATE, ComparisonType.LE);
				}
				
				//retrieves the invoices whose status is "Paid"
				
				 * Changes done by Kuldeep for Paid Status not in Due 
				 * so to retrieve only Paid and Partial_Paid records.
				 
				invoiceQuery.applyWhere(FILTER_PAIDSTATUS, PaidStatus.DUE, DataType.ENUM, ComparisonType.NE);
				invoiceQuery.applySort(searchCriteria.get(QUERY_SORTBY).toString(), SortOrder.valueOf(searchCriteria.get(FinanceConstants.SORT_ORDER).toString()));
				invoiceQuery.setFetchDistinct(true);
				paidInvoiceList = invoiceQuery.getRecords((Integer)searchCriteria.get(QUERY_STARTRECORD), (Integer)searchCriteria.get(QUERY_PAGESIZE));
				employerInvoiceListAndRecordCount.put(PAID_INVOICE_LIST, paidInvoiceList);
				employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, invoiceQuery.getRecordCount());
			} catch (ParseException e) {
				LOGGER.error("====== ParseException Exception occured during fetching paid employer invoices =========",e);
				employerInvoiceListAndRecordCount.put(RESPONSE_INVOICELIST, paidInvoiceList);
				employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, invoiceQuery.getRecordCount());
				throw new FinanceException(e);
			} catch (Exception e) {
				LOGGER.error("====== Exception occured during fetching paid employer invoices =========",e);
				employerInvoiceListAndRecordCount.put(RESPONSE_INVOICELIST, paidInvoiceList);
				employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, invoiceQuery.getRecordCount());
				throw new FinanceException(e);
			}
			
		}
		return employerInvoiceListAndRecordCount;
	}*/
	
	/**
	 * Method findEmployerInvoicesByEmployerAndInvoiceType.
	 * @param employer Employer
	 * @param invoicetype EmployerInvoices.InvoiceType
	 * @return EmployerInvoices
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#findEmployerInvoicesByEmployerAndInvoiceType(Employer, EmployerInvoices.InvoiceType)
	 */
	@Override	
	@Transactional(readOnly=true)
	public EmployerInvoices findEmployerInvoicesByEmployerAndInvoiceType(Employer employer, EmployerInvoices.InvoiceType invoicetype, String isActive) {
		return employerInvoicesRepository.findEmployerInvoicesByEmployerAndInvoiceTypeAndIsActive(employer,invoicetype, isActive);
	}
	
	/**
	 * Method updateInvoiceNumber.
	 * @param invoiceNumber String
	 * @param invoiceid int
	 * @return EmployerInvoices
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#updateInvoiceNumber(String, int)
	 */
	@Override
	@Transactional
	public EmployerInvoices updateInvoiceNumber(String invoiceNumber, int invoiceid)
	{
		EmployerInvoices employerInvoices = employerInvoicesRepository.findOne(invoiceid);
		employerInvoices.setInvoiceNumber(invoiceNumber);
		return employerInvoicesRepository.save(employerInvoices);
	}
		
	/**
	 * Method findLastPaidInvoice.
	 * @param employerid int
	 * @return EmployerInvoices
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#findPreviousInvoice(int)
	 */
	@Override
	@Transactional
	public EmployerInvoices findLastPaidInvoice(int employerid)
	{
		 return employerInvoicesRepository.findLastPaidInvoice(employerid);
	}
	
	/**
	 * Method findLastInvoice.
	 * @param employerid int
	 * @return EmployerInvoices
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#findLastInvoice(int)
	 */
	@Override
	@Transactional
	public EmployerInvoices findLastActiveInvoice(int employerid)
	{
		return employerInvoicesRepository.findLastActiveInvoice(employerid);
	}
	

	/**
	 * Method findPreviousInvoiceForReissue.
	 * @param employerid int
	 * @param invoiceid int
	 * @return EmployerInvoices
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#findPreviousInvoiceForReissue(int, int)
	 */
	@Override
	@Transactional
	public EmployerInvoices findLastInvoiceForReissue(int employerid,int invoiceid)
	{
		return employerInvoicesRepository.findLastInvoiceForReissue(employerid,invoiceid);
	}
	
	/**
	 * Method findDueEmeployerInvoices.
	 * @param isActive String
	 * @param paymentDueDate Date
	 * @return List<EmployerInvoices>
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#findDueEmeployerInvoices(String, Date)
	 */
	@Override
	@Transactional
	public List<EmployerInvoices> findDueEmeployerInvoices(String isActive, Date paymentDueDate)
	{
		return employerInvoicesRepository.findDueEmployerInvoices(isActive, paymentDueDate);
	}
	
	/**
	 * Method getBinderInvoicePaidStatusOfEmployer.
	 * @param employerid int
	 * @return EmployerInvoices.PaidStatus
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#getBinderInvoicePaidStatusOfEmployer(int)
	 */
	@Override
	@Transactional
	public EmployerInvoices.PaidStatus getBinderInvoicePaidStatusOfEmployer(int employerid){
		Employer employer = new Employer();
		employer.setId(employerid);
		EmployerInvoices employerInvoices = employerInvoicesRepository.findByInvoiceTypeAndEmployer(EmployerInvoices.InvoiceType.BINDER_INVOICE,employer);
		if(employerInvoices!=null)
		{
			return employerInvoices.getPaidStatus();
		}
		return null;
	}

	/**
	 * Method searchEmpInvoicesByIsActive.
	 * @param searchCriteria Map<String,Object>
	 * @return Map<String,Object>
	 * @throws FinanceException 
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#searchEmpInvoicesByIsActive(Map<String,Object>)
	 */
	@Override
	public Map<String, Object> searchEmpInvoicesByIsActive(Map<String, Object> searchCriteria) throws FinanceException
	{
		
		Map<String,Object> employerInvoiceListAndRecordCount = new HashMap<String,Object>();
		List<Map<String, Object>> paidInvoiceList = new ArrayList<Map<String,Object>>();
	  //  formatter = new SimpleDateFormat(FILTER_DATEFORMAT);
		DateFormat formatter = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MM_DD_YYYY);
		
	    int totalCount = 0;
	    if(searchCriteria == null || searchCriteria.isEmpty())
	    {
	    	employerInvoiceListAndRecordCount.put("invoiceList", paidInvoiceList);
	    	employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, Long.valueOf(totalCount));
	    }
	    else
	    {
	    	try {
	    		Date effectiveStartDate = null;
	    		if(searchCriteria.get(FILTER_STARTDATE) != null)
		    	{
	    			effectiveStartDate = formatter.parse((String) searchCriteria.get(FILTER_STARTDATE));	    			
		    	}
			
	    		Date effectiveEndDate = null;
		    	if(searchCriteria != null && searchCriteria.get(FILTER_ENDDATE) != null)
		    	{
		    		effectiveEndDate = formatter.parse((String) searchCriteria.get(FILTER_ENDDATE));		    		
		    	}
			
		    	String isActive = (String)searchCriteria.get(FILTER_KEY_ISACTIVE);
		    	
		    	int fromIndex = (Integer)searchCriteria.get(QUERY_STARTRECORD);
		    	int toIndex = fromIndex + (Integer)searchCriteria.get(QUERY_PAGESIZE);
		    	List<Object[]> objList = null;
		    	if(effectiveStartDate == null && effectiveEndDate == null)
		    	{
		    		objList= employerInvoicesRepository.findMultipleActiveDueEmployerInvoices(isActive);
		    	}
		    	else
		    	{
		    		if(effectiveEndDate == null)
		    		{
		    			effectiveEndDate=  formatter.parse(END_DATE);		    			
		    		}
		    		objList= employerInvoicesRepository.findMultipleActiveDueEmployerInvoicesDate(effectiveStartDate, effectiveEndDate, isActive);		    		
		    	}
		    	
		    	if(objList != null && !objList.isEmpty() )
		    	{
		    		totalCount = objList.size();
		    		if(objList.size() <  toIndex)
		    		{
		    			toIndex = objList.size();
		    		}
		    		List<Object[]> subList = objList.subList(fromIndex, toIndex);
		    		Map<String, Object> oMap = null;
			    	for(Object[] eachObj : subList)
			    	{
			    		oMap = new HashMap<String, Object>();
			    		if(null != eachObj[FinanceConstants.NUMERICAL_0])
			    		{
			    			oMap.put("id", eachObj[FinanceConstants.NUMERICAL_0]);
			    		}
			    		if(null != eachObj[FinanceConstants.NUMERICAL_1])
			    		{
			    			oMap.put(FinanceConstants.ECM_DOC_ID, eachObj[FinanceConstants.NUMERICAL_1]);
			    		}
			    		if(null != eachObj[FinanceConstants.NUMERICAL_2])
			    		{
			    			oMap.put("invoiceNumber", eachObj[FinanceConstants.NUMERICAL_2]);
			    		}
			    		if(null != eachObj[FinanceConstants.NUMERICAL_3])
			    		{
			    			oMap.put("employername", eachObj[FinanceConstants.NUMERICAL_3]);
			    		}
			    		if(null != eachObj[FinanceConstants.NUMERICAL_4])
			    		{
			    			oMap.put("employerid", eachObj[FinanceConstants.NUMERICAL_4]);
			    		}
			    		if(null != eachObj[FinanceConstants.NUMERICAL_5])
			    		{
			    			oMap.put("premiumThisPeriod", eachObj[FinanceConstants.NUMERICAL_5]);
			    		}
			    		if(null != eachObj[FinanceConstants.NUMERICAL_6])
			    		{
			    			oMap.put("paymentDueDate", eachObj[FinanceConstants.NUMERICAL_6]);
			    		}
			    		if(null != eachObj[FinanceConstants.NUMERICAL_7])
			    		{
			    			oMap.put("paidStatus", eachObj[FinanceConstants.NUMERICAL_7]);
			    		}
			    		if(null != eachObj[FinanceConstants.NUMERICAL_8])
			    		{
			    			oMap.put("totalAmountDue", eachObj[FinanceConstants.NUMERICAL_8]);
			    		}
			    		if(null != eachObj[FinanceConstants.NUMERICAL_9])
			    		{
			    			oMap.put("totalPaymentReceived", eachObj[FinanceConstants.NUMERICAL_9]);
			    		}
			    		if(null != eachObj[FinanceConstants.NUMERICAL_10])
			    		{
			    			oMap.put("isActive", eachObj[FinanceConstants.NUMERICAL_10]);
			    		}
		    	
			    		paidInvoiceList.add(oMap);
			    	}
		    	}	    	
		    	// the below condition is added for ecmDocId, because while marshalling the list containing the above columns values is failing if any value is null
		    	if(paidInvoiceList!=null && !paidInvoiceList.isEmpty()){
		    		for(int i = 0; i<paidInvoiceList.size(); i++){
		    			Map<String, Object> mapObj = paidInvoiceList.get(i);
		    			String ecmDocVal =  (String)mapObj.get(FinanceConstants.ECM_DOC_ID);
		    			if(ecmDocVal!=null){
		    				mapObj.put(FinanceConstants.ECM_DOC_ID, ecmDocVal);
		    			}else{
		    				mapObj.put(FinanceConstants.ECM_DOC_ID, "");
		    			}
		    			paidInvoiceList.set(i, mapObj);
		    		} 
		    	} 
		    	employerInvoiceListAndRecordCount.put("invoiceList", paidInvoiceList);
		    	employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, Long.valueOf(totalCount) );
			} catch (ParseException e) {
				LOGGER.error("====== ParseException Exception occured during fetching active employer invoices =========",e);
				employerInvoiceListAndRecordCount.put(RESPONSE_INVOICELIST, paidInvoiceList);
				employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, Long.valueOf(totalCount) );
				throw new FinanceException(e);
			} catch (Exception e) {
				LOGGER.error("====== Exception occured during fetching active employer invoices =========",e);
				employerInvoiceListAndRecordCount.put(RESPONSE_INVOICELIST, paidInvoiceList);
				employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, Long.valueOf(totalCount));
				throw new FinanceException(e);
			}
	    }
		return employerInvoiceListAndRecordCount;
	}

	/**
	 * Method disEnrollEmployers.
	 * @param paymentGracePeriod int
	 * @return List<EmployerInvoices>
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#disEnrollEmployers(int)
	 */
	@Override
	public List<EmployerInvoices> disEnrollEmployers(String isActive, InvoiceType invoiceType, Date currentDate) {
	
			return employerInvoicesRepository.findDisEnrollEmployers(isActive, invoiceType, currentDate);
	}

	/**
	 * Method updateECMDocId.
	 * @param ecmDocId String
	 * @param invoiceId int
	 * @return EmployerInvoices
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#updateECMDocId(String, int)
	 */
	@Override
	public EmployerInvoices updateECMDocId(String ecmDocId, int invoiceId, int userId) 
	{
		EmployerInvoices employerInvoices = employerInvoicesRepository.findOne(invoiceId);
		employerInvoices.setEcmDocId(ecmDocId);
		employerInvoices.setLastUpdatedBy(userId);
		return employerInvoicesRepository.save(employerInvoices);
	}

	/**
	 * Method findBinderDueEmployerInvoices.
	 * @param isActive String
	 * @param paymentDueDate Date
	 * @param invoiceType EmployerInvoices.InvoiceType
	 * @return List<EmployerInvoices>
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#findDueEmployerInvoices(String, Date, EmployerInvoices.InvoiceType)
	 */
	@Override
	public List<EmployerInvoices> findDueEmployerInvoices(
			String isActive,EmployerInvoices.InvoiceType invoiceType, Date currentDate) {
		return employerInvoicesRepository.findDueEmployerInvoices(isActive, invoiceType, currentDate);
	}
	
	@Override
	public EmployerInvoices getGivenPeriodCoveredEmployerInvoice(String periodcovered, int employerId)
	{
		return employerInvoicesRepository.findGivenPeriodCoveredEmployerInvoice(periodcovered,employerId);
	}
	
	
	/**
	 * Method findBinderDueEmployerInvoices.
	 * @param isActive String
	 * @param paymentDueDate Date
	 * @param invoiceType EmployerInvoices.InvoiceType
	 * @return List<EmployerInvoices>
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#findDueEmployerInvoices(String, Date, EmployerInvoices.InvoiceType)
	 */
	@Override
	public List<EmployerInvoices> findDueEmployerInvoicesForAutoPay(
			String isActive, Date paymentDueDate, EmployerInvoices.InvoiceType invoiceType) {
		return employerInvoicesRepository.findDueEmployerInvoicesForAutoPay(isActive, invoiceType, paymentDueDate);
	}
	
	@Override
	public EmployerInvoices findPreviousActiveDueInProcessInvoice(int employerId) {
		return employerInvoicesRepository.findPreviousActiveDueInProcessInvoice(employerId);
	}

	@Override
	public List<EmployerInvoices> findByEmployerAndIsActive(int employerId,
			String isActive) {
		 Employer employer = new Employer();
		 employer.setId(employerId);
		return employerInvoicesRepository.findByEmployerAndIsActive(employer, isActive);
	}
	
	@Override
	public List<EmployerInvoices> findInProcessInvoiceForReconcilation(PaidStatus paidStatus, Date paidDate) 
	{
		return employerInvoicesRepository.findInProcessInvoiceForReconcilation(paidStatus, paidDate);
	}
	
	@Override
	public List<EmployerInvoices> findActiveDueInvoicesByEmpIdNInvType(
			int employerId, EmployerInvoices.InvoiceType invoiceType) 
	{
		return employerInvoicesRepository.findActiveDueInvoicesByEmpIdNInvType(employerId, invoiceType);
	}

	@Override
	public EmployerInvoices findPreviousInvoice(int employerId,
			Date statementDate) {
		return employerInvoicesRepository.findPreviousInvoice(employerId, statementDate);
	}

	/**
	 * To fetch Employer invoices on the basis of PeriodCovered
	 */
	@Override
	public EmployerInvoices findInvoiceByTypeAndPeriodCovered(int employerId,
			InvoiceType invoicetype, String isActive, String periodCovered) 
	{
		return employerInvoicesRepository.findInvoiceByTypeAndPeriodCovered(employerId, invoicetype, isActive, periodCovered);
	}

	

	/*@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> searchPaymentHistory(Map<String, Object> searchCriteria) throws FinanceException 
	{
		QueryBuilder<EmployerInvoices> invoiceQuery = delegateFactory.getObject();
		invoiceQuery.buildObjectQuery(EmployerInvoices.class);
		Map<String,Object> employerInvoiceListAndRecordCount = new HashMap<String,Object>();
		List<EmployerInvoices> paidInvoiceList = null;
		
		if(searchCriteria == null || searchCriteria.isEmpty())
		{
			employerInvoiceListAndRecordCount.put(PAID_INVOICE_LIST, paidInvoiceList);
			employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, invoiceQuery.getRecordCount());
		}
		else
		{
			try {
				if(searchCriteria.get(FILTER_ID) != null && !searchCriteria.get(FILTER_ID).equals("") )
				{
					int employerid = Integer.parseInt(searchCriteria.get(FILTER_ID).toString());
					invoiceQuery.applyWhere(FinanceConstants.EMPLOYER_DOT_ID, employerid, DataType.NUMERIC, ComparisonType.EQ);
				}
				if(searchCriteria.get("invoiceIdList") != null)
				{
					invoiceQuery.applyWhere("id", (List<Integer>)searchCriteria.get("invoiceIdList"), DataType.LIST, ComparisonType.EQ);
				}
				invoiceQuery.applyWhere(FILTER_PAIDSTATUS, PaidStatus.PAID, DataType.ENUM, ComparisonType.EQ);
				invoiceQuery.applySort(searchCriteria.get(QUERY_SORTBY).toString(), SortOrder.valueOf(searchCriteria.get(FinanceConstants.SORT_ORDER).toString()));
				invoiceQuery.setFetchDistinct(true);
				paidInvoiceList = invoiceQuery.getRecords((Integer)searchCriteria.get(QUERY_STARTRECORD), (Integer)searchCriteria.get(QUERY_PAGESIZE));
				employerInvoiceListAndRecordCount.put(PAID_INVOICE_LIST, paidInvoiceList);
				employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, invoiceQuery.getRecordCount());
			}
			catch (Exception e) {
				LOGGER.error("====== Exception occured during fetching paid employer invoices =========",e);
				employerInvoiceListAndRecordCount.put(RESPONSE_INVOICELIST, paidInvoiceList);
				employerInvoiceListAndRecordCount.put(RESPONSE_RECORDCOUNT, invoiceQuery.getRecordCount());
				throw new FinanceException(e);
			}
			
		}
		return employerInvoiceListAndRecordCount;
	}*/

	@Override
	public List<Object[]> findEmployerInvoiceForPaidInvoice(int employerId, int employerEnrollmentId)
	{
		return employerInvoicesRepository.findEmployerInvoiceForPaidInvoice(employerId, employerEnrollmentId);
	}
	
	/**
	 * Method findInvoiceByEmployerIdNIsAcitveNIsActiveEmpEnrollId.
	 * @param employerId int
	 * @param isActiveEnrollment IsActiveEnrollment
	 * @param isActive String
	 * @param employerEnrollmentId int
	 * @return List<EmployerInvoices>
	 */
	@Override
	public List<EmployerInvoices> findInvoiceByEmployerIdNIsAcitveNIsActiveEmpEnrollId(int employerId, IsActiveEnrollment isActiveEnrollment,
																															String isActive, int employerEnrollmentId) {
		return employerInvoicesRepository.findInvoiceByEmployerIdNIsAcitveNIsActiveEmpEnrollId(employerId, isActiveEnrollment, isActive, employerEnrollmentId);
	}
	
	/**
	 * Method findPeriodCoveredAndInvoiceCreationDate.
	 * @param employerEnrollmentId int
	 * @return
	 */
	@Override
	public Object findPeriodCoveredAndInvoiceCreationDate(int employerEnrollmentId){
		return employerInvoicesRepository.findPeriodCoveredAndInvoiceCreationDate(employerEnrollmentId);
	}

	/**
	 * @Service implementation of getCountNoOfInvoiceInGracePeriod(-, -, -)
	 * @param employerId EmployerID
	 * @param employerEnrollmentId EmployerEnrollmentID
	 * @param currentDate Object<java.util.Date>
	 * @return long
	 */
	@Override
	public long getCountNoOfInvoiceInGracePeriod(int employerId,
			int employerEnrollmentId, Date currentDate) 
	{
		return employerInvoicesRepository.countGracePeriodInvoice(employerId, employerEnrollmentId, currentDate);
	}
	
		@Override
	public List<EmployerInvoices> findActiveInvoicesBeforeCurrentInvoiceDateAndStatusIn(
			Employer employer, String isActive,String invoiceNumber, List<PaidStatus> paidStatus,
			Date statementDate) {
		return employerInvoicesRepository.findByEmployerAndIsActiveAndInvoiceNumberNotAndPaidStatusInAndStatementDateBefore(employer, isActive, invoiceNumber, paidStatus, statementDate);
	}
	
	@Override
	public List<EmployerInvoices> findActiveNegativeDueInvoices(String isActive, PaidStatus paidStatus, BigDecimal totalAmountDue)
	{
		return employerInvoicesRepository.findByIsActiveAndPaidStatusAndTotalAmountDueLessThan(isActive, paidStatus, totalAmountDue);
	}
	
	/**
	 * Method searchEmployerInvoicesByECMIdNEmployerId.
	 * @param employerId
     * @param ecmDocId 
	 * @return EmployerInvoices
	 */
	@Override
	public EmployerInvoices searchEmployerInvoicesByECMIdNEmployerId(int employerId, String ecmDocId)
	{
		return employerInvoicesRepository.searchEmployerInvoicesByECMIdNEmployerId(employerId, ecmDocId);
	}
	
	@Override
	public long noOfBinderInvoiceForEmployerEnrollment(int employerID,
			int employerEnrollmentID) 
	{
		return employerInvoicesRepository.countNoOfBinderInvoiceForEmployerEnrollment(employerID, employerEnrollmentID);
	}

	@Override
	public EmployerInvoices findEmployerInvoiceByIdAndPaidStatusAndIsActiveAndIsAdjusted(int id, PaidStatus paidStatus, String isActive, IsManualAdjustment isAdjusted) 
	{
		return employerInvoicesRepository.findEmployerInvoiceByIdAndPaidStatusAndIsActiveAndIsAdjusted(id, paidStatus, isActive, isAdjusted);
	}

	@Override
	public List<EmployerInvoices> findInvoicesByEmployerAndIsActiveAndPaidStatusInAndAndInvoiceTypeIn(
			Employer employer, String isActive, List<PaidStatus> paidStatus,
			List<InvoiceType> invoiceType) {
		return employerInvoicesRepository.findByEmployerAndIsActiveAndPaidStatusInAndInvoiceTypeIn(employer, isActive, paidStatus, invoiceType);
	}
	
	@Override
	public List<EmployerInvoices> findInvoicesByIsActiveAndPaidStatusAndInvoiceTypeAndPaymentDueDate(String isActive, PaidStatus paidStatus, InvoiceType invoiceType, Date paymentDueDate)
	{
		return employerInvoicesRepository.findByIsActiveAndPaidStatusAndInvoiceTypeAndPaymentDueDate(isActive, paidStatus, invoiceType, paymentDueDate);
	}
	
	@Override
	public List<EmployerInvoices> findInprocessInvoicesWhenAutoPayHasDone(String isActive, InvoiceType invoiceType, Date autoPayDate)
	{
		return employerInvoicesRepository.findInprocessInvoicesWhenAutoPayHasDone(isActive, invoiceType, autoPayDate);
	}
	
	@Override
	public List<EmployerInvoices> findPositiveDueEmployerInvoicesForAutoPayNotice(String isActive, InvoiceType invoiceType, Date paymentDueDate, BigDecimal zero)
	{
		return employerInvoicesRepository.findPositiveDueEmployerInvoicesForAutoPayNotice(isActive, invoiceType, paymentDueDate, zero);
	}
	
	/**
	 * Method findByInvoiceTypePDFNotCreated.
	 * @return List<EmployerInvoices>
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#findByInvoiceTypePDFNotCreated(InvoiceType invoiceType)
	 */
	@Override	
	@Transactional(readOnly=true)
	public List<EmployerInvoices> findByInvoiceTypePDFNotCreated(InvoiceType invoiceType) {
		return employerInvoicesRepository.findByInvoiceTypeAndEcmDocIdIsNull(invoiceType);
	}
	
	
	/**
	 * Method findCancelledInvoiceForTermiantedReason.
	 * @param terminatedReason String
	 * @param paymentDueDate Date
	 * @param currentDate Date
	 * @return List<EmployerInvoices>
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicesService#findCancelledInvoiceForTermiantedReason(String, Date, Date)
	 */
	@Override
	@Transactional
	public List<EmployerInvoices> findCancelledInvoiceForTermiantedReason(String terminatedReason, EmployerInvoices.InvoiceType invoiceType,Date paymentDueDate, Date currentDate)
	{
		return employerInvoicesRepository.findCancelledInvoiceForTermiantedReason(terminatedReason, invoiceType, paymentDueDate, currentDate);
	}
	
	@Override
	public List<EmployerInvoices> findDueEmployerInvoicesForNotification(
			String isActive,EmployerInvoices.InvoiceType invoiceType, Date paymentDueDate) {
		return employerInvoicesRepository.findDueEmployerInvoicesForNotification(isActive, invoiceType, paymentDueDate);
	}
	
	@Override
	public List<EmployerInvoices> findEmployerInvoicesBasedOnPaymentDateAndSettlementDate(
			EmployerInvoices.InvoiceType invoiceType, Date settlementDate) {
		return employerInvoicesRepository.findEmployerInvoicesBasedOnPaymentDateAndSettlementDate(invoiceType,settlementDate);
	}
	
	
	@Override
	public List<Object> getInvoiceToBeCancelled(int  employerId, int employerEnrollId) {
		List<Object> rsList = null;
		
		EntityManager em = null;
		try{
			em = entityManagerFactory.createEntityManager();
			StringBuilder duplicateQuery = new StringBuilder("");
			duplicateQuery.append("select id from  employer_invoices where id in ");
			duplicateQuery.append("(select invoice_id from employer_invoice_lineitems,employer_invoices ");
			duplicateQuery.append("where invoice_id=employer_invoices.id and employer_enrollment_id=:employerEnrollId and isactive_enrollment='N' and employer_invoices.is_active = 'Y') ");
			duplicateQuery.append("and id not in (select invoice_id from employer_invoice_lineitems,employer_invoices  ");
			duplicateQuery.append("where invoice_id=employer_invoices.id and employer_enrollment_id<>:employerEnrollId and employer_invoices.is_active = 'Y' and invoice_id in ");
			duplicateQuery.append("(select invoice_id from employer_invoice_lineitems,employer_invoices ");
			duplicateQuery.append("where invoice_id=employer_invoices.id and employer_enrollment_id=:employerEnrollId and isactive_enrollment='N' and employer_invoices.is_active = 'Y')) ");
			duplicateQuery.append("union ");
			duplicateQuery.append("select id from employer_invoices where id in  ");
			duplicateQuery.append("(select invoice_id from employer_invoice_lineitems   ");
			duplicateQuery.append("where employer_enrollment_id=:employerEnrollId and isactive_enrollment='Y' and employer_id = :employerId)   ");
			duplicateQuery.append("and is_active='Y' and employer_id=:employerId   ");
			Query query = em.createNativeQuery(duplicateQuery.toString());
			query.setParameter("employerEnrollId", employerEnrollId);
			query.setParameter("employerId", employerId);
			rsList = query.getResultList();
			
		}catch(Exception ex){
			LOGGER.error("Exception occured in getInvoiceToBeCancelled: ", ex);
		}
		finally
        {
          if (em != null && em.isOpen())
          {
            try
            {
              em.close();
            }
            catch (IllegalStateException ex)
            {
              LOGGER.error("EntityManager is managed by application server", ex);
            }
          }
        }

		return rsList;
	}
	
	@Override
	@Transactional
	public int updateCancelStatus(String cancelReason,Date terminationDate,int userId,int invoiceId){
	return	employerInvoicesRepository.updateCancelStatus(cancelReason,terminationDate,userId,invoiceId);
	}
}	
