package com.getinsured.hix.finance.issuer.service;

import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.platform.util.exception.GIException;


public interface IssuerPaymentProcessingService 
{
	/**
	 * 
	 * @throws FinanceException
	 */
	/*void createIssuerInvoice() throws FinanceException;*/
	
	/**
	 * 
	 * @throws FinanceException
	 */
	void processPayment() throws FinanceException;
	
	/**
	 * 
	 * @throws GIException
	 */
	void generateRemittanceInfo()throws GIException;
}
