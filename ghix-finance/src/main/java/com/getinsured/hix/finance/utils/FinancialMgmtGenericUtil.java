package com.getinsured.hix.finance.utils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the generic utility class for GHIX-FINANCE module.
 * This class provides generic utility method's for finance module.
 * 
 * @author Sharma_k 
 * @since 17-Oct-2013
 */
public final class FinancialMgmtGenericUtil {
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FinancialMgmtGenericUtil.class);
	private FinancialMgmtGenericUtil()
	{
		
	}
	
	/**
	 * 
	 * @param param
	 * @return BigDecimal object with user input as value with scale 2 and rounding mode is ROUND_HALF_UP.
	 */
	public static BigDecimal getBigDecimalObj(Float param){
		
		if(param == null){
			return BigDecimal.ZERO;
		}else{
			return new BigDecimal(param.toString()).setScale(FinanceConstants.NUMERICAL_2, BigDecimal.ROUND_HALF_UP);
		}
	}
	
	/**
	 * 
	 * @param param 
	 * @return BigDecimal object with user input as value with scale 2 and rounding mode is ROUND_HALF_UP.
	 */
	public static BigDecimal getBigDecimalObj(Double param){
		
		if(param == null){
			return BigDecimal.ZERO;
			}else{
				return new BigDecimal(param.toString()).setScale(FinanceConstants.NUMERICAL_2, BigDecimal.ROUND_HALF_UP);
			}
	}
	
	/**
	 * 
	 * @param param
	 * @return BigDecimal object with user input as value with scale 2 and rounding mode is ROUND_HALF_UP.
	 */
	public static BigDecimal getBigDecimalObj(Integer param){
		if(param == null){
			return BigDecimal.ZERO;
			}else{
				return new BigDecimal(param.toString()).setScale(FinanceConstants.NUMERICAL_2, BigDecimal.ROUND_HALF_UP);
			}
	}
	
	
	/**
	 * 
	 * @param param
	 * @return BigDecimal object with value 0
	 */
	public static BigDecimal getDefaultBigDecimal(){
		
		return BigDecimal.ZERO;
	}
	
	/**
	 * 
	 * @param dividend
	 * @param divisor
	 * @return Returns a BigDecimal whose value is (dividend / divisor), whose scale is 2 and rounding mode is ROUND_HALF_UP.
	 */
	public static BigDecimal divide(BigDecimal dividend, BigDecimal divisor){
		BigDecimal result = BigDecimal.ZERO;
		try{
			
			result  = dividend.divide(divisor, FinanceConstants.NUMERICAL_2, BigDecimal.ROUND_HALF_UP);
			
		}catch(ArithmeticException exception){
			LOGGER.error("Error occured while making a division operation"+exception.getMessage(), exception);
		}
		return result;
	}
	
	/**
	 * 
	 * @param multiplicand
	 * @param multiplier
	 * @return Returns a BigDecimal whose value is (obj1 * obj2), whose scale is 2 and rounding mode is ROUND_HALF_UP.
	 */
	public static BigDecimal multiply(BigDecimal multiplicand, BigDecimal multiplier){
		
		return multiplicand.multiply(multiplier).setScale(FinanceConstants.NUMERICAL_2, BigDecimal.ROUND_HALF_UP);
	}
	
	/**
	 * 
	 * @param obj1
	 * @param obj2
	 * @return Returns a BigDecimal whose value is (obj1 + obj2), whose scale is 2 and rounding mode is ROUND_HALF_UP.
	 */
	public static BigDecimal add(BigDecimal obj1, BigDecimal obj2){
		return obj1.add(obj2).setScale(FinanceConstants.NUMERICAL_2, BigDecimal.ROUND_HALF_UP);
	}
	
	/**
	 * 
	 * @param obj1
	 * @param obj2
	 * @return  Returns a BigDecimal whose value is (obj1 - obj2), whose scale is 2 and rounding mode is ROUND_HALF_UP.
	 */
	public static BigDecimal subtract(BigDecimal obj1, BigDecimal obj2){
		return obj1.subtract(obj2).setScale(FinanceConstants.NUMERICAL_2, BigDecimal.ROUND_HALF_UP);
	}
	
	public static int compare(BigDecimal obj1, BigDecimal obj2){
		return obj1.compareTo(obj2);
	}
	
	/**
	 * 
	 * @param inputObj
	 * @return Return a BigDecimal with scale is 2.
	 */
	public static BigDecimal convertToTwoDecimals(BigDecimal conversionObj){
		return conversionObj.setScale(FinanceConstants.NUMERICAL_2);
	}
	
	public static XMLGregorianCalendar stringToXMLGregorianCalendar(String dateString) {
		XMLGregorianCalendar gregCal=null;
		if (StringUtils.isBlank(dateString)) { 
			return null;
		}else{
			try{
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date date = simpleDateFormat.parse(dateString);
			GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
			gregorianCalendar.setTime(date);
			gregCal = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gregorianCalendar.get(Calendar.YEAR), gregorianCalendar.get(Calendar.MONTH)+1, gregorianCalendar.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);
			return gregCal;
			}catch(ParseException | DatatypeConfigurationException e){
				return null;
			}
		}
	}
}
