package com.getinsured.hix.finance.cybersource.mock;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.joda.time.DateTime;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.finance.cybersource.service.PaymentProcessor;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceConstants.CyberSourceResponseMapKeys;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;
import com.getinsured.hix.platform.util.exception.GIException;

@SuppressWarnings("all")
@Component
public class CreditCardMockPaymentProcessor implements PaymentProcessor {

	@Override
	public Map authorize(Map pCust, String amount) {

		return null;
	}

	@Override
	public Map credit(Map pCust, String amount) {
		Map mockReply = null;
		if(!CollectionUtils.isEmpty(pCust)){
			mockReply = new HashMap();
			mockReply.put("ccCreditReply_amount", amount);
			mockReply.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, (String)pCust.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY));
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTTOKEN, "");
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE, "100");
			mockReply.put("ccCreditReply_requestDateTime", DateTime.now().toString());
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_DECISION, FinanceConstants.CYBERSOURCE_ACCEPT);
			mockReply.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, CyberSourceKeyConstants.PT_CURRENCY);
			mockReply.put("ccCreditReply_reasonCode", "100");
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID, RandomUtils.nextLong()+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9)+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9));
			mockReply.put("ccCreditReply_reconciliationID", RandomStringUtils.randomAlphanumeric(FinanceConstants.NUMERICAL_12));
		}
		return mockReply;
	}

	@Override
	public Map debit(Map pCust, String amount) {
		Map mockReply = null;
		if(!CollectionUtils.isEmpty(pCust)){
			mockReply = new HashMap();
			mockReply.put("ccCaptureReply_amount", amount);
			mockReply.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, (String)pCust.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY));
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTTOKEN, "");
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE, "100");
			mockReply.put("ccCaptureReply_requestDateTime", DateTime.now().toString());
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_DECISION, FinanceConstants.CYBERSOURCE_ACCEPT);
			mockReply.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, CyberSourceKeyConstants.PT_CURRENCY);
			mockReply.put("ccCaptureReply_reasonCode", "100");
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID, RandomUtils.nextLong()+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9)+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9));
			mockReply.put("ccCaptureReply_reconciliationID", RandomStringUtils.randomAlphanumeric(FinanceConstants.NUMERICAL_12));
		}
		return mockReply;	
	}

	@Override
	public Map pciAuthorize(Map pCust, String amount) throws GIException {

		return null;
	}

	@Override
	public Map pciCredit(Map pCust, String amount) throws GIException {
		Map mockReply = null;
		if(!CollectionUtils.isEmpty(pCust)){
			mockReply = new HashMap();
			String merchantRefCode = (String) pCust.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY);
			mockReply.put("ccCreditReply_amount", amount);
			mockReply.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, merchantRefCode);
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTTOKEN, "");
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE, "100");
			mockReply.put("ccCreditReply_requestDateTime", DateTime.now().toString());
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_DECISION, FinanceConstants.CYBERSOURCE_ACCEPT);
			mockReply.put("ccCreditReply_ownerMerchantID", DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MERCHANT_ID));
			mockReply.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, CyberSourceKeyConstants.PT_CURRENCY);
			mockReply.put("ccCreditReply_reasonCode", "100");
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID, RandomUtils.nextLong()+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9)+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9));
			mockReply.put("ccCreditReply_reconciliationID", RandomStringUtils.randomAlphanumeric(FinanceConstants.NUMERICAL_12));
		}
		return mockReply;
	}

	@Override
	public Map pciDebit(Map pCust, String amount) throws GIException {
		Map mockReply = null;
		if(!CollectionUtils.isEmpty(pCust)){
			mockReply = new HashMap();
			String merchantRefCode = (String) pCust.get(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY);
			mockReply.put("ccCaptureReply_amount", amount);
			mockReply.put(CyberSourceKeyConstants.MERCHANTREFERENCECODE_KEY, merchantRefCode);
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTTOKEN, "");
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE, "100");
			mockReply.put("ccCaptureReply_requestDateTime", DateTime.now().toString());
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_DECISION, FinanceConstants.CYBERSOURCE_ACCEPT);
			mockReply.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, CyberSourceKeyConstants.PT_CURRENCY);
			mockReply.put("ccCaptureReply_reasonCode", "100");
			mockReply.put(CyberSourceResponseMapKeys.CYBERSOURCE_REQUESTID, RandomUtils.nextLong()+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9)+""+RandomUtils.nextInt(FinanceConstants.NUMERICAL_9));
			mockReply.put("ccCaptureReply_reconciliationID", RandomStringUtils.randomAlphanumeric(FinanceConstants.NUMERICAL_12));
		}
		return mockReply;
	}

}
