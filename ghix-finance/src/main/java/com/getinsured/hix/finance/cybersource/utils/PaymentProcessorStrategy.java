package com.getinsured.hix.finance.cybersource.utils;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.getinsured.hix.finance.cybersource.mock.CreditCardMockPaymentProcessor;
import com.getinsured.hix.finance.cybersource.mock.ECheckMockPaymentProcessor;
import com.getinsured.hix.finance.cybersource.mock.serviceimpl.ReportDownloaderMockImpl;
import com.getinsured.hix.finance.cybersource.service.PaymentProcessor;
import com.getinsured.hix.finance.cybersource.service.ReportDownloader;
import com.getinsured.hix.finance.cybersource.serviceimpl.CreditCardProcessor;
import com.getinsured.hix.finance.cybersource.serviceimpl.ECheckProcessor;
import com.getinsured.hix.finance.cybersource.serviceimpl.ReportDownloaderImpl;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * 
 * Strategy class to decide the Processor for Payment and Report Download
 * @since 05th February 2015
 * @author Sharma_k
 *
 */
@Component
public class PaymentProcessorStrategy 
{
	public static PaymentProcessor echeckPaymentProcessor;
	public static PaymentProcessor creditCardPaymentProcessor;
	public static ReportDownloader reportDownloader;
	
	@Autowired private ECheckProcessor echeckProcessor;
	@Autowired private CreditCardProcessor creditCardProcessor;
	@Autowired private ReportDownloaderImpl reportDownloaderImpl;
	@Autowired private ReportDownloaderMockImpl reportDownloaderMockImpl;
	@Autowired private ECheckMockPaymentProcessor eCheckMockPaymentProcessor;
	@Autowired private CreditCardMockPaymentProcessor creditCardMockPaymentProcessor;
	
	/**
	 * Do not move this configuration to FinanceConstants
	 */
	public static String USE_MOCKFOR_PAYMENT_AND_REPORT;
	@Value("#{configProp['useMockForPaymentAndReport']}")
	public void setUSE_MOCKFOR_PAYMENT_AND_REPORT(String useMockForPaymentAndReport) {
		USE_MOCKFOR_PAYMENT_AND_REPORT = useMockForPaymentAndReport;
	}
	
	@PostConstruct
	public void  paymentAndReportProcessorInitializer()throws GIException
	{
		if(StringUtils.isNotEmpty(USE_MOCKFOR_PAYMENT_AND_REPORT))
		{
			
			if(USE_MOCKFOR_PAYMENT_AND_REPORT.equalsIgnoreCase(FinanceConstants.YES))
			{
				reportDownloader = reportDownloaderMockImpl;
				echeckPaymentProcessor = eCheckMockPaymentProcessor;
				creditCardPaymentProcessor = creditCardMockPaymentProcessor;
			}
			else if(USE_MOCKFOR_PAYMENT_AND_REPORT.equalsIgnoreCase(FinanceConstants.NO))
			{
				echeckPaymentProcessor = echeckProcessor;
				creditCardPaymentProcessor = creditCardProcessor;
				reportDownloader = reportDownloaderImpl;
			}
			else
			{
				throw new GIException("Expected value for Configuration 'useMockForPaymentAndReport' needs to be YES or NO instead of "+USE_MOCKFOR_PAYMENT_AND_REPORT);
			}
		}
		else
		{
			throw new GIException("Null or Empty value received for Configuration 'useMockForPaymentAndReport' Expected value YES or NO");
		}
	}	
}
