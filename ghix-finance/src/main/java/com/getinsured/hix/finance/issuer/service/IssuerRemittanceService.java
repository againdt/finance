package com.getinsured.hix.finance.issuer.service;

import java.math.BigDecimal;
import java.util.List;

import com.getinsured.hix.model.IssuerRemittance;
import com.getinsured.hix.model.IssuerRemittance.IsActive;
import com.getinsured.hix.model.IssuerRemittance.PaidStatus;

public interface IssuerRemittanceService 
{
	/**
	 * 
	 * @param issuerRemittance
	 * @return
	 */
	IssuerRemittance saveIssuerRemittance(IssuerRemittance issuerRemittance);
	
	/**
	 * 
	 * @param remittanceNumber
	 * @param remittanceId
	 * @return
	 */
	IssuerRemittance updateIssuerRemittanceNumber(String remittanceNumber, int remittanceId);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	IssuerRemittance findIssuerRemittanceById(int id);
	
	/**
	 * 
	 * @return
	 */
	List<IssuerRemittance> getUnpaidIssuerRemittance();
	
	/**
	 * 
	 * @param remittanceId
	 * @param paidStatus
	 * @param totalPaymentpaid
	 * @param amtReceivedFrmEmployer
	 * @return
	 */
	IssuerRemittance updateIssuerPaymentStatus(int remittanceId, PaidStatus paidStatus, float totalPaymentpaid, float amtReceivedFrmEmployer);
	//As per HIX-19676, data type float has been changed to BigDecimal
	
	/**
	 * 
	 * @param remittanceId
	 * @param paidStatus
	 * @param totalPaymentpaid
	 * @param amtReceivedFrmEmployer
	 * @return
	 */
	IssuerRemittance updateIssuerPaymentStatus(int remittanceId, PaidStatus paidStatus, BigDecimal totalPaymentpaid, BigDecimal amtReceivedFrmEmployer);
	
	/**
	 * 
	 * @param issuerId
	 * @return
	 */
	IssuerRemittance findIssuerRemittanceByIssuerId(int issuerId);
	
	/**
	 * 
	 * @param invoiceId
	 * @param isActive
	 * @return
	 */
	IssuerRemittance updateIssuerRemittanceActiveStatus(int invoiceId, IsActive isActive);
	
	/**
	 * 
	 * @param issuerId
	 * @return
	 */
	IssuerRemittance findPreviousIssuerRemittance(int issuerId);
	
	/**
	 * 
	 * @param paidStatus
	 * @param paidDate
	 * @return
	 */
	List<IssuerRemittance> findInProcessInvoiceForReconcilation(IssuerRemittance.PaidStatus paidStatus, java.util.Date paidDate);
}
