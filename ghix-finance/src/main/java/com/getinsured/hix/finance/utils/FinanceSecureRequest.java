package com.getinsured.hix.finance.utils;


import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestClientException;

import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * 
 * @author Sharma_k
 * @since 21st August 2013
 * Request file written to access Secured rest urls
 */
public class FinanceSecureRequest 
{
	
	public static enum RequestMethod
	{
		GET, POST;
	}
	
	private RequestMethod requestMethod;
	public RequestMethod getRequestMethod()
	{
		return requestMethod;
	}
	
	public void setRequestMethod(RequestMethod requestMethod)
	{
		this.requestMethod = requestMethod;
	}

	/**
	 * 
	 * @param ghixRestTemplate
	 * @param url
	 * @param params
	 * @param requestMethod
	 * @return
	 * @throws GIException
	 */
	public String sendSecureRequest(GhixRestTemplate ghixRestTemplate, String userName, String url, Object params, RequestMethod requestMethod)throws GIException
	{
		try
		{
			if(requestMethod.equals(RequestMethod.GET))
			{
				return (ghixRestTemplate.exchange(url, userName, HttpMethod.GET, MediaType.APPLICATION_JSON, String.class, params)).getBody();	
			}
			else
			{	
				return (ghixRestTemplate.exchange(url, userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, params)).getBody();
			}
		}
		catch(RestClientException re)
		{
			throw new GIException(re);
		}
	}
}
