package com.getinsured.hix.finance.issuer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.IssuerRepresentative.PrimaryContact;

/**
 */
public interface IIssuerRepresentativeRepository extends JpaRepository<IssuerRepresentative, Integer>
{
	/**
	 * Method findByIssuerAndPrimaryContact.
	 * @param issuer Issuer
	 * @param primaryContact PrimaryContact
	 * @return IssuerRepresentative
	 */
	IssuerRepresentative findByIssuerAndPrimaryContact(Issuer issuer, PrimaryContact primaryContact);
}
