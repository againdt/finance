package com.getinsured.hix.finance.cybersource.serviceimpl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;





import com.getinsured.hix.finance.cybersource.service.CustomerProfileInterface;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * This is a service implementation class for CustomerProfileInterface.
 * This class handles the request to create, update and query the customer profile.
 * 
 * @author Sharma_k
 * @since 24-Sep-2013
 */
@Component
public class CustomerProfileImpl implements CustomerProfileInterface 
{
	@Autowired private CustomerProfile customerProfile;
	
	/**
	 * Method createCustomerProfile
	 * @param createRequest Map 
	 * @return Map
	 * @throws GIException
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map createCustomerProfile(Map createRequest)throws GIException 
	{
		return customerProfile.createCustomerProfile(createRequest);
	}

	/**
	 * Method updateCustomerProfile
	 * @param updateRequest Map 
	 * @return Map
	 * @throws GIException
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map updateCustomerProfile(Map updateRequest) throws GIException 
	{
		return customerProfile.updateCustomerProfile(updateRequest);
	}

	/**
	 * Method retrieveCustomerProfile
	 * @param subscriptionId String 
	 * @return Map
	 * @throws GIException
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public Map retrieveCustomerProfile(String subscriptionId)throws GIException
	{
		return customerProfile.retrieveSubScriptionInfo(subscriptionId);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public Map pciCancelSubscription(String subscriptionID) throws GIException
	{
		return customerProfile.pciCancelSubscription(subscriptionID);
	}
}
