package com.getinsured.hix.finance.employer.service.jpa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.finance.employer.repository.IAdjustmentReasonCodeRepository;
import com.getinsured.hix.finance.employer.service.AdjustmentReasonCodeService;
import com.getinsured.hix.model.AdjustmentReasonCode;

/**
 * @since 12th September 2014
 * @author Sharma_k
 *
 */
@Service("adjustmentReasonCodeService")
public class AdjustmentReasonCodeServiceImpl implements AdjustmentReasonCodeService 
{
	@Autowired private IAdjustmentReasonCodeRepository iAdjustmentReasonCodeRepository; 
	
	@Override
	public AdjustmentReasonCode findById(Integer id)
	{
		if( iAdjustmentReasonCodeRepository.exists(id))
		{
			return iAdjustmentReasonCodeRepository.findOne(id);
		}
		return null;
    }

	@Override
	public List<AdjustmentReasonCode> findAllAdjustmentReasonCode() 
	{
		return iAdjustmentReasonCodeRepository.findAll();
	}

	@Override
	public List<String> findAllReasonType()
	{
		return iAdjustmentReasonCodeRepository.findAllReasonType();
	}

}
