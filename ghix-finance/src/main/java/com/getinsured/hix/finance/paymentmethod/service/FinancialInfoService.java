package com.getinsured.hix.finance.paymentmethod.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.BankInfo;
import com.getinsured.hix.model.CreditCardInfo;
import com.getinsured.hix.model.FinancialInfo;

/**
 * Service interface for FinancialInfo model related exposed services.
 * @since 17th December 2014
 * @author Khimla_S
 *
 */
public interface FinancialInfoService {

	List<FinancialInfo> findAll();

	Map<Integer, BankInfo> getBankInfo(List<FinancialInfo> financialInfos);

	Map<Integer, CreditCardInfo> getCreditCardInfo(
			List<FinancialInfo> financialInfos);

	String getBankJsonString(List<FinancialInfo> financialInfos);

	String getCreditCardJsonString(List<FinancialInfo> financialInfos);

	FinancialInfo saveFinancialInfoData(FinancialInfo financialInfo);

	// void updateFinancialInfo(FinancialInfo financialInfo);

	String validateCreditCard(FinancialInfo financialInfo);

	FinancialInfo findById(Integer id);
}
