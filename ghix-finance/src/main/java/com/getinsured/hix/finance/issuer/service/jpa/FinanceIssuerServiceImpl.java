package com.getinsured.hix.finance.issuer.service.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.finance.issuer.repository.IIssuerRepository;
import com.getinsured.hix.finance.issuer.service.FinanceIssuerService;
import com.getinsured.hix.model.Issuer;

/**
 * This service implementation class is for GHIX-FINANCE module to lookup for issuer. 
 * @author Sharma_k
 * @since 05-Aug-2013
 */
@Service("financeIssuerService")
public class FinanceIssuerServiceImpl implements FinanceIssuerService
{
	@Autowired private IIssuerRepository issuerRepository;

	/**
	 * Method getIssuerByHiosIDAndIssuerName.
	 * @param hiosIssuerID String
	 * @param issuerName String
	 * @return Issuer
	 * @see com.getinsured.hix.finance.issuer.service.FinanceIssuerService#getIssuerByHiosIDAndIssuerName(String, String)
	 */
	@Override
	public Issuer getIssuerByHiosIDAndIssuerName(String hiosIssuerID, String issuerName)
	{
		return issuerRepository.getIssuerByHiosIDAndIssuerName(hiosIssuerID, issuerName);
	}
	
	/**
	 * Method getIssuerByHiosIssuerID.
	 * @param hiosIssuerID String	 
	 * @return Issuer
	 * @see com.getinsured.hix.finance.issuer.service.FinanceIssuerService#getIssuerByHiosIssuerID(String)
	 */
	@Override
	public Issuer getIssuerByHiosIssuerID(String hiosIssuerID) 
	{
		return issuerRepository.getIssuerByHiosIssuerID(hiosIssuerID);
	}

}
