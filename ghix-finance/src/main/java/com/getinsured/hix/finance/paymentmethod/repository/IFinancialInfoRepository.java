package com.getinsured.hix.finance.paymentmethod.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.BankInfo;
import com.getinsured.hix.model.CreditCardInfo;
import com.getinsured.hix.model.FinancialInfo;

/**
 * 
 * JPA repository for FinancialInfo model
 *
 */
public interface IFinancialInfoRepository extends JpaRepository<FinancialInfo, Integer>
{	
	/**
	 * 
	 * @param bankinfo
	 * @return
	 */
	FinancialInfo findByBankInfo(BankInfo bankinfo);
	
	/**
	 * 
	 * @param creditcardInfo
	 * @return
	 */
	FinancialInfo findByCreditCardInfo(CreditCardInfo creditcardInfo);
}
