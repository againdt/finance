package com.getinsured.hix.finance.saml.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.enrollment.EnrollmentPaymentDTO;
import com.getinsured.hix.dto.finance.FinanceResponse;
import com.getinsured.hix.dto.planmgmt.IssuerPaymentInfoResponse;
import com.getinsured.hix.finance.utils.FinanceConstants.SAMLConstants;
import com.getinsured.hix.finance.utils.FinanceErrorCodes;
import com.getinsured.hix.finance.utils.FinancialMgmtUtils;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

/**
 * Handles requests for SAML payload request
 * @author Sahoo_s
 * @since 30 April 2014
 */
@Controller
@RequestMapping(value = "/finance/saml")
public class EmployeePaymentSAMLController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeePaymentSAMLController.class);
	//private static final String GHIX_ENVIRONMENT = "ghixEnvironment";
	
	/*@Autowired
	private PooledPBEStringEncryptor pooledConfigurationEncryptor;*/
    @Autowired
	private FinancialMgmtUtils financialMgmtUtils;
    @Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
    @Autowired private Gson platformGson;
	private String issuer_mock_url;
	private boolean mockEnabled;
	private boolean useDefaultKeystoreCreds = false;
	
	/*@Value("#{samlConfigProp}")
	private Properties samlConfigProp;
	
	@Value("#{configProp}")
	private Properties configProp;*/
    
	@Value("#{configProp['issuer.payment_redirect.useDefaultKeystore'] != null ? configProp['issuer.payment_redirect.useDefaultKeystore'] : 'true'}")
	public void setDefaultCredentials(String defaultCreds) {
		this.useDefaultKeystoreCreds = defaultCreds.trim().equalsIgnoreCase("true");
	}
	
    @Value("#{configProp['issuer.payment_redirect.mockUrl'] != null ? configProp['issuer.payment_redirect.mockUrl'] : 'http://localhost:8080/mock/paymentRedirect'}")
	public void setMockUrl(String mockUrl) {
		this.issuer_mock_url = mockUrl.trim();
	}
    
    @Value("#{configProp['issuer.payment_redirect.mockEnabled'] != null ? configProp['issuer.payment_redirect.mockEnabled'] : 'false'}")
   	public void setMockEnabled(String mockEnabled) {
   		this.mockEnabled = mockEnabled.trim().equalsIgnoreCase("true");
   	}
	
	/**
	 * Create SAML2.0 Assertion request 
	 * 			   for Carrier Premium Redirection
	 * @param paymentDTO
	 *            {@link EnrollmentPaymentDTO}
	 * @return financeResponse
	 *            {@link FinanceResponse}
	 */
	@RequestMapping(value="/paymentRedirect", method = RequestMethod.POST)
	@ResponseBody
	public String returnSAMLOutput(@RequestBody EnrollmentPaymentDTO paymentDTO) {
		String samlEncodedResponse = "";
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		if (paymentDTO == null) {
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			financeResponse.setErrMsg("Invalid input: enrollment DTO is null");
			return xstream.toXML(financeResponse);
		}
		try {
				String restResponse = financialMgmtUtils.getIssuerPaymentInfoByHiosId(paymentDTO.getHiosIssuerId());
				if(StringUtils.isNotBlank(restResponse))
				{
					//Gson gson = new Gson();
					IssuerPaymentInfoResponse issuerPaymentInfoResponse = platformGson.fromJson(restResponse, IssuerPaymentInfoResponse.class);
					
					Map<String, String> configParams = this.prepareConfigParams(issuerPaymentInfoResponse);
		
					samlEncodedResponse = new EmployeePaymentSAMLUtility().prepareSAMLOutput(configParams, paymentDTO);
					if (samlEncodedResponse == null) {
						financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						financeResponse.setErrMsg("Invalid input: SAML response cannot be null");
					}else {
						Map<String,Object> outputData = new HashMap<String, Object>();
						outputData.put(GhixConstants.SAML_RESPONSE, samlEncodedResponse);
						financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						financeResponse.setDataObject(outputData);
					}
				}
				else
				{
					LOGGER.debug("Issuer payment information not found for the hios_id: {} ",paymentDTO.getHiosIssuerId());
					financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					financeResponse.setErrMsg("Issuer payment information not found for the hios_id: "+paymentDTO.getHiosIssuerId());
				}

		} catch (Exception e) {
			LOGGER.error("Error occured while preparing SAML assertion : ", e);
			financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.SAMLERR.getCode());
			financeResponse.setErrMsg(e.getMessage());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return xstream.toXML(financeResponse);
	}
	
	/**
	 * Decode the SAML2.0 Assertion response 
	 * 			   for Carrier Premium Redirection
	 * @param String encodedSAML enconded SAML
	 *            {@link String}
	 * @return financeResponse
	 *            {@link FinanceResponse}
	 */
	@RequestMapping(value="/decodeSAMLOutput", method = RequestMethod.POST)
	@ResponseBody
	public String decodeSAMLOutput(@RequestBody String encodedSAML)
	{
		String samlEncodedResponse = encodedSAML;
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		FinanceResponse financeResponse = new FinanceResponse();
		try {
			Map<String,Object> outputData = new EmployeePaymentSAMLUtility().decodeSAMLResponse(samlEncodedResponse);
			if (samlEncodedResponse == null) {
				financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				financeResponse.setErrMsg("Invalid input: SAML response cannot be null");
			}else{
				financeResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				financeResponse.setDataObject(outputData);
			}
		} catch (Exception e) {
			LOGGER.error("Error occured while preparing SAML assertion : ", e);
			financeResponse.setErrCode(FinanceErrorCodes.ERRORCODE.SAMLERR.getCode());
			financeResponse.setErrMsg(e.getMessage());
			financeResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return xstream.toXML(financeResponse);
	}
	
	
	/**
	 * Use to capture the Security related information 
	 * 			   for Carrier Premium Redirection
	 * @param properties
	 *            {@link Properties}
	 * @param paymentDTO 
	 * 			  {@link EnrollmentPaymentDTO}
	 */
	private Map<String, String> prepareConfigParams(IssuerPaymentInfoResponse issuerPaymentInfoResponse) throws GIException
	{
		
		Map<String, String> inputParams = new HashMap<String, String>();
		inputParams.put(SAMLConstants.PAYREDIRECT_KEYSTORE_LOC, issuerPaymentInfoResponse.getKeyStoreFileLocation());
		inputParams.put(SAMLConstants.PAYREDIRECT_PRIVATE_KEY, issuerPaymentInfoResponse.getPrivateKeyName());
		if(!this.useDefaultKeystoreCreds) {
			inputParams.put(SAMLConstants.PAYREDIRECT_PASSWORD, ghixJasyptEncrytorUtil.decryptStringByJasypt(issuerPaymentInfoResponse.getPassword()));
			inputParams.put(SAMLConstants.PAYREDIRECT_SECUREKEY_PASSWORD, ghixJasyptEncrytorUtil.decryptStringByJasypt(issuerPaymentInfoResponse.getPasswordSecuredKey()));
		}else {
			inputParams.put(SAMLConstants.PAYREDIRECT_PASSWORD, "default");
			inputParams.put(SAMLConstants.PAYREDIRECT_SECUREKEY_PASSWORD, "default");
		}
		inputParams.put(SAMLConstants.ISSUER_NAME, issuerPaymentInfoResponse.getSecurityCertName());
		inputParams.put(SAMLConstants.SECURITY_DNS_NAME, issuerPaymentInfoResponse.getSecurityDnsName());
		inputParams.put(SAMLConstants.SECURITY_ADDRESS, issuerPaymentInfoResponse.getSecurityAddress());
		inputParams.put(SAMLConstants.SECURITY_KEY_INFO, issuerPaymentInfoResponse.getSecurityKeyInfo());
		inputParams.put(SAMLConstants.ISSUER_AUTH_URL, issuerPaymentInfoResponse.getIssuerAuthURL());
		if(this.mockEnabled) {
			inputParams.put(SAMLConstants.ISSUER_AUTH_URL, this.issuer_mock_url);
		}
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Configuration Parameters for SAML generation {}",inputParams);
		}
		
		return inputParams;
	}
}
