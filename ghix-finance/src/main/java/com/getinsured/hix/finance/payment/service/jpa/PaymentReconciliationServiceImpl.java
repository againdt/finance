package com.getinsured.hix.finance.payment.service.jpa;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.finance.payment.report.paymentevent.PaymentEventReport;
import com.getinsured.hix.dto.finance.payment.report.paymentevent.PaymentEventReport.Requests;
import com.getinsured.hix.dto.finance.payment.report.paymentevent.PaymentEventReport.Requests.Request;
import com.getinsured.hix.dto.finance.payment.report.paymentevent.PaymentEventReport.Requests.Request.Check;
import com.getinsured.hix.dto.finance.payment.report.singletxn.Report;
import com.getinsured.hix.dto.finance.payment.report.singletxn.Report.Requests.Request.ApplicationReplies;
import com.getinsured.hix.dto.finance.payment.report.singletxn.Report.Requests.Request.ApplicationReplies.ApplicationReply;
import com.getinsured.hix.dto.finance.payment.report.singletxn.Report.Requests.Request.PaymentData;
import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.finance.bank.service.BankHolidayService;
import com.getinsured.hix.finance.cybersource.utils.PaymentProcessorStrategy;
import com.getinsured.hix.finance.emailnotification.BusinessDaysNotPresent;
import com.getinsured.hix.finance.emailnotification.EmployerPaymentFailureNoticeForAdmin;
import com.getinsured.hix.finance.emailnotification.PaymentFailedUpdation;
import com.getinsured.hix.finance.employer.repository.IEmployerPaymentRepository;
import com.getinsured.hix.finance.employer.service.EmployerExchangeFeeService;
import com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService;
import com.getinsured.hix.finance.employer.service.EmployerInvoicePdfGenerationService;
import com.getinsured.hix.finance.employer.service.EmployerInvoicesService;
import com.getinsured.hix.finance.employer.service.EmployerPaymentInvoiceService;
import com.getinsured.hix.finance.employer.service.EmployerPaymentProcessingService;
import com.getinsured.hix.finance.issuer.service.IssuerPaymentDetailService;
import com.getinsured.hix.finance.issuer.service.IssuerPaymentInvoiceService;
import com.getinsured.hix.finance.issuer.service.IssuerRemittanceLineItemService;
import com.getinsured.hix.finance.issuer.service.IssuerRemittanceService;
import com.getinsured.hix.finance.notification.secureinbox.FinanceNotificationService;
import com.getinsured.hix.finance.payment.service.PaymentEventLogService;
import com.getinsured.hix.finance.payment.service.PaymentReconciliationService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceGeneralUtility;
import com.getinsured.hix.finance.utils.FinancialMgmtGenericUtil;
import com.getinsured.hix.finance.utils.FinancialMgmtUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.EmployerExchangeFees;
import com.getinsured.hix.model.EmployerExchangeFees.ExchangeFeeTypeCode;
import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.EmployerInvoiceLineItems.PaidToIssuer;
import com.getinsured.hix.model.EmployerInvoiceLineItems.Status;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.EmployerInvoices.PaidStatus;
import com.getinsured.hix.model.EmployerPaymentInvoice;
import com.getinsured.hix.model.EmployerPayments;
import com.getinsured.hix.model.IssuerPaymentDetail;
import com.getinsured.hix.model.IssuerPaymentDetail.EDIGeneratedFlag;
import com.getinsured.hix.model.IssuerPaymentInvoice;
import com.getinsured.hix.model.IssuerPayments;
import com.getinsured.hix.model.IssuerRemittance;
import com.getinsured.hix.model.IssuerRemittanceLineItem;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.PaymentEventLog;
import com.getinsured.hix.model.PaymentEventLog.PaymentStatus;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SHOPConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.notification.Notification;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * This service implementation class is for GHIX-FINANCE module for generating payment event report.
 * <p>
 *   Used in SHOP/ghix-batch only: shop/ghix-batch/src/main/java/com/getinsured/hix/batch/finance/task/PaymentEventReportTask.java
 * </p>
 *
 * @since 03rd Oct 2013
 * @author Sharma_k 
 * @version $Revision: 1.0 $
 */
@Service("paymentReconciliationService")
public class PaymentReconciliationServiceImpl implements PaymentReconciliationService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentReconciliationServiceImpl.class);
	
	@Autowired private ContentManagementService ecmService;
	@Autowired private IssuerPaymentDetailService issuerPaymentDetailService;
	@Autowired private IEmployerPaymentRepository iEmployerPaymentRepository;
	@Autowired private EmployerPaymentProcessingService employerPremiumPaymentProcessingService;
	@Autowired private EmployerInvoiceLineItemsService employerInvoiceLineItemsService;
	@Autowired private IssuerRemittanceLineItemService issuerInvoicesLineItemsService;
	@Autowired private IssuerRemittanceService issuerInvoicesService;
	@Autowired private EmployerInvoicesService employerInvoicesService;
	@Autowired private PaymentEventLogService paymentEventLogService;
	@Autowired private EmployerPaymentInvoiceService employerPaymentInvoiceService;
	@Autowired private FinanceNotificationService financeNotificationService;
	@Autowired private IssuerPaymentInvoiceService issuerPaymentInvoiceService;
	@Autowired private BankHolidayService bankHolidayService;
	@Autowired private BusinessDaysNotPresent businessDaysNotPresent;
	@Autowired private PaymentFailedUpdation paymentFailedUpdation;
	@Autowired private EmployerPaymentFailureNoticeForAdmin employerPaymentFailureNoticeForAdmin;
	@Autowired private EmployerExchangeFeeService employerExchangeFeeService;
	@Autowired private EmployerInvoicePdfGenerationService employerInvoicePdfGenerationService;
	@Autowired private FinancialMgmtUtils financialMgmtUtils;
	@Autowired private UserService userService;
		
	/**
	 *
	 * @throws GIException
	 * @see com.getinsured.hix.finance.payment.service.PaymentReconciliationService#processPaymentReport()
	 * To process both DEMO and LIVE PaymentEventReport on the basis of SEND_TO_PRODUCTION property
	 */
	@Override
	public void processPaymentReport()throws GIException
	{
		LOGGER.info("STARTED-ProcessPaymentReport() method.");
		String productionOrTestFlag = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.SEND_TO_PRODUCTION);
		String businessDaysReconcileRequired = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.BUSINESSDAYS_RECONLICE_REQUIRED);
		String paymentReconcileBusinessdays = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.PAYMENT_RECONCILE_BUSINESSDAYS);
		
		LOGGER.info(" Production Or TestFlag value for Process Payment Report"+productionOrTestFlag);
		if(StringUtils.isNotEmpty(productionOrTestFlag))
		{
			if(productionOrTestFlag.equalsIgnoreCase(FinanceConstants.TRUE))
			{
				productionPaymentReport();
				if(StringUtils.isNotEmpty(businessDaysReconcileRequired) && businessDaysReconcileRequired.equalsIgnoreCase(FinanceConstants.TRUE))
				{
					processSingleTxnReport(paymentReconcileBusinessdays, DynamicPropertiesUtil
							.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.PAYMENT_EVENT_REPORT_URL));
				}
			}
			else if(productionOrTestFlag.equalsIgnoreCase(FinanceConstants.FALSE))
			{
				/**
				 * for Testing Keeping Default fileName
				 */
				demoPaymentReportTest(FinanceConstants.DEFAULT_PAYMENTEVENT_FILENAME);
				if(StringUtils.isNotEmpty(businessDaysReconcileRequired) && businessDaysReconcileRequired.equalsIgnoreCase(FinanceConstants.TRUE))
				{
					processSingleTxnReport(paymentReconcileBusinessdays, DynamicPropertiesUtil
							.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.PAYMENT_EVENT_REPORT_URL));
				}
			}
			else
			{
				LOGGER.error("Configuration for {sendToProduction} property is contains invalid value");
				throw new GIException(FinanceConstants.ERROR_CODE_201, "Configuration for {sendToProduction} property is contains invalid value: "+productionOrTestFlag, "HIGH");
			}
		}
		else
		{
			LOGGER.error("Unable to find property {sendToProduction}, Check configuration.properties");
			throw new GIException(FinanceConstants.ERROR_CODE_201, "Unable to find property {sendToProduction}, Check configuration.properties", FinanceConstants.EXCEPTION_SEVERITY_HIGH);
		}
		LOGGER.info("END-ProcessPaymentReport(-) method");
	}
	
	/**
	 * 
	 * @throws GIException
	 * Method productionPaymentReport for production environment to download XML report from Cybersource 
	 */
	public void productionPaymentReport()throws GIException
	{
		LOGGER.info("STARTED::ProductionPaymentReport() method");
		try
		{
			String reportResponse = PaymentProcessorStrategy.reportDownloader.downloadPaymentEventReport();
			if(StringUtils.isNotEmpty(reportResponse))
			{
				LOGGER.info("PAYMENT_REPORT:: Received Response : "+reportResponse);
				processReportResponse(reportResponse);
			}
			else
			{
				LOGGER.error("PAYMENT_REPORT:: Received null or empty response....");
			}
			
		}
		catch(Exception ex)
		{
			LOGGER.error("PAYMENT_REPORT:: Exception caught: "+ex);
			throw new GIException(FinanceConstants.ERROR_CODE_201, ex.toString(), FinanceConstants.EXCEPTION_SEVERITY_HIGH);
		}
		LOGGER.info("END::ProductionPaymentReport() method ");
	}
	
	/**
	 * 
	 * @param paymentEventFileName
	 * @throws GIException
	 * Demo Payment Report for TEST environment, Parameter Payment File Name is required for reconcilation.
	 */
	public void demoPaymentReportTest(String paymentEventFileName)throws GIException
	{
		LOGGER.info("STARTED::DemoPaymentReportTest() method");
		/**
		 * either pass file name from batch job or for testing default fileName is used.
		 */
		try
		{
			processReportResponse(PaymentProcessorStrategy.reportDownloader.downloadReportFileManually(paymentEventFileName));
		}
		catch(Exception ex)
		{
			LOGGER.error("Demo Payment Event Exception caught: "+ex);
			throw new GIException(ex);
		}
		LOGGER.info("END-DemoPaymentReportTest() method");
	}
	
	/**
	 * @param is InputStream
	 * @return byte[]
	 * @throws IOException
	 * Java.io InputStream to ByteArray 
	 */
	public static byte[] toByteArrayUsingJava(InputStream is) throws IOException{
		LOGGER.info("STARTED::toByteArrayUsingJava() method");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int reads = is.read();
       
        while(reads != -1){
            baos.write(reads);
            reads = is.read();
        }
		LOGGER.info("END-toByteArrayUsingJava() method ");
        return baos.toByteArray();
       
    }
	
	/**
	 * 
	 * @param inputStream InputStream
	 * @return Report
	 * @throws GIException
	 * Create PaymentEventReport Object from String XML response.
	 */
	private PaymentEventReport createObject(String reportResponse)throws GIException
	{
		LOGGER.info("STARTED method - createObject() , Received response : "+reportResponse);
		PaymentEventReport reportObject = null;
		try
		{
			reportObject = (PaymentEventReport) GhixUtils.inputStreamToObject(IOUtils.toInputStream(reportResponse, FinanceConstants.ENCODING_UTF_8), PaymentEventReport.class);
		}
		catch(Exception e)
		{
			LOGGER.error("PAYMENT_REPORT:: Exception cauht while unmashalling. "+e);
			throw new GIException(FinanceConstants.ERROR_CODE_201, e.getMessage(), FinanceConstants.EXCEPTION_SEVERITY_HIGH);
		}
		LOGGER.info("END-Create Object() method ");
		return reportObject;
	}
	
	/**
	 * 
	 * @param reportResponse String 
	 * @throws GIException
	 * Processes the String XML response, Also saves the response @Alfresco.
	 */
	private void processReportResponse(String reportResponse) throws GIException
	{
		LOGGER.info("STARTED - Process Report Response() method");
		Map<String, Exception> paymentEventRptMap = null;
		PaymentEventReport report = createObject(reportResponse);
		AccountUser user = FinanceGeneralUtility.getAccountUser(userService);
		if(report != null)
		{
			paymentEventRptMap = new HashMap<String, Exception>();
			String ecmDocID = null;
			Requests requests = report.getRequests();
			if(requests != null && requests.getRequest() != null && !requests.getRequest().isEmpty())
			{
				try
				{
					ecmDocID = saveReportFileAtAlfresco(reportResponse.getBytes(), "PaymentEventReport");
				}
				catch(Exception ex)
				{
					LOGGER.error("Caught exception while saving file @ Alfresco: "+ex);
					throw new GIException("Exception caught while saving File @ Alfresco: "+ex.getMessage(),ex);
				}
				List<Request> requestList  = requests.getRequest();
				for(Request request : requestList)
				{
					try
					{
						Map<String, Object> paymentEventLogMap = new HashMap<String, Object>();
						String merchantRefNumber = request.getMerchantReferenceNumber();
						paymentEventLogMap.put(FinanceConstants.PaymentEventLogs.MERCHANT_REF_NUMBER_KEY, merchantRefNumber);
						LOGGER.info("PAYMENT_REPORT:: Received request : "+request.toString());
						Check checkObj = request.getCheck();
						if(checkObj != null)
						{
							LOGGER.info("PAYMENT_REPORT:: Received request : inside checkobj"+request.toString());							
							float amount = checkObj.getMerchantAmount();
							BigDecimal amountBigDec =FinancialMgmtGenericUtil.getBigDecimalObj(amount);
							String eventType = checkObj.getEvent();
							paymentEventLogMap.put(FinanceConstants.PaymentEventLogs.EVENT_TYPE_KEY, eventType);
							paymentEventLogMap.put(FinanceConstants.PaymentEventLogs.TRANSACTION_ID_KEY, checkObj.getTransactionReferenceNumber());
							paymentEventLogMap.put(FinanceConstants.PaymentEventLogs.REQUEST_ID_KEY, request.getRequestID());
							List<PaymentEventLog>  paymentEventLogList=paymentEventLogService.getPaymentEventLogByEventAndRefNo(merchantRefNumber, eventType); 
							if(paymentEventLogList == null || paymentEventLogList.isEmpty())
							{
								PaymentEventLog paymentEventLog = paymentEventLogService.getPaymentEventLogForReconciliation(merchantRefNumber, request.getRequestID(), FinanceConstants.CYBERSOURCE_ACCEPT);
								if(paymentEventLog != null)
								{
									LOGGER.info("PAYMENT_REPORT:: Received request : inside paymentEventLogList");
									if(eventType.equalsIgnoreCase(FinanceConstants.EVENT_TYPE_COMPLETED))
									{
										paymentEventLogService.updatePaymentStatus(paymentEventLog.getId(), PaymentStatus.CONFIRMED);
										LOGGER.info("PAYMENT_REPORT:: Received request : inside COMPLETED"+merchantRefNumber);
										if(merchantRefNumber.contains(FinanceConstants.ISS))
										{
											updateIssuerPayment(merchantRefNumber, amountBigDec,user);
										}
										else if(merchantRefNumber.contains(FinanceConstants.EMP))
										{
											updateEmployerPayment(merchantRefNumber, amountBigDec,user);
										}
										else
										{
											LOGGER.info("PAYMENT_REPORT:: No Data found for Employer & Issuer related to MerchantRefNumber : "+merchantRefNumber);
										}
									}
									else if(eventType.equalsIgnoreCase(FinanceConstants.EVENT_TYPE_DECLINED) || eventType.equalsIgnoreCase(FinanceConstants.EVENT_TYPE_ERROR) ||
											eventType.equalsIgnoreCase(FinanceConstants.EVENT_TYPE_FAILED) || eventType.equalsIgnoreCase(FinanceConstants.EVENT_TYPE_OTHER) || eventType.equalsIgnoreCase(FinanceConstants.EVENT_TYPE_STOP_PAYMENT)
											|| eventType.equalsIgnoreCase(FinanceConstants.EVENT_TYPE_VOID) || eventType.toUpperCase().contains(FinanceConstants.EVENT_TYPE_NSF))
									{
										paymentEventLogService.updatePaymentStatus(paymentEventLog.getId(), PaymentStatus.FAILED);
										LOGGER.info("PAYMENT_REPORT:: Received request : inside DECLINED"+merchantRefNumber);
										if(merchantRefNumber.contains(FinanceConstants.ISS))
										{
											updateIssuerPaymentFailedStatus(merchantRefNumber, amountBigDec,user.getId());
										}
										else if(merchantRefNumber.contains(FinanceConstants.EMP))
										{
											String failureReason = checkObj.getProcessorMessage();
											if( eventType.toUpperCase().contains(FinanceConstants.EVENT_TYPE_NSF))
											{
												failureReason = (!StringUtils.isEmpty(failureReason) ? failureReason : "Insufficient funds in your bank account.");
											}
											else{
												failureReason = (!StringUtils.isEmpty(failureReason) ? failureReason : "Payment failed due to some unavoidable reason, please contact administrator.");
											}
											updateEmployerPaymentFailedStatus(merchantRefNumber, failureReason, eventType, amountBigDec,user);
										}
										else
										{
											LOGGER.info("PAYMENT_REPORT:: No Data found for Employer & Issuer related to MerchantRefNumber : "+merchantRefNumber);
										}								
									}
									else
									{									
										LOGGER.info("PAYMENT_REPORT:: Skipping the response as it is not listed in processing events, Event Name:"+checkObj.getEvent());
									}
								}
								else
								{
									throw new GIException("No PaymentEventLog found for merchantRefNumber :"+merchantRefNumber);
								} 
							}							
						}
						else
						{
							LOGGER.error("PAYMENT_REPORT:: No Check record received in the response");
							//throw new GIException("No Check record received in the response, Report Object: "+report.toString());
						}
						//To Save Payment event log for the received data
						paymentEventLogMap.put(FinanceConstants.PaymentEventLogs.ECM_DOC_ID_KEY, ecmDocID);
						savePaymentEventLog(paymentEventLogMap,user.getId());
						}
						catch(Exception ex)
						{
							LOGGER.error("Exception Caught in PaymenEventReport Response processing : ", ex);
							paymentEventRptMap.put(request.getMerchantReferenceNumber(), ex);
						}
					}
					if(paymentEventRptMap != null && !paymentEventRptMap.isEmpty())
					{
						String exceptionMessage = FinanceGeneralUtility.constructSingleMessageForExceptionDetails("EXCEPTIONS OCCURRED WHILE PROCESSING PAYMENT EVENT REPORT:", "MerchantRef Number", paymentEventRptMap);
						throw new GIException(exceptionMessage);
					}
				}
				else
				{
					LOGGER.error("PAYMENT_REPORT:: Requests is null in Report object: "+report.toString());
					//throw new GIException("Requests is null in Report object, Report Object: "+report.toString());
				}
			}
			else
			{
				LOGGER.error("PAYMENT_REPORT:: No Data Received in respose Report");
				throw new GIException("No Data Received in respose Report");
			}
			LOGGER.info("END::Process Report Response() method");
	}
	
	/**
	 * 
	 * @param data byte[]
	 * @return String
	 * @throws ContentManagementServiceException
	 * Saves Byte[] Data @Alfresco with the passed parameter fileName.
	 */
	private String saveReportFileAtAlfresco(byte[] data, String fileName)throws ContentManagementServiceException
	{
		LOGGER.info("STARTED - saveReportFileAtAlfresco() method");
		String timeStamp = new SimpleDateFormat("_yyyyMMdd_HHmmssSSS").format(TSCalendar.getInstance().getTime());
		String savingName = fileName+timeStamp+".xml";
		LOGGER.info("PAYMENT_REPORT:: Before saving file, File name: "+ savingName);
		LOGGER.info("END -saveReportFileAtAlfresco() method");
		if(fileName.equals("SingleTxnReport")){
			return ecmService.createContent("Finance_PaymentReports", savingName, data, ECMConstants.Finance.DOC_CATEGORY,
					ECMConstants.Finance.SINGLE_TXN_REPORT, null);
		}
		else{
			return ecmService.createContent("Finance_PaymentReports", savingName, data, ECMConstants.Finance.DOC_CATEGORY,
					ECMConstants.Finance.PAYMENT_EVT_REPORT, null);
		}
	}
	
	/**
	 * 
	 * @param merchantRefNumber String
	 * @param paidAmount BigDecimal
	 * @throws GIException
	 * update Employer Payment against the Received response from Cybersource Report.
	 */
	private void updateEmployerPayment(String merchantRefNumber, BigDecimal paidAmount, AccountUser user)throws GIException
	{
		LOGGER.info("STARTED - updateEmployerPayment() method started with merchantRefNumber: "+merchantRefNumber+" paidAmount: "+paidAmount);
		EmployerPayments employerPayments = iEmployerPaymentRepository.findEmployerPaymentsByMerchantRefCode(merchantRefNumber);
		if(employerPayments != null)
		{
			BigDecimal merchantPaidAmount = paidAmount;
			if (Double.parseDouble(employerPayments.getTotalAmountDue().toString()) < 0 && paidAmount.negate().compareTo(employerPayments.getTotalAmountDue()) == 0){
				merchantPaidAmount = paidAmount.negate();
			}
			//String minimumPaymentLimit = FinanceConfigurationUtil.MINIMUM_PAYMENT_THRESHOLD;
			String minimumPaymentLimit = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.MINIMUM_PAYMENT_THRESHOLD);
			LOGGER.info("==Getting Employer Payment Invoice ==");
			EmployerPaymentInvoice employerPaymentInvoice = employerPayments.getEmployerPaymentInvoice();
			EmployerInvoices employerInvoices = employerPaymentInvoice.getEmployerInvoices();
			//Code changes has been made to execute the rest call before updating the Invoice status.
			List<EmployerPaymentInvoice> listofemployerpayment = null;
			List<EmployerInvoiceLineItems> employerInvoiceLineItemsList = null;
			if(employerInvoices != null)
			{
				String changeEnrollmentStatusEarly =  DynamicPropertiesUtil.getPropertyValue(SHOPConfiguration.SHOPConfigurationEnum.EMPLOYER_ENROLLMENTSTATUS_EARLY);
				if(StringUtils.isNotEmpty(employerInvoices.getInvoiceType().toString()) 
						&& "BINDER_INVOICE".equalsIgnoreCase(employerInvoices.getInvoiceType().toString()) && "NO".equalsIgnoreCase(changeEnrollmentStatusEarly))
				{
					employerPremiumPaymentProcessingService.updateEnrollmentStatus(employerInvoices);
				}
				
				LOGGER.info("Getting Invoice Line Item....");
				employerInvoiceLineItemsList = employerInvoiceLineItemsService.getInvoiceLineItem(employerInvoices);
				
				if(employerInvoiceLineItemsList != null && !employerInvoiceLineItemsList.isEmpty())
				{
					LOGGER.info("Getting Last Paid Invoice...");
					EmployerInvoices previousEmployerInvoices = employerInvoicesService.findLastPaidInvoice(employerInvoices.getEmployer().getId());
		    	
					if(previousEmployerInvoices != null)
					{
						LOGGER.info("Getting Payment Invoice by Employer Invoices.....");
						listofemployerpayment = employerPaymentInvoiceService.findPaymentInvoiceByEmployerInvoices(previousEmployerInvoices);
					}
			
					BigDecimal previousExcessAmtPaid = FinancialMgmtGenericUtil.getDefaultBigDecimal();
					if(listofemployerpayment!=null && !listofemployerpayment.isEmpty())
					{
						for (EmployerPaymentInvoice employerPaymentInvoiceitr : listofemployerpayment)
						{
							if("N".equalsIgnoreCase(employerPaymentInvoiceitr.getEmployerPayments().getIsRefund()))
							{
								previousExcessAmtPaid = employerPaymentInvoiceitr.getExcessAmount();
							}
						}
					}
					BigDecimal settlementAmt  = FinancialMgmtGenericUtil.add(previousExcessAmtPaid, merchantPaidAmount);
			
			
					if(merchantPaidAmount.compareTo(employerInvoices.getTotalAmountDue()) == 0 ||  merchantPaidAmount.compareTo(employerInvoices.getTotalAmountDue()) == 1)
					{
						employerInvoices.setPaidStatus(PaidStatus.PAID);
						employerPayments.setIsPartialPayment('N');
						employerPayments.setStatus(PaidStatus.PAID.toString());
						employerInvoices.setAmountEnclosed(merchantPaidAmount);
						for (EmployerInvoiceLineItems employerInvoiceLineItems : employerInvoiceLineItemsList)
						{
							employerInvoiceLineItems.setPaymentReceived(employerInvoiceLineItems.getNetAmount());
							employerInvoiceLineItems.setPaidStatus(Status.PAID);
							employerInvoiceLineItems.setPaidDate(new TSDate());
							employerInvoiceLineItems.setLastUpdatedBy(user);
						}
				
						EmployerInvoices latestInvoice = employerInvoicesService.findLastActiveInvoice(employerInvoices.getEmployer().getId());
						if(latestInvoice!=null && !(latestInvoice.getInvoiceNumber().equals(employerInvoices.getInvoiceNumber())))
						{
							employerInvoices.setIsActive(FinanceConstants.DECISION_N);
							employerPaymentInvoice.setIsActive(EmployerPaymentInvoice.IsActive.N);
						}
					}
					else
					{
						String paymentAllocationType = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.PARITIAL_PAYMENT_ALLOCATION_TYPE);
						employerInvoices.setPaidStatus(PaidStatus.PARTIALLY_PAID);
						employerPayments.setIsPartialPayment('Y');
						employerPayments.setStatus(PaidStatus.PARTIALLY_PAID.toString());
						employerInvoices.setAmountEnclosed(merchantPaidAmount);
						double minimumthreshold = Double.parseDouble(minimumPaymentLimit);
						BigDecimal totalLineitemOnminithreshold = FinancialMgmtGenericUtil.getDefaultBigDecimal();
						for (EmployerInvoiceLineItems employerInvoiceLineItems : employerInvoiceLineItemsList)
						{
							BigDecimal lineitemminimumamt = FinancialMgmtGenericUtil.multiply(employerInvoiceLineItems.getNetAmount(), FinancialMgmtGenericUtil.getBigDecimalObj(minimumthreshold/FinanceConstants.NUMERICAL_100));
							employerInvoiceLineItems.setPaymentReceived(lineitemminimumamt);
							employerInvoiceLineItems.setPaidStatus(Status.PARTIALLY_PAID);
							employerInvoiceLineItems.setPaidDate(new TSDate());
							employerInvoiceLineItems.setLastUpdatedBy(user);
							settlementAmt = FinancialMgmtGenericUtil.subtract(settlementAmt, lineitemminimumamt);
							totalLineitemOnminithreshold = FinancialMgmtGenericUtil.add(totalLineitemOnminithreshold, lineitemminimumamt);
						}
						if((settlementAmt.compareTo(BigDecimal.ZERO)==1) && "ratio".equalsIgnoreCase(paymentAllocationType))
						{
							for (EmployerInvoiceLineItems employerInvoiceLineItems : employerInvoiceLineItemsList)
							{
								BigDecimal lineitempendingamt = FinancialMgmtGenericUtil.subtract(employerInvoiceLineItems.getNetAmount(), employerInvoiceLineItems.getPaymentReceived());
								BigDecimal totalinvoicependingamt = FinancialMgmtGenericUtil.subtract(employerInvoices.getTotalAmountDue(), totalLineitemOnminithreshold);
								BigDecimal ratioallocationamt = FinancialMgmtGenericUtil.divide(FinancialMgmtGenericUtil.multiply(lineitempendingamt, settlementAmt), totalinvoicependingamt);
								employerInvoiceLineItems.setPaymentReceived(FinancialMgmtGenericUtil.add(employerInvoiceLineItems.getPaymentReceived(), ratioallocationamt));
							}
						}
					}
					employerPayments.setTotalPaymentReceived(merchantPaidAmount);
					employerPayments.setAmountEnclosed(merchantPaidAmount);
					employerInvoices.setTotalPaymentReceived(FinancialMgmtGenericUtil.add(employerInvoices.getTotalPaymentReceived() , merchantPaidAmount));
					employerInvoices.setEmployerInvoiceLineItems(employerInvoiceLineItemsList);
					employerPaymentInvoice.setEmployerPayments(employerPayments);
					employerPaymentInvoice.setLastUpdatedBy(user.getId());
					employerInvoices.setSettlementDateTime(new TSDate());
					employerInvoices.setLastUpdatedBy(user.getId());
					employerPaymentInvoiceService.saveEmployerPaymentInvoice(employerPaymentInvoice);
					employerInvoicesService.saveEmployerInvoices(employerInvoices);
				//below written call is to update Enrollment and shop enrollment
				}
			}
		}
		LOGGER.info("END- updateEmployerPayment() method");
	}
	
	/**
	 * 
	 * @param merchantRefNumber String
	 * @param paidAmount BigDecimal
	 * Update Issuer Payment against the received response from Cybersource PaymentEventReport. 
	 */
	private void updateIssuerPayment(String merchantRefNumber, BigDecimal paidAmount,AccountUser user)
	{
		LOGGER.info("STARTED - updateIssuerPayment(-,-) method with merchantRefNumber: "+merchantRefNumber+" paidAmount: "+paidAmount);
		IssuerPaymentDetail issuerPaymentDetail = issuerPaymentDetailService.findByMerchantRefCode(merchantRefNumber);
		if(issuerPaymentDetail != null)
		{
			BigDecimal merchantPaidAmount = paidAmount;
			if (Double.parseDouble(issuerPaymentDetail.getIssuerPayment().getTotalAmountDue().toString()) < 0 && paidAmount.negate().compareTo(issuerPaymentDetail.getIssuerPayment().getTotalAmountDue()) == 0) {
				merchantPaidAmount = paidAmount.negate();
			}
			if(issuerPaymentDetail.getIsEdiGenerated().equals(EDIGeneratedFlag.P)){
				issuerPaymentDetail.setIsEdiGenerated(EDIGeneratedFlag.N);
			}
			
			issuerPaymentDetailService.saveIssuerPaymentDetail(issuerPaymentDetail);
			
			IssuerPayments issuerPayment = issuerPaymentDetail.getIssuerPayment();
			IssuerPaymentInvoice issuerPaymentInvoice = issuerPayment.getIssuerPaymentInvoice(); 
			IssuerRemittance issuerRemittance = issuerPaymentInvoice.getIssuerRemittance();
			
			if(issuerRemittance.getTotalAmountDue().compareTo(FinancialMgmtGenericUtil.add(issuerRemittance.getTotalPaymentPaid(), merchantPaidAmount))==1)
			{
				LOGGER.info("Set For Partially Paid Status.....");
				issuerRemittance.setPaidStatus(IssuerRemittance.PaidStatus.PARTIALLY_PAID);
			}
			else
			{
				LOGGER.info("Set For Paid Status.....");
				issuerRemittance.setPaidStatus(IssuerRemittance.PaidStatus.PAID);
			}
			
			
			BigDecimal total = FinancialMgmtGenericUtil.add(issuerPayment.getTotalPaymentReceived(), merchantPaidAmount);
			
			if(total.compareTo(issuerRemittance.getTotalAmountDue())==-1)
			{
				issuerPayment.setIsPartialPayment('Y');
				issuerPayment.setStatus(PaidStatus.PARTIALLY_PAID.toString());
			}
			else
			{
				issuerPayment.setIsPartialPayment('N');
				issuerPayment.setStatus(PaidStatus.PAID.toString());
			}
			issuerRemittance.setAmountReceivedFromEmployer(BigDecimal.ZERO);
			issuerRemittance.setAmountEnclosed(FinancialMgmtGenericUtil.add(issuerRemittance.getAmountEnclosed(), merchantPaidAmount));
			issuerPayment.setTotalPaymentReceived(total);
			issuerRemittance.setTotalPaymentPaid(FinancialMgmtGenericUtil.add(issuerRemittance.getTotalPaymentPaid(),merchantPaidAmount));
			issuerPaymentInvoice.setIssuerRemittance(issuerRemittance);
			issuerPaymentInvoice.setIssuerPayments(issuerPayment);
			issuerPaymentInvoice.setLastUpdatedBy(user.getLastUpdatedBy());
			
			issuerPaymentInvoiceService.saveIssuerPaymentInvoice(issuerPaymentInvoice);
			issuerRemittance.setLastUpdatedBy(user.getLastUpdatedBy());
			issuerInvoicesService.saveIssuerRemittance(issuerRemittance);
			BigDecimal totalAmountReceivedFromEmployer = BigDecimal.ZERO;
			
			List<IssuerRemittanceLineItem> issuerRemittanceLineItemList = issuerInvoicesLineItemsService.getIssLineItemsByPaidEmpLineItems(issuerRemittance.getId(), issuerRemittance.getIssuer().getId());
			for (IssuerRemittanceLineItem issuerRemittanceLineItem : issuerRemittanceLineItemList)
			{
				//Changes Done by Kuldeep to update amtReceivedFromEmployee and amtPaidToIssuer 
				IssuerRemittanceLineItem newIssuerRemittanceLineItems = issuerInvoicesLineItemsService.updateAmtPaidFrmEmpToIssuer(issuerRemittanceLineItem.getEmployerInvoiceLineItems(), issuerRemittance);
				totalAmountReceivedFromEmployer = totalAmountReceivedFromEmployer.add(newIssuerRemittanceLineItems.getAmountReceivedFromEmployer(), new MathContext(FinanceConstants.NUMERICAL_20, RoundingMode.HALF_UP));
				employerInvoiceLineItemsService.updatePaidToIssuerStatus(issuerRemittanceLineItem.getEmployerInvoiceLineItems().getId(), PaidToIssuer.Y,user);
			}
			issuerRemittance.setAmountReceivedFromEmployer(FinancialMgmtGenericUtil.add(totalAmountReceivedFromEmployer, issuerRemittance.getAmountReceivedFromEmployer()));
			issuerRemittance.setSettlementDateTime(new TSDate());
			issuerInvoicesService.saveIssuerRemittance(issuerRemittance);
		}
		LOGGER.info("END- updateIssuerPayment() method");
	}
	
	/**
	 * 
	 * @param merchantRefNumber String
	 * Update Issuer payment for Failed status as per the received response. 
	 */
	private void updateIssuerPaymentFailedStatus(String merchantRefNumber, BigDecimal amount, int userId)throws GIException
	{
		LOGGER.info("STARTED::updateIssuerPaymentFailedStatus() method started with merchantRefNumber: "+merchantRefNumber);
		IssuerPaymentDetail issuerPaymentDetail = issuerPaymentDetailService.findByMerchantRefCode(merchantRefNumber);
		if(issuerPaymentDetail != null)
		{
			IssuerPayments issuerPayment = issuerPaymentDetail.getIssuerPayment();
			IssuerRemittance issuerRemittance = issuerPayment.getIssuerPaymentInvoice().getIssuerRemittance();
			if(issuerRemittance != null)
			{
				if(issuerRemittance.getPaidStatus() != null)
				{
					String paidStatus = issuerRemittance.getPaidStatus().toString();
					if(StringUtils.isNotEmpty(paidStatus) && paidStatus.equalsIgnoreCase(IssuerRemittance.PaidStatus.PAID.toString()))
					{
						LOGGER.warn("Received Failed status for Payment marked previously.....");
						try
						{
							failedPaymentUpdateNotification("CARRIER", amount,issuerRemittance.getRemittanceNumber(), issuerRemittance.getIssuer().getName());
							return;
						}
						catch(Exception ex)
						{
							LOGGER.error("Exception caught "+ex);
							throw new GIException(" Exception caught while sending failedPaymentUpdateNotification: "+ex);
						}
					}
				}
				else
				{
					LOGGER.warn("No Paid status found for IssuerInvoices with MerchantRefNo: "+merchantRefNumber);
					throw new GIException("No Paid status found for IssuerInvoices with MerchantRefNo: "+merchantRefNumber);
				}
				
				issuerRemittance.setPaidStatus(IssuerRemittance.PaidStatus.DUE);
				issuerRemittance.setPaidDateTime(null);//Resetting Paid_DateTime once the PaidStatus is Updated
				issuerRemittance.setLastUpdatedBy(userId);
				issuerInvoicesService.saveIssuerRemittance(issuerRemittance);
			}
			else
			{
				LOGGER.warn("==== No Issuer Invoice found for MerchantRefNo: "+merchantRefNumber);
			}
			
		}
		LOGGER.info("END::updateIssuerPaymentFailedStatus() method");
	}
	
	/**
	 * 
	 * @param merchantRefNumber String
	 * @param failureReason String 
	 * @param eventType String
	 * @throws NoticeServiceException
	 * Update employer Payment for Failed status as per the received response.  
	 */
	private void updateEmployerPaymentFailedStatus(String merchantRefNumber, String failureReason, String eventType, BigDecimal amount, AccountUser user)throws NoticeServiceException, GIException
	{
		String nsfFlatFeeNote = null;
		LOGGER.info("STARTED-updateEmployerPaymentFailedStatus(-,-) method with merchantRefNumber: "+merchantRefNumber+" failureReason: "+failureReason+" ----"+" EventType: "+eventType);
		EmployerInvoices employerInvoices = null;
		EmployerPayments employerPayments = iEmployerPaymentRepository.findEmployerPaymentsByMerchantRefCode(merchantRefNumber);
		
		if(employerPayments != null)
		{
			LOGGER.info("Getting Employer Payment Invoice....");
			EmployerPaymentInvoice employerPaymentInvoice = employerPayments.getEmployerPaymentInvoice();
			employerInvoices = employerPaymentInvoice.getEmployerInvoices();
			
			if(employerInvoices != null)
			{	
				if(employerInvoices.getPaidStatus() != null)
				{
					String paidStatus = employerInvoices.getPaidStatus().toString();
					if(StringUtils.isNotEmpty(paidStatus) && paidStatus.equalsIgnoreCase(PaidStatus.PAID.toString()))
					{
						LOGGER.warn("Received Failed status for Payment marked previously.....");
						try
						{
							failedPaymentUpdateNotification("EMPLOYER", amount,employerInvoices.getInvoiceNumber(), employerInvoices.getEmployer().getName());
							return;
						}
						catch(Exception ex)
						{
							LOGGER.error("Exception caught "+ex);
							throw new GIException(" Exception caught while sending failedPaymentUpdateNotification: "+ex);
						}
											
					}
				}
				else
				{
					LOGGER.warn("No Paid status found for EmployerInvoices with MerchantRefNo: "+merchantRefNumber);
					throw new GIException("No Paid status found for EmployerInvoices with MerchantRefNo: "+merchantRefNumber); 
				}
				
			}
			else
			{
				LOGGER.warn("No EmployerInvoices found for merchantRefNumber #"+merchantRefNumber+" where EmployerPayment ID# "+employerPayments.getId());
				throw new GIException("No EmployerInvoices found for merchantRefNumber #"+merchantRefNumber);
			}
			
			/*
			 * HIX-55785 Calculate NSF fees and add to SHOP invoice (Configurable option by State)
			 */
			String isNSFFeesRequired = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_NSF_FEES_REQUIRED);
			if("NSF".equalsIgnoreCase(eventType) && StringUtils.isNotEmpty(isNSFFeesRequired) && isNSFFeesRequired.equalsIgnoreCase(FinanceConstants.TRUE))
			{
				
				BigDecimal nsfFlatFeeBigDecimal = FinanceGeneralUtility.getConfigBigDecimalAmount(FinanceConfiguration.FinanceConfigurationEnum.NSF_FLAT_FEE);
				/* 
				 * If IS_NSF_FEES_REQUIRED is TRUE & NSF Flat fee is > than 0 then add NSF Fee to Employer Invoice 
				 */
				if(nsfFlatFeeBigDecimal.compareTo(BigDecimal.ZERO) == 1)
				{
					nsfFlatFeeNote = FinanceConstants.NSF_FEE_COMMENT_EMAIL.replaceAll("NSF_FLAT_FEE_AMOUNT", nsfFlatFeeBigDecimal.toString());
					EmployerExchangeFees employerExchangeFees = new EmployerExchangeFees();
					employerExchangeFees.setEmployerInvoices(employerInvoices);
					employerExchangeFees.setFeeTypeCode(ExchangeFeeTypeCode.NSF_FLAT_FEE);
					employerExchangeFees.setAmount(nsfFlatFeeBigDecimal);
					
					employerExchangeFeeService.saveEmployerExchangeFees(employerExchangeFees);
					employerInvoices.setExchangeFees(employerInvoices.getExchangeFees() == null ? nsfFlatFeeBigDecimal
									: FinancialMgmtGenericUtil.add(employerInvoices.getExchangeFees(), nsfFlatFeeBigDecimal));
					employerInvoices.setEcmDocId(null);
					
					employerInvoices.setTotalAmountDue(FinancialMgmtGenericUtil.add(employerInvoices.getExchangeFees(),
									 FinancialMgmtGenericUtil.add(
											employerInvoices.getPremiumThisPeriod(),
											employerInvoices.getManualAdjustmentAmt() == null ? BigDecimal.ZERO : employerInvoices.getManualAdjustmentAmt())));
				}
			}
			  boolean cancelInvoiceAndEnrollment = false; 
		      if("BINDER_INVOICE".equalsIgnoreCase(employerInvoices.getInvoiceType().toString())){
				   String paymentReconcileBusinessdays = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.PAYMENT_RECONCILE_BUSINESSDAYS);
				   DateTime paymentDueDate = new DateTime(employerInvoices.getPaymentDueDate());
				   paymentDueDate = paymentDueDate.plusDays(Integer.parseInt(paymentReconcileBusinessdays));
				   Date maxDateForCancellationOfEnrollment = paymentDueDate.toDate();
				   //if due date + 7 days are crossed for binder invoice then cancel invoice otherwise mark invoice as due in case of payment failure
				   if(new TSDate().compareTo(maxDateForCancellationOfEnrollment)>0){ 
					   cancelInvoiceAndEnrollment =  true;
				   }
			   }
		      if(cancelInvoiceAndEnrollment){
		    	  int employerEnrollId = employerPremiumPaymentProcessingService.getActiveEmployerEnrollmentId(employerInvoices);
		    	  ShopResponse shopResponse = financialMgmtUtils.employerEnrollmentTermination(employerInvoices.getEmployer().getId(),employerEnrollId,null,null,null);
		    	  if(shopResponse != null)
					{
						if(shopResponse.getErrCode()==FinanceConstants.ERROR_CODE_202 ||
								shopResponse.getErrCode()==FinanceConstants.ERROR_CODE_204 ||
								shopResponse.getErrCode()==FinanceConstants.SUCCESS_CODE_200)
						{
							employerPremiumPaymentProcessingService.cancelInvoices(employerInvoices.getEmployer().getId(),employerEnrollId,"PAYMENT_FAILURE");
						}
					}
		      }
		      else{  
		    	employerInvoices.setPaidStatus(PaidStatus.DUE);
				employerInvoices.setPaidDateTime(null);
				employerInvoices.setLastUpdatedBy(user.getId());
				List<EmployerInvoiceLineItems> employerInvoiceLineItemsList = employerInvoiceLineItemsService.getInvoiceLineItem(employerInvoices);
				if(employerInvoiceLineItemsList != null)
				{					
					for(EmployerInvoiceLineItems employerInvoiceLineItems : employerInvoiceLineItemsList)
					{
						employerInvoiceLineItems.setPaidStatus(Status.DUE);
						employerInvoiceLineItems.setLastUpdatedBy(user);
					}
					employerInvoices.setEmployerInvoiceLineItems(employerInvoiceLineItemsList);
				}
				employerPayments.setStatus(PaidStatus.DUE.toString());
				iEmployerPaymentRepository.save(employerPayments);
				employerInvoicesService.saveEmployerInvoices(employerInvoices);
		      }
				
				/*
				 * Generating PDF after changes in invoice
				 */
				if(employerInvoices.getEcmDocId() == null)	
				{
					try
					{
						employerInvoicePdfGenerationService.createPdfByInvoiceId(employerInvoices.getId());
					}
					catch(Exception ex)
					{
						LOGGER.error("Error occurred while generating PDF", ex);
						throw new GIException("Error occurred while generating PDF", ex);
					}
				}

				String response = financeNotificationService.createPaymentFailureNotice(
							employerInvoices, failureReason, nsfFlatFeeNote, amount);
				LOGGER.info("PAYMENT_REPORT:: Response received after sending notification: "+response);
				sendEmployerPaymentFailedEmailToAdmin(employerInvoices, amount, failureReason);
		}
		LOGGER.info("END-updateEmployerPaymentFailedStatus() method");
	}
	
	
	/**
	 * 
	 * @param requestObject Request
	 * @return PaymentEventLog
	 * Saves PaymentEventLog for the different Event Reported.
	 */
	private PaymentEventLog savePaymentEventLog(Map<String, Object> dataMap,int userId)
	{
		LOGGER.info("STARTED-savePaymentEventLog() method started...");
		PaymentEventLog paymentEventLog = new PaymentEventLog();
		paymentEventLog.setMerchantRefCode(dataMap.get(FinanceConstants.PaymentEventLogs.MERCHANT_REF_NUMBER_KEY)!=null ?dataMap.get(FinanceConstants.PaymentEventLogs.MERCHANT_REF_NUMBER_KEY).toString() : null);
		paymentEventLog.setRequestId(dataMap.get(FinanceConstants.PaymentEventLogs.REQUEST_ID_KEY)!=null ?dataMap.get(FinanceConstants.PaymentEventLogs.REQUEST_ID_KEY).toString() : null);
		paymentEventLog.setEventType(dataMap.get(FinanceConstants.PaymentEventLogs.EVENT_TYPE_KEY)!=null ?dataMap.get(FinanceConstants.PaymentEventLogs.EVENT_TYPE_KEY).toString() : null);
		paymentEventLog.setTransactionId(dataMap.get(FinanceConstants.PaymentEventLogs.TRANSACTION_ID_KEY)!=null ?dataMap.get(FinanceConstants.PaymentEventLogs.TRANSACTION_ID_KEY).toString() : null);
		paymentEventLog.setEcmDocId(dataMap.get(FinanceConstants.PaymentEventLogs.ECM_DOC_ID_KEY)!=null ?dataMap.get(FinanceConstants.PaymentEventLogs.ECM_DOC_ID_KEY).toString() : null);
		paymentEventLog.setLastUpdatedBy(userId);
		LOGGER.info("END::savePaymentEventLog() method end successfully");
		return paymentEventLogService.savePaymentEventLog(paymentEventLog);
	}
	

	@Override
	public void processPytRptManually(String paymentEventReportFileName)throws GIException 
	{
		LOGGER.info("Received Request to run PaymentEventReport Manually for file name : "+paymentEventReportFileName);
		demoPaymentReportTest(paymentEventReportFileName);
		LOGGER.info("================== Received Request has been successfully executed ===================================");
	}
	
	
	/**
	 * 
	 * @param noOfBusinessDays
	 * @param reportDownloadUrl
	 * @throws GIException
	 * Method to extract Carrier and Employer invoice for transaction report.
	 */
	public void processSingleTxnReport(String noOfBusinessDays, String reportDownloadUrl)throws GIException
	{		
		DateTime currentDateTime = TSDateTime.getInstance();
		DateTime fromReconcilatonDateTime = currentDateTime.minusDays(Integer.parseInt(noOfBusinessDays));
		
		List<Date> bankHolidayList = bankHolidayService.findBankHolidaysBetween(fromReconcilatonDateTime.toDate(), currentDateTime.toDate());
		if(bankHolidayList != null && !bankHolidayList.isEmpty())
		{
			LOGGER.info("BANK HOLIDAY PRESENT FOR CURRENT YEAR=================");
			fromReconcilatonDateTime = fromReconcilatonDateTime.minusDays(bankHolidayList.size());
		}
		else
		{
			/**
			 * Query won't fail even if we switch to new year as we might already
			 * sent the notification for uploading holiday list last year, & if no holidayList present this year then thow exception.
			 */
			long yearHolidayCount = bankHolidayService.findNoOfBankHolidaysByYear(Integer.toString(currentDateTime.getYear()));
			if(yearHolidayCount == 0)
			{
				try
				{
					LOGGER.info("====================BEFORE SENDING NOTIFICATION=================");
					sendBusinessDaysUploadNotification();
					LOGGER.info("====================AFTER SENDING NOTIFICATION=================");
				}
				catch(Exception ex)
				{					
					LOGGER.error("Exception caught while sending notification: "+ex);
					throw new GIException("Exception caught while sending email notification for uploading holiday list: "+ex);
				}
				throw new GIException("No Holiday list uploaded for year: "+currentDateTime.getYear());
			}
		}
		
		processIssuerInvoice(fromReconcilatonDateTime.toDate(), reportDownloadUrl);
		processEmployerInvoice(fromReconcilatonDateTime.toDate(), reportDownloadUrl);
		
		
	}
	
	/**
	 * 
	 * @param reconcilationDate
	 * @param reportDownloadUrl
	 * @throws GIException
	 * Process Single Transaction report for Carrier, Download and Update of Carrier invoice. 
	 */
	private void processIssuerInvoice(Date reconcilationDate, String reportDownloadUrl)throws GIException
	{
		String isStandaloneCreditSupported = DynamicPropertiesUtil
				.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_STANDALONE_CREDIT_SUPPORTED);
		if(StringUtils.isNotEmpty(isStandaloneCreditSupported) && "TRUE".equalsIgnoreCase(isStandaloneCreditSupported))
		{	
			Map<String, Exception> issuerExceptionDetails = null;
			List<IssuerRemittance> issuerRemittanceList = issuerInvoicesService
					.findInProcessInvoiceForReconcilation(
						IssuerRemittance.PaidStatus.IN_PROCESS,
						reconcilationDate);
			if(issuerRemittanceList != null && !issuerRemittanceList.isEmpty())
			{
				issuerExceptionDetails = new HashMap<String, Exception>();
				for(IssuerRemittance issuerRemittance : issuerRemittanceList)
				{
					try
					{
						List<Object[]> paymentDetailObjList = issuerPaymentDetailService.getRequestIdNMerchantRefCode(issuerRemittance.getId());
						if(paymentDetailObjList != null && !paymentDetailObjList.isEmpty())
						{
							for (Object[] paymentDetailData : paymentDetailObjList)
							{
								executeSingleTxnReport(issuerRemittance.getTotalAmountDue(), (String)paymentDetailData[0], (String)paymentDetailData[1], reportDownloadUrl);
							}
						}
					}
					catch(Exception ex)
					{
						LOGGER.error("Exception caught in processIssuerInvoice : ", ex);
						issuerExceptionDetails.put("ISSUER_INVOICE_ID_"+issuerRemittance.getId(), ex);
					}
				}
				if(issuerExceptionDetails != null && !issuerExceptionDetails.isEmpty())
				{
					String exceptionMessage = FinanceGeneralUtility.constructSingleMessageForExceptionDetails("EXCEPTIONS OCCURRED WHILE PROCESSING ISSUER INVOICE:", "ISSUER_ID_", issuerExceptionDetails);
					throw new GIException(exceptionMessage);
				}
			}
		}
	}
	
	/**
	 * 
	 * @param reconcilationDate
	 * @param reportDownloadUrl
	 * @throws GIException
	 * Update Employer Payment as the PaymentEventReport.
	 */
	private void processEmployerInvoice(Date reconcilationDate, String reportDownloadUrl)throws GIException
	{
		List<EmployerInvoices> employerInvoicesList = employerInvoicesService
				.findInProcessInvoiceForReconcilation(
						EmployerInvoices.PaidStatus.IN_PROCESS,
						reconcilationDate);
		Map<String, Exception> employerExceptionDetails = null;
		if(employerInvoicesList != null && !employerInvoicesList.isEmpty())
		{
			employerExceptionDetails = new HashMap<String, Exception>();
			for(EmployerInvoices employerInvoices :employerInvoicesList )
			{
				try
				{
					EmployerPayments employerPayment = iEmployerPaymentRepository.findActiveEmpPymtByEmpInvoices(employerInvoices.getId());
					executeSingleTxnReport(employerInvoices.getTotalAmountDue(), employerPayment.getTransactionId(), employerPayment.getMerchantRefCode(), reportDownloadUrl);
				}
				catch(Exception ex)
				{
					LOGGER.error("Caught Exception: ", ex);
					employerExceptionDetails.put("EMPLOYER_INVOICE_ID_"+employerInvoices.getId(), ex);
				}
			}
			if(employerExceptionDetails != null && !employerExceptionDetails.isEmpty())
			{
				String exceptionMessage = FinanceGeneralUtility.constructSingleMessageForExceptionDetails("EXCEPTIONS OCCURRED WHILE PROCESSING EMPLOYER INVOICE:", "EMPLOYER_Id_", employerExceptionDetails);
				throw new GIException(exceptionMessage);
			}
		}
	}
	
	/**
	 * 
	 * @param totalAmtDue
	 * @param requestId
	 * @param merchantReferenceNumber
	 * @param reportDownloadUrl
	 * @throws GIException
	 * @throws JAXBException
	 * @throws ContentManagementServiceException
	 * @throws NoticeServiceException
	 * Execution of SingleTransactionReport for the made payment.
	 */
	private void executeSingleTxnReport(BigDecimal totalAmtDue, String requestId, String merchantReferenceNumber, String reportDownloadUrl)throws GIException,JAXBException,ContentManagementServiceException, NoticeServiceException,Exception
	{
		
		Map<String, Object> paymentEventLogMap = new HashMap<String, Object>();
		String singleTxnReportResponse = PaymentProcessorStrategy.reportDownloader.downloadSingleTxnReport(requestId, merchantReferenceNumber, reportDownloadUrl);
		AccountUser user = FinanceGeneralUtility.getAccountUser(userService);
		if(StringUtils.isNotEmpty(singleTxnReportResponse))
		{
		singleTxnReportResponse = singleTxnReportResponse.replace("ebctest", "ebc");
		/*
		 * Unmarshalling Report response
		 */
		Report  singleTxnReport = (Report) GhixUtils.inputStreamToObject(IOUtils.toInputStream(singleTxnReportResponse, FinanceConstants.ENCODING_UTF_8), Report.class);
		if(singleTxnReport != null)
		{
			for(Report.Requests requests : singleTxnReport.getRequests())
				{
					for(Report.Requests.Request request : requests.getRequest())
					{
						//String merchantReferenceNumber = request.getMerchantReferenceNumber();
						PaymentEventLog paymentEventLog = paymentEventLogService.getPaymentEventLogForReconciliation(merchantReferenceNumber, requestId, FinanceConstants.CYBERSOURCE_ACCEPT);
						if(paymentEventLog != null)
						{
							paymentEventLogMap.put(FinanceConstants.PaymentEventLogs.MERCHANT_REF_NUMBER_KEY, merchantReferenceNumber);
							paymentEventLogMap.put(FinanceConstants.PaymentEventLogs.REQUEST_ID_KEY, request.getRequestID());
							paymentEventLogMap.put(FinanceConstants.PaymentEventLogs.TRANSACTION_ID_KEY, request.getTransactionReferenceNumber());
							
							for(PaymentData paymentData : request.getPaymentData())
							{
								String paymentEventType = paymentData.getEventType();
								paymentEventLogMap.put(FinanceConstants.PaymentEventLogs.EVENT_TYPE_KEY, paymentEventType);
								if(StringUtils.isNotEmpty(paymentEventType))
								{
									if("TRANSMITTED".equalsIgnoreCase(paymentEventType))
									{
										paymentEventLogService.updatePaymentStatus(paymentEventLog.getId(), PaymentStatus.CONFIRMED);
										if(merchantReferenceNumber.contains(FinanceConstants.EMP))
										{
											updateEmployerPayment(merchantReferenceNumber, totalAmtDue,user);
										}
										else if(merchantReferenceNumber.contains(FinanceConstants.ISS))
										{
											updateIssuerPayment(merchantReferenceNumber, totalAmtDue,user);
										}
									}
									else if("VOIDED".equalsIgnoreCase(paymentEventType) || "FAILED".equalsIgnoreCase(paymentEventType)
											|| "ERROR".equalsIgnoreCase(paymentEventType) || "Final NSF".equalsIgnoreCase(paymentEventType)
											|| "Stop Payment".equalsIgnoreCase(paymentEventType))
									{
										paymentEventLogService.updatePaymentStatus(paymentEventLog.getId(), PaymentStatus.FAILED);
										String pymtRejectionReason = null;
										for(ApplicationReplies applicationReplies: request.getApplicationReplies())
										{
											for(ApplicationReply applicationReply : applicationReplies.getApplicationReply())
											{
												pymtRejectionReason = applicationReply.getRMsg();
											}
										}
										if(merchantReferenceNumber.contains(FinanceConstants.EMP))
										{
											updateEmployerPaymentFailedStatus(merchantReferenceNumber, pymtRejectionReason, paymentEventType, totalAmtDue,user);
										}
										else if(merchantReferenceNumber.contains(FinanceConstants.ISS))
										{
											updateIssuerPaymentFailedStatus(merchantReferenceNumber, totalAmtDue,user.getId());
										}
									}
								}
							}
						}
						else
						{
							LOGGER.warn("Payment Event Log Not found for merchantReferenceNumber: "
									+ merchantReferenceNumber
									+ " And RequestID: " + requestId);
						}
						
					}
				}
			
			String ecmDocID = saveReportFileAtAlfresco(singleTxnReportResponse.getBytes(), "SingleTxnReport");
			paymentEventLogMap.put(FinanceConstants.PaymentEventLogs.ECM_DOC_ID_KEY, ecmDocID);
			savePaymentEventLog(paymentEventLogMap,user.getId());
		}
	}
		else
		{
			throw new GIException("Received null response from Cybersource for txn requestId "+requestId+ " +merchnat_Ref_No"+merchantReferenceNumber);
		}
   }
	
	/**
	 * 
	 * @throws NotificationTypeNotFound
	 * @throws Exception
	 * Send Notification about the Business Days absent in the connected DB schema.
	 */
	private void sendBusinessDaysUploadNotification()throws GIException
	{
		try
		{
			LOGGER.info("=================INSIDE sendBusinessDaysUploadNotification() METHOD");
			Map<String, Object> emailData = new HashMap<String, Object>();
			emailData.put("recipient", DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.ESCALATION_EMAIL));
			businessDaysNotPresent.setEmailData(emailData);
			Notice noticeObj = businessDaysNotPresent.generateEmail();
			Notification notificationObj = businessDaysNotPresent.generateNotification(noticeObj);
			LOGGER.info("Notice body for BusinessDays upload notification."+noticeObj.getEmailBody());
			businessDaysNotPresent.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		}
		catch(Exception ex)
		{
			throw new GIException(ex);
		}
	}
	
	/**
	 * 
	 * @throws NotificationTypeNotFound
	 * @throws Exception
	 * Notify about the Update Failed Payment @using JavaMailing.
	 */
	private void failedPaymentUpdateNotification(String userType, BigDecimal amount, String invoiceNumber, String userTypeName) throws GIException
	{
		try{
		 LOGGER.info("=================INSIDE failedPaymentUpdateNotification() METHOD=======");
		 Map<String, Object> emailData = new HashMap<String, Object>();
		 emailData.put("recipient", DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.ESCALATION_EMAIL));
		 emailData.put("userType", userType);
		 emailData.put("amount", amount != null ? amount.toString() : "0.00");
		 emailData.put("invoiceNumber", invoiceNumber);
		 emailData.put("userTypeName", userTypeName);
		 paymentFailedUpdation.setEmailData(emailData);
		 Notice noticeObj = paymentFailedUpdation.generateEmail();
		 Notification notificationObj = paymentFailedUpdation.generateNotification(noticeObj);
		 LOGGER.info("Notice body for Failed Payment update notification."+noticeObj.getEmailBody());
		 paymentFailedUpdation.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		}
		catch(Exception ex)
		{
			throw new GIException(ex);
		}
	}
	
	private void sendEmployerPaymentFailedEmailToAdmin(EmployerInvoices employerInvoices, BigDecimal paidAmount, String failureReason) throws GIException
	{
		try
		{
			Map<String, String> templateData = new HashMap<>();
			DateFormat dateFormat = new SimpleDateFormat("MMMMM dd, yyyy");
			templateData.put(FinanceConstants.SYSTEM_DATE, dateFormat.format(new TSDate()));
			templateData.put(FinanceConstants.EMPLOYER_BUSINESS_NAME, employerInvoices.getEmployer().getName());
			templateData.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));					
			templateData.put(FinanceConstants.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			templateData.put("paidAmount", (paidAmount!=null) ? paidAmount.toPlainString() : "0.00");
			templateData.put("invoiceNumber", employerInvoices.getInvoiceNumber());
			templateData.put("failureReason", StringUtils.isEmpty(failureReason)?"Insufficient Funds":failureReason);
			templateData.put(FinanceConstants.EMPLOYER_CONTACT_FULL_NAME, employerInvoices.getEmployer().getContactFirstName()+" "+employerInvoices.getEmployer().getContactLastName());
			templateData.put("employerPhone", employerInvoices.getEmployer().getContactNumber());
			templateData.put("employerEmailAddr", employerInvoices.getEmployer().getContactEmail());
			templateData.put("contactEmail", DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.ESCALATION_EMAIL));
			employerPaymentFailureNoticeForAdmin.setTemplateData(templateData);
			Notice notice = employerPaymentFailureNoticeForAdmin.generateEmail();
			Notification notification = employerPaymentFailureNoticeForAdmin.generateNotification(notice);
			employerPaymentFailureNoticeForAdmin.sendEmailToMultipleRecipient(notification, notice);
		}
		catch(Exception ex)
		{
			throw new GIException(ex);
		}
	}
}
