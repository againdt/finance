package com.getinsured.hix.finance.emailnotification;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.notification.NotificationAgent;

@Component
public class EmployerPassedEndDueDateNotice extends
		NotificationAgent {
	
	private Map<String, String> templateData;

	@Override
	public Map<String, String> getSingleData() {
		setTokens(this.templateData);
		Map<String, String> emailData = new HashMap<>();
		emailData.put("To", templateData.get("contactEmail"));
		return emailData;
	}

	/**
	 * @return the templateData
	 */
	public Map<String, String> getTemplateData() {
		return templateData;
	}

	/**
	 * @param templateData the templateData to set
	 */
	public void setTemplateData(Map<String, String> templateData) {
		this.templateData = templateData;
	}
}
