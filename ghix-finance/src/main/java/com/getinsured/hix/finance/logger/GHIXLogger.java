package com.getinsured.hix.finance.logger;

import org.slf4j.Logger;
import org.slf4j.Marker;

/**
 * This is a wrapper class for Log framework . 
 * @author khimls_s
 * @since 22-Aug-2014
 */

public final class GHIXLogger{

	private Logger logger;	
	private static GHIXLogger ghixLoggerInstance = new GHIXLogger();
	
	private  GHIXLogger() {		
	    this.logger = GHIXLoggerFactory.getLogger(GHIXLogger.class);
	}
	
	/**
	 * 
	 * @return GHIXLogger
	 */
	public static GHIXLogger getGHIXLoggerInstance()
	{   
        return ghixLoggerInstance;
	}
	/**
	 * 
	 * @return String
	 */
	public String getName() {		
		return ghixLoggerInstance.logger.getName();
	}

	/**
	 * 
	 * @return boolean
	 */
	public boolean isTraceEnabled() {
		return logger.isTraceEnabled();
	}

	/**
	 * 
	 * @param msg
	 */
	public void trace(String msg) {
		
		logger.trace(msg);
	}

	/**
	 * 
	 * @param format
	 * @param arg
	 */
	public void trace(String format, Object arg) {		
		logger.trace(format, arg);
	}
	
	/**
	 * 
	 * @param format
	 * @param arg1
	 * @param arg2
	 */
	public void trace(String format, Object arg1, Object arg2) {
		logger.trace(format, arg1, arg2);
	}
	
	/**
	 * 
	 * @param format
	 * @param argArray
	 */
	public void trace(String format, Object[] argArray) {
		logger.trace(format, argArray);
	}
	
	/**
	 * 
	 * @param msg
	 * @param t
	 */
	public void trace(String msg, Throwable t) {
		logger.trace(msg, t);
	}
	
	/**
	 * 
	 * @param marker
	 * @return
	 */
	public boolean isTraceEnabled(Marker marker) {
		return logger.isTraceEnabled(marker);
	}
	
	/**
	 * 
	 * @param marker
	 * @param msg
	 */
	public void trace(Marker marker, String msg) {
		logger.trace(marker, msg);
	}
	
	/**
	 * 
	 * @param marker
	 * @param format
	 * @param arg
	 */
	public void trace(Marker marker, String format, Object arg) {
		logger.trace(marker, format, arg);
	}
	
	/**
	 * 
	 * @param marker
	 * @param format
	 * @param arg1
	 * @param arg2
	 */
	public void trace(Marker marker, String format, Object arg1, Object arg2) {
		logger.trace(marker, format, arg1, arg2);
	}
	
	/**
	 * 
	 * @param marker
	 * @param format
	 * @param argArray
	 */
	public void trace(Marker marker, String format, Object[] argArray) {
		logger.trace(marker, format, argArray);
	}
	
	/**
	 * 
	 * @param marker
	 * @param msg
	 * @param t
	 */
	public void trace(Marker marker, String msg, Throwable t) {
		logger.trace(marker, msg, t);
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isDebugEnabled() {
		return logger.isDebugEnabled();
	}
	
	/**
	 * 
	 * @param msg
	 */
	public void debug(String msg) {
		logger.debug(msg);		
	}
	
	/**
	 * 
	 * @param format
	 * @param arg
	 */
	public void debug(String format, Object arg) {
		logger.debug(format, arg);
	}

	/**
	 * 
	 * @param format
	 * @param arg1
	 * @param arg2
	 */
	public void debug(String format, Object arg1, Object arg2) {
		logger.debug(format, arg1, arg2);
	}

	/**
	 * 
	 * @param format
	 * @param argArray
	 */
	public void debug(String format, Object[] argArray) {
		logger.debug(format, argArray);
	}

	/**
	 * 
	 * @param msg
	 * @param t
	 */
	public void debug(String msg, Throwable t) {
		logger.debug(msg, t);
	}
	
	/**
	 * 
	 * @param marker
	 * @return
	 */
	public boolean isDebugEnabled(Marker marker) {
		return logger.isDebugEnabled(marker);
	}
	
	/**
	 * 
	 * @param marker
	 * @param msg
	 */
	public void debug(Marker marker, String msg) {
		logger.debug(marker, msg);
	}
	
	/**
	 * 
	 * @param marker
	 * @param format
	 * @param arg
	 */
	public void debug(Marker marker, String format, Object arg) {
		logger.debug(marker, format, arg);
	}
	
	/**
	 * 
	 * @param marker
	 * @param format
	 * @param arg1
	 * @param arg2
	 */
	public void debug(Marker marker, String format, Object arg1, Object arg2) {
		logger.debug(marker, format, arg1, arg2);
	}
	
	/**
	 * 
	 * @param marker
	 * @param format
	 * @param argArray
	 */
	public void debug(Marker marker, String format, Object[] argArray) {
		logger.debug(marker, format, argArray);
	}
	
	/**
	 * 
	 * @param marker
	 * @param msg
	 * @param t
	 */
	public void debug(Marker marker, String msg, Throwable t) {
		logger.debug(marker, msg, t);
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isInfoEnabled() {
		return logger.isInfoEnabled();
	}
	
	/**
	 * 
	 * @param msg
	 */
	public void info(String msg) {
		logger.info(msg);
	}
	
	/**
	 * 
	 * @param format
	 * @param arg
	 */
	public void info(String format, Object arg) {
		logger.info(format, arg);
	}
	
	/**
	 * 
	 * @param format
	 * @param arg1
	 * @param arg2
	 */
	public void info(String format, Object arg1, Object arg2) {
		logger.info(format, arg1, arg2);
	}
	
	/**
	 * 
	 * @param format
	 * @param argArray
	 */
	public void info(String format, Object[] argArray) {
		logger.info(format, argArray);
	}
	
	/**
	 * 
	 * @param msg
	 * @param t
	 */
	public void info(String msg, Throwable t) {
		logger.info(msg, t);
	}
	
	/**
	 * 
	 * @param marker
	 * @return
	 */
	public boolean isInfoEnabled(Marker marker) {
		return logger.isInfoEnabled(marker);
	}
	
	/**
	 * 
	 * @param marker
	 * @param msg
	 */
	public void info(Marker marker, String msg) {		
		logger.info(marker, msg);
	}
	
	/**
	 * 
	 * @param marker
	 * @param format
	 * @param arg
	 */
	public void info(Marker marker, String format, Object arg) {
		logger.info(marker, format, arg);
	}
	
	/**
	 * 
	 * @param marker
	 * @param format
	 * @param arg1
	 * @param arg2
	 */
	public void info(Marker marker, String format, Object arg1, Object arg2) {
		logger.info(marker, format, arg1, arg2);
	}
	
	/**
	 * 
	 * @param marker
	 * @param format
	 * @param argArray
	 */
	public void info(Marker marker, String format, Object[] argArray) {
		logger.info(marker, format, argArray);
	}
	
	/**
	 * 
	 * @param marker
	 * @param msg
	 * @param t
	 */
	public void info(Marker marker, String msg, Throwable t) {
		logger.info(marker, msg, t);
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isWarnEnabled() {
		return logger.isWarnEnabled();
	}
	
	/**
	 * 
	 * @param msg
	 */
	public void warn(String msg) {
		logger.warn(msg);
	}
	
	/**
	 * 
	 * @param format
	 * @param arg
	 */
	public void warn(String format, Object arg) {
		logger.warn(format, arg);
	}
	
	/**
	 * 
	 * @param format
	 * @param argArray
	 */
	public void warn(String format, Object[] argArray) {
		logger.warn(format, argArray);
	}
	
	/**
	 * 
	 * @param format
	 * @param arg1
	 * @param arg2
	 */
	public void warn(String format, Object arg1, Object arg2) {
		logger.warn(format, arg1, arg2);
	}

	/**
	 * 
	 * @param msg
	 * @param t
	 */
	public void warn(String msg, Throwable t) {
		logger.warn(msg, t);
	}
	
	/**
	 * 
	 * @param marker
	 * @return
	 */
	public boolean isWarnEnabled(Marker marker) {
		return logger.isWarnEnabled(marker);
	}
	
	/**
	 * 
	 * @param marker
	 * @param msg
	 */
	public void warn(Marker marker, String msg) {
		logger.warn(marker, msg);
	}
	
	/**
	 * 
	 * @param marker
	 * @param format
	 * @param arg
	 */
	public void warn(Marker marker, String format, Object arg) {
		logger.warn(marker, format, arg);
	}
	
	/**
	 * 
	 * @param marker
	 * @param format
	 * @param arg1
	 * @param arg2
	 */
	public void warn(Marker marker, String format, Object arg1, Object arg2) {
		logger.warn(marker, format, arg1, arg2);
	}
	
	/**
	 * 
	 * @param marker
	 * @param format
	 * @param argArray
	 */
	public void warn(Marker marker, String format, Object[] argArray) {
		logger.warn(marker, format, argArray);
	}
	
	/**
	 * 
	 * @param marker
	 * @param msg
	 * @param t
	 */
	public void warn(Marker marker, String msg, Throwable t) {
		logger.debug(marker, msg, t);
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isErrorEnabled() {
		return logger.isErrorEnabled();
	}
	
	/**
	 * 
	 * @param msg
	 */
	public void error(String msg) {		
		logger.error(msg);
	}
	
	/**
	 * 
	 * @param format
	 * @param arg
	 */
	public void error(String format, Object arg) {
		logger.error(format, arg);
	}
	
	/**
	 * 
	 * @param format
	 * @param arg1
	 * @param arg2
	 */
	public void error(String format, Object arg1, Object arg2) {
		logger.error(format, arg1, arg2);
	}
	
	/**
	 * 
	 * @param format
	 * @param argArray
	 */
	public void error(String format, Object[] argArray) {
		logger.error(format, argArray);
	}
	
	/**
	 * 
	 * @param msg
	 * @param t
	 */
	public void error(String msg, Throwable t) {
		logger.error(msg, t);
	}
	
	/**
	 * 
	 * @param marker
	 * @return
	 */
	public boolean isErrorEnabled(Marker marker) {
		return logger.isErrorEnabled(marker);
	}
	
	/**
	 * 
	 * @param marker
	 * @param msg
	 */
	public void error(Marker marker, String msg) {
		logger.error(marker, msg);
	}
	
	/**
	 * 
	 * @param marker
	 * @param format
	 * @param arg
	 */
	public void error(Marker marker, String format, Object arg) {
		logger.error(marker, format, arg);
	}
	
	/**
	 * 
	 * @param marker
	 * @param format
	 * @param arg1
	 * @param arg2
	 */
	public void error(Marker marker, String format, Object arg1, Object arg2) {
		logger.error(marker, format, arg1, arg2);
	}
	
	/**
	 * 
	 * @param marker
	 * @param format
	 * @param argArray
	 */
	public void error(Marker marker, String format, Object[] argArray) {		
		logger.error(marker, format, argArray);
	}
	
	/**
	 * 
	 * @param marker
	 * @param msg
	 * @param t
	 */
	public void error(Marker marker, String msg, Throwable t) {
		logger.error(marker, msg, t);
	}
	
	@Override
	public String toString() {
		return "[logger=" + logger.getName() + "]";
	}

}
