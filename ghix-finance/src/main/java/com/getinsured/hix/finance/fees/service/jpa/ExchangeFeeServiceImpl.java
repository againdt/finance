package com.getinsured.hix.finance.fees.service.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.finance.fees.repository.IExchangeFeeRepository;
import com.getinsured.hix.finance.fees.service.ExchangeFeeService;
import com.getinsured.hix.model.ExchangeFee;

/**
 * This service implementation class is for GHIX-FINANCE module for Exchange Fee.
 * This class has various methods to create/modify the ExchangeFee model object.
 * 
 * @author sharma_k
 * @since 28-Jun-2013
 */
@Service("ExchangeFeeService")
public class ExchangeFeeServiceImpl implements ExchangeFeeService {

	@Autowired private IExchangeFeeRepository exchangeFeeRepository;

	/**
	 * Method saveExchangeFee.
	 * @param exchangeFee ExchangeFee
	 * @return ExchangeFee
	 * @see com.getinsured.hix.finance.fees.service.ExchangeFeeService#saveExchangeFee(ExchangeFee)
	 */
	@Override
	@Transactional
	public ExchangeFee saveExchangeFee(ExchangeFee exchangeFee) {
		return exchangeFeeRepository.save(exchangeFee);
	}
	
	
	
	/**
	 * @see com.getinsured.hix.finance.fees.service.ExchangeFeeService#updateExchangeFee(int)
	 * @param int issuerInvoiceID
	 * @return Object<ExchangeFee> | Null if No ExchangeFees found for update
	 */
	@Override
	@Transactional
	public ExchangeFee updateExchangeFee(int issuerInvoiceID)
	{
		ExchangeFee exchangeFee = exchangeFeeRepository.findExchangeFeeByIssuerRemittanceID(issuerInvoiceID);
		if (exchangeFee != null) 
		{
			/*
			 * Note: Jira Ref HIX-44185, Since we are creating Issuer invoice for Paid Employer Invoice
			 * Partial Payment for Carrier will not come to picture.
			 * Exchange Fees getting updated once IssuerInvoice status update from
			 * DUE to IN_PROCESS as exchange does have the pre-Deducted amount in account.
			 */
			exchangeFee.setPaymentReceived(exchangeFee.getTotalFeeBalanceDue());
			exchangeFee.setStatus(ExchangeFee.PaidStatus.PAID);
			return exchangeFeeRepository.save(exchangeFee);
		}
		return null;
	}

	/**
	 * Method findExchangeFeeByIssuerInvoice.
	 * @param issuerInvoiceId int
	 * @return ExchangeFee
	 * @see com.getinsured.hix.finance.fees.service.ExchangeFeeService#findExchangeFeeByIssuerRemittance(int)
	 */
	@Override	
	@Transactional(readOnly=true)
	public ExchangeFee findExchangeFeeByIssuerRemittance(int issuerRemittanceId) {
		ExchangeFee exchangeFee = null;
		exchangeFee = exchangeFeeRepository.findExchangeFeeByIssuerRemittanceID(issuerRemittanceId);
		return exchangeFee;
	}
}
