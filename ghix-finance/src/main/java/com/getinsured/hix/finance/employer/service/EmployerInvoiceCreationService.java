package com.getinsured.hix.finance.employer.service;

import java.util.Date;

import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * 
 * @author Sharma_k
 * @Since 19th December 2014
 * Interface for Employer Invoice creation Service {NORMAL | BINDER}
 */
public interface EmployerInvoiceCreationService 
{
	/**
	 * Method createEmployerInvoice.
	 * @param employerid int
	 * @return int
	 */
	int createEmployerInvoice(int employerid) throws FinanceException;
	
	/**
	 * Service to create Employer Binder Invoice.
	 * @param employerid int
	 * @param nextMonthStartDate String
	 * @param periodCovered String
	 * @return
	 * @throws FinanceException
	 */
	//int createEmployerBinderInvoice(int employerid, Date nextMonthStartDate, String periodCovered) throws FinanceException;
	int createEmployerBinderInvoice(int employerid, int employerEnrollmentId, Date nextMonthStartDate, String periodCovered) throws FinanceException;
	
	/**
	 * Method reissueInvoice.
	 * @param employerid int
	 * @param invoiceid int
	 * @return String
	 * @throws GIException
	 * @throws FinanceException 
	 */
	String reissueInvoice(int employerid,int invoiceid)throws GIException, FinanceException;


}
