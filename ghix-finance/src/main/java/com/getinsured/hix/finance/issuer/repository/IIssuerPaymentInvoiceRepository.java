package com.getinsured.hix.finance.issuer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.IssuerPaymentInvoice;
import com.getinsured.hix.model.IssuerRemittance;

/**
 */
public interface IIssuerPaymentInvoiceRepository extends JpaRepository<IssuerPaymentInvoice, Integer> {
	/**
	 * Method findByIssuerInvoices.
	 * @param issuerRemittance IssuerRemittance
	 * @return IssuerPaymentInvoice
	 */
	IssuerPaymentInvoice findByIssuerRemittance(IssuerRemittance issuerRemittance);

}
