package com.getinsured.hix.finance.paymentmethod.service.jpa;

import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.finance.PaymentMethodDTO;
import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.finance.cybersource.service.CustomerProfileInterface;
import com.getinsured.hix.finance.cybersource.utils.PaymentProcessorStrategy;
import com.getinsured.hix.finance.cybersource.utils.RequestMapper;
import com.getinsured.hix.finance.paymentmethod.repository.IPaymentMethodRepository;
import com.getinsured.hix.finance.paymentmethod.service.PaymentMethodService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceConstants.CyberSourceResponseMapKeys;
import com.getinsured.hix.model.BankInfo;
import com.getinsured.hix.model.CreditCardInfo;
import com.getinsured.hix.model.FinancialInfo;
import com.getinsured.hix.model.FinancialInfo.PaymentType;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.ModuleName;
import com.getinsured.hix.model.PaymentMethods.PaymentIsDefault;
import com.getinsured.hix.model.PaymentMethods.PaymentStatus;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.payment.util.CyberSourceKeyConstants;
import com.getinsured.hix.platform.payment.util.CyberSrcResponseCodes;
import com.getinsured.hix.platform.payment.util.PaymentUtil;
import com.getinsured.hix.platform.payments.repository.ILocRepository;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.exception.GIException;
/**
 * Service Implementation class for FinancePaymentMethodService
 * @since 23rd September 2014
 * @author Sharma_k
 *
 */
@SuppressWarnings("rawtypes")
@Service("paymentMethodService")
public class PaymentMethodServiceImpl implements PaymentMethodService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentMethodServiceImpl.class);
	
	@Autowired private IPaymentMethodRepository financePaymentMethodRepository;
	@Autowired private  ILocRepository locationRepository;
	@Autowired private CustomerProfileInterface customerProfileInterface;
	
	@Autowired private ObjectFactory<QueryBuilder> delegateFactory;	
	
	/*private static final String FILTER_ID = "id";
	private static final String FILTER_PAYMENT_TYPE = "paymentType";
	private static final String FILTER_MODULE_NAME = "moduleName";
	private static final String FILTER_UPDATED_DATE = "updatedDate";
	private static final String FILTER_SORT_ORDER =  "sortOrder";
	private static final String FILTER_SORT_BY = "sortBy";*/
	
	@Override
	@Transactional(readOnly=true)
	public List<PaymentMethods> searchByPaymentMethodName(final String paymentMethodName)
	{
		return financePaymentMethodRepository.findByPaymentMethodName(paymentMethodName);
	}
	
	@Override
	public PaymentMethods getPaymentMethodByPCI(final int paymentId)throws GIException
	{
		final PaymentMethods paymentMethods = financePaymentMethodRepository.findById(paymentId);

		final String isPaymentGateWay = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_PAYMENT_GATEWAY);
		if (StringUtils.isNotEmpty(isPaymentGateWay) && isPaymentGateWay.equalsIgnoreCase(FinanceConstants.TRUE) && paymentMethods.getSubscriptionId() != null)
		{
			//PCI PaymentMethod
			LOGGER.info("PCI: Editing the payment method through PCI complaince");
			final Map reply = customerProfileInterface.retrieveCustomerProfile(paymentMethods.getSubscriptionId());
			if (verifyCybersourceReply(reply))
			{
				final FinancialInfo financialInfo = paymentMethods.getFinancialInfo();
				final String paymentType = paymentMethods.getPaymentType().toString();
				if (paymentType.equals(PaymentType.BANK.toString()) || paymentType.equals(PaymentType.CHECK.toString())
						|| paymentType.equals(PaymentType.EFT.toString()) || paymentType.equals(PaymentType.ACH.toString()))
				{
					final BankInfo bankInfo = this.populateBankInfo(financialInfo.getBankInfo(), reply);					
					financialInfo.setBankInfo(bankInfo);
				}
				else if (paymentType.equals(PaymentType.CREDITCARD.toString()))
				{
					final CreditCardInfo creditCardInfo = this.populateCreditCardInfo(financialInfo.getCreditCardInfo(), reply);					
					financialInfo.setCreditCardInfo(creditCardInfo);
				}
				paymentMethods.setFinancialInfo(financialInfo);
				return paymentMethods;
			}
			else
			{
				throw new GIException("PCI: Unable to retreive Customer profile Info"+ paymentMethods.getSubscriptionId());
			}
		}
		else
		{
			//Non PCI PaymentMethod
			return paymentMethods;
		}
	}
	private boolean verifyCybersourceReply(final Map reply)
	{
		if (reply != null && !reply.isEmpty() 
				&& reply.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE) 
				&&Integer.parseInt(reply.get(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE).toString()) == FinanceConstants.NUMERICAL_100)
		{
			return true;
		}
		return false;
	}
	private BankInfo populateBankInfo(final BankInfo bankInfo, final Map reply)
	{
		if (reply.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CHECKACCOUNTNUMBER_KEY))
		{
			bankInfo.setAccountNumber((String) reply.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CHECKACCOUNTNUMBER_KEY));
		}
		if (reply.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CHECKBANKTRANSITNUMBER_KEY))
		{
			bankInfo.setRoutingNumber((String) reply.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CHECKBANKTRANSITNUMBER_KEY));
		}
		if (reply.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CHECKACCOUNTTYPE_KEY)) {
			bankInfo.setAccountType((String) reply.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CHECKACCOUNTTYPE_KEY));
		}
		return bankInfo;
	}
	
	private CreditCardInfo populateCreditCardInfo(final CreditCardInfo creditCardInfo, final Map reply)
	{
		if (reply.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CARDEXPIRATIONMONTH_KEY))
		{
			creditCardInfo.setExpirationMonth((String) reply.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CARDEXPIRATIONMONTH_KEY));
		}
		if (reply.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CARDEXPIRATIONYEAR_KEY))
		{
			creditCardInfo.setExpirationYear((String) reply.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CARDEXPIRATIONYEAR_KEY));
		}
		if (reply.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CARDTYPE_KEY))
		{
			final String cardType = PaymentUtil.getCardType((String) reply.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CARDTYPE_KEY));
			creditCardInfo.setCardType(cardType);
		}
		if (reply.containsKey(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CARDACCOUNTNUMBER_KEY))
		{
			creditCardInfo.setCardNumber((String) reply.get(CyberSourceKeyConstants.PAYSUBSCRIPTIONRETRIEVEREPLY_CARDACCOUNTNUMBER_KEY));
		}
		return creditCardInfo;		
	}
	
	@Override
	@Transactional
	public void migrateIssuerWithPci() throws GIException{

		List<PaymentMethods> paymentMethodsLst = null;
		final String isPaymentGateWay = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_PAYMENT_GATEWAY);
		
		if(isPaymentGateWay != null && isPaymentGateWay.equalsIgnoreCase(Boolean.TRUE.toString()))
		{
			paymentMethodsLst = new ArrayList<PaymentMethods>();

			final List<PaymentMethods> paymentMethodsList = financePaymentMethodRepository
					.findPaymentMethodLstByModuleNameAndPaymentType(
							PaymentMethods.ModuleName.ISSUER,
							PaymentMethods.PaymentType.BANK);

			if(paymentMethodsList != null && !paymentMethodsList.isEmpty())
			{
				this.migrate(paymentMethodsList, paymentMethodsLst);
			}
			if(null != paymentMethodsLst && !paymentMethodsLst.isEmpty())
			{
				LOGGER.debug("Payment Method migrated", paymentMethodsLst.size());
				financePaymentMethodRepository.save(paymentMethodsLst);
			}
		}
	}
	
	private void migrate(final List<PaymentMethods> paymentMethodsList, final List<PaymentMethods> paymentMethodsLst) throws GIException
	{
		LOGGER.debug("Payment Method to be migrated", paymentMethodsList.size());
		final Map<String, Exception> exceptionMap = new HashMap<String, Exception>();
		Map createRequest = null;
		RequestMapper requestMapper = null;
		boolean isLocationFlag = false;
		for (final PaymentMethods eachPaymentMethod : paymentMethodsList) {
			try{
				requestMapper = new RequestMapper();
				if (eachPaymentMethod.getFinancialInfo().getLocation() != null)
				{
					isLocationFlag = true;
				}
				createRequest = requestMapper.createCustomerProfileReqNonLocation(eachPaymentMethod, isLocationFlag);
				final Map reply = customerProfileInterface.createCustomerProfile(createRequest);
				if (verifyCybersourceReply(reply))
				{	
					eachPaymentMethod.setSubscriptionId(reply.get(CyberSourceKeyConstants.PAYSUBSCRIPTION_CREATEREPLY_SUBSCRIPTIONID_KEY).toString());
					final FinancialInfo financialInfo = eachPaymentMethod.getFinancialInfo();
					this.populateNULLFinancialInfo(financialInfo, eachPaymentMethod.getPaymentType().toString());
					eachPaymentMethod.setFinancialInfo(financialInfo);					
					paymentMethodsLst.add(eachPaymentMethod);					
					}else {
						throw new GIException(FinanceConstants.ERROR_CODE_201,
								FinanceConstants.ERR_MSG_CYBERSOURCE_REPLY_NULL,
								FinanceConstants.EXCEPTION_SEVERITY_HIGH);
					}
		
			}catch(final GIException giEx)
			{
				LOGGER.error("Exception in Payment Method", eachPaymentMethod.toString());
				LOGGER.error("GIException thrown - ",giEx);
				exceptionMap.put(eachPaymentMethod.getId()+"-"+eachPaymentMethod.getPaymentMethodName(), giEx);

			}catch(final Exception ex)
			{
				LOGGER.error("Exception thrown - ",ex);
				exceptionMap.put(eachPaymentMethod.getId()+"-"+eachPaymentMethod.getPaymentMethodName(), ex);
			}
		}
		if(!exceptionMap.isEmpty())
		{
			final StringBuilder sb = new StringBuilder();
			Iterator itr = exceptionMap.entrySet().iterator();
			while (itr.hasNext())
			{
				Map.Entry entry = (Map.Entry)itr.next();
				sb.append("Exception in migration for PAYMENT METHOD ID-PAYMENT METHOD NAME :: ");
				sb.append(entry.getKey());
				sb.append(System.getProperty(FinanceConstants.LINE_DOT_SEPARATOR));
				sb.append(entry.getValue());
				sb.append(System.getProperty(FinanceConstants.LINE_DOT_SEPARATOR));
				sb.append(System.getProperty(FinanceConstants.LINE_DOT_SEPARATOR));
			}
			throw new GIException(sb.toString());					
		}
	}
	private void populateNULLFinancialInfo(final FinancialInfo financialInfo, final String paymentType)
	{
		if(paymentType.equals(PaymentType.BANK.toString())
				|| paymentType.equals(PaymentType.EFT.toString()) || paymentType.equals(PaymentType.ACH.toString()))
			{
			financialInfo.getBankInfo().setAccountNumber(null);
			financialInfo.getBankInfo().setRoutingNumber(null);
			financialInfo.getBankInfo().setAccountType(null);
			financialInfo.setCreditCardInfo(null);
			}
		else if (paymentType.equals(PaymentType.CREDITCARD.toString()))
		{
			financialInfo.getCreditCardInfo().setCardNumber(null);
			financialInfo.getCreditCardInfo().setCardType(null);
			financialInfo.getCreditCardInfo().setExpirationMonth(null);
			financialInfo.getCreditCardInfo().setExpirationYear(null);
			financialInfo.setBankInfo(null);
		}
	}
	
	@Override	
	@Transactional
	public PaymentMethods savePaymentMethods(final PaymentMethods paymentMethods) throws GIException
	{
		final String isPaymentGateWay = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_PAYMENT_GATEWAY);
		final String paymentType = paymentMethods.getPaymentType().toString();
		if( (isPaymentGateWay != null && isPaymentGateWay.equalsIgnoreCase(Boolean.FALSE.toString())) 
				|| FinanceConstants.YES.equalsIgnoreCase(PaymentProcessorStrategy.USE_MOCKFOR_PAYMENT_AND_REPORT))
		{
			Location location = null;
			FinancialInfo financialInfo = paymentMethods.getFinancialInfo();
			if(financialInfo.getLocation() != null)
			{
				location = locationRepository.findOne(financialInfo.getLocation().getId());
			}
			if(location !=null)
			{
				financialInfo.setLocation(location);
			}		
			if(paymentType.equals(PaymentType.BANK.toString()) 
					|| paymentType.equals(PaymentType.EFT.toString()) || paymentType.equals(PaymentType.ACH.toString()))
			{
				paymentMethods.getFinancialInfo().setCreditCardInfo(null);
			}
			else if (paymentType.equals(PaymentType.CREDITCARD.toString()))
			{
				paymentMethods.getFinancialInfo().setBankInfo(null);
			}
			else{
				paymentMethods.getFinancialInfo().setBankInfo(null);
				paymentMethods.getFinancialInfo().setCreditCardInfo(null);
			}
			
			return financePaymentMethodRepository.saveAndFlush(paymentMethods);
		}
		else if( (paymentType.equals(PaymentType.MANUAL.toString()) || paymentType.equals(PaymentType.CHECK.toString()) ) && DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.ELECTRONIC_PAYMENT_ONLY).equals(FinanceConstants.NO) ) 
		{
			Location location = null;
			FinancialInfo financialInfo = paymentMethods.getFinancialInfo();
			if(financialInfo.getLocation() != null)
			{
				location = locationRepository.findOne(financialInfo.getLocation().getId());
			}
			if(location !=null)
			{
				financialInfo.setLocation(location);
			}
			paymentMethods.getFinancialInfo().setBankInfo(null);
			paymentMethods.getFinancialInfo().setCreditCardInfo(null);
			paymentMethods.setFinancialInfo(financialInfo);	
			return financePaymentMethodRepository.saveAndFlush(paymentMethods);
		}
		else if( (paymentType.equals(PaymentType.MANUAL.toString()) || paymentType.equals(PaymentType.CHECK.toString())) && DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.ELECTRONIC_PAYMENT_ONLY).equals(FinanceConstants.YES))
		{
			throw new GIException(FinanceConstants.ERROR_CODE_201, "InValid configuration: MANUAL payment method is not allowed when ELECTRONIC_PAYMENT_ONLY is YES",FinanceConstants.EXCEPTION_SEVERITY_HIGH);
		}
		else
		{
			String addOrUpdate = FinanceConstants.ADD;
			if(paymentMethods.getId() != FinanceConstants.NUMERICAL_0)
			{
				addOrUpdate=FinanceConstants.UPDATE;
				
			}
			return savePaymentMethod(paymentMethods, paymentType, addOrUpdate);			
		}
	}
	
	
	private PaymentMethods savePaymentMethod(final PaymentMethods paymentMethods, final String paymentType, final String addOrUpdate) throws GIException
	{
		Map reply = this.callCyberSource(paymentMethods, addOrUpdate);
		if(reply != null && !reply.isEmpty())
		{			
			final int reasonCode = Integer.parseInt(reply.get(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE).toString());
			final FinancialInfo financialInfo = paymentMethods.getFinancialInfo();
			Location loc = null;
			if(financialInfo != null && StringUtils.isNotEmpty(addOrUpdate))
			{	
				if(financialInfo.getLocation() != null && FinanceConstants.ADD.equals(addOrUpdate))
				{
					loc = locationRepository.findOne(financialInfo.getLocation().getId());
				}
				if(reasonCode == FinanceConstants.NUMERICAL_100)
				{
					this.populateNULLFinancialInfo(financialInfo, paymentType);
					if(FinanceConstants.ADD.equals(addOrUpdate))
					{
						paymentMethods.setSubscriptionId(reply.get("paySubscriptionCreateReply_subscriptionID").toString());
						if(loc !=null)
						{
							financialInfo.setLocation(loc);
						}
					}
					paymentMethods.setFinancialInfo(financialInfo);
					return financePaymentMethodRepository.saveAndFlush(paymentMethods);
				}
				else if(reasonCode == FinanceConstants.NUMERICAL_102 && financialInfo != null && financialInfo.getBankInfo() != null)
				{
					String response = "";
					if(paymentType.equals(PaymentType.BANK.toString()) || paymentType.equals(PaymentType.ACH.toString()) || paymentType.equals(PaymentType.EFT.toString()))
					{
						response = "Invalid Bank Account details , Please provide valid information"; 
					}
					else if(paymentType.equals(PaymentType.CREDITCARD.toString()))
					{
						response = "Invalid Credit Card details , Please provide valid information";
					}
					if(reply.containsKey(CyberSourceResponseMapKeys.CYBERSOURCE_INVALID_FIELD_0))
					{
						if("check_bankTransitNumber".equalsIgnoreCase(reply.get(CyberSourceResponseMapKeys.CYBERSOURCE_INVALID_FIELD_0).toString()))
						{
							response = "Invalid Bank Routing No: "+financialInfo.getBankInfo().getRoutingNumber();
						}
						else if("check_accountNumber".equalsIgnoreCase(reply.get(CyberSourceResponseMapKeys.CYBERSOURCE_INVALID_FIELD_0).toString()))
						{
							response = "Invalid Bank Account No: "+financialInfo.getBankInfo().getAccountNumber();
						}
					}
					throw new GIException(FinanceConstants.ERROR_CODE_201, response, FinanceConstants.EXCEPTION_SEVERITY_HIGH);
				}
				else {
					String errMsg="";
					if(FinanceConstants.UPDATE.equals(addOrUpdate)){
						errMsg="Failed to update information, Please contact Administrator";
					}
					else
					{
						errMsg="Failed to add payment method, Please contact Administrator";
					}
					if (CyberSrcResponseCodes.resultCodes().containsKey(reply.get(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE).toString()))
					{
						final String response = CyberSrcResponseCodes.resultCodes().get(reply.get(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE).toString());
						throw new GIException(FinanceConstants.ERROR_CODE_201, response, FinanceConstants.EXCEPTION_SEVERITY_HIGH);
					} else {
						throw new GIException(FinanceConstants.ERROR_CODE_201,errMsg,FinanceConstants.EXCEPTION_SEVERITY_HIGH);
					}				
				}
			}
			else
			{
				throw new GIException(FinanceConstants.ERROR_CODE_201,
						"Null or empty FinancialInfo object found OR Add/Update Flag is empty or null",
						FinanceConstants.EXCEPTION_SEVERITY_HIGH);	
			}
		}
		else
		{
			throw new GIException(FinanceConstants.ERROR_CODE_201,
					FinanceConstants.ERR_MSG_CYBERSOURCE_REPLY_NULL,
					FinanceConstants.EXCEPTION_SEVERITY_HIGH);			
		}
		
	}
	private Map callCyberSource(final PaymentMethods paymentMethods, final String addOrUpdate) throws GIException
	{
		final RequestMapper requestMapper = new RequestMapper();
		Map reply = null;
		if(addOrUpdate.equals(FinanceConstants.ADD))
		{
			Map createRequest = null;
			boolean isLocationFlag = false;
			
			if (paymentMethods.getFinancialInfo().getLocation() != null)
			{
				isLocationFlag = true;
			}
			createRequest = requestMapper.createCustomerProfileReqNonLocation(paymentMethods, isLocationFlag);			
			reply = customerProfileInterface.createCustomerProfile(createRequest);
			
		}
		else
		{
			if(StringUtils.isEmpty(paymentMethods.getSubscriptionId()))
			{
				throw new GIException(FinanceConstants.ERROR_CODE_201, "For Edit Payment method Subscription id is null or empty", FinanceConstants.EXCEPTION_SEVERITY_HIGH);
			}
			final Map userProfile = customerProfileInterface.retrieveCustomerProfile(paymentMethods.getSubscriptionId());
			final Map updateRequest = requestMapper.updateCustomerProfileRequest(paymentMethods, userProfile);
			reply = customerProfileInterface.updateCustomerProfile(updateRequest);
		}
		return reply;
	}
	
	@Override
	public PaymentMethods modifyPaymentstatus(int paymentId, PaymentIsDefault isdefault, PaymentStatus status, Integer lastUpdatedBy)
	{
		PaymentMethods paymentMethods = null;
		if(isdefault != null)
		{
			paymentMethods = this.changeDefaultSetting(paymentId, isdefault, lastUpdatedBy);
		}
		else if(status != null)
		{
			paymentMethods = this.updateStatus(paymentId, status, lastUpdatedBy);			
		}			
		return paymentMethods;
	}
	
	@Override	
	@Transactional(readOnly=true)
	public  PaymentMethods getPaymentMethodsDetail(final int paymentId) {		
		return financePaymentMethodRepository.findById(paymentId);				
	}
	
	@Override
	@Transactional
	public  PaymentMethods updateStatus(final int paymentId, final PaymentStatus status, Integer lastUpdatedBy) {		
		final PaymentMethods paymentMethods = getPaymentMethodsDetail(paymentId);
		paymentMethods.setStatus(status);
		paymentMethods.setLastUpdatedBy(lastUpdatedBy);
		return financePaymentMethodRepository.save(paymentMethods);
	}

	@Override
	@Transactional
	public PaymentMethods changeDefaultSetting(final int paymentId, final PaymentIsDefault isdefault, Integer lastUpdatedBy) {		
		final PaymentMethods paymentMethods = getPaymentMethodsDetail(paymentId);
		paymentMethods.setIsDefault(isdefault);
		paymentMethods.setLastUpdatedBy(lastUpdatedBy);
		return financePaymentMethodRepository.save(paymentMethods);
	}

	
	@Override
	public Page<PaymentMethods> getPaymentMethodByMouduleIdAndModuleNameAndPageable(
			int moduleId, ModuleName moduleName, Pageable pageable) {
		return financePaymentMethodRepository.findByModuleIdAndModuleName(moduleId, moduleName, pageable);
	}
		

	@Override
	public Map<String, Object> searchEmployerPaymentMethod(PaymentMethodRequestDTO paymentMethodRequestDTO)throws GIException
	{
		QueryBuilder<PaymentMethods> paymentTypeQuery = prepareQueryBuilder(paymentMethodRequestDTO);
		LOGGER.info("Inside searchEmployerPaymentMethod");
		List<PaymentMethods> paymentMethodList = null;
		Map<String, Object> paymentMethodListAndRecordCount = new HashMap<String, Object>();
		Long recordCount = 0L;
		
		try
		{
			if(paymentMethodRequestDTO.isPaginationRequired())
			{
				paymentMethodList = paymentTypeQuery.getRecords(paymentMethodRequestDTO.getStartRecord(), paymentMethodRequestDTO.getPageSize());
			}
			else
			{
				paymentMethodList = paymentTypeQuery.getRecords(0, -1);
			}
			recordCount = paymentTypeQuery.getRecordCount();
		}
		catch(Exception ex)
		{
			LOGGER.error("Exception caught in searchEmployerPaymentMethod(-) ", ex);
			throw new GIException(ex);
		}
		if(paymentMethodList != null && !paymentMethodList.isEmpty())
		{
			paymentMethodListAndRecordCount.put(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY, paymentMethodList);
			paymentMethodListAndRecordCount.put(GhixConstants.FinancePaymentMethodResponse.RECORD_COUNT_KEY, recordCount);
		}
		
		return paymentMethodListAndRecordCount;
	}
	
	/**
	 * Generic search for PaymentMethod 
	 */
	@SuppressWarnings("unchecked")
	private QueryBuilder<PaymentMethods> prepareQueryBuilder(PaymentMethodRequestDTO paymentMethodRequestDTO)
	{
		final QueryBuilder<PaymentMethods> paymentTypeQuery = delegateFactory.getObject();
		paymentTypeQuery.buildObjectQuery(PaymentMethods.class);
		
		if(paymentMethodRequestDTO.getId() != null && paymentMethodRequestDTO.getId() != 0)
		{
			paymentTypeQuery.applyWhere(FinanceConstants.ID_SMALL_CASE, paymentMethodRequestDTO.getId(), DataType.NUMERIC, ComparisonType.EQ);
		}
		if(paymentMethodRequestDTO.getPaymentType() != null && StringUtils.isNotEmpty(paymentMethodRequestDTO.getPaymentType().toString()))
		{
			paymentTypeQuery.applyWhere(FinanceConstants.PAYMENT_TYPE_LOWERCASE_KEY,
					paymentMethodRequestDTO.getPaymentType(), DataType.ENUM, ComparisonType.EQ);
		}
		if(paymentMethodRequestDTO.getPaymentTypeList() != null && !paymentMethodRequestDTO.getPaymentTypeList().isEmpty())
		{
			paymentTypeQuery.applyWhere(FinanceConstants.PAYMENT_TYPE_LOWERCASE_KEY,
					paymentMethodRequestDTO.getPaymentTypeList(), DataType.LIST, ComparisonType.EQ);
		}
		
		if(paymentMethodRequestDTO.getPaymentMethodStatus() != null &&
				StringUtils.isNotEmpty(paymentMethodRequestDTO.getPaymentMethodStatus().toString()))
		{
			paymentTypeQuery.applyWhere(FinanceConstants.STATUS,
					paymentMethodRequestDTO.getPaymentMethodStatus(), DataType.ENUM, ComparisonType.EQ);
		}
		
		if(paymentMethodRequestDTO.getPaymentMethodStatusList() != null &&
				!paymentMethodRequestDTO.getPaymentMethodStatusList().isEmpty())
		{
			paymentTypeQuery.applyWhere(FinanceConstants.STATUS,
					paymentMethodRequestDTO.getPaymentMethodStatusList() , DataType.LIST, ComparisonType.EQ);
		}
		
		if(paymentMethodRequestDTO.getIsDefaultPaymentMethod() != null &&
				StringUtils.isNotEmpty(paymentMethodRequestDTO.getIsDefaultPaymentMethod().toString()))
		{
			paymentTypeQuery.applyWhere(FinanceConstants.IS_DEFAULT_KEY,
					paymentMethodRequestDTO.getIsDefaultPaymentMethod(), DataType.ENUM, ComparisonType.EQ);
		}
		
		if(StringUtils.isNotEmpty(paymentMethodRequestDTO.getPaymentMethodName()))
		{
			paymentTypeQuery.applyWhere(FinanceConstants.PAYMENT_METHOD_NAME_KEY,
					paymentMethodRequestDTO.getPaymentMethodName().toUpperCase(), DataType.STRING, QueryBuilder.ComparisonType.LIKE);
		}
		
		if(paymentMethodRequestDTO.getPaymentMethodNameList() != null &&
				!paymentMethodRequestDTO.getPaymentMethodNameList().isEmpty())
		{
			paymentTypeQuery.applyWhere(FinanceConstants.PAYMENT_METHOD_NAME_KEY,
					paymentMethodRequestDTO.getPaymentMethodNameList(), DataType.LIST, ComparisonType.EQ);
		}
		
		if(paymentMethodRequestDTO.getModuleID() != null &&
				paymentMethodRequestDTO.getModuleID() != 0)
		{
			paymentTypeQuery.applyWhere(FinanceConstants.MODULEID_KEY,
					paymentMethodRequestDTO.getModuleID(), DataType.NUMERIC, ComparisonType.EQ);
		}
		
		if(paymentMethodRequestDTO.getModuleName() != null &&
				StringUtils.isNotEmpty(paymentMethodRequestDTO.getModuleName().toString()))
		{
			paymentTypeQuery.applyWhere(FinanceConstants.MODULENAME_KEY,
					paymentMethodRequestDTO.getModuleName(), DataType.ENUM, ComparisonType.EQUALS);
		}
		
		if(StringUtils.isNotEmpty(paymentMethodRequestDTO.getSubscriptionID()))
		{
			paymentTypeQuery.applyWhere(FinanceConstants.SUBSCRIPTIONID_KEY,
					paymentMethodRequestDTO.getSubscriptionID(), DataType.STRING, ComparisonType.EQUALS);
		}
		
		if (paymentMethodRequestDTO.getUpdatedOn() != null)
		{
			final Calendar calendarDate = TSCalendar.getInstance();
			calendarDate.setTime(paymentMethodRequestDTO.getUpdatedOn());
			paymentTypeQuery.applyWhere(FinanceConstants.UPDATEDON_KEY, calendarDate.getTime(), QueryBuilder.DataType.DATE, QueryBuilder.ComparisonType.GT);

			calendarDate.setTime(paymentMethodRequestDTO.getUpdatedOn());
			calendarDate.set(Calendar.HOUR_OF_DAY, calendarDate.getActualMaximum(Calendar.HOUR_OF_DAY));
			calendarDate.set(Calendar.MINUTE, calendarDate.getActualMaximum(Calendar.MINUTE));
			calendarDate.set(Calendar.SECOND, calendarDate.getActualMaximum(Calendar.SECOND));
			paymentTypeQuery.applyWhere(FinanceConstants.UPDATEDON_KEY, calendarDate.getTime(), QueryBuilder.DataType.DATE, QueryBuilder.ComparisonType.LT);
		}
		
		if(StringUtils.isNotEmpty(paymentMethodRequestDTO.getSortOrder()) && StringUtils.isNotEmpty(paymentMethodRequestDTO.getSortBy()))
		{
			paymentTypeQuery.applySort(paymentMethodRequestDTO.getSortBy(), QueryBuilder.SortOrder.valueOf(paymentMethodRequestDTO.getSortOrder()));
		}
		
		return paymentTypeQuery;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Map<Integer, Object>> migratePaymentMethodPci(List<PaymentMethodDTO> paymentMethodDTOList) throws GIException{

		List<Map<Integer, Object>> paymentMethodResponseList = new ArrayList();				
		
		List<Integer> paymentIdList = new ArrayList();
		final String isPaymentGateWay = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_PAYMENT_GATEWAY);
		
			if(isPaymentGateWay != null && isPaymentGateWay.equalsIgnoreCase(Boolean.TRUE.toString()))
			{
				Map<Integer, String> payIdSubcriptionKeyMap = callCyberSrcForMigratePaymentMethod(paymentMethodDTOList, paymentMethodResponseList);				
				paymentIdList.addAll(payIdSubcriptionKeyMap.keySet());
				
				List<PaymentMethods> paymentMethods = new ArrayList();
				List<PaymentMethods> paymentMethodsLst = new ArrayList();
				Iterable<PaymentMethods> paymentMethodsItr  = null;
				
				if(!paymentIdList.isEmpty())
				{
					
					paymentMethodsItr = financePaymentMethodRepository.findAll(paymentIdList);
					CollectionUtils.addAll(paymentMethods, paymentMethodsItr.iterator());										
				}
				else
				{
					throw new GIException(FinanceConstants.ERROR_CODE_201, "Cyber source call failed", FinanceConstants.EXCEPTION_SEVERITY_HIGH);
				}
				
				if(paymentMethods != null && !paymentMethods.isEmpty())
				{
					for(PaymentMethods eachPaymentMethod : paymentMethods)
					{							
						try
						{
							if(payIdSubcriptionKeyMap.containsKey(eachPaymentMethod.getId()))
							{
								eachPaymentMethod.setSubscriptionId(payIdSubcriptionKeyMap.get(eachPaymentMethod.getId()));
								final FinancialInfo financialInfo = eachPaymentMethod.getFinancialInfo();
								this.populateNULLFinancialInfo(financialInfo, eachPaymentMethod.getPaymentType().toString());
								eachPaymentMethod.setFinancialInfo(financialInfo);					
								paymentMethodsLst.add(eachPaymentMethod);
							}
						}	
						catch(Exception e){
							/*If exception occurs after the cyber source call while updating PAYMENT_METHODS, FINANCIAL_INFO tables then throw GIException.*/
							throw new GIException(e);								
						}			
					}
					if(paymentMethodsLst != null && !paymentMethodsLst.isEmpty())
					{
						LOGGER.debug("Payment Method migrated", paymentMethods.size());
						financePaymentMethodRepository.save(paymentMethods);
					}
				}			
			}
			return paymentMethodResponseList;
	}
	
	@SuppressWarnings("unchecked")
	private Map<Integer, String> callCyberSrcForMigratePaymentMethod(final List<PaymentMethodDTO> paymentMethodDTOList, final List<Map<Integer, Object>> paymentMethodResponseList) throws GIException
	{
		LOGGER.debug("Payment Method to be migrated", paymentMethodDTOList.size());
		final Map<Integer, Object> exceptionMap = new HashMap();
		Map createRequest = null;				
		Map<Integer, String> payIdSubcriptionKeyMap = new HashMap<>();		
		
		for (final PaymentMethodDTO eachPaymentMethodDTO : paymentMethodDTOList) {
			try{
				/*create request map from input PaymentMethodDTO*/
				createRequest = this.createCustomerProfileReqForMigration(eachPaymentMethodDTO);
				final Map reply = customerProfileInterface.createCustomerProfile(createRequest);
				if (verifyCybersourceReply(reply))
				{	
					payIdSubcriptionKeyMap.put(eachPaymentMethodDTO.getPaymentMethodId(), reply.get(CyberSourceKeyConstants.PAYSUBSCRIPTION_CREATEREPLY_SUBSCRIPTIONID_KEY).toString());
					
				}
				else if (CyberSrcResponseCodes.resultCodes().containsKey(reply.get(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE).toString()))
				{
					final String response = CyberSrcResponseCodes.resultCodes().get(reply.get(CyberSourceResponseMapKeys.CYBERSOURCE_REASONCODE).toString());
					throw new GIException(FinanceConstants.ERROR_CODE_201, response, FinanceConstants.EXCEPTION_SEVERITY_HIGH);
				}
				else
				{
					throw new GIException(FinanceConstants.ERROR_CODE_201, "UnExpected Error - Migration Failed.", FinanceConstants.EXCEPTION_SEVERITY_HIGH);
				}
		
			}catch(final GIException giEx)
			{
				LOGGER.error("Exception in cyber source call for payment method migration for Payment Method Id: ", eachPaymentMethodDTO.getPaymentMethodId());
				LOGGER.error("GIException thrown - ",giEx);
				/*If cyber source  calls fails then store the paymentId in map and continue with next record.*/
				exceptionMap.put(eachPaymentMethodDTO.getPaymentMethodId(), giEx.toString());

			}catch(final Exception ex)
			{
				LOGGER.error("Exception thrown - ",ex);
				/*If cyber source  calls fails then store the paymentId in map and continue with next record.*/
				exceptionMap.put(eachPaymentMethodDTO.getPaymentMethodId(), ex.toString());
			}			
		}
		if(!exceptionMap.isEmpty())
		{
			paymentMethodResponseList.add(exceptionMap);
		}
		return payIdSubcriptionKeyMap;
	}
	
	public Map createCustomerProfileReqForMigration(PaymentMethodDTO paymentMethodDTO)throws GIException
	{
		Map<String, String> requestMap = new HashMap<String, String>();
		if(paymentMethodDTO != null)
		{
			if(StringUtils.isNotEmpty(paymentMethodDTO.getFirstName()))
			{
				requestMap.put(CyberSourceKeyConstants.BILLTO_FIRSTNAME_KEY, paymentMethodDTO.getFirstName());
			}
			else{
				throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception First Name is null or empty","HIGH");
			}
			if(StringUtils.isNotEmpty(paymentMethodDTO.getLastName()))
			{
				requestMap.put(CyberSourceKeyConstants.BILLTO_LASTNAME_KEY, paymentMethodDTO.getLastName());
			}
			else{
				throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Last Name is null or empty","HIGH");
			}					
					
			if(StringUtils.isNotEmpty(paymentMethodDTO.getAddress1()))
			{
				requestMap.put(CyberSourceKeyConstants.BILLTO_STREET1_KEY, paymentMethodDTO.getAddress1());
			}
			else{
				throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Street Address1 details is null or empty","HIGH");
			}
			if(StringUtils.isNotEmpty(paymentMethodDTO.getCity())){
				requestMap.put(CyberSourceKeyConstants.BILLTO_CITY_KEY, paymentMethodDTO.getCity());
			}
			else{
				throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception city detail is null or empty","HIGH");
			}
			if(StringUtils.isNotEmpty(paymentMethodDTO.getState())){
				requestMap.put(CyberSourceKeyConstants.BILLTO_STATE_KEY, paymentMethodDTO.getState());
			}
			else{
				throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception state detail is null or empty","HIGH");
			}
			if(StringUtils.isNotEmpty(paymentMethodDTO.getZipcode())){
				requestMap.put(CyberSourceKeyConstants.BILLTO_POSTALCODE_KEY, paymentMethodDTO.getZipcode());
			}
			else{
				throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception ZipCode detail is null or empty","HIGH");
			}
			//================ code changes end here
					
					
			requestMap.put(CyberSourceKeyConstants.BILLTO_COUNTRY_KEY, "USA");
			if(StringUtils.isNotEmpty(paymentMethodDTO.getEmail())){
				requestMap.put(CyberSourceKeyConstants.BILLTO_EMAIL_KEY, paymentMethodDTO.getEmail());
			}
			else{
				throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception email detail is null or empty","HIGH");
			}
			if(StringUtils.isNotEmpty(paymentMethodDTO.getContactNumber())){
				requestMap.put(CyberSourceKeyConstants.BILLTO_PHONENUMBER_KEY, paymentMethodDTO.getContactNumber());
			}else{
				throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception contact number detail is null or empty","HIGH");
			}
			
			requestMap.put(CyberSourceKeyConstants.PURCHASETOTALS_CURRENCY_KEY, CyberSourceKeyConstants.PT_CURRENCY);
			if(StringUtils.isNotEmpty(paymentMethodDTO.getPaymentMethodName())){
				requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_TITLE_KEY, paymentMethodDTO.getPaymentMethodName());
			}else{
				throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Payment Method Name detail is null or empty","HIGH");
			}
			requestMap.put(CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_FREQUENCY_KEY, CyberSourceKeyConstants.RECURRINGSUBSCRIPTIONINFO_FREQUENCY);
					
			if(paymentMethodDTO.getPaymentType() == PaymentMethods.PaymentType.BANK 
					|| paymentMethodDTO.getPaymentType() == PaymentMethods.PaymentType.EFT || paymentMethodDTO.getPaymentType() == PaymentMethods.PaymentType.ACH){
				
				requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY, "check");
				if(StringUtils.isNotEmpty(paymentMethodDTO.getAccountNumber()))
				{
					requestMap.put(CyberSourceKeyConstants.CHECK_ACCOUNTNUMBER_KEY, paymentMethodDTO.getAccountNumber());
				}
				else
				{
					throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Bank Account Number is null or empty","HIGH");
				}
				if(StringUtils.isNotEmpty(paymentMethodDTO.getAccountType()))
				{
					requestMap.put(CyberSourceKeyConstants.CHECK_ACCOUNTTYPE_KEY, paymentMethodDTO.getAccountType());
				}
				else
				{
					throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Bank Account Type is null or empty","HIGH");
				}
				if(StringUtils.isNotEmpty(paymentMethodDTO.getRoutingNumber()))
				{
					requestMap.put(CyberSourceKeyConstants.CHECK_BANKTRANSITNUMBER_KEY, paymentMethodDTO.getRoutingNumber());
				}
				else
				{
					throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception ABA Routing Number is null or empty","HIGH");
				}
				requestMap.put(CyberSourceKeyConstants.CHECK_SECCODE_KEY, "WEB");							
				
			}
			else if(paymentMethodDTO.getPaymentType().equals(PaymentMethods.PaymentType.CREDITCARD)){
				
				requestMap.put(CyberSourceKeyConstants.SUBSCRIPTION_PAYMENTMETHOD_KEY, "credit card");
				if(StringUtils.isNotEmpty(paymentMethodDTO.getCardType()))
				{
					String cardTypeKey = PaymentUtil.getCardTypeCode(paymentMethodDTO.getCardType());
					requestMap.put(CyberSourceKeyConstants.CARD_CARDTYPE_KEY,cardTypeKey);
				}
				else
				{
					throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Credit card Type is null or empty","HIGH");
				}
				if(StringUtils.isNotEmpty(paymentMethodDTO.getCardNumber()))
				{
					requestMap.put(CyberSourceKeyConstants.CARD_ACCOUNTNUMBER_KEY, paymentMethodDTO.getCardNumber());
				}
				else
				{
					throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Card Number is null or empty","HIGH");
				}
				
				if(StringUtils.isNotEmpty(paymentMethodDTO.getExpirationMonth()))
				{
					requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONMONTH_KEY, paymentMethodDTO.getExpirationMonth());
				}
				else
				{
					throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Card Expiration Month is null or empty","HIGH");
				}
				
				if(StringUtils.isNotEmpty(paymentMethodDTO.getExpirationYear()))
				{
					requestMap.put(CyberSourceKeyConstants.CARD_EXPIRATIONYEAR_KEY, paymentMethodDTO.getExpirationYear());
				}
				else
				{
					throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception Card Expiration Year is null or empty","HIGH");
				}				
			}
			
		}
		else
		{
			throw new GIException(FinanceConstants.ERROR_CODE_201,"Exception received paymentMethod object for createCustomerProfileRequest is null","HIGH");
		}
		return requestMap;
	}	
}
