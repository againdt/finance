package com.getinsured.hix.finance.issuer.service.jpa;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.finance.issuer.repository.IIssuerRemittanceLineItemRepository;
import com.getinsured.hix.finance.issuer.service.IssuerRemittanceLineItemService;
import com.getinsured.hix.finance.utils.FinancialMgmtGenericUtil;
import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.IssuerRemittance;
import com.getinsured.hix.model.IssuerRemittanceLineItem;

/**
 * This service implementation class is for GHIX-FINANCE module for issuer invoices line items.
 * This class has various methods to create, modify and query the IssuerInvoicesLineItems model object. 
 * @author sharma_k
 * @since 28-Jun-2013
 */
@Service("issuerRemittanceLineItemService")
public class IssuerRemittanceLineItemServiceImpl implements IssuerRemittanceLineItemService
{
	@Autowired private IIssuerRemittanceLineItemRepository iIssuerRemittanceLineItemRepository;

	/**
	 * Method saveIssuerInvoicesLineItems.
	 * @param issuerInvoicesLineItems IssuerInvoicesLineItems
	 * @return IssuerInvoicesLineItems
	 * @see com.getinsured.hix.finance.issuer.service.IssuerRemittanceLineItemService#saveIssuerRemittanceLineItems(IssuerInvoicesLineItems)
	 */
	@Override
	public IssuerRemittanceLineItem saveIssuerRemittanceLineItems(IssuerRemittanceLineItem issuerRemittanceLineItem)
	{
		return iIssuerRemittanceLineItemRepository.save(issuerRemittanceLineItem);
	}

	/**
	 * Method getIssuerInvoicesLineItems.
	 * @param issuerInvoices IssuerInvoices
	 * @return List<IssuerInvoicesLineItems>
	 * @see com.getinsured.hix.finance.issuer.service.IssuerRemittanceLineItemService#getIssuerRemittanceLineItems(IssuerInvoices)
	 */
	@Override
	public List<IssuerRemittanceLineItem> getIssuerRemittanceLineItems(IssuerRemittance issuerRemittance)
	{
		return iIssuerRemittanceLineItemRepository.findByIssuerRemittance(issuerRemittance);
	}

	/**
	 * Method createIssuerInvoiceLineItemsfromEmployerInvoiceLineItems.
	 * @param employerInvoiceLineItems EmployerInvoiceLineItems
	 * @return IssuerInvoicesLineItems
	 * @see com.getinsured.hix.finance.issuer.service.IssuerRemittanceLineItemService#createIssuerRemittanceLineItemsfromEmployerInvoiceLineItems(EmployerInvoiceLineItems)
	 */
	@Override
	public IssuerRemittanceLineItem createIssuerRemittanceLineItemsfromEmployerInvoiceLineItems(EmployerInvoiceLineItems employerInvoiceLineItems)
	{
		IssuerRemittanceLineItem issuerInvoicesLineItems = new IssuerRemittanceLineItem();
		issuerInvoicesLineItems.setEmployer(employerInvoiceLineItems.getEmployer());
		issuerInvoicesLineItems.setTotalPremium(employerInvoiceLineItems.getTotalPremium());
		//	issuerInvoicesLineItems.setNetAmount(employerInvoiceLineItems.getNetAmount());
		issuerInvoicesLineItems.setNetAmount(employerInvoiceLineItems.getPaymentReceived()); // it was added by Harinath : considered for partial or full payment.
		issuerInvoicesLineItems.setRetroAdjustments(employerInvoiceLineItems.getRetroAdjustments());
		// Below two lines added by Harinath 
		issuerInvoicesLineItems.setAmountReceivedFromEmployer(FinancialMgmtGenericUtil.getDefaultBigDecimal());
		issuerInvoicesLineItems.setAmtPaidToIssuer(FinancialMgmtGenericUtil.getDefaultBigDecimal());
		//New changes added by Kuldeep after userfee calculations
		issuerInvoicesLineItems.setEmployerInvoicesLineItems(employerInvoiceLineItems);
		//Changes done by Kuldeep to add the enrollment reference details.
		issuerInvoicesLineItems.setEnrollmentId(employerInvoiceLineItems.getEnrollmentId());
		issuerInvoicesLineItems.setUserFee(BigDecimal.ZERO);
		return issuerInvoicesLineItems;
	}

	/**
	 * Method updateAmtPaidFrmEmpToIssuer.
	 * @param employerInvoiceLineItems EmployerInvoiceLineItems
	 * @return IssuerInvoicesLineItems
	 * @see com.getinsured.hix.finance.issuer.service.IssuerRemittanceLineItemService#updateAmtPaidFrmEmpToIssuer(EmployerInvoiceLineItems)
	 */
	@Override
	public IssuerRemittanceLineItem updateAmtPaidFrmEmpToIssuer(EmployerInvoiceLineItems employerInvoiceLineItems, IssuerRemittance issuerRemittance)
	{
		IssuerRemittanceLineItem issuerRemittanceLineItem = null; 
		issuerRemittanceLineItem = iIssuerRemittanceLineItemRepository.findByIssuerRemittanceAndEmployerInvoiceLineItems(issuerRemittance, employerInvoiceLineItems);
		if(issuerRemittanceLineItem != null)
		{
			BigDecimal paymentReceived = employerInvoiceLineItems.getPaymentReceived();
			paymentReceived = (paymentReceived!=null) ? paymentReceived : FinancialMgmtGenericUtil.getDefaultBigDecimal();
			issuerRemittanceLineItem.setAmountReceivedFromEmployer(paymentReceived);
			BigDecimal amountPaidToIssuer = FinancialMgmtGenericUtil.divide(
					FinancialMgmtGenericUtil.multiply(paymentReceived,
							issuerRemittanceLineItem.getNetAmount()),
							FinancialMgmtGenericUtil.add(
									issuerRemittanceLineItem.getNetAmount(),
									issuerRemittanceLineItem.getUserFee()));
			issuerRemittanceLineItem.setAmtPaidToIssuer(amountPaidToIssuer);
			//Once saved return this as updated object
			return iIssuerRemittanceLineItemRepository.save(issuerRemittanceLineItem);
		}
		//If found null then return this as null
		return issuerRemittanceLineItem;
	}

	@Override
	public List<IssuerRemittanceLineItem> getIssLineItemsByPaidEmpLineItems(int invoiceId, int issuerId)
	{
		return iIssuerRemittanceLineItemRepository.getIssLineItemsByPaidEmpLineItems(invoiceId, issuerId);
	}

	@Override
	public List<IssuerRemittanceLineItem> getPaidIssuerLineItems(int invoiceId)
	{
		return iIssuerRemittanceLineItemRepository.getPaidIssuerLineItems(invoiceId);
	}

}
