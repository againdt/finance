package com.getinsured.hix.finance.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;



/**
 * Class for static constants string key used in Finance Module. 
 * @author Sharma_k
 * @since 24th June, 2013
 */

//@SuppressWarnings("static-access")
@Component
public final class FinanceConstants 
{
	public static final String EMPLOYERID 					= "EMPLOYERID";
	public static final String INVOICEID 					= "INVOICEID";
	public static final String ID_KEY 						= "ID";
	public static final String PAIDAMT_KEY 					= "PAIDAMT";
	public static final String PAYMENT_TYPE_KEY 			= "PAYMENTTYPE";
	public static final String IS_ACTIVE_KEY 				= "ISACTIVE";
	public static final String PAYMENTDUEDATE_KEY 			= "PAYMENTDUEDATE";
	public static final String DECISION_Y 					= "Y";
	public static final String DECISION_N 					= "N";
	public static final long EFTTRACE_NO_SERIES 			= 100000000L;
	public static final String EXCG_PAYMENT_TYPE_PREM 		= "PREM";
	public static final String EXCG_PAYMENT_TYPE_BAL 		= "BAL";
	public static final String CYBERSOURCE_ACCEPT 			= "ACCEPT";
	public static final String SUPPRESS_WARNING_UNCHECKED 	= "unchecked";
	public static final String SUPPRESS_WARNING_RAWTYPES 	= "rawtypes";
	public static final String EXCEPTION_SEVERITY_HIGH 		= "HIGH";
	public static final String EXCEPTION_SEVERITY_WARN 		= "WARN";
	public static final String ST01_EDI20 					= "820";
	public static final String EDI820_PAYERNAME_SBE 		= "SBE";
	public static final String EDI820_FILE_NAME_POSTFIX 	= "_820_SHOP";
	public static final String ECM_DOC_ID                   = "ecmDocId";
	public static final String ID_SMALL_CASE                = "id";
	public static final String EMPLOYER_DOT_ID              = "employer.id";
	public static final String ADDRESS1                     = "addressLine1";
	public static final String ADDRESS2                     = "addressLine2";
	public static final String CONTACT_CITY                         = "contactCity";
	public static final String CONTACT_STATE                        = "contactState";
	public static final String CONTACT_ZIPCODE                      = "contactZip";
	public static final String CONTACT_ADDRESS              = "contactAddress";
	public static final String DUEMONTH                     = "dueMonth";
	public static final String EFFECTIVEDATE                = "effectiveDate";
	public static final String GRACEDATE                    = "graceDate";
	public static final String EXCHANGE_PHONE             = "exchangePhone";
	public static final String EMPLOYER_INVOICES            = "employerInvoices";
	public static final String DUE_AMOUNT                   = "dueAmount";
	public static final String SYSTEM_DATE                  = "systemDate";
	public static final String EMPLOYER_BUSINESS_NAME       = "EmployerBusinessName";
	public static final String ISS                           = "ISS";
	public static final String EMP                           = "EMP";
	public static final String LINE_DOT_SEPARATOR            = "line.separator";
	public static final String Y                             = "Y";
	public static final String N                             = "N";
	public static final String PAYMENT_TYPE_LOWERCASE_KEY  	 = "paymentType";
	public static final String IS_DEFAULT_KEY 				 = "isDefault";
	public static final String PAYMENT_METHOD_NAME_KEY 		 = "paymentMethodName";
	public static final String MODULEID_KEY					 = "moduleId";
	public static final String MODULENAME_KEY 				 = "moduleName";
	public static final String SUBSCRIPTIONID_KEY 			 = "subscriptionId";
	public static final String UPDATEDON_KEY				 = "updatedOn";
	public static final String CONTACT_EMAIL                 = "contactEmail";
	public static final String EMPTY                         = "";
	public static final String EMPLOYER_CONTACT_FULL_NAME      = "employerContactFullName";
	public static final String FILENAME_KEY 				 = "fileName";
	public static final String AGENT_NOTIFICATION_FILENAME   = "Notice-EmployerInvoiceDueEmailNoticeToAgent-";
	public static final String BROKER_FIRSTNAME_KEY 		 = "brokerFirstName";
	public static final String BROKER_LASTNAME_KEY			 = "brokerLastName";
	public static final String TOEMAIL_LIST_KEY 			 = "toEmailList";
	public static final String TO_FULLNAME_KEY				 = "toFullName";
	public static final String TOTAL_COUNT_KEY				 = "totalCount";
	public static final String TEMPLATENAME_KEY				 = "templateName";
	public static final String NOTIFICATION_AGENTEMPINVOICEDUE  = "AgentEmpInvoiceDueNotification";
	public static final String CONTACT_NUMBER_KEY 				= "contactnumber";
	public static final String IS_PAYMENT_GATEWAY_KEY		= "isPaymentGateway";
	public static final String COVERAGE_START_DATE_KEY 		= "COVERAGE_START_DATE";
	public static final String MODULE_NAME                   = "ModuleName";
	public static final String MODULE_ID                     = "ModuleID";
	public static final String BINDER_DUE_DATE               = "binderDueDate";
	public static final String EMPLOYEE_NAME               = "employeeName";
	

	public static final int ISSUER_INVOICE_GLCODE 			= 2000;
	public static final int EMPLOYER_INVOICE_GLCODE 		= 1100;
	public static final String NSF_FEE_COMMENT_PDF 			= "Exchange Fees: Includes NSF charges of $NSFFlatFee due to failed payment.";
	public static final String EMPLOYER_FEE_COMMENT_PDF		= "Exchange Fees: Includes SHOP Employer Fees at $EMPLOYER_FEE_AMOUNT per enrollment, as well as, any retro-active charges or refunds.";
	
	public static final String NSF_FEE_COMMENT_EMAIL 		= "NOTE: Because your account had insufficient funds to process the payment, the amount due on your invoice will include an extra NSF fee of $NSF_FLAT_FEE_AMOUNT.";
	
	
	
	
	 public static final String ERROR_NULL_RESPONSE 			= "No response received based on given search criteria";
	 public static final String ERROR_NULL_REQUEST 				= "Invalid input: Search criteria is null or empty";
	 public static final String ERR_MSG_IS_ACTIVE_KEY_NULL 		= "Invalid input: parameter is_active_key is null or empty";
	 public static final String ERR_MSG_PAYMENT_DUE_DATE_NULL 	= "Invalid input: parameter payment due date is null or empty";
	 public static final String ERR_MSG_EMPLOYER_ID_NULL 		= "Invalid input: parameter employer_id is null or empty";
	 public static final String ERR_MSG_INVOICE_ID_NULL 		= "Invalid input: parameter invoice_id is null or empty";
	 public static final String ERR_MSG_ID_CANNOT_BE_ZERO 		= "Invalid input: Id cannot be zero";
	 public static final String ERR_KEY_NOT_FOUND 				= "KeyNotFound";
	 public static final String ERR_MSG_CYBERSOURCE_REPLY_NULL 	= "PCI: Cybersource returned null or empty response";
	 public static final String ERR_MSG_NULL_EMPTY_RESPONSE  	= "Received Null or Empty response from: ";
	 public static final String ERR_MSG_WHILE_SENDING_EMPLOYER_NOTIFICATION = "Error occurred while sending the employer notification ";
	 public static final String ERR_MSG_WHILE_SENDING_EMPLOYEE_NOTIFICATION = "Error occurred while sending the employee notification ";
	 
	 public static final int SUCCESS_CODE_200 				= 200;
	 public static final int ERROR_CODE_201 				= 201;
	 public static final int ERROR_CODE_202 				= 202;
	 public static final int ERROR_CODE_203 				= 203;
	 public static final int ERROR_CODE_204 				= 204;
	 public static final int ERROR_CODE_205 				= 205;
	 public static final int ERROR_CODE_206 				= 206;
	 public static final int ERROR_CODE_207 				= 207;
	 public static final int ERROR_CODE_208 				= 208;
	 public static final int ERROR_CODE_209 				= 209;
	 public static final int ERROR_CODE_210 				= 210;
	 
	 public static final String ERR_MSG_ISSUER_ID_NOT_EXISTS 		= "Issuer does not exist with hios_issuer_id: ";
	 public static final String ERR_MSG_ISSUER_NAME_NULL 			= "Invalid Input: Issuer_Name is null or empty";
	 public static final String ERR_MSG_EMPLOYER_CASE_ID_NULL 		= "Invalid Input: Employer_Case_ID is null or empty";
	 public static final String ERR_MSG_ENROLLMENT_ID_NULL 			= "Invalid Input: Enrollment_ID is null or empty";
	 public static final String ERR_MSG_EMPLOYEE_CASE_ID 			= "Invalid Input: Employee_Case_ID is null or empty";
	 public static final String ERR_MSG_GROSS_PREMIUM_NULL 			= "Invalid Input: Gross_Premium is null or empty";
	 public static final String ERR_MSG_PREMIUM_START_DATE 			= "Invalid Input: Premium Applied Start Date is not appropriate: ";
	 public static final String ERR_MSG_PREMIUM_END_DATE 			= "Invalid Input: Premium Applied End Date is not appropriate: ";
	 public static final String ERR_MSG_EMPLOYEE_LIST_NULL 			= "Invalid Input: Employee List is null or empty";
	 
	 public static final String ERR_MSG_ENROLLMENT_NOT_EXISTS 		= "Enrollment does not exist with enrollment_id: ";
	 public static final String ERR_MSG_EMPLOYER_NOT_EXISTS 		= "Employer does not exist with external_id: ";
	 
	 public static final String CURRENT_DATE 								= "currentDate";
	 public static final String LAST_INVOICE_DATE 							= "lastInvoiceDate";
	 public static final String STATUS 										= "status";
	 public static final String EMPLOYER_ID 								= "employerid";
	 public static final String EMPLOYER_ENROLLMENT_ID						= "employerEnrollmentId";
	 public static final String EMP_ENRL_DATE_FORMAT                        = "yyyyMMdd";
	 public static final String DATE_FORMAT_MM_DD_YYYY                      = "MM-dd-yyyy";
	 public static final String DATE_FORMAT_MMMMM_DD_YYYY                   = "MMMMM dd, yyyy";
	 public static final String DATE_FORMAT_MM_DD_YY                   		= "MM/dd/yy";
	 public static final String DATE_FORMAT_DD_MM_YY                   		= "dd/MM/yy";
	 public static final String DATE_FORMAT_MMDDYYYY                   		= "MMddyyyy";
	 public static final String CREATEDON_TIME_FORMAT                       = "dd-MMM-yyyy HH.mm.ss.S";
	 public static final String REINSTIATION_TIME_FORMAT                    = "MM/dd/yyyy hh:mm:ss.S";
	 public static final String LAST_INVOICE_TIMESTAMP                      = "lastInvoiceTimestamp";
	 public static final String DATE_FORMAT_MMM								= "MMM";
	 	
	 public static final String START_DATE                                    = "startDate";
	 public static final String END_DATE                                      = "endDate";
	 public static final String CARRIER_NAME                                  = "carrierName";
	 public static final String EMPLOYER_NAME                                 = "employerName";
	 public static final String BROKER_F_NAME									="brokerFName";
	 public static final String BROKER_L_NAME									="brokerLName";
	 public static final String INVOICE_NUMBER                                = "invoiceNumber";
	 public static final String CREATION_TIMESTAMP                            = "createdOn";
	 public static final String MERCHANT_ID                                   = "merchantId";
	 public static final String SORT_BY                                       = "sortBy";
	 public static final String SORT_ORDER                                    = "sortOrder";
	 public static final String NO_OF_DAYS_PENDING							  = "noOfDaysPending"; 
	 public static final String CRITERIA									  = "criteria";
	 public static final String TRANSACTION_STATUS							  = "transactionStatus";
	 
	 public static final String EMPTY_REQUEST                          		  = "Empty Request or Search Criteria is null";
	 public static final String ERROR_MSG                              		  = "Error occurred while processing request";
	 public static final String FAILURE                                		  = "FAILURE";
	 public static final String SUCCESS                                		  = "SUCCESS";
	
	 public static final String TRUE										  = "TRUE";
	 public static final String FALSE										  = "FALSE";
	 
	 public static final String IS_ELECTRONIC_PAYMENT_ONLY                    = "isElectronicPaymentOnly";
	 
	 public static final int NUMERICAL_0 				= 0;
	 public static final int NUMERICAL_1 				= 1;
	 public static final int NUMERICAL_2 				 = 2;
	 public static final int NUMERICAL_3 				 = 3;
	 public static final int NUMERICAL_4 				 = 4;
	 public static final int NUMERICAL_5 				 = 5;
	 public static final int NUMERICAL_6                 = 6;
	 public static final int NUMERICAL_7 				 = 7;
	 public static final int NUMERICAL_8 				 = 8;
	 public static final int NUMERICAL_9 				 = 9;
	 public static final int NUMERICAL_10 				 = 10;
	 public static final int NUMERICAL_12 				 = 12;
	 public static final int NUMERICAL_14 				 = 14;
	 public static final int NUMERICAL_15 				 = 15;
	 public static final int NUMERICAL_20                = 20;
	 public static final int NUMERICAL_23 				 = 23;
	 public static final int NUMERICAL_24 				 = 24;
	 public static final int NUMERICAL_30                = 30;
	 
	 public static final int NUMERICAL_NEG_1 			 = -1;
	 public static final int NUMERICAL_NEG_2 			 = -2;
	 
	 public static final int NUMERICAL_59 				 = 59;
	 public static final int NUMERICAL_60 				= 60;
	 public static final int NUMERICAL_100 				= 100;
	 public static final int NUMERICAL_102 				= 102;
	 public static final int NUMERICAL_999 				= 999;
	 public static final int NUMERICAL_1000 			= 1000;
	 
	
	 
	
	 
	 public static final int NUMERICAL_20000 			 = 20000;
	 public static final int NUMERICAL_20001 			 = 20001;
	 public static final int NUMERICAL_20002 			 = 20002;
	 public static final int NUMERICAL_20003 			 = 20003;
	 public static final int NUMERICAL_20004 			 = 20004;
	 public static final int NUMERICAL_20005 			 = 20005;
	 public static final int NUMERICAL_20006 			 = 20006;
	 public static final int NUMERICAL_20007 			 = 20007;
	 public static final int NUMERICAL_20008 			 = 20008;
	 public static final int NUMERICAL_20009			 = 20009;
	 public static final int NUMERICAL_20010 			 = 20010;
	 public static final int NUMERICAL_20011 			 = 20011;
	 public static final int NUMERICAL_20012			 = 20012;
	 public static final int NUMERICAL_20020 			 = 20020;
	 public static final int NUMERICAL_20021 			 = 20021;
	 public static final int NUMERICAL_20022 			 = 20022;
	 public static final int NUMERICAL_20023 			 = 20023;
	 public static final int NUMERICAL_20024 			 = 20024;
	 public static final int NUMERICAL_20025 			 = 20025;
	 public static final int NUMERICAL_20026			 = 20026;
	 public static final int NUMERICAL_20027 			 = 20027;
	 public static final int NUMERICAL_20028 			 = 20028;
	 public static final int NUMERICAL_20029 			 = 20029;
	 
	 public static final String YES                                           = "YES";
	 public static final String NO                                            = "NO";
	 public static final String PAYMENT_RECEIVED                              = "PAYMENT_RECEIVED";
	 public static final String ORDER_CONFIRMED                               = "ORDER_CONFIRMED";
	 public static final String CONFIRM                                       = "CONFIRM";
	 public static final String PENDING                                       = "PENDING";
	 public static final String TERM                                          = "TERM";
	 public static final String CANCEL										  = "CANCEL";
	 
	 public static final String BINDER_INVOICE								  ="BINDER_INVOICE";
	 public static final String NORMAL_INVOICE								  ="NORMAL";
	 
	 public static final String EVENT_TYPE_COMPLETED						  ="COMPLETED";
	 public static final String EVENT_TYPE_DECLINED						  	  ="DECLINED";
	 public static final String EVENT_TYPE_ERROR						  	  ="ERROR";
	 public static final String EVENT_TYPE_FAILED						  	  ="FAILED";
	 public static final String EVENT_TYPE_OTHER						  	  ="OTHER";
	 public static final String EVENT_TYPE_VOID						  	      ="VOID";
	 public static final String EVENT_TYPE_STOP_PAYMENT				  	      ="STOP PAYMENT";
	 public static final String EVENT_TYPE_NSF				  	      		  ="NSF";
	 public static final String EVENT_TYPE_FINAL_NSF				  	      ="FINAL NSF";
	 public static final String EVENT_TYPE_FIRST_NSF				  	      ="FIRST NSF";
	 public static final String EVENT_TYPE_SECOND_NSF				  	      ="SECOND NSF";
	 
	 public static final String TERMINATION_REASON_CODE				  	      ="59";
	 public static final String NEW											  ="New";
	 public static final String OLD											  ="Old";
	 
	 public static final String ADD = "ADD";
	 public static final String UPDATE = "UPDATE";
	 /**
	  * Adding constants for InVoluntary Termination
	  */
	 public static final String INVOLUNTARY_TERMINATION 						= "18";
	 public static final String TERMINATON_REASON_STRING						= "NON_PAYMENT";
	 public static final String EMPLOYER_ID_KEY									= "EMPLOYER_ID";
	 public static final String TERMINATION_DATE_KEY							= "TERMINATION_DATE";
	 public static final String TERMINATION_REASON_KEY 							= "TERMINATION_REASON";
	 public static final String TERMINATION_CODE_KEY 							= "TERMINATION_CODE";
	 public static final String PAID_INVOICE_CREATION_DATE 						= "LAST_PAID_INVOICE_DATE";
	 public static final String ENROLLMENT_LIST 							    = "ENROLLMENT_ID_LIST";
	 
	 public static final String ENCODING_UTF_8 										= "UTF-8";
	 public static final String SINGLE_TXN_DOWNLOD_REPORT_VERSIONNUMBER 			= "1.5";
	 public static final String SINGLE_TXN_DOWNLOD_REPORT_TYPE 						= "transaction";
	 public static final String SINGLE_TXN_DOWNLOD_REPORT_SUBTYPE 					= "transactionDetail";
	 public static final String DEFAULT_PAYMENTEVENT_FILENAME						= "PaymentEventReport.xml";
	 public static final String DEFAULT_SINGLE_PYMTTXN_REPORT 						= "SinglePaymentTxnReport.xml";
	 
	 public static final String TRANSACTION_TYPE_CREDIT						= "Credit";
	 public static final String TRANSACTION_TYPE_DEBIT						= "Debit";
	 public static final String PAYMENT_TYPE_EXCESS							= "excess";
	 
	 public static final String OPEN_ENROLLMENT_OVER_KEY 					= "OPEN_ENROLLMENT_OVER";
	 public static final String EMPLOYER_ENROLLMENT_ID_KEY 					="EMPLOYER_ENROLLMENT_ID";
	 public static final String CURRENT_MAP_KEY                             = "Current";
	 
	 public static final String EVENT_TYPE_REINSTATEMENT="025";
	 public static final String EVENT_REASON_REENROLLMENT = "41";
	 public static final int SHOP_ERROR_CODE_PAYMENT_UPDATE = 105;
	 public static final int ENROLLMENT_ERROR_CODE_PAYMENT_UPDATE = 204;
	 public static final int EMPLOYER_ENROLLMENT_NOT_PRESENT = 104;
	 
	 public static final String LINE_ITEM_LIST = "LINE_ITEM_LIST";
	 public static final String EMPLOYER_FEE_COUNT = "EMPLOYER_FEE_COUNT";
	 /*
	  * Cybersource request map related constants
	  */
	 public static final String CYBERSOURCE_ECCREDIT_SERVICE_DEBIT_REQUESTID     = "ecCreditService_debitRequestID";
	 
	 
	 /**
	  * cybersource response maps related constants.
	  *
	  */
	 public static final class CyberSourceResponseMapKeys
	 {
		 public static final String CYBERSOURCE_REQUESTID 					= "requestID";
		 public static final String CYBERSOURCE_REASONCODE 					= "reasonCode";
		 public static final String CYBERSOURCE_REQUESTTOKEN				= "requestToken";
		 public static final String	CYBERSOURCE_MERCHANTREFERENCECODE		= "merchantReferenceCode";
		 public static final String CYBERSOURCE_DECISION 					= "decision";
		 public static final String CYBERSOURCE_ECDEBIT_REASONCODE 			="ecDebitReply_reasonCode";
		 public static final String CYBERSOURCE_ECDEBIT_SETTLEMENTMETHOD 	="ecDebitReply_settlementMethod";
		 public static final String CYBERSOURCE_ECDEBIT_REQUESTDATETIME 	="ecDebitReply_requestDateTime";
		 public static final String CYBERSOURCE_ECDEBIT_AMOUNT				="ecDebitReply_amount";
		 public static final String CYBERSOURCE_ECDEBIT_VERIFICATIONLEVEL	="ecDebitReply_verificationLevel";
		 public static final String CYBERSOURCE_ECDEBIT_RECONCILIATIONID	="ecDebitReply_reconciliationID";
		 public static final String CYBERSOURCE_ECDEBIT_PROCESSORRESPONSE	="ecDebitReply_processorResponse";
		 public static final String CYBERSOURCE_ECCREDIT_RECONCILIATIONID	="ecCreditReply_reconciliationID";
		 
		 /**
		  * Adding reconciliationID key for Credit Card Payment.
		  */
		 public static final String CYBERSOURCE_CCCREDITREPLY_RECONCILIATIONID = "ccCreditReply_reconciliationID";
		 public static final String CYBERSOURCE_CCCAPTUREREPLY_RECONCILIATIONID = "ccCaptureReply_reconciliationID";
		 
		 public static final String CYBERSOURCE_ECDEBIT_PROCESSOR_TRANSACTION_ID	="ecDebitReply_processorTransactionID";
		 public static final String CYBERSOURCE_ECCREDIT_PROCESSOR_TRANSACTION_ID	= "ecCreditReply_processorTransactionID";
		 
		 public static final String CYBERSOURCE_INVALID_FIELD_0 			= "invalidField_0";
		 
		 private CyberSourceResponseMapKeys() {
			 
		 }		 
	 }
	 
	 
	 /**
	  * Payment Event Report of Echeck related constants
	  *
	  */
	 public static final class PaymentEventReport
	 {
		 public static final String MERCHANTID_KEY 							= "merchantID";
		 public static final String USERNAME_KEY 							= "username";
		 public static final String PASSWORD_KEY 							= "password";
		 public static final String STARTDATE_KEY 							= "startDate";
		 public static final String STARTTIME_KEY 							= "startTime";
		 public static final String ENDDATE_KEY 							= "endDate";
		 public static final String ENDTIME_KEY 							= "endTime";
		 public static final String FORMAT_KEY 								= "format";
		 
		 public static final String REPORT_FORMAT 							= "xml";
		 public static final String REPORT_CONTENTTYPE 						= "application/xml"; 
		 
		 public static final String TXN_STATUS = "txnStatus";
		 public static final String REQUEST_ID = "requestId";
		 public static final String MERCHANT_REF_CODE = "merchantRefCode";
		 public static final String ISSUER_INVOICES_ID = "issuerInvoices";
		 public static final String EMPLOYER_INVOICES_ID = "employerInvoices";
		 
		 private PaymentEventReport() {
			 
		 }
	 }
	 
	 /**
	  * Email Notification related constants
	  *
	  */
	 public static final class EmailNotificationConstant
	 {
		 public static final String MAIL_STATUS 							= "Sent";
		 public static final String MAIL_STATUS_COMMENT 					= "main sent";
		 public static final String EMPLOYER_PASSED_DUE_DATE				= "EmployerPassedDueDateNotice";
		 public static final String EMPLOYER_PASSED_MIDDLE_DUE_DATE			= "EmployerPassedMiddleDueDateNotice";
		 public static final String EMPLOYER_PASSED_END_DUE_DATE			= "EmployerPassedEndDueDateNotice";
		 
		 public static final String EMPLOYER_BINDER_INVOICE_DUE				= "EmployerBinderInvoiceDueNotification";
		 public static final String EMPLOYER_BINDER_INVOICE_READY			= "EmployerBinderInvoiceReadyNotification";
		 public static final String EMPLOYER_INVOICE_READY					= "EmployerInvoiceReadyNotification";
		 
		 public static final String PAYMENT_FAILURE_NOTICE					= "PaymentFailureNotification";
		 public static final String EMPLOYER_INVOICES						= "EmployerInvoices";
		 
		 public static final String BROKER_PASSED_DUE_DATE					= "BrokerPassedDueDateNotice";
		 public static final String BROKER_PASSED_MIDDLE_DUE_DATE			= "BrokerPassedMiddleDueDateNotice";
		 public static final String BROKER_PASSED_END_DUE_DATE				= "BrokerPassedEndDueDateNotice";
		 
		 public static final String BROKER_BINDER_INVOICE_DUE				= "BrokerBinderInvoiceDueNotification";
		 public static final String EMPLOYEE_INVOICE_DUE_EMAIL_NOTICE       = "EmployeeInvoiceDueEmailNotification";
		 public static final String EMPLOYER_AUTOPAY_APPROACHING_NOTICE     = "EmployerInvoiceAutoPaymentApproachingNotice";
		 public static final String EMPLOYER_AUTOPAY_MADE_NOTICE            = "EmployerAutoPaymentProcessMadeNotice";
		 
		 public static final String EMPLOYER_REMINDER_NOTICE            = "EmployerPaymentRemainder";
		 public static final String GROUP_GOOD_STANDINGNOTICE            = "EmployeeGroupPaymentInGoodStanding";
		 
		 private EmailNotificationConstant() {
			 
		 }
	 }
	 
	 public static final class SingleTxnReportDownloadConstant
	 {
		public static final int INVALID_URL_ERRORCODE = 201;
		public static final String INVALID_URL_ERRORMSG = "Invalid or empty URL received";
		
		private SingleTxnReportDownloadConstant() 	{
			
		}
	 }
	 
	 public static final class PaymentEventLogs
	 {
		 public static final String MERCHANT_REF_NUMBER_KEY = "MERCHANT_REFERENCE_NUMBER";
		 public static final String REQUEST_ID_KEY = "REQUEST_ID";
		 public static final String EVENT_TYPE_KEY = "EVENT_TYPE";
		 public static final String TRANSACTION_ID_KEY = "TRANSACTION_ID";
		 public static final String ECM_DOC_ID_KEY = "ECM_DOC_ID";
		 public static final String PAYMENT_STATUS = "PAYMENT_STATUS";
		 
		 private PaymentEventLogs()	 {
			 
		 }		
	 }
	 
	 /**
	  * Payment Redirect SAML Assertion related constants
	  */
	 public static final class SAMLConstants
	 {
		 public static final String RANDOM_SESSION_ID = "abcdedf1234567";
		 public static final String SAML_SUBJECT_CONFIRMATION = "urn:oasis:names:tc:SAML:2.0:cm:bearer";
		 public static final String SAML_AUTHN_CONTEXT_CLASSREF = "urn:oasis:names:tc:SAML:2.0:ac:classes:Password";
		 public static final String SAML_RESPONSE_STATUS = "urn:oasis:names:tc:SAML:2.0:status:Success";
		 public static final String SAML_RESPONSE_SUCCESS = "Success";
		 public static final String GI_SAML_ASSERTION = "GISamlAssertion-";
		 public static final String GI_SAML_RESPONSE = "GIResponse-";
		 public static final String ASSERTION_LVL_NSPACE = "http://www.w3.org/2001/XMLSchema-instance";
		 
		 public static final String PAYREDIRECT_KEYSTORE_LOC = "ffmKeystoreFileLocation";
		 public static final String PAYREDIRECT_PRIVATE_KEY = "ffmPrivateKeyName";
		 public static final String PAYREDIRECT_PASSWORD = "ffmPassword";
		 public static final String PAYREDIRECT_SECUREKEY_PASSWORD = "ffmPassword_securekey";
		 public static final String ISSUER_NAME = "issuer_name";
		 public static final String SECURITY_DNS_NAME = "security_dns_name";
		 public static final String SECURITY_ADDRESS = "security_address";
		 public static final String SECURITY_KEY_INFO  = "security_keyinfo";
		 public static final String ISSUER_AUTH_URL = "issuer_auth_url";
		 
		 private SAMLConstants() {
			 
		 }
	 }
	 
	 public static final class ACHPaymentConstants
	 {
		 public static final String COMPANY_ID = "COMPANY_ID";
		 public static final String COMPANY_NAME = "COMPANY_NAME";
		 public static final String EFFECTIVE_ENTRY_DATE = "EFFECTIVE_ENTRY_DATE";
		 public static final String ENTRY_DESCRIPTION = "ENTRY_DESCRIPTION";
		 public static final String SERVICE_CLASS_CODE = "SERVICE_CLASS_CODE";
		 public static final String STANDARD_CLASS_CODE = "STANDARD_CLASS_CODE";
		 public static final String INDIVIDUAL_INFO = "INDIVIDUAL_INFO";
		 
		 private ACHPaymentConstants() {
			 
		 }
	 }
	 
	 public static final class InvoiceConstants
	 {
		 public static final String IS_ACTIVE = "isActive";
		 public static final String PAID_STATUS = "paidStatus";
		 public static final String INVOICE_TYPE = "invoiceType";
		 public static final String EMPLOYER_ID = "employerId";
		 
		 private InvoiceConstants() {
			 
		 }
	 }
	 
	private static String cybersourceReportAccUserName;
	private static String cybersourceReportAccPassword;
	
	
	/**
	 * @return the cybersourceReportAccUserName
	 */
	public static String getCybersourceReportAccUserName() {
		return cybersourceReportAccUserName;
	}

	/**
	 * @return the cybersourceReportAccPassword
	 */
	public static String getCybersourceReportAccPassword() {
		return cybersourceReportAccPassword;
	}

	
	
	@Value("#{configProp['cybersourceReportAccUserName']}")
	public void setCybersourceReportAccUserName(String cybersourceReportAccUserName)
	{
		this.cybersourceReportAccUserName = cybersourceReportAccUserName;
	}
	
	@Value("#{configProp['cybersourceReportAccPassword']}")
	public void setCybersourceReportAccPassword(String cybersourceReportAccPassword)
	{
		this.cybersourceReportAccPassword = cybersourceReportAccPassword;
	}
}
