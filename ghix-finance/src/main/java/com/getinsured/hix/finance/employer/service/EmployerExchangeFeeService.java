package com.getinsured.hix.finance.employer.service;

import java.util.List;

import com.getinsured.hix.model.EmployerExchangeFees;

/**
 * 
 * @author Sharma_k
 * @since 08th January 2015
 * Interface for EmployerExchangeFees Model related CRUD operations. 
 */
public interface EmployerExchangeFeeService 
{
	/**
	 * 
	 * @param employerExchangeFees
	 * @return
	 */
	EmployerExchangeFees saveEmployerExchangeFees(EmployerExchangeFees employerExchangeFees);
	
	/**
	 * 
	 * @param invoiceId
	 * @return
	 */
	List<EmployerExchangeFees> findEmpExchangeFeesByInvoiceId(final int invoiceId);
	
	/**
	 * 
	 * @param employerExchangeFeesList
	 * @return
	 */
	List<EmployerExchangeFees> saveEmployerExchangeFeesList(List<EmployerExchangeFees> employerExchangeFeesList);
	
	/**
	 * 
	 * @param invoiceId
	 * @return
	 */
	List<EmployerExchangeFees> findEmpExchangeFeesByInvIdExceptEmpUserFee(final int invoiceId);

}
