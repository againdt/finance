package com.getinsured.hix.finance.employer.service.jpa;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.finance.EmployerInvoicesDTO;
import com.getinsured.hix.finance.employer.service.EmployerExchangeFeeService;
import com.getinsured.hix.finance.employer.service.EmployerInvoiceLineItemsService;
import com.getinsured.hix.finance.employer.service.EmployerInvoicePdfGenerationService;
import com.getinsured.hix.finance.employer.service.EmployerInvoicesService;
import com.getinsured.hix.finance.exception.FinanceException;
import com.getinsured.hix.finance.notification.secureinbox.FinanceNotificationService;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceConstants.EmailNotificationConstant;
import com.getinsured.hix.finance.utils.FinanceGeneralUtility;
import com.getinsured.hix.finance.utils.FinancialMgmtGenericUtil;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.EmployerExchangeFees;
import com.getinsured.hix.model.EmployerExchangeFees.ExchangeFeeTypeCode;
import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.EmployerInvoiceLineItemsDetail;
import com.getinsured.hix.model.EmployerInvoices;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;


/**
 * This service implementation class is for GHIX-FINANCE module for processing employer invoices.
 * @author sharma_k
 * @since 28-Jun-2013
 */
@Service("employerInvoicePdfGenerationService")
@Transactional
public class EmployerInvoicePdfGenerationServiceImpl implements EmployerInvoicePdfGenerationService
{

	@Autowired private EmployerInvoicesService employerInvoicesService;
	@Autowired private EmployerInvoiceLineItemsService itemsService;
	@Autowired private UserService userService;
	@Autowired private NoticeService noticeService;
	@Autowired private FinanceNotificationService financeNotificationService;
	@Autowired private EmployerExchangeFeeService employerExchangeFeeService;

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployerInvoicePdfGenerationServiceImpl.class);

	/**
	 * Method createPdf.
	 * @throws NoticeServiceException
	 * @throws FinanceException 
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicePdfGenerationService#createPdf()
	 */
	@Override
	public void createPdf(List<EmployerInvoices> listInvoice) throws FinanceException 
	{
		//final List<EmployerInvoices> listInvoice = employerInvoicesService.findEmployerInvoicesWhereInvoiceNotCreated();
		Map<String, Exception> exceptionDetails = null;
		if(listInvoice != null)
		{
			LOGGER.info("createPdf : inside listInvoice");
			for(final EmployerInvoices invoice : listInvoice)
			{
				try
				{
					createPdfByInvoiceId(invoice.getId());
				}
				catch(NoticeServiceException noticeEx)
				{
					LOGGER.error("Notice service exception: ", noticeEx);
					throw new FinanceException(noticeEx.getMessage(), noticeEx);
				}
				catch(final Exception e)
				{
					LOGGER.error("EXCEPTIONS OCCURRED WHILE GENERATING INVOICE REPORT FOR THE FOLLWING INVOICE: "+invoice.getInvoiceNumber(), e);
					if(exceptionDetails == null)
					{
						exceptionDetails = new HashMap<String, Exception>();
					}
					exceptionDetails.put(invoice.getInvoiceNumber(), e);
				}
			}
			if(exceptionDetails != null && !exceptionDetails.isEmpty())
			{
				final String exceptionMessage = FinanceGeneralUtility.constructSingleMessageForExceptionDetails("EXCEPTIONS OCCURRED WHILE GENERATING INVOICE REPORT FOR THE FOLLWING INVOICES:", "Invoice Number", exceptionDetails);
				throw new FinanceException(exceptionMessage);
			}
		}
		else
		{
			LOGGER.warn("No Invoice available for InvoicePdf generation");
		}
	}

	/**
	 * Method createPdfByInvoiceId.
	 * @param invoiceid int
	 * @throws NoticeServiceException
	 * @throws FinanceException 
	 * @see com.getinsured.hix.finance.employer.service.EmployerInvoicePdfGenerationService#createPdfByInvoiceId(int)
	 */
	@Override
	public void createPdfByInvoiceId(final int invoiceid) throws NoticeServiceException, FinanceException
	{
		final EmployerInvoices employerInvoices = employerInvoicesService.findEmployerInvoicesById(invoiceid);
		if(employerInvoices !=null)
		{
			LOGGER.info("createPdfByInvoiceId : inside empInvoices");
			List<EmployerInvoiceLineItems> employerInvLineItemsList = itemsService.getInvoiceLineItem(employerInvoices);
			String ecmDocID = createPdfByEmployerInvoiceAndLineItems(employerInvoices, employerInvLineItemsList, Boolean.TRUE);
			AccountUser user = FinanceGeneralUtility.getAccountUser(userService);
			if(StringUtils.isNotEmpty(ecmDocID))
			{
				employerInvoicesService.updateECMDocId(ecmDocID, employerInvoices.getId(),user.getId());
			}
			else
			{
				throw new FinanceException("Unable to create PDF for EmployerInvoiceID: "+invoiceid+" received Null or empty ECM_DOC_ID from service.");
			}
		}
		else
		{
			LOGGER.warn("Unable to find EmployerInvoice for InvoiceId# "+invoiceid);
		}
	}

	@Override
	public String createPdfByEmployerInvoiceAndLineItems(
			EmployerInvoices employerInvoices, List<EmployerInvoiceLineItems> employerInvLineItemsList, boolean isNotificationRequired)throws NoticeServiceException 
			{
		String response = null;
		String ecmDocID = null;
		final Map<String, Object>replaceableObject = getInvoiceData(employerInvoices, employerInvLineItemsList);

		replaceableObject.put(TemplateTokens.EXCHANGE_FULL_NAME,
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		replaceableObject.put(TemplateTokens.EXCHANGE_PHONE,
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		replaceableObject.put(TemplateTokens.EXCHANGE_URL,
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		replaceableObject.put(TemplateTokens.EXCHANGE_ADDRESS_1,
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		replaceableObject.put(TemplateTokens.CITY_NAME,
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		replaceableObject.put(TemplateTokens.PIN_CODE,
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		replaceableObject.put(TemplateTokens.STATE_NAME,
				DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
		replaceableObject.put(TemplateTokens.HOST, GhixEndPoints.GHIXWEB_SERVICE_URL);
		replaceableObject.put(FinanceConstants.IS_ELECTRONIC_PAYMENT_ONLY, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.ELECTRONIC_PAYMENT_ONLY));
		replaceableObject.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));

		final List<ModuleUser> moduleUserlist = userService.getModuleUsers(employerInvoices.getEmployer().getId(),ModuleUserService.EMPLOYER_MODULE);
		final String fileName = "Invoice-"+employerInvoices.getInvoiceNumber()+"-"+new TSDate().getTime()+".pdf";
		final ModuleUser moduleUser = moduleUserlist.get(0);
		final Notice notice = noticeService.createNoticeWOInbox(
				EmailNotificationConstant.EMPLOYER_INVOICES,
				GhixLanguage.US_EN, replaceableObject, moduleUser.getUser()
				.getId() + "/invoices", fileName, moduleUser.getUser());


		if(notice != null)
		{
			ecmDocID = notice.getEcmId();
			if(isNotificationRequired)
			{
				//this code is for notifying (sending notification) employer regarding invoice is generated and hence can see the invoice
				final Map<String, Object> noticeReplaceableObject = new HashMap<>();
				//noticeReplaceableObject.put(FinanceConstants.DUE_AMOUNT, employerInvoices.getTotalAmountDue());
				if(EmployerInvoices.InvoiceType.BINDER_INVOICE.equals(employerInvoices.getInvoiceType()))
				{
					LOGGER.info("createPdfByInvoiceId : inside BINDER_INVOICE");
					response = financeNotificationService.sendNotification(EmailNotificationConstant.EMPLOYER_BINDER_INVOICE_READY, noticeReplaceableObject, employerInvoices);
					LOGGER.info("createPdfByInvoiceId : after notificaiton for binder");
				}
				else
				{
					LOGGER.info("createPdfByInvoiceId : inside normal");
					response = financeNotificationService.sendNotification(EmailNotificationConstant.EMPLOYER_INVOICE_READY, noticeReplaceableObject, employerInvoices);
					LOGGER.info("createPdfByInvoiceId : after notificaiton for normal");
				}
				if("Fail".equals(response) || response == null)
				{
					LOGGER.error("Sending notification for the employer with invoice id:"+employerInvoices.getId() +"is failed");
				}
			}
		}
		else
		{
			throw new NoticeServiceException("failed to generate the pdf for employer invoice id "+employerInvoices.getId());
		}
		return ecmDocID;
			}


	/**
	 * Method getInvoiceData.
	 * @param invoices EmployerInvoices
	 * @return Map<String,Object>
	 */
	public Map<String, Object> getInvoiceData(final EmployerInvoices employerInvoices, final List<EmployerInvoiceLineItems> employerInvLineItemsList) 
	{
		final Map<String, Object> invoiceMap = new HashMap<String, Object>();
		Map<String, List<EmployerInvoiceLineItemsDetail>> planCarrier = null;
		final SimpleDateFormat formatter = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MM_DD_YY);	
		String paymentDate = "";
		String paymentDueDate = "";
		String spanishPaymentDueDate = "";
		String statementDate = "";
		String spanishStatementDate = "";
		String fromDate = "";
		String toDate = "";
		final String to = "to";	
		if (employerInvoices.getStatementDate() != null) {
			statementDate = formatter.format(employerInvoices.getStatementDate());
			//spanishStatementDate = newformatter.format(employerInvoices.getStatementDate());
		}
		if (employerInvoices.getPaymentDueDate() != null) {
			paymentDueDate = formatter.format(employerInvoices.getPaymentDueDate());
			//spanishPaymentDueDate = newformatter.format(employerInvoices.getPaymentDueDate());
		}
		String periodCovered = employerInvoices.getPeriodCovered();
		String spanishPeriodCovered = employerInvoices.getPeriodCovered();
		if (!StringUtils.isEmpty(periodCovered)) {
			final SimpleDateFormat format = new SimpleDateFormat(FinanceConstants.DATE_FORMAT_MM_DD_YYYY);
			final String strDate1 = periodCovered.substring(FinanceConstants.NUMERICAL_0, FinanceConstants.NUMERICAL_10);
			final String strDate2 = periodCovered.substring(FinanceConstants.NUMERICAL_14);
			try {
				final Date date1 = format.parse(strDate1);
				final Date date2 = format.parse(strDate2);
				fromDate = formatter.format(date1);
				toDate = formatter.format(date2);
				periodCovered = fromDate + " " + to + " " + toDate;
				spanishPeriodCovered = fromDate + " " + "al" + " " + toDate;
			} catch (final ParseException exception) {
				LOGGER.error("date parsing exception while generating pdf "
						+ exception);
			}
		}

		List<EmployerInvoiceLineItemsDetail> itemDetails = null;
		if(employerInvLineItemsList != null && !employerInvLineItemsList.isEmpty()) 
		{
			final Date date = employerInvLineItemsList.get(0).getPaidDate();
			paymentDate = (date == null) ? "" : formatter.format(date);
			itemDetails = getInvoiceLineItemsDetail(employerInvLineItemsList);
			planCarrier = getCarrierDetails(itemDetails);
		}

		/*	BigDecimal balanceForward = BigDecimal.ZERO;*/
		BigDecimal amountLastInvoice=BigDecimal.ZERO;
		BigDecimal lastPaymentReceived=BigDecimal.ZERO;
		final EmployerInvoices empLastPaidInvoice = employerInvoicesService
				.findLastInvoiceForReissue(employerInvoices.getEmployer()
						.getId(), employerInvoices.getId());	
		if(null != empLastPaidInvoice)
		{
			amountLastInvoice = empLastPaidInvoice.getTotalAmountDue();
			lastPaymentReceived = empLastPaidInvoice.getTotalPaymentReceived();
			/*	balanceForward = employerInvoices.getAmountDueFromLastInvoice();*/
		}

		EmployerInvoicesDTO invoiceDTO = FinanceGeneralUtility.populateInvoicesDTOFromEmployerInvoice(employerInvoices);
		invoiceDTO.setPeriodCovered(periodCovered);
		includeEmployerAddress(employerInvoices.getEmployer(), invoiceMap);

		invoiceMap.put("invoiceDTO", invoiceDTO);
		invoiceMap.put("statementDate", statementDate);
		invoiceMap.put("paymentDate", paymentDate);
		invoiceMap.put("paymentDueDate", paymentDueDate);
		invoiceMap.put("items", itemDetails);
		invoiceMap.put("carriers", planCarrier);
		invoiceMap.put("amountLastInvoice", amountLastInvoice);
		invoiceMap.put("lastPaymentReceived", lastPaymentReceived);
		invoiceMap.put("spanishPeriodCovered", spanishPeriodCovered);
		//invoiceMap.put("spanishStatementDate", spanishStatementDate);
		//invoiceMap.put("spanishDueDate", spanishPaymentDueDate);
		/*	invoiceMap.put("balanceForward", balanceForward);*/

		/*
		 * Adding Notes and Special Comments to Invoice PDF
		 */ 
		invoiceMap.put("commentsAndNotesList", populateComments(employerInvoices.getId()));
		return invoiceMap;
	}

	private Map<String, List<EmployerInvoiceLineItemsDetail>> getCarrierDetails(final List<EmployerInvoiceLineItemsDetail> itemDetails)
	{
		final Map<String, List<EmployerInvoiceLineItemsDetail>> planCarrier = new HashMap<String, List<EmployerInvoiceLineItemsDetail>>();
		final List<EmployerInvoiceLineItemsDetail> carriersList = populateCarrierBreakDown(itemDetails);

		for (final EmployerInvoiceLineItemsDetail carrier : carriersList) {
			final String carrierName = carrier.getCarrierName();
			if (planCarrier.get(carrierName) == null) {
				final List<EmployerInvoiceLineItemsDetail> tempCarrier = new ArrayList<EmployerInvoiceLineItemsDetail>();
				tempCarrier.add(carrier);
				planCarrier.put(carrierName, tempCarrier);
			} else {
				final List<EmployerInvoiceLineItemsDetail> localCarrier = planCarrier
						.get(carrierName);
				localCarrier.add(carrier);
				planCarrier.put(carrierName, localCarrier);
			}
		}
		return planCarrier;
	}
	/**
	 * Method populateCarrierBreakDown.
	 * @param empInvoiceLineItemList List<EmployerInvoiceLineItemsDetail>
	 * @return List<EmployerInvoiceLineItemsDetail>
	 */
	@SuppressWarnings("rawtypes")
	private List<EmployerInvoiceLineItemsDetail> populateCarrierBreakDown(final List<EmployerInvoiceLineItemsDetail> empInvoiceLineItemList)
	{
		final List<EmployerInvoiceLineItemsDetail> populatedEmployerInvoiceList = new ArrayList<EmployerInvoiceLineItemsDetail>();
		final Map<String, EmployerInvoiceLineItemsDetail> invoiceDetailsMap = new HashMap<String, EmployerInvoiceLineItemsDetail>();

		for(final EmployerInvoiceLineItemsDetail newemployerInvoiceLineItemsDetail : empInvoiceLineItemList)
		{
			String keyCarrerNamePlanType = newemployerInvoiceLineItemsDetail.getCarrierName()+"~"+newemployerInvoiceLineItemsDetail.getPlanType();
			if(invoiceDetailsMap != null && invoiceDetailsMap.containsKey(keyCarrerNamePlanType))
			{
				invoiceDetailsMap.get(keyCarrerNamePlanType).setNetAmount(invoiceDetailsMap.get(keyCarrerNamePlanType).getNetAmount().add(newemployerInvoiceLineItemsDetail.getNetAmount()).setScale(FinanceConstants.NUMERICAL_2, BigDecimal.ROUND_HALF_UP));
				if(!StringUtils.isEmpty(newemployerInvoiceLineItemsDetail.getPersonsCovered()) 
						&& FinanceConstants.NUMERICAL_0 != Integer.parseInt(newemployerInvoiceLineItemsDetail.getPersonsCovered()) )
				{
					invoiceDetailsMap.get(keyCarrerNamePlanType).setQuantityCovered(invoiceDetailsMap.get(keyCarrerNamePlanType).getQuantityCovered() + FinanceConstants.NUMERICAL_1);
				}
				invoiceDetailsMap.get(keyCarrerNamePlanType).setTotalPremium(invoiceDetailsMap.get(keyCarrerNamePlanType).getTotalPremium().add(newemployerInvoiceLineItemsDetail.getTotalPremium()).setScale(FinanceConstants.NUMERICAL_2, BigDecimal.ROUND_HALF_UP));
				//	invoiceDetailsMap.get(key_CarrerName_PlanType).setAdjustment(invoiceDetailsMap.get(key_CarrerName_PlanType).getAdjustment().add(newemployerInvoiceLineItemsDetail.getTotAdj_RetroAdj_ManualAdj()).setScale(2, BigDecimal.ROUND_HALF_UP));
				invoiceDetailsMap.get(keyCarrerNamePlanType).setTotAdj_RetroAdj_ManualAdj(invoiceDetailsMap.get(keyCarrerNamePlanType).getTotAdj_RetroAdj_ManualAdj().add(newemployerInvoiceLineItemsDetail.getTotAdj_RetroAdj_ManualAdj()).setScale(FinanceConstants.NUMERICAL_2, BigDecimal.ROUND_HALF_UP));
			}
			else
			{
				final EmployerInvoiceLineItemsDetail employerInvoiceLineItemsDetail = new EmployerInvoiceLineItemsDetail();
				employerInvoiceLineItemsDetail.setEmployeeId(newemployerInvoiceLineItemsDetail.getEmployeeId());
				employerInvoiceLineItemsDetail.setEmployeeName(newemployerInvoiceLineItemsDetail.getEmployeeName());
				employerInvoiceLineItemsDetail.setPolicyNumber(newemployerInvoiceLineItemsDetail.getPolicyNumber());
				employerInvoiceLineItemsDetail.setCarrierName(newemployerInvoiceLineItemsDetail.getCarrierName());
				employerInvoiceLineItemsDetail.setPlanType(newemployerInvoiceLineItemsDetail.getPlanType());
				employerInvoiceLineItemsDetail.setNetAmount(newemployerInvoiceLineItemsDetail.getNetAmount());
				employerInvoiceLineItemsDetail.setTotalPremium(newemployerInvoiceLineItemsDetail.getTotalPremium());
				employerInvoiceLineItemsDetail.setAdjustment(newemployerInvoiceLineItemsDetail.getRetroAdjustments());
				employerInvoiceLineItemsDetail.setManualAdjAmount(newemployerInvoiceLineItemsDetail.getManualAdjAmount());
				employerInvoiceLineItemsDetail.setManualEmployeeContriAdj(newemployerInvoiceLineItemsDetail.getManualEmployeeContriAdj());
				employerInvoiceLineItemsDetail.setManualEmployerContriAdj(newemployerInvoiceLineItemsDetail.getManualEmployerContriAdj());
				employerInvoiceLineItemsDetail.setTotAdj_RetroAdj_ManualAdj(newemployerInvoiceLineItemsDetail.getTotAdj_RetroAdj_ManualAdj());
				employerInvoiceLineItemsDetail.setTotEmployeeContri_RegContri_ManualContri(newemployerInvoiceLineItemsDetail.getTotEmployeeContri_RegContri_ManualContri());
				if(FinanceConstants.NUMERICAL_0 != Integer.parseInt(newemployerInvoiceLineItemsDetail.getPersonsCovered())){
					employerInvoiceLineItemsDetail.setQuantityCovered(FinanceConstants.NUMERICAL_1);
				}
				employerInvoiceLineItemsDetail.setPersonsCovered(newemployerInvoiceLineItemsDetail.getPersonsCovered());
				employerInvoiceLineItemsDetail.setEmployerContribution(newemployerInvoiceLineItemsDetail.getEmployerContribution());
				invoiceDetailsMap.put(keyCarrerNamePlanType, employerInvoiceLineItemsDetail);
			}

		}

		final Iterator itreateCarrier = invoiceDetailsMap.entrySet().iterator();
		while(itreateCarrier.hasNext())
		{
			final Map.Entry pairs = (Map.Entry)itreateCarrier.next();
			populatedEmployerInvoiceList.add((EmployerInvoiceLineItemsDetail)pairs.getValue());
		}
		return populatedEmployerInvoiceList;
	}

	/**
	 * Method getInvoiceLineItemsDetail.
	 * @param invoiceDetails List<EmployerInvoiceLineItems>
	 * @return List<EmployerInvoiceLineItemsDetail>
	 */
	private List<EmployerInvoiceLineItemsDetail> getInvoiceLineItemsDetail(final List<EmployerInvoiceLineItems> invoiceDetails){

		final List<EmployerInvoiceLineItemsDetail> empInvoiceLineItemList = new ArrayList<EmployerInvoiceLineItemsDetail>();

		if(!invoiceDetails.isEmpty())
		{
			for(final EmployerInvoiceLineItems invoiceLineItems : invoiceDetails)
			{
				final EmployerInvoiceLineItemsDetail employerInvoiceLineItemsDetail = new EmployerInvoiceLineItemsDetail();
				if(invoiceLineItems.getEmployee()!=null)
				{
					//	employerInvoiceLineItemsDetail.setId(invoiceLineItems.getEmployee().getId());
					//	employerInvoiceLineItemsDetail.setName(invoiceLineItems.getEmployee().getName());
					employerInvoiceLineItemsDetail.setEmployeeId(invoiceLineItems.getEmployee().getId());
					employerInvoiceLineItemsDetail.setEmployeeName((invoiceLineItems.getEmployee().getName())!=null?invoiceLineItems.getEmployee().getName():"");
				}
				else
				{
					//	employerInvoiceLineItemsDetail.setId(0);
					employerInvoiceLineItemsDetail.setEmployeeId(0);
					//	employerInvoiceLineItemsDetail.setName("");
					employerInvoiceLineItemsDetail.setEmployeeName("");
				}
				employerInvoiceLineItemsDetail.setPolicyNumber((invoiceLineItems.getPolicyNumber())!=null?invoiceLineItems.getPolicyNumber():"");
				employerInvoiceLineItemsDetail.setCarrierName((invoiceLineItems.getCarrierName())!=null?invoiceLineItems.getCarrierName():"");
				employerInvoiceLineItemsDetail.setPlanType((invoiceLineItems.getPlayType())!=null?invoiceLineItems.getPlayType():"");
				employerInvoiceLineItemsDetail.setCoverageType((invoiceLineItems.getCoverageType())!=null?invoiceLineItems.getCoverageType():"");
				employerInvoiceLineItemsDetail.setPersonsCovered((invoiceLineItems.getPersonsCovered())!=null?invoiceLineItems.getPersonsCovered():"0");
				employerInvoiceLineItemsDetail.setTotalPremium((invoiceLineItems.getTotalPremium())!=null?invoiceLineItems.getTotalPremium():BigDecimal.ZERO);
				employerInvoiceLineItemsDetail.setEmployeeContribution((invoiceLineItems.getEmployeeContribution())!=null?invoiceLineItems.getEmployeeContribution():BigDecimal.ZERO);
				employerInvoiceLineItemsDetail.setRetroAdjustments((invoiceLineItems.getRetroAdjustments())!=null?invoiceLineItems.getRetroAdjustments():BigDecimal.ZERO);
				employerInvoiceLineItemsDetail.setManualAdjAmount((invoiceLineItems.getManualAdjustmentAmount())!=null?invoiceLineItems.getManualAdjustmentAmount():BigDecimal.ZERO);
				employerInvoiceLineItemsDetail.setNetAmount((invoiceLineItems.getNetAmount())!=null?invoiceLineItems.getNetAmount():BigDecimal.ZERO);
				employerInvoiceLineItemsDetail.setManualEmployeeContriAdj((invoiceLineItems.getManualEmployeeContribution())!=null?invoiceLineItems.getManualEmployeeContribution():BigDecimal.ZERO);
				employerInvoiceLineItemsDetail.setManualEmployerContriAdj((invoiceLineItems.getManualEmployerContribution())!=null?invoiceLineItems.getManualEmployerContribution():BigDecimal.ZERO);
				BigDecimal totAdjRetroAdjManualAdj = FinancialMgmtGenericUtil.add(employerInvoiceLineItemsDetail.getRetroAdjustments(), employerInvoiceLineItemsDetail.getManualAdjAmount());
				BigDecimal totEmployeeContriRegContriManualContri = FinancialMgmtGenericUtil.add(employerInvoiceLineItemsDetail.getEmployeeContribution(), employerInvoiceLineItemsDetail.getManualEmployeeContriAdj());
				employerInvoiceLineItemsDetail.setTotAdj_RetroAdj_ManualAdj(totAdjRetroAdjManualAdj);
				employerInvoiceLineItemsDetail.setTotEmployeeContri_RegContri_ManualContri(totEmployeeContriRegContriManualContri);
				employerInvoiceLineItemsDetail.setEmployerContribution((invoiceLineItems.getEmployerContribution())!=null?invoiceLineItems.getEmployerContribution():BigDecimal.ZERO);

				empInvoiceLineItemList.add(employerInvoiceLineItemsDetail);
			}
		}
		return empInvoiceLineItemList;
	}

	private Map<String, Object> includeEmployerAddress(Employer employer, Map<String, Object> replaceableObject){

		Location empLocation = employer.getContactLocation();

		replaceableObject.put(FinanceConstants.EMPLOYER_BUSINESS_NAME, employer.getName());
		if(empLocation != null)
		{
			replaceableObject.put(FinanceConstants.CONTACT_ADDRESS, empLocation.getAddress1());
			replaceableObject.put(FinanceConstants.ADDRESS2, empLocation.getAddress2() != null ? empLocation.getAddress2() : "");
			replaceableObject.put(FinanceConstants.CONTACT_CITY, empLocation.getCity());
			replaceableObject.put(FinanceConstants.CONTACT_STATE, empLocation.getState());
			replaceableObject.put(FinanceConstants.CONTACT_ZIPCODE, empLocation.getZip());
		}
		else
		{
			LOGGER.info("Employer Location is null or empty");
			replaceableObject.put(FinanceConstants.CONTACT_ADDRESS, StringUtils.EMPTY);
			replaceableObject.put(FinanceConstants.ADDRESS2, StringUtils.EMPTY);
			replaceableObject.put(FinanceConstants.CONTACT_CITY, StringUtils.EMPTY);
			replaceableObject.put(FinanceConstants.CONTACT_STATE, StringUtils.EMPTY);
			replaceableObject.put(FinanceConstants.CONTACT_ZIPCODE, StringUtils.EMPTY);
		}
		return replaceableObject;
	}


	/**
	 * 
	 * @param employerInvoiceID
	 * @return
	 */
	private List<String> populateComments(int employerInvoiceID)
	{
		String isEmployerFeeRequired = DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.IS_EMPLOYER_FEE_REQUIRED);
		List<String> commentList = null;
		List<EmployerExchangeFees> employerExchangeFeesList = employerExchangeFeeService.findEmpExchangeFeesByInvoiceId(employerInvoiceID);
		if(employerExchangeFeesList != null && !employerExchangeFeesList.isEmpty())
		{
			LOGGER.info("EMPLOYER INVOICE PDF SERVICE: Found Exchange Fees records for EmployerInvoiceID: "+employerInvoiceID);
			Map<EmployerExchangeFees.ExchangeFeeTypeCode, BigDecimal> exchangeFeeTypeCodeMap = new HashMap<EmployerExchangeFees.ExchangeFeeTypeCode, BigDecimal>();
			for (EmployerExchangeFees employerExchangeFees : employerExchangeFeesList)
			{
				if(!exchangeFeeTypeCodeMap.isEmpty() && exchangeFeeTypeCodeMap.containsKey(employerExchangeFees.getFeeTypeCode()))
				{
					/*
					 * Adding amount for Similar Fee Type Code.
					 */
					exchangeFeeTypeCodeMap.put(employerExchangeFees.getFeeTypeCode(),
							FinancialMgmtGenericUtil.add(exchangeFeeTypeCodeMap.get(employerExchangeFees.getFeeTypeCode()),
									employerExchangeFees.getAmount()));
				}
				else
				{
					exchangeFeeTypeCodeMap.put(employerExchangeFees.getFeeTypeCode(), employerExchangeFees.getAmount());
				}
			}

			if(!exchangeFeeTypeCodeMap.isEmpty())
			{
				commentList = new ArrayList<String>();
				/*
				 * NSF Fee Comment
				 */
				if(exchangeFeeTypeCodeMap.containsKey(ExchangeFeeTypeCode.NSF_FLAT_FEE))
				{
					commentList.add(FinanceConstants.NSF_FEE_COMMENT_PDF.replaceAll("NSFFlatFee", exchangeFeeTypeCodeMap.get(ExchangeFeeTypeCode.NSF_FLAT_FEE).toString()));
				}

				/*
				 * For Employer Fee comment
				 * Below code is commented as Jira ID: HIX-59216 postponed to SHOP10 release 
				 */
				if(StringUtils.isNotEmpty(isEmployerFeeRequired) &&
						"TRUE".equalsIgnoreCase(isEmployerFeeRequired) &&
						exchangeFeeTypeCodeMap.containsKey(ExchangeFeeTypeCode.EMPLOYER_USER_FEE))
				{

					commentList.add(FinanceConstants.EMPLOYER_FEE_COMMENT_PDF.replaceAll("EMPLOYER_FEE_AMOUNT",
							DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.EMPLOYER_FLAT_FEE_PER_ENROLLMENT).replace("$", "")));
				}

				/*
				 * To add other Exchange Fees Data 
				 */
			}
		}
		return commentList;
	}	
}
