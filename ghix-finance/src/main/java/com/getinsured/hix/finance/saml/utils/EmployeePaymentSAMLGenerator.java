package com.getinsured.hix.finance.saml.utils;

import java.io.StringWriter;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.joda.time.DateTime;
import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.common.SAMLObjectBuilder;
import org.opensaml.common.SAMLVersion;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.AttributeValue;
import org.opensaml.saml2.core.Audience;
import org.opensaml.saml2.core.AudienceRestriction;
import org.opensaml.saml2.core.AuthnContext;
import org.opensaml.saml2.core.AuthnContextClassRef;
import org.opensaml.saml2.core.AuthnStatement;
import org.opensaml.saml2.core.Conditions;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.NameIDType;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.Status;
import org.opensaml.saml2.core.StatusCode;
import org.opensaml.saml2.core.StatusMessage;
import org.opensaml.saml2.core.Subject;
import org.opensaml.saml2.core.SubjectConfirmation;
import org.opensaml.saml2.core.SubjectConfirmationData;
import org.opensaml.saml2.core.SubjectLocality;
import org.opensaml.saml2.core.impl.ResponseMarshaller;
import org.opensaml.saml2.core.impl.StatusBuilder;
import org.opensaml.saml2.core.impl.StatusCodeBuilder;
import org.opensaml.saml2.core.impl.StatusMessageBuilder;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.XMLObjectBuilder;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.schema.XSString;
import org.opensaml.xml.security.SecurityConfiguration;
import org.opensaml.xml.security.SecurityHelper;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.security.keyinfo.KeyInfoGenerator;
import org.opensaml.xml.security.keyinfo.KeyInfoGeneratorFactory;
import org.opensaml.xml.security.keyinfo.KeyInfoGeneratorManager;
import org.opensaml.xml.security.keyinfo.KeyInfoHelper;
import org.opensaml.xml.security.keyinfo.NamedKeyInfoGeneratorManager;
import org.opensaml.xml.security.x509.BasicX509Credential;
import org.opensaml.xml.signature.KeyInfo;
import org.opensaml.xml.signature.Signature;
import org.opensaml.xml.signature.SignatureConstants;
import org.opensaml.xml.signature.SignatureException;
import org.opensaml.xml.signature.Signer;
import org.opensaml.xml.signature.X509SubjectName;
import org.opensaml.xml.util.Base64;
import org.opensaml.xml.util.XMLHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.w3c.dom.Element;

import com.getinsured.hix.finance.saml.ca.utils.CarrierPayment;
import com.getinsured.hix.finance.utils.FinanceConstants;
import com.getinsured.hix.finance.utils.FinanceConstants.SAMLConstants;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.platform.util.GhixPlatformKeystore;
import com.getinsured.timeshift.TSDateTime;

/**
 * Class creates a valid SAML 2.0 Assertion.
 * @author Sahoo_s
 * @since 30 April 2014
 */

public class EmployeePaymentSAMLGenerator 
{

	private static final String Attribute_carrierPayment = "carrierPayment";

	private static final String SCHEMA_LOCATION_CALHEERS = "http://www.calheers.ca.gov/CarrierPayment";

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeePaymentSAMLGenerator.class);
	
	private static Credential signingCredential = null;
	
	@Autowired private GhixPlatformKeystore ghixPlatformKeystore  = null;
	static JAXBContext contextObj = null;
	
	public EmployeePaymentSAMLGenerator(){
		ghixPlatformKeystore = (GhixPlatformKeystore) GHIXApplicationContext.getBean("ghixPlatformKeystore");
		try {
			contextObj = JAXBContext.newInstance(CarrierPayment.class);
		} catch (JAXBException e) {
			LOGGER.error("Error in creating JAXBContext instance");
		}
	}
	
	/**
	 * Returns the encoded SAML string which will passed 
	 * to the payment redirect URL for FFM checkout.
	 * @param keyStoreParams
	 * 					Contain key store related information
	 * @param inputData
	 * 					Contain input parameters to prepare SAML assertion
	 * @param samlInput holds carrier name and enrollment information
	 * @param paymentURL carrier payment URL
	 * @return encodedSAMLRespone
	 * @throws CertificateException 
	 * @throws KeyStoreException 
	 */
	public static String generateEncodedSAML(EmployeePaymentSAMLInputContainer samlInput, Map<String, String> keyStoreParams, String paymentURL)
	{
		String encodedSAMLRespone = null; 
		try 
		{
			EmployeePaymentSAMLGenerator generator = new EmployeePaymentSAMLGenerator();
			generator.intializeCredentials(keyStoreParams);
			Assertion assertion = EmployeePaymentSAMLGenerator.buildDefaultAssertion(samlInput, keyStoreParams, paymentURL);
			
			Signature responseSignature = null;
			responseSignature = buildSignatureDetails(keyStoreParams);
			
			Response resp = EmployeePaymentSAMLGenerator.buildResponse(paymentURL);
			resp.setSignature(responseSignature);
			resp.getAssertions().add(assertion); 
			
			try {
				Configuration.getMarshallerFactory().getMarshaller(resp).marshall(resp);
				Signer.signObject(responseSignature);
			} catch (MarshallingException | SignatureException e) {
				LOGGER.error("MarshallingException caught in generateEncodedSAML:: ", e);
			}
			
			
			ResponseMarshaller respMarshaller = new ResponseMarshaller();
			Element plain = respMarshaller.marshall(resp);
			String samlResponse = XMLHelper.nodeToString(plain);
			/*This try catch only use for unit testing purpose
			try {
				System.out.println(samlResponse);
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				DOMSource source = new DOMSource(plain);
				StreamResult result = new StreamResult(new File("C:\\data1\\GeneratedSAMLFile.xml"));
				transformer.transform(source, result);
			} catch (Exception e) {
				System.err.println(samlResponse);
			}*/
			
			// Now Base64 Encode
			encodedSAMLRespone = new String(Base64.encodeBytes(samlResponse.getBytes(), Base64.DONT_BREAK_LINES));
			LOGGER.info("Encoded SAML Response :: "+encodedSAMLRespone);
		} catch (MarshallingException e) {
			LOGGER.error("Error- MarshallingException while executing generateEncodedSAML:: ", e);
		}	
		LOGGER.info("=============== Inside the method generateEncodedSAML :: End =================");
		return encodedSAMLRespone;
	}

	/**
	 * 
	 * @param credential
	 * @return
	 * @throws org.opensaml.xml.security.SecurityException
	 */
	public static KeyInfo getKeyInfo(Credential credential)	throws org.opensaml.xml.security.SecurityException 
	{
		SecurityConfiguration secConfiguration = Configuration.getGlobalSecurityConfiguration();
		NamedKeyInfoGeneratorManager namedKeyInfoGeneratorManager = secConfiguration.getKeyInfoGeneratorManager();
		KeyInfoGeneratorManager keyInfoGeneratorManager = namedKeyInfoGeneratorManager.getDefaultManager();
		KeyInfoGeneratorFactory factory = keyInfoGeneratorManager.getFactory(credential);
		KeyInfoGenerator generator = factory.newInstance();
		return generator.generate(credential);
	}

	/**
	 * Initialize the Security Key related 
	 * credentials for next level validation
	 * @param keyStoreParams
	 * 			{@link Map<String, String>}
	 * @throws CertificateException
	 * @throws KeyStoreException
	 */
	private void intializeCredentials(Map<String, String> keyStoreParams)
	{
		
		String loadPassword = keyStoreParams.get(SAMLConstants.PAYREDIRECT_SECUREKEY_PASSWORD);
		String privateKeyAliasName = keyStoreParams.get(SAMLConstants.PAYREDIRECT_PRIVATE_KEY);
		PrivateKey pk = null;
		if(privateKeyAliasName == null || privateKeyAliasName.length()== 0) {
			privateKeyAliasName = "Default".intern();
		}
		if(loadPassword.equalsIgnoreCase("Default".intern())) {
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Loading key using default platform key credentials (configuration)".intern());
			}
			pk = this.ghixPlatformKeystore.getPrivateKey(privateKeyAliasName, null);
		}else {
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Loading key using UI supplied credentials, PK {} and password: xxxxxx".intern(),privateKeyAliasName );
			}
			pk = this.ghixPlatformKeystore.getPrivateKey(privateKeyAliasName, loadPassword.toCharArray());
		}
		if(pk!=null){
			X509Certificate certificate = (X509Certificate) this.ghixPlatformKeystore.getCertificates(privateKeyAliasName);
			BasicX509Credential credential = new BasicX509Credential();
			credential.setEntityCertificate(certificate);
			credential.setPrivateKey(pk);
			signingCredential = credential;
		}else {
			throw new RuntimeException("Failed to initialize the SAML, missing private key ".intern());
		}
		
	}

	
	/**
	 * Builds a SAML Attribute of type String
	 * 
	 * @param name
	 * 			Attribute Name
	 * @param value
	 * 			Attribute Value
	 * @param builderFactory
	 * 			XMLObjectBuilderFactory
	 * @return attribute
	 * @throws ConfigurationException
	 */
	public static Attribute buildStringAttribute(String name, String value)	throws ConfigurationException 
	{
		SAMLObjectBuilder<?> attrBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(Attribute.DEFAULT_ELEMENT_NAME);
		Attribute attrFirstName = (Attribute) attrBuilder.buildObject();
		attrFirstName.setNameFormat(Attribute.UNSPECIFIED);
		attrFirstName.setName(name);

		// Set custom Attributes
		XMLObjectBuilder<?> stringBuilder = XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(XSString.TYPE_NAME);
		XSString attrValueFirstName = (XSString) stringBuilder.buildObject(AttributeValue.DEFAULT_ELEMENT_NAME, XSString.TYPE_NAME);
		attrValueFirstName.setNoNamespaceSchemaLocation(null);
		attrValueFirstName.setValue(value);

		attrFirstName.getAttributeValues().add(attrValueFirstName);
		
		
		return attrFirstName;
	}

	/**
	 * Helper method which includes some basic SAML fields which are part of
	 * almost every SAML Assertion.
	 * 
	 * @param input
	 * @param configParams 
	 * @param paymentURL 
	 * @param keyStoreParams 
	 * @return Assertion
	 */
	public static Assertion buildDefaultAssertion(EmployeePaymentSAMLInputContainer input, Map<String, String> configParams, String paymentURL) 
	{
		try {
			
			// Create the NameIdentifier
			SAMLObjectBuilder<?> nameIdBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(NameID.DEFAULT_ELEMENT_NAME);
			NameID nameId = (NameID) nameIdBuilder.buildObject();
			nameId.setNameQualifier(input.getStrNameQualifier());
			nameId.setFormat(NameID.ENTITY);
			
			// Create the SubjectConfirmation
			SAMLObjectBuilder<?> subjectConfirmationDataBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(SubjectConfirmationData.DEFAULT_ELEMENT_NAME);
			SubjectConfirmationData subjectConfirmationData = (SubjectConfirmationData)subjectConfirmationDataBuilder.buildObject();
			DateTime dateTime = TSDateTime.getInstance();
			//subjectConfirmationData.setNotOnOrAfter(dateTime.plusMinutes(FinanceConstants.NUMERICAL_5));
			//HIX-77826
			//subjectConfirmationData.setNotOnOrAfter(dateTime.plusHours(24));
			subjectConfirmationData.setNotOnOrAfter(dateTime.plusMinutes(FinanceConstants.NUMERICAL_7));
			subjectConfirmationData.setRecipient(paymentURL);
			
			SAMLObjectBuilder<?> subjectConfirmationBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(SubjectConfirmation.DEFAULT_ELEMENT_NAME);
			SubjectConfirmation subjectConfirmation = (SubjectConfirmation) subjectConfirmationBuilder.buildObject();
			subjectConfirmation.setMethod(SAMLConstants.SAML_SUBJECT_CONFIRMATION);
			subjectConfirmation.setSubjectConfirmationData(subjectConfirmationData);
			
			// Create the Subject
			SAMLObjectBuilder<?> subjectBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(Subject.DEFAULT_ELEMENT_NAME);
			Subject subject = (Subject) subjectBuilder.buildObject();
			subject.setNameID(nameId);
			subject.getSubjectConfirmations().add(subjectConfirmation);
			
			// Create Authentication Statement
			SAMLObjectBuilder<?> authStatementBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(AuthnStatement.DEFAULT_ELEMENT_NAME);
			AuthnStatement authnStatement = (AuthnStatement) authStatementBuilder.buildObject();
			DateTime now1 = TSDateTime.getInstance();
			authnStatement.setAuthnInstant(now1);
			authnStatement.setSessionIndex(input.getSessionId());
			authnStatement.setSessionNotOnOrAfter(now1);

			SAMLObjectBuilder<?> authContextBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(AuthnContext.DEFAULT_ELEMENT_NAME);
			AuthnContext authnContext = (AuthnContext) authContextBuilder.buildObject();

			SAMLObjectBuilder<?> authContextClassRefBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(AuthnContextClassRef.DEFAULT_ELEMENT_NAME);
			AuthnContextClassRef authnContextClassRef = (AuthnContextClassRef) authContextClassRefBuilder.buildObject();
			authnContextClassRef.setAuthnContextClassRef(SAMLConstants.SAML_AUTHN_CONTEXT_CLASSREF); 
			authnContext.setAuthnContextClassRef(authnContextClassRef);
			
			// Now let us create subject locality
			SAMLObjectBuilder<?> subjectLocalityBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(SubjectLocality.DEFAULT_ELEMENT_NAME);
			SubjectLocality subjectLocality = (SubjectLocality)subjectLocalityBuilder.buildObject();
			subjectLocality.setDNSName(configParams.get(SAMLConstants.SECURITY_DNS_NAME));
			subjectLocality.setAddress(configParams.get(SAMLConstants.SECURITY_ADDRESS));
			authnStatement.setSubjectLocality(subjectLocality);
			authnStatement.setAuthnContext(authnContext);
			
			// Builder Attributes
			SAMLObjectBuilder<?> attrStatementBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(AttributeStatement.DEFAULT_ELEMENT_NAME);
			AttributeStatement attrStatement = (AttributeStatement) attrStatementBuilder.buildObject();
				
			if(input.getCaCustomAttributes() != null && !input.getCaCustomAttributes().isEmpty()){
				Attribute carrierPaymentAttribute =buildCarrierPaymentAttributeForCA((CarrierPayment) input.getCaCustomAttributes().get("CARRIER_PAYMENT"));
				attrStatement.getAttributes().add(carrierPaymentAttribute);
			}
			else
			{
				// Create the attribute statement
				Map<String, String> attributes = input.getAttributes();
				if (attributes != null)
				{
					for (Map.Entry<String, String> entry : attributes.entrySet()) {
						Attribute attrFirstName = buildStringAttribute(entry.getKey(),	entry.getValue());
						attrStatement.getAttributes().add(attrFirstName);
					}
				}
			}
			DateTime now = new DateTime();
			SAMLObjectBuilder<?> conditionsBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(Conditions.DEFAULT_ELEMENT_NAME);
			Conditions conditions = (Conditions) conditionsBuilder.buildObject();
			if (configParams.get(SAMLConstants.ISSUER_AUTH_URL) != null && !configParams.get(SAMLConstants.ISSUER_AUTH_URL).isEmpty()) {
				SAMLObjectBuilder<?> audienceBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(Audience.DEFAULT_ELEMENT_NAME);
				Audience audience = (Audience) audienceBuilder.buildObject();
				audience.setAudienceURI(configParams.get(SAMLConstants.ISSUER_AUTH_URL));
				
				SAMLObjectBuilder<?> audienceRestBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(AudienceRestriction.DEFAULT_ELEMENT_NAME);
				AudienceRestriction audienceRestriction = (AudienceRestriction) audienceRestBuilder.buildObject();
				audienceRestriction.getAudiences().add(audience);
				
				conditions.setNotBefore(now);
				conditions.setNotOnOrAfter(now.plusMinutes(FinanceConstants.NUMERICAL_7));
				conditions.getAudienceRestrictions().add(audienceRestriction);
			}
			
			// Create Issuer
			SAMLObjectBuilder<?> issuerBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(Issuer.DEFAULT_ELEMENT_NAME);
			Issuer issuer = (Issuer) issuerBuilder.buildObject();
			issuer.setFormat(NameIDType.ENTITY);
			issuer.setValue(input.getStrIssuer());
		
			// Create the assertion
			SAMLObjectBuilder<?> assertionBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(Assertion.DEFAULT_ELEMENT_NAME);
			Assertion assertion = (Assertion) assertionBuilder.buildObject();
			assertion.setIssuer(issuer);
			assertion.setIssueInstant(now);
			assertion.setVersion(SAMLVersion.VERSION_20);
			assertion.setID(SAMLConstants.GI_SAML_ASSERTION+generateUUIDHexString(FinanceConstants.NUMERICAL_30));
			assertion.setSubject(subject);
			assertion.setSchemaLocation(SAMLConstants.ASSERTION_LVL_NSPACE);
			assertion.getAuthnStatements().add(authnStatement);
			assertion.getAttributeStatements().add(attrStatement);
			if (configParams.get(SAMLConstants.ISSUER_AUTH_URL) != null) {
				assertion.setConditions(conditions);
			}
			
			
			//Add security information on Assertion level
			Signature assertionSignature = null;
			assertionSignature = buildSignatureDetails(configParams);
			assertion.setSignature(assertionSignature);
			Configuration.getMarshallerFactory().getMarshaller(assertion).marshall(assertion);
	        Signer.signObject(assertionSignature);
	        return assertion;
		} catch (Exception e) {
			LOGGER.error("Error in buildDefaultAssertion Method: ",e);
		}
		return null;
	}

	/**
	 * Build the Security XMLSignature Object
	 * to prepare SAML2.0 Response pay load
	 * @param keystoreParams 
	 * 
	 * @return signature
	 * 			{@link Signature}
	 */
	private static Signature buildSignatureDetails(Map<String, String> keystoreParams) {
		Signature signature;
		try {
			DefaultBootstrap.bootstrap();
		} catch (ConfigurationException e) {
			LOGGER.error("Error while executing generateEncodedSAML:: ", e);
		}
		
		signature = (Signature) Configuration.getBuilderFactory().getBuilder(Signature.DEFAULT_ELEMENT_NAME).buildObject(Signature.DEFAULT_ELEMENT_NAME);
		signature.setSigningCredential(signingCredential);

		signature.setSignatureAlgorithm(SignatureConstants.ALGO_ID_SIGNATURE_RSA);
		signature.setCanonicalizationAlgorithm(SignatureConstants.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);
		
		// This is also the default if a null SecurityConfiguration is specified
		SecurityConfiguration secConfig = Configuration.getGlobalSecurityConfiguration();
		// If null this would result in the default KeyInfoGenerator being used
		
		try {
			SecurityHelper.prepareSignatureParams(signature, signingCredential, secConfig, null);
		} catch (SecurityException e) {
			LOGGER.error("Error while executing generateEncodedSAML:: ", e);
		} catch (org.opensaml.xml.security.SecurityException e) {
			LOGGER.error("Error while executing generateEncodedSAML:: ", e);
		}
		
		// Adding KeyInfo
		try {
			KeyInfo aKeyInfo = EmployeePaymentSAMLGenerator.getKeyInfo(signingCredential);
			X509SubjectName x509SubjectName = KeyInfoHelper.buildX509SubjectName(keystoreParams.get(SAMLConstants.ISSUER_AUTH_URL));
			aKeyInfo.getX509Datas().get(0).getX509SubjectNames().add(x509SubjectName);
			signature.setKeyInfo(aKeyInfo);
		} catch (SecurityException e1) {
			LOGGER.error("SecurityException caught in generateEncodedSAML:: ", e1);
		} catch (org.opensaml.xml.security.SecurityException e1) {
			LOGGER.error("org.opensaml.xml.security.SecurityException caught in generateEncodedSAML:: ", e1);
		}
		return signature;
	}

	/**
	 * Build the SAML2.0 Response Object
	 * 
	 * @param anAssertion
	 * 			{@link Assertion}
	 * @return resp
	 * 			{@link Response}
	 */
	static Response buildResponseWithAssertion(Assertion anAssertion) 
	{
		Response resp = (Response) Configuration.getBuilderFactory().getBuilder(Response.DEFAULT_ELEMENT_NAME).buildObject(Response.DEFAULT_ELEMENT_NAME);
		resp.setID("GIResponse-"+EmployeePaymentSAMLGenerator.generateUUIDHexString(FinanceConstants.NUMERICAL_30));
		DateTime dateTime = TSDateTime.getInstance();
		resp.setIssueInstant(dateTime);
		resp.setStatus(EmployeePaymentSAMLGenerator.buildStatus(SAMLConstants.SAML_RESPONSE_SUCCESS, null));
		resp.getAssertions().add(anAssertion);
		return resp;
	}

	/**
	 * Build the SAML2.0 Response Object
	 * 
	 * @param paymentURL
	 * 			{@link String}
	 * @return resp
	 * 			{@link Response}
	 */
	static Response buildResponse(String paymentURL) 
	{
		Response resp = (Response) Configuration.getBuilderFactory().getBuilder(Response.DEFAULT_ELEMENT_NAME).buildObject(Response.DEFAULT_ELEMENT_NAME);
		DateTime dateTime = TSDateTime.getInstance();
		resp.setIssueInstant(dateTime);
		resp.setDestination(paymentURL);
		resp.setID(SAMLConstants.GI_SAML_RESPONSE+EmployeePaymentSAMLGenerator.generateUUIDHexString(FinanceConstants.NUMERICAL_30));
		resp.setStatus(EmployeePaymentSAMLGenerator.buildStatus(SAMLConstants.SAML_RESPONSE_STATUS, null));
		return resp;
	}

	/**
	 * Build the SAML2.0 Status Object
	 * 
	 * @param status
	 * 			{@link String}
	 * @param statMsg
	 * 			{@link String}
	 * @return status
	 * 			{@link Status}
	 */
	private static Status buildStatus(String status, String statMsg) 
	{
		Status stat = new StatusBuilder().buildObject();
		// Set the status code
		StatusCode statCode = new StatusCodeBuilder().buildObject();
		statCode.setValue(status);
		stat.setStatusCode(statCode);
		// Set the status Message
		if (statMsg != null) 
		{
			StatusMessage statMesssage = new StatusMessageBuilder().buildObject();
			statMesssage.setMessage(statMsg);
			stat.setStatusMessage(statMesssage);
		}
		return stat;
	}
	
	/**
	 * Generate UUID id for SAML2.0 Response & Assertion level
	 * @param size
	 * 			{@link Integer}
	 * @return hexString
	 * 			{@link String}
	 */
	private static String generateUUIDHexString(int size) {
    	String hexString = null;
    	final int ten = FinanceConstants.NUMERICAL_10, thirtyTwo = FinanceConstants.NUMERICAL_30;
    	if(size>ten && size<=thirtyTwo){
    		String uuid = UUID.randomUUID().toString();
    		uuid = uuid.replaceAll("[^0-9a-zA-Z]+", "");
    		hexString = uuid.substring(0, size);
    	}
        return hexString;
    }
	
	public static Attribute buildCarrierPaymentAttributeForCA(CarrierPayment carrierPayment)	throws ConfigurationException, Exception
	{
		SAMLObjectBuilder<?> attrBuilder = (SAMLObjectBuilder<?>) XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(Attribute.DEFAULT_ELEMENT_NAME);
		Attribute attrFirstName = (Attribute) attrBuilder.buildObject();
		attrFirstName.setNameFormat(Attribute.UNSPECIFIED);
		attrFirstName.setName(Attribute_carrierPayment);

		// Set custom Attributes
		XMLObjectBuilder<?> stringBuilder = XmlObjectBuilderFactory.getSAMLBuilder().getBuilder(XSString.TYPE_NAME);
		XSString attrCarrierPaymentValue = (XSString) stringBuilder.buildObject(AttributeValue.DEFAULT_ELEMENT_NAME, XSString.TYPE_NAME);
		try{
			Marshaller marshallerObj = contextObj.createMarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); 
			StringWriter sw = new StringWriter();
			marshallerObj.marshal(carrierPayment, sw);
			String xmlString = sw.toString();
			
			attrCarrierPaymentValue.setValue(new String(Base64.encodeBytes(xmlString.getBytes(),Base64.DONT_BREAK_LINES)));
			attrFirstName.getAttributeValues().add(attrCarrierPaymentValue);
			
		}
		catch(Exception e){
			LOGGER.error("Error in creating CA Carrier Payment Attribute in Payment Redirect Flow.");
			throw e;
		}
		
		
		
		return attrFirstName;
	}

}
