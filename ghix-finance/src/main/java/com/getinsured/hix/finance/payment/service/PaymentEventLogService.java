package com.getinsured.hix.finance.payment.service;

import java.util.List;

import com.getinsured.hix.model.PaymentEventLog;

/**
 * 
 * @author Sharma_k
 *
 */
public interface PaymentEventLogService 
{
	/**
	 * 
	 * @param paymentEventLog
	 * @return
	 */
	PaymentEventLog savePaymentEventLog(PaymentEventLog paymentEventLog);
	
	/**
	 * 
	 * @param merchantRefNo
	 * @param Eventype
	 * @return
	 */
	List<PaymentEventLog> getPaymentEventLogByEventAndRefNo(String merchantRefNo, String eventType);
	
	/**
	 * 
	 * @param merchantRefNo
	 * @return
	 */
	List<PaymentEventLog> getPymtEventLogByMerchantRefNo(String merchantRefNo);
	
	List<PaymentEventLog> getPaymentEventLogByMerchantRefCodeList(List<String> merchantRefCode);
	
	/**
	 * 
	 * @param merchantRefCode
	 * @param requestID
	 * @param transactionStatus
	 * @return
	 */
	PaymentEventLog getPaymentEventLogForReconciliation(String merchantRefCode, String requestID, String transactionStatus);
	
	/**
	 * 
	 * @param id
	 * @param paymentStatus
	 * @return
	 */
	PaymentEventLog updatePaymentStatus(Integer id, PaymentEventLog.PaymentStatus paymentStatus);

}
