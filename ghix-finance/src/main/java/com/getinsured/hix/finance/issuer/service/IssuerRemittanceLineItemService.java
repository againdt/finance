package com.getinsured.hix.finance.issuer.service;

import java.util.List;

import com.getinsured.hix.model.EmployerInvoiceLineItems;
import com.getinsured.hix.model.IssuerRemittance;
import com.getinsured.hix.model.IssuerRemittanceLineItem;

public interface IssuerRemittanceLineItemService 
{
	
	IssuerRemittanceLineItem saveIssuerRemittanceLineItems(IssuerRemittanceLineItem issuerRemittanceLineitem);
	
	List<IssuerRemittanceLineItem> getIssuerRemittanceLineItems(IssuerRemittance issuerRemittance);
	
	IssuerRemittanceLineItem createIssuerRemittanceLineItemsfromEmployerInvoiceLineItems(EmployerInvoiceLineItems employerInvoiceLineItems);
	
	//IssuerInvoicesLineItems updateAmountReceivedFromEmployer(IssuerInvoices issuerInvoices,Employer employer,Employee employee,String planType, float amtReceivedFrmEmployer);
	
	IssuerRemittanceLineItem updateAmtPaidFrmEmpToIssuer(EmployerInvoiceLineItems employerInvoiceLineItems, IssuerRemittance issuerRemittance);
	
	/*List<IssuerInvoicesLineItems> getLineItemsWithEnrollment(int issuerInvoicesId);*/
	
	/**
	 * 
	 * @param invoiceId
	 * @param issuerId
	 * @return
	 */
	List<IssuerRemittanceLineItem> getIssLineItemsByPaidEmpLineItems(int invoiceId, int issuerId);
	
	/**
	 * 
	 * @param invoiceId
	 * @return
	 */
	List<IssuerRemittanceLineItem> getPaidIssuerLineItems(int invoiceId);
}
