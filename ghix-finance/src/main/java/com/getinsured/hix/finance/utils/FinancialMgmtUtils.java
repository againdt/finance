package com.getinsured.hix.finance.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

import com.getinsured.hix.dto.broker.BrokerDTO;
import com.getinsured.hix.dto.broker.BrokerInformationDTO;
import com.getinsured.hix.dto.broker.BrokerListDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeDetailsDto;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentCurrentMonthDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.dto.planmgmt.IssuerRequest;
import com.getinsured.hix.dto.shop.EmployerEnrollmentDTO;
import com.getinsured.hix.dto.shop.EmployerEnrollmentRequest;
import com.getinsured.hix.dto.shop.GetEmployeesDTO;
import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.finance.utils.FinanceSecureRequest.RequestMethod;
import com.getinsured.hix.model.EmployeeDetails;
import com.getinsured.hix.model.EmployerEnrollment;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentRemittanceDTO;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.BrokerServiceEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.ShopEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.extended.ISO8601DateConverter;

/**
 * This is the utility class for GHIX-FINANCE module.
 * 
 * @author Sharma_k
 * @since 28-Jun_2013 
 */

@Component
public class FinancialMgmtUtils 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(FinancialMgmtUtils.class);	

	/*@Autowired private RestTemplate restTemplate;*/
	@Autowired private GhixRestTemplate ghixRestTemplate;
	
	/**
	 * Method to fetch Enrollment by passing enrollmentID
	 * @param int enrollmentId
	 * @return Object<Enrollment>
	 * @throws GIException
	 */
	public Enrollment getEnrollmentById(int enrollmentId)throws GIException
	{
		
		FinanceSecureRequest financesecureRequest = new FinanceSecureRequest();
		String getresp = financesecureRequest.sendSecureRequest(ghixRestTemplate,
				DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER),
				EnrollmentEndPoints.FIND_ENROLLMENT_BY_ID_URL+enrollmentId, null, RequestMethod.GET);
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse  enrollmentResponse = (EnrollmentResponse) xstream.fromXML(getresp);
		
		if(enrollmentResponse.getStatus() != null && GhixConstants.RESPONSE_FAILURE.equals(enrollmentResponse.getStatus()))
		{
			LOGGER.warn("Enrollment getEnrollmentById call failed - " +  enrollmentResponse.getErrCode() + enrollmentResponse.getErrMsg());
			throw new GIException(enrollmentResponse.getErrCode(), enrollmentResponse.getErrMsg(), FinanceConstants.EXCEPTION_SEVERITY_WARN);
		}
		
		return enrollmentResponse.getEnrollment();
	}
	
	/**
	 * Method to check whether Enrollment Exists or not 
	 * @param int enrollmentId
	 * @return boolean
	 * @throws GIException
	 */
	public boolean isEnrollmentExists(int enrollmentId)throws GIException
	{
		FinanceSecureRequest financeSecureRequest = new FinanceSecureRequest();
		String getresp = financeSecureRequest.sendSecureRequest(ghixRestTemplate,
				DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER),
				EnrollmentEndPoints.IS_ENROLLMENT_EXISTS+enrollmentId, null, RequestMethod.GET);
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse  enrollmentResponse = (EnrollmentResponse) xstream.fromXML(getresp);
		
		if(enrollmentResponse.getStatus() != null && GhixConstants.RESPONSE_FAILURE.equals(enrollmentResponse.getStatus()))
		{
			LOGGER.warn("Enrollment getIsEnrollmentExists call failed - " +  enrollmentResponse.getErrCode() + enrollmentResponse.getErrMsg());
			throw new GIException(enrollmentResponse.getErrCode(), enrollmentResponse.getErrMsg(), FinanceConstants.EXCEPTION_SEVERITY_WARN);
		}
		
		return enrollmentResponse.getIsEnrollmentExists();
	}
		
	/**
	 * Method to get List<EnrollmentCurrentMonthDTO> by passing set of parameter refer @param
	 * @param Date currentDate 
	 * @param Date lastInvoiceDate
	 * @param List<String> status
	 * @param int employerId
	 * @param int employerEnrollmentId
	 * @return List<EnrollmentCurrentMonthDTO>
	 * @throws GIException
	 */

	public List<EnrollmentCurrentMonthDTO> getEnrollmentByCurrentMonth(Date invoiceCreationTime,
			Date currentDate, List<String> status, int employerId, int employerEnrollmentId)throws GIException
	{
		Map<String, Object> enrollmentFilterData = new HashMap<String, Object>();
		List<EnrollmentCurrentMonthDTO> enrollmentCurrentMonthDto = null;
		/*
		 * As this parameter's name are hard coded we need to send value with reference to this keys
		 */
		DateFormat dateFormat = new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT);
		String currentdate = dateFormat.format(currentDate);
		enrollmentFilterData.put(FinanceConstants.CURRENT_DATE, currentdate);// Sysdate
		enrollmentFilterData.put(FinanceConstants.STATUS, status);//lookupService.getLookupValueCode("ENROLLMENT_STATUS","Enrolled")
		enrollmentFilterData.put(FinanceConstants.EMPLOYER_ID, employerId);//Employer Id whose enrollment need to be fetched
		DateFormat dateTimeFormat = new SimpleDateFormat(FinanceConstants.CREATEDON_TIME_FORMAT);
		String createdOn = dateTimeFormat.format(invoiceCreationTime);
		enrollmentFilterData.put(FinanceConstants.LAST_INVOICE_TIMESTAMP, createdOn);
		enrollmentFilterData.put(FinanceConstants.EMPLOYER_ENROLLMENT_ID, employerEnrollmentId);
		
		FinanceSecureRequest financesecureRequest = new FinanceSecureRequest();
		String getresp = financesecureRequest.sendSecureRequest(ghixRestTemplate, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER), EnrollmentEndPoints.SEARCH_ENROLLMENT_BY_CURRENTMONTH, enrollmentFilterData, RequestMethod.POST);
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse  enrollmentResponse = (EnrollmentResponse) xstream.fromXML(getresp);
		
		if(enrollmentResponse.getStatus() != null && GhixConstants.RESPONSE_FAILURE.equals(enrollmentResponse.getStatus()))
		{
			LOGGER.warn("Enrollment getEnrollmentByCurrentMonth call failed - " +  enrollmentResponse.getErrCode() + enrollmentResponse.getErrMsg());
			throw new GIException(enrollmentResponse.getErrCode(), enrollmentResponse.getErrMsg(), FinanceConstants.EXCEPTION_SEVERITY_WARN);
		}
		else
		{
			Map<String, Set<EnrollmentCurrentMonthDTO>> enrollmentFinanceMap = enrollmentResponse.getEnrollmentFinanceMap();
			if(enrollmentFinanceMap == null || enrollmentFinanceMap.isEmpty())
			{
				enrollmentCurrentMonthDto = new ArrayList<>();
			}
			else
			{
				SortedSet<EnrollmentCurrentMonthDTO> enrollmentCurrentMonthDTOSet = (SortedSet<EnrollmentCurrentMonthDTO>) enrollmentFinanceMap.get(FinanceConstants.CURRENT_MAP_KEY);
				if(enrollmentCurrentMonthDTOSet == null || enrollmentCurrentMonthDTOSet.isEmpty())
				{
					enrollmentCurrentMonthDto = new ArrayList<>();
				}
				else
				{
					enrollmentCurrentMonthDto = new ArrayList<>(enrollmentCurrentMonthDTOSet);
				}
			}
		}
		return enrollmentCurrentMonthDto;
	}
	
	/**
	 * Method to get Enrollee Object by passed id
	 * @param int id
	 * @return Object<Enrollee>
	 * @throws GIException
	 */
	public Enrollee getEnrolleeByEnrollmentId(int id)throws GIException
	{
		FinanceSecureRequest financesecureRequest = new FinanceSecureRequest();
		String getResp = financesecureRequest.sendSecureRequest(ghixRestTemplate,
				DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER),
				EnrollmentEndPoints.SEARCH_SUBSCRIBER_BY_ENROLLMENT_ID+Integer.toString(id), null, RequestMethod.GET);
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrolleeResponse  enrolleeResponse = (EnrolleeResponse) xstream.fromXML(getResp);
		if(enrolleeResponse.getStatus() != null && GhixConstants.RESPONSE_FAILURE.equals(enrolleeResponse.getStatus()))
		{
			LOGGER.warn("ENROLLEE getEnrolleeByEnrollmentId call failed - " +  enrolleeResponse.getErrCode() + enrolleeResponse.getErrMsg());
			throw new GIException(enrolleeResponse.getErrCode(), enrolleeResponse.getErrMsg(), FinanceConstants.EXCEPTION_SEVERITY_WARN);
		}
		return enrolleeResponse.getEnrollee();
	}
	
	/**
	 * Method updateEnrollmentStatus
	 * @param int employerEnrollmentId
	 * @param Object<EnrollmentStatus> status
	 * @return String
	 * @throws GIException
	 */
	public String updateEnrollmentStatus(int employerEnrollmentId, EnrollmentStatus status)throws GIException
	{
		/*
		 * Below object is created due to
		 * org.springframework.http.converter.HttpMessageNotWritableException: Could not write JSON: Infinite recursion 
		 * As If we pass a hibernate mapped Employer object it causes the above mentioned Exception. 
		 */
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest(); 
		enrollmentRequest.setEnrollmentStatus(status);
		enrollmentRequest.setEmployerEnrollmentId(employerEnrollmentId);
		String ernReq = xstream.toXML(enrollmentRequest);
		FinanceSecureRequest financesecureRequest = new FinanceSecureRequest();
		String getResp = financesecureRequest
				.sendSecureRequest(
						ghixRestTemplate,
						DynamicPropertiesUtil
								.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER),
						EnrollmentEndPoints.UPDATE_SHOP_ENROLLMENT_STATUS_URL, ernReq, RequestMethod.POST);
		
		EnrollmentResponse  enrollmentResponse = (EnrollmentResponse) xstream.fromXML(getResp);

		/*
		 * Excluding 204 error code which enrollment is in Terminated state or does'nt exist
		 */
		if(enrollmentResponse != null && StringUtils.isNotEmpty(enrollmentResponse.getStatus())
				&& GhixConstants.RESPONSE_FAILURE.equals(enrollmentResponse.getStatus()) && 
				enrollmentResponse.getErrCode() != FinanceConstants.ENROLLMENT_ERROR_CODE_PAYMENT_UPDATE)
		{
			LOGGER.warn("ENROLLMENT updateEmployerEnrollmentStatus call failed - "
					+ enrollmentResponse.getErrCode()
					+ enrollmentResponse.getErrMsg());
			throw new GIException(enrollmentResponse.getErrCode(),
					enrollmentResponse.getErrMsg(),
					FinanceConstants.EXCEPTION_SEVERITY_WARN);
		}
		return GhixConstants.RESPONSE_SUCCESS;
	}
	
	/**
	 * Method to update Employer Enrollment status 
	 * @param status
	 * @param employerEnrollmentId
	 * @return String
	 * @throws GIException
	 */
	public String updateEmployerEnrollmentStatus(String status,int employerEnrollmentId) throws GIException
	{
		/*
		 * Below object is created due to
		 * org.springframework.http.converter.HttpMessageNotWritableException: Could not write JSON: Infinite recursion 
		 * As If we pass a hibernate mapped Employer object it causes the above mentioned Exception. 
		 */
		Map<String, String> requestParameters = new HashMap<String, String>();
		requestParameters.put("status", status);
		requestParameters.put("employerEnrollmentId", String.valueOf(employerEnrollmentId));
		   try
		   {
				String pathVars = FinanceGeneralUtility.generateQueryString(requestParameters);
				LOGGER.info("Sending request to GHIX-SHOP to update employer enrollment status for the employer enrollment id:"+employerEnrollmentId);
				/*String get_resp = restTemplate.getForObject(ShopEndPoints.UPDATE_EMPLOYER_ENROLLMENT_STATUS+pathVars, String.class);*/
				FinanceSecureRequest financesecureRequest = new FinanceSecureRequest();
				String getResp = financesecureRequest
						.sendSecureRequest(
								ghixRestTemplate,
								DynamicPropertiesUtil
										.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER),
										ShopEndPoints.UPDATE_EMPLOYER_ENROLLMENT_STATUS+pathVars, null, RequestMethod.GET);
				
				LOGGER.info("Response received from GHIX-SHOP for the employer enrollment id:"+employerEnrollmentId);
				XStream xstream = GhixUtils.getXStreamStaxObject();
				ShopResponse shopResponse = (ShopResponse) xstream.fromXML(getResp);
				if(shopResponse != null)
				{
					LOGGER.info("Response received from GHIX-SHOP as "+shopResponse.getStatus() +"for the employer enrollment id:"+employerEnrollmentId);
					/*
					 * Excluding 105 error code for Payment Status update
					 */
					if(GhixConstants.RESPONSE_FAILURE.equals(shopResponse.getStatus()) &&
							shopResponse.getErrCode() != FinanceConstants.SHOP_ERROR_CODE_PAYMENT_UPDATE)
					{
						LOGGER.error("Error in GHIX-SHOP employer enrollment updation:-"+shopResponse.getErrMsg());
						throw new GIException("Unable to Update SHOP Employer Enrollment: "+shopResponse.getErrMsg());
					}
					return GhixConstants.RESPONSE_SUCCESS;
				}
			}
			catch(Exception exception)
			{
				throw new GIException(exception);
			}
		return null;
	}

	/**
	 * Method getRemittanceDataByEnrollmentId
	 * @param int id
	 * @return Object<EnrollmentRemittanceDTO>
	 * @throws GIException
	 */
	public EnrollmentRemittanceDTO getRemittanceDataByEnrollmentId(int id)throws GIException
	{
		FinanceSecureRequest financesecureRequest = new FinanceSecureRequest();
		String getResp = financesecureRequest.sendSecureRequest(ghixRestTemplate, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER), EnrollmentEndPoints.GET_REMITTANCEDATABY_ENROLLMENT_ID +Integer.toString(id), null, RequestMethod.GET);
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrollmentResponse  enrollmentResponse = (EnrollmentResponse) xstream.fromXML(getResp);
		if(enrollmentResponse.getStatus() != null && GhixConstants.RESPONSE_FAILURE.equals(enrollmentResponse.getStatus()))
		{
			LOGGER.warn("ENROLLEE getEnrolleeByEnrollmentId call failed - " +  enrollmentResponse.getErrCode() + enrollmentResponse.getErrMsg());
			throw new GIException(enrollmentResponse.getErrCode(), enrollmentResponse.getErrMsg(), FinanceConstants.EXCEPTION_SEVERITY_WARN);
		}
		return enrollmentResponse.getEnrollmentRemittanceDto();
	}
	
	
	/**
	 * @since 01-01-2014
	 * @param int employerId
	 * @return Object<ShopResponse>
	 * @throws GIException
	 */
	public ShopResponse getEmployerEnrollmentData(int employerId)throws GIException
	{
		ShopResponse shopResponse = null;
		try
		{
			LOGGER.info("Fetching Employer Enrollment for employer_ID: "+employerId);
			Map<String, String> requestParameters = new HashMap<String, String>();
			requestParameters.put("employerId", employerId+"");
			String pathVars = FinanceGeneralUtility.generateQueryString(requestParameters);
			FinanceSecureRequest financesecureRequest = new FinanceSecureRequest();
			/*String get_resp = restTemplate.getForObject(ShopEndPoints.GET_EMPLOYER_DETAILS+pathVars, String.class);*/
			String getResp = financesecureRequest
					.sendSecureRequest(
							ghixRestTemplate,
							DynamicPropertiesUtil
									.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER),
									ShopEndPoints.GET_EMPLOYER_DETAILS+pathVars, null, RequestMethod.GET);
			
			
			XStream xstream = GhixUtils.getXStreamStaxObject();
			shopResponse = (ShopResponse) xstream.fromXML(getResp);
			if(shopResponse != null && GhixConstants.RESPONSE_FAILURE.equals(shopResponse.getStatus()))
			{
					LOGGER.warn("SHOP getEmployerEnrollmentData call failed - " +  shopResponse.getErrCode() + shopResponse.getErrMsg());
					throw new GIException(shopResponse.getErrCode(), shopResponse.getErrMsg(), FinanceConstants.EXCEPTION_SEVERITY_WARN);
			}
		}
		catch(RestClientException rce)
		{
			LOGGER.error("Caught RestClientException in getEmployerEnrollmentData call");
			throw new GIException(rce);
		}
		return shopResponse;
	}
	
	/**
	 * @author Sharma_k
	 * @since 03rd MARCH 2014
	 * @param int employerId
	 * @param int employerEnrollmentId
	 * @param Date terminationDate
	 * @return
	 * @throws GIException
	 * Rest call for Employer_Enrollment INVoluntary termination.
	 */
	public ShopResponse employerEnrollmentTermination(int employerId, int employerEnrollmentId, Date terminationDate, String invoiceCreationDate, List<Long> listOfEnrollment)throws GIException
	{
		LOGGER.info("Terminating Employer Enrollment for Employer enrollment Id : "+employerEnrollmentId);
		Map<String, Object> requestMap = new HashMap<String, Object>();
		requestMap.put(FinanceConstants.EMPLOYER_ENROLLMENT_ID_KEY, employerEnrollmentId);
		requestMap.put(FinanceConstants.EMPLOYER_ID_KEY, employerId);
		requestMap.put(FinanceConstants.TERMINATION_DATE_KEY, terminationDate);
		requestMap.put(FinanceConstants.TERMINATION_REASON_KEY, FinanceConstants.TERMINATON_REASON_STRING);
		requestMap.put(FinanceConstants.TERMINATION_CODE_KEY,  FinanceConstants.TERMINATION_REASON_CODE);
		requestMap.put(FinanceConstants.PAID_INVOICE_CREATION_DATE,  invoiceCreationDate);
		requestMap.put(FinanceConstants.ENROLLMENT_LIST,  listOfEnrollment);
		/*
		 *HIX-45436 Remove logged in user check from REST API requests
		 */
		/*String userName = null;
		try
		{
        	userName = userService.getLoggedInUser().getUsername();
		}
		catch (InvalidUserException e) 
		{
			LOGGER.error("Invalid user exception thrown.",e);
		}
		
		 //If userName is coming out to be null/ then
		requestMap.put("USER_NAME", userName!=null ? userName : DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER));*/
		
		requestMap.put("USER_NAME", DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER));
		ShopResponse shopResponse = null;
		XStream xstream = GhixUtils.getXStreamStaxObject();
		try
		{
			FinanceSecureRequest financesecureRequest = new FinanceSecureRequest();
			String getResp = financesecureRequest.sendSecureRequest(ghixRestTemplate, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER), ShopEndPoints.TERMINATE_EMPLOYER_ENROLLMENT, requestMap, RequestMethod.POST);
			shopResponse = (ShopResponse) xstream.fromXML(getResp);
			
			if(shopResponse != null && shopResponse.getStatus() != null && shopResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE))
			{
				LOGGER.warn("SHOP employerEnrollmentTermination call failed - " +  shopResponse.getErrCode() + shopResponse.getErrMsg());
				throw new GIException(shopResponse.getErrCode(), shopResponse.getErrMsg(), FinanceConstants.EXCEPTION_SEVERITY_WARN);
			}
			else if(shopResponse == null)
			{
				LOGGER.warn("SHOP no response received from employerEnrollmentTermination call.");
			}
		}
		catch(RestClientException rce)
		{
			LOGGER.error("Caught Exception in employerEnrollmentTermination rest call: "+rce);
			throw new GIException(rce);
		}
		catch(Exception ex)
		{
			LOGGER.error("Caught Exception in employerEnrollmentTermination rest call: "+ex);
			throw new GIException(ex);
		}
		return shopResponse;
	}
	
	/**
	 * Method getBrokerByEmployerId
	 * @param employerId int
	 * @return BrokerDTO
	 * @throws GIException
	 */
	public BrokerDTO getBrokerByEmployerId(int employerId)throws GIException
	{
		BrokerInformationDTO brokerInformationDTO = new BrokerInformationDTO();
		brokerInformationDTO.setEmployerId(employerId);
		
		FinanceSecureRequest financesecureRequest = new FinanceSecureRequest();
		XStream requestStream = GhixUtils.getXStreamStaxObject();
		String getResp = financesecureRequest.sendSecureRequest(ghixRestTemplate, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER), BrokerServiceEndPoints.GET_AGENT_INFORMATION_BY_EMPLOYERID, requestStream.toXML(brokerInformationDTO), RequestMethod.POST);
				
		XStream xStream = GhixUtils.getXStreamStaxObject();
		
		BrokerDTO brokerResponseDTO = null;
		if(!StringUtils.isEmpty(getResp)){
			brokerResponseDTO = (BrokerDTO) xStream.fromXML(getResp);
			if(FinanceConstants.FAILURE.equalsIgnoreCase(brokerResponseDTO.getResponseDescription()) ){
				LOGGER.info("Unable to get Agent details by employer id");
				throw new GIException("Unable to get Broker details by employer id");
			}
		}
		
		return brokerResponseDTO;
	}
	
	/**
	 * Rest call to search List of Active Enrollment ID by EmployerId, Status and CurrentDate
	 * 
	 * @param employerID int EmployerID
	 * @param statusList Enrollment status in List
	 * @param currentDate String date in {mm/dd/yyyy} format
	 * @return List<Integer>
	 * @throws GIException
	 */
	/*public List<Integer> findActiveEnrollmentByEmployer(int employerID, List<String> statusList, String currentDate)throws GIException
	{
		LOGGER.info("================ REST Call for findActiveEnrollmentsForEmployer started ..... ");
		Map<String, Object> requestMap = new HashMap<String, Object>();
		requestMap.put(FinanceConstants.CURRENT_DATE, currentDate);
		requestMap.put(FinanceConstants.STATUS, statusList);
		requestMap.put(FinanceConstants.EMPLOYER_ID, employerID);
		
		LOGGER.info("Received Request Object for findActiveEnrollmentsForEmployer REST call : EmployerId: "+employerID
				+ " StatusList: " +statusList +" currentDate: "+currentDate);
		
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		List<Integer> enrollmentIdList = new ArrayList<Integer>();
		try
		{
			FinanceSecureRequest financesecureRequest = new FinanceSecureRequest();
			String restResponse = financesecureRequest.sendSecureRequest(ghixRestTemplate, 
					DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER), 
					EnrollmentEndPoints.FIND_ACTIVE_ENROLLMENTS_FOR_EMPLOYER, requestMap, RequestMethod.POST);
			
			if(StringUtils.isNotEmpty(restResponse))
			{
				EnrollmentResponse enrollmentResponse = (EnrollmentResponse)xstream.fromXML(restResponse);
				if(enrollmentResponse !=null  && enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE))
				{
					LOGGER.warn("Received FAILURE response from Enrollment Rest Call findActiveEnrollmentByEmployer ");
					throw new GIException(enrollmentResponse.getErrCode(), enrollmentResponse.getErrMsg(), FinanceConstants.EXCEPTION_SEVERITY_WARN);
				}
				enrollmentIdList = enrollmentResponse.getIdList();
			}
			
		}
		catch(RestClientException rce)
		{
			LOGGER.error("RestClientException caught in findActiveEnrollmentByEmployer() : "+rce);
			throw new GIException(rce);
		}
		catch(Exception ex)
		{
			LOGGER.error("Exception caught in findActiveEnrollmentByEmployer() : "+ex);
		}
		
		LOGGER.info("================ REST Call for findActiveEnrollmentsForEmployer Ends ..... ");
		return enrollmentIdList;
	}*/
	
	/**
	 * 
	 * @param employerID int Employer ID
	 * @param enrollmentStatus String EmployerEnrollment Status ACTIVE/PENDING
	 * @param nextMonthStartDate java.util.Date()
	 * @return Map<String, Object> contains Open enrollment status and Employer_Enrollment ID 
	 * @throws GIException
	 */
	public Map<String, Object> findEmployerEnrollmentDetails(int employerID, EmployerEnrollment.Status enrollmentStatus, Date nextMonthStartDate)throws GIException
	{
		LOGGER.info("================ In findEmployerEnrollmentDetails method ===============");
		EmployerEnrollmentRequest employerEnrollmentRequest = new EmployerEnrollmentRequest();
		employerEnrollmentRequest.setEmployerId(employerID);
		employerEnrollmentRequest.setStatus(enrollmentStatus);
		employerEnrollmentRequest.setCoverageDate(nextMonthStartDate);
		Map<String, Object> responseDataMap = null;
		
		LOGGER.info("Received request data for findEmployerEnrollmentDetails:  employerID: "
				+ employerID
				+ " enrollmentStatus: "	+ enrollmentStatus
				+ " nextMonthStartDate: " + nextMonthStartDate);
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		
		String openErollmentOver = FinanceConstants.NO;
		FinanceSecureRequest financesecureRequest = new FinanceSecureRequest();
		String getResponse = financesecureRequest.sendSecureRequest(ghixRestTemplate, 
				DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER),
				ShopEndPoints.GET_EMPLOYER_ENROLLMENTS_BY_EMPLOYERID, employerEnrollmentRequest, RequestMethod.POST);
			
		if(StringUtils.isNotEmpty(getResponse))
		{
			responseDataMap = new HashMap<String, Object>();
			ShopResponse shopResponse = (ShopResponse)xstream.fromXML(getResponse);
			if(shopResponse != null)
			{
				if(shopResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE))
				{
					LOGGER.warn("Received FAILURE response for findEmployerEnrollmentDetails request");
					throw new GIException(shopResponse.getErrCode(), shopResponse.getErrMsg(), FinanceConstants.EXCEPTION_SEVERITY_WARN);
				}
				
				if(shopResponse.getResponseData() != null && !shopResponse.getResponseData().isEmpty())
				{
					EmployerEnrollmentDTO employerEnrollmentDTO = (EmployerEnrollmentDTO)shopResponse.getResponseData().get("EmployerEnrollment");
					/*
					 * Only in case of Binder Invoice where status is PENDING provide the OpenEnrollmentEndDate flag.
					 */
					
					/*
					 *For Jira ID: HIX-62617 adding CoverageStartDate responseDataMap 
					 */
					responseDataMap.put(FinanceConstants.COVERAGE_START_DATE_KEY, employerEnrollmentDTO.getCoverageDateStart());
					
					if(enrollmentStatus.toString().equalsIgnoreCase(FinanceConstants.PENDING))
					{
						Date openenrollmentenddate = employerEnrollmentDTO.getOpenEnrollmentEnd();
						if(openenrollmentenddate != null)
						{
							Date currentdate = new TSDate();
							LOGGER.info("inside responsedata open enrollment end date is "+openenrollmentenddate+"..current date is "+currentdate);
							if(currentdate.compareTo(openenrollmentenddate)>0)
							{
								openErollmentOver = FinanceConstants.YES;
							}
							responseDataMap.put(FinanceConstants.OPEN_ENROLLMENT_OVER_KEY, openErollmentOver);
						}
					}
					responseDataMap.put(FinanceConstants.EMPLOYER_ENROLLMENT_ID_KEY, employerEnrollmentDTO.getId());
				}
				
			}
		}
		else
		{
			LOGGER.warn(FinanceConstants.ERR_MSG_NULL_EMPTY_RESPONSE +ShopEndPoints.GET_EMPLOYER_ENROLLMENTS_BY_EMPLOYERID+" rest call For EmployerId: "+employerID);
			throw new GIException(FinanceConstants.ERR_MSG_NULL_EMPTY_RESPONSE +ShopEndPoints.GET_EMPLOYER_ENROLLMENTS_BY_EMPLOYERID+" rest call For EmployerId: "+employerID);
		}
		return responseDataMap;
	}
	
	/**
	 * New API exposed from Enrollment to fetch Enrollment for Binder invoice generation.
	 * If value for lastInvoiceCreationTimeStamp is null then it is expected to send only employerId and employerEnrollmentId in the request.
	 * @param employerId int
	 * @param employerEnrollmentId int
	 * @param statusList List<String> {CONFIRM, TERM, PAYMENT_RECEIVED, ENROLLMENT_STATUS_CANCEL}
	 * @param lastInvoiceCreationTimeStamp Date 
	 * @param nextMonthEndDate Date 
	 * @return
	 */
	public Map<String, Set<EnrollmentCurrentMonthDTO>> findEnrollmentByEmployerDetails(int employerId,
			int employerEnrollmentId, List<String> statusList, Date lastInvoiceCreationTimeStamp, Date nextMonthEndDate)throws GIException
	{
		LOGGER.info("==== inside findEnrollmentByEmployerDetails(-,-,-,-,-) received parameters: EmployerID: "
				+ employerId + " EmployerEnrollmentId: "+ employerEnrollmentId
				+ " statusList: "+ statusList+ " LastInvoiceCreationTimeStamp: "
				+ lastInvoiceCreationTimeStamp+ " NextMonthEndDate: "+ nextMonthEndDate);
		
		Map<String, Object> requestDataMap = new HashMap<String, Object>();
		
		DateFormat dateTimeFormat = new SimpleDateFormat(FinanceConstants.CREATEDON_TIME_FORMAT);
		DateFormat dateFormat = new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT);
		
		requestDataMap.put(FinanceConstants.EMPLOYER_ID, employerId);
		requestDataMap.put(FinanceConstants.EMPLOYER_ENROLLMENT_ID, employerEnrollmentId);
		requestDataMap.put(FinanceConstants.STATUS, statusList);
		//LAST_INVOICE_TIMESTAMP is expected in format {dd-MMM-yyyy HH.mm.ss.S}
		requestDataMap.put(FinanceConstants.LAST_INVOICE_TIMESTAMP, lastInvoiceCreationTimeStamp !=null ? dateTimeFormat.format(lastInvoiceCreationTimeStamp) : null);
		//CURRENT_DATE which is NextMonthEndDate expected in String format {MM/dd/yyyy}
		requestDataMap.put(FinanceConstants.CURRENT_DATE, nextMonthEndDate!=null ? dateFormat.format(nextMonthEndDate): null);
		
		EnrollmentResponse  enrollmentResponse = null;

		try
		{
			FinanceSecureRequest financesecureRequest = new FinanceSecureRequest();
		    String restCallResponse = financesecureRequest.sendSecureRequest(ghixRestTemplate,
					DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER),
					EnrollmentEndPoints.FIND_ENROLLMENT_BY_EMPLOYER, requestDataMap, RequestMethod.POST);
			if(StringUtils.isNotEmpty(restCallResponse))
			{
				XStream xstream = GhixUtils.getXStreamStaxObject();
				enrollmentResponse = (EnrollmentResponse) xstream.fromXML(restCallResponse);
				
				if(enrollmentResponse != null)
				{
					if(GhixConstants.RESPONSE_FAILURE.equals(enrollmentResponse.getStatus()))
					{
						LOGGER.warn("Enrollment getEnrollmentByEmployer call failed - " +  enrollmentResponse.getErrCode() + enrollmentResponse.getErrMsg());
						throw new GIException(enrollmentResponse.getErrCode(), enrollmentResponse.getErrMsg(), FinanceConstants.EXCEPTION_SEVERITY_WARN);
					}
				}
				else
				{
					throw new GIException("Received Null or empty Enrollment Response for FIND_ENROLLMENT_BY_EMPLOYER call");
				}
				
			}
			else
			{
				LOGGER.warn(FinanceConstants.ERR_MSG_NULL_EMPTY_RESPONSE +EnrollmentEndPoints.FIND_ENROLLMENT_BY_EMPLOYER +
						"rest call for EmployerId: "+employerId);
				throw new GIException(FinanceConstants.ERR_MSG_NULL_EMPTY_RESPONSE +EnrollmentEndPoints.FIND_ENROLLMENT_BY_EMPLOYER +
						"rest call for EmployerId: "+employerId);
			}
		}
		catch(RestClientException rce)
		{
			LOGGER.error("RestClientException caught in findEnrollmentByEmployerDetails(-,-,-,-,-) "+rce);
			throw new GIException("RestClientException caught in findEnrollmentByEmployerDetails(-,-,-,-,-) for EmployerID: "+employerId, rce);
		}
		catch(Exception ex)
		{
			LOGGER.error("Exception caught in findEnrollmentByEmployerDetails(-,-,-,-,-) For EmployerID: "+employerId+" cause: "+ex);
			throw new GIException("Exception caught in findEnrollmentByEmployerDetails(-,-,-,-,-) for EmployerID: "+employerId, ex);
		}
		return enrollmentResponse.getEnrollmentFinanceMap();
	}
	
	/**
	 * Rest call to fetch employees details based on employees IDs
	 * @param employeeIds list of employees IDs
	 * @throws GIException 
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, EmployeeDetails> getEmployeeDetails(List<Integer> employeeIds) throws GIException
	{
		GetEmployeesDTO getEmployeesDTO = new GetEmployeesDTO();
		getEmployeesDTO.setEmployeeIds(employeeIds);
		FinanceSecureRequest financeSecureRequest = new FinanceSecureRequest();
		String shopResp =financeSecureRequest
				.sendSecureRequest(
						ghixRestTemplate,
						DynamicPropertiesUtil
								.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER),
						ShopEndPoints.GET_EMPLOYEES_DETAILS, getEmployeesDTO,
						RequestMethod.POST);
		
	//	String shop_resp = restTemplate.postForObject(ShopEndPoints.GET_EMPLOYEES_DETAILS, getEmployeesDTO, String.class);
		XStream xstream = GhixUtils.getXStreamStaxObject();
		ShopResponse shopResponse = (ShopResponse)xstream.fromXML(shopResp);
		if(shopResponse != null)
		{
			if(GhixConstants.RESPONSE_SUCCESS.equals(shopResponse.getStatus()))
			{
				Map<String, Object> shopResponseData = shopResponse.getResponseData();
				if(shopResponseData != null && !shopResponseData.isEmpty())
				{
					return (Map<Integer, EmployeeDetails>) shopResponseData.get("empDetlsMap");
				}
				else
				{
					throw new GIException("Null or Empty ResponseData found in ShopResponse");
				}
			}
			else
			{
				LOGGER.error("Error while fetching shop employees Details "+shopResponse.getErrMsg());
				throw new GIException(shopResponse.getErrCode(), shopResponse.getErrMsg(), FinanceConstants.EXCEPTION_SEVERITY_HIGH);
			}
		}
		else
		{
			throw new GIException("Shop Response is either Null or Empty");
		}
			
	}
	
	/**
	 * Rest call to fetch Designated Broker details by List of EmployerID's
	 * @since 01st December 2014
	 * @param employerIDList ArrayList<Integer>
	 * @return List<Object<BrokerDTO>>
	 * @throws GIException
	 */
	public List<BrokerDTO> getDesignatedBrokersForListOfEmployers(List<Integer> employerIDList)throws GIException
	{
		LOGGER.info("Before making rest call to Broker for DesginatedBrokersList url: "+BrokerServiceEndPoints.GET_LISTOF_DESIGNATED_BROKERS);
		BrokerInformationDTO brokerInformationDTO = new BrokerInformationDTO();
		brokerInformationDTO.setEmployerIds(employerIDList);

		FinanceSecureRequest financesecureRequest = new FinanceSecureRequest();
		XStream requestStream = GhixUtils.getXStreamStaxObject();
		
		String getResp = financesecureRequest
				.sendSecureRequest(ghixRestTemplate,
						DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER),
						BrokerServiceEndPoints.GET_LISTOF_DESIGNATED_BROKERS,
						requestStream.toXML(brokerInformationDTO),
						RequestMethod.POST);
			
		XStream xStream = GhixUtils.getXStreamStaxObject();
		
		if(!StringUtils.isEmpty(getResp))
		{
			BrokerListDTO brokerListDTO;
			brokerListDTO = (BrokerListDTO) xStream.fromXML(getResp);
			
			if(brokerListDTO != null)
			{
				if(StringUtils.isNotEmpty(brokerListDTO.getResponseDescription()) &&
						FinanceConstants.FAILURE.equalsIgnoreCase(brokerListDTO.getResponseDescription()))
				{
					LOGGER.error("Exception occurred in rest call to fetch Broker List: "+brokerListDTO.getResponseDescription());
					throw new GIException(brokerListDTO.getResponseCode(), brokerListDTO.getResponseDescription(),
							FinanceConstants.EXCEPTION_SEVERITY_HIGH);
				}
				
				return brokerListDTO.getDesignatedBrokersListResponse();
			}
			else
			{
				throw new GIException("No Brokers found or Empty response recieved from "+BrokerServiceEndPoints.GET_LISTOF_DESIGNATED_BROKERS);
			}
		}		
		else
		{
			throw new GIException("Null or Empty response recieved from "+BrokerServiceEndPoints.GET_LISTOF_DESIGNATED_BROKERS+ " Rest Call");
		}
	}
	
	/**
	 * Rest call to fetch issuer payment information by hios_id
	 * @since 01st December 2014
	 * @param hios_id String
	 * @return String
	 * @throws GIException
	 */
	public String getIssuerPaymentInfoByHiosId(String hiosId) throws GIException
	{
		IssuerRequest issuerRequest = new IssuerRequest();
		issuerRequest.setId(hiosId);
		return new FinanceSecureRequest().sendSecureRequest(ghixRestTemplate, DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER), PlanMgmtEndPoints.GET_ISSUER_PAYMENT_INFO_BY_HIOSID, issuerRequest, RequestMethod.POST);
	}
	
	
	public EnrolleeDetailsDto findEnrolleeDtlBeforeLastInvoice(Integer enrolleeId,Date lastInvoiceCreationTimeStamp)throws GIException
	{
		LOGGER.info("==== inside findEnrolleeDtlBeforeLastInvoice(-,-) received parameters: EnrolleeId: "
				+ enrolleeId + " Last Invoice date and time: "+ lastInvoiceCreationTimeStamp);
		
		EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
		XStream requestStream = GhixUtils.getXStreamStaxObject();
		
		enrolleeRequest.setEnrolleeId(enrolleeId);
		enrolleeRequest.setLastInvoiceTimestamp(lastInvoiceCreationTimeStamp);
		
		EnrolleeResponse  enrolleeResponse = null;
		
		try
		{
			FinanceSecureRequest financesecureRequest = new FinanceSecureRequest();
		
			String restCallResponse = financesecureRequest.sendSecureRequest(ghixRestTemplate,
					DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER),
					EnrollmentEndPoints.GET_ENROLLEE_COVERAGE_DETAILS_BEFORE_LAST_INVOICE, requestStream.toXML(enrolleeRequest), RequestMethod.POST);
			
			if(StringUtils.isNotEmpty(restCallResponse))
			{
				XStream xstream = GhixUtils.getXStreamStaxObject();
				
				enrolleeResponse = (EnrolleeResponse) xstream.fromXML(restCallResponse);

				if(enrolleeResponse != null)
				{
					if(GhixConstants.RESPONSE_FAILURE.equals(enrolleeResponse.getStatus()))
					{
						LOGGER.warn("Enrollment findEnrolleeDtlBeforeLastInvoice call failed - " +  enrolleeResponse.getErrCode() + enrolleeResponse.getErrMsg());
						throw new GIException(enrolleeResponse.getErrCode(), enrolleeResponse.getErrMsg(), FinanceConstants.EXCEPTION_SEVERITY_WARN);
					}
				}
				else
				{
					throw new GIException("Received null or empty EnrolleeResponse from GET_ENROLLEE_COVERAGE_DETAILS_BEFORE_LAST_INVOICE call");
				}
			}
			else
			{
				LOGGER.warn(FinanceConstants.ERR_MSG_NULL_EMPTY_RESPONSE +EnrollmentEndPoints.GET_ENROLLEE_COVERAGE_DETAILS_BEFORE_LAST_INVOICE +
						"rest call for EnrolleeId: "+enrolleeId);
				throw new GIException(FinanceConstants.ERR_MSG_NULL_EMPTY_RESPONSE +EnrollmentEndPoints.GET_ENROLLEE_COVERAGE_DETAILS_BEFORE_LAST_INVOICE +
						"rest call for EnrolleeId: "+enrolleeId);
			}
		}
		catch(RestClientException rce)
		{
			LOGGER.error("RestClientException caught in findEnrolleeDtlBeforeLastInvoice(-,-,-,-,-) "+rce);
			throw new GIException("RestClientException caught in findEnrolleeDtlBeforeLastInvoice(-,-,-,-,-) for EnrolleeId: "+enrolleeId, rce);
		}
		catch(Exception ex)
		{
			LOGGER.error("Exception caught in findEnrolleeDtlBeforeLastInvoice(-,-,-,-,-) For EnrolleeId: "+enrolleeId+" cause: "+ex);
			throw new GIException("Exception caught in findEnrolleeDtlBeforeLastInvoice(-,-,-,-,-) for EnrolleeId: "+enrolleeId, ex);
		}
		return enrolleeResponse.getEnrolleeDetailsDto();
	}
}
