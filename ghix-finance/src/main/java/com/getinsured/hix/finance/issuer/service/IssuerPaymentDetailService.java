package com.getinsured.hix.finance.issuer.service;

import java.util.Date;
import java.util.List;

import com.getinsured.hix.model.IssuerPaymentDetail;
import com.getinsured.hix.model.IssuerPaymentDetail.EDIGeneratedFlag;

public interface IssuerPaymentDetailService {
	IssuerPaymentDetail saveIssuerPaymentDetail(IssuerPaymentDetail issuerPaymentDetail);
	
	List<IssuerPaymentDetail> getCurrentMonthPayments(Date startDate, Date endDate);
	
	List<IssuerPaymentDetail> listOfNonEDIGeneratedPayments(EDIGeneratedFlag ediGeneratedFlag);
	
	IssuerPaymentDetail findByMerchantRefCode(String merchantRefCode);
	
	/**
	 * 
	 * @return
	 */
	List<Integer> getDistinctIssuerIdEDINotGenerated();
	
	/**
	 * 
	 * @param issuerId
	 * @return
	 */
	List<IssuerPaymentDetail> findPaymentDtlByIssuer(int issuerId);
	
	/**
	 * 
	 * @param issuerPaymentDetailList
	 * @return
	 */
	List<IssuerPaymentDetail> saveIssuerPymtDtlList(List<IssuerPaymentDetail> issuerPaymentDetailList);
	
	/**
	 * 
	 * @param employerInvoicesId
	 * @return
	 */
	List<Object[]> getRequestIdNMerchantRefCode(Integer employerInvoicesId);
}
